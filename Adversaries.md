![Narvi](https://bitbucket.org/HawthornThistleberry/narvi/raw/24835bdaaaa49bed9a5153abd7a3a8691524f041/images/banner.jpg)

a Discord bot for the *The One Ring* roleplaying game, written by HunterGreen

* for support visit the TOR/LOTR:RP Discord at https://discord.me/theonering
* for announcements of updates subscribe at https://discord.gg/the-one-ring-lotr-rpg-348254014598545408

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V44RKAI) to help pay for cloud-hosting

# Introduction

This document explains how to create adversaries and add them to the catalog, find adversaries already in the catalog, and then use them and other functions to build encounters you will be able to use in combats. See the [main README](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md) and the [How To](https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md) for more information about how to use these encounters in combat.

As you read on, you will be learning each of the three main steps in making use of Narvi's support for adversaries and encounters. It's important to understand how each of the three steps fits with the other two in this sequence:

1. **Find or Create Adversaries**: You will use `/aedit` to create an adversary and add it to the global catalog. (Note that you might not have to create them, as many are already in there; you can just use them, but you'd still use `/aedit` to find them.)
2. **Assemble Adversaries into Encounters**: Then you will use `/encounter` to create, in advance of your session, encounters within your game, that include one or more adversaries, and optionally other elements, like events or complications.
3. **Run The Encounters**: Finally, when it's game time, you will `/start` a combat and specify one of the encounters you created, and everything in it will be brought into the combat and handled as automatically as possible for you.

[TOC]

# Adversaries

## The Adversary Catalog

As with journeys and lookup tables, anyone can create an adversary, then publish it so anyone else can use it. The core adversaries in the published books are already there for you to use in your own encounters.

* `/aedit list`: See a list of all adversaries available to you. You can further refine the list by looking for only your own (or not your own), only published (or not published), only ones with certain tags, or only ones with some text appearing in them.
* `/aedit select`: Select an adversary for editing or viewing.
* `/aedit show`: Display the selected adversary.

![Southern Raider](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/southern-raider.png)

## Creating and Editing Adversaries

The commands you will use for creating and editing your own adversaries:

* `/aedit create`: Create an adversary.
* `/aedit copy`: Create an adversary by making a copy of an existing one. You can edit from there.
* `/aedit set`: Set some property of the adversary (see below).
* `/aedit setup`: Set a bunch of properties of the adversary all at once.
* `/aedit attack`: Give the adversary a combat proficiency (see below).
* `/aedit ability`: Give the adversary a fell ability (see below).
* `/aedit clear`: Clear all attacks or fell abilities.
* `/aedit publish`: When the adversary is done, publish or unpublish it.
* `/aedit delete`: Delete an adversary.

### Shortnames

Every adversary is known by a 'shortname' which is sort of like its license plate. Much like game names, table shortnames, and journey shortnames, this is a single word name that can be used in commands to refer to the adversary. Try to choose a short name that is easy to type and yet clear which kind of enemy it is, keeping in mind that it should be distinct from any others -- for instance, if you named the first adversary you made `troll` what happens when you make a new adversary later that's also a Troll?

### Tags

Narvi has a list of tags that you can (and should!) tack onto your adversaries to help people searching for adversaries find the right ones. It's vital to understand that an adversary can have multiple tags, and usually should. Here are the tags and their meanings (more may become available later):

* `tor`: This adversary is used for _The One Ring_.
* `aime`: This adversary is used for _Adventures in Middle-earth_.
* `lotrrp`: This adversary is used for _Lord of the Rings: Roleplaying_.
* `core`: This adversary comes from the core rules.
* `strider`: This adversary comes from the Strider Mode rules.
* `starter`: This adversary comes from the TOR starter set.
* `ruins`: This adversary comes from _Ruins of the Lost Realm_.
* `english`: This adversary is in the English language.
* `espanol`: This adversary is in the Spanish language.

Beyond these, all the remaining tags are various types of creatures: `men`, `evilmen`, `southerners`, `ruffians`, `orcs`, `chieftains`, `goblins`, `archers`, `guards`, `soldiers`, `trolls`, `robbers`, `undead`, `wraiths`, `wights`, `wolves`, `beasts`, `wargs`, `spiders`, `nameless`, `blacknumenoreans`, `dwarves`, `elves`, `hobbits`, `wyrms`, `wyverns`, `ents`, `eagles`, `balrogs`, `nazgul`, `mumakil`, `ogres`, `birds`.

For example, a Southern Raider might have these tags: `core, evilmen, men, tor, english, southerners`. Thus, someone doing a search for all adversaries from the core book could find them, or someone finding all evil men, or all southerners. Setting these lets people find the adversary they want even if the catalog gets large.

If you have a desire for another tag, please ask HunterGreen, the author of Narvi, for it, either in the TOR/LOTR:RP Discord (https://discord.me/theonering) in the #narvi channel, or with the `/suggest` command. While we don't want the list of tags to get too huge, generally speaking, more tags is better. 

### Attacks (Combat Proficiencies)

An adversary can have multiple attacks, and each one has a name, number of dice, damage, and injury. For instance:

`Axe 3 (5/14)`: 3 dice, 5 damage, and 14 injury

Attacks can also have a special attack type (Shield Smash, Pierce, Seize, Drain), and also a comment. For example:

`Sword 2 (6/18, Pierce): jagged and rusty but still effective`

Finally, attacks can be tagged as ranged (for instance, bows). This is used for two things: first, to determine if it can be used in Opening Volley (and thus, in turn, whether the adversary can act in Opening Volley), and second, whether attacks with it ignore all combat stance bonus and penalty dice.

If you enter an attack in error, just enter it again; if the name is the same it will replace the existing one. You can clear them all and start over with `/aedit clear`.

### Fell Abilities

Fell Abilities work much like attacks, in that you can have multiple ones, ones with the same name replace existing ones, and you can clear them. A fell ability's name can be anything, but Narvi will look for certain specific names (in English only), much like how it handles virtues on character sheets, to automatically handle them.

Some fell abilities come in slightly different variations. For each fell ability you can specify a `type` and `amount` which may be applicable for those specific abilities. These are the abilities that use types or amounts:

* **Dreadful Spells**: Some creatures (e.g., wights) have a version that causes unconsciousness; some (notably in _Ruins of the Lost Realm_) have variations that cause other conditions. You can use the `type` to note what condition the victim will get if they fail; see Conditions in the [main README](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md) for the list of available conditions.
* **Strike Fear**: The amount specifies how many points of Dread (some creatures have 1, some 2). Generally, if you specify an amount, it will be used for the shadow loss on any fell ability that uses a shadow test, though most will default to the typical amount.
* **Unnatural Hunger**: When activated, the creature gains as many points of Hate as you specified for an amount.

Others may be added to this list as the publisher releases more creatures, especially if (as they did with _Ruins_) they take an ability that had been consistent in all appearances in earlier books, and give it a variation.

Note: some adversaries, such as the Barrow-wight, have so many abilities that they exceed Discord's limitations for a text display within an embed. When this happens, Narvi will omit the comments, like so:

![Abbreviated Adversary Display](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/barrow-wight.png)

### Other Properties

In addition to the above, you can specify the following things with `/aedit set`:

* Full Name: While the shortname is what you'll type, the full name is what everyone calls the creature. Use a singular form (e.g., Goblin Spy, not Goblin Spies).
* Description: Usually a paragraph, this appears at the top after the creature's name.
* Image: The URL of an image that will appear both at the bottom of the creature's display, and on its rolls.
* Features: Most adversaries have two distinctive features, which you should separate with commas.
* Hate or Resolve: Minions of the Enemy and monstrous creatures use Hate, while evil men and non-monstrous creatures use Resolve. You will specify both the amount, and which kind it represents. The amount here is the amount the adversary starts with.
* Endurance: Also the amount the adversary starts with.
* Attribute, Might, Parry, Armour: Vital statistics for the adversary.
* Bane Type: If this is set to Orcs, Trolls, Wolves, Evil Men, Spiders, Undead, or The Enemy, Famous Weapons with the corresponding type will have appropriate abilities automatically implemented.
* Size: While this is a free-form description, any value other than `Tiny` or `Small` will trigger the Hobbit virtue Small Folk.

# Encounters

An encounter is how a loremaster can package a group of adversaries, along with some other elements, together to prepare for a combat. Unlike adversaries, encounters are part of a specific game, and thus are not published. Players cannot see or interact with encounters -- no sense spoiling them about what's coming up!

As with adversaries, encounters have a shortname. (As we will soon see, the adversaries within them also have unique shortnames.)

It's important to note that an encounter will later _be activated in a combat_, and as that combat plays out, the encounter will be changed (e.g., as adversaries are damaged or killed). If you want to save an encounter for reuse, make a copy, so when one combat happens and changes the encounter, the copy remains ready for use. (You will have the option to do this automatically when you start the combat.)

This also means you can run an encounter, end the combat, and then start again with the same encounter and it will be in the same state as at the end of the last combat; this can be useful when a fight runs for a while, breaks, and then resumes (e.g., during the Battle of the Hornburg, there were a few breaks in the action, but after the fighting was re-engaged, the heroes and adversaries were in about the same state as they'd been in before, minus whatever rest they'd gotten).

* An encounter being created has no effect on anything until you `/start` a combat and specify that that encounter should be used in that combat. Starting a combat and using the encounter will automatically select the encounter; but having selected the encounter does not automatically include it in a combat unless you do so when you use `/start`. After all, you might create multiple encounters in advance of a session, so Narvi needs to know which one this combat should use. Once you `/start` and specify the encounter, the adversaries, events, and modifiers included in it will become part of the combat. See **Using Encounters In Combats** below.

## Creating Encounters

To create and work with the basics of an encounter:

* `/encounter create`: Create an encounter within your game.
* `/encounter select`: Select an encounter to edit.
* `/encounter show`: Show the encounter.
* `/encounter copy`: Make a copy of an encounter.
* `/encounter combine`: Merge two encounters into one. Narvi may append letters to the end of adversary shortnames to avoid duplicates.

## Editing Encounter Basics

You can do these things (as well as those in the next two sections, Events and Adversaries), _before_ the combat (to prepare ahead), but you can also do most of them on the fly _during_ the combat. (Note, though, that the `title` and `modifier` are only used at the encounter's start, so changing them while the combat is active will have no meaningful effect.)

* `/encounter title`: Give an encounter a long title which will become the title of the combat when activated.
* `/encounter notes`: Add some notes that only you will typically see.
* `/encounter image`: Add a URL for the encounter that will be displayed when it starts.
* `/encounter modifier`: If an encounter will start with a complication or advantage, you can include it.
* `/encounter clear`: Remove all events or adversaries from an encounter, or details from an adversary within it (see below).

## Events

An encounter can also contain one or more events, which are simply reminders to you that something will happen. For example, maybe three rounds after the combat begins, a trap goes off, or reinforcements arrive, or the bridge collapses, or the sorcerer's spell is completed. Use `/encounter event` to add an event, specifying the round and phase, along with the text. That event will then be displayed in initiative during that round and help to remind you when something happens. Events can be set as private and will not show in `/current`, `/next`, `/report initiative`, `/game show`, etc. but when their time comes up they will be displayed in the `/game lmchannel`.

## Adversaries

An encounter can contain multiple adversaries. Every adversary starts out as a copy from one in the adversary catalog (except automatically generated Nameless Things, see below); but you can then modify the copy as much as you want. In fact, Narvi has a function to modify them for you as they're added, which we'll discuss in a moment.

Add an adversary (or several) with the `/encounter adversary` command. You will specify four things, which we will cover one by one below:

* `catalog`: the shortname of the Adversary catalog entry to start from
* `shortname`: the shortname of the resulting adversary created in the encounter
* `quantity`: how many of this adversary to add to the encounter
* `vary`: whether Narvi should do some math to make multiple adversaries slightly different

### Catalog and Shortnames

If you have six orcs in a fight, you need a way to refer to each one individually, so every adversary within the fight has a _unique_ shortname. Displaying the adversary within the encounter will show the shortname of the adversary catalog entry that was used to create it, just for informational purposes, but it will primarily use the adversary shortname you specified.

For instance, if you added a `bnsoldier` (Black Numenorean Soldier) to the battle, you could use as its shortname `bnsoldier` if you really wanted to, but you could also use `BN` for faster typing later, or `Leroy` if that happens to be his name.

When you specify a quantity to add, Narvi will add a digit to the end of the shortname you provide. For instance, if you chose `BN` as your shortname, you would get adversaries named `BN1`, `BN2`, and so on. If you use a phrase, such as `Enemy Soldier`, Narvi will add a space, giving you results like `Enemy Soldier 1` and `Enemy Soldier 2`. (But note that since you're going to have to be typing their names, and they're all the same so no short version will be unique, you probably will be better off to go with a very short version like `ES`.)

### Varying Stats

Normally every Goblin Archer has exactly the same stats. Players may get to know them and start to metagame about this; for instance, choosing Heavy Blow instead of Pierce because they're confident they'll get enough to beat a Goblin Archer. Which is not always a bad thing; their player-heroes get to know how much a Goblin Archer can take, after all. But are all Goblin Archers really identical?

When you add one or more adversaries to an encounter, if you specify `vary`, the stat or stats you identify will be passed through a simple random number multiplier when they're being added. The function Narvi uses (`(1 + (5d21-55)/100)`, if you're curious) makes a fairly tight bell curve; for instance, for a 20 endurance, 11 and 29 are possible, but only in 0.02% of cases each, while a 20 has about a 14.3% chance, 19 or 21 each have about a 13.5% chance, and so on. So most soldiers have 20 endurance, or only a point or two more or less, though a few exceedingly rare ones might have almost half or almost double.

![bell curve](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/bell-curve.png)

Note that this is a multiplier, so you will see less impact on lower values. Varying parry or armor may have little or no effect unless these values are at least 3.

### Revising Adversaries In Encounters

You can change individual adversaries within an encounter (even while that encounter is active) using the `/encounter set`, `/encounter attack`, `/encounter ability`, and `/encounter clear` commands; this works similarly to the corresponding `/aedit` commands above, affecting just a single adversary in that encounter, plus you can set a number of additional values reflecting that adversary's current status (e.g., its current endurance).

You can specify an adversary shortname in `/encounter show` to view that adversary in detail.

Delete a single adversary out of an encounter with `/encounter delete`.

### Nameless Things

Narvi can automatically generate a Nameless Thing with the `/nameless` command. Just provide a shortname and optionally a few other things the tables don't provide. The Nameless Thing is built and added to your current encounter (but is not added to the adversary catalog), and you can then use `/encounter set` to modify things as you see fit.

![Dargreb, the watcher of Udun](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/dargreb.png)

### Generators

Moria introduced the Orc-Band Generator, and Narvi knows how to automatically build orc-bands using it, and if similar generators are made in the future, Narvi will be able to use those as well. (If you want to make your own, though, get in touch with HunterGreen since some hidden codes have to be built into the [lookup tables](https://bitbucket.org/HawthornThistleberry/narvi/src/master/LookupTables.md) that you won't be able to do.) Just create the encounter, and then use `/encounter generate` to add adversaries to it according to the rolls. The table names for the Moria orc band generator are `orcbandfeat` and `orcbandsuccess` (which are the defaults, so you won't need to type them).

# Using Encounters In Combats

Encounters you create just sit around in your game waiting to be used, and have no other effect, until you start a combat using `/start` and then in the `encounter` option specify the encounter's shortname. This is to allow you to have multiple encounters cued up and ready to use when you start your session.

Note that when you `/start` a combat and include an encounter, that encounter will be changed by the combat. Someone does damage to a foe? That damage will persist, even after you `/end` the combat. Thus, if you have made a sort of 'generic' encounter you will want to reuse later - say, a set of bandits of a type that the heroes might encounter again another day - and you want the new bandits to come at the heroes fresh and rested, when you `/start` the combat, and pick the encounter, you can use `copyencounter`. Narvi will make a copy, then all subsequent changes will happen to the copy, leaving your original encounter pristine and ready for reuse later.

But often, maybe most of the time, you will use the encounter just once, so when you `/start` and use the encounter, then you `/end`, you have the option to delete the encounter. If it's a copy because you used `copyencounter` above you almost certainly want to delete it. Even if it's not, you might anyway. If all those trolls are dead, why keep them around, clogging up Narvi?

But maybe once in a while you don't want to delete it. Maybe the encounter ends because the heroes (or the adversaries) fled, but they will meet up again, with all the same wounds and weariness still stacked up on them. Simply not deleting the encounter when you `/end`, and then specifying it again when you `/start`, will return things to just how they were. (You can also use `/encounter` commands to give the adversaries changes in between, such as if they got a bit of time to heal up.)

Using an encounter does the following:

* It is automatically selected, so you can edit it on the fly with `/encounter`.
* You cannot delete the encounter while it is being played.
* The encounter's title will be used for the combat, unless you provide a title yourself.
* Any modifier in the encounter will be added to the combat, possibly in addition to one you provide.
* Events in the encounter will appear in initiative displays. Secret ones will go to your `lmchannel`.
* The adversaries in the encounter will appear in initiative displays in the Adversary Actions phase.
* Narvi will keep track of which adversaries have taken their action, and help you note when it's time to move on from the Adversary Actions phase.
* Players can use `/foe list` to see their menu of targets to attack, as well as engagement. (Optionally if you have used `/game config Combat Adversary Health` this list also shows a rough estimate of the adversary's wound and endurance status.)
* Player-heroes can `/attack` a `target` by its shortname, automatically doing damage and applying other effects, including those from qualities, virtues, rewards, etc.
* Adversaries can `/foe attack` and similarly automatically affect player-heroes with damage, protection tests, etc.
* You will be able to use `/foe ability` to use certain actively-used fell abilities.
* Many fell abilities will be automatically implemented. See the [main README](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md) for the details.
* Protection tests and shadow tests are queued for both player-heroes and adversaries, and automatically resolved with a corresponding `/roll` or `/foe protection` roll (or via a `/go` panel).

The encounter remains part of the combat until you `/end` it, but any changes you make to the encounter (including using `/encounter combine` to bring in a second encounter, or adding more adversaries with `/encounter adversary`), will take effect in the combat immediately.