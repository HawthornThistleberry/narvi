![Narvi](https://bitbucket.org/HawthornThistleberry/narvi/raw/24835bdaaaa49bed9a5153abd7a3a8691524f041/images/banner.jpg)

a Discord bot for the *The One Ring* roleplaying game, written by HunterGreen

for support visit the TOR/LOTR:RP Discord at https://discord.me/theonering

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V44RKAI) to help pay for cloud-hosting

This page includes guides on how to do various common game processes for TOR in Narvi. While the [main README](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md) is like a product guide, this is more like a cookbook. We'll talk about *how* to do things here, and you can go back to the main README to learn more about why those commands do those things.

# Interactive Interface #

Narvi features an interactive interface which you can start with the very simple `/go` command. How it works: Narvi will figure out what's currently going on for you, and present a panel of information with buttons and menu options relevant to what you're most likely to be doing just then. This won't be able to support every possible option, of course, especially for the Loremaster, but it will help you with some common options and make it easier to learn how to do them, even if you may sometimes have to drill deeper to use the commands.

Panels supported for the Loremaster (whichever one appears first on this list is the one you'll get):

* if any adversaries have a pending protection test, a panel to roll them (this automatically pops up in your LM channel too)
* if a journey, council, or skill endeavour are active, a panel with options relevant to those
* if none of the above apply, a panel with buttons that launch other panels:
  - Start Skill Endeavour, Start Council: the options that let you set one up and get it going
  - Start Journey, Start Combat: pull-down menu of journeys or encounters and buttons to start them
  - Shadow Tests: create shadow tests of any type for anyone or everyone in the company
  - Combat Modifiers: create modifiers (advantages and complications) for your combat
  - Environmental Sources of Injury: cause endurance loss due to fire, cold, etc.
  - Eye Awareness: set and adjust eye awareness and threshhold
  - Company-Wide Actions: clear conditions, give out awards, handle rest and recovery, etc.
  - If a combat is active, this also includes a row to change the light level.

![menu panel](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/go-lmmenu.png)
![shadow tests panel](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/lm-shadowtest.png)

# Game Setup

Only the loremaster can set up a game, and that game will forever belong to that loremaster. Creating a game is only a few steps:

* Pick a name. Be prepared to have to change it if someone else beat you to that name. Spaces and capitalization will be removed from the game name, so be prepared for that too; a game named Push It Farther is going to look a bit rude when the spaces are taken out.
* `/game create` - do this in the server where the game will be played, so Narvi can automatically associate the server with the game.
* You might `/game system strider` to note a Strider Mode game, or for [other games](https://bitbucket.org/HawthornThistleberry/narvi/src/master/OtherGames.md).
* Advanced users might want to set `/game config` to configure Narvi to some special behaviors (see the main README).
* You could set `/game dicestyle` to change from the default way dice rolls are shown.
* Sometimes Narvi will send private messages to the loremaster; by default these go by Discord private messages. You might want to create a channel in your game server, go there, and do `/game lmchannel here` to designate it to get private messages for that game.
* Finally, you should definitely `/game add` for each of your players, which gives them the ability to do character creation. You also can create characters (and even transfer them to players if you want to set up pregens).

# Character Creation

Whether a player or a loremaster, creating a character in a game is mostly done through the Google Sheets character sheet.

* If your game isn't selected, `/game select`. Note, if you have `/autoswitch` set on, doing any command in the server associated with the game will save you this step.
* https://docs.google.com/spreadsheets/d/1ORyskKJvepDqS5EK0mzlO-q1qeaLno6tyBW7_MPTuNM/template/preview
* Click **Use Template** in the upper right corner. This makes a new, blank sheet in your own Google Drive.
* Save it with the name of the character (or any other filename you want).
* Follow the instructions within the sheet for completing it.
* Click **Share** in the upper right, then **Change** the settings -- it's a good idea to make it so anyone on the Internet can view, though it's not absolutely necessary
* In the `Add people and groups` box, paste this: `tor2e-character-sheets-api@tor2e-character-sheets-api.iam.gserviceaccount.com`
* Change its permissions to `Editor`, uncheck `Notify` and click **Share**
* Copy the character sheet's URL
* Back in Discord, `/character create`
* Note that you can create multiple characters, and switch between them with the `/character select` command
* If you have an image of your character, copy its URL and `/character image` so it will show in rolls
* If you want to add notes in Narvi for a character beyond what's on the Google Sheets sheet, you can use Scribbles: `/scribbles set`
* Later, use `/status` for a quick glance at your character's status (things like endurance, hope, and fatigue).

# Skill Rolls

Most rolls can be done with `/skill` (make a roll on any skill) or `/roll` (also covers other tests like Protection).

There are many options you can use, but here are the common ones:

* `hope`: Spend hope to get an extra 1d.
* `inspired`: Spend hope to get an extra 2d, because a distinctive feature applies.
* `magical`: Spend hope to get a magical success.
* `favoured`: Force the roll to be favored. Narvi normally does this automatically.
* `ill-favoured`: Force the roll to be ill-favored.
* `unfavoured`: Even if a skill roll would normally be favored, don't roll it that way.
* `noitem`: Make Narvi ignore any Useful Item or Marvellous Artifact you have that might otherwise apply.
* `bonus`: Add or remove additional bonus dice.
* `modifier`: Add or subtract something from the total.
* `lmdice`: Force the dice to be, or not be, loremaster (dark) dice, where Eye is the best roll.
* `comment`: Add a comment that helps your LM know what you're rolling.

Also keep in mind that you can use `/feat` to roll one or more feat dice, `/success` to roll one or more success dice, and `/tor` to roll any typical combination of TOR dice, independent of any skill, test, or attack.

# Councils

* The loremaster determines the type of council (`reasonable`, `bold`, `outrageous`) and optionally the attitude (`reluctant`, `open`, or `friendly`).
* `/council start` to start the council. Set `original` to use the rules in first printing before errata.
* `/game config council-original`: `original` becomes the default for all councils in this game.
* Players now make skill rolls as normal, but they specify the roll affects a council when the roll is for some interaction that is part of the council. Narvi automatically figures out the phase of the council and the results, and ends the council when enough rolls have happened, and reports the result.
* `/council show` shows the current status of the council. A briefer version is included in `/game show`.
* The loremaster can `/council time|timelimit|score|resistance` to change any of the values. They can also change the attitude during the council with `/council attitude`, or record a pass or fail with `/council pass|fail`.
* The loremaster can `/council end` to force the council to a conclusion, or `/council undo` to undo the effect of the last player roll that affected the council.

# Skill Endeavours

* The loremaster determines the type of task (`simple`, `laborious`, `daunting`) and time limit.
* `/endeavour start` to start the endeavour.
* Players now make skill rolls as normal, but they specify the roll affects an endeavour when the roll is for some action that is part of the endeavour. Narvi automatically figures out the phase of the endeavour and the results, and ends the endeavour when enough rolls have happened, and reports the result.
* `/endeavour show` shows the current status of the endeavour. A briefer version is included in `/game show`.
* The loremaster can `/endeavour time|timelimit|score|resistance` to change any of the values. They can also record a pass or fail with `/endeavour pass|fail`.
* The loremaster can `/endeavour end` to force the endeavour to a conclusion, or `/endeavour undo` to undo the effect of the last player roll that affected the endeavour.

# Tests

* Narvi automatically builds and queues many kinds of Shadow and Protection tests for both players and adversaries, as a result of attacks or fell abilities.
* When a player uses `/roll` to make a protection or shadow test, Narvi will automatically use the resulting test and implement the corresponding result.
* You can use the `/test` command to see and edit these.
* `/test` can also be used to add a test to the queue. For instance, `/test add` can add a test for a single character or adversary, or `company` to queue one for everyone. You can specify how many points of Shadow for shadow tests, as well as a condition gained on failure (such as daunted), or a TN for protection tests. You can also provide modifiers such as making the test rolls ill-favoured when appropriate.

# Environment Damage (Endurance Loss)

In some situations Narvi will automatically use the rules for Sources of Injury that cause Endurance Loss (TOR p143) including fire, cold, poison, suffocation, and falling (e.g., due to a fell ability, a foe attack, or a journey event). The Loremaster can also do this directly:

* `/environment`: Specify the affected character (or `company` for the whole company), the type of damage, and the severity level.

Narvi will automatically apply wounds or dying conditions as per the rules.

# Journeys

## Quick Journeys

Most of the functionality for journeys described here assumes you can prepare a journey in advance, which in my experience is almost always the case. But sometimes you don't know where the company is going to go. My long-term plans include a way to define a journey on the fly as it progresses, and dynamically update the track on a map. But as a stopgap until then, Narvi supports a "quick journey" which basically works like a long string of short journeys, one step at a time, each of which gets created and started and then deletes itself once it's done. This does mean that fatigue accrues throughout, not all at the end, though; and there are edge cases where a single marching roll ought to carry the company over a border between two types of terrain, but doing it this way will make the company lose some of their progress at those borders. Still, the "quick journey" can be a useful tool, if the loremaster adapts to these factors.

* `/journey quick`: Specify the number of hexes, the type of journey, and optionally the terrain, peril, season, event type table, feat die modifier, and whether the journey is forced. Narvi creates a journey and starts it; then you play it out as described below, and it gets deleted automatically whenever it finishes. You cannot edit or publish the quick journey.

## Creating And Publishing Journeys

For everything else, you'll create the journey, save it, and associate it with your game before you run it. These can also be published; after all, once someone has described the road journey from Bree to Rivendell, everyone else doesn't need to repeat that work.

While the journey has a bunch of useful information like origin, destination, and a map, the key to a journey is a series of **steps**. Each step represents one or more hexes that are all alike. For instance, if your journey goes along the road three hexes and then goes off the road into the wilderness for two more hexes, that is at minimum two steps, a three-hex road step, and a two-hex wilderness step. A step ends and a new one begins whenever the type, terrain, or peril change.

Steps include distances the heroes will cover, and thus, does not include the first hex that the heroes start in, since they're already there.

For example, consider this journey. A Ranger of the North is the guide, escorting four luckless Hobbits east out of Bree, but avoiding the roads to evade pursuit. They have only one pack pony due to some misfortunes in Bree.

![A journey from Bree to the foot of Weathertop](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/journey-example.png)

The company starts in Bree, but immediately moves off the road and into wild lands, heading northeast, one hex. The loremaster decides that under the circumstances the Chetwood is hard terrain. Next, they head east into a perilous area, the Midgewater Marshes, which is only one hex and a peril score of 1, and still hard terrain and wild lands. Finally two more hexes across the Weather Hills, which the loremaster determines is still wild lands but not hard terrain anymore. Here's the resulting steps:

* [1] 1 hexes hard wild: passing through the Chetwood
* [2] 1 hexes hard wild, peril 1: passing through Midgewater Marshes
* [3] 2 hexes wild: crossing the Weather Hills

I hope whoever they were hoping to meet on Weathertop is there, but first they need to make camp and rest before climbing.

### Making and Selecting a Journey

* `/jedit create`: Create a journey. The name must be unique and just letters and numbers. It is automatically selected.
* `/jedit select`: Select a created journey to work on (creating one automatically selects it).
* `/jedit show`: Show your selected journey.
* `/jedit copy`: Make a copy of a journey, so you can edit that one. Can even be someone else's published journey.
* `/jedit reverse`: As with `copy` but also reverses the journey. Good for that 'and back again' chapter of the story.
* `/jedit combine`: As with `copy` but combines two journeys into a new one; it's on you to be sure the ends line up, and you can then edit the new one after (e.g., to replace the image).
* `/jedit delete`: Destroy the journey record. You can't do this if it's published, so unpublish first.
* `/jedit publish`: Journey must be complete. Makes it available to other loremasters to use in their games. Also used to unpublish.

### Setting Basic Journey Info

* `/jedit name`: Give the journey a longer and more descriptive name.
* `/jedit notes`: Additional notes for a journey's display.
* `/jedit tags`: Give your journey one or more tags (separated by spaces) which will help other people find it. Some tags to consider:
    * tor, aime, lotrrp, strider: the system(s) this journey is intended for use with
    * starter, homebrew, ruins: sources for the journey
    * english, espanol: the language you've used to fill out the text
    * breeland, lonelands, trollshaws, etc.: the main region of the journey
    * moria: A journey in Moria will automatically be adjusted to use the relevant rules and tables.
    * underground: Underground journeys prevent the benefits of horses (except Bree-ponies).
* `/jedit origin`: Specify where the journey starts.
* `/jedit destination`: Specify where the journey ends.
* `/jedit eventtype`: Journeys by default use the `journeyevents` table for journey events (from the core rules). However, alternate journey event tables exist, such as the Moria and Redhorn Pass tables from the Moria supplement. Use this to specify the shortname of the table (as seen on `/table list`) of a table to use for this journey. It must be a properly formatted table, and Narvi must be coded for its events, so you can't just make your own table and have it work; it will only work with the following tables at this time (others are being worked on):
  - journeyevents, moriajourneyevents, redhornevents, solojourney, seavoyageevents, northernwasteevents
* `/jedit image`: A URL for an image to show with the journey. Typically an image of a map, but it doesn't have to be.

### Setting Up Steps

* `/jedit add`: Add a 'step' to the journey as described above:
    * `length`: How many hexes long this step is. (Hexes normally represent 20mi, but in Moria they represent 2mi.)
    * `type` (`border`, `wild`, or `dark`): The kind of land it is. Moria should always use `wild`. Even if you expect your group to use a Patron bonus like Gilraen's, specify the true (from the map) land type, because you will be able to override it when you start the journey, so the same journey definition will work for people who aren't using that patron bonus.
    * `terrain` (`hard` or `road`): The kind of terrain, which affects event resolution rolls and fatigue. In Moria, use `hard` when in the Deeps or the Mines of Moria.
    * `peril`: If this step describes passing through a perilous area, the number shown for that area, which indicates the number of event tests it will require. Every perilous area should be its own step.
    * `comment`: A description which will be shown while the journey is playing out.
* `/jedit remove`: Remove a step (based on the number shown on `/jedit show` or `/jedit steps`).
* `/jedit steps`: Shows just the steps.
* `/jedit clear`: Wipes the steps.

## Finding Journeys

* `/journey list`: See a list of available journeys. You will only see journeys available to you, that is, your own journeys and those that others have published. Narrow the list down with various search options:
    * `tag`: Only journeys that include a specific tag or tags (see above for the tags)
    * `mine: See only your journeys, or only those that aren't yours.
    * `published`: See only published or unpublished journeys.
    * `searchtext`: See only journeys with specific text in them somewhere.
* `/journey use`: Use a journey in your current game. This happens automatically with your current game when you originally create it, so you would use this to pull journeys from your other games, or journeys other people have published.
* `/journey remove`: Undoes the `use` above. If this leaves a journey not used in any game, it will be deleted.

## Starting A Journey

* Use `/report roles` to get an idea of who's good at what journey skill so you can choose roles.
* Everyone then does `/role` to choose their journey role. Set `absent` to specify a character is not present for this journey.
    * In the rare case when someone has multiple roles, setting `multiple` to True means you are toggling roles; choose a role you don't have to add it, or a role you do have to remove it.
* Use `/report journey` to see who is in what role and their relevant skills.
* `/journey start`: Start a journey. Options include:
    * `forced`: A forced march takes less time but brings a lot more fatigue.
    * `season`: Specify a season, which determines how far the Guide advances on a failed roll.
    * `type` = `border`, `wild`, or `dark`: Override the land type for the whole journey. Useful for things like Gilraen's patron ability.
    * `modifier`: A modifier on event feat rolls, as for Ponder Storied and Figured Maps (`+1`).
    * `eventtype`: Overrides the journey event type specified for the journey at creation. Note also, Narvi will automatically override `journeyevents` with `solojourney` if you are starting in Strider Mode, so if you have some reason to stick with `journeyevents`, setting it explicitly here will prevent that smart override.
* `/game config journey-weather`: Make Narvi automatically generate weather before each marching test.
* `/game config journey-tiring`: Instead of applying fatigue at the end of the journey as per the rules, Narvi will apply it immediately after each journey event.

## Walking The Journey

* Narvi will continually report what has happened, and what has to happen next. For instance, it might tell you that a scout (and it shows who that is) has to roll Explore to resolve a Mishap test, or that the Guide has to roll a Marching Test.
* `/journey next` will show this information again, along with the current progress and status.
* The loremaster can use `/weather` to randomly generate weather using the Hex Weather Flower.
* `/roll marching`: The Guide will roll this to determine how far the company advances before an event. Once the advance is done, Narvi will automatically roll and describe the journey event. As with any roll you can modify the command (e.g., to use hope).
* Note that in some cases when journey events on different tables have the same effect, Narvi may use the core rules name for the effect. For instance, Becalmed is the same effect as Despair.
* As the journey goes on, Narvi keeps tracking of how much fatigue you will be gaining, but it does not get applied until the end. You can see it in `/status` as Pending Fatigue. Narvi accounts for virtues like Cram, Endurance of the Ranger, and Twice-Baked Honey-Cakes automatically when accumulating pending fatigue.
* One exception: some of the events in the Northern Wastes Journey Events table are too situational for Narvi to implement the results of. Only rolls 1, 3, and G are automatically carried out.
* `/skill`: Roll to resolve an event test, but specify that it affects the journey so it will advance things. Narvi will implement all the consequences and advance to the next marching test. (Exception: some of the events will result in a Shadow gain, but in these cases, you will have to `/roll dread` to do the Shadow test, which will then add the resulting Shadow on a failure.)
* Note that for simple choices you can click a button to do the skill test, but you will need to use the command if you need any options like bonus dice or no item use.
* Note: in the case of Strider Mode, sometimes you get a Noteworthy Encounter instead of a skill test. In this case, you can still do any skill test you deem appropriate as above, and let the success or failure be determined by the roll, or you can `/skill affects=journeypass` or `/skill affects=journeyfail` to determine a passed or failed test yourself.
* If a player forgets the `journey` part of a roll, the loremaster can record the result as if they had included it with `/journey pass|fail`.
* This repeats until the journey is concluded, at which point Narvi will apply all pending fatigue and report the duration of the journey.
* Everyone should then `/roll arrival` to reduce fatigue based on their Travel roll and steed (if any). The loremaster can do this for everyone at once with `/company arrival`, but this deprives them of the chance to modify their rolls (e.g., by spending hope).
* Anyone can `/report fatigue` to see how tired everyone is.

## Managing Journeys In Progress

* `/journey next` shows the current status of the journey and what has to happen next.
* `/journey log` shows a log of everything that has happened in the journey. You can even do this after the journey has ended.
* `/journey undo`: Should someone do a roll that shouldn't have been done, and Narvi advanced the journey, use this to undo it. Immediately; there is only one level of undo (at this time).
* `/journey end`: The loremaster can jump directly to the end of the journey. The `delete` option also prompts Narvi to try to delete the journey, as `/jedit delete`, afterwards.
* `/journey abort`: As above, but Narvi doesn't do any of the journey end things like applying pending fatigue.
* `/journey reset`: Undoes everything in the journey back to the start and starts it over. Note: if a journey event had resulted in hope, shadow, or wounds, these are not automatically undone.
* Though Narvi handles all the fatigue from the journey, loremasters can give or take away extra fatigue to everyone in the company: `/company fatigue` (the amount can be negative). This affects actual fatigue, not pending fatigue.
* When the players get to take a rest, the loremaster can `/company short` for a short rest (e.g., lunchtime), `/company prolonged` for a prolonged rest (e.g., overnight by the side of the path), or `/company safe` for a prolonged rest in a safe place (e.g., overnight in the Easterly Inn before continuing on). If you have a hero with Elven Dreams, you can also use `/company shortsafe` for a short rest in a safe place, which would allow that hero to recover Fatigue during short rests.

# Combat

## Preparations For Combat

* Full details on how to set up adversaries and encounters are explained in [Adversaries and Encounters](https://bitbucket.org/HawthornThistleberry/narvi/src/master/Adversaries.md). This is just a brief summary.
* The loremaster might use `/aedit` to create and publish adversaries, if they are not already in the catalog.
* The loremaster can use `/encounter` to create encounters in advance or on the fly. An encounter can contain:
    - A title along with notes and possibly an image that will be displayed when it starts.
    - Adversaries copied from the catalog. Multiple adversaries of the same type can be added at once with automatic numbering and even the ability to vary them slightly.
    - Events, which are messages that appear at a particular time (e.g., to remind everyone when the guards arrive).
    - A modifier (advantage or complication for the player-heroes) the combat will begin with.
    - An environmental source of injury (e.g., fire, suffocation) that fires on a specific round or every round.
* Players set stances: `/stance`. Loremasters can also do this by specifying the character. This can be done again at the beginning of each new round.
* If a hero is not present for a fight, set the stance as `absent`.
* Use `/foe list` to show the players a sanitized list of adversaries so they know what targets to pick.
* For play-by-post games you might want to `/game config unphased-initiative` (see the main README for how this works).

## Playing Out Combat

* The loremaster determines if the battle starts with an ambush, allowing adversaries to go first. It's also possible for adversaries to be ambushed, losing Opening Volley and being at -1d during the first round.
* Loremaster: `/start` to begin the battle; can give a name, set a starting modifier, set `ambush` so adversaries go first, and can specify an encounter (or a copy of one) which will load in all its adversaries and other elements. Note that the encounter will be altered during the combat to reflect the state of the adversaries and events, so use a copy if you want to keep the original untouched. You can also specify a light level; if you have adversaries, they may be affected if they have appropriate fell abilities.
* For the first round, the loremaster might want to set engagement (this will mostly happen automatically after this) using the `engage` function of `/foe`.
* From here on, `/current` shows the current phase of the battle and who can act in it, including who already has. A brief version appears in `/game show`. The full round can be shown with `/report initiative`. Phases specify who acts when; they occur in this order (only showing when appropriate):
    + Opening Volley: only on the first round, when player-heroes (and sometimes adversaries!) can make ranged attacks.
    + Change Stances: heroes get to choose a new stance. Any environmental effect happens.
    + Forward, Open, Defensive, Skirmish, and Rearward Stance: player-heroes make their actions.
    + Adversary Actions: adversaries make their actions.
    + Then the next round starts on Change Stances again.
* Loremaster can record or remove advantages or complications with `/modifier`.
* Loremaster uses `/next` to advance to the next phase when all actions in the current phase are done. This may cause events in the encounter, if present, to show up (either publicly or privately depending on how they were made).
* When it's a player's turn, she might:
    + Make an attack: `/attack`
        - Specify the target from an encounter and Narvi will automatically use its parry, do damage, etc.
        - Narvi automatically accounts for stance, advantages, and complications, and calculates the TN based on the parry.
        - Can also use `escape` to do an attempt to escape roll, doing no damage.
        - If your weapon can be used both ways, specify `1h` or `2h` in the handedness.
        - Can specify `non-lethal` for a non-lethal attack (damage only, no piercing blow).
        - Can specify a weapon name (as it appears on your sheet) to use that weapon; if no weapon name is given, your best weapon is automatically used.
        - If a target was specified, damage is done automatically, with the results showing in the loremaster's private channel. Narvi also accounts for things like qualities, banes, virtues, fell abilities, etc. when applicable.
        - If the loremaster needs to roll a protection test, `/foe protection` knows what tests are needed and carries out the results.
        - If an attack scored any 6s, click on the buttons under the attack to spend them, or use the `/spend` command to see and spend them.
    + Use `/actions` to see a list of combat tasks available in her character's stance.
    + Do an action: `/rally`, `/prepare`, `/intimidate`, `/protect`, `/gainground`; in most cases the results are automatically handled by Narvi. `/intimidate` is also used to Riddle a Dull-witted troll.
    + Make a skill roll or do something else.
* When it's the adversary's turn, the loremaster might:
    + Make an attack: `/foe attack`, which will automatically do damage, set engagement, queue protection rolls, etc.
        - Narvi automatically accounts for stances, records engagement, uses shields only if the hero isn't using a 2h weapon, etc.
        - Damage happens automatically; players can use `/status` to set knockback and get a refund of half damage.
        - If a protection test is required, the player would `/roll protection`. Narvi knows what tests are due and will carry out the consequences including wounds, but you can also use `/wound` to set wounds and `/status` to set poisoned and other conditions.
	- Certain conditions can be set or cleared with `/status` such as Daunted, as well as adjusting things like Temporary Parry.
    + Use a Fell Ability: `/foe ability` which automatically executes hate spends and consequences on some abilities.
    + Take other actions: the loremaster might `/tor` as required. If this other action counts as an engagement, they should also use `/foe engage` to record it.
* At any time you might want to `/report damage` to see everyone's status, `/report initiative` to see the entire round, `/report adversaries` to see all adversaries and engagement, `/report stance` to see stances and engagement, or `/status` to see your own status.
* The loremaster can use `/encounter` to modify the encounter even while it's in play. For instance, `/encounter adversary` to add an adversary, `/encounter combine` to bring in a whole second set of encounter elements (when the second wave of villains arrives!), and `/encounter set` to change adversary stats on the fly (protip: set endurance 0 to take someone out of the fight).
* The loremaster can also use `/foe gain` and `/foe lose` to add or subtract to foe values (including endurance, hate, rounds of weariness, etc.) and can specify `all` to affect all adversaries at once. Use `/foe damage` and `/foe heal` as a shortcut for endurance losses and gains.
* When the battle ends, the loremaster will `/end`; this stops the combat and clears many things. If the combat includes an encounter, this can also delete it (useful if it was a copy, or if you're done with it).

![An Initiative Report](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/initiative.png)

# Eye Awareness

If you want Narvi to track Eye Awareness, you must first determine the base, or starting, Eye score (see TOR p170) based on your party composition, and the hunt threshold (see TOR p172) based on the region traversed. Set these for your game:

* `/eye base`: set the base Eye score (based on party composition) - if you don't supply the value, Narvi will try to calculate it, but note that it might get it wrong in some edge cases such as famous war gear that does not have any special qualities unlocked
* `/eye threshold`: set the threshold for the Hunt (based on how dangerous is the land they're in)

In addition, you have several configuration choices you can make:

* `/game config show-eye`: configure your game to show the Eye status; if not, Narvi will DM the loremaster
* `/game config roll-eye`: automatically increment Eye when an Eye is rolled out of combat
* `/game config magical-eye`: automatically increment Eye when `magical` is used on any roll

From here forward, the Eye score will automatically update itself when Eyes are rolled or `magical` results are requested, if so configured. You can also adjust the Eye score yourself:

* `/eye set`: set the current Eye status; if your amount starts with a + or minus, it instead adds to or subtracts from the current value.

The resulting display will appear wherever the command that caused the change, if `show-eye` is set; if not, it will be sent in private message to the loremaster. The same is true for any change which causes the score to meet or exceed the Hunt Threshold, including a notification that a Revelation Episode should happen. When this happens, the score is automatically reset to the base (this also happens when you run `/company fellowship` or `/company yule`, and can be done manually with `/eye reset`).

All of these commands will produce a display of the current Eye status, showing as a sort of 'progress bar' with brown representing the base, red any accrued Eye score, and white showing how much is left to the threshold. You can also see this at any time with the `/eye show` command.

# Rest and Recovery

What to do about all that fatigue, endurance loss, wounds, spent hope, and so on?

* Players can `/heal` to recover endurance, or just `/set endurance` directly.
* They can also `/gain`, `/lose`, or `/set` most everything else, e.g., fatigue, hope, etc.
* A player can treat a wound or poison on another player with `/treat` and Narvi automatically figures out the results. You can also `/treat self` to make the Healing roll for yourself. Remember, once it's been treated, it can't be again that day (cleared by `/company prolonged`).
* A player can record that their wound is treated: `/wound treated` to mark it as treated, `/wound failed` to mark a failed attempt, and they can also change the number of days left.
* The loremaster can use `/clear` to clear various kinds of damage from everyone at once: `knockback`, `conditions`, `endurance`, `fatigue`, `hope`, `wounds`, or `damage` (includes both wounds and endurance lost). Note however that other things like rest and fellowship phases will usually do those automatically.
* Or they can give endurance back to everyone at once with `/company endurance` (or take it away with a negative).
* Rests can be done with the `/company` command: `short` for a short (midday) rest, `prolonged` for a long (overnight) rest, and `safe` for an overnight rest in a safe place like an inn. This handles both endurance, and wound and poison recovery.
* During a rest, the company can also distribute the Fellowship Pool to recover hope. The loremaster would set the pool (if not already set) with `/pool set`, then can distribute it to people with `/pool distribute` (e.g., `/pool distribute Frodo=1 Samwise=2 Pippin=1`). Alternately the loremaster can `/pool distribute` to let Narvi decide who gets hope based on who needs it the most.

# Treasure Hoards

* Give everyone in the company the same amount of treasure: `/company treasure `.
* Roll multiple feat dice to figure out the chance of magical treasure: `/feat`.
* Roll for a magical treasure: `/table roll magicaltreasure`.
* Roll a blessing for a wondrous item: `/table roll blessings`.

# Rewards and Fellowship Phase

At the end of a session or adventure, the loremaster can:

* `/company ap`: Give everyone the same number of Adventure Points.
* `/company sp`: Give everyone the same number of Skill Points. You'll need to manually add the number from Wits when appropriate.
* `/company session`: Gives both AP and SP in the same amount to everyone, and then does an automatic `/pool distribute` and resets the pool, to wrap up a session when Narvi's smart pool distribution is good enough.
* `/company fellowship`: Clears all the things that are cleared by a fellowship phase such as wounds, fatigue, and songbook uses, as well as the specified amount of Shadow (defaults to none). Also handles the hope recovery during fellowship phases.
* `/company yule`: For a Yule fellowship phase, this does all of the above plus the additional hope gain for Yule phases.
