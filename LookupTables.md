![Narvi](https://bitbucket.org/HawthornThistleberry/narvi/raw/24835bdaaaa49bed9a5153abd7a3a8691524f041/images/banner.jpg)

a Discord bot for the *The One Ring* roleplaying game, written by HunterGreen

for support visit the TOR/AiME Discord at https://discord.me/theonering

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V44RKAI) to help pay for cloud-hosting

# Lookup Tables and Hex Flowers

The [main README](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md) directions describe how tables are used; these instructions are reproduced here for completeness. Below that will be a more detailed explanation of how tables are designed and built.

# Using Table Lookups

Narvi allows you to build custom lookup tables and hex flowers, and then have you and your players roll against them. Tables can then be published to a catalog which allows everyone else who uses Narvi to use (but not modify) them. Narvi knows all the tables in the published books. See https://bitbucket.org/HawthornThistleberry/narvi/src/master/LookupTables.md for instructions for designing and building tables.

Tables can be tagged with one or more tags that identify something about the category or type of table it is. For instance, all core rules tables have the tag `core`, all Strider Mode tables have the tag `strider`, and all TOR tables have the tag `tor`. (A table can have many tags.) Hex flowers don't use tags.

Tables can either have one or two rolls. A simple table with only one row might use a d8 and have eight rows numbered 1 through 8. Most tables are like this. However, there are also tables that use two separate rolls; for instance, Strider Mode lore tables each use a feat die roll, followed by a success die roll, and thus have 72 possible outcomes. When there are two rolls, you separate the rolls, or their modifiers, with a `/`.

When feat dice are used, E is used for Eye rolls (and will sometimes be shown as a 0), and G is used for Gandalf rolls (and will sometimes be shown as 11).

For using tables:

* `/table list`: Get a list of all available tables you can use. Optionally only show those you created (`mine`), that are published (`published`), that have one or more tags, and/or that include a given search term in the name, notes, or rows.
* `/table select`: Pick a table using its table name. All other commands will use this table (but `roll`, `lookup`, and `show` let you override that).
* `/table show`: Get a look at the table.
* `/table roll`: Roll on a table (by default, your selected one). If the table has more than one roll, you can specify separate modifiers for each roll by separating with a `/` (for instance, `+2/-1`). If any feat dice are involved, you can specify if they are favoured or ill-favoured, and also set `natural` false to set Narvi to allow modifiers to change rolls to or from Eye or Gandalf.
* `/table lookup`: Look up a result without rolling (in case you already rolled some other way). When there are multiple rolls, separate them with `/`.

Hex flowers are only rolled on within a game, since they include a 'memory' of where they were last, but otherwise work very much like tables, using the `/hexflower` command. Use `/hexflower current` to see the current value, and `/hexflower next` to roll and advance. Note that the `/weather` commands are simply a specific hex flower (named `weather`, naturally).

# Designing Tables

When you build a table, it is, by default, available to you, and any player in a game you are the loremaster of (though only you can modify it). You are also able to publish it to the catalog, making it available to all other Narvi users. You can also unpublish it. The catalog is intended to provide a means by which the common core tables from the rules are easily shared between us, but can also be used for your homebrew rules.

Tables are not associated with specific games; once you make one, it's available to everyone in every game you run.

## Elements of Table Design

A table includes these elements:

* ShortName: This is a short (one word) unique name that identifies the table. This is what people will be typing, and what they'll see when they roll, so make it brief but descriptive.
* Name: A longer name which will only show when someone does a `/table show`.
* Notes: This appears at the bottom of the table in a `/table show` and should be used to explain what purpose the table serves.
* Tags: See the Tags section.
* Published: A flag to identify if the table has been shared to the public catalog.
* Rolltype: What dice are rolled for this table. See the Rolls section.
* Rows: The various rows of the table. See the Rows section.

Start the process of creating a table with `/table create`. This will also select the table. Later on you can `/table select` to select that table. Then set the simpler values like this:

* `/table name `
* `/table notes`

## Tags

Narvi has a list of tags that you can (and should!) tack onto your tables to help people searching for tables find the right ones. It's vital to understand that a table can have multiple tags, and usually should. Here are the tags and their meanings (more may become available later):

* `tor`: This table is used for _The One Ring_.
* `aime`: This table is used for _Adventures in Middle-earth_.
* `lotrrp`: This table is used for _Lord of the Rings: Roleplaying_.
* `core`: This table comes from the core rules.
* `strider`: This table comes from the Strider Mode rules.
* `starter`: This table comes from the TOR starter set.
* `ruins`: This table comes from _Ruins of the Lost Realm_.
* `lifepaths`: This table comes from the Lifepaths Supplement.
* `shire`: This table comes from the Shire starter pack books.
* `homebrew`: This table is a homebrew.
* `encounter`: This table is used for random encounters.
* `chargen`: This table is used during character generation.
* `english`: This table is in the English language.
* `espanol`: This table is in the Spanish language.
* `journey`: This table is used for journeys.
* `council`: This table is used for councils.
* `combat`: This table is used in combat.
* `equipment`: This table is used for gear, treasure hoards, or other equipment.
* `nameless`: This table is used in the generation of Nameless Things.

For example, a table might have the tags `tor lifepaths chargen english` to indicate it's for TOR, it's from the Lifepaths book, it's used for character generation, and it's in English.

Setting these lets people find the table they want even if the catalog gets large. Maybe someone wants to find only the tables from the Spanish translation Strider Mode that are used for encounters; they might `/table list` and specify for the tag `strider encounter espanol` to narrow down on just the small number of tables they want.

If you have a desire for another tag, please ask HunterGreen, the author of Narvi, for it, either in the TOR/LOTR:RP Discord (https://discord.me/theonering) in the #narvi channel, or with the `/suggest` command. While we don't want the list of tags to get too huge, generally speaking, more tags is better. In fact, there's no reason Narvi's lookup table functionality has to be limited to TOR and LOTR:RP; you can enter tables for other games, too! (And tags will help ensure that even if you go crazy with this, your huge pile of tables won't clutter everyone else's screen, since they can just look for other tags.)

Set a table's tags with `/table tags`. If you want to change them later, repeat the whole command. So if you set `/table tags tor strider` and you realize you should also have tagged it as English, do `/table tags tor strider english`. The order doesn't really matter -- it's how they will show in a `/table show`, but it won't affect searching by tags.

## Rolls

Tables can have either one or two rolls.

### One Roll

Most tables are based on a single roll. For instance, an encounter table might use a percentile die (d%) and have rows for values from 1 to 100. This is the most common type by far.

In this case, you specify the roll type with `/table rolltype <type>`. The types can be `feat`, `success`, or any standard die roll (e.g., `d8`, `3d6`, `d%`).

### Two Rolls

In some tables, most notably as seen in Strider Mode, you find the table listed as a group of tables, where you roll one die to pick a 'subtable', then you roll another die to choose a row on that table. The Lore Table in Strider Mode is like this -- in fact, it's three separate tables like this, since each column represents a different table.

In Narvi's implementation, we don't build this as two rolls done sequentially, and twelve tables. We design it as a single table with the roll type `feat/success`. Thus, you have 72 rows; there's a 1/1 row which represents rolling a 1 on the feat die, then a 1 on the success die. For a Lore Action roll, that'd give the result Aid. Then there's a 6/3 row, which is Leave, and a G/6 row, which is Strengthen. This approach means you do a single command to get a result in a single step.

When you're setting up tables this way, you will use a `/` to separate the parts of roll types, rolls, and modifiers to rolls. For instance, you would set the roll type with `/table rolltype feat/success` or `/table rolltype d8/3d6`.

## Rows

The meat and potatoes of a table is its rows. Each row consists of two parts: range and text. (There is a third part, a code, but this is only used for internal Narvi purposes.)

### Range

A range is what rolls are reflected in this row. A range can be a single value, or a set of values separated by a dash (`-`). Ranges must be unique and not overlap, and when you're done, all possible rolls should be represented. For instance, a table with roll type `feat` might have ranges of E, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, and G; or, it might have fewer rows, for instance, E, 1-3, 4-6, 7-8, 9-10, G.

In a table with two rolls, each range is actually two ranges, one for each die. So on a `feat/success` table, you might have rows with ranges E/1, E/2, E/3, and so on for 72 separate rows. Or you might have E/1-6, 1/1, 1/2, 1/3-4, 1/5-6, 2-3/1, 2-3/2-4 and so on -- any arbitrary combination of ranges will work, so long as there's no overlaps and every possible roll is covered (Narvi will go to considerable effort to vet your table to confirm that this is the case).

### Text

Whatever the result of the roll is. This might be a word, a phrase, a whole paragraph. It's meant for human eyes -- the person who rolls, and/or the loremaster. This is what it was all about.

### Editing Rows

Rows are edited with the following commands:

* `/table add`: Create a row. You can do them in any order: Narvi will sort them automatically.
* `/table remove`: Remove a row you already created.
* `/table clear`: Remove all rows, if you decide it's a mess and you want to start over.
* `/table show`: Not only does this show the table, if you're the creator of the table, it also shows you the first problem Narvi found when validating the table (this also is displayed when you `add` a row). There may be other problems; it only shows the first one it finds.

Putting it all together, here are some examples:

* `/table add 01-08 Gelatinous Cube`
* `/table add G/2 Duty`
* `/table add 5 Enemies run afoul of danger.`
* `/table add 10 You face a test which is contrary to your nature or abilities.`
* `/table add 4-5 Wondrous Item: An enchanted item possessing two Blessings.`
* `/table add 8 Showing Some Coin: A wealthy merchant is passing through Bree and has bought a round of drinks for everyone in attendance this evening. Unfortunately, some less than savoury drinkers seem to have a keen eye on his money purse.`

## The Catalog

When your table is done, and you're sure it's something other people might like, `/table publish` will mark it as visible to everyone else in the world that uses Narvi, instantaneously. No one else can modify it, but everyone can look at it, and roll against it. Before you publish, please be sure:

* This is going to be of interest to other people, not only to your own group.
* The name, notes, and especially the tags, are accurate, descriptive, and meaningful.
* The table is complete and passes all validation tests.
* The table and its contents are not in bad taste or offensive.

Narvi's creator reserves the right to change, unpublish, or delete your table if it violates these rules.

You can change the table after it's published, and your effects will happen immediately. Even if someone else halfway across the world is currently using that table! So please do this only to be correcting problems.

If you have second thoughts about sharing a table, `/table publish` can also remove it from the catalog.

Note that there is no announcement (as of this writing) when tables are published or unpublished. They simply become available when people do a `/table list`. You might want to visit the TOR/LOTR:RP Discord (https://discord.me/theonering) and mention your new creation in the #narvi channel.

## Deleting Tables

Finally, if you no longer need a table, you can delete it with `/table delete`. This is irrevocable. If it's published, and you delete it, everyone else who was using it can't anymore, because it's gone for everyone forever. So don't do this lightly for published tables.

## Table Groups

Once in a while you might want to make a roll on a series of groups all at once. You could use macros to do that (see the [main README](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md) to learn about macros) but that only works for you. What if you're publishing a group of tables that are meant to go together? Well, that's what a table group is for, and you can define one so that everyone else can use it:

* `/table creategroup <name> <tables>`: Give the group a name, and then specify the shortnames for the tables to be rolled, one after the other separated by spaces. All the tables you specify must be published. You can repeat the command to update the group, replacing it with a new set of tables.
* `/table deletegroup <name>`: Delete a group you've created.
* `/table list`: Will show groups at the bottom of the list, if they match your `mine` and `searchtext` search parameters.
* `/table show <name>`: If you specify the name for a group, shows a summary of a table group you've created.
* `/table roll <name>`: If you specify the name for a group, rolls on all of the tables in the group, using any parameters you specify (like modifiers). You'll see all the results one after another.

# Designing Hex Flowers

If you're not sure what a hex flower is, it's basically a very clever little invention by Goblin's Henchmen which combines some functions of a lookup table with a few other virtues, notably, that they have a memory, and that they very simply code for higher and lower probability outcomes by their shape. For the full details, see here: [Hex Flower Cookbook](https://preview.drivethrurpg.com/en/product/295083/hex-flower-cookbook-an-overview-and-some-thoughts-on-hex-flower-game-engines-by-goblin-s-henchman).

Hex flowers can be played with a printout, a pawn, and a pair of dice, but Narvi digitizes this experience and lets us share hex flowers with one another. Because there needs to be a place for Narvi to record where the pawn is, that is, the table's memory, a hex flower can only be used within a game, but you can create, view, and publish them without one.

## Creating a Hex Flower

Hex flowers are simpler than lookup tables: they consist only of a shortname (as above), a description, and their contents. No tags, no roll types, none of that. Use `/hexflower create` to make a hex flower, providing a name and description. Then it's time to set the hexes.

The hexes are numbered as follows. (Sorry if it starting with 0 is confusing -- that's how us coders think.)

```
       __
    __/7 \__
 __/3 \__/12\__
/0 \__/8 \__/16\
\__/4 \__/13\__/
/1 \__/9 \__/17\
\__/5 \__/14\__/
/2 \__/10\__/18\
\__/6 \__/15\__/
   \__/11\__/
      \__/
```

For each of the 19 hexes, you will use the `/hexflower hex` command to specify a label, which is the value that is displayed for that hex. For instance, in the weather hex flower, labels include things like "Sunny", "Partly Cloudy", and "Hazard".

You can also (and are strongly encouraged to) provide an image for each hex. This is done similar to other images: upload the image to Discord, then right-click (or long-press) it and Copy Link or Copy Media Link, and paste that in. Ideally your image should have a transparent background and be about 128 to 256 pixels on a side and squarish. The result will look something like this:

![Weather Hex Flower](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/weatherhexflower.png)

## The Hex Flower Catalog

As with tables:

* `/hexflower list` to search for hex flowers in the catalog
* `/hexflower publish` to publish your hex flower (if it's complete) for everyone else to use
* `/hexflower select` to select a hex flower to roll on or edit
* `/hexflower show` will display the hex flower
* `/hexflower delete` to delete your selected hex flower

## Using Your Hex Flower

Once you have selected a hex flower, and you are in a game, you can use `/hexflower next` to roll to move to a new hex and display it, or `/hexflower current` to show where you already are. `/hexflower set` lets you directly move the pawn to a specific hex. By default, the pawn starts in the center (hex 9).

