![Narvi](https://bitbucket.org/HawthornThistleberry/narvi/raw/24835bdaaaa49bed9a5153abd7a3a8691524f041/images/banner.jpg)

a Discord bot for the *The One Ring* roleplaying game, written by HunterGreen

for support visit the TOR/AiME Discord at https://discord.me/theonering

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V44RKAI) to help pay for cloud-hosting

# Installation

Add Narvi to your Discord server by going here:

* https://discordapp.com/oauth2/authorize?&client_id=688546328011341876&scope=bot&permissions=8

as long as you are the admin of your server, of course.

# Using Narvi For Other Games

Narvi is very definitely meant for _The One Ring_, but over the years I have slipped a few other kinds of dice rolling into it, when those didn't take a lot of coding and didn't conflict with TOR functionality. I'm not trying to make Narvi a substitute for [Dice Maiden](https://top.gg/bot/377701707943116800), but when I happened to be involved in another game with specific kinds of dice rolling that Dice Maiden wasn't that good at, or clumsy at, or couldn't do at all, and I could some good work without burdening Narvi, I did. So here's a bit about these other games Narvi can help you out with.

Narvi's support for these other games consists of two parts: dice rolling, and scribbles. Let's start by talking about scribbles, as the dice rolling can then use them.

# Scribbles

When you set a game's system to one of the games listed here (e.g., `/game system coyoteandcrow`), you can still create characters and set their images with the `/character create` and `/character image` commands respectively, but all of the TOR character sheet that normally comes with a character doesn't apply. But Narvi supports a thing called Scribbles that can let you make up a custom character sheet on the fly and then use it in your commands. In Narvi's primary use supporting TOR, these are used to represent little notes you write in the margins of your character sheet -- hence, scribbles. But when you're playing another game, scribbles can be your whole character sheet. Or only the bits you want.

## Setting Up A Character Sheet

For instance, imagine you're playing _The Expanse_. You might set up some scribbles using commands like `/scribble set Accuracy 2`, and then, when you've finished, you could look at your `/sheet`:

```
Accuracy      2
Communication 2
Constitution  2
Dexterity     1
FreeFall      3
Crafting      3
Fighting      2
Brawling      4
Intelligence  4
Technology    6
Security      4
Perception    1
Strength      0
Willpower     0
```

You could put in only a few values, or fill up the whole character sheet (though in a single column, it'd be pretty long). In `/expanse`, `/coyote`, and `/cortex`, you will even be able to use these in your commands.

Scribbles don't have to be numeric; you can put anything there (but if it's not numeric, you can't add and subtract values).

## More Scribbles Commands

* `/scribble set <scribble>`: Look up the value of a scribble.
* `/scribble set <scribble> <value>`: Set a scribble's value, creating it if necessary.
* `/scribble gain <scribble> <value>`: Add to a scribble's numerical value.
* `/scribble lose <scribble> <value>`: Add to a scribble's numerical value.
* `/scribble scribbles`: View all your scribbles. They will also be appended to the bottom of other sheet displays.
* `/scribble delete <scribble>`: Remove (erase?) a scribble.

# Dice Rolling

## Generic Dice Rolling

Since the days of red-box D&D there has been a standard format for dice rolling: 2d6 means two six-sided dice rolled and totaled, and you can use this format with Narvi: `/dice 2d6`. You can leave off the first number and it will assume a single die (e.g., `/dice d8`), and you can include additions and subtractions (e.g., `/dice 2d12+5`). You can even use `d%` as a shorthand for `d100`. And you can add multiple dice rolls and modifiers together (e.g., `/dice  3d6 + 2d4 - d8 + 6`).

![Polyhedra roll](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/polyhedra.png)

Combine this with the macros function (see the main README) and you can make your own commands to do whatever game you use, or even your own character skills.

But you can't do things like dice pools, exploding dice, open-ended dice, or dice pools, because I didn't want this to get too complicated. You can always ask [Dice Maiden](https://top.gg/bot/377701707943116800) for help.

## Interactive Interface

As of July 2024, Narvi now features the beginnings of an interactive interface which you can start with the very simple `/go` command. When you are in a game using any of the following systems, Narvi will either simply do the roll for that game, or present you with an interactive panel letting you do the various functions of these dice rolls without commands. (In any case, some options may not be available in these simplified interfaces, and you may need to use the commands for those.)

## The Expanse

* `/game system expanse`

Dice in _The Expanse_ are basically just 3d6, but where one of them is a special "drama die", and Narvi knows how to warn you about stunt dice as well. To roll:

* `/expanse`

You will have optional parameters to provide a target, a comment, and most importantly, a bonus (typically your relevant skill). The bonus can also use one of your Scribbles if you put it inside curly or square brackets. For instance, if you created a Scribble named Perception, you could type `{Perception}` and it'll use that as the bonus. (Scribbles are smart enough to find a partial match, so `{Per}` is probably enough, unless you also had, say, Persuasion.

![Expanse roll](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/expanse.png)

You can also use the Narvi `/counter` commands to track the Churn, and Narvi will specifically watch for a counter named Churn and warn you when it hits 10, 20, or 30 (and it resets at that point). It also will warn you when a roll could result in a Churn increase (a 6 on the Drama Die). For instance, `/counter set churn 0` to start at 0 (or `/counter set churn 0 true` to make it a secret value that is not shown to your players), then `/counter set churn +1` to increase it each time a relevant event happens.

## Coyote & Crow

* `/game system coyoteandcrow`

Coyote & Crow uses a dice pool system with a multi-step process in which you roll, then can change some of your rolls, then there is a chance for critical rolls to result. Thus, rolling in this system can be three separate commands. See the full rules for the explanation, but in brief, the process is:

* Determine your pool, then roll that many white dice.
* Optionally, spend legendary ranks and/or Mind (focus) to change some of the dice.
* When 12s have appeared, roll additional critical (black) dice, which can in turn result in even more critical dice if they come up 12.
* Count successes based on a success number (SN).

Here's how you'd do this in Narvi:

**1. Do the initial roll**: `/coyote roll`

* You have to specify the number of dice to roll.
* The number of dice can also use one of your Scribbles if you put it inside curly or square brackets. For instance, if you created a Scribble named Will, you could type `{Will}` and it'll use that as the number of dice. (Scribbles are smart enough to find a partial match, so `{W}` is probably enough, unless you also had, say, Wisdom.)
* You can also specify the Success Number (SN) but it defaults to 8.
* In some rare cases such as martial arts skills you might have a number of critical dice in the first roll, so you can specify those.
* `sort` will cause Narvi to sort your dice. This may help if you plan to spend legendary ranks or Focus on them.
* Use `finish` to jump right to the final result if you know you aren't planning to, or able to, spend legendary ranks or Focus.
* Include a comment to make that show in the roll; this may help your gamemaster know what you're rolling for.

**2. Change your dice rolls**: `/coyote change`

* The game allows you to do this in two phases, one using legendary ranks and one spending Mind on Focus, but as these have identical results, Narvi handles them both with the same command.
* You can do this command as many times as you like.
* You must identify which roll to change, and what to change it to.
* When specifying the new value you can just type `up` (your most common change) or `down` (not sure why you would, but Narvi will let you), or just put in the new value.
* Narvi doesn't keep track of how many changes you're allowed to make, that's on you!
* If Narvi can't find a roll matching what you specified, it will complain.
* If a change moves a value below 1 it stops at 1, or above 12, it stays at 12.
* After changes, Narvi will show you where you stand in terms of results.
* You can provide a new comment (if you don't, a previous one will hold) and again specify `finish` to note that this is the end.

**3. Finalize the roll by accounting for criticals**: `/coyote finish`.

* You get one more chance to enter or change the comment.
* Every white die that is a 12 adds one black die to the roll.
* Then every black 12 also adds another black die, which can also be a 12 adding yet another black die.
* Final results are calculated. White dice add 1 success if at or over the SN, and subtract 1 success on a 1. Black dice add 1 success below the SN and 2 successes at or over it.
* The final outcome is either critical failure (for <0 successes), failure (for 0 successes), or success (for >0 successes).

**Example of Use**

Aya is rolling on Science with a 3 INT and 1 skill rank. `/coyote roll 4`

![Initial roll](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/coyote-start.png)

She decides to spend one legendary rank to change that 11 into a 12: `/coyote change 11 up`

![Change](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/coyote-change.png)

Then she finalizes the roll, so that 12 will become a critical die: `/coyote finish`

![Finish](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/coyote-finish.png)

Note that as of this writing Narvi does not try to handle the unusual case of a SN over 12. I will add that eventually if people seem to be using this.

## GURPS

* `/game system gurps`

GURPS is just 3d6, but my group was playing it and mistyping a lot so I made `/gurps` work. You can even `/gurps -2` if you have a modifier.

## Cortex (e.g., Serenity)

* `/game system serenity`

In the Cortex system, as used in the game _Serenity_, you generally roll a dice pool with two (sometimes three or more, but rarely) dice sizes, so there is a very simple syntax to do these with minimal typing: `/cortex 10 8` for a Cortex roll with a d10 and a d8. You can specify up to three values (usually you'd be doing two, but shifts can push a large die into two dice). The dice sizes can also use one of your Scribbles if you put it inside curly or square brackets. For instance, if you created a Scribble named Pistol, you could type `{Pistol}` and it'll use that as the number of dice. (Scribbles are smart enough to find a partial match, so `{Pi}` is probably enough, unless you also had, say, Pilot.)

![Cortex roll](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/cortex.png)

In addition, the Cortex system makes extensive (some might say excessive) use of what it calls complex actions, where you have to roll so many times until you reach a total number of successes. Narvi makes it easy to do all the rolls you need in one command, though it doesn't handle situations where the dice size may change during the course of the roll -- you can always ignore all results after a botch and do a second command, though. Here's how it is:

* `target`: Keep rolling those dice until the total of all the rolls equals or exceeds the target. Narvi shows all the rolls, and counts how many it took. This might be used in a situation like this: you have to fix your ship's engine, each roll takes 10 minutes, and you just need to know how long it takes (and whether any botches happened along the way) so's you know how much docking fees the rutting prods will be collecting.
* `multiple`: Roll the specified dice that many times and report the total. This might be used when you have a fixed amount of time and want to know how much you get done; for instance, you only have an hour until the purple-bellies figure out your cry-baby; how much salvage can you scavenge?
* `target` and `multiple`: Combining both of these, you roll either until you hit the number of rolls, or you reach the target, whichever comes first. You have an hour until the reactor goes critical (should've done got them catalyzers when you had the chance, _dong ma?_), and each roll takes ten minutes; can you reach the target number of successes in six rolls, or is it time to look for a Shepherd?

There is also a mode where multiple characters can contribute to a complex action, using the `group` option. It works like this:

* Each contributing character does their roll with `group` set true to add their contribution to the group.
* If someone makes a mistake, they can repeat this to replace their previous entry.
* Once all contributors have done so, the gamemaster types `/cortex group target=<target>` without specifying any dice, and Narvi will complete the roll with cooperating characters, determining how many 'rounds' it takes. (Naturally depending on the action, rounds may mean different things.)

## Raven

* `/game system raven`

The Raven RPG's dice are either ravens or cats, and cats eat ravens. The result just depends on what's left over. Using the `/raven` command, specify the number of dice to roll and an optional comment, and off you go.

![Raven roll](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/raven.png)

## Others?

As noted, I don't mean to replace Dice Maiden, but if you have a game with dice that Dice Maiden can't do (or can't do well) that isn't in the above list, `/suggest` it and let me know how it works, or talk to me in the #other-tabletop-rpgs or #narvi channel in the [TOR/LOTR:RP Discord](https://discord.me/theonering).

