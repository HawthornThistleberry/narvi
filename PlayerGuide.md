![Narvi](https://bitbucket.org/HawthornThistleberry/narvi/raw/24835bdaaaa49bed9a5153abd7a3a8691524f041/images/banner.jpg)

a Discord bot for the *The One Ring* roleplaying game, written by HunterGreen

for support visit the TOR/LOTR:RP Discord at https://discord.me/theonering

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V44RKAI) to help pay for cloud-hosting

Narvi's documentation includes a lengthy [README](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md) and a briefer, more focused [How To](https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md), but both of them are full to the gills of stuff only the loremaster needs to know, like how to create games and journeys. This shorter document contains **only the instructions a player needs**. You'll get a lot of benefit from reading through it.

# Configuration #

Before we dig in, here's something you almost certainly want to do if you are in more than one game (and it does no harm if you're not):

* `/autoswitch on`: Automatically switch to this server's game when you do a command in this server.

Autoswitch can only tell what game to switch to if there is only one game in this server you are in.

# Interactive Interface #

Narvi features an interactive interface which you can start with the very simple `/go` command. How it works: Narvi will figure out what's currently going on for you, and present a panel of information with buttons and menu options relevant to what you're most likely to be doing just then, which only you can see (and which you can dismiss when you're done with it). This won't be able to support every possible option, of course, but it will cover probably ninety percent of the things you might be about to do. This means you might not ever have to learn any of the commands that Discord uses to talk to Narvi, except for `/go` and a few others you'll only do once in a while, such as during character setup.

Panels currently supported (whichever one appears first on this list is the one you'll get):

* if you're not in any game, a panel to do basic dice rolls
* if you're in a Coyote & Crow, Expanse, Cortex, or GURPS game, a panel to roll those dice (or just roll them)
* if you have a pending protection or shadow test, a panel to roll those
* if stance change phase, a panel to change stance
* if opening volley phase or a phase you act on, a panel to choose an action (attacks, tasks, seeking advantage) and a target, and roll for the action
* if adversary phase, a panel to record damage, take knockback, record conditions, and record wounds
* if a council is active, a panel to make rolls affecting the council
* if a skill endeavor is active, a panel to make rolls affecting the endeavor
* if anyone is wounded and needs treatment and you have pips in healing, a panel to roll to treat someone
* if none of the above apply, a panel that lets you review your character, gain and lose hope/shadow, and do skill rolls

![dice panel](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/go-roll.png)
![player panel](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/go-player.png)
![combat panel](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/go-attack.png)

# Character Sheet #

Narvi uses a Google Sheet for character sheets, and can read and write them to automate most of the paperwork of gameplay. To create a character sheet, you need to do a few steps: use the template, save your own copy, _change its permissions_, fill it out, and finally, tell Narvi where it is. **These are the steps players tend to get stuck on the most**, so read this section step by step and you'll be fine.

## Get A Blank Sheet ##

Go here to see the latest version of the character sheet template: https://docs.google.com/spreadsheets/d/1ORyskKJvepDqS5EK0mzlO-q1qeaLno6tyBW7_MPTuNM/template/preview

* In the upper right click **Use Template**. This makes a new, blank sheet in your own Google Drive. 
* In the upper left where it says "TOR2e Character Sheet", click there and pick a new filename (the character name is often a good choice).
* When you press Enter, the document is saved. You have your own blank sheet!

## Set Permissions ##

Unfortunately Google Sheets doesn't make it easy for me, or you, to tell it that Narvi is allowed to read and write the sheet. Fortunately it's something you only have to do once (per character sheet). Follow these steps carefully:

* Click **Share** in the upper right.
* Under the General Access section, change to "Anyone with this link". This lets you, your loremaster, Narvi, and the whole world, _see_ your character sheet, but not change it.
* Copy this ugly and cumbersome address: `tor2e-character-sheets-api@tor2e-character-sheets-api.iam.gserviceaccount.com`
* Paste it into the Add people or groups box.
* Click **Send** (no other changes are required). Now Narvi can also write to your character sheet.

When you're done, your permissions should look like this:

![permissions](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/google-sheets-permissions.png)

## Fill Out The Sheet ##

The Google Sheets sheet has a tab of instructions, and a lot of automation to try to make it quick and easy to make a character. If you are reasonably familiar with the rules, you can get a new character built in just a few minutes, plus whatever time it takes you to make all the decisions. You can fill things out in almost any order, but the following is a pretty sensible series of steps to take:

* Read the Instructions tab. Or don't; we will cover the same material here, mostly.
* Click on the Character tab.
* Enter a name. (Or save this for later. For me, names usually come last!)
* Choose a Heroic Culture. Narvi will automatically fill in everything that comes from that (it may take a few seconds).
* Choose your Strength, Heart, and Wits, and fill these into the red-bordered boxes. More values will populate.
* Choose one of the two highlighted skills available to mark favored by checking the box left of the skill name.
* Check boxes in the appropriate Combat Proficiencies for whichever ones you chose.
* Use the pull-down menu to record your two choices for Distinctive Features.
* Choose a Calling. Again, Narvi fills in more of the sheet for you.
* Mark two more skills (based on those available for your calling) as favored.
* If you are using the Character Lifepaths supplement and choose to roll on its table, select the option you rolled in Lifepath Event. Leave blank if you're not doing this.
* Spend your Previous Experience per the rules on p46 of the book. To update a skill, just check the relevant boxes.
* Click on the War Gear tab.
* To add a weapon or piece of armour to your character sheet, just check the box in the + column. It takes a few moments.
* Fill in 1 under both Valour and Wisdom (near the center of the sheet).
* For each Useful Item you get, add it under Travelling Gear, along with the associated skill.
* If you start with a horse, put a description or name in the box under Steed, then record its Vigour.
* Choose your starting Reward and Virtue (see p51 of the core rules -- note these are the only options available at character creation!).
* Record these in the boxes under Rewards and Virtues. Spelling counts, so Narvi can recognize them. You can add to the end, though; for instance, "Prowess - Strength" works, as long as it starts with "Prowess".
* When entering a Reward, you can (and should) put it in the Rewards box but also put it on the line under Famous Weapons and Armour; this will also make the character sheet update the `bonus`, `enc`, `dmg`, `inj`, or `edge` columns appropriately. For some Virtues, you'll have to make the change yourself, by just _typing over the existing value_:
    - Confidence: handled automatically by the sheet.
    - Dour-handed: no need to record anything, Narvi will know what to do (as long as you spell it right, including the hyphen).
    - Hardiness: handled automatically by the sheet.
    - Mastery: check the checkboxes left of skill names for two more skills.
    - Nimbleness: increase your Parry (under Wits) by 1.
    - Prowess: decrease one of your TNs by 1.
* You may need to check if your current Endurance is equal to your Maximum, and likewise for Hope.
* Fill in, if desired, the Age box, and record any other items you carry (e.g., an instrument) in the Other Items box.
* Click on the Notes tab. Record your character backstory, personality, and other notes. You can paste an image into place too.
* Record your company's patron and safe havens, and when you have chosen it, your fellowship focus. You can also record a summary of your adventures as they happen in the Tale of Years.

## Link Narvi To The Sheet ##

By now, your loremaster has hopefully created a game and added you to it. Now you need to tell Narvi you have a character, and tell it where to find the sheet.

* Copy the web address (URL) for your character sheet. It will look similar to this: https://docs.google.com/spreadsheets/d/1ijCEj1WzobC1yq5lGu38zCEMNoUKQo209qnjkHCQzz0/edit#gid=1075136867
* Go back to Discord and to the server for your game. Your loremaster might have set up a #bot-commands channel or a private channel for you; if so, go there, so the rest of the server won't be spammed by your commands.
* You might have to select the game: `/game select`. But if you have `/autoswitch on` this is probably not necessary.
* Using the address you copied earlier, paste it into this command: `/character create`
* If you get an error message here, heed it. You probably missed a step on the permissions section above.
* Narvi will take a moment to fetch your character sheet. Confirm it with `/sheet compact`.

## Character Portraits ##

You can upload an image so that Narvi will display it on your rolls and other commands.

* Paste or upload your image to Discord anywhere.
* Right-click or long-press it and choose Copy Link or Copy Media Link to copy the address.
* Back in Discord, with that game and character selected, paste into this command: `/character image`

## Character Sheet Updates ##

* When you make changes to your sheet outside Narvi, for instance when you get new equipment or train up your skills during a fellowship phase, _Narvi does not automatically know about it_, due to limitations in Google Sheets, so type `/character refresh` to make Narvi update.
* Things you might typically update on the sheet include new gear (including recording the qualities, banes, blessings, etc. for enchanted gear), and updates during fellowship phases (to skills, combat proficiencies, virtues, rewards, useful items, skill and adventure points, and treasure).
* Your loremaster can use Narvi to distribute adventure points, skill points, and treasure, which Narvi will update automatically. Later, when you spend AP and SP, you will change the values in Spent and Available yourself as you're spending them.
* You do not need to notify Narvi of anything you change on the Notes tab; Narvi only looks at the Character tab.
* You can also record information using Narvi about anything else you want, which Narvi calls "scribbles" (to refer to things you scribble in the margins of your character sheet), using `/scribbles set`.
* Do not make any other changes to the Character tab, or you might break Narvi's ability to find stuff. This includes adding stuff into the empty spaces around the sheet.

# Seeing Your Character Info #

While the Google Sheet is your character sheet in all ways (and updates automatically as Narvi makes updates to it), there are also some things you can do inside Narvi to see your character's current status and get useful information:

* `/sheet`and `/sheet compact` show a summary of the key parts of your character sheet.
* `/sheet skills`, `/sheet virtues`, `/sheet rewards`, `/sheet usefulitems`, `/sheet weapons` show specific parts of your sheet.
* `/status` shows the things that change often like hope, endurance, shadow, and wounds.
* `/game show` shows a summary of the current game, including what's going on in any council, combat, skill endeavour, or journey underway.
* `/council show`, `/endeavour show`, and `/journey show` show a summary of the current council, skill endeavour, or journey.
* `/current` shows the current phase of any active combat.
* `/report` offers a lot of useful reports that summarize information across the whole company. Some reports you might find useful as a player:
    - `/report <skill>`: Shows everyone's skill; great for figuring out "who should be the one to do this?"
    - `/report roles`: Like the above but shows all the skills used in journeys, helpful for picking roles.
    - `/report journey`: Shows who is in which role and their relevant skills.
    - `/report fatigue`: Shows everyone's fatigue and weariness status.
    - `/report stance`: Shows who is in what stance as well as initiative and knockback in combat.
    - `/report damage`: Shows everyone's wound and endurance status, as well as who is weary.
    - `/report hope`: Shows the fellowship pool and everyone's hope, helpful when choosing how to distribute the pool at end of session.
    - `/report shadow`: Shows everyone's shadow and shadow scars.
    - `/report weapons`: Shows everyone's weapons and their stats.

# Rolling Dice #

Now that you've done all this work, here's where it pays off. When it's time to roll dice and take actions, Narvi can now automatically think about a lot of rules for you. For instance, it knows your skills and your stance, it knows your TNs, it can apply most of your virtues and rewards, it takes into account your useful items and blessings, it knows about combat advantages and complications, it keeps track of how many foes are engaging you, and lots, lots more. So most of the time to roll something, you just need one very short and simple command:

* `/roll`: can do any kind of test, including skills
* `/skill`: a quicker way to do skill tests
* `/tor`: Just roll a feat die and that many success dice without doing any kind of lookups.
* `/feat` and `/success`: Just roll dice of the specified type.

Some examples of what you can roll:

* `/skill insight`: Roll on a skill.
* `/roll axes`: Roll on a combat proficiency (you would rarely use this though...).
* `/roll protection`: Make a protection roll.
* `/roll sorcery`: Make a sorcery shadow test.
* `/roll marching`: Make a marching test.
* `/roll arrival`: Roll to recover fatigue at the end of a journey.

In many cases that's all you need, but you have a lot of options when making rolls, and Narvi supports them. Here are the most important ones for a player:

* `hope`: Spend one hope to get one bonus die added to whatever else is applicable.
* `inspired`: As above, but you get two dice. Use if you have a relevant Distinctive Feature.
* `magical`: Spend one hope to get an automatic success and a magical result, if you have a means to do this.
* `favoured`: Force the roll to be favored.
* `illfavoured`: Force the roll to be ill-favored.
* `unfavoured`: Even if a skill roll would normally be favored, don't roll it that way.
* `target`: Override the default target number (or specify one, if you're just giving a number of dice, not a skill or test).
* `weary`: Force the roll to be made as if weary or not weary. (Narvi normally does this automatically.)
* `noitem`: Make Narvi ignore any Useful Item or Marvellous Artefact you have that might otherwise apply. Or if the game defaults to noitem, setting this false will make Narvi use the item.
* `bonus`: Add or remove additional bonus dice.
* `modifier`: Add or subtract something from the total.

You can also add a comment. This is very helpful to your loremaster, especially when a bunch of people are rolling things all at once.

# Hope And Other Adjustments #

Most of the time you use hope will be using the `hope`, `inspired`, and `magical` options above, but there are a few cases where you will use hope (or the fellowship pool) at other times -- for instance, spending hope to support a company member, or using a patron ability.

* `/lose hope`: Reduces available hope by the amount you specify (defaults to 1). You can do the same with `pool`, `endurance`, `shadow`, etc.
* `/gain hope`: Increases the same things the same way.
* `/set hope`: Set your hope to a specific amount, regardless of where it is now.
* `/pool`: See the current state of the fellowship pool. Also shows on `/report hope`.
* `/status`: Don't forget about this quick view of these commonly changing values.

# Combat #

During combat, Narvi (and probably your loremaster) will call out what phase of combat is active and who has an action in it, so keep aware of when yours is coming up so you'll know what you want to do. You might do these things during a fight:

* `/stance`: Review or set your stance. You can set this before combat begins, and again at the top of each round. Use `/report stance` to see everyone's stance.
* `/report adversaries`: Shows the adversaries in the fight so you know the shortname to use when attacking.
* `/attack`: Do an attack. Specify the adversary (using the shortname from above). All the options noted for rolls above apply, in addition to a few special ones used in combat:
    - a weapon name: By default Narvi uses your best weapon (based on proficiency, with ties broken by damage), but you can override that by specifying a name, _exactly as it shows on your character sheet_, but with spaces removed (e.g., `/attack 2 greatbow`) You can also type a shorter part of the name, as long as it's enough to uniquely identify it. You can also use `unarmed` for a punch, or `stone` if you are Sure At The Mark.
    - `handedness`: If your weapon can be used both ways, specify which. Narvi will use this both to reflect the right stats, and to allow or prevent you from benefitting from a shield later when foes attack you.
    - `attacktype=escape`: Attempt to escape combat instead of to do damage.
    - `attacktype=nonlethal`: Do a non-lethal attack (endurance damage only, no piercing blows).
* `/spend`: When you get Tengwar on an attack roll, you can use this to choose how to spend them. You can also click on the buttons under the attack roll. The options are `heavy` (axe) for extra damage, `pierce` (target) to increase the chance of a piercing blow, `fend` (fencer) to increase your parry, and `shield` (shield) for a shield thrust.
* `/roll protection`: When a foe has a chance to wound you with a piercing blow, Narvi will remember it and automatically carry out the test when you roll. You may also sometimes have to `/roll sorcery` or `/roll dread` based on fell abilities.
* `/actions`: See a list of special actions available in your stance and circumstances.
* `/rally`, `/prepare`, `/intimidate`, `/protect`, `/gainground`: Carry out a combat action. You can put roll options like `hope` on these.
* `/next`: Usually for most actions Narvi automatically keeps track of whether you have used your action, but sometimes you will only do a skill roll that takes your major action, or do nothing, etc. In these cases, you can use `/next` to tell Narvi that you've done all you're going to do, so that Narvi can tell the Loremaster when the phase is done.
* `/damage`: Take some endurance damage (Narvi generally does this automatically). Optionally you can add `knockback` to take knockback, which will halve the damage, but you cannot act next round. You can also use `/status` to set knockback and get a refund of that half damage. If you accidentally do too much, you can `/heal`.
* `/wound`: Record a wound or update an existing record. The roll, which is rolled by Narvi if you leave it out, determines the recovery time (see TOR101). Status can indicate you're unwounded, or your wound is untreated, treated, or failed (attempted a treatment but it failed, can't try again until tomorrow), or that you're dying (two wounds).
* `/treat`: Do a Healing roll to treat someone (or yourself with `self`) and automatically apply the results.
* `/status`: In addition to seeing your status this can be used to set certain temporary conditions like daunted, temporary parry, or number of engaged enemies.

Note that when doing an attack roll, Narvi automatically accounts for a lot of stuff: brawling, stances, engagement, complications, advantages, relevant virtues, weapon handedness, weapon stats, and more. An `/explain` command can help figure out how it came to the conclusion it did.

# Councils and Skill Endeavours #

Narvi handles most of the work of councils and skill endeavours automatically. All you have to do is, whenever you are doing a `/roll` that is intended to count towards a council (typically a social skill like Persuade), specify that it `affects` a council; and likewise, when your roll is intended to affect an endeavour, specify that for `affects`. As with combat, Narvi automatically accounts for things like attitude so you don't need to include bonus dice. For instance, if you're in a council and you're trying to persuade Lobelia to give back some spoons, you might:

* `/roll persuade affects=council`: To try to persuade Lobelia, but
* `/roll awareness`: To notice Drogo sneaking up behind Lobelia, which might be a roll done _during_ a council but which doesn't affect the council itself.
* `/council show`: To see the status of the council. Also shows in brief form in `/game show`.

# Journeys #

Journeys are very similar to councils in that you just do the rolls you would have done, but include `journey` on the roll. But journeys also have journey roles. Throughout a journey, Narvi will tell you exactly who should be rolling and what to roll, including what the command should be, so the important thing is just to watch what it's telling you.

* `/report journey`: See who is in what role.
* `/report roles`: See who is good at what, to decide who should be in what role.
* `/role`: Set your role. Narvi will not enforce rules like "only one guide", but your loremaster probably will.
* In the rare case when someone has multiple roles, setting `multiple` to True means you are toggling roles; choose a role you don't have to add it, or a role you do have to remove it.
* `/skill affects=journey`: Do a roll that counts to advance the journey (typically the one Narvi just told you to do). You can also do this by clicking a button indicating your hope spend, assuming you do not need any other options (like bonus dice) -- if you do, you'll need to use the command.
* `/roll marching`: Do a marching test. This is just short for `/roll travel affects=journey` and also has clickable buttons.
* `/journey next`: See the status of the journey and what has to happen next.
* `/weather`: See the current weather (if your Loremaster is using this function).
* `/journey log`: See the log of the journey so far.
* `/roll arrival`: When the journey ends, do this (once) to recover fatigue. This also accounts for your horse.

# Other Functions #

A few other things you might want to know about:

* Your loremaster, and Narvi, will automatically take care of most of the group things like recovery during rests, awarding of AP and SP, the gradual recovery of wounds, most of your virtues, the distribution of fellowship pool at the end of a session, etc. So just sit back and focus on your character's decisions and behavior.
* When your company has composed one more songs, you can use Narvi's built in `/songbook` to record them and track their uses. When you use a song, Song rolls that succeed automatically set you as unweary.
* In some games you might have multiple characters. Just add them as described above. Then you can `/character select` to switch between them.
* Narvi has the ability to record 'macros', where you can take a long command you type a lot and give it a very short name you can then use. Macros are per person, not per game. See more about this with `/macro`.
* Narvi has lookup tables (and you can make your own); use `/table` to learn how to use them.
* Report problems or make suggestions with `/suggest`.
* Join the #narvi channel at the [TOR/LOTR:RP Discord](https://discord.me/theonering) for support, and announcements of new releases.
