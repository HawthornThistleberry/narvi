![Narvi](https://bitbucket.org/HawthornThistleberry/narvi/raw/24835bdaaaa49bed9a5153abd7a3a8691524f041/images/banner.jpg)

a Discord bot for the *The One Ring* roleplaying game, written by HunterGreen

for support visit the TOR/AiME Discord at https://discord.me/theonering

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V44RKAI) to help pay for cloud-hosting

# Privacy Policy

_It is said that Narvi became ever more eccentric and secretive, hiding away in his workshop for months on end, and binding his apprentices to secrecy with terrible oaths._

What information does Narvi store? Basically only things you explicitly tell it to store, but what does that mean? Here's a complete and exhaustive list.

Things Narvi stores:

* If you create a game, Narvi records your username and Discord ID so it knows you're the loremaster.
* It also records the ID of which server the game was created in.
* If a loremaster adds you to a game, Narvi records your Discord ID.
* If you set a language, a prefix, or custom macros, Narvi stores them.
* Narvi records a 'last command' for each user to support the `/last` command. But you can opt out of this with the command `/savelast false`.
* Narvi keeps track of which is your selected game. When this becomes invalid Narvi may automatically select a game for you.
* Narvi keeps track of which character you have selected within each game (and again may automatically select a character for you if you don't have a valid selected character).
* When you enter a character's information, Narvi stores it, either internally or via an external Google Sheets sheet you've told it to use (even when using Google Sheets, some ephemeral information is stored internally, such as your combat stance, and a cached local copy).
* Narvi remembers other things the loremaster has set up for a game, like counter values, journeys, and encounters.
* Narvi remembers adversaries, tables, hex flowers, and journeys you create, and shares them with others if you publish them to the catalog.

Things Narvi does not store:

* Narvi never stores or pays any attention to any text, image, videos, or other Discord activity; it reacts only to its own slash commands.
* Narvi never stores any of the text it sees in any channel or PM, save only a recognized command, which it saves for the `/last` command, and it won't even save that if you type `/savelast false`.
* Narvi does not know about voice channels, video, or stages, and does not read or store anything from them.
* Narvi does not store your commands, apart from a single `/last` command as noted above.
* Narvi does not keep track of how often you've used commands or done anything else.
* Narvi has no access to any personal information about you, and does not store anything from your Discord profile other than the internal ID number and in some cases your Discord name.
* Narvi will temporarily cache in memory a list of users in a server but does not save this anywhere, nor does it store anything about any user that hasn't been explicitly added to a game.
* Narvi never looks through a server for lists of channels, threads, or other elements.

Things Narvi shares with its creator:

* Narvi tells me various error messages when it's trying to communicate with Google Sheets.
* Narvi stores its internal data in a few data files which I can read, and which I regularly back up on my own computer. Thus, anything on the list of what Narvi stores, I could potentially look at, if I needed for debugging purposes. (But it's in a JSON data file meant for computers not people to read, and there's a lot in there.)
* I have a diagnostic command that can show me a list of servers, a list of games, and a list of 2e characters with their Google Sheets keys, which I can use when diagnosing problems.

Who can see what Narvi stores:

* The loremaster can see their game and the characters within it.
* A player can see their own characters, and selected information about other characters in the game.
* Someone who is not in a game cannot see anything in that game, or even see that the game exists or what is name is, even if the game is on a server they are in. (Except for the creator's debugging access noted above.)
* Notwithstanding the above, if you type a command and Narvi answers showing something, like a report, a character sheet, or a roll, anyone who can see the channel you did it in can see whatever Narvi said. So if you check your `/sheet` in front of other people, they can see your sheet. If you don't want this to happen, you can always open a DM window with Narvi and do any commands you want there.

In short, the only information Narvi knows is the games and characters it was told about, and who told it.

Note that Narvi is open source, with all the source code visible at https://bitbucket.org/HawthornThistleberry/narvi/src/master/ so with a little time and Javascript/NodeJS knowledge you can verify the above. (My apologies for the places where the code is a bit ugly; Narvi's gone through some massive scope creep and is about 20x as big as I ever expected it to become.)

See https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md for full directions on what Narvi is and how to use it, and https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md for a how-to.
