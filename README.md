![Narvi](https://bitbucket.org/HawthornThistleberry/narvi/raw/24835bdaaaa49bed9a5153abd7a3a8691524f041/images/banner.jpg)

a Discord bot for the *The One Ring* roleplaying game, written by HunterGreen

* for support visit the TOR/LOTR:RP Discord at https://discord.me/theonering
* for announcements of updates subscribe at https://discord.gg/the-one-ring-lotr-rpg-348254014598545408

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V44RKAI) to help pay for cloud-hosting

# Other Documents

This is a pretty thorough manual for Narvi, but you might be better served by one of these more focused documents:

* [How To](https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md): A 'cookbook' that tells how to do each thing you might do with Narvi.
* [Player Guide](https://bitbucket.org/HawthornThistleberry/narvi/src/master/PlayerGuide.md): A shorter 'cookbook' with only things players need to know.
* [Adversaries](https://bitbucket.org/HawthornThistleberry/narvi/src/master/Adversaries.md): Build and share your own adversaries and encounters.
* [Lookup Tables](https://bitbucket.org/HawthornThistleberry/narvi/src/master/LookupTables.md): Build and share your own lookup tables and hex flowers.
* [Other Games](https://bitbucket.org/HawthornThistleberry/narvi/src/master/OtherGames.md): Using Narvi with games other than TOR.
* [Version History](https://bitbucket.org/HawthornThistleberry/narvi/src/master/VersionHistory.md): A list of every update in Narvi's long history.
* [Privacy Policy](https://bitbucket.org/HawthornThistleberry/narvi/src/master/Privacy.md): Narvi's official privacy policy.

[TOC]

# Installation

Add Narvi to your Discord server by going here:

* https://discordapp.com/oauth2/authorize?&client_id=688546328011341876&scope=bot&permissions=8

as long as you are the admin of your server, of course.

## Application Commands (Slash Commands)

As of April 2023 **all Narvi commands are now Application Commands** (also called Slash Commands), which are started using the forward slash `/` key, and which provide an interactive list of their options to guide you in entering them.

If you're not seeing them, maybe your server admin has disabled application commands in the permissions for your roles in those channels. This is the setting as it needs to be set for this and almost all other bots:

![application commands](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/discord-application-settings.png)

Or maybe you've set your client to use the legacy chat input in your Accessibility settings. This is how that should be set:

![accessibility settings](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/discord-legacy-input.png)

# TOR First Edition

__Narvi's support for TOR First Edition has been removed__ as of March 2023 (at that time, there were no 1e games in Narvi that had been touched in months). Any remaining 1e games have been converted to TOR games (meaning 2e) and characters will be Nameless Things until removed or updated with `/character key`.

# What Is Narvi?

Narvi can be just a dice-rolling bot, or it can be a tremendously powerful assistant for loremasters and players, when combined with Google Sheets's character creator and repository. In broad strokes, here's what Narvi will do for you, if you learn how:

* Roll TOR dice. This can be manual, or very automatic -- e.g., it can look up your combat proficiency and the target's parry, roll an attack, identify whether protection tests are needed and queue them for resolution, account for your virtues, tell you how much damage was done, and update the adversary's resulting endurance. It can automatically account for things like attribute TNs, inspiration, useful items, temporary bonus and penalties, gear quality, virtues, rewards, and cultural blessings while doing rolls.
* Access and display your character sheet through Google Sheets, using it to automatically handle things like weariness, skill ranks, the effects of virtues and rewards, and useful items.
* Update things on your sheet that change a lot, like recording endurance lost in battle (automatically), and recovered during rests and fellowship phases (including endurance, hope, wounds, and shadow).
* Figure out initiative, keep track of stances and knockback, cycle through combatants in order, and track and update engagement.
* Automatically execute combat actions like rallying comrades, and apply resulting temporary bonuses and parries on the next action.
* Attack foes (and be attacked back), with damage, protection tests, shadow tests, hate drains, etc. automatically handled.
* Track the progress of time limits and outcomes for councils and skill endeavours, automatically adjusting for relevant virtues.
* Walk you through the steps of a journey and record resulting fatigue and other effects, and recovery from them, too.
* Track eye awareness and thresholds and warn the loremaster when a revelation episode occurs.
* Let you record look-up tables and hex flowers, and make rolls against them.
* Record a list of company songs and keep track of their usage.
* See graphical status displays and reports that summarize an entire company by status, skills, attributes, journey roles, etc.
* Record macros to save frequently used commands for easy reuse.

What will you need to do to get all this?

* Install Narvi on your server. That's all you need to do manual dice rolls. You can stop there if that's all you wanted!
* The loremaster creates a game and adds the players.
* Each player creates or records their character on Google Sheets, which provides a complete character record -- no need to duplicate your efforts in a PDF.
* Each player tells Narvi which Google Sheets character is the one they're playing in this game.
* Learn a dozen or so Narvi commands for rolls, attacks, initiative, journeys, displays, etc. Narvi makes this as easy as possible through the interactivity of Application Commands, so you always see a summary of your options and what they mean.

Most groups that use Narvi use only a tiny fraction of its capabilities, and there's nothing at all wrong with that! But if you dig into all that Narvi can really do for you, you might be astonished at how much it can help, how much of gameplay can go so much more quickly and smoothly (and let you focus all the more on the story, the roleplay, and the immersion while Narvi does all the math, accounting, and paperwork for you). Narvi goes beyond a dice bot, beyond a Virtual Tabletop (VTT), into being an aid beyond expectations for both players and loremasters.

## Interactive Interface ##

Narvi features an interactive interface which you can start with the very simple `/go` command. How it works: Narvi will figure out what's currently going on for you, and present a panel of information with buttons and menu options relevant to what you're most likely to be doing just then. This won't be able to support every possible option, of course, but it will cover probably ninety percent of the things you might be about to do. This means you might not ever have to learn any of the commands that Discord uses to talk to Narvi, except for `/go` and a few others you'll only do once in a while, such as during character setup.

## Quick Start

The vast majority of things you do in Narvi will involve one of these commands:

* `/go`: An interactive interface (see above) for actions you're most likely to be doing right now.
* `/roll`: Roll dice, or a skill check, a test, etc.
* `/set`: Set a frequently changing value for your character, like hope or endurance.
* `/gain`, `/lose`: Like `/set` but to increase or decrease a numeric value.
* `/actions`: See available actions, or do one of them.
* `/attack`: Do a combat attack.
* `/start`, `/end`, `/next`, `/current`: Start and run an initiative.
* `/company` and `/clear`: Things that affect an entire company, like rests, fellowship phase, etc.
* `/game`: Create, switch between, view, and edit games.
* `/character`: Create, switch between, view, and edit characters.
* `/report`: See reports of almost anything in your game.
* `/foe`: The loremaster uses this to do actions for adversaries.

Some of these commands have variations and shortcuts for specific situations (e.g., `/damage` is a shortcut to `/lose endurance` since it comes up a lot). And there are less commonly used commands, like ones to set up macros, use counters, and send in bug reports. We'll cover all of these as we go, but don't get overwhelmed. The items above are all you'll need to learn to get almost all the power out of Narvi.

## Give Narvi A Test Run

To see Narvi in action, start out with this command:

* `/tor 3`

Then try it again and play with the options like setting a TN, adding a comment, making the feat die favoured, and trying out LM dice.

Pretty cool, right? Big bright 2e-style dice, and the tiniest taste of what Narvi can do with easily figuring stuff out for you when you roll, but this barely scratches the surface. Once you have a game set up, not only will that roll automatically apply your skill ranks, attribute TNs, useful items, virtues, and more, it will even show with an avatar of your character, or with Sauronic dice colors when the Loremaster is rolling.

## What Next?

Here's what we'll go over from here:

1. **Setting Up A Game**: What the loremaster and players do to get their game ready to go.
2. **Smart Rolling**: All the things you can do with the `/roll` command.
3. **Viewing and Updating Characters**: The `/set` command and its many shortcuts, plus `/sheet`, `/status`, and `/report`.
4. **Combat**: Running initiative, setting stances, doing attacks and combat actions, and recovering afterwards.
5. **Journeys**: Doing rolls and resolutions for travel, and the recovery of fatigue.
6. **Councils**: Setting up and running councils.
7. **Endeavours**: Setting up and running skill endeavours.
8. **Other Functions**: A hodgepodge of other goodies, such as non-TOR dice, counters, customization, table lookup, and more.

Also see https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md for a 'cookbook' explanation of all the steps, in order, to do various game things like combats, councils, and fellowship phases.

# Setting Up A Game

## Quick Reference

The loremaster creates a game, adds players to it, and can set a few things about it. The only tricky thing here is that every game needs a unique, one-word name, made of only letters and numbers. To create the game:

* `/game create`
* `/game add` to add players to the game

Players will then create or record their characters at Google Sheets. The loremaster can do this too, for instance to load pregens that will be reassigned later. The [Player Guide](https://bitbucket.org/HawthornThistleberry/narvi/src/master/PlayerGuide.md) has more details about this. Players will then:

* `/character create`

And that's it, the game is ready to go. A few more things you might want to know how to do:

* `/game list`: See a list of games you are in.
* `/game select`: If you're in multiple games, switch between them.
* `/game show`: See a summary of your selected game.
* `/game leave`: Remove yourself and all your characters from a game (cannot be undone).

## Maintaining Games

The loremaster might need to do some housework with their games:

* `/game remove`: Kick someone out of the game. Note: if the player is no longer in the server, you might need to use `/report` to see a report of Google Sheets keys, find the numeric player ID, and use that to remove the player. If the player has left the Discord, use `/game show` to see the player ID.
* `/game reassign`: Transfer a character to another player (including themself).
* `/game rename`: Change a game's name.
* `/game refresh`: Refresh all characters in the game.
* `/game end`: Permanently delete an entire game.
* `/game system`: Set up a game for Strider rules, or for one of the other rules sets that Narvi knows the dice for (see Non-TOR Dice below).
* `/game give`: Transfer ownership of the game to another person. You won't be in the game afterwards.

## Google Sheets Character Sheets

Narvi for 2e makes extensive use of the Google Sheets character sheet so that you can have a smart, and complete (and I mean complete), character sheet you can share with your loremaster, that Narvi can both read and write. This lets Narvi be very, very smart about automating things because it knows your skills, your current status, your weapons, your armor, even your rewards, virtues, and blessings.

Narvi is able to write updates back to your Google Sheets character sheet, and will do so automatically for those things that change a lot -- for instance, as you gain and lose endurance and hope, and for advancement things like skill and adventure points and treasure. However, Narvi does not attempt to update most of your character sheet. You'll go back to Google Sheets to do things like train your skills, change your gear, add rewards and virtues, and update your notes.

### Creating Your Sheet

Start by going to https://docs.google.com/spreadsheets/d/1ORyskKJvepDqS5EK0mzlO-q1qeaLno6tyBW7_MPTuNM/template/preview and clicking **Use Template** in the upper right corner. This makes a new, blank sheet in your own Google Drive.

Next, change the sheet name in the upper left (your character's name is a good choice) here, then complete the character sheet by following the instructions on the Instructions tab (you _did_ read the Instructions, right?).

### Sharing Your Sheet With Narvi

This part is a tiny bit technical but you only have to do it once. At this point you need to use the **Share** button to make Narvi able to see and change it. To do this:

* Click **Share** in the upper right.
* Under the General Access section, change to "Anyone with this link". This lets you, your loremaster, Narvi, and the whole world, _see_ your character sheet, but not change it.
* Copy this ugly and cumbersome address: `tor2e-character-sheets-api@tor2e-character-sheets-api.iam.gserviceaccount.com`
* Paste it into the Add people or groups box.
* Click **Send** (no other changes are required). Now Narvi can also write to your character sheet.

When you're done, your permissions should look like this:

![permissions](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/google-sheets-permissions.png)

Now copy the web address (URL) in your browser. It looks something like this:

https://docs.google.com/spreadsheets/d/1Sd8OeIclmrJjrNqUd6ZBIs1lGzVNsp3XjZvYOWlMP4f/edit#gid=0

Then paste it into a command like this:

* `/character create`

Give Narvi a second to pull your character sheet from Google Sheets, then type `/sheet` to see it. If all went well, you should be seeing your character.

If it didn't, and you got the key wrong, you can fix it by pasting the URL again into this command:

* `/character key`

If you're sure you got it right, but the sheet isn't showing, double-check that you set the permissions as described above, and once you're sure, `/character refresh` to ask Narvi to reload it.

### Updating Your Sheet

Whenever Narvi is the one causing your sheet to update, it'll automatically update its own copy as well. But if you go into Google Sheets and make changes, back in Discord, type:

* `/character refresh`

to force Narvi to get a fresh copy of your character sheet.

# Smart Rolling

The first purpose of Narvi is to roll dice. It can do this very manually, or very automatically. Most dice rolling uses the `/roll` command, though there are also `/tor`, `/feat`, and `/success` commands for manually rolling.

## What Are You Rolling For?

You can just tell Narvi how many dice to roll:

* `/tor 2`

That's a roll with a feat die and two success dice. Narvi shows the rolls and the total, and leaves it at that. You could also roll just feat dice, and specify a number of them -- this is good for, for instance, rolling treasure hoards -- or just success dice:

* `/feat 3`
* `/success 2`

But Narvi gets so much smarter when you tell it what you're rolling for, and let it use your character sheet to figure out the details. You can do a skill roll, a weapon attack, or a test, just by specifying the name. Some examples:

* `/roll song`
* `/roll protection`
* `/roll sorcery`
* `/roll spears`
* `/roll wisdom`

You can use `/skill` as a quicker way to get to doing a skill test, instead of doing `/roll` and then specifying the test is a skill test and selecting the skill on a second menu.

Some of the automatic things Narvi knows to do when you do this:

* Look up the relevant value(s) from your character sheet to figure out how many dice to roll.
* For a skill, apply bonus dice if you have a relevant Useful Item, and make the roll favored if it's a favored skill.
* Look at your weapons and armor to figure out the stats needed.
* Apply bonus dice based on things like your stance and the results of someone else doing a rally.
* Account for your virtues, rewards, and cultural blessings as they affect the roll.
* Figure out the target number and determine if you succeeded or failed.
* For weapon attacks, determine damage and whether a piercing blow results.
* Account for your character being weary or miserable.
* And a lot more -- check out `/explain` after a roll to see everything it did to figure that roll out.

## Modifying The Roll

A lot of things you can add to the roll command will cause Narvi to further modify the roll, though most of these will rarely be needed:

* `hope`: Spend one hope to get one bonus die added to whatever else is applicable.
* `inspired`: As above, but you get two dice. Use if you have a relevant Distinctive Feature.
* `magical`: Spend one hope to get an automatic success and a magical result. Usually available only to Elves and those with relevant magical artifacts.
* `favoured`: Force the roll to be favored.
* `illfavoured`: Force the roll to be ill-favored.
* `unfavoured`: Even if a skill roll would normally be favored, don't roll it that way.
* `tn=<num>`: Override the default target number (or specify one, if you're just giving a number of dice, not a skill or test).
* `weary`: Force the roll to be made as if weary or not weary (otherwise Narvi determines this based on your status).
* `noitem`: Make Narvi ignore any Useful Item or Marvellous Artefact you have that might otherwise apply. Or if the game defaults to noitem, setting this false will make Narvi use the item.
* `bonus`: Add or remove additional bonus dice.
* `modifier`: Add or subtract something from the total.
* `lmdice`: Force the dice to be (or not be) loremaster (dark) dice, where Eye is the best roll.

There are a few more options that will be mentioned further on under **Councils**, **Endeavours**, **Journeys**, and **Combat**.

## Adding A Comment

Often, Narvi will build its own comment to explain what the roll is based on what it knows so far -- for instance, if you did `/roll Craft` Narvi's output will show that it's a Craft roll. But often, especially when a lot of people are rolling at the same time, it is a huge help to the Loremaster if you put a comment in that specifies exactly what this roll is for. When the screen is scrolling with lots of rolls, it can be very hard to know what each roll is about! Sure, that's a lot more typing, but take it from me, your loremaster will greatly appreciate it.

## Other Roll Commands

When you are attacking, you will use the `/attack` command instead of `/roll`. This introduces a handful of combat-specific options: specifying your weapon (if you don't, Narvi will pick your best one), combat options like `escape`, and entering a parry. Narvi also accounts for stance and engagement automatically, applying or taking away bonus dice accordingly, and even doing damage and setting up protection tests for adversaries (more about this in **Combat**).

The loremaster may need to do a roll as one of the players (or one of their own characters), especially if, say, someone had to leave early. Simply put a full or partial name in the `character` option in any rolling command.

## Figuring Rolls Out

Narvi automatically accounts for a ton of things when it does rolls to figure out feat and success dice, including:

* hope or inspiration
* Brave At A Pinch and Desperate Courage can change hope to inspired
* skill lookup
* weapon lookup or Brawling (possibly modified by Brother to Bears)
* forward, skirmish, and defensive stance
* a previous rally action
* Sure At The Mark, Stout-Hearted, Furious, Hobbit-Sense, Against the Unseen, Stone-hart, Hobbit-Sense, Strength of Will, Untameable Spirit, Herbal Remedies, Skin-Coat, and many other rewards, virtues, qualities, and fell abilities
* council attitudes
* Useful Items

Sometimes you're not sure how it got what it got. Type `/explain` after a roll to get the explanation for the last roll Narvi did (only if the roll was made within a game). Because this is an internal debug process, this is only provided in English.

# Viewing and Updating Characters

## Multiple Characters

Everyone, including the loremaster, can have more than one character (though this may come up only rarely), and each character can be active or inactive. If you have multiple characters, here are the commands you need:

* `/character list`: See a list of your characters in the current game
* `/character show`: A quick display of your current character
* `/character select`: Switch to a character; all other Narvi commands will then work on that character
* `/character active`: Mark the character active or inactive; inactive characters are left out of initiative, mass heals, reports, etc.
* `/character image`: Attach an image to your character that will be used in the displays of your rolls. The URL must be the image itself, not of a webpage that contains the image (and some sites like imgur will make it very hard to get the image separate from its page, and so might not work). Best way to host an image where Narvi can always find it is to post it to Discord itself, then right-click it and Copy Link, and use that link.
* `/character key`: If you need to make a new character sheet and point your character in Narvi to it, you could use this.
* `/character remove`: Remove this character from Narvi
* `/character reassign`: Transfer ownership of this character to another player or the loremaster

Loremasters can create pregen characters and then transfer them to other players with the `reassign` option (don't forget to set the character inactive and then active accordingly too).

Nearly every command a player can do to affect their character, the loremaster can also do, by adding the name of the character. When specifying a character name, a few letters will usually be enough (just enough to uniquely distinguish the character from all other characters and from internal terms like 'pool' and their abbreviations).

## Character Sheet

You can peek at a summary version of your character sheet with these commands:

* `/sheet`: Shows most of your character, though some details are excluded -- use Google Sheets for a true full view
* `/sheet compact`: Same info as `/sheet` but with fewer emoji so it's a quicker view, and it also shows virtues, rewards, and useful items
* `/sheet skills`: Just the skills
* `/sheet weapons`: Just the weapons
* `/sheet virtues|rewards|items`: Just a list of your virtues, rewards, or useful items
* `/status`: Shows just things that change a lot: endurance, hope, wounds, temporary bonus and parry, conditions, and fellowship pool. Can also be used to set conditions.

The `/status` display is important; keep a finger on the pulse of your character by viewing it. It uses emoji representing glass beads or tokens to help make very visible your current condition. Watch, for instance, how much bigger your Endurance row is than your Load row, to see how far you are from becoming Weary.

## Updating Your Status

These commands can be done by either the player or the loremaster. And believe me, your loremaster will appreciate it if you get in the hang of using them yourself. They have a lot to do already! When the loremaster does them, they have to insert your character name (or at least the first few letters) after the command. These are the commands you'll use:

* `/set`: Directly sets a value for many things. You can't set your Wisdom, Strength, or Craft skill this way -- go to the Google Sheets sheet for that -- but you can set your endurance, hope, etc. But you are more likely to be adding or subtracting than directly setting, so...
* `/gain`: Add a certain amount (defaults to 1) to a value. Just got back three lost endurance? `/gain endurance 3`.
* `/lose`: As above but negative.
* `/damage`: As `/lose` but assumes it's endurance. You can also note `knockback`, which will halve it.
* `/heal`: as `/damage` but adds some back in (up to your maximum). Most recovery will happen in company-wide things though, about which, see **Company-Wide Actions**.
* `/restore`: Restore it back to its maximum (e.g., you've recovered all your lost endurance).
* `/wound`: Record a wound or update an existing wound. The roll, which is rolled by Narvi if you leave it out, determines the recovery time (see TOR101). Status can indicate you're unwounded, or your wound is untreated, treated, or failed (attempted a treatment but it failed, can't try again until tomorrow), or that you're dying (two wounds).
* `/treat`: Do a Healing roll to treat someone (or yourself with `self`) and automatically apply the results. Will handle wounds or poison (always prioritizing wounds).
* `/status`: In addition to seeing your status this can be used to set certain temporary conditions like daunted, temporary parry, or number of engaged enemies.

Note that, to make things clear and simple, Narvi, like the Google Sheets sheet, uses "Encumbrance" to refer to the part of your load that comes from carried gear, "Fatigue" for fatigue, and "Load" only for the total of the two. The core rules use "Load" to refer to both Encumbrance and the sum of Encumbrance and Fatigue, which gets confusing at times.

## Reports

There is a `/report` command that can display information for every active character in the game; this is very useful for a lot of purposes, so look over the list of report options available. When you type `/report` you will see three main headings for reports: `skills`, `attributes`, and `status`. Click or tap on one to see a further menu of reports in that heading. Reports are mentioned in other parts of this guide where they're relevant. Some worth drawing attention to:

* `/report skills`: A very dense report of all skill values. Good for identifying what gaps a group has.
* `/report traits`: Shows the culture, calling, and distinctive features for every character in the game.
* `/report treasure`: Shows everyone's AP, SP, treasure, and standard of living.
* `/report hope`: Also shows the fellowship pool; useful for figuring out how to distribute it.
* `/report weapons`: A report of weapons in the game.
* `/report key`: Clickable links to the character sheets for every character in the game.
* `/report stances`: Shows everyone's combat status including stances, knockback, and engagement, along with rallies in effect.

# Combat

Narvi can help the loremaster automate combat to a great degree. To get the full functionality and smarts out of Narvi, the loremaster should set up encounters full of adversaries, but it's also possible to "do it manually" without this.

## Encounters and Adversaries

The loremaster can create adversaries, or find them in a global catalog, and then assemble them into encounters built on the fly or ahead of time. An encounter can also contain events -- basically messages timed to appear at a particular moment -- and advantages or complications that the combat starts with. Full details on creating these can be found in [Adversaries.md](https://bitbucket.org/HawthornThistleberry/narvi/src/master/Adversaries.md). Once an encounter is created, the loremaster can activate it to include it in the combat.

## Summary of Combat

In brief, the timeline of a combat is as follows (each of these steps will be covered below):

* `/start`: The Loremaster starts the combat and optionally loads an encounter and other parameters.
* Narvi keeps track of the current round and what phase in that round it is. Each phase is when one group of things happen.
    - Opening Volley: Player-heroes with ranged weapons can make an attack with `/attack`.
    - Change Stances: Player-heroes can use `/stance` to choose a stance.
    - Forward, Open, Defensive, Skirmish, and Rearward: Player-heroes can `/attack` or use various other actions.
    - Adversary Actions: Foes can do things like `/foe attack` and `/foe ability`.
* Most actions have their consequences automatically, such as damage, hate drains, etc.
* Protection and shadow tests can be done with `/roll` or `/foe protection` as needed.
* When all actions in a phase are done, Narvi will tell you, and the loremaster does `/next`.
* When the fight is over, the loremaster can `/end` it.

## Starting A Combat

The `/start` command includes a number of optional settings:

* `encounter`: Specify the name of an encounter for the combat to use. If specified, the foes, events, and modifier are used throughout, and the image is displayed. The title is also used unless one was specified to override it.
* `copyencounter`: If set, a copy of the encounter is used, so that the untouched original is available to use again later.
* `ambush`: If specified, the combat begins with Adversary Actions and there is no Opening Volley. It's also possible for adversaries to be ambushed, losing Opening Volley and being at -1d during the first round.
* `title`: Sets a title displayed on various reports and initiative displays.
* `modifier`: If provided, begins the fight with an advantage or complication for the player-heroes.
* `duration`: Changes the modifier noted above to only last a certain number of rounds.
* `light`: If the fight is in sunlight or darkness, this causes some fell abilities to be implemented.

Choosing an encounter when you start a combat also selects it so you can use the `/encounter` command to modify it on the fly.

## Initiative

At any moment when a combat is underway type `/current` to see the current phase. This also shows who can act with color-coded symbols, and whether they have acted or not, along with relevant modifiers as applicable. You can also see a short version of this in `/game show`. `/report initiative` shows the same information for the entire round, with colors to indicate things already done, currently happening, and in the future.

![An Initiative Report](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/initiative.png)

Narvi keeps track of who has an available action and if they have taken it, and accounts for things like knockback, foes with Might over 1, and different things that count as major actions, but it can't get it all right thanks to a lot of special circumstances (e.g., something that's usually a major action might be a minor action sometimes). So Narvi will not stop you from doing another action. And while Narvi will keep track of whether everything in the phase has happened, to tell you it's time to advance, you can always advance with the `/next` command even if it doesn't think everything has been done (you have to confirm this though).

## Heroes Attacking Adversaries

Use `/foe list` to show the players a sanitized list of adversaries (living/dead status, shortname, fullname, and engagement only, the same thing they see when they run `/report adversaries`) so they know what targets to pick from. When a player makes an `/attack`, if there is an active encounter, they can use the `target` option to specify an adversary to attack. In this case the parry of the adversary in the encounter is used (unless parry is also specified, in which case it takes precedence), and furthermore, damage is automatically done and reported to the loremaster on the LM channel. When the adversary's endurance drops enough or it gets enough wounds, it is automatically removed from initiative and from counting as an engagement (but still is listed, with a skull and crossbones icon).

Narvi will automatically account for most relevant virtues, rewards, qualities, and fell abilities, along with stances, engagement, and all other modifiers. Use `/explain` to see how it came to the conclusions it came to after the roll. When a hero gets tengwar to spend, green buttons will display their options and can be clicked to carry out that choice (or the `/spend` command can also do the same thing).

If the player-hero scores a piercing blow, a protection test is queued up. The loremaster can resolve this with the `/foe protection` command, which will carry out the first queued protection test for that adversary and render whatever consequences it has. Also note that the Hideous Toughness ability may queue up a test if enough damage is done, and `/foe protection` will also cause the corresponding recovery if the foe survives.

## Other Hero Actions

Depending on a hero's stance, they may have several other options:

* `/intimidate` in forward stance to give enemies pushback which subtracts dice from their actions.
* `/intimidate` can also be used with the Riddle option to use Riddle to strip hate from the Dull-witted.
* `/rally` in open stance to give allies a bonus.
* `/protect` in defensive stance to give an ally a protective bonus.
* `/gainground` in skirmish stance, and `/prepare` in rearward stance, to give yourself a bonus on a future attack.

## Advantages and Complications

Usually based on a `/skill battle` roll or the circumstances of the fight, a hero might gain advantages or cancel out complications, as might the adversaries. The loremaster can record these modifiers:

* `/modifier list`: Show any active advantages or complications. Also shows in `/game`.
* `/modifier set`: Add a modifier. You must specify the bonus or penalty (negative numbers for the latter) and can also specify a duration (lasts the whole combat if not), a starting phase (it will remain dormant until that comes around), and a list of characters it affects (or `adversaries` to affect the adversaries).
* `/modifier delete`: Delete a modifier before it has expired on its own.
* `/modifier clear`: Clear all modifiers. Narvi does this automatically when combats start and end.

Modifiers are automatically applied to all hero attack rolls alongside other effects like stances.

## Protection and Shadow Tests

Certain foe actions will queue up protection or shadow tests for player-heroes, and player-hero actions can also queue up protection test for foes. Queued tests can also include a condition (see Conditions below) if they're failed, and other values to configure for different kinds of tests and modifiers to them. You can view, and the loremaster can change, tests manually:

* `/test list` to see them (also shows in abbreviated form in `/status`)
* `/test clear` to clear them
* `/test add` to create new ones

When a player uses `/roll protection`, `/roll dread`, `/roll sorcery`, or `/roll greed`, Narvi will check for any queued test of the appropriate type, and use it if one is found; in this case, the test is taken off the queue, and the result of the roll is carried out (e.g., you automatically get a wound or condition, or gain shadow). In the same way, the loremaster can use `/foe protection` to roll an adversary's queued protection tests.

## Conditions

Narvi implements the following conditions, automatically applies them, and clears them at appropriate times (often the ends of combats or during fellowship phases). They are shown in `/status` and can also be set and cleared there, and also can be cleared with `/clear`.

* Daunted: prevents you from spending hope
* Misfortune: a consequence of some Dreadful Spells, this increases all your TNs by your Shadow
* Dismayed: a consequence of some Dreadful Spells, this makes all your rolls ill-favoured
* Haunted: a consequence of some Dreadful Spells, this penalizes all your rolls with -1d
* Dreaming: a consequence of some Dreadful Spells, this renders you unconscious
* Seized: you are forced to fight with Brawling from Forward stance
* Drained: as Seized plus each round you take damage and a protection test
* Shield Smashed: your shield is broken and ineffectual for parry and shield thrust
* Knockback: you cannot act during the next round
* Weary: for times you are weary for reasons other than being low on endurance
* Unweary: prevents weariness from taking effect (e.g., due to a Song)
* Miserable: for times you are miserable for reasons other than being low on hope

## Adversaries Attacking Heroes

Foes can attack players with `/foe attack`. You specify the adversary doing the attack, and then a bunch of optional stuff:

* `character`: the player-hero to attack (if omitted, uses whoever this foe attacked last time)
* `attack`: which of the adversary's attacks to use
* `fierce`: if the foe has the Fierce fell ability, specify whether to use it
* `hate`: spend Hate or Resolve for a bonus die?
* `bonus`, `feat`, `modifier`, `weary`, `tn`, `comment`: as with other rolls

Narvi will roll the attack, accounting for stances, parries, shields, knockback, Shield Thrust pushback, and Small Folk; as usual, you can `/explain` the resulting roll. It will then implement any consequences (including damage, protection tests, and dread tests with Dreadful Wrath). When the foe gets tengwar, Narvi will figure out if any special attack is applicable (whether the attack has one, and if it's Break Shield but the foe has no active shield); if there are no options, it will automatically do Heavy Blow, but if there are, it will add tengwar spend buttons as for player attacks. If you specified the fight was in darkness during `/start`, it will also account for Denizens of the Dark.

Adversaries can also get tengwar. If they have a special option that's available, you'll get red tengwar-spend buttons to click; if there is no choice, Narvi will automatically do the heavy blow when doing damage.

When a foe does damage to a player-hero (or if you record it manually with the `/damage` command), Narvi keeps track of it. If the player then uses `/status` to set their character to be knocked back (thus forfeiting their action in the next round), Narvi automatically refunds them half of that damage.

## Adversaries Using Fell Abilities

`/foe ability` supports these fell abilities (if you use any others, Narvi will simply tell you that it doesn't know what to do with them):

* __Deathless/Spirit__: Adversary must have a wound or lost endurance, and will cure the wound first if it has one. Can be used even if the adversary is out of the fight (and potentially restores them to it).
* __Horrible Strength__: A queued protection test (see below) caused by this adversary on the specified character (defaults to first engaged) is changed to ill-favoured.
* __Foul Reek__: Adversary must be engaged (using `/foe attack` or `/foe engage`) first. 
* __Dreadful Spells__: When creating the adversary, specify the condition for the type, and number of points for the amount, and those will be used (but if none are specified, it will default to 1 point and no condition). If no target is specified, but the adversary has anyone engaged, defaults to the first one.
* __Freeze the Blood__: If the fell ability on the adversary specifies amount and/or condition those are used; if not, defaults to 2 points, dismayed.
* __Strike Fear__: As Freeze the Blood, but defaults to 1 point, daunted.
* __Thing of Terror__: As Freeze the Blood, but defaults to 3 points, daunted.
* __Yell/Howl of Triumph__: Instead of specifying a character, specify a string of adversary shortname beginnings, to identify which adversaries are affected. For instance, if you have a bunch of Orc Soldiers whose shortnames are OS1 to OS6, and some Goblin Archers with shortnames GA1 to GA4, and some Wargs with shortnames W1 to W2, and the yell should affect the goblins and the orcs but not the wargs, type `OS GA`, and Narvi will only apply the hate gain to the adversaries whose shortnames start with OS or GA, so the wargs will be omitted. If you don't specify anything, every adversary (except the one using the power) will get the gain.

Note that using a fell ability does _not_ usually consume the adversary's action and so does not cause their action in Narvi's initiative to be checked off.

Foes can use `/foe roll` for any other rolls not covered by the above.

## Engagement

Whenever you use `/foe attack` or a foe is defeated, Narvi automatically records the engagement, which then shows in `/report adversaries`; foes with Might of 2 or higher can have multiple player-heroes engaged this way. You can also manually set engagement with `/foe engage`, which is especially important during the first round when players in defensive stance need to know how many foes have engaged them, before the attacks have come along.

Note that Narvi does not attempt to limit engagement in any way to enforce the rules; that's entirely on your shoulders. It only records it and then uses it for things like defensive stance and some fell abilities.

## Modifying Encounters And Adversaries

While a fight is underway most updates to foes happen automatically, but the loremaster might need to make other changes:

* `/encounter show`: Shows an entire encounter or a single adversary in detail. Be sure not to do this in front of your players by accident!
* `/report adversaries`: Show the status and engagement of all adversaries -- also probably best kept secret. But when a player runs this, they just see the target choices, useful preparing for an `/attack`.
* `/foe gain` and `/foe lose`: Increase and decrease values of things like endurance or pushback, for a single adversary or all of them.
* `/foe clear`: Reset values like wounds or endurance foe a single adversary or all of them.
* `/encounter delete`: Can be used to delete a single adversary if a shortname is provided.

## Ending The Combat

While you `/end` a fight you have the option to delete the encounter, which is useful if you're not going to reuse it, especially if you used the `copyencounter` option when starting the combat. Ending the fight also clears certain effects and conditions as appropriate.

## Combats Without Encounters

* `/foe roll`: Do an attack roll on a hero, accounting for stances and other things. Also counts up engagement.
* `/foe roll engage`: Sets an engagement count on a hero.
* `/tor`: Use this for protection rolls by adversaries and other rolls as needed.

## Reports

Some reports useful in battles:

* `/report damage`: A quick summary of the heroes and their current condition.
* `/report stance`: Show everyone's stance and Battle scores.
* `/report summary: Produces a dense but brief cheat sheet of important info in combat.
* `/report adversaries`: Shows engagement and status of adversaries.
* `/report initiative`: Shows the entire round with relevant bonuses and statuses.

## Company-Wide Actions

Sometimes during, and more often after, a battle, the loremaster will need to do things that affect the entire fellowship, for cleaning up after a battle and reflecting healing. Sometimes this isn't even during a combat, though.

* `/clear knockback`: Remove knockback from everyone.
* `/clear endurance`: Restore everyone to full endurance, regardless of status.
* `/clear hope`: Restore everyone to full hope, regardless of status.
* `/clear wounds`: Removes all wounds.
* `/clear damage`: Both of the above at once.
* `/clear fatigue`: Removes all wounds.
* `/clear conditions`: Removes conditions like daunted, seized, and shield smashed.
* `/company short`: A short rest.
* `/company prolonged`: A prolonged rest.
* `/company safe`: A prolonged rest in a safe place like a safe haven.
* `/company short safe`: A short rest in a safe place -- only needed if one of your heros has Elven Dreams.
* `/company endurance`: Give everyone a certain amount of endurance recovery, or with a negative, take endurance away.
* `/company ap|sp|treasure`: Give rewards (adventure points, skill points, or treasure) to everyone.
* `/company session`: Gives both AP and SP in the same amount to everyone, and then does an automatic `/pool distribute` and resets the pool, to wrap up a session when Narvi's smart pool distribution is good enough.

## Unphased Initiative

In some games, especially play-by-post, it slows things down too much to have combat go phase by phase. To support this, Narvi supports an unphased initiative via `/game config unphased-initiative`. If you enable this mode for a game:

* Stance changes can be done at any time.
* Opening Volley is still treated the same.
* Change Stances phase is skipped. Instead, after opening volley or after adversary actions, combat shows all stance phases (from forward to rearward) at once, and all of them can be acted upon. Narvi still tracks who has acted, and warns you when everyone has done so.
* Adversary phase is treated the same.

Thus, you can `/start` a combat, play out opening volleys, then everyone can act in any order. It's up to you to sort out things like whether the person who acts in Rearward is attacking a target that someone else already killed in Forward. When everyone has acted and you do `/next` (or you can force it even if not everyone has acted using the confirm option), the adversary phase plays out as usual. When that's done and you do `/next`, Narvi goes back to the all-phases display and everyone can act again.

# Journeys

In broad strokes, how Narvi implements journeys:

* You can set up a journey by giving it a name, notes, an image, an origin, a destination, and a series of steps.
* You can publish your journey so others can use it, or use journeys others have published.
* You start a journey, possibly specifying if it's forced march, what season it is, etc.
* Narvi notifies everyone what the current status is and who has to do what next.
* That can be telling the Guide it's time to do a Marching test; when they do, Narvi advances the journey.
* Or it can be rolling a journey event and telling someone based on role to roll an appropriate test, then implementing the consequences.
* Narvi even handles perilous areas as it goes.
* You can undo the last roll if it was done in error, see a log of the journey, or end or abort it early.
* When the journey ends, Narvi applies fatigue and tells you how long it took.
* Everyone can then `/roll arrival` to recover based on steed and their Travel roll.

See the https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md document for a step-by-step guide to all of this. Following is just a summary of the commands you'll use.

* `/role <role>`: Set your journey role. In the rare case when someone has multiple roles, setting `multiple` to True means you are toggling roles; choose a role you don't have to add it, or a role you do have to remove it.
* `/journey list`: See a list of available journeys.
* `/jedit create`: Create a journey. Then edit it with the options in `/jedit` (see [How To](https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md) for more details).
* `/journey start`: Start a journey.
* `/journey quick`: Create an on-the-fly, temporary, single-step journey and start it; it will be deleted when it is finished.
* `/roll marching`: Make a marching test when prompted.
* `/skill <skill> affects=journey`: Make an event resolution test when prompted. (For Strider Mode noteworthy encounters you can also `/roll <skill> affects=journeypass` and `/roll <skill> affects=journeyfail`.)
* `/journey pass|fail`: If a player did a roll and forgot to include `journey` the loremaster can use this command to record the result as if they had.
* `/journey next`: See the current status of a journey and what's needed next.
* `/journey log`, `/journey undo`, `/journey reset`, `/journey abort`, `/journey end`: Manage the journey.
* `/roll arrival`: Recover fatigue when the journey is over.
* `/company arrival`: As above, but the loremaster does this for everyone at once, assuming no hope spends etc.
* `/report roles`: Shows everyone's journey-related skills, good for choosing roles.
* `/report journey`: Shows everyone's role, Travel skill, and their role-related skill.
* `/weather next`: Uses Goblin's Henchman's [Hex Weather Flower](https://www.drivethrurpg.com/product/367072/Weather-Hex-Flower--Random-weather-generation--by-Goblins-Henchman) system (see below) to randomly generate weather.
* `/weather show`: Show weather in this game without advancing it.
* `/weather set`: Directly set the weather (use the green numbers shown below):
* `/game config journey-weather`: Make Narvi automatically generate weather before each marching test.
* `/game config journey-tiring`: Instead of applying fatigue at the end of the journey as per the rules, Narvi will apply it immediately after each journey event.

![Hex Weather Flower](https://bitbucket.org/HawthornThistleberry/narvi/raw/8cbfdb3fdf4cdf3b2b0c5d5a3c05cdbe24a8505e/images/weatherhexflower.png)

# Councils

To make councils go smoothly while still allowing players to have full control over the actions of their characters, most of running a council is done using the `/roll` command you're already familiar with, but with a few new options. First, the loremaster can start, end, and modify a council as follows:

* `/council start`: Starts a council. The `type` can be `reasonable`, `bold`, or `outrageous`, and is required. You can also provide an attitude (`reluctant`, `open`, or `friendly`), defaulting to `open`. In addition, you can specify whether to use `core` rules (base time limit is equal to 3) or `original` rules (from first printing before errata, base time limit is resistance), with `core` being the default.
* `/game config council-original`: `original` becomes the default for all councils in this game.
* `/council end`: Ends a council.
* `/council time|timelimit|score|resistance|attitude`: Directly change a value in the current council.
* `/council pass|fail`: Record a result. Useful if a player forgot to specify the `affects`.
* `/council undo`: Undoes the effect of the last roll that affected the council, even if it just ended it. You can only undo one action.

Once a council is happening, players do their actions by using the `/roll` command, and they specify that it affects the council, and the result will automatically advance things. For instance, if you were using Awe for your introduction, you might:

* `/roll awe affects=council`

Specifying that it affects the council does three things:

* Narvi knows to count this roll towards the council. The first roll thus done is automatically treated as the introduction, which helps determine the resulting time limit. Subsequent rolls are interactions, which count time up towards that limit, and (hopefully) accrue a score.
* The roll automatically applies any attitude modifiers (+1d for friendly, -1d for reluctant), including accounting for if you have the Friendly And Familiar virtue. (Note that it can't automatically apply Dwarf-friend, but you can always override the attitude by specifying it with your roll, e.g., `/roll awe affects=councilfriendly` to override any standing attitude.)
* After the roll, Narvi shows the current status of the council, and evaluates whether it has ended, and if so, how. You can also get this display any time with `/council show` (or `/game show`).

The players can certainly be doing rolls without `council` for anything they're doing that should _not_ contribute to the score and progress of the council. For instance, if one hero is sneaking into the treasure room while the others are talking, the sneak just doesn't include `affects=council` when they `/roll stealth`.

# Endeavours

Skill Endeavours are very much like councils (or more accurately, the converse is true). As with councils, most of skill endeavours is done with the `affects` option of the `/roll` and `/skill` commands. The loremaster starts, ends, and controls a skill endeavour as follows:

* `/endeavour start`: Starts an endeavour. The `task` can be `simple`, `laborious`, or `daunting`. You must also specify a time limit, typically 3-6.
* `/endeavour end`: Ends an endeavour.
* `/endeavour time|timelimit|score|resistance`: Directly change a value in the current endeavour.
* `/endeavour pass|fail`: Record a result. Useful if a player forgot to specify the `affects`.
* `/endeavour undo`: Undoes the effect of the last roll that affected the endeavour, even if it just ended it. You can only undo one action.

Once an endeavour is happening, players do their actions by using the `/roll` command, and they specify that it affects the endeavour, and the result will automatically advance things and show the resulting status of the endeavour, including a final conclusion if it is now over.

# Other Functions

Here are some more features Narvi offers that can help a busy Loremaster.

## Hope And Shadow

* `/report hope`: A report of the status of everyone's hope, plus the fellowship pool.
* `/company hope`: Give (or if a negative number, take away) hope from everyone.
* `/company shadow`: Give (or if a negative number, take away) shadow from everyone.
* `/pool show`: See the Fellowship Pool.
* `/pool max`: Set the maximum size of the fellowship pool.
* `/pool set`: Set the current size of the fellowship pool.
* `/pool use`: Spend a fellowship pool point, e.g., for a Patron ability.
* `/pool claim`: Transfer fellowship pool points to your own hope.
* `/pool distribute`: Attempts to automatically and fairly distribute the pool, by giving to those in most need until the pool is depleted. Often useful when there's enough in the pool to refresh everyone.
* `/pool distribute <character>=<amount> <character>=<amount> ...`: Distribute the pool manually. Subtracts from pool and adds to characters, until the pool is depleted.
* `/pool reset`: Restore the pool to maximum size. Also done by `/company fellowship` and `/company yule`, and can be done by `/pool distribute` if specified.
* `/company fellowship`: Recovery during a fellowship phase: clears all wounds, maybe recover hope or lose shadow, etc. The specified amount (0-3) is how much Shadow the company will recover (default 1).
* `/company yule`: As above, but a Yule fellowship phase, with even more hope recovery.

## Eye Awareness

Suitably configured, your game can automatically track Eye Awareness using the following commands:

* `/eye base`: set the base Eye score (based on party composition) - if you don't supply the value, Narvi will try to calculate it, but note that it might get it wrong in some edge cases such as famous war gear that does not have any special qualities unlocked
* `/eye threshold`: set the threshold for the Hunt (based on how dangerous is the land they're in)
* `/eye set`: set the current Eye status, or increase or decrease if the amount starts with `+` or `-`
* `/eye show: show the current Eye status
* `/eye reset`: reset the Eye score back to the base
* `/game config show-eye`: configure your game to show the Eye status; if not, Narvi will DM the loremaster
* `/game config roll-eye`: automatically increment Eye when an Eye is rolled out of combat
* `/game config magical-eye`: automatically increment Eye when `magical` is used on any roll, except if a combat is active
* `/company fellowship` and `/company yule` will also reset the Eye score back to the base

Whenever anything raises the Eye score to the threshold, Narvi will notify you of a Revelation episode, and reset the score. The notification will be sent to the Loremaster in a DM or the LM-channel if `show-eye` is not set; if it is, it'll appear within whatever made the score increase, e.g., the result of an `/eye` command or of a roll.

## Environment Damage (Endurance Loss)

In some situations Narvi will automatically use the rules for Sources of Injury that cause Endurance Loss (TOR p143) including fire, cold, poison, suffocation, and falling (e.g., due to a fell ability, a foe attack, or a journey event). The Loremaster can also do this directly:

* `/environment`: Specify the affected character (or `company` for the whole company), the type of damage, and the severity level.

Narvi will automatically apply wounds or dying conditions as per the rules.

## Counters and Secrets

The loremaster can track other numeric values for any purpose desired in their game. For example, count days since a particular event, how an NPC is feeling about something, treasure not yet distributed, etc. These can be counters (visible to the players) or secrets (only visible to the loremaster).

* `/counter list`: See a list of all counter values. Secret messages are sent to the loremaster only.
* `/counter set`: Set a counter. The value can be a number, or if it starts with + or - it will change the existing value. The secret flag only applies if the counter is being created.
* `/counter delete`: Delete a counter.

## Songbooks

Each game has a songbook that the heroes can use to track any songs they compose during fellowship phases.

* `/songbook show`: See the songbook.
* `/songbook add`: Add a song (or update an existing one). The type must be Lay, Victory, or Walking. The author name must be a single word but does not have to be one of the game's characters. Bonus, if present, should be a plus or minus followed by a number of dice; however, note that Narvi doesn't know when a player's Song roll is meant to be a songbook use, so it won't automatically put the `bonus` into the roll.
* `/songbook remove`: Remove a song.
* `/songbook use`: Note that a song has been used once. Automatically makes subsequent Song skill rolls set an unweary condition on success.
* `/company fellowship` and `/company yule`: Restores all songs to unused.

## Table Lookups

Narvi allows you to build custom lookup tables and hex flowers, and then have you and your players roll against them. Tables can then be published to a catalog which allows everyone else who uses Narvi to use (but not modify) them. Narvi knows all the tables in the published books. See https://bitbucket.org/HawthornThistleberry/narvi/src/master/LookupTables.md for instructions for designing and building tables.

Tables can be tagged with one or more tags that identify something about the category or type of table it is. For instance, all core rules tables have the tag `core`, all Strider Mode tables have the tag `strider`, and all TOR tables have the tag `tor`. (A table can have many tags.) Hex flowers don't use tags.

Tables can either have one or two rolls. A simple table with only one row might use a d8 and have eight rows numbered 1 through 8. Most tables are like this. However, there are also tables that use two separate rolls; for instance, Strider Mode lore tables each use a feat die roll, followed by a success die roll, and thus have 72 possible outcomes. When there are two rolls, you separate the rolls, or their modifiers, with a `/`.

When feat dice are used, E is used for Eye rolls (and will sometimes be shown as a 0), and G is used for Gandalf rolls (and will sometimes be shown as 11).

For using tables:

* `/table list`: Get a list of all available tables you can use. Optionally only show those you created (`mine`), that are published (`published`), that have one or more tags, and/or that include a given search term in the name, notes, or rows.
* `/table select`: Pick a table using its table name. All other commands will use this table (but `roll`, `lookup`, and `show` let you override that).
* `/table show`: Get a look at the table.
* `/table roll`: Roll on a table (by default, your selected one). If the table has more than one roll, you can specify separate modifiers for each roll by separating with a `/` (for instance, `+2/-1`). If any feat dice are involved, you can specify if they are favoured or ill-favoured, and also set `natural` false to set Narvi to allow modifiers to change rolls to or from Eye or Gandalf.
* `/table lookup`: Look up a result without rolling (in case you already rolled some other way). When there are multiple rolls, separate them with `/`.

Hex flowers are only rolled on within a game, since they include a 'memory' of where they were last, but otherwise work very much like tables, using the `/hexflower` command. Use `/hexflower current` to see the current value, and `/hexflower next` to roll and advance. Note that the `/weather` commands are simply a specific hex flower (named `weather`, naturally).

## Strider Mode

By setting a game to Strider Mode with `/game edition strider` you activate several new features:

* All dice rolls default to `lmdice=false` except `/foe` rolls.
* Any die roll that is a Gandalf or Eye will automatically also roll on the Fortune or Ill-Fortune table.
* `/stance skirmish` becomes available. In Skirmish stance:
    * You are warned if you use a non-ranged attack (should not be possible per the rules, but Narvi will finish the roll with the warning message anyway).
    * Your attacks are at -1d.
    * You can Escape Combat from Skirmish stance, without the -1d.
    * You can do `/gainground` which is similar to Prepare Shot but lets you use either Scan or Athletics; Narvi makes a good guess as to which is better and uses that, or you can specify which to use.

Note that skirmish stance also means foes attacking you are at -1d if using a melee weapon, but since Narvi doesn't know if a `/foe` attack is melee or ranged, it does not try to handle this for you automatically.

Otherwise, everything in Strider Mode works the same as in 2e.

## Scribbles

With a paper character sheet, there might be designated spots for all sorts of things, but sometimes you want to write something there's not a designated space for. To scribble in the margins. Narvi lets you do that. For every character sheet, you can have as many scribbles as you want, and each one has a name and a value. Even better, you can let Narvi look them up and use them in any command.

* `/scribbles list`: View all your scribbles. They will also be appended to the bottom of other sheet displays.
* `/scribbles set`: Set a scribble's value, creating it if necessary.
* `/scribbles gain`: As `set` but assumes it is numeric, and adds an amount to it. (If it's not numeric, it's overwritten.)
* `/scribbles lose`: As `gain` but subtracts.
* `/scribbles delete <scribble>`: Remove (erase?) a scribble.

## Non-TOR Dice

Narvi also has some very basic support for other kinds of dice, including generic RPG dice, and specific rolls for Coyote & Crow, The Expanse, and Cortex (Serenity), though it's not trying to compete with something like [Dice Maiden](https://top.gg/bot/377701707943116800). You can also use scribbles for custom character sheets for these games. For more about this, see https://bitbucket.org/HawthornThistleberry/narvi/src/master/OtherGames.md for instructions.

# Narvi's Limitations

Narvi _will_ automatically account for the following cultural blessings, virtues, rewards, qualities, and fell abilities, if present on the character sheet (be sure to enter your Virtue spelled exactly as below, including punctuation, though it's okay to have it followed by something additional) or the adversary:

* Against The Unseen: All Dread tests are favored. (Add `bonus=1` manually when the roll is forced by a spirit.)
* Allegiance of the Dunedain: Reduced Hope recovery during regular fellowship phases.
* Ancient Fire: Reduces severity of fire environmental damage.
* The Art of Smoking: All Hope gains are +1 during `/company`, `/pool distribute`, `/pool claim`, and journey events, but not `/gain` as that can be used for corrections as well as actual gains, and can be set to the right value manually.
* Beset By Woe: Shadow recovery only during Yule fellowship phases.
* Bodiless: Accounted for any time a foe is damaged; queues a protection test, then implements the consequences when that test is resolved with `/foe protection`. Also, `/foe ability` can be used to clear a wound with a Hate spend.
* Brave At A Pinch: If Miserable, Weary, or Wounded, any time you add `hope` to a roll it is treated as `inspired`.
* Brother To Bears: Brawling attacks do not lose 1d and can pierce on Gandalf with injury 12.
* Cleaving: On a kill, attacker can do another attack (right now just displayed).
* Close-fitting and Ancient Close Fitting: Adjusted on the Google Sheets sheet (and automatically updates).
* Cram: -1 to fatigue gained during journey events. Increase recovery for whole company during short rests.
* Craven: Accounted for in `/intimidate`.
* Cunning Make and Ancient Cunning Make: Adjusted on the Google Sheets sheet (and automatically updates).
* Dark For Dark Business: Automatically applied in combats when the light level is set to darkness.
* Darken: -1d on all rolls; use `noartifact` to cancel this if the item is shrouded.
* Deadly Wound: `/foe attack` sets a flag on the protection test, that gets used during `/roll protection`.
* Deathless: Available in `/foe ability`.
* Defiance: During an `/end` of combat, recover Endurance (greater of Heart or Wisdom).
* Denizen of the Dark: Accounted for if you specify darkness in `/start` when doing a `/foe attack`.
* Desperate Courage: Add `courage` to a roll to spend Hope and also gain Shadow and be inspired.
* Dour-handed: Add +1 to the damage from a Heavy Blow, and +1 to the Feat die on a pierce.
* Dragon-slayer: Attack rolls are favoured against enemies with Might of 2 or higher.
* Dreadful Spells: Available in `/foe ability`.
* Dreadful Wrath: Accounted for in `/foe attack`.
* Drums in the Deep: Spend hate to increase Eye score.
* Dull-Witted: Accounted for with an option in `/intimidate`.
* Elvish Dreams: Treat `/company short` as `/company prolonged`.
* Endurance of the Ranger: All Journey fatigue skips you if you are wearing the right armor.
* Fearless: Accounted for in `/intimidate`.
* Fell and Superior Fell: Adjusted on the Google Sheets sheet (and automatically updates).
* Fierce: Available in `/foe attack`.
* Fierce Shot: Foe's protection tests are ill-favored on ranged attacks.
* Flame of Hope: If you hit in battle with this weapon, everyone in the company gains Endurance.
* Flame of Ud�n: If you hit the adversary, suffer an Endurance loss (use 1/2/3 points for moderate/severe/grievous, default 1).
* Foe-Slaying: Bane target's protection rolls are ill-favoured (if already ill-favored, autofails) (right now just displayed).
* Foul Dust: Spend Hate to inflict everyone in close combat with an Endurance loss (use 1/2/3 points for moderate/severe/grievous, default 1).
* Foul Reek: Available in `/foe ability`.
* Freeze the Blood: Available in `/foe ability`.
* Friendly and Familiar: Councils gain one for time limit, and your rolls are treated as Friendly.
* Furious: Protection and Attack rolls are favoured when you're wounded.
* Gleam of Resolve: Implemented when using attack on an adversary.
* Gleam of Wrath: Implemented when using attack on an adversary.
* Gleam of Terror: Strips Hate/Resolve on a hit.
* Great Strength: Add +2 to the damage from a Heavy Blow.
* Grievous and Superior Grievous: Adjusted on the Google Sheets sheet (and automatically updates).
* Hate Sunlight: Accounted for by initiative if sunlight is set in `/start`.
* Heartless: Accounted for in `/intimidate`.
* Herbal Remedies: +1d to Healing rolls, and an extra Healing roll during journey arrival recovery
* Hideous Toughness: Accounted for any time a foe is damaged; queues a protection test, then implements the consequences when that test is resolved with `/foe protection`.
* Hobbit-Sense: All Wisdom rolls are favored, and +1d to resist Greed.
* Horrible Strength: Available in `/foe ability`.
* Howl of Triumph: Available in `/foe ability`.
* Ill-luck: Eye rolls cause automatic failures as if miserable.
* Ill-omen: -1d to Council rolls.
* In Defiance of Evil: Strip 1 H/R on all enemies on a kill; attacks favoured against a foe out of hate/resolve.
* Keen and Superior Keen: Adjusted on the Google Sheets sheet (and automatically updates).
* Keening Wail: Synonym for Dreadful Spells.
* Lembas: -1 to fatigue gained during journey events. Prolonged tests count as safe for the whole company.
* The Long Defeat: Reduced Shadow recovery during fellowship phases.
* Malice: Cannot spend hope on rolls using this item.
* Many Arms: The adversary's available attacks equal its Might plus one, but then decrease by one with each Wound.
* Memory of Ancient Days: Affects rolls during journey events.
* Mithril Weapon: Can attack 1H even if seized.
* Mithril Shield: Still works after a 2H attack if a buckler.
* Orc-poison: Implemented automatically through the protection test.
* Over Dangerous Leagues: +1 recovery on `/roll arrival` or `/company arrival`.
* Poison: Implemented automatically through the protection test.
* Reinforced and Superior Reinforced: Adjusted on the Google Sheets sheet (and automatically updates).
* Rune-Scored Armour: Protection rolls are never weary or miserable.
* Rune-Scored Helm: Combat task rolls are never weary or miserable.
* Rune-Scored Shield: `/foe` attacks on you are made weary.
* Rune-Scored Weapon: Attack rolls with it are never weary and miserable.
* Shots In The Dark: Automatically applied in combats when the light level is set to darkness.
* Skin-coat: Add +1d to Protection rolls if in no or leather armor.
* Small Folk: Automatically applied when adversary sizes are not Small or Tiny.
* Snake-like Speed: The loremaster can use `/foe ability` to tell Narvi a foe automatically uses Snake-like Speed up to a certain hate reserve when it can.
* Spirit: Available in `/foe ability`.
* Splitting Blow: Handled whenever you attack an adversary.
* Stone-hard: All Protection tests are favored if not miserable.
* Stout-Hearted: All Valour rolls are favored.
* Straight Flight: Attacks with this weapon ignore complications.
* Strength of Will: +1d on Dread tests.
* Strike Fear: Available in `/foe ability`.
* Sure at the Mark: Ranged attacks are favored. Can use `stone` as a weapon name for a special thrown stone attack.
* Thick Armour: Available in `/foe protection`.
* Thick Hide: Available in `/foe protection`.
* Thing of Terror: Available in `/foe ability`.
* Tough As Old Tree-Roots: Choose better of two Feat rolls when determining wound severity. Double Strength when recovering during rests.
* Twice-Baked Honey-Cakes: Reduces fatigue from journey events.
* Untameable Spirit: +1d on Sorcery tests. (Hope bonus is handled on the sheet.)
* Virtue of Kings: Wisdom and Valour tests with hope spend are upgraded to inspired.
* Wind-Like Speed: All attacks against the adversary are ill-favoured.
* Yell of Alarm: Spend hate to increase Eye score.
* Yell of Triumph: Available in `/foe ability`.

The following will _not_ be implemented because they are too situational, not relevant to things Narvi handles, or already handled by adjustments made in the source character sheet:

* A Hunter's Resolve: Handled by commands for adjusting hope and endurance.
* Aquatic: Not worth having a flag for this when it comes up so rarely. Players can specify `feat` in `/attack` rolls.
* Art of Disappearing: Situational.
* Artificer of Eregion: Happens during fellowship phases.
* Baruk Khazad!: Players can add `favored` to roll commands.
* Beauty of the Stars: Adjusted on the Google Sheets sheet.
* Beorn's Enchantment: Players can add `magical` to roll commands.
* Bree-blood: The loremaster sets the `/pool` value.
* Bree-pony: Update Hope and Vigour on the Google Sheets sheet.
* Broken Spells: Players can add `magical` to roll commands.
* Combat Sorcerer: The loremaster can track whether the foe's actions are used up and use `/next confirm`, and refund the hate cost with `/foe gain`.
* Confidence: Adjusted on the Google Sheets sheet.
* Curse of Weakness: Gives you the effect of a Shadow Path.
* Deadly Archery: Player can just do a `/prepare` and also a roll for another action.
* Drown: Not worth having a flag for this when it comes up so rarely. Use `/foe lose` to spend hate and `/damage` for the effects of suffocation.
* Durin's Way: The loremaster can add `-2` to `/foe` commands (equivalent to +2 parry).
* Dwarf-friend: Narvi doesn't know about fellowship focus choices.
* Dwarves of Durin's Folk - Naugrim: Affects character creation.
* Dwarves of Durin's Folk - Redoubtable: Already handled by the Google Sheets sheet.
* Elbereth Gilthoniel!: Add Hope on the sheet, and add `inspired` to roll commands.
* Elf-lights: Situational.
* Elven-Skill: Players can add `magical` to roll commands.
* Elven-Wise: Players can add `magical` to roll commands.
* Fear of Fire: Depends on both engagement and holding a torch; can be done with `/foe lose`.
* Foresight of Their Kindred: Handled by loremaster fiat.
* Forest Harrier: Player can add `inspired` during opening volleys.
* Ghost Bird's Nest: Not worth having a flag for this when it comes up so rarely. Players can specify `feat` in `/attack` rolls, and loremasters in `/foe attack` rolls.
* Ghost Bird's Speed: Not worth having a flag for this when it comes up so rarely. Use `/foe lose` to spend hate, and players can specify feat and bonus in `/attack`.
* Great Leap: Narvi does not limit attacks by stance; use `/foe lose` to spend hate.
* Hardiness: Adjusted on the Google Sheets sheet.
* Hatred: No way for Narvi to know who it should affect (sometimes it's things that aren't on the character sheet, like 'bearer of the dagger'). Use the feat option in `/foe attack`.
* Heir of Arnor: Handled by adding gear to the character sheet.
* High Destiny: Since it only happens once, this is handled manually.
* Hollow Steel: Allows another roll during opening volleys.
* Hound of Mirkwood: Situational (depends on presence of hound).
* Hunted: You draw attention from a particular kind of enemy.
* Kings of Men: Affects character creation.
* The Language of Birds: Players can add `inspired` to roll commands.
* Luminescence: Comes up only when determining ambush, and in narrative.
* Master & Commander: Not worth having a flag for this when it comes up so rarely. Use `/foe lose` to reduce rounds of pushback.
* Mastery: Adjusted on the Google Sheets sheet.
* Might of the Firstborn: Adjusted on the Google Sheets sheet.
* Mithril Armour: Adjusted on the Google Sheets sheet (and automatically updates).
* Natural Watchfulness: Players can add `magical` to roll commands.
* Nimbleness: Adjusted on the Google Sheets sheet.
* Owned: Use `noitem` when you're in the presence of the enemy or type of enemy.
* Prowess: Adjusted on the Google Sheets sheet.
* Royalty Revealed: Player can do `/rally` and continue with another action; everyone can add `inspired` to rolls.
* Shadow Taint: Simply give the character more Shadow.
* Skill of the Eldar: Adjudicated by the loremaster.
* Staunching Song: Roll Song instead of Healing situationally.
* Strange As News From Bree: Used during fellowship phases.
* Telchar's Secret: Add the items to the Google Sheet as usual.
* Three Is Company: The loremaster sets the `/pool` value.
* Unnatural Hunger: Not worth having a flag for this when it comes up so rarely. Use `/encounter` commands to change the Beast if this comes into play.
* Ways of the Wild: Players can add `magical` to roll commands.
* Weakening: Adjust a TN on the Google Sheets sheet.
* Wood-goer: Situational.
* Wooden Stilts: Not worth having a flag for this when it comes up so rarely. Narvi does not limit what stance a foe can attack into, or limit foes escaping.

# Customization

Narvi has several functions to help you customize it for your preferences and needs.

## Language

As of this writing, Narvi has been translated into French. If your language has been translated, Narvi will automatically talk to you in that language, based on your Discord language setting.

If you are seeing only English, and would like to volunteer to translate to another language; or if your language is not fully translated, or poorly translated, and you'd like to volunteer to take over that language; please contact the developer (see **Getting Support** below) to volunteer to help.

## AutoSwitch

If you are in multiple games, it can be a pain to keep switching servers and doing commands and realizing you're in the wrong game. By using `/autoswitch true` you tell Narvi that for you (this is per user), every time you type a command, Narvi should consider automatically switching games for you. It will do so if you are typing a command other than `/game`, and you're on a server that has one and only one game created on it, and that's not already your selected game.

## SaveLast

Narvi lets you type `/last` to repeat your last command.

If you don't like the idea that Narvi records your last command, type `/savelast false` and Narvi will stop. As a result, Narvi no longer stores anything you type (though it will still store things you tell it to, like your character stats). This will mean you can't use the `/last` command. Type `/savelast true` to turn the default behavior back on. See the [Privacy Policy](https://bitbucket.org/HawthornThistleberry/narvi/src/master/Privacy.md) for why this is a thing.

## Game Configuration

Advanced loremasters can use a configuration control to set up special thing about how Narvi implements the rules in their game.

* `/game config` to turn on a configuration flag from the list below.

All of them default to false. The following are the flags Narvi knows (this list will no doubt grow during coming updates):

* `highest-knockback`: `/knockback` refunds half of the damage of the _biggest_ attack so far this round, not the _most recent_ attack.
* `allow-skirmish`: `/stance skirmish` and `/gainground` are allowed in 2e games that are not Strider Mode.
* `cram-nostack`: Only one party member, the one with the highest Wisdom, can contribute a Cram bonus to `/company short` endurance recovery. By default, multiple Cram users stack their bonus.
* `deathless-loss`: The fell ability Deathless counts as a loss of hate, not a spend, for counting how much hate has been spent.
* `default-noitem`: All dice rolling defaults to `noitem=true` unless set to false.
* `journey-mention`: When displaying Journey events, Narvi will @mention the players being called on to do the roll, if it can.
* `journey-weather`: Always roll a weather event and display it before any marching test.
* `journey-tiring`: Fatigue from a journey is gained immediately, not at the end.
* `combat-mention`: When displaying Combat initiative, Narvi will @mention the players being called on to do the roll, if it can.
* `show-eye`: Show the Eye status and changes publicly; if not, Narvi will DM the loremaster.
* `roll-eye`: Automatically increment Eye when an Eye is rolled out of combat
* `magical-eye`: Automatically increment Eye when `magical` is used on any roll
* `eleven-can-pierce`: Those Favoured by the Grey Wizard treat 11s as 1s but can still pierce with enough tengwar (see below).
* `eleven-pierces`: Those Favoured by the Grey Wizard treat 11s as a piercing blow.
* `hide-pierces`: When a hero gets Tengwar, Narvi hides the Pierce button if there's already a piercing blow, and marks it with an X instead of a target if they don't have enough Tengwar to reach edge. With this set, it hides the button even in the latter case.
* `always-explain`: Append an `/explain` to every roll.
* `unlocked-counters`: Players can use the `/counter` command to set, not just see, counters (but not secret ones).
* `unlocked-commands`: Players can do all commands as if they were loremasters; used, for instance, in a two-player Strider Mode game.

You can also designate a channel where private messages to the LM go (by default they go to private messages). This can be handy because you can keep everything about a game in that game's server, especially if you are in multiple games. This will be especially useful when you use adversaries, since you'll get a lot of secret messages about damage to them, etc. To do this, create a channel, configure it so those pesky players can't see it, then go to it and type `/game lmchannel` and choose the **Here** option.

### Favoured by the Grey Wizard

There are some odd interactions of the lifepath "Favoured by the Grey Wizard" (which makes it so feat die rolls of 1 count as 11) and piercing blows, and Narvi has some configuration entries to control them. Basically there are three ways of looking at the rules:

* Michele Bugio gave the official interpretation on the Free League forums in March of 2023. Basically, it means this: if you are Favoured by the Grey Wizard, and you roll a 1 (that is, an 11), not only do you not get a piercing blow, you can _never_ get a piercing blow on that roll, no matter how many Tengwar you have. This is markedly distinct from and asymmetrical to everything else: for every other possibility, whether you can get a piercing blow derives directly from the mathematics of what you rolled and what kind of weapon you have, but this introduces an exception where that mathematics no longer applies. I dislike this ruling. Sure, it's rare that you'd have enough tengwar with the right weapon to come back from a 1, but why prevent it for those favoured by the Grey Wizard while allowing it for everyone else? Nevertheless, since this is the official ruling, it is Narvi's default behavior.
* The interpretation I favor is this: "The Feat die rolling a 1 is not changed to an 11 when considering piercing blows". In this case, persons favoured by the Grey Wizard get their piercing blows exactly the same as everyone else, no more and no less. To use this interpretation, tell Narvi `/game config eleven-can-pierce`.
* Or you can go with the most generous interpretation: the Feat die rolling 1 coming up as 11 counts as 11, and thus, those favoured by the Grey Wizard get significantly more piercing blows (for a typical weapon, it increases the odds from 2/12 to 3/12). If you like this interpretation, `/game config eleven-pierces`.

## Dice Styles

By default Narvi shows all the dice you rolled and their results, but there are other dice styles for people who prefer more compact displays, or who find Narvi's display unclear. For most rolls, there's no difference; this only comes into play when there are favored rolls, or other things where multiple feat dice are rolled; and when there are mastery rolls, when extra success dice are rolled (this is a 1e mechanic, not yet used in 2e but still supported by Narvi).

In the default dicestyle `full`, the feat dice will show as follows in a favored roll:

* {6 9} 9

indicating that you rolled a 6 and a 9, and the 9 is the one that counts. The stuff inside brackets is not part of the final roll, but just provided informationally. In the same way, the success dice, when mastery rolls are used, are sorted and have the rolls that were discarded inside brackets:

* 5 3 3 {1}

Put this together, and a roll that is both favored and has mastery dice might look like this:

* {6 9} 9 . 5 3 3 {1}

It's easy to tell both what the actual dice read, and what the total is -- it's the numbers not in brackets, 9 5 3 3, that get added up to make a total of 20.

Here are the other dice styles, the loremaster can set with `/game dicestyle <style>`:

* `concise`: Show only the dice that contribute to the final result. Dice that were rolled and discarded are not displayed. (Be sure to explain this to your players or they will start objecting 'but wait, my roll was favored!' and think they did the command wrong.) In the above example, the display would be 9 5 5 3.
* `rolled`: Show everything that was rolled, exactly as they were rolled, and let the players figure out the results. Narvi still shows totals, compares to target numbers, etc. but the dice display does not show any of that work. In this case, you would see 6 9 3 5 1 3.
* `sorted`: Just like `rolled` but the success dice are always sorted, even if no mastery dice are involved. 6 9 5 3 3 1.
* `nobrackets`: This style is internally inconsistent and thus hard to explain. For the feat dice, we show all the feat dice rolled, without any guidance about which ones count, as in `rolled`. However, in the success dice, we show only the outcome, as in `concise`. The resulting display is a bit hard to read because of the inconsistency, but it has the virtue of avoiding parentheses. In this example, it would be 6 9 5 3 3.
* `full`: The default described above.

## Macros

You can save commands to be executed later exactly the same way, including doing a series of commands together; this is called a macro. Your macros are defined for a user, not for a game, so you could use the same macros in different games. Macros can include any slash commands. Note that you have to have SaveLast turned on for macros to work.

To create a single-command macro:

* Do the command.
* `/macro create <name>`

To create a multiple-command macro:

* Do the first command.
* `/macro create <name>`
* Do the second command
* `/macro append <name>`
* Repeat... within reason. Discord puts limits in how much a bot can output in a single burst, so if you stack several commands and they have lengthy output, the resulting display may be throttled.

To run the macro you created:

* `/macro run <name>`
The output of the command, or multiple commands, will all be output together (but see note about throttling above).

You can also manage your macros:

* `/macro list`
* `/macro delete <name>` (unlike `append` and `run`, `delete` will require you to type the full macro name, and not do partial matches, to avoid accidental deletions)

Because of the design of slash commands, it's impractical to make parameterized macros. What's a parameterized macro? Say you make a macro to roll an `/attack` -- it has the parry baked in, and you can't make it so the next time you run it, you put in a different parry. Every time a macro runs it does _exactly_ what you did last time.

## Other Character Repositories

As of this writing, Google Sheets is the only known repository for 2e characters. However, at least one other (tentatively named Blackroot Vale) was being developed, and another (called TOR Companion) is in development; and Narvi is ready and eager to tie into it and any others that come along, provided they have the data needed, and an API that lets Narvi read and write. Should this come to pass, the the `/character create` and `/character key` commands would have you specify `googlesheets` first, so in some future version, other names may go there for other repositories. If you're interested in developing one, please reach out!

# Getting Support

The first thing to check is this guide, and the others linked at the top. If that's not making something clear, by far the best way to get support for Narvi is to join the TOR/LOTR:RP discord at https://discord.me/theonering and particularly to visit the **#narvi** channel. The developer is almost always active there.

To send a message directly to the developer, you can also use this command in Narvi:

* `/suggest`

At this time, the developer is not looking for anyone to help with the coding of Narvi, but is always interested in hearing your ideas for things that should be coded into Narvi.

# Privacy Policy

See https://bitbucket.org/HawthornThistleberry/narvi/src/master/Privacy.md for Narvi's privacy policy. In brief, though, Narvi only stores what you tell it (games, characters, counters, macros, etc.), and the Discord IDs of the owners of that content (e.g., loremasters for games, players for characters) and of the server the content lives in. It only shows this information to those people (except that a loremaster can see the characters of all players in her game), except for a few debugging diagnostic things the software developer can do at need.

# Credits

Thanks to Jacob (Zed) Rogers, who gave me the starting code and encouragement; Eris and its author abalabahaha, and discord.io and its author izy521, for the Discord connection libraries; the testers in CircleGrove, the TOR/LOTR:RP discord, and my Laughter of Dragons game, for testing, patience, and ideas; people in the discord.io discord (Mimee, Cadence, and Mr Pool) for help with keeping up with Discord's many API changes; Haladeen for translation; and anyone who's contributing to the ko-fi to host Narvi in the cloud.
