# Version History

* **v1.0.0** 2020-03-15: Initial release.
* **v1.0.1** 2020-03-19: Use the user's nickname if set.
* **v2.0.0** 2020-03-21: Added token tracking.
* **v2.0.1** 2020-03-22: Changed shadow token to brown so it's more visible on dark theme.
* **v2.0.2** 2020-03-22: Fixed a problem with the `use` command.
* **v2.1.0** 2020-03-22: `\` as a synonym for `!tor`; `\last`; fixed bug in `\use pool`.
* **v2.1.2** 2020-03-22: Added `\bonus` command; fixed bug in swapping token and number; fixed auto-success.
* **v2.2.0** 2020-03-22: Added `\change` command for loremasters to do deltas for others.
* **v2.3.0** 2020-03-22: Changed `\last` to be your last command, regardless of what others have done.
* **v2.5.0** 2020-03-23: Added maxhope and maxendurance; only endurance can be negative; display conditions, restore command.
* **v2.6.0** 2020-03-23: Added mastery dice.
* **v2.7.0** 2020-03-25: Added autoweary, `notweary` option.
* **v2.8.0** 2020-03-27: To support a better way my computer hosts this, added a command only for me.
* **v3.0.0** 2020-03-29: Major overhaul. Now supports multiple games. All token tracking moved to within games.
* **v3.0.1** 2020-03-30: Some more error checking and logging.
* **v3.0.2** 2020-03-30: More notifications. Start replacing some Mee6 functionality in TOR/AiME discord.
* **v3.0.3** 2020-03-30: Mee6 replacement is finished but not yet tested.
* **v3.1.0** 2020-03-30: Mee6 replacement is finalized.
* **v3.1.1** 2020-03-30: Changed timing on reconnection and server join notifications. Added `\suggest` command.
* **v3.2.0** 2020-03-30: Better server list handling and announce; roll comments.
* **v3.2.2** 2020-04-01: Fixed an addToRole command's syntax.
* **v3.2.4** 2020-04-04: Fixed a bug in determining miserable status. What was I thinking?
* **v3.3.0** 2020-04-04: Added a `\secret` command for LMs for things like Eye Awareness; `\news` for what's new
* **v3.4.0** 2020-04-05: Bug fixes; more token abbreviations; find names by substring; `\damage` and `\heal` commands; auto lm rolls; role removal.
* **v3.4.1** 2020-04-05: For very large number of endurance tokens, use half-sized circles to make it fit.
* **v3.4.2** 2020-04-06: `\game rename`.
* **v3.5.0** 2020-04-06: `\treat` command, automatic recovery of treated wounds.
* **v3.5.1** 2020-04-06: Error checking on treating wounds.
* **v3.5.2** 2020-04-15: Merge multi-word game names into a single word.
* **v3.5.3** 2020-04-16: Split `\help tokens` off from the main `\help` which was getting a bit too long.
* **v3.6.0** 2020-04-19: `\r` as synonym for `\roll`; counter values; bug fixes; more half-sized circles; `\wound` command; `\report damage`; `\eye`
* **v3.6.1** 2020-04-20: Fixed a bug with some commands when not in a game.
* **v3.6.2** 2020-04-21: Now stripping < and > from commands for people unfamiliar with Backus-Naur form.
* **v4.0.0** 2020-05-02: Rewrite to use Eris instead of discord.io. No functionality changes.
* **v4.1.0** 2020-05-03: Roll comments now prefaced with `\`; `\secret` now answers in private message; bug fixes; `\status report` and `\damage report` now synonyms for `\report damage`; `tn` without a value defaults to 14
* **v4.1.1** 2020-05-05: Roll up to 6 feat dice and take the best
* **v4.2.0** 2020-05-10: Status message; can now roll simple D&D dice
* **v4.3.0** 2020-05-11: Store heart and do `\recover`
* **v4.3.2** 2020-05-17: Added Adv and Dis dice for more AiME support
* **v4.4.0** 2020-05-18: Added emoji showing which are feat and which are success rolls
* **v4.4.1** 2020-05-24: Fixed a quirk in the `\suggest` command from servers I'm not in
* **v4.4.2** 2020-05-31: Fixed minor bugs about dice roll type emojis
* **v4.4.4** 2020-05-31: Autoswap on `\dmg` and `\heal`
* **v5.0.0** 2020-07-02: User commands
* **v5.0.2** 2020-08-04: New TOR/AiME discord roles
* **v5.0.3** 2020-08-10: Non-TOR dice no longer case sensitive; synonyms in `\cmd` syntax
* **v5.1.0** 2020-08-23: A quick Expanse dice hack
* **v5.2.0** 2020-11-03: A way to choose a dice face style
* **v5.2.1** 2020-11-08: Fixed a problem with privileged intents and member lists; autoselect of games respects being a loremaster
* **v5.3.0** 2021-01-24: Added `\ap` for advancement point pip tracking
* **v5.3.1** 2021-01-31: Restructuring of user name checking for privileged intents since the bot is verified
* **v5.3.2** 2021-01-31: Added `\report ap`
* **v5.3.3** 2021-01-31: Internal change -- move heart to a sheet structure
* **v5.4.0** 2021-02-03: The basic `\sheet` command (not yet with weapons)
* **v5.5.0** 2021-02-03: `\roll <skill|test>` works, with all skills, Wisdom, Valour, Armour, and more.
* **v5.6.0** 2021-02-04: Quick sheet setup commands
* **v5.6.1** 2021-02-04: `\report combat`
* **v5.7.0** 2021-02-04: `\sheet weapon`
* **v6.0.0** 2021-02-04: `\roll <weapon>`
* **v6.0.1** 2021-02-07: Error checking on rolls by word, ap <0 or >3
* **v6.1.0** 2021-02-08: Error checking on sheet values
* **v6.1.1** 2021-02-08: Fixed bug in AP error checking
* **v7.0.0** 2021-02-09: Added `\stance`, `\start`, `\next`, `\knockback`, `\actions` for initiative handling.
* **v7.1.0** 2021-02-10: `\clear` can now do knockback, endurance, wounds, and damage.
* **v7.2.0** 2021-02-12: Removed all the Mee6-killer stuff since Discord has nerfed intents so hard.
* **v7.2.1** 2021-02-13: Some 2e preparations: changed to tinco, added roll synonyms for favored
* **v7.2.2** 2021-02-13: Bug fixes; `\clear ap`; prevent tokens going negative (except endurance)
* **v7.3.0** 2021-02-14: `\roll bonus=<number>` for adding or removing success dice to a skill/test/weapon roll
* **v7.4.0** 2021-02-14: `\role` and `\report journey`
* **v7.5.0** 2021-02-14: Added a lot of auto error correction for common typing errors (more as I see them)
* **v7.6.0** 2021-02-15: Favored skills
* **v7.7.0** 2021-02-16: Removed `\setup` and `\change`, standardized LM version of all token commands
* **v7.7.1** 2021-02-20: First steps to splitting up file; many bug fixes; more auto error corrections
* **v7.7.2** 2021-02-21: Recognize `\awe` etc.; prevent stance change if knocked back; new `absent` stance if not in combat; rolling bonus dice automatically decrements (or warns); `\recover rally <amt>`
* **v7.8.0** 2021-02-21: `\bonus <num>`, removing bonus tokens, `\foe` command
* **v7.8.1** 2021-02-23: Still more work for Narvi to guess incorrect syntax (this time, `\axe` instead of `\r axe`)
* **v7.8.2** 2021-03-06: `\sheet skills`
* **v7.8.3** 2021-03-06: Bug fixes with weapon updates, **Persuade** rolls, `\spend hope`
* **v7.8.4** 2021-03-06: More DWIM logic (`\<stance>` is `\stance <stance>`). `\report <skill/stat>`. 
* **v7.9.0** 2021-03-07: `\sheet weapon order` and `\attack` commands
* **v7.9.1** 2021-03-08: More separation into files. Also gave up on doing most of it, it would be a major redesign.
* **v7.9.2** 2021-03-08: First i18n steps, more aliases, DWIM for macros
* **v7.9.4** 2021-03-20: Fixed a number of i18n bugs; added `\ap <skill> <type>`
* **v7.9.5** 2021-03-21: Changed `mastery` to the number of mastery dice (so it works with bonus dice)
* **v7.9.6** 2021-03-27: Internationalization coding is done and mostly tested. `piglatin` created for testing purposes (no really) while I wait for translations to be done. Once I have at least one real translation, we'll call that v8.0.0.
* **v7.9.7** 2021-03-28: Most of the reports are rewritten to use a module that dynamically allocates column widths so they will work even with languages that use wider or narrower titles or words.
* **v7.9.8** 2021-04-03: More DWIM logic (`\r attack`), and fixes on headings on `\report combat`.
* **v7.9.9** 2021-04-04: Better parsing of `\r` command -- tests and skills can go anywhere, not only first
* **v8.0.0** 2021-04-08: Internationalization is now live with the debut of Portuguese.
* **v8.0.1** 2021-04-08: Assorted internationalization bug fixes with parsing and identifying parameters.
* **v8.0.2** 2021-04-09: More internationalization bug fixes.
* **v8.0.3** 2021-04-11: `\clear ap` by a player does `\ap clear`. Fixed problem with some stats in `\sheet`.
* **v8.0.4** 2021-04-17: `\hope` is DWIM for `\use hope`; fixed `\start adversary`; fixed DWIM of tests, weapons, macros
* **v8.0.5** 2021-04-20: Italian translation.
* **v8.1.0** 2021-04-22: Added `encumbrance` and `fatigue` tokens and all support required; changed weary calculation accordingly.
* **v8.1.1** 2021-04-23: Added `\report fatigue` for a display of encumbrance, fatigue, and weariness
* **v8.1.2** 2021-04-23: More DWIM for synonyms of sheet elements in the `\sheet` command (e.g., `armor`)
* **v8.1.3** 2021-04-29: French translation.
* **v8.1.4** 2021-05-01: `\sheet <name>` by a player works if it's their name. `\report hope` includes the pool. `\clear bonus` does not make a grammar error.
* **v8.1.5** 2021-05-02: Better splitting of large outputs; `\attack tn=12` works; `\axe 12` works (assumes it's a TN).
* **v8.2.0** 2021-05-08: `\prefix` command.
* **v8.2.1** 2021-05-12: Fixed a bug with Loremasters using `\sheet` to work with player sheets.
* **v8.2.2** 2021-05-13: `\ap <skill> <success> [trait]`
* **v8.2.3** 2021-05-13: `\eye <value>`
* **v8.2.4** 2021-05-16: DWIM logic if people use `\set` when they should have used `\sheet`.
* **v8.3.0** 2021-05-19: Added the `enhearten` token and associated logic
* **v8.4.0** 2021-05-29: Added the `\rollas` command
* **v8.4.1** 2021-06-01: Admin command for purging unused games
* **v8.4.2** 2021-06-06: Associate games with servers; delete games if kicked out of a server; DWIM on `\ap` command, typing `+` instead of `=`in rolls, putting targets on `\attack`
* **v8.4.3** 2021-06-13: `\recover hope`; `\sheet weapon <name> <skill>` without the other parameters; `\sheet <weapon>` DWIM; `\sheet armor 3d` DWIM.
* **v8.4.4** 2021-07-04: Returned to using the Tengwar rune instead of Tinco. Now using the red runes from 2e for tengwar, Eye, and Gandalf.
* **v8.4.5** 2021-07-05: Raised limit of bonus dice to 99, feat to 12, mastery to 12 due to 2e rendering some past limits moot.
* **v8.5.0** 2021-07-07: 2e changes: Load, `\game edition`, `\roll <type> hope|inspired`, full shadow ill-favored, no stripping of bonus in 2e, Dread/Greed/Sorcery
* **v8.5.1** 2021-07-08: 2e changes: `\set` for all sheet setting, `\recover rally` is 1e only, `hope` and `inspired` in `\roll` are 2e only, Enhearten and Bonus tokens 1e only, can now `\gain` and `\lose` numeric values on sheets too, can now `\set`/`\gain`/`\lose` <weapon> without the word `weapon`
* **v8.5.2** 2021-07-08: 2e changes: `\use pool` is 1e-only; `\recover` is now primarily `\company` since it will also be used for non-recovery things in future; new `\company` options for `short`,`song`,`safe`; `\end` command to end combat that will later handle some after-combat activities
* **v8.5.3** 2021-07-09: bug fix for when feat=best has two eye rolls (or two gandalf rolls as LM)
* **v8.5.4** 2021-07-10: `\company fellowship` and `\company fatigue`
* **v8.5.5** 2021-07-12: `\company song` now automatically does **Song** rolls; `\company arrival`; Strength, Enhearten, and Scan now known as synonyms for Body, Inspire, and Search (but the 1e sheets still show the 1e skill names and positions)
* **v8.5.6** 2021-07-14: `\action intimidate|rally|protect|prepare`
* **v8.5.7** 2021-07-16: Help and stance action lists for `\action`
* **v8.5.8** 2021-07-17: `\pool distribute`
* **v8.5.9** 2021-07-18: `\pool {<char>=<amount>}`
* **v8.6.0** 2021-07-19: Fixed `remove` as a synonym for `delete` in `\sheet weapon` command
* **v8.6.1** 2021-07-20: Widened some validation checks on sheet values due to higher and lower values in 2e; added some sanitizing on loremaster names; some internal changes preparing for integration with Blackroot
* **v8.6.2** 2021-07-20: Lots of internal changes preparing for Blackroot integration
* **v8.6.3** 2021-07-20: More Blackroot internal changes
* **v8.6.4** 2021-07-21: New dice images; `dicetype` eliminated; Sauronic dice for LM rolls.
* **v8.6.5** 2021-07-23: Dice rolls are in embeds now.
* **v8.6.6** 2021-07-24: Set image for dice roll embeds. Start of work for Blackroot Vale API. Game edition change warnings. Show game edition on lists.
* **v8.6.7** 2021-07-24: Fixed bugs with tokens, initiative
* **v8.6.8** 2021-07-25: Subtle bit of DWIM: `ill favored` will be recognized as a synonym for `ill-favored`. Ditto for any other hyphenated parameters in any language.
* **v8.6.9** 2021-07-26: Genericized references to the character repository. Dice now in a separate message for full sized images.
* **v8.7.0** 2021-07-29: Precautions for joining problematic servers; `\dmg [char] <amt> [knockback]`; `\set stance|role|knockback`
* **v8.7.1** 2021-07-29: Weapon roll bug fix; `\songbook` command
* **v8.7.2** 2021-07-30: Remaining songbook functions in `\company fellowship` and `\company song [title]`
* **v8.7.3** 2021-07-30: Dice styles (`\game dicestyle <style>` supporting `full`, `concise`, `rolled`, and `nobrackets`)
* **v8.7.4** 2021-07-31: Fixed a bug with some dice styles when only one feat die is rolled.
* **v8.7.5** 2021-08-04: Fine, fine, I am taking out "Auto-failure". It's less of a thing in 2e anyway.
* **v8.7.6** 2021-08-05: Fixed some bugs with \sheet, min values, and spacing.
* **v8.7.7** 2021-08-07: Some defensive coding for handling large messages. Endurance recovery shows resulting status.
* **v8.7.8** 2021-08-17: Added `magical` option in rolling for magical successes
* **v8.7.9** 2021-08-18: Added check to prevent adding yourself to your own game; fixed some error messages; made the game add command make more clear what game you're adding people to.
* **v8.8.0** 2021-08-21: Fixed a bug with setting or adding to skills
* **v9.0.0** 2021-08-30: The `\character` command for multiple characters, LM characters
* **v9.0.1** 2021-08-30: Bug with weapon DWIM and character zero; some data file cleanup
* **v10.0.0** 2021-09-21: Google Sheets integration; tons of 2e functions built (many more to come)
* **v10.0.1** 2021-09-22: Fixed bug in \sheet key; fixed bug showing unloaded sheet; game select substring match; Auto-failure is back! (on miserable eye roll); wound command handles 2e wounds
* **v10.0.2** 2021-09-23: Fixed Google authorization for writing to sheets
* **v10.0.3** 2021-09-25: Better more helpful error messages when people have trouble getting their sheets to load.
* **v10.0.4** 2021-09-28: Fixed wound bug in 2e `\sheet`; added `\treat <num>` in 2e; added `\treat <person>` in 2e; fixed `\clear wounds`
* **v10.0.5** 2021-09-30: Revised the authorization used by Google API to no longer depend on expiration-prone tokens.
* **v10.0.6** 2021-09-30: Prevent duplicate character creates; show useful items on reports of skills; 2e report for internal use
* **v10.1.0** 2021-10-03: Wounds treatment via updated Google Sheets; removed `\company song` for now; automatically works with passing the entire Google Sheets URL; company prolonged handles healing countdown
* **v10.1.1** 2021-10-03: `\sheet items`; `\company arrival` accounts for steed Vigour; `\r unfavored` to override favor; warning of defensive stance dice; Songbook uses the release song types
* **v10.1.2** 2021-10-09: Several error catching steps to ensure that Google Sheets problems (like giving the wrong key or skipping the permissions step) get the error to the right place, and do not create a situation where Narvi keeps slamming the API and my error log
* **v10.1.3** 2021-10-10: Better error messages on bad keys, prechecks on keys to save API calls
* **v10.1.4** 2021-10-12: Rally Comrades chooses bonuses based on the next round, not current location
* **v10.1.5** 2021-10-16: Fixed 2e songbook types in help; prevent setting non-skills favored; fixed problem with token commands outputting values over 25; DWIM for `\r bonus`, and `\sheet <char> <command>` by players
* **v10.1.6** 2021-10-17: Fixed a bug with wounds making a character unconscious; `\set image` works; `\company rally` reports correct values after the rally in 1e games
* **v10.1.7** 2021-10-19: Fixed a bug with using the whole URL of a Google Sheets sheet instead of just its key
* **v10.1.8** 2021-10-22: `\char <name>` should work if you have no character selected; `\foe` will show 'Adversary' as rolling, not your currently selected character; `\company enhearten` (1e) does not have a missing word in its header anymore
* **v10.1.9** 2021-10-23: `\company ap|sp|treasure <amt>`
* **v10.2.0** 2021-10-25: Fellowship and Yule recovery of hope and shadow reflect special rules for Rangers, Elves, and High Elves; added `\company heal <amount>`.
* **v10.2.1** 2021-10-29: Slowed Narvi's first pull of sheets when it starts up to avoid hitting API limits; fixed some error logging.
* **v10.2.2** 2021-11-04: Added `\company shadow`.
* **v10.2.3** 2021-11-05: Fixed bug in not reflecting "shadow has reached maxhope" (ill-favored); fixed bug where it would decide you had done that when you hadn't (good thing those bugs came together!); further slowed Google Sheets API hit rate; DWIM for char and game commands out of order; fixed a bug in 2e calculating TN on `\foe` rolls; fixed a flaw in `\sheet compact` for 2e
* **v10.2.4** 2021-11-06: Fixed bug in reading armor and shield values when sheets had undefined cells
* **v10.2.5** 2021-11-10: Added `\game rename` command (how did I miss that?).
* **v10.2.6** 2021-11-11: Added `\report traits` command.
* **v10.2.7** 2021-11-12: Systemic redo of handling numeric values that should prevent various hard-to-track bugs (and probably create a few more); fixed transparency on some emoji.
* **v10.2.8** 2021-11-10: Fixed a bug in `\company sp`.
* **v10.3.0** 2021-11-22: Revised 2e attacks includes `\attack <parry> <weaponname>`, `escape`, `non-lethal`, `1h`, `2h`, handling of split injury weapons, spending tengwar on heavy/fend/pierce/shield
* **v10.3.1** 2021-11-23: Added `\spend other` and `\report rewards`
* **v10.3.2** 2021-12-01: Added `\game mode`; `\kb` by itself sets knockback; Narvi automatically does knockback refunds
* **v10.3.3** 2021-12-01: `\foe` automatically ignores shields if the hero's last attack was 2h, and accounts for number of engaged enemies when the hero is in defensive stance based on how many `\foe` rolls targetted them; use `\foe <hero> engage [num]` to simply set or add engagement; `\report stance` now shows engagement
* **v10.3.4** 2021-12-04: Heroes who are knocked back can change stance in 2e. Who knew?
* **v10.3.5** 2021-12-05: Added `\autoswitch` command
* **v11.0.0** 2021-12-07: Added support for a large number of virtues and cultural blessings
* **v11.0.1** 2021-12-09: Restored `\use pool` in 2e thanks to some patron abilities allowing direct spending of the pool; added `stone` as a weapon for those with Sure At The Mark
* **v11.0.2** 2021-12-12: `\stop` as synonym for `\end`; cannot `\start` when a combat is active; `\clear hope`; in 1e shows any available bonus dice when an action comes up; `\end` clears bonus dice in 1e; `\report weapons [type]`
* **v11.0.3** 2021-12-14: Fixed a bug that interpreted characters as miserable when their stats hadn't been set up, causing some autofailures inappropriately
* **v11.0.4** 2021-12-20: `\pool distribute` gives extra weight to anyone with zero hope
* **v11.0.5** 2021-12-22: `\savelast on|off` to opt out of having your `\last` command saved
* **v11.0.6** 2021-12-26: `\use pool hope` to transfer one point from the pool to your hope
* **v11.0.7** 2021-12-27: `\game reassign <character> <player>` to move a character to another player
* **v11.0.8** 2021-12-30: Possibly fixed an unreplicable bug with token commands giving a spurious error message
* **v11.0.9** 2021-12-31: Fixed one missed change from alpha to release 2e in protect companion task outcome
* **v11.1.0** 2022-01-03: Added Coyote & Crow dice, and documented other games in OtherGames.md.
* **v11.1.1** 2022-01-14: Emoji for d6s; placeholder emoji for most other non-TOR/non-C&C dice
* **v11.1.2** 2022-01-17: Added `\report skills`
* **v11.2.0** 2022-01-18: Attack rolls with tengwar have clickable emoji reactions for spending tengwar
* **v11.2.1** 2022-01-19: Added `\report roles`
* **v11.2.2** 2022-01-19: Fixed a bug in DWIM logic doing rolls
* **v11.2.3** 2022-01-23: `\game select none`
* **v11.3.0** 2022-01-24: More flexible dice rolling for regular polyhedra, more faces
* **v11.3.1** 2022-02-01: `\char reassign` synonym for `\game reassign`; better handling of transferring characters with lost owners; fixed bug with Google Sheets API on fetching formula values for sheets that haven't been opened recently
* **v11.3.2** 2022-02-16: Added `\r brawling`
* **v11.4.0** 2022-02-24: Added `\weather` command using Goblin Henchman's Hex Weather Flower.
* **v11.4.1** 2022-03-04: Expanse and Serenity rolls are in an embed and use character name and image.
* **v11.4.2** 2022-03-07: Serenity group rolling.
* **v11.4.3** 2022-03-21: Some Expanse Churn-handling helpers.
* **v11.4.4** 2022-03-30: Fixed `\action` handling LM characters; added `noitem` to all `\action` rolls.
* **v11.4.5** 2022-03-31: Allowed LMs to do `\action <character> [...]` to do actions on behalf of others.
* **v11.4.6** 2022-04-25: Fixed bug with 2e prolonged/safe rests and untreated wounds doing 1 instead of Strength.
* **v11.5.0** 2022-05-01: Added a function to help police the resources channels of the TOR/AiME discord; custom prefix overrides forward-slash to avoid conflict with other bots
* **v12.0.0** 2022-05-04: Added `\council` command and associated `\roll` options; fixed a bug with 1e legacy characters in 2e games not automatically getting a TN
* **v12.1.0** 2022-05-11: Added support for lifepath in Google Sheets sheet, and the favoured lifepath event making feat dice come up 11
* **v12.1.1** 2022-05-11: Added many new virtues and cultural blessings from _Peoples of Wilderland_.
* **v12.2.0** 2022-05-29: Added lookup table building, rolling, and lookup.
* **v12.2.1** 2022-05-29: `\table roll` supports `favored`, `illfavored`, and `feat=<num>` for feat dice only. `\tables list` shows counts. Publish checks various completeness validations. Row codes are now no one's problem but mine and not even visible to anyone else anymore. Better display for very wide tables. Show G and E as icons in `\table show`. Show long but narrow tables in several across.
* **v12.2.2** 2022-06-01: Fixed problem with Brawling proficiency calculation.
* **v12.2.3** 2022-06-03: `\table list` searches are case-insensitive and include the table name. `\table roll <tablename>` to roll on a table without selecting it. Added `!mine` to `\table list` options.
* **v12.2.4** 2022-06-21: Fixed display of shadow scars during `\set` et al., and `\autoswitch` trying to switch to games you're not in.
* **v12.2.5** 2022-06-22: `googlesheets` is now the default in `\char create` (since as yet there's no other choice).
* **v12.2.6** 2022-06-27: Fixed a bug in Gandalf rolls in lookup tables.
* **v12.2.7** 2022-06-28: Added `published` and `!published` to `\table list` filters.
* **v12.2.8** 2022-07-01: Updated the code and Google Sheets sheet to allow for eight rewards and eight virtues.
* **v12.3.0** 2022-07-04: Fixed bug with `\char create` on default key. Character sheets that won't load now have the name "Nameless Thing" plus a number, to allow the character to be selected and manipulated enough to fix it (this may also happen briefly when Narvi has restarted but not yet loaded all sheets). Added `\game edition strider`. In Strider mode, default rolls to `notlm` and automatically append a Fortune or Ill-Fortune table roll when appropriate. Added `\stance skirmish` and `\action gain` for Strider Mode.
* **v12.3.1** 2022-07-04: Even in Strider Mode `\foe` uses `lm` dice.
* **v12.4.0** 2022-07-04: Introduced scribbles (user-defined character sheet elements) and game editions for Expanse, Coyote and Crow, Serenity, and GURPS with defaulting dice rolls.
* **v12.4.2** 2022-07-07: Fixed rare bug with some commands with no parameters; made `\r feat` roll a single feat die with no complaints.
* **v12.4.3** 2022-07-09: Fixed more bugs with some commands with no parameters; `\bonus` now respects Strider Mode dice colors.
* **v12.4.4** 2022-07-09: Added `\as` command
* **v12.4.5** 2022-07-17: Tough As Old Tree-Roots always did both rolls but now it shows them.
* **v12.4.6** 2022-07-17: Added `explain` option and `\explain` command to provide details about how Narvi figured out a roll.
* **v12.4.7** 2022-07-17: Fixed some bugs where bogus syntax could double success dice on rolls; made `\explain` game-specific.
* **v12.4.8** 2022-07-29: Lifepath Favored no longer applies to enemy rolls.
* **v12.4.9** 2022-07-29: `\game mode forgiving` in a 2e game makes it allow `\stance skirmish` and `\action gain`.
* **v12.5.0** 2022-07-31: Fixed rolling 3d6 in a designated game.
* **v12.5.1** 2022-07-31: Square brackets also work for Scribble substitution.
* **v12.5.1** 2022-08-02: In forgiving mode with skirmish stance mixed with others, it goes between defensive and rearward. Escape mode correctly accounted for in skirmish stance. No spend options for escape.
* **v12.5.3** 2022-08-13: Fixed bug with using `escape` on a `\foe` roll (is that even allowed?).
* **v12.5.4** 2022-09-03: Protection against a failure on showing token status (not sure what caused it but it'll fail gracefully next time).
* **v12.5.5** 2022-09-03: Dour-handed was missing the Pierce benefit, it now has it.
* **v12.5.6** 2022-09-04: During startup, Nameless Thing is instead 'Still loading, please wait...'
* **v12.5.7** 2022-09-06: Added `natural` option to `\table roll`.
* **v12.5.8** 2022-09-11: Scribbles are less ruthlessly sanitized. Removed some tracing.
* **v12.5.9** 2022-09-17: Target numbers below 8 are possible now (e.g., in 1e when foes are harried).
* **v12.6.0** 2022-11-06: Added `\game leave`. Fixed a few bugs in `\game remove`.
* **v12.6.1** 2022-11-07: Added `\table show <name>`.
* **v12.6.2** 2022-11-08: Made `\table show <name>` and `\table roll <name>` not require a selected table.
* **v12.6.3** 2022-11-13: Added `\game show full` to always show user numbers.
* **v12.6.4** 2022-11-13: Fixed a bug in rolling on or showing a specified table.
* **v12.6.5** 2022-11-20: Fixed a bug in `\company arrival`; added `\r scout` for various journey roles; skill reports show favored checkboxes.
* **v12.6.6** 2022-11-21: Added `\roll arrival` to do an arrival roll and clear fatigue automatically, including steed vigour; rolling on `journeytarget` shows a list of people in that role.
* **v12.6.7** 2022-11-21: Narvi now uses a local cache for Google Sheets so that when it restarts it should have characters available immediately, but will still poll for refreshes thereafter.
* **v13.b.1** 2022-11-28: Beta test of Journeys, plus showing modifiers on `\status` and a few bug fixes.
* **v13.b.2** 2022-12-02: Added special Strider Mode rolls and tables for journeys, and Noteworthy Encounters.
* **v13.b.3** 2022-12-04: Added feat modifier to `\j start` for things like Ponder Storied and Figured Maps.
* **v13.b.4** 2022-12-04: Fixed `\sheet image` for non-TOR games; added `\journey combine`.
* **v13.0.0** 2022-12-04: Fixed a few minor bugs. Journeys are now out of beta.
* **v13.0.1** 2022-12-09: Added `\sheet compact full`
* **v13.0.2** 2022-12-09: Fixed a bug with Strider Mode journeys that made event roll fatigue rarely apply
* **v13.0.3** 2022-12-11: `\journey start` now checks if the journey has no steps.
* **v13.0.4** 2022-12-12: `\r journey` works again (both in and out of journeys); better handling of bad character image URLs (these will be cleared and not allowed to be set, though I can't help when it's a valid URL but not to an image but a page)
* **v13.0.5** 2022-12-12: Fixed a bug in `\sheet image`.
* **v13.0.6** 2022-12-26: In Strider Mode any roll made for a journey on which hope is spent is automatically upgraded to inspired.
* **v13.0.7** 2022-12-26: Replaced `\game mode` with `\game config` and implemented three starting config flags.
* **v13.0.8** 2022-12-26: Added `journey-mention` config to tag players in journey announcements.
* **v13.0.9** 2022-12-29: When doing Strider Mode, the journey log now includes the additional roll's text.
* **v13.1.0** 2022-12-29: Added support for blessings and `noartifact` flag.
* **v13.1.1** 2022-12-30: `\r attack` now picks the highest weapon skill, same as `\attack` does. Best weapon now breaks ties with damage.
* **v13.1.2** 2023-01-01: Fixed a bug with internal precached standard of living changes, and clearing knockback and other things at combat start/end.
* **v13.1.3** 2023-01-05: Added `\game give` command after multiple requests.
* **v13.2.0** 2023-01-05: Added `\modifier` command for advantages and complications in combat.
* **v13.2.1** 2023-01-07: Fixed a possible crash with `\start` without complications or advantages.
* **v13.2.2** 2023-01-07: New `\report shadow` that isn't just a token report but integrates shadow, shadow scars, and hope in one report.
* **v13.2.3** 2023-01-08: "Elf of Mirkwood" no longer suffers the shadow recovery penalty that the "Elf of Lindon" suffers. 
* **v13.2.4** 2023-01-09: Songbook entries have an optional bonus dice parameter. New config entry `journey-weather` makes Narvi automatically roll weather before each Marching Test in journeys.
* **v13.2.5** 2023-01-10: When Narvi can't answer you because of channel permissions it will try to tell you in private messages.
* **v13.2.6** 2023-01-15: In Expanse games you can now include `tn=<num>` or `target=<num>` and `\r+3` will work despite the missing space.
* **v13.2.7** 2023-01-16: Fixed bug with reporting journey test roles; added `\report key`; added `\company short safe`; added `\journey pass|fail`, internal testing for mention generation
* **v13.2.8** 2023-01-19: Implemented all implementable Qualities for famous weapons and armor.
* **v13.3.0** 2023-01-20: Also implemented Curses. Made `noartifact` work to counter Qualities where applicable.
* **v13.4.0** 2023-01-22: Eye Awareness and `\eye` command.
* **v13.4.1** 2023-01-22: Fixed a bug with `\game config`.
* **v13.4.2** 2023-01-23: Fixed a bug with `\r` without a game.
* **v13.4.3** 2023-01-23: Fixed a bug with `\r` when someone has the Furious cultural blessing.
* **v13.4.4** 2023-01-25: Fixed some DWIM so that `\spend hope` still redirects to `\use hope` (it had been that `\spend 1 hope` did, but `\spend hope` was the more common mistaken syntax to catch).
* **v13.4.5** 2023-01-25: Added some internal functions for counting and purging empty games and counting 1e games.
* **v13.4.6** 2023-01-25: All functionality related to 1e AP has been removed.
* **v13.4.7** 2023-02-17: The `\eye` command has been migrated to an interactive `/eye` slash command. More will come!
* **v13.4.8** 2023-02-17: Slash commands now support autoswitch.
* **v13.4.9** 2023-02-18: Protective code on journey mentions; `\news` removed, now part of `\version`
* **v13.5.0** 2023-02-18: Moved `\version` and `\suggest` to slash commands
* **v13.5.1** 2023-02-19: Fixed a bug with `roll-eye` when not miserable; added `\council attitude open|friendly|reluctant`; fixed a bug with councils not automatically ending; added `\council undo`
* **v13.5.2** 2023-02-20: Moved `\weather` to slash commands
* **v13.5.3** 2023-02-20: Fixed a bug with `\report shadow` handling shadow scars.
* **v13.5.4** 2023-02-25: Added a redirection message on old commands now slash commands; `\mellon` moved to slash commands.
* **v13.5.5** 2023-02-25: `\songbook` moved to slash commands; support for picklist localization.
* **v13.5.6** 2023-02-25: `\autoswitch` and `\savelast` moved to slash commands.
* **v13.5.7** 2023-02-26: Better error trapping; fix to `\mellon 1e`; moved `\counter` and `\secret` to a single `/counter` slash command.
* **v13.5.8** 2023-02-27: Hopefully fixed some problems with odd situations with weapon rolls when you specify the wrong weapon name or it has spaces in it; temporary combat modifiers don't start until the top of the next round so they don't get cut off prematurely.
* **v13.5.9** 2023-02-27: Fixed a bug introduced in 13.5.8 with finding weapons other than the first; fixed more reports not being messed up by long names; `^` can now be used instead of `\` to introduce comments in `\r` commands for those who have trouble typing `\`; Narvi now treats 11s rolled by those favoured by the Grey Wizard as -not- a piercing blow (and in fact as a 1) per the ruling given by Michele on the forums; also added game config entry `pierce-eleven` which means an 11 does count as a piercing blow as it has previously. (Note my restraint in not naming that `this-goes-to-eleven`.)
* **v13.6.0** 2023-02-27: Split displays of hope and shadow changes that fall inside roll embeds onto a new line. Renamed `pierce-eleven` to `eleven-pierces` and added `eleven-can-pierce` to the `\game config`.
* **v13.7.0** 2023-03-08: Moved `\status` to slash commands. All 1e support is removed; existing 1e games converted to TOR games, and the characters will show as Nameless Things until updated with `\sheet key <key>`. Some fixes to `\treat <character> [rolloptions]` not yet tested. `\game edition` is now `\game system`.
* **v13.7.1** 2023-03-09: Moved `\pool` to slash commands and changed the syntax somewhat, moving `\use pool` and `\use pool hope` into it.
* **v13.7.2** 2023-03-10: Fixed a bug with token commands like `\add` affecting a Nameless Thing instead of yourself.
* **v13.7.3** 2023-03-11: Fixed a bug that could make Narvi non-responsive when rolls were done on incomplete characters.
* **v13.7.4** 2023-03-11: `\report roles` is now narrower by using abbreviated skill pips; `\role absent` excludes you from fatigue when journeys are being done; hopefully a fix on journey role mentions?; moved `\clear` to slash commands.
* **v13.7.5** 2023-03-15: Moved `\company` to slash commands.
* **v13.7.6** 2023-03-18: Added `/mellon refresh` to force a refresh of all external characters. Skill rolls with targets show the TN. Moved `\set` to slash commands.
* **v13.7.7** 2023-03-19: Removed a DWIM with rolling weapons that could cause parry and TN to get mixed up. Moved `\gain`, `\lose`, `\restore`, `\damage`, and `\heal` to slash commands.
* **v13.7.8** 2023-03-20: Fixed a bug with token commands without a valid character. Fixed a bug where not specifying a value could cause a crash. Changed `/damage` and `/heal` to require a value, not default to 1.
* **v13.7.9** 2023-03-20: Fixed a bug with `\clear`. Fixed a bug with `/company` leaving out names. `\report <skill>` now shows TNs. Added some debugs to help track down an irreproducable bug with `\treat`.
* **v13.8.0** 2023-03-22: Fixed a bug with `/company treasure` not updating Standard of Living properly.
* **v13.8.1** 2023-03-22: Systemic changes removing weird 1e-to-2e workarounds for wounds and doing a 2e-native handling throughout; moved `\wound` to slash commands and changed its syntax and functionality a bit; moved `\set active` to `\char active`.
* **v13.8.2** 2023-03-25: Moved `\report` to slash commands and did a thorough rewrite and reorganization to it. Fixed a bug with losing the effect of combat actions if Narvi restarted at the wrong moment. `/status`, `/company`, `/report`, and other places are using a common display function for things like hope and wounds, so the displays are always complete, consistent, and recognizable.
* **v13.8.3** 2023-03-26: Fixed some display bugs in `/company`. Fixed a bug with journeys ending in a perilous area. Added more visible indications for entering and leaving perilous areas.
* **v13.8.4** 2023-03-26: Added `/dice` and `/gurps` though these are still present in `\r`.
* **v13.8.5** 2023-03-26: Added `/expanse` though this is still present in `\r`. Fixed a problem with `\sheet image` on non-TOR games.
* **v13.8.6** 2023-03-26: Fixed some bugs about the timing of outputs and the parsing of slash command inputs.
* **v13.8.7** 2023-03-27: Added `/last` (note that `\last` will repeat the last old-style command and will eventually go away, while `/last` will repeat the last slash command and will eventually become the underpinnings of the `/macro` system).
* **v13.8.8** 2023-03-28: Added `/coyote` for Coyote and Crow dice rolling, with a slightly altered syntax.
* **v13.8.9** 2023-03-30: Fixed bug with fatigue reductions accumulating in `/company` commands, and wound recovery not being shown. Fixed some bugs with token commands.
* **v13.9.0** 2023-03-30: Added `/cortex` command for Serenity dice rolling. The `\r` command no longer supports polyhedra, GURPS, Expanse, Cortex, and Coyote and Crow functions. Added some DWIM to `/pool distribute`, as well as internal logging to try to track down an irreproducible math error.
* **v13.9.1** 2023-03-31: Started work on the `/tor` command (currently it's just a stub that parses its input but doesn't do anything with it) and all the supporting slash command functionality. Fixed a bug with `/dice` and added better bug tracing. Fixed a bug with certain returns in `\roll`.
* **v13.9.2** 2023-04-01: Fixed a bug with specifying an invalid character in token commands causing a failure. Autoswitch now handles the case where there are multiple games on a server, but the user is only in one of them. Fixed a bug with journey rolls.
* **v13.9.3** 2023-04-02: Added Friendly and Familiar to the logging in `\explain`. Fixed the naming for Awe in various places in `/report`. Added `/tor` command for rolling TOR dice with just a number of dice, not a test, as well as `/feat` to just roll multiple feat dice and `/success` for just success dice.
* **v13.9.4** 2023-04-05: Fixed some errors sending blank messages. Added the `/roll` command, but `\roll continues to exist for now.
* **v13.9.5** 2023-04-05: Replaced `\explain` with `/explain`. A bunch of refactoring of `/roll` to prepare for `/attack` and other commands that do roll handling.
* **v13.9.6** 2023-04-06: Added `/attack` though `\attack` is still around for a little while. Emoji for spending tengwars can now be clicked by the loremaster as well as the attacker. Weapon matching will now try substring matches if full names aren't found.
* **v13.9.7** 2023-04-06: Added `/foe` though `\foe` is still around for a little while.
* **v13.9.8** 2023-04-07: Replaced `\help` with `/help` which is just links to the documentation, since slash commands are largely self-documenting. Added `/skill` as a quicker version of `/roll` if you want to roll a skill (same functionality, but the options tree takes you directly to the skill list).
* **v13.9.9** 2023-04-08: Fixed a bug with magical results and eye accrual.
* **v14.0.0** 2023-04-08: Removed a lot of remnants of the old `\roll` command. Added `/treat` and removed `\treat` and all the remnants of token commands. Lots of internal cleanup. We are now over the top of the hill on slash commands.
* **v14.0.1** 2023-04-10: Changed `\game` to `/game`. `/game give` now leaves you in the game with your characters if you have any, otherwise you leave the game after giving it to someone. Many fixes and cleanups to `/game` command. Configured bot to get user names on loadup.
* **v14.0.2** 2023-04-11: Retired `\as` command since it will not port over to slash commands. Replaced `\character` with `/character` which inherits also the options to set key, image, and active. More stripping of now-obsolete code. Fixed some bugs with commands right at startup causing crashes, and problems with `\sheet image` causing problems with no URL. `/character create` in non-TOR games takes its parameter as a character name instead of looking for a Google Sheets key. Improved efficiency of loading Google Sheets when the same character is in more than one game.
* **v14.0.3** 2023-04-12: `\sheet` has been replaced by `/sheet` and `/scribbles`. Some small improvements in sheet displays.
* **v14.0.4** 2023-04-13: `\roll`, `\rollas`, `\foe`, and `\attack` are all now disabled in favor of their slash command replacements. `\cmd` is now replaced with `/macro` which has the ability to stack commands with the `append` option. Nearly all DWIM logic is now removed, and lots more of the old i18n system. At this point probably half of Narvi has been rewritten with cleaner, more robust, more manageable code. Fixed a long-standing bug in string searching that must have been responsible for a lot of other rare problems.
* **v14.0.5** 2023-04-13: Changed journey and council prompts for rolls to use current roll syntax. Removed some more obsolete i18n.
* **v14.1.0** 2023-04-13: Replaced `\\actions` command with `/actions`, `/intimidate`, `/rally`, `/protect <person>`, `/prepare`, and `/gainground` (all of which can be modified just like `/roll`).
* **v14.2.0** 2023-04-14: Limited maximum number of dice to 50 for polyhedra and 20 for TOR to avoid people rolling huge numbers and lagging Narvi. Replaced `\modifier`, `\stance`, `\spend`, and `\knockback` with the equivalent slash commands.
* **v14.2.1** 2023-04-16: Fixed some bugs with `/sheet` in non-TOR games and `/scribble set` finding matches.
* **v14.2.2** 2023-04-16: `/roll` attempting to affect a journey or council when none is active gets an error message. Fixed a problem with `/pool distribute` when Google Sheets is slow to respond.
* **v14.2.3** 2023-04-18: Fixed `/roll` bug on shadow tests not correctly implementing Hobbit-sense. Early stages of initiative revamp are in place but not yet linked into anything. Removed hints about using `/skill` when you had actually done so.
* **v14.2.4** 2023-04-24: Fixed a bug with autoswitch. Lots of the new initiative system is in place but not yet activated.
* **v14.3.0** 2023-04-24: Moved `\start`, `\end`, and `\next` to slash commands and added `/current` and `/report initiative`. Initiative is now handled using phases which track who has acted and display in a colorful guide.
* **v14.3.1** 2023-04-24: Added `/game config council-ruins` to default to using the _Ruins of the Lost Realm_ rules. Revised startup process so that very long load times will not prevent locale loads.
* **v14.3.2** 2023-04-24: `/report initiative` no longer tries to show info for a combat that ended.
* **v14.4.0** 2023-04-25: Disabled a startup option ensuring Narvi would always know Discord user names because it was causing slow startup which caused `/tor`, `/skill`, `/roll`, `/attack`, `/foe`, `/treat`, and the action commands to sometimes not get their roll options. Moved `\council` to slash commands. Improved brief display of council status in `/game show`.
* **v14.4.1** 2023-04-25: Replaced `\role` with `/role`.
* **v14.4.2** 2023-04-26: Fixed a bug where bonus dice being added could change in odd ways. Made another change to hopefully fix the problem where shared roll options fail to appear on their slash commands, and more debug tracing to try to identify that problem.
* **v14.4.3** 2023-04-26: Re-fixed stance modifiers incorrectly applying during Opening Volley phase.
* **v14.4.4** 2023-04-26: An experimental loading of users asynchronously without it holding up startup.
* **v14.4.5** 2023-04-27: `/gainground` allows you to specify the skill (but still defaults to the better skill).
* **v14.4.6** 2023-04-27: New `/jedit` command handles all creating and editing of journeys; these functions are removed from `\journey`. Note that instead of typing the journey name over and over, you now `/jedit select` a journey to work on. `/jedit remove` lets you remove a single step from a journey.
* **v14.5.0** 2023-04-27: Moved `\journey` to `/journey` and improved some things in its displays.
* **v14.5.1** 2023-04-28: Fixed a typo causing a problem in initiative in defensive phase.
* **v15.0.0** 2023-04-28: `\table` moved to `/table`. `\language` and `\prefix` removed. All commands are now slash commands. Lots of now obsolete code removed. Fixed a few small bugs in `/rally` and similar action commands. 
* **v15.0.1** 2023-04-29: Added some sanity checks on table rolls to prevent bombs when rolling on non-existent, inaccessible, and incomplete tables.
* **v15.0.2** 2023-04-29: A few more bug fixes related to journeys calling table rolls, and one in `/jedit`.
* **v15.1.0** 2023-04-30: Better name trimming in most reports thanks to Gert Goatleaf. Fixed more bugs with journeys rolling on tables. Added `/journey quick` to create and start a one-step temporary journey in one go. Fixed a bug in localizing subcommands.
* **v15.1.1** 2023-05-02: Added error trapping on creating interaction messages. `/journey show`, `/jedit show`, and `/table show` now show created dates as a dynamic timestamp that is always right in your personal time zone. Newly created journey logs will show their timestamps the same way in `/journey log` (but already existing ones will not).
* **v15.1.2** 2023-05-03: Added `/game config always-explain` to automatically put an `/explain` after every roll.
* **v15.2.0** 2023-05-03: The emoji reactions for spending Tengwar are now replaced with buttons which also show what the bonus will be. Small improvements in the better skill chooser (currently only used in `/gainground`). A more technically correct way of stringing multiple responses together will look odd until you get used to it.
* **v15.2.1** 2023-05-05: Better error-checking to provide a more meaningful error message when trying to start a non-existent, inaccessible, or incomplete journey, or using a different case when starting than you did when creating. Added `/game lmchannel` command.
* **v15.2.2** 2023-05-07: Fixed a bug with `/end` when someone had been wounded. Fixed token commands with no character specified behaving badly. Initiative's change stance phase shows current stances.
* **v15.2.3** 2023-05-07: Added `/game config combat-mention`. Fixed a bug with `/pool distribute` with amounts specified. In initiative, append a message when the last action in the current phase has been used.
* **v15.2.4** 2023-05-08: Fixed a bug with `/game add` double-acknowledging an interaction. Fixed a bug caused by previous bufg fix for token interactions without a character specified.
* **v15.3.0** 2023-05-08: Added `/pool max`, `/pool reset`, a reset option on `/pool distribute`, and better display of the pool.
* **v15.3.1** 2023-05-08: When choosing the better of two skills, Narvi is a fair bit smarter. Right now only used in `/gainground` and only if you don't specify a skill, but this might be useful later.
* **v15.3.2** 2023-05-08: Modifiers have a starting phase and can be made to start immediately.
* **v15.3.3** 2023-05-09: Per the rules on p139, miserable characters do not lose shadow at fellowship phases. `/modifier` now has an option to specify one or more characters and if so the modifier only applies to them. Fixed several bugs in modifier handling.
* **v15.3.4** 2023-05-10: Fixed a bug where added error-checking before `/journey start` was using the wrong value and thus preventing journeys from starting in most cases. Fixed some bugs with journeys that start in a perilous area not displaying right.
* **v15.4.0** 2023-05-11: Added the `/aedit` command for creating and editing adversaries, though they are not yet used anywhere -- all in good time!
* **v15.4.1** 2023-05-13: Per a ruling by Michele Bugio, the default for all lookup table rules is that no modifier can change results to or from Eye or Gandalf, so the `natural` option on `/table roll` now defaults to True, and all internal table rolls (such as for journeys) honor this as well.
* **v15.4.2** 2023-05-13: Added `/aedit setup` to set up many parts of an adversary in one command. Allowed `/aedit set` to set an image to `none`. Setting abilities inherits the comment if it's blank and another has the same ability.
* **v15.4.3** 2023-05-13: Fixed bugs with displaying adversaries with a lot of abilities, with `/aedit show <shortname>`, and with showing without having selected anything.
* **v15.4.4** 2023-05-20: Fixed bugs with removing ranges from lookup tables with multiple dice. Fixed a bug with calculating whether anyone can act in a given phase. Fixed Stout-hearted not applying on Dread tests. Fixed a missing localization on `/treat self` and some localizations in journeys. Added a `feat` option to `/wound` when letting it roll, so the roll can be favored or ill-favored.
* **v15.4.5** 2023-05-22: `/company yule` now also gives everyone SP equal to their Wits. Fixed another bug where success dice were treated as strings, not numbers. Eliminated spurious notifications of a completed round when a combat isn't active. Added a tip directing `/lose endurance` to `/damage` or `/heal`. `noitem` no longer prevents Marvellous Artefacts from applying (e.g, in combat). Added `/council pass` and `/council fail`.
* **v15.4.6** 2023-05-22: Corrected a cache calculation so that Narvi accounts for Marvellous Artefacts and carried treasure when recalculating encumbrance.
* **v15.4.7** 2023-05-25: Fixed several `/pool` command not defaulting or requiring a number of points. Added `/game config default-noitem` causing noitem to default to true. `/game config` defaults to setting the config flag specified.
* **v15.5.0** 2023-05-25: Added `/game config unphased-initiative` for showing all of combat at once, especially useful for Play-By-Post.
* **v15.5.1** 2023-05-29: Added conditions: daunted (hope spends are ignored), shield smashed (cannot do Shield Thrust and do not benefit from shields), seized (forced to Forward and can only make Brawling attacks), dreaming (from Dreadful Spells: unconscious until a Song or 1 hour), misfortune (from Dreadful Spells: all TNs increased by Shadow), and dismayed (from Dreadful Spells: all rolls Ill-favoured). Some of these clear at `/end` of combat, and others in various `/company` options. `/status` shows these and now has syntax to set and clear them as well as a bunch of other 'temporary' effects like temporary parry. New button to spend Tengwar on escaping seized status. `/clear conditions` will clear all of them.
* **v15.6.0** 2023-06-03: Major refinements in how application commands are loaded to avoid Discord rate limiting. New `/encounter` command for building encounters which will later be used in combats as part of Adversary support. (Both `/aedit` and `/encounter` will be added to the documentation later when the stuff they lead to is built.) Sure At The Mark won't change feat dice for a weapon unless you're either in Rearward, or this is an Opening Volley attack, or the weapon is solely ranged.
* **v15.6.1** 2023-06-03: `/encounter delete` supports deleting a single adversary from an encounter.
* **v15.6.2** 2023-06-03: Fixed some permission problems with `/aedit set` and `/encounter adversary`.
* **v15.6.3** 2023-06-03: Moved the display of adversary and encounter images to a followup to force them to display properly since for unknown reasons Discord is ignoring them when within embeds lately.
* **v15.6.4** 2023-06-03: Added a new Haunted status (-1d on all rolls for duration of the combat, from Snava the Orc) and a new Bleeding stat (from Vampire Bats Bite attack) which can be set and cleared in `/status` and are automatically executed and cleared as appropriate. Adversaries can now have a Bite special attack. Dreadful Spells can now have a Haunted type.
* **v15.6.5** 2023-06-04: Fixed a display problem with change stance phase in an initiative report. Fixed a bug with displaying roles when they are not lowercased. Battle rolls during combat are assumed to advance initiative. Possible improvement for, and more logging for, user mentions in journeys and initiative. Fixed a bug adding encounters to very old games. Journeys now account for all being on horseback in determining duration, and advance wound recovry based on the number of days. Revamped the handling of damage refunds for knockback to work correctly with and without highest-knockback config, and to work immediately but update as circumstances change. Removed `/knockback` command since it can be done with both `/damage` and `/status`. Added knockback to conditions shown in `/status`.
* **v15.7.0** 2023-06-06: Encounters can now be used in combats, though the adversaries do not yet appear other than being listed in the Adversary Action phase, but titles, modifiers, and events have their parts in the combat. You can copy the encounter while you `/start` with it, and/or delete it when you `/end`.
* **v15.7.1** 2023-06-12: Someone's managed to add an adversary to a non-existent or incomplete encounter and I can't reproduce this, so I put some code into the `/encounter adversary` code to catch and either error out or fix this for now.
* **v15.7.2** 2023-06-13: Rounding for knockback refunds. Adversaries added while combat is active get added to the tracking in that round.
* **v15.7.3** 2023-06-17: `/report key` now shows player numbers for use in `/game remove` and shortens the key to a linked key code.
* **v15.7.4** 2023-06-20: Protective code for a journey error I can't reproduce. More emphasis and logging on bomb errors. Narvi now protects itself against too many errors too quickly by putting those who cause them onto a 20-second cooldown.
* **v15.7.5** 2023-06-21: Prevent journeys from having steps of length <= 0 or non-numeric which should avoid the problem noted above.
* **v15.7.6** 2023-06-24: Fixed a bug starting journeys when the journeys list had multiple items. Fixed a bug in storing users for the cooldown.
* **v15.7.7** 2023-06-26: Fixed a bug where a `/roll` or `/skill` command doing Battle could advance an initiative.
* **v15.7.8** 2023-06-26: More (partial) support for adversaries: `/attack` lets you specify a target or a parry; if neither is specified, parry is set to 0; if an adversary is specified, the parry is taken from that; parry is now an addition to the TN instead of a subtraction from the roll; `/attack` now does base damage automatically and notifies the Loremaster (via the lmchannel) of the outcome; adversaries who are at 0 endurance now show with a skull on the initiative lists and do not have to act to advance out of the adversary phase; Dragon-Slayer, Hammering, Biting Dart, Gleam of Terror now automatically applied when the target is chosen; Superior Grievous automatically does extra damage to Bane enemies; fixed a bug in finding the best weapon when values exceed 9. Narvi does not yet implement any of these effects when spending Tengwar, nor does it do anything for adversaries making protection rolls, including handling Superior Keen, Superior Fell, Foe-Slaying, Fierce Shot, and Splitting Blow, nor does it handle `/intimidate` stripping hate/resolve or Fearless. Yet. There are also some changes coming to the Google sheet handling Superior qualities that vary by origin and/or bane more intelligently. 
* **v15.7.9** 2023-06-26: Fixed a bug in which bonuses from things like Close-Fitting were not being applied properly.
* **v15.7.10** 2023-06-26: Fixed a bug in which bonuses from things like Close-Fitting were not being applied to the right things.
* **v15.7.11** 2023-06-28: `/attack` now implements Superior Keen and Superior Fell on bane enemies, Splitting Blow, Fierce Shot, and Foe-Slaying. `/spend` and the buttons now can do damage, pierce, and shield properly to an adversary when one is specified. `/intimidate` now sets adversaries weary and respects Fearless.
* **v15.7.12** 2023-06-29: Fixed a bug with qualities causing H/R loss. Added support for `/intimidate` causing H/R loss to the Craven, and no effect at all to the Heartless without a magical success. An attack that would bring a foe with Hideous Toughness to 0 now queues up a protection test instead (later the protection test will know it came from a Hideous Toughness test and will implement the right effects).
* **v15.7.13** 2023-07-02: Fixed a problem with `/character active` when the LM has no characters. Disabled check on Dreadful Spells type. `/report keys` now shows active status.
* **v15.7.14** 2023-07-02: Fixed a problem with `/clear conditions`. LMs can always change everyone else's stance, even mid-round, with phases updating properly accordingly when possible. Fixed a bug with attacks when the item has multiple injury levels.
* **v15.7.15** 2023-07-02: Opening volley phases no longer show as all checked off before they begin. If the loremaster changes someone's stance to the current phase they don't show as having already taken their action. Knocked-back characters cannot `/attack`.
* **v15.8.0** 2023-07-07: Fixed another bug with protection tests and handedness. If an encounter has an image, `/start` using that encounter will display it. Added the `/test` command for recording, listing, and clearing pending protection and shadow tests. Fixed a bug with some encounters being too big to display. `/status` shows a brief description of pending tests. `/roll` for tests will use the stats and consequences for the next pending test of that type, though explicitly specified factors like feat will override the test. New `affects` option of `Not Queued` to override this function and do a test without using the queued tests. A failed protection roll for a queued test will automatically implement the wound and account for Deadly Wound when applicable. `/clear` now has options to clear all player-hero protection or shadow tests. Journey events can queue shadow tests. Adversary damage is shown with the maximum as well as current values of endurance and hate/resolve.
* **v15.8.1** 2023-07-24: Built the syntax for the new `/foe` command but for now `/foe roll` and `/foe engage` are the only commands implemented. `/foe roll` works essentially identically to the old `/foe` command; `/foe engage` lets you record engagement for an adversary and this does get counted throughout, and displays in `/report stance`.
* **v15.8.2** 2023-07-27: Added `/foe lose`, `/foe gain`, `/foe clear`, and `/report adversaries`.
* **v15.8.3** 2023-08-03: Expanse dice don't show stunt points on failures. Added `/foe protection` and made many corrections to the queueing of protection tests; `/foe protection` handles Hideous Toughness, Tough Hide/Armour, Splitting Blow, Cleaving, Might over 1, and every other modifier I know of. Improved display of adversaries (e.g., in `/encounter show`) to reflect wound counts.
* **v15.8.4** 2023-08-16: `/foe attack` now looks up attacks for damage and injury, applies Small Folk and skirmish modifiers, supports Fierce, checks for knockback, implements pushback, does damage, queues protection tests (including Dreadful Wrath), sets engagement, tracks initiative, and implements tengwar spend buttons. Fixed a bug with `/foe engage` when you have no encounter or it has no adversaries. Fixed a bug with `/attack` when you don't specify an adversary. Fixed a bug with `/success` or `/feat` causing Eye Awareness or Strider table rolls. Normalized `/explain` text's case and added notices of weary and miserable. `/company fellowship|yule` now shows shadow scars. Embeds now have a bar color specific to the type of embed (green for PH actions, red for adversary actions, blurple for commands, and greyple for questions and answers), with their buttons matching. Built some provisions for adding more buttons in future.
* **v15.8.5** 2023-08-22: `/foe ability` is now available and supports these fell abilities: Deathless, Spirit, Horrible Strength, Foul Reek, Dreadful Spells, Freeze the Blood, Strike Fear, Thing of Terror, Yell/Howl of Triumph. Fixed a bug with `/status` with temp bonuses (which can also be penalties now). `/start` now allows setting a light level, so that Denizen of the Dark and Hate Sunlight are implemented.
* **v16.0.0** 2023-08-23: Old-style engagement is only available and counted when there is no encounter active. `/intimidate` now has a riddle option for using Riddle against Dull-witted foes. Added weary and miserable conditions to `/status` and `/clear` (and clearing in some `/company` options) for when heroes are weary or miserable for reasons other than their endurance or hope. Added a `/nameless` command to generate Nameless Things automatically. Dark for Dark Business now automatically applied when in a fight set to darkness. This completes Adversary Support!
* **v16.0.1** 2023-08-23: `/report adversaries` now shows adversary stats only when run by the LM, but when a player runs it, it just shows the shortname and fullname of the adversaries, which ones are out of the fight, and their engagement. This is useful for players to pick targets.
* **v16.0.2** 2023-08-26: On already piercing blows, don't show Pierce button; on ones where you have tengwar but not enough to score a piercing blow, show them with a red X instead of a target symbol (but also hide those if game config hide-pierce is on). Fixed a bug with Strider table rolls invoked by a `/roll protection` or `/roll shadow` showing as an undefined table. Allow partial matches on adversary names where applicable.
* **v16.0.3** 2023-08-28: Narvi no longer advises people to use `/skill` for `/roll marching`. Inactive characters no longer accrue journey fatigue. Fixed a problem with player-heroes rolling queued protection tests, and added an indication of which test to the roll results. Added `/foe list` so the loremaster can show the sanitized version of the adversary list for player use. Superior Reinforced shields cannot be broken by foes. Finally fixed a problem that caused journey mentions (and possibly others) to sometimes go wonky.
* **v16.0.4** 2023-09-01: Fixed a problem where `/dice` would misbehave when invalid dice formats are used. Prevented several problems resulting from running combats without encounters. Fixed a bug with wounds after protection tests given without TNs.
* **v16.0.5** 2023-09-04: Fixed some internal localization functionality (in preparation for one of the localizations finally being worked on). `/help` will start showing a list of languages once there are any more than English US to show. Added support for The Art of Smoking to `/company fellowship`, `/pool distribute`, and `/pool claim`, but not to `/gain` since that can be used for corrections and can be adjusted manually.
* **v16.0.6** 2023-09-10: Fixed a double minus in combat modifier display in `/current`. Yell/Howl of Triumph no longer affects unconscious/dead adversaries. Fixed some odd displays of `/attack` results when certain special effects combine with a piercing blow. `/current` and other initiative displays show a list of engaged targets for each character.
* **v16.0.7** 2023-09-20: Fixed a bug in `/journey quick`.
* **v16.0.8** 2023-09-21: Prevent initiative from counting adversary actions done on the wrong phase. Fixed a bug with checking shields for smashability based on qualities.
* **v16.0.9** 2023-09-22: Fixed Gleam of Wrath stacking with Gleam of Terror. That's a lot of gleaming. Added buttons to the "all actions in this phase taken" message (now an embed) that activate `/next` and `/end` with a click.
* **v16.1.0** 2023-09-23: Added 1h and 2h buttons when an `/attack` does not specify but needs to. Added confirm buttons to every command where you can get the 'must confirm' error message and only a single confirmation is required.
* **v16.1.1** 2023-09-24: Fixed a bug with the 'you can advance' button. Added a confirmation button to the `/next` display when people still have an action. LM messages about damage and HR stripping of adversaries now include a progress bar. Fixed a bug with title casing of buttons.
* **v16.1.2** 2023-09-25: Fixed a bug with adversary tengwar spend buttons.
* **v16.1.3** 2023-09-25: Fixed a bug with the display when a `/stance` command completes the Change Stances phase. Changed wording of who must make a protection roll to opponent instead of foe, since foes can do that to player-heroes too.
* **v16.1.4** 2023-09-26: Fixed two bugs with the Riddle mode of `/intimidate` where it might strip 99 hate, where it would allow the riddle action outside of Forward stance (and then not count it against initiative when used that way), and where it would affect non-trolls as a normal Intimidate. Fixed a bug in hate spends on `/foe attack` when not using adversaries.
* **v16.1.5** 2023-10-06: Fixed a bug in Stout-hearted being applied. Fixed a bug in Coyote and Crow change command. Fixed a bug with `/foe roll` when damage is specified but no adversaries and a tengwar is scored.
* **v16.1.6** 2023-10-07: Restored display of player numbers to `/game show` when the loremaster looks at it.
* **v16.1.7** 2023-10-23: Shortened LM messages from `/attack` to better fit the gauge in one line. Fixed wrong description on `/test list`. Fixed (again) `/success` triggering eye awareness. `/test list` and `/test clear` now supports `company` to show/clear all tests. `/company fellowship` and `/company yule` now clear all pending shadow and protection tests. Fixed a bug preventing a failed protection test from causing a dying condition when already wounded. `/foe attack` no longer shows buttons, and automatically does a heavy blow, if the special action is Pierce but there's not enough Tengwar to get to a piercing blow, or it already has one.
* **v16.2.0** 2023-11-04: Added support for skill endeavours with the `/endeavour` command and the `endeavour` option on `affects` for the `/roll` and `/skill` commands.
* **v16.2.1** 2023-11-04: Added support for poisons. You can use `/status` to set a poisoned state (0 is unpoisoned, 1-3 are moderate/severe/grievous), or a protection test might set poison automatically, if set with `/test` or if resulting from a `/foe attack` with a relevant Fell Ability. Poisoned status shows on `/status` and also appears in the Google Sheets character sheet under other conditions. `/company` will implement the lack of rest, and `/company prolonged` (along with journey resolution) will do the daily rolls for endurance damage and possible recovery or unconsciousness. `/treat` can now treat poison (it will always prioritize wounds first though).
* **v16.2.2** 2023-11-05: Fixed how Narvi handles the weariness from Intimidate Foes (it only affects attacks and lasts per attack, not per round).
* **v16.2.3** 2023-11-17: `/foe list` can be used by players (but it's the same as `/report adversaries` is for them). Fixed a bug with trying to edit an encounter that has been deleted or never created.
* **v16.2.4** 2023-11-17: Fixed a bug in `/set` using relative amounts.
* **v16.2.5** 2023-11-20: Narvi says a phase is done and offers advance buttons if there are still characters in it but they are knocked back. /attack warns if you've picked an adversary that's already out of combat. Attacks (and tengwar spends on Heavy Blow) show if they have taken a foe out of the fight.
* **v16.2.6** 2023-12-11: Fixed some alignment in `/sheet compact`. Added `/game refresh`. Implemented homebrew culture blessings and virtues Virtue of Kings, Over Dangerous Leagues, and In Defiance of Evil.
* **v16.3.0** 2023-12-22: Added generalized support for hex flowers with the `/hexflower` command, and migrated `/weather` to use it.
* **v16.3.1** 2023-12-22: Fixed bug with hex flower pawn positions not being saved; added a display of current pawn position to `/hexflower show`.
* **v16.3.2** 2024-01-08: Google Sheets API now retries quota errors on writes, not just on reads. `/aedit set` handles putting commas into tag lists. Warning when using a dead adversary in a `/foe` command to take an action. `/attack` supports the `unarmed` weapon to automatically default to a punching attack (dmg 1, no chance of piercing blows).
* **v16.3.3** 2024-01-12: Added `gain` and `lose` options to `/scribbles`. Fixed a bug in `/scribbles set`.
* **v16.4.0** 2024-01-15: Fixes reflecting [third printing errata and changes](https://freeleaguepublishing.com/wp-content/uploads/2023/09/TOR_CB_Errata_and_FAQ.pdf). To wit: Daggers can now spend tengwar on Pierce like swords. Defiance recovers better of Heart or Wisdom, not Valour. Spending Tengwar on Pierce maxes at 10. Protect Companion now uses Athletics, not Battle. Hideous Toughness now restores to half endurance (rounded down, though all known adversaries at this time have an even number), not full. Cannot use an unarmed attack on anyone with Hideous Toughness. Intimidate Foe's weariness now lasts rounds, not actions, again. Helms prevent Endurance of the Ranger from working. Councils by default use the rule previously added in Ruins and now included in errata, but can be overridden to first printing rules with `original` or `/game config council-original`. Memory of Ancient Days converts Dark to Wild and Wild to Border.
* **v16.4.1** 2024-01-15: Narvi now counts how much hate/resolve has been spent in a round and sends a warning if it exceeds might, but does not prevent it, only sends a warning to the LM privately. The amount spent is shown in `/report adversaries`.
* **v16.4.2** 2024-01-16: Fixed a bug in journeys when rolls should be favoured. Fixed one hate spend that wasn't going through the above check.
* **v16.4.3** 2024-01-18: Fixed a bug in running macros when the commands within them have simple string responses.
* **v16.4.4** 2024-01-21: Fixed a bug in reporting the name of an unmatched weapon. Changed the name matching in `/attack` to be include, not substring (that is, "spear" will match "Great Spear" now).
* **v16.4.5** 2024-01-23: Cram no longer offers short rest healing to those who are wounded. Fixed a bug with journey rolls checking for Endurance of the Ranger.
* **v16.5.0** 2024-01-23: `/modifier` now permits specifying a modifier (advantage or complication) applies to adversaries, which works the same as those for player-heroes, with starting phase and duration available.
* **v16.5.1** 2024-01-29: Added a hint for rolling on Healing suggesting `/treat`. Fixed a bug with creating an encounter with a name with uppercase and then not having it selected. `/report attributes summary` report got some updates from 1e to 2e including how to display armour, shields, and weapons.
* **v16.5.2** 2024-02-04: Fixed a bug with Gleam of Wrath. Fixed a bug with hints about marching rolls. Fixed a bug with `/company prolonged/safe` with recovery with a treated wound. `/foe roll` no longer requires a character. Fixed a bug with `/company shortsafe` not reporting the right recovery. `/pool distribute` corrects if you type a dash instead of an equal sign. When rolling an `/attack` trying to escape, handedness is assumed 1h if not specified. `/report damage` shows if the character is weary.
* **v16.5.3** 2024-02-05: `/treat` shows who is being treated. `/wound` command overrides rather than adding to a previous wound when being used to adjust the wound. Fixed handling of Virtue of Kings for inspiration. When adding multiple adversaries to an encounter, you are warned if it has no attacks (but can still add them one at a time).
* **v16.5.4** 2024-02-05: When you `/start` an ambush, the initiative now goes into Change Stances phase, so the heroes can pick stances, then immediately jumps from there to Adversary Actions. Previously it was impossible for them to change stances in an ambush.
* **v16.5.5** 2024-02-18: Added `deathless-loss` game config that interprets the Deathless fell ability as a loss, not a spend, of hate, for purposes of counting how much hate can be spent in a round. Fixed a bug that caused Narvi to require an event roll at the end of the journey in some situations if the last marching roll just barely reached the end.
* **v16.5.6** 2024-02-18: Fixed language about Fierce Shot. `/start` shows a `/foe list` automatically. Foes can't spend tengwar on Pierce if they rolled a Gandalf. Gandalf is a zero, not an auto-failure, for enemies. Non-lethal attacks are indicated in the comment. Fixed a bug preventing modifiers from applying. `/attack` defaults the target when there's an encounter with adversaries, exactly one of them is engaged with the attacker, and they did not specify a target.
* **v16.6.0** 2024-02-20: Journeys now show buttons so players can quickly and easily advance their tasks with a click (unless they need options beyond hope choice, such as bonus dice or noitem, in which case, they still use the command).
* **v16.6.1** 2024-02-24: Fixed a bug with `/wound` when a character name is specified but is not found. Fixed a bug in the confirm button for `/character remove`. Added the ability for Narvi to identify a version number for the Google Sheets template.
* **v16.6.2** 2024-02-29: Added buttons for the arrival roll at the end of a journey.
* **v16.6.3** 2024-03-03: Added the Delver role to `/role` and `/report`, but it is not yet used in journeys. Journeys can have tag `moria` and/or `underground`. The `underground` tag makes Narvi ignore horses for `/roll arrival` and evaluating travel time. Fixed a bug with creating tables.
* **v16.6.4** 2024-03-03: The `moria` tag on a journey now activates the Moria journey rules: Narvi uses the relevant tables and implements the relevant journey event consequences, calculates the travel time according to the 2 mile hexes of Moria journeys, and rolls on the Random Chamber Generator when necessary. Bree-pony now allows Vigour during arrival rolls even underground, and if by happy chance the entire party has Bree-ponies, Narvi will halve travel time even underground. Note: Moria Strider Mode is not yet implemented, and Strider Mode journey takes precedence over Moria mode for now.
* **v16.6.5** 2024-03-04: `/journey quick` now supports a Moria option. Narvi now allows `/attack` rolls using a Mithril Weapon even when seized, and using a Mithril Shield buckler while using 2H weapons.
* **v16.6.6** 2024-03-07: Fixed a bug that prevented shadow recovery in `/company fellowship` when someone was near but not quite at the point of their shadow equalling their maximum hope. Mithril Weapon used while seized uses Brawling thus loses one die, per ruling by Michele Bugio. /game config deathless-loss is gone and it's always treated as a loss per ruling by Michele Bugio. 
* **v16.6.7** 2024-03-07: Minor correction to Mithril Weapon: it's not -1d, it's actually whatever brawling is, so in the rare case your mithril weapon is not your best weapon, you may actually be better with it while seized than while not.
* **v16.6.8** 2024-03-07: Some people still argue Deathless should be involuntary and therefore a loss, not a spend, so I have reinstated the `/game config deathless-loss` (it is treated as a spend unless this is set).
* **v16.6.9** 2024-03-08: Following further discussion of Michele's ruling, Narvi now automatically uses Brawling for any 1h Mithril Weapon attack if this results in a higher score than the base weapon proficiency, also accounting for Brother to Bears.
* **v16.6.10** 2024-03-11: Added Fiery Blow options to adversary special attacks. Adversary displays now better handle multi-word special attacks.
* **v16.6.11** 2024-03-14: Deathless and Spirit can be executed with `/foe ability` even if the foe is considered dead. Fixed a bug with not exiting a peril step at the right time, especially when it comes first.
* **v16.6.12** 2024-03-14: /game config deathless-loss was interfering with actually subtracting the hate rather than not counting it against the limit; now Deathless and Spirit will always reduce Hate by 1 whether your config says it's a spend or a loss.
* **v16.6.13** 2024-03-17: Fixed a problem with figuring the time limit of councils using core rules. Fixed a bug that offered Shield Thrust buttons when no shield is available in some situations.
* **v16.6.14** 2024-03-27: Added a warning sign on `/aedit create` error messages to help them stand out. `/rill arrival` and `/company arrival` now ignore the fatigue earned during the journey when deciding if you're weary since technically that fatigue doesn't apply until just after the arrival roll. Fixed a bug with embed buttons for players for arrival.
* **v16.6.15** 2024-03-29: Fixed a bug with `/wound` when you have no current character and you do not specify a character or you specify one that cannot be found. Fixed problems with localization of token picklists, common options, game config, and journey steps. French localization is now live thanks to Haladeen.
* **v16.6.16** 2024-03-29: Fixed localization problems with various buttons and embeds.
* **v16.6.17** 2024-03-31: Fixed localization problems with even more embeds.
* **v16.6.18** 2024-04-01: Fixed bug with `/attack` `/explain`. Fixed a bug preventing some grey buttons from working.
* **v16.6.19** 2024-04-02: More explicit `/explain` about weariness. Added support for specifying an events type table for a journey, and overriding it when starting a journey. Automatically switches to the solojourney for Strider Mode if not overridden (not yet handling the Moria solo stuff, but that's coming). Coded support for the events of the Redhorn Pass (redhornjourneyevents).
* **v16.6.20** 2024-04-02: Fixed a bug with `unfavoured` options throughout.
* **v16.7.0** 2024-04-03: Miserable autofailure with 6s on an `/attack` no longer offers tengwar-spend buttons. Added support for the Tales from the Lone-lands journey events tables.
* **v16.7.1** 2024-04-07: Fixed bug in `/test list` that showed the wrong TN for dread tests (the right TN was used while rolling though). Fixed a bug in `/intimidate` in riddle mode showing the hate score. Updated French translations for some journey actions. Updated text strings for the results of intimidate actions to match third printing.
* **v16.7.2** 2024-04-07: Fixed bug preventing rally being cleared when combats start and end. Qualities that strip hate, and Hammering, have additions to the output of the /attack command. Fixed a bug with `/foe list` and `/current` displays in `/start` colliding. If `/attack` or `/spend` causes an adversary to get a queued protection test, a message goes to the LM channel about it, to help the LM not miss it. Adversaries attacking during opening volley no longer gain/lose dice due to player-hero stances. Fixed bugs with journey roll buttons not always working, and with embeds crashing if too many people can click a button (in this case Narvi switches to letting everyone click it, if there's not enough room in the Discord API for recording who can).
* **v16.7.3** 2024-04-10: Added `unmounted` option to `/journey start` and `/journey quick` to exclude mounts from Arrival rolls and calculations of travel time (e.g., if this trip is on a boat). Added config entry magical-tengwar that makes magical success rolls count for one extra success.
* **v16.7.4** 2024-04-11: Fixed a bug on `/tor` command caused by the magical-tengwar flag.
* **v16.7.5** 2024-04-12: `/encounter combine` now appends a letter to the ends of adversary names to avoid duplicates.
* **v16.8.0** 2024-04-13: Implemented the new Drain attack from Moria. Fixed a bug in the display of endurance loss in certain Moria journey events.
* **v16.8.1** 2024-04-13: Implemented a host of new Moria fell abilities: Keening Wail, Drums in the Deep, Yell of Alarm, Foul Dust, Wind-like Speed, Flame of Ud�n, and Many Arms.
* **v16.8.2** 2024-04-15: Fixed bugs with Many Arms, counting H/R loss, `/status` setting tempbonus negative, displaying negative temp bonuses, displaying zero net bonuses.
* **v16.8.3** 2024-04-15: `/status` and other things that show if you're miserable also show if you're ill-favoured (shadow >= max hope) and thus due for a Bout of Madness.
* **v16.9.0** 2024-04-15: Added `/encounter generate` which fills an encounter with adversaries using the, at present, Moria Orc-Band Generator, but is also set up that in future more tables of the same sort can be used.
* **v16.9.1** 2024-04-17: `/foe attack` fierce was not checking if they had the hate/resolve to spend, but is now. `/attack` supports Furious (formerly only `/roll protection` did). Absent characters no longer trigger a confirm on `/next` during unphased initiative.
* **v16.9.2** 2024-04-17: A somewhat experimental update which shows skill names in Small Caps in many places, though not everywhere (sometimes because of spacing, such as reports; other times due to localization limitations, and sometimes because I just haven't found them yet). Added brags when hitting multiples of 100 on game and server counts.
* **v16.10.0** 2024-04-24: Adversary actions are listed and tracked during Opening Volley, but since many adversaries won't have anything they can or want to do during opening volley (and it's not easy for Narvi to know which), advancing out of this phase will not depend on them.
* **v16.10.1** 2024-04-25: Moria solo journey tables and associated rules, including switching to an alternate version of the Chance Meeting table, Right Way also canceling fatigue in Strider mode. Rolling Eye on the ill-fortune table now handles the two point Eye gain. Better error indications when failures happen with the `/table` command or table rolls or validations. Note: the Moria Alpha is missing an event detail table for Dread and Wonder of Moria, so Narvi will fall back on the non-solo tables in this case.
* **v16.10.2** 2024-04-25: Moria solo journeys are now configured assuming that the Chance Meeting table in the alpha was just a titling error and that that was actually Dread and Wonder of Moria.
* **v16.10.3** 2024-04-28: Fixed a bug with combats without encounters with opening volleys throwing an error when started. Fixed bug with foe hate spends not adding success dice. Fixed a bug with localization on messages while spending hate. Fixed a bug with localization on lookup tables.
* **v16.10.4** 2024-04-28: Fixed a bug with action and hate spend not resetting in unphased initiative for adversaries who acted during opening volley. If no weapon is specified in `/attack`, Narvi now defaults to only looking at ranged weapons (as indicated by the checkmark under the bow symbol in the weapons list) to automatically choose a weapon, when you are in Opening Volley or Rearward Stance.
* **v16.10.5** 2024-04-30: Added `/game config unlocked-commands` allowing all players to do loremaster commands. Added `/game config unthrowable-daggers` which overrides the ranged flag on any weapon with 'dagger' in the name.
* **v16.10.6** 2024-05-02: Partially reversed a change made in v16.6.15 to account for an inconsistency: where you are on a journey after a marching roll depends on what you want to know. If you're doing a journey event, you are not yet into the hex the march will lead to, but if you're seeing whether you've arrived and thus don't even have another event, you are in that hex.
* **v16.10.7** 2024-05-04: Fixed a bug with untreated wounds being considered as poison when determining prolonged and safe rest recovery. Fixed a bug with journeys showing the status in terms of having finished the march instead of where the event will happen (which someone's going to report as a bug again somewhere else, I bet...)
* **v16.10.8** 2024-05-11: Fixed a bug with the game config entries for Eleven Pierces and Eleven Can Pierce. (The former takes precedence.)
* **v16.10.9** 2024-05-12: Fixed a very rare bug that would cause a crash in various displays if an adversary is engaged with a player-hero that no longer exists.
* **v16.10.10** 2024-05-18: Fixed a bug with choosing adversary-specific options like hate spends on a `/foe roll` command without an adversary.
* **v16.10.11** 2024-05-20: Added `/game config shadow-eye` for automatically accruing eye whenever shadow is gained outside of combat. Added a solicitation when creating a new game on a server if no other games already exist on that server.
* **v16.10.12** 2024-05-24: Fixed a bug that could cause a perilous area to be reentered in certain combinations of step sizes adjoining. Updates to French translation.
* **v16.10.13** 2024-05-25: Knockback automatically clears in unphased initiative.
* **v16.10.14** 2024-05-25: Protective code handles localization files with strings too long for Discord, by warning in the log and patching with truncation for now.
* **v16.10.15** 2024-05-26: Protective code for absurdly big encounters.
* **v16.10.16** 2024-05-26: Allow stance changes during opening volley phase.
* **v16.11.0** 2024-05-27: `/report` for encumbrance and fatigue now shows pairs of lines, one with endurance, one with load, and no longer uses halved icons, so you can see how your weariness is coming along and where it's coming from. Sorry this will look terrible on mobile.
* **v16.11.1** 2024-06-01: Wording tweaks. Fixed a bug that could cause double-dipping of fatigue with `journey-tiring` in some situations.
* **v16.12.0** 2024-06-01: Support for player-heroes having multiple journey roles.
* **v16.12.1** 2024-06-14: New `/game config combat-advhealth` makes the `/foe list` use the emoji to give a broad idea of health of the foe. New `/stance unchanged` command so you can make clear you're keeping your stance -- so Narvi will notify the loremaster when everyone has weighed in.
* **v16.12.2** 2024-06-21: Fixed a bug where a miserable autofailure on `/attack` could still count as and get treated as a success. French language updates.
* **v16.12.3** 2024-06-23: Clarified the message for Hammering firing. If the LM changes stance for a character who was marked for the current phase, Narvi correctly removes them from the people who need to act that phase. Shield Thrust is not offered in opening volley, if the foe's attribute is too high, or the foe is already pushed back. During opening volley, Narvi attempts to automatically pick a ranged attack for an adversary if no attack is specified. `/start` ambush option now also supports when all adversaries are surprised.
* **v16.12.4** 2024-07-02: Even in a fight where the heroes are ambushed, Opening Volley comes up (the adversaries might have an attack in opening volley!) and Stance Change still comes up (so the heroes can pick stances instead of landing in whatever they were in during the last fight).
* **v16.12.5** 2024-07-03: Adversary surprise option in `/start` has been corrected to use third printing rules -- adversaries do not lose a turn, they simply lose -1d during the first round and get no opening volley.
* **v16.13.0** 2024-07-06: Debut of the `/go` command and Interactive Interfaces, featuring only two panels: no game (a bunch of dice rolling options) and with a game (options for character info, gaining and losing hope and shadow, and rolling on skills).
* **v16.13.1** 2024-07-07: `/go` now handles Expanse and GURPS rolls (no panel required). New panels for pending protection tests and shadow tests, and for stance change phase.
* **v16.13.2** 2024-07-07: Protection and Shadow Test panels no longer show inspired and magical success roll buttons. Fixed a bug with the initiative ready to advance dialog. `/go` now offers a panel during opening volley and whatever phase your action comes in; the panel offers all available actions relevant (based on phase, stance, and siezed status) and a list of targets (the icons here respect the `/game config combat-advhealth` flag), along with roll buttons. So it can be used to do attacks, seek advantage, combat tasks, and escaping seizes.
* **v16.13.3** 2024-07-07: Fixed a bug with `/go` rolls costing hope even if not using it, and not using the selected weapon. Fixed a bug in the Google API on character callbacks on retry. `/feat` no longer requires a number of dice (defaulting to 1). Adversaries out of combat no longer show their bonuses in initiative.
* **v16.13.4** 2024-07-08: New `/go` panels for adversary phase (various methods of marking harm to yourself), councils (various rolls towards a council, with council skills on top), and skill endeavours (same, but with the normal order of skills). A few bug fixes for handling skill rolls from interactive interface panels.
* **v16.13.5** 2024-07-08: New `/go` panels for treating wounds, Cortex rolls, and Coyote & Crow rolls. Unless someone comes up with an idea for things you might want to have happen in a journey, this concludes the planned player-focused Interactive Interfaces panels (loremaster ones will be my next project).
* **v16.13.6** 2024-07-10: New `/go` panels for loremasters while in a journey, council, or skill endeavour; or if any adversaries have protection tests. The latter pops up automatically in the GM channel whenever an adversary gets a protection test. `/test list` now supports `adversaries`. Fixed a bug with `/encounter set` error messages being reversed.
* **v16.13.7** 2024-07-11: Fixed a bug with Narvi's handling of using an incompletely built adversary in an encounter. Fixed a bug with hideous toughness sometimes setting full and sometimes half depending on the way it was rolled. Added a /game config that prevents Hammering from giving a foe more than one round of knockback. Fixed a bug with the confirm dialog for /encounter delete.
* **v16.13.8** 2024-07-14: Fixed a bug when Narvi looks for a default attack for an adversary during opening volley when no attack is specified.
* **v16.13.9** 2024-07-17: Two new LM panels: the menu of panels, and the Shadow Tests panel (the other buttons are there but go to a 'under construction' message for now).
* **v16.13.10** 2024-07-22: Fixed a bug in applying adversary surprise penalty die. Possibly fixed a rare situation where arrival clears too much fatigue (but cannot reproduce). Changed checking for traits to ignore dashes and spaces so "Bree Pony" will match "Bree-pony". `/report damage` now notes conditions. Some tweaks to try to avoid Narvi not identifying correctly who can do a journey event (again cannot reproduce).
* **v16.13.11** 2024-07-26: Flame of Hope now shows the endurance increases as they happen. New LM panels: start endeavour, start council; start journey; start combat.
* **v16.13.12** 2024-07-27: Added a combat modifier LM panel.
* **v17.0.0** 2024-07-27: Added eye awareness and company action LM panels; this concludes the panels currently planned. Added a `/go here` option that lets you specify where the output of `/go` actions should be, so you can have the panels (which can be spammy) in a private place and their results in a more public game channel. This completes the Interactive Interface.
* **v17.0.1** 2024-07-29: Fixed a bug with `/go` panel for weapons using the wrong locale. Fixed a bug with prolonged rest restoring 1 hope to someone with 0. `/company prolonged` and `/company safe` now use the specified `value` as the number of days, to easily skip ahead over a longer rest. Fixed a problem with foe attacks in opening volley not finding the ranged weapon if the name had mixed case. Fixed a problem with `/go here` for more complicated outputs like skill rolls. `/go here` is now specific to each game (but to players, not to characters).
* **v17.0.2** 2024-08-01: Fixed a bug with `/go` panel for no games. Fixed a bug for games without characters (why?).
* **v17.0.3** 2024-08-01: Apparently someone set an LM channel and then deleted the channel, causing a long string of Discord errors. Added code to disable LM channels and gohere channels if they point to somewhere that does not exist or is inaccessible.
* **v17.0.4** 2024-08-09: Fixed a bug with choosing a better skill in Skirmish stance when doing the `/gainground` action. Fixed a bug with identifying variations on Virtues when people did odd spacing or punctuation or left out "the".
* **v17.0.5** 2024-08-12: Fixed a bug with `/go` with solely-two-handed weapons during action phases. Fixed a bug that could create a protection test on the wrong adversary, or none at all, when multiple people are spending Tengwar at once. Changed most `/go` panels to offer links to something relevant (typically the character sheet of the acting character) instead of only to Narvi's home page.
* **v17.0.6** 2024-08-15: Added buttons to the Loremaster's `/go` panel when a combat is underway to change the light levels (not available in any commands).
* **v17.0.7** 2024-08-19: Stones thrown now do 1 damage, per the table on p100. Stones are always available as a weapon, not just to those Sure at the Mark.
* **v17.0.8** 2024-08-28: `/game config adv-weary-spend' means adversaries spending their last Hate/Resolve on a roll do not become weary until after that roll.
* **v17.0.9** 2024-09-02: Fierce Shot only applies if you're in Rearward or during Opening Volley. Tengwar spends now specify the character name and, where applicable, the adversary name (since it can be confusing which one is which person's when many people are acting at once). `/foe` will not allow an action by a knocked back adversary (typically due to Hammering), but you can `/foe lose` to clear the knockback and try again. The `/go` panels that show skills for a player now show a brief reminder of favored and pips. `/set`, `/gain`, and `/lose` now work with AP, SP, and Treasure, so these can be handed out individually, not only to the whole company. `/company treasure` now puts the treasure into your Carried Treasure, where it affects Load. The `/encounter` command now warns you if you are editing an encounter while you have a different encounter active in combat.
* **v17.0.10** 2024-09-02: Cram offers no healing to the wounded during Short Rests per Michele Bugio ruling.
* **v17.0.11** 2024-09-12: Damage report shows days left on wounds.
* **v17.1.0** 2024-09-14: `/songbook use` followed by `/skill song` can automatically set an Unweary flag (also can be set manually with `/status`) which prevents weariness from taking effect for the duration of the combat, journey, or council. Songs with a bonus in the songbook also apply the bonus to the roll.
* **v17.1.1** 2024-09-15: Fixed knockback preventing foes from rolling protection. Fixed bugs with line placement when showing targets during Heavy Blow spends. Virtue of Kings adds a message suggesting a possible reroll on Eye rolls. Rests now bring you from 0 to 1 endurance even if you're wounded or poisoned.
* **v17.1.2** 2024-09-16: Improved the LM `/go` panel that comes up when there are pending adversary protection tests to list the pending tests. Added some code to ensure that the dropdown menu correctly selects the adversary to roll for.
* **v17.1.3** 2024-09-17: Fixed a bug with `/feat` for favoured rolls.
* **v17.1.4** 2024-10-05: Thrown stones can no longer Fend Off. Shield Thrust is never possible from Rearward. A player using the `character` option of `/status` to choose themselves will no longer cause a "only LMs can use this" error. When Google's API barfs a page of HTML instead of an error message, Narvi tries to extract the error message. Fixed a bug that extended adversary ambush over all rounds. Fixed a bug that caused Shadow tests to give the wrong amount of Eye increase when shadow is gained out of combat with the `shadow-eye` configuration is set. Added a display of the eye update when this happens. Using a song from the songbook will only afford the song bonus and a chance to gain the unweary condition for a single `/skill song` roll per active character. Players can use `/next` to indicate that they have taken all their actions, in cases when they do things Narvi doesn't know complete their action for the phase.
* **v17.1.5** 2024-10-05: Added a `/raven` command for the dice for the Raven RPG.
* **v17.1.6** 2024-10-07: Raise hope from 0 to 1 before doing the normal hope recovery for a regular Fellowship Phase (assuming a prolonged rest also happens in there somewhere). Fixed a bug with players using `/next`. Changed the icon on `/go` when in combat that indicates engagement.
* **v17.1.7** 2024-10-12: Fixed a bug with some commands that display embeds, such as `/go`, when run without a game. Fixed a bug with `/company` when someone has 0 Hope.
* **v17.1.8** 2024-10-30: Fixed a bug with `/test list` when adversaries are specified but there is no encounter. Broadened the set of Google API errors that cause a retry when updating a character sheet.
* **v17.1.9** 2024-11-03: Narvi respects if a weapon is unequipped (unchecked) when picking the default weapon for `/attack` and when picking weapons to show on `/go`. Fixed hate drain for In Defiance of Evil. Fixed a problem with very long messages to LM channels. Foe-Slaying should only be shown when there's a protection roll. Added `/foe damage` and `/foe heal` as synonyms for the corresponding `gain` and `lose` for endurance. Fixed a display problem with `/sheet weapons`.
* **v17.2.0** 2024-11-04: Updated Eris to v0.18.0. Updated `/version` display.
* **v17.2.1** 2024-11-05: Eris v0.18.0 seems to bring us over the line I have long been expecting Discord to bring us over: Narvi can no longer see message content. Fortunately due to slash commands we only needed this for one thing, blocking non-resource posts in resource channels. This version temporarily mostly disables that function while we try to figure out if there's any way around this.
* **v17.2.2** 2024-11-05: Resource channel monitoring is completely removed. This is the last thing that used the message content intent.
* **v17.3.0** 2024-11-09: Introduces table groups. Create and update with `/table creategroup` and delete with `/table deletegroup`. Existing commands now work with them, if you specify the group name as a shortname: `/table list`, `/table show`, and `/table roll`.
* **v17.3.1** 2024-11-10: Fixed a bug with Foe-Slaying queueing up a protection test without the ill-favour.
* **v17.3.2** 2024-11-13: Fixed a bug with councils applying attitudes to introduction rolls. Fixed ambush status carrying from one combat to another if the first is ended before the status is cleared.
* **v17.3.3** 2024-11-24: Hate strips due to Dull-witted now are shown in the LM channel just like ones from Craven do. Lengthened the engagement column in `/foe list` for Might 2+ adversaries. Fixed a bug that made Song always be a success when using something from the Songbook. `/status` defaults value to 1 if you specify a status but not a value.
* **v17.3.4** 2024-11-25: If you specify a `/foe attack` for an adversary with Might 2 or higher that already has engagement, and don't specify a target, and then do it again, the subsequent attacks will cycle through the engagement automatically. That is, if the Balrog is engaged with both Legolas and Gimli, the first `/foe attack` without a target will target Legolas, then the second will target Gimli.
* **v17.3.5** 2024-11-30: `/attack` will not let you attack a dead adversary (so you don't have to undo things like hope spends or temporary bonus burns when you redo it).
* **v17.4.0** 2024-11-30: New `/environment` command to do environmental damage (cold, fire, etc.) as per Sources of Injury on p143 of the core book, to one character or the whole company. Ancient Fire now protects against fire damage as it should. Endurance damage can create wounds or dying as appropriate to the type when they reduce a hero to zero endurance.
* **v17.4.1** 2024-12-02: Somehow someone was checking the weary status of a character that didn't exist so I put in protections against that, though I have no idea how it happened. Encounters can now have an `/environment` source of injury that automatically happens on a specific round or every round.
* **v17.4.2** 2024-12-03: Add a Loremaster `/go` panel for doing environmental sources of injury.
* **v17.4.3** 2024-12-04: Hopefully fixed a situation where the bot can't find a person's DM channel. Force shortnames of adversaries to lowercase when adding them in `/encounter adversary`.
* **v17.4.4** 2024-12-06: Fixed a bug preventing the arrival roll embed from appearing if you delete the journey during a `/journey end`. Fixed some displays of adversary names in tengwar spends showing null when the Loremaster does not use adversaries. Fixed a bomb when not using encounters and doing a `/next`.
* **v17.4.5** 2024-12-07: Fixed a bug with the display and setting of the bleeding status. Fixed a bug with the defaulting of the `/status` command to setting a status true. Added a Bitten condition which is like Seized plus Bleeding 2. Fixed a bug where initiative would skip over Forward if someone was forced by seized, bitten, or drained into Forward but was not otherwise in it.
* **v17.4.6** 2024-12-10: Fixed a bug with knockback refunds sometimes being too high. Added additional explanations when foes attack but the game does not use encounters or adversaries. New `/game config` option `unlocked-counters` lets players set and manipulate counters (`/counter`) but not secret ones. Failure to find an encounter in `/start` does not start an incomplete combat. New options for `ambush` in `/start` allow only some player-heroes to be ambushed (in this case, knockback is used to remind you to skip their actions on the first round).
* **v17.4.7** 2024-12-13: Reorganized the `/game config` menu into separate categories due to Discord limitations in how many options can be in one menu (similar to how `/report` is divided). New config `knockback-defensive` makes a player-hero's stance assumed as Defensive whenever knocked back, so they don't have to remember to change, and change back. Fixed a bug that explicitly setting feat options in `/attack` could be overridden by things like Furious and Wind-like Speed. Fierce Shot and Splitting Blow only show their message when edge is made, as with Foe-slaying.
* **v17.5.0** 2024-12-14: Added a `/report` that shows a log of combat, during or after. The log is made up of the text of various outputs, so it might be a bit clunky in places, but it's all in there.
* **v17.5.1** 2024-12-15: Fixed a sometimes misstated edge value in spend pierce messages. Changed the autoswitch message to always appear first.
* **v17.5.2** 2025-01-06: Fixed confirm dialog buttons for hexflower delete, journey delete, journey clear, adversary delete. Added `/foe engage back` to clear engagement and mark the adversary to no longer do auto-engage, because they're standing back, perhaps using ranged weapons; use `/foe engage none` to reset this. `/foe ability` for Snake-like Speed now lets you set a foe to automatically use it when it can, with some reserve of hate/resolve it maintains. Realms of the Three Rings alpha changes: /foe ability bodiless`: spend 1 point of Hate to cancel a wound. Bodiless works like Hideous Toughness to automatically queue protection tests and restore to full. Dreadful Spells now supports a Poison Concoction type which will do severe endurance damage due to poison on failing the test. Dreadful Spells now supports a Visions of Torment type which will do double the shadow loss on failure with an auto-wound if it drops to 0. New Frostbreath ability targets someone with a severe (by default - override with points=1 or 3) cold endurance loss. Lembas reduces fatigue accrual and turns prolonged rests into safe rests, doubling the fatigue recovery resulting.
* **v17.5.3** 2025-01-07: The message about contradicting selected encounter and combat encounter no longer shows if you don't have a combat encounter in the first place (i.e., if you did not specify one when you did `/start`). Added the encounter name to the combat log entry for starting a combat. Fixed parsing of the private flag in the `/counter` command.
* **v17.5.4** 2025-01-07: New `/game config` flag under combat, Remind Engagement, makes a bold message appear when you leave Opening Volley phase to remind you to set engagement.
* **v17.5.5** 2025-01-19: `/sheet` will not object you're not the Loremaster if you specify yourself. Adversary attacks without an injury rating (such as the Large Spider's Webs attack) no longer trigger a piercing blow or offer a Pierce button for tengwar spends. Fixed a bug with `/go` when a player has a game but isn't in the game.
* **v17.5.6** 2025-01-27: Softened the wording of the 'initiative ready to advance' message to make clear that Narvi may not always know with certainty that there aren't action still to be done (e.g., when an adversary has a ranged attack that it doesn't recognize during Opening Volley, when an adversary can do a fell ability still, or when a player-hero gets an extra free secondary action). Fixed poison setting the victim to dying if they get to 0 endurance. `/explain` message on an arrival roll will make clear that the recently accrued fatigue is being backed out since it does not kick in until after an Arrival roll is done. Major change to adversary attacks: instead of inferring whether they are ranged, Narvi now has you explicitly set that when editing an adversary with `/aedit` or `/encounter`, and uses this to determine such things as whether the attack can be used in Opening Volleys. A bow and arrow next to the name is used to indicate whether an attack is ranged. All existing attacks were automatically set according to the guess it used to use (based on name matching). This also allows a correction in Narvi's behavior: adversaries making ranged attacks no longer implement bonus or penalty dice based on the target's stance.
* **v17.5.7** 2025-01-27: New `/company session` command that awards the specified amount in both AP and SP and then does an automatic `/pool distribute`, for those session ends where Narvi's smart pool distribution is good enough.
* **v17.5.8** 2025-02-01: Handedness resets to default 1h at `/start` and `/end`, in case loremasters do foe attacks outside of a declared combat, so shields still apply by default. Fixed some fell ability amounts not correctly applying to the resulting protection tests or endurance loss effects. New /game config autotreat-poison that says if you use /treat to treat a wound successfully but the person also has poison we also clear the poison for free.
* **v17.5.9** 2025-02-03: Fixed the auto-engagement warning to only appear once in round 1 in the change stance phase. `/start` initiated from `/go` will send an encounter's image to the `/go here` channel if one is configured. Added a bonus pulldown to the various player panels that involve making rolls since setting a bonus seems to be a common reason that one can't use `/go` and has to learn the commands.
* **v17.6.0** 2025-02-10: Rewrote the `noitem` flag so that, if you specify it, the language is clear about whether you're using items or not. The `/game config` still works as before by setting the default. Made the `/mellon` command's output ephemeral, but still working on `/go`.
* **v17.6.1** 2025-02-12: Added a better message if you try to `/game add` when not in a server so Narvi can't find users. All `/go` panels are now ephemeral - only seen by the requester.
* **v17.6.2** 2025-03-03: Fixed an error message with `/aedit delete`. Fixed some internal bugs with handling missing fell abilities and the ephemeral flag.
* **v17.7.0** 2025-03-03: All `/go here` functionality is removed now that `/go` is ephemeral so it's no longer necessary. Fixed the display of some `/go` menus to not do Weirdly Proper Title Casing. Loremaster `/go` panels that come from the main LM panel will also be ephemeral.
* **v17.7.1** 2025-03-04: The value for `/eye base` is now optional, and if omitted, Narvi calculates it itself. Note though that a Famous War Gear item with only one regular quality unlocked will look like an ordinary item to Narvi so it will not add 1 to the base for that.
* **v17.7.2** 2025-03-04: When selecting things on the menus from an ephemeral `/go` panel, the message showing that you picked something is also ephemeral.
