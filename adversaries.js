// ADVERSARIES -----------------------------------------------------------------

const filehandling = require('./filehandling.js');
const stringutil = require('./stringutil.js');
const table = require('./table.js');
const slashcommands = require('./slashcommands.js');
const embeds = require('./embeds.js');

var adversaries;
const adversariesFilename = 'adversaries.json';

/*
adversaries: {
	"tags": [...],
	"adversaries": {
		"shortname": {
			tags: ['core','strider',...]
			name: '...',
			description: '...',
			image
			creator: userID,
			createdate: today,
			published: false,
			features
			attribute
			hrtype
			hrscore
			might
			endurance
			parry
			armour
			banetype
			size
			attacks: [{
				name
				ranged
				dice
				damage
				injury
				special
				comment
				}]
			abilities: [{
				name
				type
				amount
				comment
				}]
		}, ...
	}
*/

// load the adversaries file on startup
function adversaries_load() {

	adversaries = filehandling.readJSONFile(adversariesFilename);
	if (adversaries == null || Object.keys(adversaries).length === 0)	{
		adversaries = {
			tags: ['tor', 'aime', 'lotrrp', 'core', 'strider', 'starter', 'homebrew', 'ruins', 'english', 'espanol', 'men', 'evilmen', 'southerners', 'ruffians', 'orcs', 'chieftains', 'goblins', 'archers', 'guards', 'soldiers', 'trolls', 'robbers', 'undead', 'wraiths', 'wights', 'wolves', 'beasts', 'wargs', 'spiders','nameless','blacknumenoreans','dwarves','elves', 'hobbits', 'wyrms', 'wyverns', 'ents', 'eagles', 'balrogs', 'nazgul', 'mumakil', 'ogres', 'birds'],
			adversaries: {}
		};
		adversaries_save();
	}
	let changes = false;
	for (let adversaryName in adversaries.adversaries) {
		// changes to adversaries
		/*
		if (adversaryName != stringutil.lowerCase(adversaryName)) {
			newadversaryName = stringutil.lowerCase(adversaryName);
			changes = true;
			adversaries.adversaries[newadversaryName] = adversaries.adversaries[adversaryName];
			delete adversaries.adversaries[adversaryName];
		}
		// changes to adversary attacks
		adversaries.adversaries[adversaryName].attacks.forEach(attack => {
			if (attack.ranged == undefined)	{
				changes = true;
				attack.ranged = false;
				['spear','bow','knife','web'].forEach(w => {
					if (attack.name.toLowerCase().includes(w)) attack.ranged = true;
				});
			}
		});
		*/
	}
	if (changes) adversaries_save();
}

// save the journeys file after each change
function adversaries_save() {
	return filehandling.writeJSONFile(adversariesFilename, adversaries);
}

// HOST FUNCTIONS -------------------------------------------------------------

// add a tag
function adversaries_tag_add(tagName) {
	if (adversaries.tags.includes(stringutil.lowerCase(tagName))) return false;
	adversaries.tags.push(stringutil.lowerCase(tagName));
	adversaries_save();
	return true;
}

// remove a tag
function adversaries_tag_remove(tagName) {
	if (!adversaries.tags.includes(stringutil.lowerCase(tagName))) return false;
	adversaries.tags = adversaries.tags.filter(value => value != stringutil.lowerCase(tagName));
	adversaries_save();
	return true;
}

// verify if a tag is available
function adversaries_tag_verify(tagName) {
	return adversaries.tags.includes(stringutil.lowerCase(tagName));
}

// return a tag list (e.g., for displaying)
function adversaries_tag_list() {
	return adversaries.tags;
}

// take over ownership of an adversary
function adversaries_claim(adversaryName, userID) {
	adversaries.adversaries[adversaryName].creator = userID;
}

// ADVERSARY CREATION AND EDITING ---------------------------------------------

// determine if an adversary can be read or written by a particular user
function adversaries_accessible(adversaryName, userID, hostUser, type) {
	// if the journey doesn't exist, you can't access it
	if (adversaries.adversaries[adversaryName] == null) return false;
	// the host user can do everything to all journeys
	if (userID == hostUser) return true;
	// if the journey is owned by this user, they can do anything
	if (adversaries.adversaries[adversaryName].creator == userID) return true;
	// if we're asking for write access, and no earlier test passed, they can't do it
	if (type == 'write') return false;
	// so we're asking for read now, and it's not the user's own journey
	// otherwise it only depends on whether the adversary is published, 
	if (adversaries.adversaries[adversaryName].published) return true;
	return false;
}

// return an adversary structure so it can be copied into an encounter
function adversaries_fetch(adversaryName) {
	return adversaries.adversaries[adversaryName];
}

// /aedit create <shortname>
function adversaries_create(adversaryName, userID) {
	adversaryName = stringutil.lowerCase(adversaryName);

	// if an adversary already exists with that name, return false
	if (adversaries.adversaries[adversaryName] != null) return false;

	// create an adversary with that name and fill in some basic info:
	adversaries.adversaries[adversaryName] = {
		tags: [],
		name: stringutil.titleCaseWords(adversaryName),
		description: '',
		image: '',
		creator: userID,
		createdate: Date.now(),
		published: false,
		features: '',
		attribute: 1,
		hrtype: 'hate',
		hrscore: 1,
		might: 1,
		endurance: 12,
		parry: 0,
		armour: 1,
		banetype: '',
		size: 'Medium',
		attacks: [],
		abilities: []
	};
	adversaries_save();
	return true;
}

// /aedit copy
function adversaries_copy(adversaryName, newadversaryName, userID, locale, reverse) {
	if (adversaries.adversaries[newadversaryName] != null) return 'adversary-exists';
	// build the copy
	newadversaryName = stringutil.lowerCase(newadversaryName);
	adversaries.adversaries[newadversaryName] = {
		tags: adversaries.adversaries[adversaryName].tags,
		name: adversaries.adversaries[adversaryName].name,
		description: String(adversaries.adversaries[adversaryName].description + ' ' + slashcommands.localizedString(locale, 'adversary-copynote',[adversaryName]) ).trim(),
		image: adversaries.adversaries[adversaryName].image,
		creator: userID,
		createdate: Date.now(),
		published: false,
		features: adversaries.adversaries[adversaryName].features,
		attribute: adversaries.adversaries[adversaryName].attribute,
		hrtype: adversaries.adversaries[adversaryName].hrtype,
		hrscore: adversaries.adversaries[adversaryName].hrscore,
		might: adversaries.adversaries[adversaryName].might,
		endurance: adversaries.adversaries[adversaryName].endurance,
		parry: adversaries.adversaries[adversaryName].parry,
		armour: adversaries.adversaries[adversaryName].armour,
		banetype: adversaries.adversaries[adversaryName].banetype,
		size: adversaries.adversaries[adversaryName].size,
		attacks: [...adversaries.adversaries[adversaryName].attacks],
		abilities: [...adversaries.adversaries[adversaryName].abilities]
	};
	adversaries_save();
	return '';
}

// basic adversary editing
function adversaries_edit_validate(locale, field, value) {
	if (field == 'name' || field == 'description' || field == 'features' || field == 'banetype' || field == 'size') value = stringutil.mildSanitizeString(value);
	if (field == 'image' && value == slashcommands.localizedString(locale, 'none',[])) value = '';
	if (field == 'image' && value != '' && !value.startsWith('http://') && !value.startsWith('https://' )) return [null,'error-badurl'];
	if (field == 'name' && value.length > 120) return [null,'adversary-nametoolong'];
	if (field == 'attribute' || field == 'might' || field == 'endurance' || field == 'parry' || field == 'armour' || field == 'hrscore') {
		if (isNaN(value)) value == -1; else value = Number(value);
		if (value < 0 || value > (field == 'endurance' ? 1000 : 20)) return 'adversary-valuetoohigh';
	}
	if (field == 'hrtype') {
		let hate = slashcommands.localizedString(locale, 'adversary-hate',[]);
		let resolve = slashcommands.localizedString(locale, 'adversary-resolve',[]);
		value = stringutil.lowerCase(value);
		if (value == hate) value = 'hate';
		else if (value == resolve) value = 'resolve';
		else if (hate.startsWith(value)) value = 'hate';
		else if (resolve.startsWith(value)) value = 'resolve';
		else return [null,'adversary-hateorresolve'];
	}
	if (field == 'tags') {
		let r = [];
		let problem = null;
		value = value.trim().replace(/,/g,' ').replace(/  +/g, ' ');
		value.split(' ').forEach(tag => {
			if (!adversaries_tag_verify(tag)) problem = 'error-unknowntag';
			r.push(tag);
		});
		if (problem) return [null,problem];
		value = r;
	}
	return [value, null];
}

function adversaries_edit(adversaryName, userID, hostUser, locale, field, value) {
	if (!adversaries_accessible(adversaryName, userID, hostUser, 'write')) return 'adversary-notexists';
	// validation
	let error;
	[value, error] = adversaries_edit_validate(locale, field, value);
	if (error) return error;
	if (field == 'published' && value) {
		// must have a name that's not the same as the shortname
		if (adversaries.adversaries[adversaryName].name == '') {
			return 'adversary-noname';
		}
		// must have at least one tag
		if (adversaries.adversaries[adversaryName].tags.length == 0) {
			return 'adversary-notags';
		}
	}
	adversaries.adversaries[adversaryName][field] = value;
	adversaries_save();
	return '';
}

// /aedit delete confirm
function adversaries_delete(adversaryName, userID, hostUser) {
	if (!adversaries_accessible(adversaryName, userID, hostUser, 'write')) return 'adversary-notexists';
	// do not allow if it's published
	if (adversaries.adversaries[adversaryName].published) return 'adversary-unpublishfirst';
	adversaries_delete_immediately(adversaryName);
	return '';
}

// directly delete
function adversaries_delete_immediately(adversaryName) {
	delete adversaries.adversaries[adversaryName];
	adversaries_save();
}

function adversaries_show(adversaryName, locale, userID, hostUser, userNameFromID) {
	if (!adversaries_accessible(adversaryName, userID, hostUser, 'read')) return slashcommands.localizedString(locale,'adversary-notexists',[adversaryName]);
	let theEmbed = embeds.buildAdversary(locale, adversaryName, adversaries.adversaries[adversaryName]);
	let created = stringutil.titleCaseWords(slashcommands.localizedString(locale, 'created', [])) + ':  <t:' + String(Math.floor(adversaries.adversaries[adversaryName].createdate/1000)) + ':F> ' + slashcommands.localizedString(locale, 'by', []) + ' ' + userNameFromID(adversaries.adversaries[adversaryName].creator) + '\n';
	if (adversaries.adversaries[adversaryName].image != '') return [{content: created, embed: theEmbed}, adversaries.adversaries[adversaryName].image];
	return {content: created, embed: theEmbed};
}

// add an attack to an adversary
function adversaries_attack_validate(dice, damage, injury) {
	if (damage < 0 || damage > 20 || injury < 0 || injury > 30 || dice < 0 || dice > 10) return false;
	return true;
}

function adversaries_attack(adversaryName, userID, hostUser, name, dice, damage, injury, ranged, special, comment) {
	if (!adversaries_accessible(adversaryName, userID, hostUser, 'write')) return 'adversary-notexists';

	// validation
	if (!adversaries_attack_validate(dice, damage, injury)) return 'adversary-badattack';

	// check for existing one to replace
	let r = '';
	adversaries.adversaries[adversaryName].attacks.forEach(attack => {
		if (stringutil.caseInsensitiveMatch(attack.name, name)) {
			attack.dice = dice;
			attack.damage = damage;
			attack.injury = injury;
			attack.ranged = ranged;
			attack.special = special;
			attack.comment = stringutil.mildSanitizeString(comment.trim());
			adversaries_save();
			r = adversaries.adversaries[adversaryName].attacks.length;
		}
	});
	if (r != '') return r;

	// create a new one
	let newAttack = {
		'name': stringutil.mildSanitizeString(name),
		'dice': dice,
		'damage': damage,
		'injury': injury,
		'ranged': ranged,
		'special': special,
		'comment': stringutil.mildSanitizeString(comment.trim())
	};
	adversaries.adversaries[adversaryName].attacks.push(newAttack);
	adversaries_save();
	return adversaries.adversaries[adversaryName].attacks.length;
}

// add an ability to an adversary
function adversaries_ability_validate(name, type, amount, comment) {
	let lname = stringutil.lowerCase(name);
	if (lname.startsWith('dreadful spells')) {
		let ltype = stringutil.lowerCase(type);
		if (ltype == 'wight') type = 'unconsciousness';
		if (ltype == 'shadow' || ltype.startsWith('lure')) type = 'madness';
		// ltype = stringutil.lowerCase(type);
		// if (!(ltype.startsWith('other') || ltype.startsWith('unconscious') || ltype.startsWith('misfortune') || ltype.startsWith('madness') || ltype.startsWith('haunted') || ltype.startsWith('daunted') || ltype.startsWith('seized') || ltype.startsWith('shieldsmashed') || ltype.startsWith('dismayed') || ltype.startsWith('dreaming') || ltype.startsWith('bleeding'))) return [name, type, amount, comment, 'adversary-error-dreadful'];
		if (amount < 1) amount = 3;
	}
	if (lname.startsWith('hatred') && type == '') {
		return [name, type, amount, comment, 'adversary-error-hatred'];
	}
	if (lname.startsWith('strike fear') && amount < 1) amount = 1;
	if (lname.startsWith('unnatural hunger') && amount < 1) amount = 3;
	name = stringutil.mildSanitizeString(name);
	type = stringutil.mildSanitizeString(type);
	comment = stringutil.mildSanitizeString(comment.trim());

	// if no comment but something else has the exact configuration, use its comment
	if (comment == '') {
		for (let aName in adversaries.adversaries) {
			adversaries.adversaries[aName].abilities.forEach(ability => {
				if (stringutil.caseInsensitiveMatch(ability.name, name) && ability.type == type && ability.amount == amount && ability.comment != '') {
					comment = ability.comment;
				}
			});
		}
	} else if (comment == slashcommands.localizedString(locale, 'none', [])) {
		comment = '';
	}
	return [name, type, amount, comment, null];
}

function adversaries_ability(adversaryName, userID, hostUser, name, type, amount, comment) {
	if (!adversaries_accessible(adversaryName, userID, hostUser, 'write')) return 'adversary-notexists';

	// validation: certain fell abilities should be checked for type or amount
	let error;
	[name, type, amount, comment, error] = adversaries_ability_validate(name, type, amount, comment);
	if (error) return error;

	// check for existing one to replace
	let r = '';
	adversaries.adversaries[adversaryName].abilities.forEach(ability => {
		if (stringutil.caseInsensitiveMatch(ability.name, name)) {
			ability.type = type;
			ability.amount = amount;
			ability.comment = comment.trim();
			adversaries_save();
			r = adversaries.adversaries[adversaryName].abilities.length;
		}
	});
	if (r != '') return r;

	// create a new one
	let newAbility = {
		'name': name,
		'type': type,
		'amount': amount,
		'comment': comment
	};
	adversaries.adversaries[adversaryName].abilities.push(newAbility);
	adversaries_save();
	return adversaries.adversaries[adversaryName].abilities.length;
}

function adversaries_clear(adversaryName, userID, hostUser, type) {
	if (!adversaries_accessible(adversaryName, userID, hostUser, 'write')) return 'adversary-notexists';
	let r;
	if (type == 'attacks') {
		adversaries.adversaries[adversaryName].attacks = [];
		r = 'adversary-cleared-attacks';
	}
	if (type == 'abilities') {
		adversaries.adversaries[adversaryName].abilities = [];
		r = 'adversary-cleared-abilities';
	}
	adversaries_save();
	return r;
}

// ADVERSARY LIST -------------------------------------------------------------

// /aedit list [mine] [tag=<tag>] [search-text] - lists all journeys that match (only shows mine or published, except that I can see them all)
function adversaries_list(userID, hostUser, locale, mine, published, searchtext, tags) {
	let matches = [];
	// go through the journeys checking both accessibility and the search terms
	for (let adversaryName in adversaries.adversaries) {
		let listIt = true;
		if (!adversaries_accessible(adversaryName, userID, hostUser, 'read')) listIt = false;
		if (mine === true && adversaries.adversaries[adversaryName].creator != userID) listIt = false;
		if (mine === false && adversaries.adversaries[adversaryName].creator == userID) listIt = false;
		if (published === true && !adversaries.adversaries[adversaryName].published) listIt = false;
		if (published === false && adversaries.adversaries[adversaryName].published) listIt = false;
		if (tags) tags.split(' ').forEach(tag => {
			if (!adversaries.adversaries[adversaryName].tags.includes(tag)) listIt = false;
		});
		if (searchtext) searchtext.split(' ').forEach(term => {
			if (!adversaries_search_term(adversaryName,term)) listIt = false;
		});
		if (listIt) matches.push(adversaryName);
	}
	return adversaries_display_list(matches, locale);
}

function adversaries_display_list(matches, locale) {
	if (matches.length == 0) return slashcommands.localizedString(locale, 'adversary-listnomatch',[]);
	let reportHeadings = [
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'adversary-adversaries',[])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'aedit', 'set', 'name')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'aedit', 'set', 'attribute')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'aedit', 'set', 'might')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'aedit', 'set', 'endurance')), emoji:false, minWidth:0, align:'left'},
		{title:'Pub', emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	matches.forEach(adversaryName => {
		reportData.push([
			adversaryName, 
			adversaries.adversaries[adversaryName].name,
			String(adversaries.adversaries[adversaryName].attribute),
			String(adversaries.adversaries[adversaryName].might),
			String(adversaries.adversaries[adversaryName].endurance),
			adversaries.adversaries[adversaryName].published ? ':ledger:' : ''
		]);
	});
	return table.buildTable(reportHeadings, reportData, 2) + '\n' + slashcommands.localizedString(locale, 'adversary-matched',[matches.length, Object.keys(adversaries.adversaries).length]);
}

// check all the relevant fields of an adversary for a search term
function adversaries_search_term(adversaryName, searchTerm) {
	searchTerm = stringutil.lowerCase(searchTerm);
	if (stringutil.lowerCase(adversaryName).includes(searchTerm)) return true;
	if (stringutil.lowerCase(adversaries.adversaries[adversaryName].name).includes(searchTerm)) return true;
	if (stringutil.lowerCase(adversaries.adversaries[adversaryName].description).includes(searchTerm)) return true;
	if (stringutil.lowerCase(adversaries.adversaries[adversaryName].features).includes(searchTerm)) return true;
	if (stringutil.lowerCase(adversaries.adversaries[adversaryName].banetype).includes(searchTerm)) return true;
	if (stringutil.lowerCase(adversaries.adversaries[adversaryName].size).includes(searchTerm)) return true;
	let result = false;
	adversaries.adversaries[adversaryName].attacks.forEach(attack => {
		if (stringutil.lowerCase(attack.name).includes(searchTerm)) result = true;
		if (stringutil.lowerCase(attack.comment).includes(searchTerm)) result = true;
	});
	adversaries.adversaries[adversaryName].abilities.forEach(ability => {
		if (stringutil.lowerCase(ability.name).includes(searchTerm)) result = true;
		if (stringutil.lowerCase(ability.comment).includes(searchTerm)) result = true;
	});
	return result;
}


// EXPORTS --------------------------------------------------------------------

module.exports = Object.assign({ adversaries_load, adversaries_tag_add, adversaries_tag_remove, adversaries_tag_verify, adversaries_tag_list, adversaries_claim, adversaries_accessible, adversaries_fetch, adversaries_create, adversaries_copy, adversaries_edit_validate, adversaries_edit, adversaries_delete, adversaries_show, adversaries_attack_validate, adversaries_attack, adversaries_ability_validate, adversaries_ability, adversaries_clear, adversaries_list });

