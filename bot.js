var Eris = require('eris');

// CONFIGURATION --------------------------------------------------------------
var config = require('./config.json');
const version = 'v17.7.2 2025-03-0`4';
const erisVersion = 'v0.18.0';
const newsString = '**What\'s New**: `/environment`; Realms of the Three Rings changes; ranged adversary attacks';

// INCLUDES -------------------------------------------------------------------
var stringutil = require('./stringutil.js');
var emoji = require('./emoji.js');
var filehandling = require('./filehandling.js');
var lookuptables = require('./lookuptables.js');
var journeys = require('./journeys.js');
var regdice = require('./regdice.js');
var coyoteandcrow = require('./coyoteandcrow.js');
var table = require('./table.js');
var embeds = require('./embeds.js');
var googlesheets = require('./googlesheets.js');
var hexflower = require('./hexflower.js');
var slashcommands = require('./slashcommands.js');
var reports = require('./reports.js');
var initiative = require('./initiative.js');
var adversaries = require('./adversaries.js');
var encounters = require('./encounters.js');

const versionString = featRollText(emoji.GANDALF, false) + ' **' + version + '** by **HunterGreen** :small_orange_diamond: a bot to help run games of *[The One Ring](<https://freeleaguepublishing.com/games/the-one-ring/>)* in Discord\n :small_blue_diamond:[instructions, privacy policy, and source code](<https://bitbucket.org/HawthornThistleberry/narvi/>)\n :small_blue_diamond:[get technical support](<https://discord.me/theonering>) (channel **#narvi**)\n :small_blue_diamond:[help pay for cloud-hosting](<https://ko-fi.com/huntergreen>)\n :small_blue_diamond:uses [Eris](<https://github.com/abalabahaha/eris>) version ' + erisVersion;

// what kind of external repository character sheets does Narvi know about?
const knownKeyTypes = ['googlesheets'];

// Unicode pairs generated with http://russellcottrell.com/greek/utilities/SurrogatePairCalculator.htm
const SPEND_HEAVY      = '\uD83E\uDE93';        // heavy:     axe            U+1FA93
const SPEND_FEND       = '\uD83E\uDD3A';        // fend:      fencer         U+1F93A
const SPEND_PIERCE     = '\uD83C\uDFAF';        // pierce:    bullseye       U+1F3AF
const SPEND_SHIELD     = '\uD83D\uDEE1';        // shield:    shield         U+1F6E1
const SPEND_ESCAPE     = '\uD83D\uDD13';        // escape:    unlock         U+1F513
const SPEND_SEIZE      = '\uD83E\uDEF3';        // seize:     palm hand down U+1FAF3
const SPEND_FIRE       = '\uD83D\uDD25';        // fiery:     fire           U+1F525
const SPEND_REDX       = '\u274C';              // n/a:       red X          U+274C
const BUTTON_ZERO      = '\u0030\uFE0F\u20E3';  // 0:         keycap zero    U+0030 U+FE0F U+20E3
const BUTTON_ONE       = '\u0031\uFE0F\u20E3';  // 1h/1d:     keycap one     U+0031 U+FE0F U+20E3
const BUTTON_TWO       = '\u0032\uFE0F\u20E3';  // 2h/2d:     keycap two     U+0032 U+FE0F U+20E3
const BUTTON_THREE     = '\u0033\uFE0F\u20E3';  // 3d:        keycap three   U+0033 U+FE0F U+20E3
const BUTTON_FOUR      = '\u0034\uFE0F\u20E3';  // 4d:        keycap four    U+0034 U+FE0F U+20E3
const BUTTON_FIVE      = '\u0035\uFE0F\u20E3';  // 5d:        keycap five    U+0035 U+FE0F U+20E3
const BUTTON_SIX       = '\u0036\uFE0F\u20E3';  // 6d:        keycap six     U+0036 U+FE0F U+20E3
const BUTTON_SEVEN     = '\u0037\uFE0F\u20E3';  // 7d:        keycap seven   U+0037 U+FE0F U+20E3
const BUTTON_EIGHT     = '\u0038\uFE0F\u20E3';  // 8d:        keycap eight   U+0038 U+FE0F U+20E3
const BUTTON_NINE      = '\u0039\uFE0F\u20E3';  // 9d:        keycap nine    U+0039 U+FE0F U+20E3
const BUTTON_TEN       = '\uD83D\uDD1F';        // 10:        keycap ten     U+1F51F
const BUTTON_NUMBER    = '\u0023\uFE0F\u20E3';  // #:         keycap #       U+0023 U+FE0F U+20E3
const BUTTON_CONFIRM   = '\u2705';              // confirm:   check-mark     U+2705
const GUI_HELP         = '\u2753';              // help:      question mark  U+2753
const GUI_VERSION      = '\u2139\uFE0F';        // version:   information    U+2139 U+FE0F
const GUI_DICE         = '\uD83C\uDFB2';        // dice:      die            U+1F3B2
const GUI_PLAIN        = '\uD83D\uDE42';        // plain:     slight smile   U+1F642
const GUI_HOPE         = '\uD83D\uDE03';        // hope:      grin           U+1F603
const GUI_INSPIRED     = '\uD83E\uDD29';        // inspired:  starstruck     U+1F929
const GUI_MAGICAL      = '\uD83D\uDE07';        // magical:   halo           U+1F607
const GUI_STATUS       = '\uD83D\uDCC3';        // status:    page with curl U+1F4C3
const GUI_SHEET        = '\uD83D\uDCDC';        // sheet:     scroll         U+1F4DC
const GUI_REFRESH      = '\uD83D\uDD04';        // refresh:   recycle        U+1F504
const GUI_EXPLAIN      = '\uD83D\uDE4B';        // explain:   raised hand    U+1F64B
const GUI_GAIN1        = '\u2B06\uFE0F';        // gain1:     up arrow       U+2B06 U+FE0F
const GUI_LOSE1        = '\u2B07\uFE0F';        // lose1:     down arrow     U+2B07 U+FE0F
const GUI_GAIN2        = '\uD83D\uDD3C';        // gain2:     up triangle    U+1F53C
const GUI_LOSE2        = '\uD83D\uDD3D';        // lose2:     down triangle  U+1F53D
const GUI_GAIN3        = '\u23EB';              // gain3:     up double      U+23EB
const GUI_LOSE3        = '\u23EC';              // lose3:     down double    U+23EC
const GUI_AWE          = '\uD83D\uDDE3\uFE0F';  // awe:       speaking head  U+1F5E3 U+FE0F
const GUI_ATHLETICS    = '\uD83D\uDC5F';        // athletics: sneaker        U+1F45F
const GUI_AWARENESS    = '\uD83D\uDC40';        // awareness: eyes           U+1F440
const GUI_HUNTING      = '\uD83C\uDFF9';        // hunting:   bow            U+1F3F9
const GUI_SONG         = '\uD83C\uDFBC';        // song:      musical score  U+1F3BC
const GUI_CRAFT        = '\u2692\uFE0F';        // craft:     hammer & pick  U+2692 U+FE0F
const GUI_ENHEARTEN    = '\uD83E\uDEC2';        // enhearten: hugging        U+1FAC2
const GUI_TRAVEL       = '\uD83D\uDDFA\uFE0F';  // travel:    map            U+1F5FA U+FE0F
const GUI_INSIGHT      = '\uD83E\uDD28';        // insight:   raised eyebrow U+1F928
const GUI_HEALING      = '\u2695\uFE0F';        // healing:   caduceus       U+2695 U+FE0F
const GUI_COURTESY     = '\uD83C\uDF39';        // courtesy:  rose           U+1F339
const GUI_BATTLE       = '\u2694\uFE0F';        // battle:    swords         U+2694 U+FE0F
const GUI_PERSUADE     = '\uD83D\uDE0E';        // persuade:  sunglasses     U+1F60E
const GUI_STEALTH      = '\uD83E\uDD2B';        // stealth:   shush          U+1F92B
const GUI_SCAN         = '\uD83D\uDD0D';        // scan:      magnifier      U+1F50D
const GUI_EXPLORE      = '\uD83E\uDDED';        // explore:   compass        U+1F9ED
const GUI_RIDDLE       = '\uD83E\uDDE9';        // riddle:    puzzle         U+1F9E9
const GUI_LORE         = '\uD83D\uDCDA';        // lore:      books          U+1F4DA
const GUI_FORWARD      = '\u25B6\uFE0F';        // forward:   play           U+25B6 U+FE0F
const GUI_OPEN         = '\u23FA\uFE0F';        // open:      record         U+23FA U+FE0F
const GUI_DEFENSIVE    = '\u25C0\uFE0F';        // defensive: reverse        U+23EA
const GUI_REARWARD     = '\u23EA';              // rearward:  fast reverse   U+1F4DA
const GUI_SKIRMISH     = '\u23CF\uFE0F';        // skirmish:  eject          U+23CF U+FE0F
const GUI_ABSENT       = '\u23F9\uFE0F';        // absent:    stop           U+23F9 U+FE0F
const GUI_UNCHANGED    = '\uD83D\uDD01';        // unchanged: repeat         U+1F501
const GUI_BRAWLING     = '\u270A';              // brawling:  raised fist    U+270A
const GUI_KNIFE        = '\uD83D\uDDE1\uFE0F';  // knife:     dagger         U+1F5E1 U+FE0F
const GUI_STONE        = '\uD83E\uDEA8';        // stone:     rock           U+1FAA8
const GUI_ADVERSARY    = '\uD83E\uDDCC';        // adversary: troll          U+1F9CC
const GUI_ADVWOUND     = '\uD83E\uDE78';        // adv wound: drop of blood  U+1FA78
const GUI_ADVLOW       = '\u2764\uFE0F';        // adv low:   red heart      U+2764 U+FE0F
const GUI_ADVMED       = '\uD83D\uDC9B';        // adv med:   yellow heart   U+1F49B
const GUI_ADVHIGH      = '\uD83D\uDC9A';        // adv high:  green heart    U+1F49A
const GUI_HURT         = '\uD83E\uDD15';        // hurt:      bandaged head  U+1F915
const GUI_KNOCKBACK    = '\uD83D\uDE43';        // knockback: upside down    U+1F643
const GUI_DAUNTED      = '\uD83D\uDE22';        // daunted:   crying face    U+1F622
const GUI_SEIZED       = '\u26D3\uFE0F';        // seized:    chains         U+26D3 U+FE0F
const GUI_BITTEN       = '\uD83E\uDD87';        // bitten:    bat            U+1F987
const GUI_MISFORTUNE   = '\uD83E\uDEE8';        // misfortune:shaking face   U+1FAE8
const GUI_DISMAYED     = '\uD83D\uDE2C';        // dismayed:  grimacing face U+1F62C
const GUI_DREAMING     = '\uD83D\uDE34';        // dreaming:  sleeping face  U+1F634
const GUI_HAUNTED      = '\uD83D\uDE31';        // haunted:   screaming fear U+1F631
const GUI_BLEEDING     = '\uD83E\uDE78';        // adv wound: drop of blood  U+1FA78
const GUI_WEARY        = '\uD83E\uDD71';        // weary:     yawning face   U+1F971
const GUI_MISERABLE    = '\uD83D\uDE2D';        // miserable: loudly crying  U+1F62D
const GUI_WOUND        = '\uD83E\uDE79';        // wound:     bandage        U+1FA79
const GUI_HATE         = '\uD83D\uDE21';        // hate:      enraged face   U+1F621
const GUI_TIME         = '\uD83D\uDD70\uFE0F';  // time:      mantle clock   U+1F570 U+FE0F
const GUI_TIMELIMIT    = '\u23F1\uFE0F';        // timelimit: stopwatch      U+23F1 U+FE0F
const GUI_SCORE        = '\uD83C\uDFB0';        // score:     slot machine   U+1F3B0
const GUI_RESISTANCE   = '\uD83E\uDD45';        // resistance:goal           U+1F945
const GUI_EYE          = '\uD83D\uDC41\uFE0F';  // eye:       eye            U+1F441 U+FE0F
const GUI_COMPANY      = '\uD83D\uDC65';        // company:   people         U+1F465
const GUI_SHADOW       = '\uD83D\uDFE4';        // shadow:    brown circle   U+1F7E4
const GUI_PERSON       = '\uD83E\uDDD1';        // person:    person         U+1F9D1
const GUI_EASY         = '\uD83D\uDEB6';        // easy:      walking        U+1F6B6
const GUI_MEDIUM       = '\uD83C\uDFCB\uFE0F';  // medium:    lifting weight U+1F3CB U+FE0F
const GUI_HARD         = '\uD83E\uDD39';        // hard:      juggling       U+1F939
const GUI_NEUTRAL      = '\uD83D\uDE10';        // neutral:   neutral face   U+1F610
const GUI_UNFRIENDLY   = '\u2639\uFE0F';        // unfriendly:frowning       U+2639 U+FE0F
const GUI_SUMMER       = '\u2600\uFE0F';        // summer:    sun            U+2600 U+FE0F
const GUI_WINTER       = '\u2744\uFE0F';        // winter:    snowflake      U+2744 U+FE0F
const GUI_DARKNESS     = '\uD83C\uDF11';        // darkness:  new moon       U+1F311
const GUI_NOLIGHT      = '\uD83C\uDF24\uFE0F';  // nolight:   sun/clouds     U+1F324 U+FE0F
const GUI_SURPRISE     = '\uD83E\uDEE2';        // surprise:  surprised face U+1FAE2
const GUI_REST         = '\uD83D\uDECC';        // rest:      in bed         U+1F6CC
const GUI_ENDURANCE    = '\uD83D\uDD35';        // endurance: blue circle    U+1F535
const GUI_HOPETOKEN    = '\u26AA';              // hopetoken: white circle   U+26AA
const GUI_POOL         = '\uD83D\uDFE3';        // pool:      purple circle  U+1F7E3
const GUI_YULE         = '\uD83C\uDF84';        // yule:      Yule tree      U+1F384
const GUI_TREASURE     = '\uD83E\uDE99';        // treasure:  coin           U+1FA99
const GUI_FALLING      = '\uD83E\uDD3E';        // falling:   handball       U+1F93E
const GUI_SUFFOCATION  = '\uD83D\uDE16';        // suffocate: confouned      U+1F616
const GUI_POISON       = '\uD83E\uDD22';        // poison:    nauseated      U+1F922
const GUI_TORNADO      = '\uD83C\uDF2A\uFE0F';  // tornado:   tornado        U+1F32A U+FE0F

const proficiencyList = ['Axes','Bows','Spears','Swords'];

// BOT CONNECTION -------------------------------------------------------------

console.log('-----------------------------STARTUP-------------------------------------');
var bot = new Eris('Bot '+ config.token, {intents: ['guilds', 'guildMembers', 'guildMessages', 'guildMessageReactions', 'directMessages'] });
bot.connect();
bot.on('ready', () => {
	readyEvent();
});

var braggedServers, braggedGames;

function readyEvent() {
	let now = new Date();
    console.log('Logged in as: ' + bot.user.username + ' ' + version + ' (' + bot.user.id + ') at ' + now.getFullYear()+'-'+stringutil.padLeft(now.getMonth()+1,2,'0')+'-'+stringutil.padLeft(now.getDate(),2,'0') + ' ' + stringutil.padLeft(now.getHours(),2,'0')+':'+stringutil.padLeft(now.getMinutes(),2,'0')+':'+stringutil.padLeft(now.getSeconds(),2,'0'));
    botPresence();
	slashcommands.slashInitialize(bot, notifyHostUser, config.commandsGuild, userConfig.lastLoadedSlashCommands, saveSlashLastLoadedTimestamp);
	setTimeout(function(){ loadAllUsers(); }, 10000);
	braggedGames = Object.keys(gameData).length;
	braggedServers = bot.guilds.size;
}

function saveSlashLastLoadedTimestamp(timestamp) {
	if (timestamp != null) {
		userConfig.lastLoadedSlashCommands = timestamp;
		filehandling.saveStateData(userConfig);
	}
}

// locales will be done before the ready event, so that things will display properly, even if slash command updates haven't been done yet
slashcommands.slashInitializeLocales();

bot.on('disconnect', () => {
	console.log('Attempting recovery from a disconnect in five seconds');
	setTimeout(function(){
		bot.connect();
	}, 5000);	
});
bot.on('error', (err) => {
    console.error('Server received an uncaught error: ', err);
});
bot.on('unknown', (err) => {
	if (['CHANNEL_TOPIC_UPDATE','VOICE_CHANNEL_STATUS_UPDATE','GUILD_JOIN_REQUEST_DELETE', 'APPLICATION_COMMAND_PERMISSIONS_UPDATE','GUILD_JOIN_REQUEST_UPDATE'].includes(err.t)) return;
    console.error('Server received an unknown event: ', JSON.stringify(err));
});

function botPresence() {
    bot.editStatus({name: Object.keys(gameData).length + ' games on ' + bot.guilds.size + ' servers', type: 0});
	if (Object.keys(gameData).length > braggedGames && Object.keys(gameData).length % 100 == 0) {
		braggedGames = Object.keys(gameData).length;
		botSendLargeMessage(config.bragTo,"I am now running " + braggedGames + " games!")
	}
	if (bot.guilds.size > braggedServers && bot.guilds.size % 100 == 0) {
		braggedServers = bot.guilds.size;
		botSendLargeMessage(config.bragTo,"I am now on " + braggedServers + " servers!")
	}
};

// call fetchAllMembers() on every guild the bot knows, one at a time in sequence
// this will ensure that we can get user names from IDs without the six minute startup delay
let guildsToLoad = [];
function loadAllUsers() {
	console.log('Attempting to load all users in all servers one server at a time');
	// save all the guilds in an array (bot.guilds does it as a collection instead so convert to an array)
	bot.guilds.forEach(g => guildsToLoad.push(g));
	// kick off the process
	loadUsersFromOneGuild();
}

// call fetchAllMembers() on the first guild on the list, then recurse
function loadUsersFromOneGuild() {
	// if we're done, say so and drop out of this loop
	if (guildsToLoad.length == 0) {
		console.log('Done loading all users in all servers one server at a time');
		return;
	}
	// pull off one guild
	let guildToLoad = guildsToLoad.shift();
	// attempt to load it
	//console.log(' loading members for ' + guildToLoad.name);
	guildToLoad.fetchAllMembers()
	.then(m => {
		// wait a half second and then start the next guild
		setTimeout(function(){ loadUsersFromOneGuild() ; }, 500);
		// doing this by setTimeout() not only gives Discord a moment to breathe,
		// it ensures this recursion isn't going to smash the stack since this function isn't really calling itself directly
	})
	.catch(err => {
		// if there's an error, for now we'll just let the process stop, to avoid runaway errors
		notifyHostUser('loadUsersFromOneGuild error ' + err + ' loading members for ' + guildToLoad.name, false);
		// if this happens, maybe I can figure out why and fix it, and if so, then I can still do the setTimeout here too so it tries the next
	});
}

const DISCORD_MAX_LENGTH = 2000;

let lastSuggestor = null;

// put people who cause a lot of bomb messages quickly onto a list of people to put on cooldown

const bombMessage = ':bomb: Something went wrong! If this keeps happening, please `/suggest` and let us know what you were trying to do when this happened (and **don\'t keep doing it** -- this can cause the bot to become unresponsive for everyone else!).';
const bombCooldown = ':bomb: You\'ve caused too many errors too quickly, so your commands are being ignored for 20 seconds to prevent repeated errors from causing Narvi to become non-responsive to other users.';

let evilMidnightBombersWhatBombAtMidnight = {};

// add a user to the bomber count
function recordEvilMidnightBombers(userID) {
	if (evilMidnightBombersWhatBombAtMidnight[userID] == undefined) {
		evilMidnightBombersWhatBombAtMidnight[userID] = {'count': 1, 'last': Date.now()};
	} else {
		evilMidnightBombersWhatBombAtMidnight[userID].count ++;
		evilMidnightBombersWhatBombAtMidnight[userID].last = Date.now();
	}
}

// 20 seconds after your last bomb, all is forgiven (or at least forgotten)
setInterval(function () {
	let purge = []
	for (let userID in evilMidnightBombersWhatBombAtMidnight) {
		if (Date.now() - evilMidnightBombersWhatBombAtMidnight[userID].last > 20000) purge.push(userID);
	}
	purge.forEach(userID => {
		delete evilMidnightBombersWhatBombAtMidnight[userID];
	});
}, 10000);

// check for them to be in cooldown
function isEvilMidnightBomber(userID) {
	if (evilMidnightBombersWhatBombAtMidnight[userID] != undefined && evilMidnightBombersWhatBombAtMidnight[userID].count > 4) {
		console.log('Blocking interaction from user ' + userID + ' due to excessive bombings.');
		return true;
	}
	return false;
}

// splits large outputs up
function botSendLargeMessage(channelID, message) {
	let breakpoint;
	if (message == undefined || message == null) return false;
	while (message && message.length > DISCORD_MAX_LENGTH) {
		breakpoint = DISCORD_MAX_LENGTH;
		// try to break by finding the previous \n
		while (breakpoint > 0 && message.charAt(breakpoint) != '\n') breakpoint--;
		// if we can't find one, try to find a space instead
		if (breakpoint == 0) {
			breakpoint = DISCORD_MAX_LENGTH;
			while (breakpoint > 0 && message.charAt(breakpoint) != ' ') breakpoint--;
		}
		// if still no joy, just do an arbitrary break
		if (breakpoint == 0) breakpoint = DISCORD_MAX_LENGTH - 1;
		botSendMessage(channelID, message.substring(0,breakpoint));
		message = message.substring(breakpoint);
	}
	botSendMessage(channelID, message);
	return true;
}

function botChannelExists(channelID) {
	try	{
		if (bot.getChannel(channelID) && bot.getChannel(channelID).name) return true;
		return false;
	} catch (err) {
		return false;
	}
}

function botChannelName(channelID) {
	let channelName = channelID;
	if (bot.getChannel(channelID) && bot.getChannel(channelID).name) channelName = bot.getChannel(channelID).name;
	return channelName;
}

function botChannelMention(channelID) {
	let channelName = channelID;
	if (bot.getChannel(channelID) && bot.getChannel(channelID).name) channelName = bot.getChannel(channelID).mention;
	return channelName;
}

// handles the promise when sending messages
function botSendMessage(channelID, message) {
	if (message == '') return true;
	bot.createMessage(channelID, message)
	.then(response => {
		return true;
	})
	.catch(err => {
		if (String(err).includes('401')) {
			// just fired too early, try again in a second
			setTimeout(function(){ botSendMessage(channelID, message); }, 1000);
			return true;
		} else if (String(err).includes('50013')) {
			// permissions error, try to notify the person
			botSendPrivateMessage(lastUser, 'I was unable to send a message to the channel ' + botChannelName(channelID) + ' due to lack of permissions. You might want to check your Discord\'s configuration so that I\'ll be able to answer your commands.\n\nThe last command I couldn\'t answer was: `' + lastCommand + '`\nThe message I couldn\'t send in response was:\n' + message);
			return true;
		} else {
			console.log('Error sending a message to channel ' + botChannelName(channelID) + ': ' + err + '\n(message: ' + message + ')');
			if (channelID != config.hostChannel) notifyHostUser('Error sending a message to channel ' + botChannelName(channelID) + ': ' + err + '\n(message: ' + message + ')' + err.stack,1,'');
			return false;
		}
	});
}

// always send to a user privately
function botSendPrivateMessage(userID, message) {
	//bot.users.get(userID).getDMChannel()
	bot.getDMChannel(String(userID))
	.then(response => {return botSendLargeMessage(response.id, message, [])})
	.catch(err => {
		console.log('Error finding a private channel for ' + userID + ': ' + err);
		return userLocalizedString(userID, 'error-failedprivate', []) + ' ||' + message + '||';
	});
}

// send a private message but go through the LMchannel if provided
function botSendLMPrivateMessage(g, userID, lmchannel, message) {
	if (!botChannelExists(lmchannel)) {
		g.lmchannel = '';
		lmchannel = '';
		botSendPrivateMessage(userID, ':bomb: LM channel no longer exists or is unreachable, so has been deactivated.');
	}
	if (lmchannel == '') return botSendPrivateMessage(userID, message);
	return botSendLargeMessage(lmchannel, message);
}

function isEmpty(obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// fetch information from a user via Discord client object
function userNameFromID(userID) {
	let user = bot.users.find( ({ id }) => id == userID );
	if (user == undefined) return 'Unknown User ' + userID;
	return user.username;
}

function userAvatarFromID(userID) {
	let user = bot.users.find( ({ id }) => id == userID );
	if (user == undefined) return '';
	return user.dynamicAvatarURL('png', 256);
}

// build a mention for a user
function userMention(userID) {
	if (userID == 0 || userID == null) return '';
	return '<@' + String(userID) + '>';
	/*
	let m;
	try {
		m = bot.users.get(userID);
	}
	catch (err) {
		return '';
	}
	if (m == undefined) return '';
	return m.mention;
	*/
}

// CHARACTER DATA -------------------------------------------------------------

const alwaysStoreLocally = ['stance', 'tempbonus', 'knockback', 'tempparry', 'journeyRole', 'charImage', 'repositoryKey','keyType','handedness','dmgReceivedThisRound','tengwar','protectionTN','fendParry','damageDone','numEngaged', 'scribbles', 'pendingfatigue', 'daunted', 'seized', 'bitten', 'drained', 'drainedTN', 'drainedAdv', 'shieldsmashed', 'misfortune', 'dismayed', 'dreaming', 'bleeding', 'haunted', 'weary', 'unweary', 'miserable', 'poisoned', 'poisontreated', 'shadowTests', 'protectionTests'];

// returns the character ID of the currently selected character of the currently selected game, or null if none
function currentCharacter(userID) {
	let g = mySelectedGame(userID);
	if (g == null) return null;
	if (g.loremaster == userID) return g.lmCurrCharacter;
	if (g.players[userID] == undefined) return null;
	if (g.players[userID].currCharacter == '') return null;
	return g.players[userID].currCharacter;
}

// front end to characterDataFetch that makes nulls into empty strings
function characterDataFetchHandleNulls(g, charID, field, subfield) {
	let r = characterDataFetch(g, charID, field, subfield);
	if ((r == null || r == undefined || r == '') && field == 'name') {
		if (g.characters[charID] && g.characters[charID].name != '') r = g.characters[charID].name;
		else r = 'Nameless Thing ' + charID;
	}
	if (r == null || r == undefined) return '';
	return r;
}

// front end to characterDataFetch that converts to numeric
function characterDataFetchHandleNumbers(g, charID, field, subfield) {
	let r = characterDataFetch(g, charID, field, subfield);
	if (r == null || r == undefined || isNaN(r)) return 0;
	return Number(r);
}

// find data from a character wherever it is stored
function characterDataFetch(g, charID, field, subfield) {

	if (charID == null || g == null || g.characters[charID] == undefined) return null;
	let p = g.characters[charID];
	if (alwaysStoreLocally.includes(field)) {
		// using the local storage within Narvi's JSON-saved structure
		let r = p[field];
		if (r == null || subfield == null) return r;
		return r[subfield];
	} else {
		// reading from the character repository cache
		return getDataFromExternalCache(p.keyType, p.repositoryKey, field, subfield);
	}
}

// update character data wherever it is stored
function characterDataUpdate(g, charID, field, subfield, operation, value, min, max, channelID) {
	if (!isTOR(g) && field != 'charImage') return false;
	if (charID == null || g == null || g.characters[charID] == undefined) return false;
	let p = g.characters[charID];

	// figure out value accounting for lookups, deltas, mins, and maxes
	let val = value;
	if (val && isNaN(val)) {
		let l = characterDataFetch(g, charID, val, null);
		if (l != null & l != '') val = Number(characterDataFetch(g, charID, val, null));
	}
	if (operation == '-') val = characterDataFetchHandleNumbers(g, charID, field, subfield) - val;
	if (operation == '+') val = characterDataFetchHandleNumbers(g, charID, field, subfield) + val;

	// if min or max are a field name, look that value up
	if (min && isNaN(min)) min = characterDataFetchHandleNumbers(g, charID, min, null);
	if (max && isNaN(max)) max = characterDataFetchHandleNumbers(g, charID, max, null);

	// cut off value at minimums and maximums if necessary
	if (min != null && !isNaN(min) && Number(val) < Number(min)) val = min;
	if (max != null && !isNaN(max) && Number(val) > Number(max)) val = max;

	if (alwaysStoreLocally.includes(field)) {
		try {		
			// find the right record
			if (subfield) {
				p[field][subfield] = val;
			} else {
				p[field] = val;
			}
		}
		catch (err) {
			return false;
		}
		return true;

	} else {
		let p = g.characters[charID];
		return updateDataThroughExternalCache(p.keyType, p.repositoryKey, field, subfield, val, channelID);
	}

}

// FRONT ENDS TO EXTERNAL CACHES -----------------------------------------------

// request the external cache to load a sheet
function loadExternalCacheSheet(keyType, repositoryKey, forceUpdate, channelID) {
	if (keyType == 'googlesheets')	{
		if (forceUpdate) return googlesheets.loadCharacter(repositoryKey, channelID, loadExternalCacheSheetErrorCallback);
		else return googlesheets.updateCharacter(repositoryKey, channelID, loadExternalCacheSheetErrorCallback);
	}
	return null;
}

// preload cache on startup
function preloadExternalCaches() {
	googlesheets.preloadCache(gameData, config.hostChannel, loadExternalCacheSheetErrorCallback);
}

// when the external functions for loadExternalCacheSheet find an error, they call this so it can do any cleanup
function loadExternalCacheSheetErrorCallback(keyType, repositoryKey, err) {
	if (keyType == 'googlesheets' && (String(err).includes('Requested entity was not found') || String(err).includes('Unable to parse range: Character'))) {
		console.log('Attempting to purge all references to the key ' + repositoryKey);
		// delete the key
		for (g in gameData) {
			for (p in gameData[g].characters) {
				if (gameData[g].characters[p].repositoryKey == repositoryKey && gameData[g].characters[p].keyType == 'googlesheets')  {
					console.log('  purging one from game ' + g);
					gameData[g].characters[p].repositoryKey = '';
					gameData[g].characters[p].keyType = '';
				}
			}
		}
		filehandling.saveGameData(gameData);
	}
}

// get data from the appropriate cache
function getDataFromExternalCache(keyType, repositoryKey, field, subfield) {
	let r = '';
	if (keyType == 'googlesheets') r = googlesheets.cacheDataFetch(repositoryKey, field, subfield);
	if (r == null || r == undefined) return null;
	return r;
}

// get useful item by skill from the appropriate cache
function getUsefulItemFromExternalCache(keyType, repositoryKey, skill) {
	let r = '';
	if (keyType == 'googlesheets') return googlesheets.characterHasUsefulItem(repositoryKey, skill);
	return r;
}

// get blessing item by skill from the appropriate cache
function getBlessingFromExternalCache(keyType, repositoryKey, skill) {
	let r = '';
	if (keyType == 'googlesheets') return googlesheets.characterHasBlessing(repositoryKey, skill);
	return r;
}

// get whether a virtue or reward is relevant from the appropriate cache
function hasTraitFromExternalCache(keyType, repositoryKey, traittype, value, substring) {
	let r = '';
	if (keyType == 'googlesheets') return googlesheets.characterHasTrait(repositoryKey, traittype, value, substring);
	return r;
}

// get whether a quality or curse is relevant from the appropriate cache
function hasQualityFromExternalCache(keyType, repositoryKey, type, value, substring, armorOnly) {
	let r = '';
	if (keyType == 'googlesheets') return googlesheets.characterHasQuality(repositoryKey, type, value, substring, armorOnly);
	return r;
}

// update data through the appropriate cache
function updateDataThroughExternalCache(keyType, repositoryKey, field, subfield, value, channelID) {
	if (keyType == 'googlesheets') return googlesheets.cacheDataUpdate(repositoryKey, field, subfield, value, channelID);
	return false;
}

// determine if something is updatable through the appropriate cache
function updatableDataThroughExternalCache(g, p, field, subfield) {
	if (g == null) return false;
	if (alwaysStoreLocally.includes(field)) return true;
	if (g.characters[p].keyType == 'googlesheets') return googlesheets.cacheDataUpdatable(g.characters[p].repositoryKey, field, subfield);
	return false;
}

// function to clean up improperly provided keys
function cleanRepositoryKey(type, s) {
	if (type == 'googlesheets' && s.startsWith('http')) {
		s = s.substring(s.indexOf('/d/') + 3).slice(0,44);
	}
	// remove everything but letters, numbers, dash, underscore
	r = '';
	for (i = 0; i < s.length; i++) {
		c = s.charAt(i);
		if (/^[A-Z0-9\-_]$/i.test(c)) r += c;
	}
	if (type == 'googlesheets' && s.length != 44) {
		return '';
	}
	return r;
}

// USER CONFIG ----------------------------------------------------------------

// safe fetch from userConfig
function getUserConfig(userID, field) {
	if (userConfig[userID] == null || userConfig[userID] == undefined) {
		if (field == 'saveLast') return true;
		if (field == 'jeditName') return '';
		if (field == 'aeditName') return '';
		if (field == 'autoSwitch') return true;
		if (field == 'locale') return 'base';
		return null;
	}
	if (userConfig[userID][field] == null || userConfig[userID][field] == undefined) return null;
	return userConfig[userID][field];
}

// safe store into userConfig
function setUserConfig(userID, field, value) {
	if (userID == null || value == null) return;
	if (userConfig[userID] == null || userConfig[userID] == undefined) userConfig[userID] = {};
	userConfig[userID][field] = value;
	filehandling.saveStateData(userConfig);
}

// a localized string based on the userConfig locale which is automatically set by interactions
function userLocalizedString(userID, id, params) {
	return slashcommands.localizedString(getUserConfig(userID, 'locale'), id, params);
}

// DICE ROLLING ---------------------------------------------------------------

const regularPolyhedraPattern = /^(\d*)[dD](\d+|%)([+\-]((\d*)[dD](\d+|%)|\d+))*/;

// basic polyhedral dice
function diceCommand(userID, userName, locale, data, gurps) {
	let g = mySelectedGame(userID);
	let charID = currentCharacter(userID);
	let args = '';
	if (gurps) args = '3d6 ';
	let comment = '';
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'roll') args += data.options[i].value;
		if (data.options[i].name == 'comment') comment = data.options[i].value;
	}
	// substitute scribbles and assemble args
	args = args.trim().split(' ');
	if (g != null && charID != null) {
		args = scribblesSubstitutions(g, charID, args, '{','}');
		args = scribblesSubstitutions(g, charID, args, '[',']');
	}

	let r = regdice.polyhedralDieRoll(args, comment, userName, locale);
	return [r[1],r[2]];
}

// Expanse dice
function expanseCommand(userID, userName, locale, data) {
	let charName = userName, charImage = '', target = null, comment = '', args = '';
	let g = mySelectedGame(userID);
	let charID = currentCharacter(userID);

	// get character name and image from the game if there is one
	if (g != null && charID != null) {
		charName = characterDataFetchHandleNulls(g, charID, 'name', null);
		charImage = characterDataFetchHandleNulls(g, charID, 'charImage', null);
	}
	if (charImage == '' || charImage == null || charImage == undefined) charImage = userAvatarFromID(userID);

	// build the arguments, target, and comment
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'modifiers') args += data.options[i].value;
		if (data.options[i].name == 'target') target = Number(data.options[i].value);
		if (data.options[i].name == 'comment') comment = data.options[i].value;
	}

	// branch to dice if that's what was specified
	if (args.match(regularPolyhedraPattern)) {
		let r = regdice.polyhedralDieRoll(args.trim().split(' '), comment, userName, locale);
		return [r[1],r[2]];
	}

	// substitute scribbles and assemble args
	args = args.trim().split(' ');
	if (g != null && charID != null) {
		args = scribblesSubstitutions(g, charID, args, '{','}');
		args = scribblesSubstitutions(g, charID, args, '[',']');
	}

	// roll!
	let r = regdice.expanseDieRoll(args, comment, charName, charImage, target, locale);
	return [r[1],r[2]];
}

// Coyote and Crow dice
function coyoteCommand(userID, userName, locale, data) {
	let charName = userName, charImage = '';
	let g = mySelectedGame(userID);
	let charID = currentCharacter(userID);

	// get character name and image from the game if there is one
	if (g != null && charID != null) {
		charName = characterDataFetchHandleNulls(g, charID, 'name', null);
		charImage = characterDataFetchHandleNulls(g, charID, 'charImage', null);
	}
	if (charImage == '' || charImage == null || charImage == undefined) charImage = userAvatarFromID(userID);

	// parse the arguments
	let cmd = data.options[0].name;
	let numdice, sn=8, criticals=0, sort=false, finish=false, comment='', roll, newvalue;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'numdice') numdice = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'sn') sn = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'criticals') criticals = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'sort') sort = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'finish') finish = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'roll') roll = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'newvalue') newvalue = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'comment') comment = data.options[0].options[i].value;
	}

	// roll!
	let r = coyoteandcrow.coyoteAndCrowCommand(locale, charName, charImage, cmd, numdice, sn, criticals, sort, finish, comment, roll, newvalue);
	return [r[1],r[2]];
}

// Cortex (Serenity) dice
function cortexCommand(userID, userName, locale, data) {
	let charName = userName, charImage = '';
	let g = mySelectedGame(userID);
	let charID = currentCharacter(userID);

	// get character name and image from the game if there is one
	if (g != null && charID != null) {
		charName = characterDataFetchHandleNulls(g, charID, 'name', null);
		charImage = characterDataFetchHandleNulls(g, charID, 'charImage', null);
	}
	if (charImage == '' || charImage == null || charImage == undefined) charImage = userAvatarFromID(userID);

	// parse the arguments
	let dice = [], target = 0, multiple = 0, comment = '', group = false;
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name.startsWith('die')) dice.push(data.options[i].value);
		if (data.options[i].name == 'target') target = Number(data.options[i].value);
		if (data.options[i].name == 'multiple') multiple = Number(data.options[i].value);
		if (data.options[i].name == 'group') group = data.options[i].value;
		if (data.options[i].name == 'comment') comment = data.options[i].value;
	}

	// to be valid there either have to be dice, or it has to be an LM with group and a target
	if (dice.length == 0 && (!isPlayerLoremasterOfGame(userID, g) || !group || target == 0)) {
		return slashcommands.localizedString(locale, 'roll-nodice', []);
	}
	// substitute scribbles and assemble args
	if (g != null && charID != null) {
		dice = scribblesSubstitutions(g, charID, dice, '{','}');
		dice = scribblesSubstitutions(g, charID, dice, '[',']');
	}

	// roll!
	let r = regdice.cortexDieRoll(charName, charImage, locale, stringutil.titleCase(getUserConfig(userID,'selectedGame')), isPlayerLoremasterOfGame(userID, g), dice, target, multiple, group, comment);
	return [r[1],r[2]];
}

// Raven dice
function ravenCommand(userID, userName, locale, data) {
	let charName = userName, charImage = '';
	let g = mySelectedGame(userID);
	let charID = currentCharacter(userID);

	// get character name and image from the game if there is one
	if (g != null && charID != null) {
		charName = characterDataFetchHandleNulls(g, charID, 'name', null);
		charImage = characterDataFetchHandleNulls(g, charID, 'charImage', null);
	}
	if (charImage == '' || charImage == null || charImage == undefined) charImage = userAvatarFromID(userID);

	// parse the arguments
	let mist = 2, corvus = 2, comment = '', group = false;
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'mist') mist = Number(data.options[i].value);
		if (data.options[i].name == 'corvus') corvus = Number(data.options[i].value);
		if (data.options[i].name == 'comment') comment = data.options[i].value;
	}

	// roll!
	let r = regdice.ravenDieRoll(mist, corvus, comment, charName, locale);
	return [r[1],r[2]];
}

function getCharLink(g, charID) {
	if (!charID || !g.characters[charID]) return 'https://bitbucket.org/HawthornThistleberry/narvi/';
	return embeds.buildEmbedLink(g.characters[charID].keyType, g.characters[charID].repositoryKey);
}

// get information for a character for the embed -- only call if a valid character!
function getRollCharacterInfo(g, charID, userID) {
	let charName = characterDataFetchHandleNulls(g, charID, 'name', null);
	let charImage = characterDataFetchHandleNulls(g, charID, 'charImage', null);
	let charLink = getCharLink(g, charID);
	if (charImage == '' || charImage == null || charImage == undefined) charImage = userAvatarFromID(userID);
	return [charName, charImage, charLink];
}

// TOR dice by number, not skill/test
function torCommand(cmd, userID, userName, locale, data) {
	let charName = userName, charImage = '', charLink = '';
	let g = mySelectedGame(userID);
	let charID = currentCharacter(userID);
	if (g != null && charID != null) {
		[charName, charImage, charLink] = getRollCharacterInfo(g, charID);
	}
	if (charImage == '' || charImage == null || charImage == undefined) charImage = userAvatarFromID(userID);

	let dice = null, feat = null, magical = null, modifier = 0, weary = null, miserable = null, lmdice = null, tn = null, comment = '';

	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'dice') dice = Number(data.options[i].value);
		if (data.options[i].name == 'feat') feat = data.options[i].value;
		if (data.options[i].name == 'hope') magical = data.options[i].value;
		if (data.options[i].name == 'modifier') modifier = Number(data.options[i].value);
		if (data.options[i].name == 'weary') weary = data.options[i].value;
		if (data.options[i].name == 'miserable') miserable = data.options[i].value;
		if (data.options[i].name == 'lmdice') lmdice = data.options[i].value;
		if (data.options[i].name == 'tn') tn = Number(data.options[i].value);
		if (data.options[i].name == 'comment') comment = data.options[i].value;
	}

	// determine roll parameters
	if (dice == null) dice = 0;

	if (feat == 'favoured') feat = 2;
	else if (feat == 'illfavoured') feat = -2;
	else feat = 1;
	if (cmd == 'feat') {
		if (feat < 0 && dice != 0) feat = -1 * dice;
		else if (dice != 0) feat = dice;
		if (!feat) feat = 1;
		dice = 0;
	}
	if (cmd == 'success') {
		feat = 0;
		if (dice == 0) dice = 1;
	}

	// in /roll we will also have to handle unfavoured and default to whatever the skill/test says

	if (weary == 'weary') weary = true;
	else if (weary == 'notweary') weary = false;
	else if (g != null && charID != null) weary = characterWeary(g, charID, null);
	else weary = false;

	if (miserable == 'miserable') miserable = true;
	else if (miserable == 'notmiserable') miserable = false;
	else if (g != null && charID != null) miserable = characterMiserable(g, charID);
	else miserable = false;

	if (magical == 'magical') magical = true;
	else magical = false;
	// for this command we only care about magical results

	if (lmdice == null) {
		if (g && isPlayerLoremasterOfGame(userID, g)) lmdice = true;
		else lmdice = false;
	}

	// do the roll
	let rollResults = torRollDice(feat, dice, 0, modifier, weary, miserable, magical, lmdice, false, locale, diceStyle(mySelectedGame(userID)),false);

	// build the result
	let rollLine = '**'+ charName + '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' '+ rollResults[3];

	let r = '';
	if (cmd == 'tor' && !lmdice && g && !g.combat.active && rollResults[4] == 0 && gameHasConfigFlag(g, 'roll-eye')) {
		r += increaseEyeRating(g, 1);
	}

	if (cmd == 'tor' && !lmdice && magical) {
		comment += ' (' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'roll-magical', [])) + ')';
		if (g && gameHasConfigFlag(g, 'magical-eye')) {
			r += increaseEyeRating(g, 1);
		}
	}

	if (tn && tn != 0) r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', [])) + '__**: ' + compareToTarget(rollResults, tn, locale);

	let followup = [];
	if (g && isStriderMode(g) && cmd == 'tor' && (rollResults[4] == 0 || rollResults[4] == 12)) {
		let [error, range, code, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll((rollResults[4] == 12 ? 'fortune' : 'illfortune'), locale, '', null, false, torRollDice);
		if (error == null) {
			followup = followup.concat(rollstrings);
			followup.push(tables_formatted_result(newTableName, finalroll, range, code, text));
			if (range == '0') r += increaseEyeRating(g, 2);
		}
	}
	if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []))  + '__**: ' + comment;

	let theEmbed = embeds.buildEmbed(
		charName,
		charImage,
		charLink,
		rollLine,
		r + '\n',
		'',
		bot, null, 
		getUserConfig(userID,'selectedGame'),
		embeds.BUTTON_GREYPLE,
		userID
		);
	return [rollResults[5], {embed: theEmbed}, ...followup];
}

// implement the feat option -- you should already have a default featToRoll based on skill lookup by now
function torRollHandleFeat(feat, featToRoll, debugString, dismayed) {
	if (feat == 'favoured') {
		featToRoll = 2;
		debugString += ':anger: Command options explicitly set feat to best of two\n';
	} else if (feat == 'illfavoured') {
		featToRoll = -2;
		debugString += ':anger: Command options explicitly set feat to worst of two\n';
	} else if (feat == 'failing') {
		featToRoll = -99;
		debugString += ':anger: Command options explicitly set feat to failing\n';
	} else if (feat == 'unfavoured') {
		featToRoll = 1;
		debugString += ':anger: Command options explicitly set feat to one roll\n';
	} else if (dismayed) {
		featToRoll = -2;
		debugString += ':anger: Dismayed set roll Ill-favoured\n';
	}
	return [featToRoll, debugString];
}

// handle a bunch of other common things in /roll and /attack and others
function torRollHandleBonuses(bonus, noitem, modifier, tn, targetTN, g, charID, successToRoll, mods, debugString) {
	successToRoll = Number(successToRoll);
	if (bonus && bonus != 0) {
		successToRoll += Number(bonus);
		debugString += ':anger: Success dice changed to ' + successToRoll + ' by `bonus=' + bonus + '`\n';
	}
	if (charID && g.characters[charID].haunted) {
		successToRoll--;
		debugString += ':anger: Haunted lowered success dice to ' + successToRoll + '\n';
	}
	if (!noitem && charID && hasCurse(g, charID, 'darken')) {
		successToRoll--;
		debugString += ':anger: Darken lowered success dice to ' + successToRoll + '\n';
	}
	if (modifier != 0) {
		mods += Number(modifier);
		debugString += ':anger: Modifier changed to ' + mods + ' by `' + (modifier > 0 ? '+' : '') + modifier + '`\n';
	}
	if (tn) {
		targetTN = tn;
		debugString += ':anger: TN changed to ' + targetTN + ' by `tn=' + tn + '`\n';
	}
	return [successToRoll, mods, targetTN, debugString];
}

// handle determining weariness and miserableness
function torRollHandleWearyMiserable(g, charID, weary, miserable, debugString, test) {
	if (miserable == null && charID && hasCurse(g, charID, 'ill-luck')) { // Ill-luck: Eye rolls cause automatic failure (as if miserable)
		miserable = 'miserable';
		debugString += ':anger: Ill-Luck causes auto-failure as if miserable\n';
	}
	// let explicit determinations override the above by doing them after
	if (weary == 'weary') weary = true;
	else if (weary == 'notweary') weary = false;
	else if (g != null && charID != null) {
		weary = characterWeary(g, charID, test);
		if (weary) {
			let load = Number(tokenValue(g, charID, 'encumbrance')) + Number(tokenValue(g, charID, 'fatigue'));
			if (test == 'arrival') {
				load -= g.characters[charID].pendingFatigue;
				debugString += ':anger: For Arrival roll, ' + String(g.characters[charID].pendingFatigue) + ' fatigue is ignored for purposes of calculating Weariness (since it kicks in after the roll)\n';
			}
			if (g.characters[charID].weary) debugString += ':anger: Weary status makes this roll Weary\n';
			else debugString += ':anger: Endurance ' + tokenValue(g, charID, 'endurance') + ' versus load ' + String(load) + ' makes this roll Weary\n';
		}
	} else weary = false;

	if (miserable == 'miserable') miserable = true;
	else if (miserable == 'notmiserable') miserable = false;
	else if (g != null && charID != null) {
		miserable = characterMiserable(g, charID);
		if (miserable) debugString += ':anger: Hope versus shadow makes this roll Miserable\n';
	} else miserable = false;
	return [weary, miserable, debugString];
}

// handle various options for spending hope
function torRollHandleHope(g, charID, hope, magical, successToRoll, warnings, debugString, channelID, test, locale) {
	if (hope == 'plain') hope = null;
	successToRoll = Number(successToRoll);
	if (hope == 'courage' && charID && !hasTrait(g, charID, 'virtue', 'desperate courage')) {
		warnings += slashcommands.localizedString(locale, 'roll-nocourage', []) + '\n';
		hope = null;
	}
	if (charID && g.characters[charID].daunted && hope != null) {
		warnings += slashcommands.localizedString(locale, 'condition-warning-daunted', []) + '\n';
		hope = null;
	}
	if (hope != null && hope != 'plain' && charID && characterDataFetchHandleNumbers(g, charID, 'hope', null) < 1) {
		debugString += ':anger: Hope option ignored due to not having hope to spend\n';
		hope = null;
	}
	if (hope == 'hope' && charID && hasTrait(g, charID, 'virtue', 'brave at a pinch') && (characterMiserable(g, charID) || characterWeary(g, charID, null) || characterDataFetchHandleNulls(g, charID, 'wounded', null) == 'TRUE')) {
		debugString += ':anger: Upgraded to `inspired` due to Brave At A Pinch virtue\n';
		hope = 'inspired';
	}
	if (hope == 'hope' && charID && (hasTrait(g, charID, 'virtue', 'dark for dark business') || hasTrait(g, charID, 'virtue', 'shots in the dark')) && g.combat.active && g.combat.light == 'darkness') {
		debugString += ':anger: Upgraded to `inspired` due to Dark for Dark Business or Shots In The Dark virtue\n';
		hope = 'inspired';
	}
	if (hope == 'hope' && hasCulturalBlessing(g, charID, 'virtue', 'virtue of kings') && (test == 'valour' || test == 'wisdom' || test == 'dread' || test == 'greed' || test == 'sorcery')) {
		debugString += ':anger: Upgraded to `inspired` due to Virtue of Kings cultural blessing\n';
		hope = 'inspired';
	}
	if (charID && hope == 'courage') {
		debugString += ':anger: Upgraded to inspired due to Desperate Courage\n';
		hope = 'inspired';
		warnings += characterGainShadow(g, charID, 1, channelID);
		filehandling.saveGameData(gameData);
		warnings += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-shadow', [])) + ' ' + slashcommands.localizedString(locale, 'setto', []) + '\n' + emoji.valueAndTokens(characterDataFetchHandleNumbers(g, charID, 'shadow', null), 'shadow', 0, false) + '\n';
	}
	if (hope == 'hope') {
		successToRoll++;
		debugString += ':anger: Success dice changed to ' + successToRoll + ' by spending hope\n';
	}
	if (hope == 'inspired') {
		successToRoll += 2;
		debugString += ':anger: Success dice changed to ' + successToRoll + ' by inspired hope\n';
	}
	if (hope == 'magical') {
		magical = true;
		debugString += ':anger: Feat die set to ' + featRollText(emoji.GANDALF, false) + ' by spending hope on a Magical Success\n';
	}
	if (charID && hope != null) {
		characterDataUpdate(g, charID, 'hope', null, '-', 1, 0, 'maxhope', channelID);
		filehandling.saveGameData(gameData);
		warnings += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-hope', [])) + ' ' + slashcommands.localizedString(locale, 'setto', []) + '\n' + emoji.valueAndTokens(characterDataFetchHandleNumbers(g, charID, 'hope', null), 'hope', characterDataFetchHandleNumbers(g, charID, 'maxhope', null), false) + '\n';
	}
	return [hope, magical, successToRoll, warnings, debugString];
}

// handle rolls that can cause eye to accrue
function torRollHandleEyeAccrual(g, lmdice, magical, featRoll, locale) {
	let r = '', comment = '';
	if (!lmdice && g && !g.combat.active && featRoll == 0 && gameHasConfigFlag(g, 'roll-eye')) {
		r += increaseEyeRating(g, 1);
	}
	// eye accrual due to magical results
	if (!lmdice && magical) {
		comment += ' (' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'roll-magical', [])) + ')';
		if (g && gameHasConfigFlag(g, 'magical-eye')) {
			r += increaseEyeRating(g, 1);
		}
	}
	return [comment, r];
}

// TOR dice by skill/test
function rollCommand(cmd, userID, channelID, userName, locale, data, allowhints) {
	let charName = userName, charImage = '', charLink = '';
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID);
	g.lastUsed = Date.now();

	// parse the options
	let test = null, skill = null, affects = '', bonus = 0, noitem = gameHasConfigFlag(g, 'default-noitem'), magical = false;
	let dice = null, feat = null, hope = null, modifier = 0, weary = null, miserable = null, lmdice = null, tn = null, comment = '';
	let warnings = '', marching = false;
	if (cmd == 'skill') test = 'skill';

	// parse options
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'test') test = data.options[i].value;
		if (data.options[i].name == 'skill') skill = data.options[i].value;
		if (data.options[i].name == 'affects') affects = data.options[i].value;
		if (data.options[i].name == 'bonus') bonus = Number(data.options[i].value);
		if (data.options[i].name == 'noitem') noitem = (data.options[i].value == 'no');
		if (data.options[i].name == 'noitem') console.log('noitem flag: ' + data.options[i].value + ' and noitem=' + String(noitem));
		if (data.options[i].name == 'feat') feat = data.options[i].value;
		if (data.options[i].name == 'hope') hope = data.options[i].value;
		if (data.options[i].name == 'modifier') modifier = Number(data.options[i].value);
		if (data.options[i].name == 'weary') weary = data.options[i].value;
		if (data.options[i].name == 'miserable') miserable = data.options[i].value;
		if (data.options[i].name == 'lmdice') lmdice = data.options[i].value;
		if (data.options[i].name == 'tn') tn = Number(data.options[i].value);
		if (data.options[i].name == 'comment') comment = data.options[i].value;
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[i].value);
		}
	}
	if (test == 'marching') {
		marching = true;
		test = 'skill';
		skill = 'travel';
		if (!affects.startsWith('journey')) affects = 'journey';
	}
	if (test == 'skill' && skill == null) skill = 'travel';
	if (affects.startsWith('journey') && !g.journey.active) {
		return slashcommands.localizedString(locale, 'roll-nojourney', []);
	}
	if (affects.startsWith('council') && !councilActive(g)) {
		return slashcommands.localizedString(locale, 'roll-nocouncil', []);
	}
	if (affects.startsWith('endeavour') && !endeavourActive(g)) {
		return slashcommands.localizedString(locale, 'roll-noendeavour', []);
	}

	// now look up the character info, after we have determined who it is
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	[charName, charImage, charLink] = getRollCharacterInfo(g, charID, userID);

	// determine the favored, bonus, tn based on the kind of test
	let debugString = '';
	let featToRoll = 1, successToRoll = 0, mods = 0, targetTN = 0, debugStringAddition = '', outputAddition = '';

	// if protection, dread, greed, or sorcery, and a relevant test is pending, and affects is not notqueue, apply its values and later its consequences
	let resultingPoints = null, resultingCondition = null, resultingDeadly = false, rollAdditional = '', doWound = false, doShadow = false;
	if (affects != 'notqueue' && test == 'protection' && g.characters[charID].protectionTests.length != 0) {
		let thisTest = g.characters[charID].protectionTests.shift();
		doWound = true;
		targetTN = Number(thisTest.injury);
		if (!targetTN) targetTN = 16; // this should never happen
		debugString += ':anger: Fulfilling pending protection test from ' + (thisTest.responsible || 'unknown') + ' (TN' + thisTest.injury + ')';
		rollAdditional = ' [' + thisTest.responsible + ']';
		if (feat == null && thisTest.feat != null) {
			feat = thisTest.feat;
			debugString += ' (feat ' + thisTest.feat + ')';
		}
		if (thisTest.bonus) {
			bonus += thisTest.bonus;
			debugString += ' (bonus increased by ' + thisTest.bonus + ')';
		}
		if (thisTest.modifier) {
			modifier += thisTest.modifier;
			debugString += ' (modifier increased by ' + thisTest.modifier + ')';
		}
		debugString += '\n';
		resultingCondition = thisTest.condition;
		resultingPoints = thisTest.poison;
		resultingDeadly = thisTest.deadly;
	}
	if (affects != 'notqueue' && ['dread','greed','sorcery'].includes(test) && g.characters[charID].shadowTests.length != 0) {
		let thisTest = null, newShadow = [];
		// only match the corresponding kind of test!
		g.characters[charID].shadowTests.forEach(aTest => {
			if (thisTest == null && aTest.type == test) {
				thisTest = aTest;
			} else {
				newShadow.push(aTest);
			}
		});
		if (thisTest != null) {
			// we found a match
			// remove it from the list
			g.characters[charID].shadowTests = [... newShadow];
			filehandling.saveGameData(gameData);
			doShadow = true;

			debugString += ':anger: Fulfilling pending ' + test + ' test from ' + thisTest.responsible + ' (' + thisTest.points;
			rollAdditional = ' [' + thisTest.responsible + ']';
			if (thisTest.condition) debugString += ', ' + thisTest.condition;
			debugString += ')';
			if (targetTN == null && thisTest.tn != null) {
				targetTN = thisTest.tn;
				debugString += ' (TN' + thisTest.feat + ')';
			}
			if (feat == null && thisTest.feat != null) {
				feat = thisTest.feat;
				debugString += ' (feat ' + thisTest.feat + ')';
			}
			if (thisTest.bonus) {
				bonus += thisTest.bonus;
				debugString += ' (bonus increased by ' + thisTest.bonus + ')';
			}
			if (thisTest.modifier) {
				modifier += thisTest.modifier;
				debugString += ' (modifier increased by ' + thisTest.modifier + ')';
			}
			debugString += '\n';
			resultingCondition = thisTest.condition;
			resultingPoints = thisTest.points;
		}
	}	
	
	switch (test) {
		case 'skill':
			[featToRoll, successToRoll, mods, targetTN, debugStringAddition, outputAddition] = torRollLookupSkill(g, charID, skill, noitem, locale);
			if (allowhints && regdice.simpleRoll(3) == 1 && cmd != 'skill' && !marching) warnings += ':bulb: Next time try `/skill` to save a step.\n';
			else if (allowhints && regdice.simpleRoll(3) == 1 && skill == 'healing') warnings += ':bulb: Next time try `/treat` if you\'re treating a wound.\n';
			break;
		case 'protection':
			[featToRoll, successToRoll, mods, targetTN, debugStringAddition, outputAddition] = torRollLookupProtection(g, charID, locale, targetTN);
			break;
		case 'dread': case 'greed': case 'sorcery':
			[featToRoll, successToRoll, mods, targetTN, debugStringAddition, outputAddition] = torRollLookupShadowTest(g, charID, test, locale);
			break;
		case 'wisdom': case 'valour':
			[featToRoll, successToRoll, mods, targetTN, debugStringAddition, outputAddition] = torRollLookupAttribute(g, charID, test, locale);
			break;
		case 'arrival':
			[featToRoll, successToRoll, mods, targetTN, debugStringAddition, outputAddition] = torRollLookupSkill(g, charID, 'travel', noitem, locale);
			break;
		case 'axes': case 'bows': case 'spears': case 'swords': case 'brawling':
			[featToRoll, successToRoll, mods, targetTN, debugStringAddition, outputAddition] = torRollLookupProficiency(g, charID, test, locale);
			break;
		default:
			return slashcommands.localizedString(locale, 'error-internal', ['No test found in `/roll`']);
	}
	debugString += debugStringAddition;

	if (lmdice == null) lmdice = false; // always default to false since even if the LM is rolling, they're rolling for a character

	// handle options common to different kinds of rolls
	[featToRoll, debugString] = torRollHandleFeat(feat, featToRoll, debugString, g.characters[charID].dismayed);
	[successToRoll, mods, targetTN, debugString] = torRollHandleBonuses(bonus, noitem, modifier, tn, targetTN, g, charID, successToRoll, mods, debugString);

	// handle weary and miserable
	if (!noitem && test == 'protection' && hasQuality(g, charID, 'rune-scored armour')) {
		weary = 'notweary';
		miserable = 'notmiserable';
		debugString += ':anger: Rune-Scored Armour prevents weary and miserable from applying\n';
	}
	[weary, miserable, debugString] = torRollHandleWearyMiserable(g, charID, weary, miserable, debugString, channelID, test);

	// handle hope spends
	if (hope == 'hope' && isStriderMode(g) && affects.startsWith('journey')) {
		hope = 'inspired';
		debugString += ':anger: Upgraded to inspired due to Strider distinctive feature\n';
	}
	if (hope != null && test == 'protection' && armorHasCurse(g, charID, 'malice')) {
		debugString += ':anger: Malice prevented the spending of hope\n';
		hope = null;
	}
	[hope, magical, successToRoll, warnings, debugString] = torRollHandleHope(g, charID, hope, magical, successToRoll, warnings, debugString, test, locale);

	// journey modifiers
	if (affects == 'journey') {
		if (test != 'skill' || skill != g.journey.skill) {
			warnings += slashcommands.localizedString(locale, 'journey-wrongskill', [stringutil.makeSmallCaps(stringutil.titleCaseWords(skill)),stringutil.makeSmallCaps(stringutil.titleCaseWords(g.journey.skill))]) + '\n';
			affects = '';
		} else {
			let diff = journeys.journeys_modifier_dice(g, userID);
			if (diff) {
				successToRoll += journeys.journeys_modifier_dice(g, userID);
				debugString += ':anger: Success dice changed by ' + diff + ' to ' + successToRoll + ' due to journey difficulty\n';
			}
		}
	}

	// council modifiers
	if (affects.startsWith('council')) {
		if (g.council.introduction != 0) {
			let attitude = g.council.attitudemod;
			if (affects == 'councilfriendly') attitude = 1;
			if (affects == 'councilopen') attitude = 0;
			if (affects == 'councilreluctant') attitude = -1;
			if (hasTrait(g, charID, 'virtue', 'friendly and familiar')) {
				debugString += ':anger: Council attitude changed to friendly due to Friendly and Familiar virtue\n';
				attitude = 1;
			}
			successToRoll += attitude;
			debugString += ':anger: Council ' + councilAttitudeDesc(attitude, locale) + ' attitude changed success bonus dice to ' + successToRoll + '\n';
			warnings += slashcommands.localizedString(locale, 'council-attitude', [councilAttitudeDesc(attitude, locale)]) + '\n';
		}
		// 	Ill-omen: -1d to all council rolls
		if (hasCurse(g, charID, 'ill-omen')) {
			successToRoll--;
			debugString += ':anger: Ill-omen changed success bonus dice to ' + successToRoll + '\n';
		}
	}

	// song modifiers
	if (test == 'skill' && skill == 'song' && g.songBonus && g.songCharacters.includes(String(charID))) {
		successToRoll++;
		debugString += ':anger: Songbook song bonus changed success bonus dice to ' + successToRoll + '\n';
	}

	let followup = [];
	if (test == 'skill' && skill == 'battle') {
		let s = initiative.takeInitiativeAction(locale, g, charID, skill);
		if (s != '') followup.push(s);
	}

	// do the roll
	let r = '', rollResults, rollLine = '**'+ charName + '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' ';
	if (test == 'skill') rollLine += stringutil.makeSmallCaps(stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'roll', 'skill', skill)));
	else rollLine += stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'roll', 'test', test));
	rollLine += rollAdditional;
	rollLine += ': ';
	if (affects == 'journeypass') {
		rollResults = [99, 1, 0, '', emoji.GANDALF, ''];
		rollLine += slashcommands.localizedString(locale, 'roll-pass', []);
	} else if (affects == 'journeyfail') {
		rollResults = [0, -1, 0, '', emoji.EYE, ''];
		rollLine += slashcommands.localizedString(locale, 'roll-fail', []);
	} else {
		rollResults = torRollDice(featToRoll, successToRoll, 0, mods, weary, miserable, magical, lmdice, hasLifepath(g, charID, 'favoured'), locale, g.dicestyle, gameHasConfigFlag(g, 'magical-tengwar'));
		rollLine += rollResults[3];
	}

	// apply various consequences and effects
	// eye accrual due to rolling Eye
	let [commentadd, radd] = torRollHandleEyeAccrual(g, lmdice, magical, rollResults[4], locale);
	comment += commentadd;
	r += radd;
	let successes = 0;
	if (rollResults[1] >= 0 && targetTN && (rollResults[0] >= targetTN || rollResults[1] == 1)) successes = rollResults[2] + 1;
	// if this is a council roll, apply it and add a pose that shows the outcome
	if (affects.startsWith('council')) followup.push(councilApplyRoll(g, locale, successes));
	// if this is a endeavour roll, apply it and add a pose that shows the outcome
	if (affects.startsWith('endeavour')) followup.push(endeavourApplyRoll(g, locale, successes));
	// if this is a journey roll, apply it and add a pose that shows the outcome
	if (affects.startsWith('journey')) {
		let jN = journeys.journeys_apply_roll(g, charID, charName, successes, locale, channelID);
		if (Array.isArray(jN)) {
			followup = followup.concat(jN);
		} else {
			followup.push(jN);
		}
		filehandling.saveGameData(gameData);
	}
	// if arrival, determine fatigue loss and apply it
	if (test == 'arrival') {
		let fatigueLoss = Number(successes);
		if (hasTrait(g, charID, 'virtue', 'over dangerous leagues')) fatigueLoss++;
		let vigour = Number(characterDataFetchHandleNumbers(g, charID, 'steedvigour', null));
		if (g.journey.underground && !hasTrait(g, charID, 'virtue', 'bree-pony')) vigour = 0;
		if (g.journey.unmounted) vigour = 0;
		if (fatigueLoss + vigour > 0) {
			let delta = fatigueLoss + vigour;
			characterDataUpdate(g, charID, 'fatigue', null, '-', delta, 0, null, channelID);
			filehandling.saveGameData(gameData);
			warnings += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-fatigue', [])) + ' ' + slashcommands.localizedString(locale, 'reduced', []) + ' ' + fatigueLoss + ((vigour > 0) ? (' ' + slashcommands.localizedString(locale, 'plus', []) + ' ' + vigour + ' ' + slashcommands.localizedString(locale, 'stat-vigour', [])) : '') + '\n' + emoji.valueAndTokens(characterDataFetchHandleNumbers(g, charID, 'fatigue', null), 'fatigue', 0, false) + '\n';
		}
	}
	// if there's a target, put in a success or failure
	if (targetTN && targetTN != 0) {
		r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', [])) + '__**: ' + compareToTarget(rollResults, targetTN, locale);
		// if brawling and the hero is seized and they got at least one Tengwar, spend it on freeing
		if (test == 'brawling' && (g.characters[charID].seized || g.characters[charID].bitten || g.characters[charID].drained) && successes >= 1) {
			g.characters[charID].seized = false;
			g.characters[charID].bitten = false;
			g.characters[charID].drained = false;
			g.characters[charID].bleeding = 0;
			followup.push(slashcommands.localizedString(locale, 'condition-freed', [charName]));
		}
	}
	// if virtue of kings and an eye roll, suggest a reroll
	if (hasCulturalBlessing(g, charID, 'virtue', 'virtue of kings') && rollResults[4] == 0) {
		warnings += slashcommands.localizedString(locale, 'roll-virtueofkings', []) + '\n';
	}

	// if this is a test and it failed, apply conditions or other consequences
	if (resultingPoints && test != 'protection' && resultingPoints > successes && doShadow) {
		// accrue shadow
		let r = tokenCommandExecute('gain', channelID, locale, g, charID, 'shadow', '+', Number(resultingPoints - successes), false);
		followup.push(slashcommands.localizedString(locale, 'condition-shadow', [charName, String(resultingPoints - successes)]));
		if (g && gameHasConfigFlag(g, 'shadow-eye') && !g.combat.active) followup.push(r);
	}
	resultingCondition = stringutil.lowerCase(resultingCondition);
	if (resultingCondition.startsWith('poison') && successes == 0) {
		// do the poison endurance damage
		let [dmg, r2] = enduranceLossDamage(resultingPoints, 'poison', g, charID, locale, channelID);
		followup.push(r2 + '\n');
		resultingCondition = '';
	}
	if (resultingCondition.startsWith('visions') && successes == 0) {
		// do the endurance damage
		let r = tokenCommandExecute('gain', channelID, locale, g, charID, 'endurance', '-', resultingPoints * 2, false);
		followup.push(slashcommands.localizedString(locale, 'condition-endurance', [charName, String(resultingPoints * 2)]));
		if (tokenValue(g, charID, 'endurance') <= 0) followup.push(giveWound(g, charID, channelID, locale, 'untreated', 1, null, false));
		resultingCondition = '';
	}
	if (resultingCondition && successes == 0 && g.characters[charID][resultingCondition] == false) {
		// get the condition
		g.characters[charID][resultingCondition] = true;
		followup.push(slashcommands.localizedString(locale, 'condition-gained', [charName, slashcommands.localizedString(locale, 'condition-' + resultingCondition, [])]));
	}
	if (successes == 0 && test == 'protection' && targetTN != 0 && doWound) {
		// roll for a wound, account for resultingDeadly and Tough as Old Tree-Roots
		followup.push(giveWound(g, charID, channelID, locale, 'untreated', resultingDeadly ? -2 : 1, null, false));
	}
	if (successes == 0 && test == 'protection' && resultingPoints && doWound) {
		if (givePoison(g, charID, resultingPoints, false, channelID)) {
			followup.push(slashcommands.localizedString(locale, 'condition-poisoned', [charName, slashcommands.localizedString(locale, 'condition-severity-' + resultingPoints, []) + ' ' + slashcommands.localizedString(locale, 'condition-poison', [])]));
		}
	}
	// if a Song roll when a song is in use, apply unweary
	if (test == 'skill' && skill == 'song' && g.songCharacters.includes(String(charID))) {
		if (successes > 0 && ((councilActive(g) && g.songUsed == 'Lay') || (g.combat.active && g.songUsed == 'Victory') || (g.journey.active && g.songUsed == 'Walking'))) {
			g.characters[charID].unweary = true;
			followup.push(slashcommands.localizedString(locale, 'condition-unwearied', [charName]));
		}
		g.songCharacters = g.songCharacters.filter(x => x != String(charID));
	}
	// if Strider mode, possibly add on a fortune or ill fortune roll
	if (isStriderMode(g) && (rollResults[4] == 0 || rollResults[4] == 12)) {
		let newTableName = (rollResults[4] == 12 ? 'fortune' : 'illfortune');
		let [error, range, code, text, finalroll, rollstrings, ] = lookuptables.tables_roll(newTableName, locale, '', null, false, torRollDice);
		if (error == null) {
			followup = followup.concat(rollstrings);
			followup.push(tables_formatted_result(newTableName, finalroll, range, code, text));
			if (range == '0') r += increaseEyeRating(g, 2);
		}
	}
	// add any comment
	if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []))  + '__**: ' + comment;

	if (debugString != '') g.debugString = debugString;
	if (gameHasConfigFlag(g, 'always-explain')) followup.push('**Explanation**:\n' + g.debugString);

	if (test == 'protection') combat_log(g, rollLine + ': ' + r + '\n' + warnings);

	let theEmbed = embeds.buildEmbed(
		charName,
		charImage,
		charLink,
		rollLine,
		r + '\n' + outputAddition + warnings,
		'',
		bot, null,
		getUserConfig(userID,'selectedGame'),
		embeds.BUTTON_GREEN,
		userID
		);

	return [rollResults[5], {embed: theEmbed}, ...followup];
}

// TOR dice to do Healing for a wound or poison treatment
function treatCommand(userID, channelID, userName, locale, data) {
	let charName = userName, charImage = '', charLink = '';
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID);
	g.lastUsed = Date.now();

	// parse the options
	let patient = null, bonus = 0, noitem = gameHasConfigFlag(g, 'default-noitem'), magical = false;
	let dice = null, feat = null, hope = null, modifier = 0, weary = null, miserable = null, lmdice = null, tn = null, comment = '';
	let warnings = '', treatingPoison = false;

	// parse options
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'patient') {
			if (stringutil.lowerCase(data.options[i].value) == slashcommands.localizedString(locale,'self',[])) {
				patient = currentCharacter(userID);
			} else {
				patient = findCharacterInGame(g, data.options[i].value);
			}
			if (patient == null) return slashcommands.localizedString(locale, 'error-charnotfound', [data.options[i].value]);
		}
		if (data.options[i].name == 'bonus') bonus = Number(data.options[i].value);
		if (data.options[i].name == 'noitem') noitem = (data.options[i].value == 'no');
		if (data.options[i].name == 'feat') feat = data.options[i].value;
		if (data.options[i].name == 'hope') hope = data.options[i].value;
		if (data.options[i].name == 'modifier') modifier = Number(data.options[i].value);
		if (data.options[i].name == 'weary') weary = data.options[i].value;
		if (data.options[i].name == 'miserable') miserable = data.options[i].value;
		if (data.options[i].name == 'lmdice') lmdice = data.options[i].value;
		if (data.options[i].name == 'tn') tn = Number(data.options[i].value);
		if (data.options[i].name == 'comment') comment = data.options[i].value;
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[i].value);
		}
	}

	// verify the patient is wounded and not treated
	if (patient == null) return slashcommands.localizedString(locale, 'error-notarget', []);
	let patientName = characterDataFetchHandleNulls(g, patient, 'name', null),
		wounded = characterDataFetchHandleNulls(g, patient, 'wounded', null),
		woundtreated = characterDataFetchHandleNulls(g, patient, 'woundtreated', null),
		wounddaysleft = characterDataFetchHandleNulls(g, patient, 'wounddaysleft', null);

	if (wounded != 'TRUE' && g.characters[patient].poisoned > 1) treatingPoison = true;
	if (wounded == 'TRUE' && woundtreated != 'untreated' && g.characters[patient].poisoned > 1) treatingPoison = true;

	if (treatingPoison) {
		if (g.characters[patient].poisontreated) return slashcommands.localizedString(locale, 'treat-alreadytreated', [patientName]);
	} else {
		if (wounded != 'TRUE') return slashcommands.localizedString(locale, 'treat-notwounded', [patientName]);
		if (woundtreated != 'untreated') return slashcommands.localizedString(locale, 'treat-alreadytreated', [patientName]);
	}

	// now look up the character info, after we have determined who it is
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	[charName, charImage, charLink] = getRollCharacterInfo(g, charID, userID);

	// determine the favored, bonus, tn based on the kind of test
	let debugString = '';
	let featToRoll = 1, successToRoll = 0, mods = 0, targetTN = 0, debugStringAddition = '', outputAddition = '';
	[featToRoll, successToRoll, mods, targetTN, debugStringAddition, outputAddition] = torRollLookupSkill(g, charID, 'healing', noitem, locale);
	debugString += debugStringAddition;
	if (treatingPoison && g.characters[patient].poisoned > 1) {
		successToRoll -= g.characters[patient].poisoned - 1;
		debugString += ':anger: Success dice lowered to ' + successToRoll + ' due to poison severity\n';
	}

	if (lmdice == null) lmdice = false; // always default to false since even if the LM is rolling, they're rolling for a character

	// handle options common to different kinds of rolls
	[featToRoll, debugString] = torRollHandleFeat(feat, featToRoll, debugString, g.characters[charID].dismayed);
	[successToRoll, mods, targetTN, debugString] = torRollHandleBonuses(bonus, noitem, modifier, tn, targetTN, g, charID, successToRoll, mods, debugString);

	// handle weary and miserable
	[weary, miserable, debugString] = torRollHandleWearyMiserable(g, charID, weary, miserable, debugString, channelID, 'treat');

	// handle hope spends
	[hope, magical, successToRoll, warnings, debugString] = torRollHandleHope(g, charID, hope, magical, successToRoll, warnings, debugString, 'healing', locale);

	// do the roll
	let r = '', rollResults, rollLine = '**'+ charName + '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' ';
	rollLine += stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'roll', 'skill', 'healing')) + ' (' + patientName + '): ';
	
	rollResults = torRollDice(featToRoll, successToRoll, 0, mods, weary, miserable, magical, lmdice, hasLifepath(g, charID, 'favoured'), locale, g.dicestyle, gameHasConfigFlag(g, 'magical-tengwar'));
	rollLine += rollResults[3];

	// eye accrual due to rolling Eye
	let [commentadd, radd] = torRollHandleEyeAccrual(g, lmdice, magical, rollResults[4], locale);
	comment += commentadd;
	r += radd;
	let successes = 0;
	if (rollResults[1] >= 0 && targetTN && (rollResults[0] >= targetTN || rollResults[1] == 1)) successes = rollResults[2] + 1;

	// if there's a target, put in a success or failure
	if (targetTN && targetTN != 0) r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', [])) + '__**: ' + compareToTarget(rollResults, targetTN, locale);

	// if virtue of kings and an eye roll, suggest a reroll
	if (hasCulturalBlessing(g, charID, 'virtue', 'virtue of kings') && rollResults[4] == 0) {
		warnings += slashcommands.localizedString(locale, 'roll-virtueofkings', []);
	}

	// carry out the results
	let followup = [];
	if (successes == 0) { // set to failed
		if (treatingPoison) g.characters[patient].poisontreated = true;
		else characterDataUpdate(g, patient, 'woundtreated', null, '=', 'failed', null, null, channelID);
	} else {
		if (treatingPoison) {
			givePoison(g, patient, 0, true, channelID);
			followup.push(patientName + ' ' + slashcommands.localizedString(locale, 'condition-poison-gandalf', []));
		} else { // set to treated
			characterDataUpdate(g, patient, 'woundtreated', null, '=', 'treated', null, null, channelID);
			wounddaysleft -= Number(successes);
			if (wounddaysleft < 1) wounddaysleft = 1;
			characterDataUpdate(g, patient, 'wounddaysleft', null, '=', wounddaysleft, 0, null, channelID);
			followup.push(slashcommands.localizedString(locale, 'treat-treated', [patientName, String(wounddaysleft)]));
			if (gameHasConfigFlag(g, 'autotreat-poison') && g.characters[patient].poisoned > 1) {
				givePoison(g, patient, 0, true, channelID);
				followup.push(patientName + ' ' + slashcommands.localizedString(locale, 'condition-poison-gandalf', []));
			}
		}
	}
	filehandling.saveGameData(gameData);

	// if Strider mode, possibly add on a fortune or ill fortune roll
	if (isStriderMode(g) && (rollResults[4] == 0 || rollResults[4] == 12)) {
		let [error, range, code, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll((rollResults[4] == 12 ? 'fortune' : 'illfortune'), locale, '', null, false, torRollDice);
		if (error == null) {
			followup = followup.concat(rollstrings);
			followup.push(tables_formatted_result(newTableName, finalroll, range, code, text));
			if (range == '0') r += increaseEyeRating(g, 2);
		}
	}
	// add any comment
	if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []))  + '__**: ' + comment;

	if (debugString != '') g.debugString = debugString;
	if (gameHasConfigFlag(g, 'always-explain')) followup.push('**Explanation**:\n' + g.debugString);

	let theEmbed = embeds.buildEmbed(
		charName,
		charImage,
		charLink,
		rollLine,
		r + '\n' + outputAddition + warnings,
		'',
		bot, null,
		getUserConfig(userID,'selectedGame'),
		embeds.BUTTON_GREEN,
		userID
		);

	return [rollResults[5], {embed: theEmbed}, ...followup];
}

function calculatePiercingBlow(wData, adversary, bane, g, charID, locale, handedness) {
	if (wData[3] == null || wData[2] == null) return ['', 99, 0, 1];
	let r = '';
	let protectionTN = wData[3];
	// in 2e use handedness to divide if it's got two values
	if (String(protectionTN).includes('/') && handedness) {
		protectionTN = protectionTN.split('/')[Number(handedness)-1];
	}
	protectionTN = Number(protectionTN);
	// superior fell
	if (adversary && bane && weaponHasQuality(wData, 'superior fell') && weaponHasOrigin(wData,'numenorean')) {
		protectionTN += characterDataFetchHandleNumbers(g, charID, 'sheet','valour') - 2;
	}
	// figure out modified edge
	let edge = wData[2];
	// superior keen (E) with bane: improve edge by Valour-1
	if (adversary && bane && weaponHasQuality(wData, 'superior keen') && weaponHasOrigin(wData,'elven')) {
		if (stringutil.lowerCase(edge) == 'g') edge = 11;
		edge = Number(edge);
		edge -= characterDataFetchHandleNumbers(g, charID, 'sheet','valour') - 1;
	}

	// figure out modified protection feat roll
	let protectionFeat = 1;
	return [r, protectionTN, edge, protectionFeat];
}

function weaponCanBeUsedBrawling(wData) {
	if (stringutil.lowerCase(wData[5]) == 'brawling') return true;
	if (wData[8] == '2H') return false;
	if (weaponHasQuality(wData, 'mithril weapon')) return true;
	return false;
}

function weaponCanBeUsedRanged(wData) {
	if (stringutil.lowerCase(wData[17]).includes('dagger') && gameHasConfigFlag(g, 'unthrowable-daggers')) return false;
	if (wData[10] == 'TRUE') return true;
	return false;
}

// pop up the LM's protection test panel in their channel
function showLMprotTestPanel(g, userID, locale) {
	if (adversariesWithTests(g).length == 0) return; // maybe a foe got a test but also got killed on that action so they don't show up and no one else has a test
	let interfaceElements = null, buttonColor = embeds.BUTTON_GREYPLE, title = 'Narvi', bodyText = '';
	[interfaceElements, buttonColor, title, bodyText] = goCommandLMProtTests(g, userID, locale);
	botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, 
		embeds.buildGUI(
			locale, g, getGameName(g), [userID],
			title,
			'https://cdn.discordapp.com/attachments/848011016612478987/881999824503857212/Narvi_avatar.png',
			'https://bitbucket.org/HawthornThistleberry/narvi/',
			bodyText,
			'',
			buttonColor,
			interfaceElements
		)
	);
}

// TOR dice for attacks by player-heroes
function attackCommand(userID, channelID, userName, locale, data) {
	let charName = userName, charImage = '', charLink = '';
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID);
	g.lastUsed = Date.now();

	// parse the options
	let parry = null, weapon = null, handedness = null, attacktype = null, bonus = 0, noitem = gameHasConfigFlag(g, 'default-noitem'), magical = false, target = null;
	let dice = null, feat = null, hope = null, modifier = 0, weary = null, miserable = null, lmdice = null, tn = null, comment = '';
	let warnings = '';

	// parse options
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'target') target = data.options[i].value;
		if (data.options[i].name == 'parry') parry = Number(data.options[i].value);
		if (data.options[i].name == 'weapon') weapon = data.options[i].value;
		if (data.options[i].name == 'handedness') handedness = data.options[i].value;
		if (data.options[i].name == 'attacktype') attacktype = data.options[i].value;
		if (data.options[i].name == 'bonus') bonus = Number(data.options[i].value);
		if (data.options[i].name == 'noitem') noitem = (data.options[i].value == 'no');
		if (data.options[i].name == 'feat') feat = data.options[i].value;
		if (data.options[i].name == 'hope') hope = data.options[i].value;
		if (data.options[i].name == 'modifier') modifier = Number(data.options[i].value);
		if (data.options[i].name == 'weary') weary = data.options[i].value;
		if (data.options[i].name == 'miserable') miserable = data.options[i].value;
		if (data.options[i].name == 'lmdice') lmdice = data.options[i].value;
		if (data.options[i].name == 'tn') tn = Number(data.options[i].value);
		if (data.options[i].name == 'comment') comment = data.options[i].value;
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[i].value);
		}
	}
	if (attacktype == 'escape' && handedness == null) handedness = '1h';

	// now look up the character info, after we have determined who it is
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	[charName, charImage, charLink] = getRollCharacterInfo(g, charID, userID);
	if (g.combat.active && g.characters[charID].knockback) return slashcommands.localizedString(locale, 'combat-isknockback', [charName]);

	// default target based on engagement, if only a single engaged enemy
	if (target == null && encounters.count_engagement(g, charID) == 1) {
		target = encounters.engaged_adversaries(g, charID)[0];
	}

	// if no target and no parry, default to 0
	if (target == null && parry == null) parry = 0;
	// if no combat or no encounter, ignore targets
	if (target && (!g.combat.active || g.combat.encounter == '')) target = null;

	let adversary = null;
	if (target) {
		// search for it and report an error if not found, then skip the rest
		adversary = encounters.encounters_adversary_name_match(g, g.combat.encounter, target, true);
		if (adversary == null) return slashcommands.localizedString(locale, 'adversary-notexists', [target]);
		// look up its parry (only if parry is null)
		if (parry == null) parry = g.encounters[g.combat.encounter].adversaries[adversary].parry;
	}

	if (g.combat.encounter != '' && adversary && encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[adversary]))
		// warnings += slashcommands.localizedString(locale, 'foe-already-dead', [adversary]) + '\n';
		// the above is how it used to be and makes it a warning, and the below makes it a show-stopper so you never spend the hope or waste the roll; but why would you ever attack a dead adversary?
		return slashcommands.localizedString(locale, 'foe-already-dead', [adversary]);

	// determine the favored, bonus, tn based on the kind of test
	let debugString = '';
	let featToRoll = 1, successToRoll = 0, mods = 0, targetTN = 0;

	// determine the weapon and look it up
	let rangedonly = false;
	if (g.combat.active && g.combat.phase == initiative.PHASE_OPENINGVOLLEY) rangedonly = true;
	if (g.characters[charID].stance == 'rearward') rangedonly = true;
	if (weapon == null) weapon = stringutil.wordWithoutSpaces(findBestWeapon(g, charID, rangedonly));
	if (weapon == '' || weapon == null) return slashcommands.localizedString(locale, 'attack-noweapons', []);
	let wData, wName = weapon;
	[weapon, wData] = getWeaponData(g, charID, weapon);
	if (wData == null) return slashcommands.localizedString(locale, 'attack-weaponnotfound', [wName]);
	if ((g.characters[charID].seized || g.characters[charID].bitten || g.characters[charID].drained) && !weaponCanBeUsedBrawling(wData)) {
		return slashcommands.localizedString(locale, 'condition-brawling-only', []);
	}
	if (weapon == 'unarmed' && g.combat.encounter != '' && adversary && encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'hideous toughness')) {
		return slashcommands.localizedString(locale, 'foe-no-unarmed', []);
	}

	// figure out the success dice
	successToRoll = skillValue(wData[0]);
	if ((g.characters[charID].seized || g.characters[charID].bitten || g.characters[charID].drained) && weaponCanBeUsedBrawling(wData)) {
		handedness = '1h';
	}
	if (handedness == '1h' && weaponHasQuality(wData, 'mithril weapon') && (characterDataFetchHandleNumbers(g, charID, 'sheet', 'brawling') > skillValue(wData[0]) || (characterDataFetchHandleNumbers(g, charID, 'sheet', 'brawling') == skillValue(wData[0]) && hasTrait(g, charID, 'virtue', 'brother to bears')))) {
		wData[5] = 'brawling'; // per Michele Bugio, Mithril Weapon can always be brawling
		successToRoll = characterDataFetchHandleNumbers(g, charID, 'sheet', 'brawling');
		debugString += ':anger: using Brawling for Mithril Weapon, so set success dice to ' + successToRoll + '\n';
	} else {
		successToRoll = skillValue(wData[0]);
		debugString += ':anger: Weapon lookup set success dice to ' + successToRoll + '\n';
	}
	if (stringutil.lowerCase(wData[5]) == 'brawling' && hasTrait(g, charID, 'virtue', 'brother to bears')) {
		successToRoll++; // reverse the -1d
		debugString += ':anger: Brother To Bears raised Brawling success dice to ' + successToRoll + '\n';
	}

	// if handedness specified but weapon 8 does not support it, error message
	if ((handedness == '1h' && wData[8] == '2H') || (handedness == '2h' && wData[8] == '1H')) {
		warnings += slashcommands.localizedString(locale, 'attack-badhandedness', [weapon, wData[8], stringutil.upperCase(handedness)]);
	}
	// if no handedness specified but the chosen weapon supports both, error message, must specify
	if (handedness == null && wData[8] == 'Both') {

		// build and save the 1h and 2h versions of the command
		let data1h = JSON.parse(JSON.stringify(data));
		if (data1h.options == undefined) data1h.options = [];
		data1h.options.push({'value': '1h', 'type': 3, 'name': 'handedness'});
		setUserConfig(userID, 'button-1h', data1h);
		let data2h = JSON.parse(JSON.stringify(data));
		if (data2h.options == undefined) data2h.options = [];
		data2h.options.push({'value': '2h', 'type': 3, 'name': 'handedness'});
		setUserConfig(userID, 'button-2h', data2h);

		// build and return the embed with the buttons that will later refer to them
		return embeds.buildDialog(
			locale, g, getGameName(g), [userID],
			charName,
			'https://images.emojiterra.com/google/android-12l/512px/2694.png',
			'https://bitbucket.org/HawthornThistleberry/narvi/src/master/PlayerGuide.md',
			slashcommands.localizedString(locale, 'attack-missinghandedness', [weapon]),
			'',
			embeds.BUTTON_GREYPLE,
			[
				{
					'command':'button-1h',
					'color':2,
					'label':'attack-button-1h',
					'extratext':'',
					'emoji':BUTTON_ONE
				},
				{
					'command':'button-2h',
					'color':2,
					'label':'attack-button-2h',
					'extratext':'',
					'emoji':BUTTON_TWO
				}
			]);
	}

	// if no handedness specified set to what the weapon supports
	if (handedness == null) handedness = stringutil.lowerCase(wData[8]);
	handedness = Number(handedness.charAt(0));

	if (comment == '') comment = slashcommands.localizedString(locale, attacktype == 'nonlethal' ? 'attack-nonlethally' : 'attack-with', []) + ' ' + weapon;

	if (lmdice == null) lmdice = false; // always default to false since even if the LM is rolling, they're rolling for a character

	// handle options common to different kinds of rolls
	[featToRoll, debugString] = torRollHandleFeat(feat, featToRoll, debugString, g.characters[charID].dismayed);
	// Sure at the Mark
	if (feat == null && featToRoll == 1 && hasTrait(g, charID, 'virtue', 'sure at the mark') && wData[10] == 'TRUE' && (g.characters[charID].stance == 'rearward' || wData[9] == 'FALSE' || (g.combat.active && g.combat.phase == initiative.PHASE_OPENINGVOLLEY))) {
		featToRoll = 2;
		debugString += ':anger: Sure At The Mark set feat dice to best of two\n';
	}
	if (feat == null && featToRoll == 1 && hasCulturalBlessing(g, charID, 'furious') && characterDataFetchHandleNulls(g, charID, 'wounded', null) == 'TRUE') {
		featToRoll = 2;
		debugString += ':anger: Feat die favored due to Furious cultural blessing\n';
	}
	// In Defiance of Evil
	if (feat == null && featToRoll == 1 && hasTrait(g, charID, 'virtue', 'in defiance of evil') && adversary && g.encounters[g.combat.encounter].adversaries[adversary].hrscore <= 0) {
		featToRoll = 2;
		debugString += ':anger: In Defiance of Evil set feat dice to best of two\n';
	}
	// Dragon-Slayer: /attack is favored if target Might > 1
	if (feat == null && featToRoll == 1 && hasTrait(g, charID, 'virtue', 'dragon-slayer') && adversary && g.encounters[g.combat.encounter].adversaries[adversary].might > 1) {
		featToRoll = 2;
		debugString += ':anger: Dragon-slayer set feat dice to best of two\n';
	}
	if (feat == null && adversary && encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'wind-like speed')) {
		if (featToRoll == 2) featToRoll = 1; else featToRoll = -2;
		debugString += ':anger: Wind-like Speed set feat dice to worst of two\n';
	}
	if (feat == null && adversary && 
		encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'snake-like speed') && // has the ability
		g.encounters[g.combat.encounter].adversaries[adversary].snakeLike >= 0 && // has been set to use it
		g.encounters[g.combat.encounter].adversaries[adversary].hateSpent < g.encounters[g.combat.encounter].adversaries[adversary].might && // has hate to spend this round
		g.encounters[g.combat.encounter].adversaries[adversary].hrscore > g.encounters[g.combat.encounter].adversaries[adversary].snakeLike // maintains its reserves if it spends
		) {
		hateSpend(locale, g, adversary, 1, true);
		if (featToRoll == 2) featToRoll = 1; else featToRoll = -2;
		debugString += ':anger: Snake-like Speed set feat dice to worst of two\n';
	}

	targetTN = characterDataFetchHandleNumbers(g, charID, 'strengthtn',null);
	[successToRoll, mods, targetTN, debugString] = torRollHandleBonuses(bonus, noitem, modifier, tn, targetTN, g, charID, successToRoll, mods, debugString);
	if (!isNaN(g.characters[charID].tempbonus) && g.characters[charID].tempbonus != 0) {
		successToRoll += g.characters[charID].tempbonus;
		debugString += ':anger: A temporary bonus/penalty changed saved raised success dice to ' + successToRoll + '\n';
		g.characters[charID].tempbonus = 0;
		filehandling.saveGameData(gameData);
	}
	if (parry != 0) {
		targetTN += parry;
		debugString += ':anger: Parry ' + parry + ' raised TN to ' + targetTN + '\n';
	}

	// handle weary and miserable
	[weary, miserable, debugString] = torRollHandleWearyMiserable(g, charID, weary, miserable, debugString, channelID, 'attack');
	// Rune-Scored Weapon: ignore weary and miserable when attacking with it
	if ((weary || miserable) && !noitem && weaponHasQuality(wData, 'rune-scored weapon')) {
		weary = false;
		miserable = false;
		debugString += ':anger: Rune-Scored Weapon prevents weary and miserable from applying\n';
	}

	// handle hope spends
	if (hope != null && weaponHasCurse(wData, 'malice')) {
		hope = null;
		debugString += ':anger: Malice prevents the spending of hope\n';
	}
	[hope, magical, successToRoll, warnings, debugString] = torRollHandleHope(g, charID, hope, magical, successToRoll, warnings, debugString, wData[5], locale);

	//if an active combat, handle stance, rally
	if (g.combat.active) {
		if (attacktype == 'escape' && charEffectiveStance(g, charID) != 'defensive' && charEffectiveStance(g, charID) != 'skirmish') {
			warnings += slashcommands.localizedString(locale, 'attack-escapefromdefensive', []);
			attacktype = null;
		}
		if (g.combat.phase >= initiative.PHASE_FORWARD && g.combat.phase <= initiative.PHASE_REARWARD) {
			// apply stance effects
			if (charEffectiveStance(g, charID) == 'forward') {
				successToRoll += 1;
				debugString += ':anger: Forward stance raised success bonus dice to ' + successToRoll + '\n';
			}
			if (charEffectiveStance(g, charID) == 'skirmish' && attacktype != 'escape') {
				successToRoll -= 1;
				debugString += ':anger: Skirmish stance lowered success bonus dice to ' + successToRoll + '\n';
			}
			if (charEffectiveStance(g, charID) == 'defensive') {
				successToRoll -= encounters.count_engagement(g, charID);
				debugString += ':anger: Defensive stance lowered success bonus dice by ' + encounters.count_engagement(g,charID) + ' to ' + successToRoll + '\n';
			}
		}
		// account for rally
		let stancenum = stanceList.indexOf(charEffectiveStance(g, charID)) + 1;
		if (stancenum <= g.rallycurrent) {
			successToRoll++;
			debugString += ':anger: Rally raised success bonus dice to ' + successToRoll + '\n';
		}
	}
	// advantages/complications (accounting for straight flight)
	let ignoreComplications = !noitem && weaponHasQuality(wData, 'straight flight');
	g.combatMods.forEach(c => {
		if (combatModActive(c,charID)) {
			if (c.modifier > 0 || !ignoreComplications) {
				successToRoll += c.modifier;
				debugString += ':anger: ' + (c.modifier > 0 ? 'Advantage raised' : 'Complication lowered') + ' success bonus dice by ' + String(c.modifier) + ' to ' + successToRoll + '\n';
			} else if (c.modifier < 0 && ignoreComplications) {
				debugString += ':anger: Complication (' + c.modifier + 'd) ignored due to Straight Flight\n';
			}
		}
	});

	let followup = [];
	let s = initiative.takeInitiativeAction(locale, g, charID, 'attack');
	if (s != '') followup.push(s);

	// do the roll
	let r = '', rollResults, rollLine = '**'+ charName + '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' ';
	rollLine += stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'attack')) + ' (' + weapon + ') ';
	if (adversary) rollLine +=' - ' + adversary;
	rollLine += ': ';
	rollResults = torRollDice(featToRoll, successToRoll, 0, mods, weary, miserable, magical, lmdice, hasLifepath(g, charID, 'favoured'), locale, g.dicestyle, gameHasConfigFlag(g, 'magical-tengwar'));
	rollLine += rollResults[3];
	let successes = 0;
	if (rollResults[1] >= 0 && targetTN && (rollResults[0] >= targetTN || rollResults[1] == 1)) successes = rollResults[2] + 1;

	// apply various consequences and effects
	// eye accrual due to rolling Eye
	let [commentadd, radd] = torRollHandleEyeAccrual(g, lmdice, magical, rollResults[4], locale);
	comment += commentadd;
	r += radd;
	// if there's a target, put in a success or failure
	if (targetTN && targetTN != 0) r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', [])) + '__**: ' + compareToTarget(rollResults, targetTN, locale);

	let theButtons = [];
	// if in skirmish mode and this is not a ranged weapon, add a warning
	if (charEffectiveStance(g, charID) == 'skirmish' && !weaponCanBeUsedRanged(wData)) warnings += slashcommands.localizedString(locale, 'attack-skirmishnotranged', [weapon]) + '\n';

	r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'attack')) + '__**: ';

	// record the weapon so that /spend can reflect what kind of weapon it is properly
	// and also the handedness, for later lookup of shields
	g.characters[charID].curWeapon = weapon;
	g.characters[charID].handedness = handedness;
	g.characters[charID].tengwar = 0;
	g.characters[charID].damageDone = 0;
	g.characters[charID].protectionTN = null;

	let loremasterOutcome = '';
	// if virtue of kings and an eye roll, suggest a reroll
	if (hasCulturalBlessing(g, charID, 'virtue', 'virtue of kings') && rollResults[4] == 0) {
		warnings += slashcommands.localizedString(locale, 'roll-virtueofkings', []);
	}

	// if it was a hit
	let hit = false;
	if ((rollResults[0] >= targetTN && rollResults[1] != -1) || rollResults[1] == 1) {
		hit = true;
		let bane = false;
		// it was a hit! figure out the damage
		let dmg = Number(wData[1]);
		// tengwar get recorded for later /spend; also account for escape
		g.characters[charID].tengwar = Number(rollResults[2]);
		if (attacktype == 'escape') dmg = 0;
		if (attacktype == 'escape' && dmg == 0) r += slashcommands.localizedString(locale, 'attack-escaped', []);
		else {

			// check for Flame of Ud�n
			if (adversary && (encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'flame of udun') || encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'flame of ud�n'))) {
				let points = 1;
				g.encounters[g.combat.encounter].adversaries[adversary].abilities.forEach(a => {
					if (stringutil.lowerCase(a.name).startsWith('flame of u')) points = a.amount;
				});
				if (isNaN(points) || points < 1 || points > 3) points = 1;
				let [dmg, r2] = enduranceLossDamage(fellAbility.amount ? fellAbility.amount : 1, 'fire', g, charID, locale, channelID);
				r += r2 + '\n';
			}

			// check for bane and superior grievous increasing weapon damage
			if (adversary) {
				bane = weaponHasRelevantBane(wData, g.encounters[g.combat.encounter].adversaries[adversary].banetype, g.encounters[g.combat.encounter].adversaries[adversary].hrtype);

				if (weaponHasQuality(wData, 'superior grievous') && weaponHasOrigin(wData, 'numenorean') && bane) {
					dmg += characterDataFetchHandleNumbers(g, charID, 'sheet','valour') - 1;
				}
			}
			
			// show damage and tengwar results
			r += slashcommands.localizedString(locale, 'attack-hitdmg', [String(dmg)]);
			g.characters[charID].damageDone = dmg;
			if (g.characters[charID].tengwar) {
				r += ' ' + slashcommands.localizedString(locale, 'attack-spendtengwar', [g.characters[charID].tengwar, emoji.customEmoji(emoji.TENGWAR,0,0,false)]);
			}
			r += '!';

			// all other adversary handling
			if (adversary) {

				// automatically do damage
				let [lmAddition, rAddition] = damageAdversary(g, adversary, dmg, locale, charID, charName, wData, handedness, successes);
				loremasterOutcome += lmAddition;
				if (rAddition != '') r += '\n ' + rAddition;
			}
		}

		// natural 11 (due to Favoured lifepath) special handling
		let modifiedFeatRoll = Number(rollResults[4]);
		let savedFeatRoll = modifiedFeatRoll;
		if (modifiedFeatRoll == 11) {
			modifiedFeatRoll = 1; // by default, 11 is a 1
			if (gameHasConfigFlag(g, 'eleven-pierces')) modifiedFeatRoll = 11; // already a pierce
			else if (gameHasConfigFlag(g, 'eleven-can-pierce')) savedFeatRoll = 1; // start at 1 but can spend to pierce
			else savedFeatRoll = -99; // cannot spend to pierce
		}

		// calculate stuff related to the piercing blow
		let [rAddition, protectionTN, edge, protectionFeat] = calculatePiercingBlow(wData, adversary, bane, g, charID, locale, handedness);
		r += rAddition;

		g.characters[charID].protectionTN = {'injury': protectionTN, 'edge': edge, 'feat': protectionFeat, 'bonus': 0, 'charID': charID, 'attackFeatRoll': savedFeatRoll, 'adversary': adversary};

		// now see if we already made edge
		let madeEdge = false;
		if (attacktype != 'nonlethal' && attacktype != 'escape' && (
				(!isNaN(edge) && modifiedFeatRoll >= Number(edge)) ||
				(stringutil.lowerCase(edge) == 'g' && modifiedFeatRoll >= 12))) {
			madeEdge = true;
			// Foe-Slaying: Bane target's protection rolls are ill-favoured (if already ill-favored, autofails)
			if (weaponHasQuality(wData, 'foe-slaying') && bane) {
				r += '\n' + slashcommands.localizedString(locale, 'attack-foeslaying', []);
				protectionFeat = 'illfavoured';
			}
			// Fierce Shot: protection roll is ill-favored
			// Splitting Blow: adversary Protection rolls are ill-favoured in close combat
			if ((hasTrait(g, charID, 'virtue', 'fierce shot') && wData[10] == 'TRUE' && ((g.combat.active && g.combat.phase == initiative.PHASE_OPENINGVOLLEY) || charEffectiveStance(g, charID) == 'rearward')) || 
				(hasTrait(g, charID, 'virtue', 'splitting blow') && ['forward','open','defensive'].includes(charEffectiveStance(g, charID)))) {
				r += '\n' + slashcommands.localizedString(locale, 'attack-protectionillfavored', []);
				if (adversary) protectionFeat = 'illfavoured';
			}
			r += '\n ' + slashcommands.localizedString(locale, 'attack-madeedge', [protectionTN]);
			// save a queued protection test for the adversary
			if (g.combat.encounter != '' && adversary && g.encounters[g.combat.encounter].adversaries[adversary]) {
				if (g.encounters[g.combat.encounter].adversaries[adversary].protectionTests == undefined) g.encounters[g.combat.encounter].adversaries[adversary].protectionTests = [];
				g.encounters[g.combat.encounter].adversaries[adversary].protectionTests.push({'injury': protectionTN, 'responsible': charID, 'feat': protectionFeat, 'bonus': 0, 'modifier': 0, 'condition': null, 'poison': 0, 'hideoustoughnessroll': 0});
				loremasterOutcome += adversary + ': ' + charName + ' ' + slashcommands.localizedString(locale, 'attack-madeedge', [protectionTN]) + '\n';
				filehandling.saveGameData(gameData);
				showLMprotTestPanel(g, g.loremaster, locale);
			}
		}
		// build tengwar buttons
		if (hit && Number(rollResults[2]) > 0 && attacktype != 'escape') {
			theButtons.push(embeds.buildButton(locale, 'heavy', 3, 'spend-h', '(+' + spendCalculateHeavy(g, charID) + ')', SPEND_HEAVY, getGameName(g), [userID]));

			if (['axes','brawling','swords','spears'].includes(stringutil.lowerCase(wData[5])) && weapon != 'stone') {
				theButtons.push(embeds.buildButton(locale, 'fend', 3, 'spend-f', '(+' + spendCalculateFend(g, charID) + ')', SPEND_FEND, getGameName(g), [userID]));
			}
			let buttonEmoji = SPEND_PIERCE;
			let highestFeatPossible = modifiedFeatRoll + Number(rollResults[2]) * spendCalculatePierce(g, charID);
			if (highestFeatPossible < Number(edge)) buttonEmoji = SPEND_REDX;
			if (!madeEdge && (!gameHasConfigFlag(g, 'hide-pierce') || buttonEmoji == SPEND_PIERCE) && (['bows','swords','spears'].includes(stringutil.lowerCase(wData[5])) || stringutil.lowerCase(wData[18]).includes('dagger')) && attacktype != 'nonlethal') {
				theButtons.push(embeds.buildButton(locale, 'pierce', 3, 'spend-p', '(+' + spendCalculatePierce(g, charID) + ')', buttonEmoji, getGameName(g), [userID]));
			}
			if (hasShield(g, charID, true, false) !== false && g.combat.phase != initiative.PHASE_OPENINGVOLLEY && g.combat.phase != initiative.PHASE_REARWARD && (!adversary || (characterDataFetchHandleNumbers(g, charID, 'sheet','strength') > g.encounters[g.combat.encounter].adversaries[adversary].attribute && !g.encounters[g.combat.encounter].adversaries[adversary].pushback))) {
				theButtons.push(embeds.buildButton(locale, 'shield', 3, 'spend-s', '(-1d)', SPEND_SHIELD, getGameName(g), [userID]));
			}
			if (g.characters[charID].seized || g.characters[charID].bitten || g.characters[charID].drained) {
				theButtons.push(embeds.buildButton(locale, 'escape', 3, 'spend-u', '', SPEND_ESCAPE, getGameName(g), [userID]));
			}
		}
		// Flame of Hope: all allies gain +1 Endurance when you hit, plus 1 per tengwar
		if (weaponHasQuality(wData, 'flame of hope')) {
			r += '\n' + slashcommands.localizedString(locale, 'attack-flameofhope', [Number(rollResults[2])+1]);
			for (let key in g.characters) {
				if (!g.characters[key].active) continue;
				r += '\n' + tokenCommandExecute('heal', channelID, locale, g, key, 'endurance', '+', Number(rollResults[2])+1, false);
				//characterDataUpdate(g, key, 'endurance', null, '+', Number(rollResults[2])+1, 0, 'maxendurance', channelID);
			}
		}
		// if these weren't handled automatically, mention them instead
		if (!adversary) {
			// Biting Dart: strip 1 Hate/Resolve (3 if Bane) on any hit
			if (weaponHasQuality(wData, 'biting dart')) {
				r += '\n' + slashcommands.localizedString(locale, 'attack-bitingdart', []);
			}
			// Gleam of Terror: strip 2 Hate/Resolve on any hit
			if (weaponHasQuality(wData, 'gleam of terror')) {
				r += '\n' + slashcommands.localizedString(locale, 'attack-gleamofterror', []);
			}
			// Cleaving: get a second attack on an engaged enemy on a kill
			if (weaponHasQuality(wData, 'cleaving')) {
				r += '\n' + slashcommands.localizedString(locale, 'attack-cleaving', []);
			}
			// Hammering: if endurance loss >= twice Attribute, foe is knocked back
			if (weaponHasQuality(wData, 'hammering')) {
				r += '\n' + slashcommands.localizedString(locale, 'attack-hammering', [dmg]);
			}
		}

	} else {
		if (attacktype == 'escape') r += slashcommands.localizedString(locale, 'attack-failedescape', []);
		else r += slashcommands.localizedString(locale, 'attack-missed', []);
	}
	filehandling.saveGameData(gameData);

	if (loremasterOutcome != '') botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, loremasterOutcome);

	// add any comment
	if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []))  + '__**: ' + comment;

	if (debugString != '') g.debugString = debugString;
	if (gameHasConfigFlag(g, 'always-explain')) followup.push('**Explanation**:\n' + g.debugString);

	combat_log(g, rollLine + ': ' + r + '\n' + warnings);

	let theEmbed = embeds.buildEmbed(
		charName,
		charImage,
		charLink,
		rollLine,
		r + '\n' + warnings,
		'',
		bot, null,
		getUserConfig(userID,'selectedGame'),
		embeds.BUTTON_GREEN,
		userID
		);

	let resultObject = {embed: theEmbed};
	if (theButtons.length > 0) {
		resultObject.components = [{"type": 1,"components": [...theButtons]}];
	}
	return [rollResults[5], resultObject, ...followup];
}

// central function for handling endurance damage to adversaries
function damageAdversary(g, adversary, dmg, locale, charID, charName, wData, handedness, successes) {

	let bane = weaponHasRelevantBane(wData, g.encounters[g.combat.encounter].adversaries[adversary].banetype, g.encounters[g.combat.encounter].adversaries[adversary].hrtype);

	let loremasterOutcomeAddition = '', r = '';
	let oldEndurance = g.encounters[g.combat.encounter].adversaries[adversary].curendurance;
	encounters.encounters_adversary_damage(g, g.combat.encounter, adversary, 'curendurance', dmg);
	let hrloss = 0;
	if (g.encounters[g.combat.encounter].adversaries[adversary].curendurance <= 0) {
		// Hideous Toughness: When you are about to get to 0 Endurance, the attack is a Piercing Blow instead, and if you survive it, you're back to half endurance. Bodiless: same, but full.
		if (encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'hideous toughness') || encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'bodiless')) {
			// restore the endurance since this piercing blow comes into play instead of the damage
			g.encounters[g.combat.encounter].adversaries[adversary].curendurance = oldEndurance;
			// add a protection test but give it a flag saying it's a hideous toughness one
			let [rAddition, protectionTN, edge, protectionFeat] = calculatePiercingBlow(wData, adversary, bane, g, charID, locale, handedness);
			r += slashcommands.localizedString(locale, (encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'hideous toughness') ? 'attack-hideoustoughness' : 'attack-bodiless'), []) + '\n' + rAddition;
			if (weaponHasQuality(wData, 'foe-slaying') && bane) {
				r += '\n' + slashcommands.localizedString(locale, 'attack-foeslaying', []);
				protectionFeat = 'illfavoured';
			}
			// Fierce Shot: protection roll is ill-favored
			// Splitting Blow: adversary Protection rolls are ill-favoured in close combat
			if ((hasTrait(g, charID, 'virtue', 'fierce shot') && wData[10] == 'TRUE' && ((g.combat.active && g.combat.phase == initiative.PHASE_OPENINGVOLLEY) || charEffectiveStance(g, charID) == 'rearward')) || 
				(hasTrait(g, charID, 'virtue', 'splitting blow') && ['forward','open','defensive'].includes(charEffectiveStance(g, charID)))) {
				r += '\n' + slashcommands.localizedString(locale, 'attack-protectionillfavored', []);
				if (adversary) protectionFeat = 'illfavoured';
			}
			g.encounters[g.combat.encounter].adversaries[adversary].protectionTests.push({'injury': protectionTN, 'responsible': charID, 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': null, 'poison': 0, 'hideoustoughnessroll': (encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'hideous toughness') ? 0.5 : 1)});
			filehandling.saveGameData(gameData);
			// tell the loremaster
			loremasterOutcomeAddition += slashcommands.localizedString(locale, 'attack-lm-hideoustoughness', [charName, adversary, dmg, g.encounters[g.combat.encounter].adversaries[adversary].curendurance]) + '\n';
			showLMprotTestPanel(g, g.loremaster, locale);
		} else {
			loremasterOutcomeAddition += slashcommands.localizedString(locale, 'attack-lm-zeroendurance', [charName, adversary]) + '\n';
			r += ':skull_crossbones: **' + slashcommands.localizedString(locale, 'foe-protection-fatal', []) + '**\n';
			// Cleaving: get a second attack on an engaged enemy on a kill
			if (weaponHasQuality(wData, 'cleaving')) {
				r += slashcommands.localizedString(locale, 'attack-cleaving', []) + '\n';
			}
			if (hasTrait(g, charID, 'virtue', 'in defiance of evil')) {
				for (a in g.encounters[g.combat.encounter].adversaries)	{
					encounters.encounters_adversary_damage(g, g.combat.encounter, adversary, 'curhrscore', 1);
					loremasterOutcomeAddition += slashcommands.localizedString(locale, 'attack-lm-hrloss', [charName, adversary, 1, g.encounters[g.combat.encounter].adversaries[adversary].curhrscore, g.encounters[g.combat.encounter].adversaries[adversary].hrscore]) + ' ' + emoji.progressBar(g.encounters[g.combat.encounter].adversaries[adversary].curhrscore, g.encounters[g.combat.encounter].adversaries[adversary].hrscore, 10) + '\n';
				}
			}
		}
	} else {
		loremasterOutcomeAddition += slashcommands.localizedString(locale, 'attack-lm-damaged', [charName, adversary, dmg, g.encounters[g.combat.encounter].adversaries[adversary].curendurance, g.encounters[g.combat.encounter].adversaries[adversary].endurance]) + ' ' + emoji.progressBar(g.encounters[g.combat.encounter].adversaries[adversary].curendurance, g.encounters[g.combat.encounter].adversaries[adversary].endurance, 10) +'\n';
	}
	// Biting Dart: strip 1 Hate/Resolve (3 if Bane) on any hit
	if (weaponHasQuality(wData, 'biting dart')) {
		hrloss = (bane ? 3 : 1);
	}
	// Gleams of Terror and Wrath, and In Defiance of Evil, strip hate/resolve
	if (weaponHasQuality(wData, 'gleam of terror') && hrloss < 2) hrloss += 2;
	if (hasTrait(g, charID, 'virtue', 'gleam of wrath')) hrloss += successes;
	if (hrloss != 0) {
		encounters.encounters_adversary_damage(g, g.combat.encounter, adversary, 'curhrscore', hrloss);
		loremasterOutcomeAddition += slashcommands.localizedString(locale, 'attack-lm-hrloss', [charName, adversary, hrloss, g.encounters[g.combat.encounter].adversaries[adversary].curhrscore, g.encounters[g.combat.encounter].adversaries[adversary].hrscore]) + ' ' + emoji.progressBar(g.encounters[g.combat.encounter].adversaries[adversary].curhrscore, g.encounters[g.combat.encounter].adversaries[adversary].hrscore, 10) + '\n';
		r += slashcommands.localizedString(locale, 'foe-losthate', [adversary, hrloss]) + '\n';
	}
	// Hammering: if endurance loss >= twice Attribute, foe is knocked back
	if (weaponHasQuality(wData, 'hammering') && dmg >= 2 * g.encounters[g.combat.encounter].adversaries[adversary].attribute && (g.encounters[g.combat.encounter].adversaries[adversary].knockback == 0 || !gameHasConfigFlag(g, 'adv-single-knockback'))) {
		g.encounters[g.combat.encounter].adversaries[adversary].knockback++;
		loremasterOutcomeAddition += slashcommands.localizedString(locale, 'attack-lm-knockback', [charName, adversary, g.encounters[g.combat.encounter].adversaries[adversary].knockback]) + '\n';
		r += slashcommands.localizedString(locale, 'attack-hammered', [dmg]) + '\n';
	}				
	return [loremasterOutcomeAddition, r];
}

// does a player have a usable shield?
function hasShield(g, charID, checkHandedness, checksmashable) {
	let armourStats = characterDataFetchHandleNulls(g, charID, 'sheet', 'armour');
	if (checksmashable && (hasQuality(g, charID, 'rune-scored shield') || hasQuality(g, charID, 'superior reinforced'))) return false;
	// if checking handedness and 2H, only a mithril buckler counts, but it counts even if shieldsmashed
	if (checkHandedness && g.characters[charID].handedness == 2) return (hasMithrilBuckler(g, charID) != 0);
	if (!g.characters[charID].shieldsmashed && armourStats[3] > 0) return armourStats[3];
	return false;
}

// does the player have a shield that's a buckler? assuming they only have one shield...
function hasMithrilBuckler(g, charID) {
	if (g.characters[charID].keyType == 'googlesheets') return googlesheets.characterHasMithrilBuckler(g.characters[charID].repositoryKey);
	return 0;
}

// set a poisoned status
function givePoison(g, charID, level, force, channelID) {
	if (!force && g.characters[charID].poisoned >= level) return false;
	if (level < 0) level = 0;
	if (level > 3) level = 3;
	g.characters[charID].poisoned = level;
	g.characters[charID].poisontreated = false;
	// update otherconditions to put in a descriptive-only indication
	let otherconditions = characterDataFetchHandleNulls(g, charID, 'otherconditions', null);
	let poisonDesc = slashcommands.localizedString(locale, 'condition-severity-' + level, []) + ' ' + slashcommands.localizedString(locale, 'condition-poison', []);
	if (level == 0) poisonDesc = '';
	if (otherconditions.includes('poison')) {
		let oArray = otherconditions.split(';');
		otherconditions = [];
		let found = false;
		oArray.forEach(o => {
			if (o.includes('poison') && !found) {
				found = true;
				otherconditions.push(poisonDesc);
			} else otherconditions.push(o.trim());
		});
		otherconditions = otherconditions.join('; ');
	} else {
		if (otherconditions != '') otherconditions += '; ';
		otherconditions += poisonDesc;
	}
	characterDataUpdate(g, charID, 'otherconditions', null, '=', otherconditions, null, null, channelID);
	return true;
}

// do a hate spend and warn if it's too much per third printing errata rules
function hateSpend(locale, g, adversary, amount, countAsSpend) {
	g.encounters[g.combat.encounter].adversaries[adversary].curhrscore -= amount;
	if (countAsSpend) {
		g.encounters[g.combat.encounter].adversaries[adversary].hateSpent += amount;
		if (g.encounters[g.combat.encounter].adversaries[adversary].hateSpent > g.encounters[g.combat.encounter].adversaries[adversary].might) {
			botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, slashcommands.localizedString(locale, 'foe-toomuchhate', [adversary, g.encounters[g.combat.encounter].adversaries[adversary].hateSpent, g.encounters[g.combat.encounter].adversaries[adversary].might])); 
		}
	}
}

//const rangedAttacks = ['spear','bow','knife','web'];

// TOR dice for attacks on player-heroes and other foe actions
function foeCommand(userID, channelID, userName, locale, data) {
	let charName = userName, charImage = '', charLink = '';
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let cmd = data.options[0].name;
	if (cmd == 'list') return reports.generateReport(locale, g, 'adversaries', false);

	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
	g.lastUsed = Date.now();

	// everything but roll requires an active encounter
	if (cmd != 'roll') {
		if (!g.combat.active || g.combat.encounter == '') return slashcommands.localizedString(locale, 'encounter-inactive', []) + '\n:bulb: The `/foe roll` command works without encounters or adversaries.\n';
	}

	// parse the options
	let dice = null, character = null, damage = null, injury = null, engage = null, charID = null;
	let feat = null, modifier = 0, weary = null, lmdice = true, tn = null, comment = '';
	let adversary = null, attack = null, type = null, ability = null;
	let fierce = false, additional = false, thickhide = false;
	let bonus = 0, pts = 1, hate = false, target = null;

	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'dice') dice = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'bonus') bonus = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'points') pts = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'hate') hate = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'attack') attack = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'damage') damage = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'injury') injury = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'engage') engage = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'feat') feat = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'type') type = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'modifier') modifier = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'weary') weary = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'fierce') fierce = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'additional') additional = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'thickhide') thickhide = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'tn') tn = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'comment') comment = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'ability') ability = stringutil.lowerCase(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'character') {
			target = data.options[0].options[i].value;
			if (stringutil.lowerCase(data.options[0].options[i].value) == 'none') charID = 'none';
			else if (stringutil.lowerCase(data.options[0].options[i].value) == 'back') charID = 'back';
			else charID = findCharacterInGame(g, data.options[0].options[i].value);
			if (charID == null) charID = 'none';
		}
		if (data.options[0].options[i].name == 'adversary') {
			if (data.options[0].options[i].value != 'all') {
				adversary = encounters.encounters_adversary_name_match(g, g.combat.encounter, data.options[0].options[i].value, true);
				if (adversary == null) return slashcommands.localizedString(locale, 'adversary-notexists', [data.options[0].options[i].value]);
			} else adversary = 'all';
		}
	}

	// everything but roll requires a specified adversary
	if (cmd != 'roll') {
		if (adversary == null || (adversary == 'all' && cmd != 'clear' && cmd != 'lose' && cmd != 'gain' && cmd != 'damage' && cmd != 'heal')) return slashcommands.localizedString(locale, 'adversary-notspecified', []);
	}

	// /foe engage <char>|none <adversary> [additional]
	if (cmd == 'engage') {
		if (charID == 'none' || charID == 'back') {
			g.encounters[g.combat.encounter].adversaries[adversary].engagement = [];
			g.encounters[g.combat.encounter].adversaries[adversary].standBack = (charID == 'back');
			filehandling.saveGameData(gameData);
			return slashcommands.localizedString(locale, (charID == 'back' ? 'foe-engagement-back' : 'foe-engagement-reset'), [adversary]);
		}
		if (additional) {
			g.encounters[g.combat.encounter].adversaries[adversary].engagement.push(Number(charID));
		} else {
			g.encounters[g.combat.encounter].adversaries[adversary].engagement = [Number(charID)];
		}
		filehandling.saveGameData(gameData);
		let engagedNames = [];
		g.encounters[g.combat.encounter].adversaries[adversary].engagement.forEach(e => {
			engagedNames.push(characterDataFetchHandleNulls(g, e, 'name', null));
		});
		combat_log(g, slashcommands.localizedString(locale, 'foe-engagement-set', [adversary, engagedNames.join(', ')]));
		return slashcommands.localizedString(locale, 'foe-engagement-set', [adversary, engagedNames.join(', ')]);
	}

	// /foe gain|lose|clear <adversary>|all <type>|all [<points>]
	if (cmd == 'damage' || cmd == 'heal') {
		cmd = (cmd == 'damage' ? 'lose' : 'gain');
		type = 'endurance';
	}
	if (cmd == 'gain' || cmd == 'lose' || cmd == 'clear') {
		let newVal = null;
		let typeDisplay = slashcommands.localizedSubcommandOption(locale, 'foe', 'clear', 'type', type);
		if (adversary == 'all') {
			for (let a in g.encounters[g.combat.encounter].adversaries) {
				encounters.adjust_adversary(g, a, cmd, type, pts);
			}
			filehandling.saveGameData(gameData);
			return slashcommands.localizedString(locale, 'foe-all-'+ cmd, [typeDisplay, pts]);
		} else {
			newVal = encounters.adjust_adversary(g, adversary, cmd, type, pts);
			filehandling.saveGameData(gameData);
			if (type == 'pushback') return slashcommands.localizedString(locale, 'foe-setpushback-' + (newVal ? 'yes' : 'no'), [adversary]);
			if (type == 'all') return slashcommands.localizedString(locale, 'foe-' + cmd + '-all', [adversary, pts]);
			return slashcommands.localizedString(locale, 'foe-cleargainlose', [newVal, typeDisplay, adversary]);
		}
	}

	let r = '';

	if ((cmd == 'ability' || cmd == 'roll' || cmd == 'attack' || cmd == 'protection') && g.combat.encounter != '' && adversary && encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[adversary]) && !(cmd == 'ability' && (ability == 'deathless' || ability == 'bodiless' || ability == 'spirit'))) return slashcommands.localizedString(locale, 'foe-already-dead', [adversary]) + '\n';

	if ((cmd == 'ability' || cmd == 'roll' || cmd == 'attack') && g.combat.encounter != '' && adversary && g.encounters[g.combat.encounter].adversaries[adversary].knockback > 0 && !(cmd == 'ability' && (ability == 'deathless' || ability == 'bodiless' || ability == 'spirit'))) return slashcommands.localizedString(locale, 'combat-isknockback', [adversary]) + '\n';

	// /foe ability <adversary> <ability> <char>
	if (cmd == 'ability') {
		// find a matching ability
		let fellAbility = null;
		g.encounters[g.combat.encounter].adversaries[adversary].abilities.forEach(a => {
			if (ability == stringutil.lowerCase(a.name))
				fellAbility = a;
		});
		if (!fellAbility) {
			g.encounters[g.combat.encounter].adversaries[adversary].abilities.forEach(a => {
				if (stringutil.lowerCase(a.name).startsWith(ability))
					fellAbility = a;
			});
		}
		if (!fellAbility) {
			return slashcommands.localizedString(locale, 'foe-noability', [adversary, ability]);
		}
		let theTest;
		switch (stringutil.lowerCase(fellAbility.name)) {
		case 'snake-like speed':
			if (pts < -1) pts = -1;
			g.encounters[g.combat.encounter].adversaries[adversary].snakeLike = pts;
			if (pts < 0) r = slashcommands.localizedString(locale, 'foe-ability-snakelikeoff', [adversary]);
			else r = slashcommands.localizedString(locale, 'foe-ability-snakelike', [adversary, pts]);
			break;
		case 'deathless':
		case 'spirit':
		case 'bodiless':
			// Spend 1 H/R to cancel a Wound. When an attack would reduce to 0 Endurance, spend 1 H/R to go back to full (not for Bodiless).
			// Does not work when struck by a Bane weapon for Undead, but Narvi won't worry about that.
			// verify they have something to cure
			if (g.encounters[g.combat.encounter].adversaries[adversary].wounds == 0 && (stringutil.lowerCase(fellAbility.name) == 'bodiless' || g.encounters[g.combat.encounter].adversaries[adversary].curendurance > 0))
				return slashcommands.localizedString(locale, 'foe-nothurt', [adversary, stringutil.titleCaseWords(fellAbility.name)]);
			// verify they have hate to spend
			if (g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1)
				return slashcommands.localizedString(locale, 'foe-needshate', [adversary, 1]);
			// spend it
			hateSpend(locale, g, adversary, 1, !gameHasConfigFlag(g, 'deathless-loss'));
			// cure it and choose an appropriate message
			if (g.encounters[g.combat.encounter].adversaries[adversary].wounds != 0) {
				g.encounters[g.combat.encounter].adversaries[adversary].wounds--;
				r = slashcommands.localizedString(locale, 'foe-ability-nowound', [adversary, stringutil.titleCaseWords(fellAbility.name)]);
			} else {
				g.encounters[g.combat.encounter].adversaries[adversary].curendurance = g.encounters[g.combat.encounter].adversaries[adversary].endurance;
				r = slashcommands.localizedString(locale, 'foe-ability-fullendurance', [adversary, stringutil.titleCaseWords(fellAbility.name)]);
			}
			break;
		case 'horrible strength':
			// Change the queued protection test on the target to ill-favored for 1 Hate.
			if (charID == null && g.encounters[g.combat.encounter].adversaries[adversary].engagement.length != 0) {
				charID = g.encounters[g.combat.encounter].adversaries[adversary].engagement[0];
			}
			// verify a target was specified
			if (!charID || charID == 'none') return slashcommands.localizedString(locale, 'error-notarget', []);
			// verify they have hate to spend
			if (g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1)
				return slashcommands.localizedString(locale, 'foe-needshate', [adversary, 1]);
			// find the first protection test by the specified adversary that's not already ill-favored
			let found = false;
			for (i = 0; i < g.characters[charID].protectionTests.length; i++) {
				if (!found && g.characters[charID].protectionTests[i].responsible == adversary
					&& g.characters[charID].protectionTests[i].feat != 'illfavoured') {
					g.characters[charID].protectionTests[i].feat = 'illfavoured';
					found = true;
				}
			}
			if (!found) return slashcommands.localizedString(locale, 'foe-noprottest', [characterDataFetchHandleNulls(g, charID, 'name', null)]);
			// if we got this far, the effect happened, so spend the hate
			hateSpend(locale, g, adversary, 1, true);
			r = slashcommands.localizedString(locale, 'foe-ability-horriblestrength', [characterDataFetchHandleNulls(g, charID, 'name', null)]);
			break;
		case 'foul reek':
			// Spend 1 H/R to make all combatants engaged with you -1d for the round.
			// verify we have engagement
			if (g.encounters[g.combat.encounter].adversaries[adversary].engagement.length == 0)
				return slashcommands.localizedString(locale, 'foe-needsengagement', [adversary]);
			// verify they have hate to spend, and spend it
			if (g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1)
				return slashcommands.localizedString(locale, 'foe-needshate', [adversary, 1]);
			hateSpend(locale, g, adversary, 1);
			// reduce temp bonus for all engaged foes
			let names = [];
			g.encounters[g.combat.encounter].adversaries[adversary].engagement.forEach(c => {
				g.characters[c].tempbonus--;
				names.push(characterDataFetchHandleNulls(g, c, 'name', null));
			});
			r = slashcommands.localizedString(locale, 'foe-ability-foulreek', [names.join(', ')]);
			break;
		case 'foul dust':
			// Spend 1 H/R to make all combatants in close combat stances take <points> endurance loss
			// verify they have hate to spend, and spend it
			if (g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1)
				return slashcommands.localizedString(locale, 'foe-needshate', [adversary, 1]);
			hateSpend(locale, g, adversary, 1);
			// endurance loss for all close combat enemies
			r = '';
			for (let p in g.characters) {
				if (!g.characters[p].active || charEffectiveStance(g, p) == 'rearward') continue;
				let [dmg, r2] = enduranceLossDamage(fellAbility.amount ? fellAbility.amount : 1, 'poison', g, p, locale, channelID);
				r += r2 + '\n';
			}
			break;
		case 'frostbreath':
			// Spend 1 H/R to make a particular combatant take cold endurance loss
			// verify a target is specified
			if (charID == null && g.encounters[g.combat.encounter].adversaries[adversary].engagement.length != 0) {
				charID = g.encounters[g.combat.encounter].adversaries[adversary].engagement[0];
			}
			if (!charID || charID == 'none') return slashcommands.localizedString(locale, 'error-notarget', []);
			// verify they have hate to spend, and spend it
			if (g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1)
				return slashcommands.localizedString(locale, 'foe-needshate', [adversary, 1]);
			hateSpend(locale, g, adversary, 1);
			// endurance loss for all close combat enemies
			r = '';
			let [dmg, r2] = enduranceLossDamage(fellAbility.amount ? fellAbility.amount : 2, 'cold', g, charID, locale, channelID);
			r += r2 + '\n';
			break;
		case 'drums in the deep':
		case 'yell of alarm':
			// Spend 1 H/R to increase eye awareness by <points>
			// verify they have hate to spend, and spend it
			if (g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1)
				return slashcommands.localizedString(locale, 'foe-needshate', [adversary, 1]);
			hateSpend(locale, g, adversary, 1);
			r += increaseEyeRating(g, fellAbility.amount ? fellAbility.amount : 1);
			break;
		case 'dreadful spells': // queues a sorcery shadow test
		case 'keening wail':
			// verify a target is specified
			if (charID == null && g.encounters[g.combat.encounter].adversaries[adversary].engagement.length != 0) {
				charID = g.encounters[g.combat.encounter].adversaries[adversary].engagement[0];
			}
			if (!charID || charID == 'none') return slashcommands.localizedString(locale, 'error-notarget', []);
			// verify they have hate to spend, and spend it
			if (g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1)
				return slashcommands.localizedString(locale, 'foe-needshate', [adversary, 1]);
			hateSpend(locale, g, adversary, 1, true);
			// build and queue the test
			theTest = {'type': 'sorcery', 'tn': null, 'responsible': adversary, 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': fellAbility.type, 'points': (fellAbility.amount ? fellAbility.amount : (stringutil.lowerCase(fellAbility.type).includes('poison') ? 2 : 1))};
			pushTest(g, charID, 'sorcery', theTest);
			r = slashcommands.localizedString(locale, 'foe-ability-sorcery', [characterDataFetchHandleNulls(g, charID, 'name', null)]);
			break;
		case 'freeze the blood': // all heroes in sight must make a 2 point Shadow Dread test; those who fail are Dismayed (all rolls ill-favoured).
		case 'strike fear': // N point Shadow Dread test on all player-heroes in sight. Those who fail are also Daunted.
		case 'thing of terror': // all heroes in sight must make a 3 point Shadow Dread test; those who fail are Daunted.
			// figure out how many points it will be (with defaults if not encoded in the ability)
			let points = fellAbility.amount;
			if (points == null || points == 0) {
				if (stringutil.lowerCase(fellAbility.name) == 'freeze the blood') points = 2;
				else if (stringutil.lowerCase(fellAbility.name) == 'thing of terror') points = 3;
				else points = 1;
			}
			// figure out the condition it brings (with defaults if not encoded in the ability)
			let condition = fellAbility.type;
			if (condition == null || condition == '') {
				if (stringutil.lowerCase(fellAbility.name) == 'freeze the blood') condition = 'dismayed';
				else condition = 'daunted';
			}
			// build the test
			theTest = {'type': 'dread', 'tn': null, 'responsible': adversary, 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': condition, 'points': points};
			// for every active character, push this test
			for (let p in g.characters) {
				if (!g.characters[p].active) continue;
				pushTest(g, p, 'dread', theTest);
				r += slashcommands.localizedString(locale, 'foe-ability-dread', [characterDataFetchHandleNulls(g, p, 'name', null)]) + '\n';
			}
			break;
		case 'yell of triumph':
		case 'howl of triumph':
			// verify they have hate to spend, and spend it
			if (g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1)
				return slashcommands.localizedString(locale, 'foe-needshate', [adversary, 1]);
			let matches = null, anyMatched = false;
			if (target != null && target != '') {
				target = target.trim().replace(/,/g,' ');
				target = target.replace(/  +/g, ' ');
				matches = stringutil.lowerCase(target).split(' ');
			}
			for (let a in g.encounters[g.combat.encounter].adversaries) {
				if (a == adversary || encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[a])) continue;
				matched = true;
				if (matches != null) {
					// see if the name matches one of them
					matched = false;
					matches.forEach(m => {
						if (stringutil.lowerCase(a).startsWith(m)) matched = true;
					});
				}
				if (!matched) continue;
				anyMatched = true;
				g.encounters[g.combat.encounter].adversaries[a].curhrscore++;
				if (g.encounters[g.combat.encounter].adversaries[a].curhrscore >= g.encounters[g.combat.encounter].adversaries[a].hrscore) 
					g.encounters[g.combat.encounter].adversaries[a].curhrscore = g.encounters[g.combat.encounter].adversaries[a].hrscore;
				else botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, slashcommands.localizedString(locale, 'foe-ability-givehate', [adversary, a, g.encounters[g.combat.encounter].adversaries[a].curhrscore, stringutil.titleCaseWords(fellAbility.name)])); 
			}
			// if no matches, error
			if (!anyMatched) return slashcommands.localizedString(locale, 'foe-ability-triumphfail', [adversary, stringutil.titleCaseWords(fellAbility.name)]);
			// spend the hate
			hateSpend(locale, g, adversary, 1, true);
			r = slashcommands.localizedString(locale, 'foe-ability-triumph', [adversary, stringutil.titleCaseWords(fellAbility.name)]);
			break;
		default:
			return slashcommands.localizedString(locale, 'foe-ability-notknown', [stringutil.titleCaseWords(fellAbility.name)]);
			break;
		}
		combat_log(g, stringutil.stringWithoutNewlines(r));
		filehandling.saveGameData(gameData);
		return r;
	}

	// roll, attack, and protection are all woven together here since they all come together into a roll
	// /foe protection <adversary> [thickhide] [<rolloptions>] -- interwoven with roll
	// /foe attack <char> <adversary> [<attack>] [fierce] [<rolloptions>] -- interwoven with roll

	// verify a target when one is required
	if (cmd == 'roll' || cmd == 'attack') {

		// if foe is engaged but no character specified, use the existing engagement
		if (charID == null && adversary && g.encounters[g.combat.encounter].adversaries[adversary].engagement.length != 0) {
			let action = g.encounters[g.combat.encounter].adversaries[adversary].actionsThisRound;
			if (action > g.encounters[g.combat.encounter].adversaries[adversary].engagement.length) action = 0;
			charID = g.encounters[g.combat.encounter].adversaries[adversary].engagement[action];
		}
		// if no charID yet, bug out, we can't proceed
		if (!charID || charID == 'none') return slashcommands.localizedString(locale, 'error-notarget', []);
	}

	// get the relevant info for adorning the adversary roll
	if (cmd == 'roll') {
		[charName, charImage, charLink] = getRollCharacterInfo(g, charID, userID);
	} else {
		charName = g.encounters[g.combat.encounter].adversaries[adversary].name;
		charImage = g.encounters[g.combat.encounter].adversaries[adversary].image;
		charLink = '';
	}

	if (cmd == 'roll' && engage != null) {
		// not doing a real roll, just setting old-style engagement count
		// prevent this when there's an encounter active
		if (g.combat.active && g.combat.encounter != '') return slashcommands.localizedString(locale, 'foe-engagement-old', []);
		g.characters[charID].numEngaged += engage;
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'foe-engaged', [charName, g.characters[charID].numEngaged]);
	}

	// determine the favored, bonus, tn based on the kind of test
	let debugString = '', protTest = null, protResponsible = slashcommands.localizedString(locale, 'none', []);
	let featToRoll = 1, successToRoll = dice, mods = 0, targetTN = 0;
	let startingHR = 99;
	if (successToRoll != null) debugString += ':anger: Success dice set explicitly to ' + successToRoll + '\n';
	if (g.combat.encounter && adversary && g.encounters[g.combat.encounter].adversaries[adversary]) startingHR = g.encounters[g.combat.encounter].adversaries[adversary].curhrscore;
	if (cmd == 'protection') {
		// for protection, look up armor for dice, tn from queued effect, account for thickhide
		if (successToRoll == null) {
			successToRoll = g.encounters[g.combat.encounter].adversaries[adversary].armour;
			debugString += ':anger: Adversary armor set success dice to ' + successToRoll + '\n';
		}
		if (tn == null && g.encounters[g.combat.encounter].adversaries[adversary].protectionTests.length == 0) {
			// no queued tests and no TN specified
			return slashcommands.localizedString(locale, 'test-nonequeued', [adversary]);
		}
		if (g.encounters[g.combat.encounter].adversaries[adversary].protectionTests.length != 0) {
			// pop the first test out of the queue and save it
			protTest = g.encounters[g.combat.encounter].adversaries[adversary].protectionTests.shift();
			filehandling.saveGameData(gameData);
			if (protTest.responsible && protTest.responsible == 'loremaster') {
				protResponsible = slashcommands.localizedString(locale, 'loremaster');
			} else if (protTest.responsible && !isNaN(protTest.responsible)) {
				protResponsible = characterDataFetchHandleNulls(g, protTest.responsible, 'name', null);
			}
			if (tn == null) {
				targetTN = protTest.injury;
				debugString += ':anger: Queued protection test set TN to ' + targetTN + '\n';
			}
			if (feat == null && protTest.feat != null) {
				feat = protTest.feat;
				debugString += ':anger: Queued protection test set feat to ' + feat + '\n';
			}
			if (protTest.bonus != 0) {
				bonus += protTest.bonus;
				debugString += ':anger: Queued protection test increased bonus dice to ' + bonus + '\n';
			}
			if (Number(protTest.modifier) != 0) {
				modifier += Number(protTest.modifier);
				debugString += ':anger: Queued protection test increased modifier to ' + modifier + '\n';
			}
		}
		if (thickhide && g.encounters[g.combat.encounter].adversaries[adversary].curhrscore > 0) {
			// verify the thick hide ability
			if (!encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'thick hide') && !encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'thick armour')) {
				comment += '\n' + slashcommands.localizedString(locale, 'foe-nothickhide', [adversary]);
			} else {
				bonus += 2;
				hateSpend(locale, g, adversary, 1, true);
				debugString += ':anger: Thick Hide increased bonus dice to ' + bonus + '\n';
			}
		}
	}
	let ranged = false, attackEntry = null;
	let followup = [];
	if (cmd == 'attack') {
		if (g.encounters[g.combat.encounter].adversaries[adversary].attacks.length == 0) {
			return slashcommands.localizedString(locale, 'foe-noattacks', [adversary]);
		}
		if (g.encounters[g.combat.encounter].adversaries[adversary].knockback > 0) {
			return slashcommands.localizedString(locale, 'combat-isknockback', [adversary]);
		}
		// if no attack specified, take the first one, or the first ranged one if opening volley
		if (attack == null) {
			if (g.combat.active && g.combat.phase == initiative.PHASE_OPENINGVOLLEY) {
				g.encounters[g.combat.encounter].adversaries[adversary].attacks.forEach(att => {
					if (attack == null && att.ranged) attack = att.name; // take the first ranged attack we find if any
					//rangedAttacks.forEach(w => {
					//	if (att.name.toLowerCase().includes(w)) attack = att.name;
					//});
				});
			}
			if (attack == null) attack = g.encounters[g.combat.encounter].adversaries[adversary].attacks[0].name;
		}
		// find the attack
		attack = stringutil.lowerCase(attack);
		g.encounters[g.combat.encounter].adversaries[adversary].attacks.forEach(a => {
			if (stringutil.lowerCase(a.name) == attack) {
				attackEntry = a;
			}
		});
		if (!attackEntry) { // try a substring match
			g.encounters[g.combat.encounter].adversaries[adversary].attacks.forEach(a => {
				if (!attackEntry && stringutil.lowerCase(a.name).includes(attack)) {
					attackEntry = a;
					attack = stringutil.lowerCase(a.name);
				}
			});
		}
		if (!attackEntry) {
			return slashcommands.localizedString(locale, 'foe-attacknotfound', [adversary, attack]);
		}
		// for attack, look up successToRoll, damage, injury based on adversary attack
		successToRoll = attackEntry.dice;
		debugString += ':anger: Attack lookup set success dice to ' + successToRoll + '\n';
		if (!damage) damage = attackEntry.damage;
		if (!injury) injury = attackEntry.injury;

		// any complications or modifiers affecting adversaries?
		g.combatMods.forEach(c => {
			if (combatModActive(c, 0)) {
				successToRoll += c.modifier;
				debugString += ':anger: ' + (c.modifier > 0 ? 'Advantage raised' : 'Complication lowered') + ' success bonus dice by ' + String(c.modifier) + ' to ' + successToRoll + '\n';
			}
		});

		// determine if ranged
		if (attack.ranged) ranged = true;

		// record engagement new-style
		if (g.characters[charID].stance != 'rearward' && !g.encounters[g.combat.encounter].adversaries[adversary].standBack && !g.encounters[g.combat.encounter].adversaries[adversary].engagement.includes(charID)) {
			if (g.encounters[g.combat.encounter].adversaries[adversary].engagement.length >= initiative.adversaryCombatActions(g, adversary)) {
				// remove the first engagement to replace it, but that'll be at the end, to cycle
				g.encounters[g.combat.encounter].adversaries[adversary].engagement.shift();
			}
			g.encounters[g.combat.encounter].adversaries[adversary].engagement.push(charID);
		}
		// note the action for initiative but account for multiple attacks for Might > 1
		let s = initiative.takeAdversaryAction(g, locale, adversary);
		if (s != '') followup.push(s);
	}
	if (cmd == 'roll') {
		// record engagement old-style
		g.characters[charID].numEngaged++;
	}
	if (cmd == 'roll' || cmd == 'attack') {
		targetTN = characterDataFetchHandleNumbers(g, charID, 'parry',null);
	}

	// handle options common to different kinds of rolls
	[featToRoll, debugString] = torRollHandleFeat(feat, featToRoll, debugString, false);
	[successToRoll, mods, targetTN, debugString] = torRollHandleBonuses(bonus, true, modifier, tn, targetTN, g, charID, successToRoll, mods, debugString);
	if (hate && g.combat.encounter && g.encounters[g.combat.encounter].adversaries && adversary && g.encounters[g.combat.encounter].adversaries[adversary].curhrscore > 0) {
		successToRoll++;
		hateSpend(locale, g, adversary, 1, true);
		debugString += ':anger: Hate spend increased success dice to ' + successToRoll + '\n';
	}
	if (fierce && g.encounters[g.combat.encounter].adversaries[adversary].curhrscore > 0) {
		if (!encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'fierce') || g.encounters[g.combat.encounter].adversaries[adversary].curhrscore < 1) {
			comment += '\n' + slashcommands.localizedString(locale, 'foe-nofierce', [adversary]);
		} else {
			successToRoll++;
			featToRoll = 2;
			hateSpend(locale, g, adversary, 1, true);
			debugString += ':anger: Fierce increased bonus dice to ' + successToRoll + ' and made attack roll favored\n';
		}
	}
	if (cmd == 'attack' && encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'denizen of the dark') && g.combat.light == 'darkness') {
		featToRoll = 2;
		debugString += ':anger: Denizen of the Dark made attack roll favored\n';
	}

	// attack modifiers
	if (cmd == 'roll' || cmd == 'attack') {
		if (g.combat.active && g.combat.phase != initiative.PHASE_OPENINGVOLLEY && !ranged) {
			if (charEffectiveStance(g, charID) == 'forward') {
				successToRoll++;
				debugString += ':anger: Forward stance of target increased dice to ' + successToRoll + '\n';
			}
			if (charEffectiveStance(g, charID) == 'defensive') {
				successToRoll--;
				debugString += ':anger: Defensive stance of target decreased dice to ' + successToRoll + '\n';
			}
			// skirmish stance -1d if not ranged
			if (charEffectiveStance(g, charID) == 'skirmish' && !ranged) {
				successToRoll--;
				debugString += ':anger: Skirmish stance of target decreased dice to ' + successToRoll + '\n';
			}
		}
		let shield = hasShield(g, charID, true, false);
		if (shield) {
			targetTN += shield;
			debugString += ':anger: Shield increased TN to ' + targetTN + '\n';
		}
		if (g.characters[charID].tempparry > 0) {
			successToRoll -= g.characters[charID].tempparry;
			debugString += ':anger: Temporary parry decreased dice to ' + successToRoll + '\n';
			g.characters[charID].tempparry = 0;
		}
		if (g.characters[charID].fendParry > 0) {
			targetTN += g.characters[charID].fendParry;
			debugString += ':anger: Fend parry increased TN to ' + targetTN + '\n';
			tn += g.characters[charID].fendParry;
		}
		if (cmd == 'attack') {
			if (g.encounters[g.combat.encounter].adversaries[adversary].pushback > 0) {
				successToRoll--;
				debugString += ':anger: Pushback decreased dice to ' + successToRoll + '\n';
			}
			if (g.combat.advsurprise) {
				successToRoll--;
				debugString += ':anger: Ambush decreased dice to ' + successToRoll + '\n';
			}
			// small folk
			if (hasTrait(g, charID, 'virtue', 'small folk') && !['small','tiny'].includes(stringutil.lowerCase(g.encounters[g.combat.encounter].adversaries[adversary].size))) {
				targetTN += 2;
				debugString += ':anger: Small Folk increased TN to ' + targetTN + '\n';
			}
		}
	}

	// handle weary
	let miserable;
	[weary, miserable, debugString] = torRollHandleWearyMiserable(g, null, weary, null, debugString, channelID, 'foe');
	// other causes for weariness
	if (cmd == 'attack' || cmd == 'protection') {

		if ((!gameHasConfigFlag(g, 'adv-weary-spend') && g.encounters[g.combat.encounter].adversaries[adversary].curhrscore <= 0) ||
		    (gameHasConfigFlag(g, 'adv-weary-spend') && startingHR <= 0)) {
			debugString += ':anger: Adversary drained hate/resolve made the roll be made as if weary\n';
			weary = true;
		}
		if (cmd == 'attack' && g.encounters[g.combat.encounter].adversaries[adversary].weary > 0) {
			debugString += ':anger: Adversary weary condition made the roll be made as if weary\n';
			// no longer applicable; lasts the whole round per errata 1/15/2024
			// g.encounters[g.combat.encounter].adversaries[adversary].weary--;
			weary = true;
		}
	}
	if (cmd == 'roll' || cmd == 'attack') {
		// Rune-Scored Shield: make the attack be weary
		if (hasQuality(g, charID, 'rune-scored shield')) {
			weary = true;
			debugString += ':anger: Rune-Scored Shield made the attack be made as if weary\n';
		}
	}

	// do the roll
	let rollResults, rollLine = '**';
	// use the charName here for protection and attack, and 'foe' for roll
	if (cmd == 'roll') rollLine += stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'foe'));
	else rollLine += charName;
	rollLine += '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' ';
	if (cmd == 'protection') {
		rollLine += stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'foe', 'protection'));
		if (protResponsible != slashcommands.localizedString(locale, 'none', [])) rollLine += ' (' + protResponsible + '): ';
	} else {
		rollLine += stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'foe', 'attack')) + ' (' + characterDataFetchHandleNulls(g, charID, 'name', null);
		if (cmd == 'attack' && attackEntry) rollLine += ' ' + slashcommands.localizedString(locale, 'roll-using', []) + ' ' + attackEntry.name;
		rollLine += '): ';
	}
	rollResults = torRollDice(featToRoll, successToRoll, 0, mods, weary, miserable, false, lmdice, false, locale, g.dicestyle, gameHasConfigFlag(g, 'magical-tengwar'));
	rollLine += rollResults[3];
	let successes = 0;
	if (rollResults[1] >= 0 && targetTN && (rollResults[0] >= targetTN || rollResults[1] == 1)) successes = rollResults[2] + 1;

	// if there's a target, put in a success or failure
	if (targetTN && targetTN != 0) r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', [])) + '__**: ' + compareToTarget(rollResults, targetTN, locale);

	r += '\n**__';
	if (cmd == 'protection') r += stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'foe', 'protection'));
	else r += stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'foe', 'attack'));
	r += '__**: ';
	let loremasterOutcome = '';

	// if it was a hit
	let theButtons = [];
	if (successes) {
		// a hit, a very palpable hit!
		if (cmd == 'protection') {
			r += slashcommands.localizedString(locale, 'foe-protection-passed', [protResponsible]);
			if (protTest && protTest.hideoustoughnessroll) {
				// hideoustoughnessroll consequences on passing
				encounters.adjust_adversary(g, adversary, (protTest.hideoustoughnessroll == 0.5 ? 'half' : 'clear'), 'endurance', 0);
				r += '\n' + slashcommands.localizedString(locale, (protTest.hideoustoughnessroll == 0.5 ? 'foe-protection-hideous' : 'foe-protection-bodiless'), []);
				loremasterOutcome += slashcommands.localizedString(locale, 'foe-lm-hideoustoughness', [adversary, g.encounters[g.combat.encounter].adversaries[adversary].curendurance]) + '\n';
			}
		} else {
			let tengwar = Number(rollResults[2]);
			let showButtons = true;
			if (!attackEntry || !adversary) showButtons = false;
			if (damage && showButtons && tengwar > 0 && (attackEntry.special == null || attackEntry.special == '' || (attackEntry.special == 'breakshield' && hasShield(g, charID, false, false) === false) || (attackEntry.special.startsWith('fieryblow') && (charEffectiveStance(g, charID) == 'rearward' || charEffectiveStance(g, charID) == 'skirmish')) || (attackEntry.special == 'pierce' && Number(rollResults[4]) != 0 && (Number(rollResults[4]) + tengwar * 2 < 10 || Number(rollResults[4]) >= 10)))) {
				// if there is no valid and feasible special action, just increase the damage accordingly
				showButtons = false;
				damage += tengwar * g.encounters[g.combat.encounter].adversaries[adversary].attribute;
			}
			if (damage) {
				r += slashcommands.localizedString(locale, 'attack-hitdmg', [String(damage)]);
				r += '\n' + tokenCommandExecute('damage', channelID, locale, g, charID, 'endurance', '-', damage, false);
			} else r += slashcommands.localizedString(locale, 'attack-hit', []);
			if (tengwar > 0) {
				if (showButtons && adversary) {
					// build buttons for special actions -- always Heavy, sometimes other things too
					theButtons.push(embeds.buildButton(locale, 'heavy_' + String(charID), 4, 'spend-h', '(+' + String(g.encounters[g.combat.encounter].adversaries[adversary].attribute) + ')', SPEND_HEAVY, getGameName(g), [userID]));
					// plus optionally Break Shield (only show if target has a shield?), Pierce (+2), or Seize (always show)
					if (attackEntry.special == 'breakshield' && hasShield(g, charID, false, true) !== false) {
						theButtons.push(embeds.buildButton(locale, 'breakshield_' + String(charID), 4, 'spend-bs', '', SPEND_SHIELD, getGameName(g), [userID]));
					}
					if (attackEntry.special.startsWith('fieryblow') && charEffectiveStance(g, charID) != 'rearward' && charEffectiveStance(g, charID) != 'skirmish') {
						theButtons.push(embeds.buildButton(locale, attackEntry.special + '_' + String(charID), 4, 'spend-fb', '(' + slashcommands.localizedString(locale, 'condition-severity-' + attackEntry.special.slice(-1), []) + ')', SPEND_FIRE, getGameName(g), [userID]));
					}
					if (attackEntry.special == 'pierce' && injury && injury > 0) {
						theButtons.push(embeds.buildButton(locale, 'pierce_' + String(charID), 4, 'spend-p', '(+2)', SPEND_PIERCE, getGameName(g), [userID]));
					}
					if (attackEntry.special == 'seize') {
						theButtons.push(embeds.buildButton(locale, 'seize_' + String(charID), 4, 'spend-sz', '', SPEND_SEIZE, getGameName(g), [userID]));
					}
					if (attackEntry.special == 'bitten') {
						theButtons.push(embeds.buildButton(locale, 'seize_' + String(charID), 4, 'spend-bt', '', SPEND_SEIZE, getGameName(g), [userID]));
					}
					if (attackEntry.special == 'drain') {
						theButtons.push(embeds.buildButton(locale, 'drain_' + String(charID), 4, 'spend-dr', '', SPEND_SEIZE, getGameName(g), [userID]));
					}
					// store the tengwar count for the sake of counting down later
					g.encounters[g.combat.encounter].adversaries[adversary].tengwar[charID] = {
						'total': tengwar, 'current': tengwar, 'injury': injury, 'roll': Number(rollResults[4])
					};
				}
			}
			if (Number(rollResults[4]) >= 10 && injury && injury > 0) r += adversaryPiercingBlow(g, adversary, charID, injury, tengwar, locale, false);
		}
	} else {
		// a miss!
		if (cmd == 'protection') {
			// handle protection results, including consequences
			r += slashcommands.localizedString(locale, 'foe-protection-failed', [protResponsible]);
			// gain a wound
			g.encounters[g.combat.encounter].adversaries[adversary].wounds++;
			loremasterOutcome += slashcommands.localizedString(locale, 'foe-lm-gainwound', [adversary, g.encounters[g.combat.encounter].adversaries[adversary].wounds]) + '\n';
			if (g.encounters[g.combat.encounter].adversaries[adversary].wounds >=
				g.encounters[g.combat.encounter].adversaries[adversary].might) {
				r += '\n**' + slashcommands.localizedString(locale, 'foe-protection-fatal', []) + '** :skull_crossbones:';
				loremasterOutcome += slashcommands.localizedString(locale, 'foe-lm-died', [adversary]) + '\n';
				if (protTest && protTest.responsible && !isNaN(protTest.responsible)) {
					let wData = characterDataFetch(g, protTest.responsible, 'weapons', g.characters[protTest.responsible].curWeapon);
					if (wData && weaponHasQuality(wData, 'cleaving')) {
						r += '\n' + slashcommands.localizedString(locale, 'attack-cleaving', []);
					}
				}
			} else if (protTest && protTest.hideoustoughnessroll) {
				// hideoustoughnessroll consequences if survived
				encounters.adjust_adversary(g, adversary, (protTest.hideoustoughnessroll == 0.5 ? 'half' : 'clear'), 'endurance', 0);
				r += '\n' + slashcommands.localizedString(locale, (protTest.hideoustoughnessroll == 0.5 ? 'foe-protection-hideous' : 'foe-protection-bodiless'), []);
				loremasterOutcome += slashcommands.localizedString(locale, 'foe-lm-hideoustoughness', [adversary, g.encounters[g.combat.encounter].adversaries[adversary].curendurance]) + '\n';
			}
			// the only test condition applicable to adversaries is knockback (and even that's not RAW)
			if (protTest && protTest.condition == 'knockback') {
				g.encounters[g.combat.encounter].adversaries[adversary].knockback += (protTest.points ? Number(protTest.points) : 1);
				loremasterOutcome += slashcommands.localizedString(locale, 'foe-lm-gainknockback', [adversary, g.encounters[g.combat.encounter].adversaries[adversary].knockback]) + '\n';
			}
		} else r += slashcommands.localizedString(locale, 'attack-missed', []);

	}
	filehandling.saveGameData(gameData);

	// add any comment
	if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []))  + '__**: ' + comment;

	if (debugString != '') g.debugString = debugString;
	if (gameHasConfigFlag(g, 'always-explain')) followup.push('**Explanation**:\n' + g.debugString);
	if (loremasterOutcome != '') botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, loremasterOutcome);

	combat_log(g, rollLine + ': ' + r);

	let theEmbed = embeds.buildEmbed(
		charName + (adversary ? ' (' + adversary + ')' : ''),
		charImage,
		charLink,
		rollLine,
		r + '\n',
		'',
		bot, null,
		getUserConfig(userID,'selectedGame'),
		embeds.BUTTON_RED,
		userID
		);

	let resultObject = {embed: theEmbed};
	if (theButtons.length > 0) {
		resultObject.components = [];
		if (theButtons.length > 0) resultObject.components.push({"type": 1,"components": [...theButtons]});
	}
	return [rollResults[5], resultObject, ...followup];
}

// handles just the rolling of the dice, assuming someone else figured out how many, and someone else will deal with the result
function torRollDice(feat, success, mastery, modifier, weary, miserable, magical, lmdice, lifepathfavored, locale, dicestyle, magicalTengwar) {
	// return [0] = total of roll
	// return [1] = 0 for a regular result, 1 for an auto success (gandalf), -1 for an auto failure (eye)
	// return [2] = number of tengwars in the result
	// return [3] = string of the resulting roll
	// return [4] = feat die in the resulting roll
	// return [5] = string of just the dice in the resulting roll

	let rollResults = [0, 0, 0, '', 0, ''];
	rollResults[0] = modifier;
	if (lmdice) lifepathfavored = false; // lmdice roll overrides lifepath, enemies never have lifepaths

	if (success > 20) success = 20;

	// first do the feat roll language
	if (feat != 0) {
		let finalFeatRoll;
		let finalFeatRollVal = 0;
		if (feat < 0) finalFeatRollVal = 13;
		if (magical) {
			finalFeatRoll = lmdice ? emoji.EYE : emoji.GANDALF;
			finalFeatRollVal = featRollValue(finalFeatRoll, lmdice);
		} else if (feat == -99)	{ // auto-failure
			finalFeatRoll = lmdice ? emoji.GANDALF : emoji.EYE;
			finalFeatRollVal = featRollValue(finalFeatRoll, lmdice);
		} else {
			if (feat != 1 && feat != -1) {
				finalFeatRoll = 0;
				if (dicestyle == 'full') rollResults[5] += emoji.emojiPunctuation('(');
				for (let i = 0; i < feat * (feat < 0 ? -1 : 1); i++) {
					let thisFeatRoll = featRoll();
					if (lifepathfavored && thisFeatRoll == 1) thisFeatRoll = 11;
					let thisFeatRollVal = featRollValue(thisFeatRoll, lmdice);
					if ((feat > 0 && thisFeatRollVal > finalFeatRollVal) || (feat < 0 && thisFeatRollVal < finalFeatRollVal)) {
						finalFeatRoll = thisFeatRoll;
						finalFeatRollVal = thisFeatRollVal;
					}
					if (dicestyle != 'concise') rollResults[5] += featRollText(thisFeatRoll, lmdice);
				}
				if (dicestyle == 'full') rollResults[5] += emoji.emojiPunctuation(')') + ' ';
			} else {
				finalFeatRoll = featRoll();
				if (lifepathfavored && finalFeatRoll == 1) finalFeatRoll = 11;
				finalFeatRollVal = featRollValue(finalFeatRoll, lmdice);
			}
		}
		if (dicestyle == 'full' || dicestyle == 'concise' || feat == 1) rollResults[5] += featRollText(finalFeatRoll, lmdice) + ' ';
		rollResults[4] = finalFeatRoll;

		if (finalFeatRoll == emoji.EYE && lmdice) {
			rollResults[1] = 1; // indicate auto success
			rollResults[0] += 10;
			rollResults[4] = 12;
		} else if (feat == -99 || (finalFeatRoll == emoji.EYE && !lmdice)) {
			if (miserable) rollResults[1] = -1; // indicate auto failure
			//rollResults[0] += 0;
			rollResults[4] = 0;
		} else if (finalFeatRoll == emoji.GANDALF && lmdice) {
			//rollResults[1] = -1; // indicate auto failure -- nope, it's just a zero
			//rollResults[0] += 0;
			rollResults[4] = 0;
		} else if (finalFeatRoll == emoji.GANDALF && !lmdice) {
			rollResults[1] = 1; // indicate auto success
			rollResults[0] += 10;
			rollResults[4] = 12;
		} else rollResults[0] += finalFeatRoll;
	}

	// now it's time for success dice
	let i, countRolls;
	countRolls = mastery + success;
	let successRolls = [];
	for (i=0; i<countRolls; i++) {
		successRolls[i] = regdice.simpleRoll(6);
	}

	if (mastery || dicestyle == 'sorted') successRolls.sort().reverse();
	for (i=0; i<success; i++) {
		rollResults[5] += successRollText(successRolls[i], weary, lmdice) + ' ';
		if (successRolls[i] == 6) rollResults[2]++;
		if (!weary || successRolls[i] >= 4) rollResults[0] += successRolls[i];
	}
	if (mastery) {
		if (dicestyle == 'full') rollResults[5] += ' '+ emoji.emojiPunctuation('(');
		for (; i<mastery+success; i++) {
			if (dicestyle == 'full' || dicestyle == 'rolled' || dicestyle == 'sorted') rollResults[5] += successRollText(successRolls[i], weary, lmdice) + ' ';
		}
		if (dicestyle == 'full') rollResults[5] += emoji.emojiPunctuation(')') + ' ';
	}

	rollResults[3] = rollResults[5] + '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-total', [])) + '__: ' + rollResults[0] + '**';
	if (magicalTengwar && magical) rollResults[2]++;
	for (i=0; i<rollResults[2]; i++) rollResults[3] += emoji.customEmoji(emoji.TENGWAR,0,0, lmdice);

	return rollResults;
}

// look up a particular skill and return the factors needed to roll on it
function torRollLookupSkill(g, charID, skill, noitem, locale) {
	let skillVal = characterDataFetchHandleNulls(g, charID, 'sheet', skill);
	let outputAddition = '';

	let successToRoll = Number(skillValue(skillVal));
	let debugStringAddition = ':anger: Skill lookup ' + stringutil.makeSmallCaps(stringutil.titleCaseWords(skill)) + ' raised success dice to ' + successToRoll + '\n';

	// handle factors that give a bonus die
	if (hasTrait(g, charID, 'virtue', 'herbal remedies') && skill == 'healing') {
		successToRoll++;
		debugStringAddition += ':anger: Success dice raised to ' + successToRoll + ' due to Herbal Remedies virtue\n';
	}
	if (!noitem) {
		let item = getUsefulItemFromExternalCache(g.characters[charID].keyType, g.characters[charID].repositoryKey, skill);
		if (item != '') {
			successToRoll++;
			debugStringAddition += ':anger: Success dice raised to ' + successToRoll + ' due to useful item ' + item + '\n';
			outputAddition += '(' + slashcommands.localizedString(locale, 'roll-using', []) + ' ' + item + ')\n';
		}
	}
	item = getBlessingFromExternalCache(g.characters[charID].keyType, g.characters[charID].repositoryKey, skill);
	if (item != '') {
		successToRoll += 2;
		debugStringAddition += ':anger: Success dice raised to ' + successToRoll + ' due to blessing on ' + item + '\n';
		outputAddition += '(' + slashcommands.localizedString(locale, 'roll-using', []) + ' ' + item + ')\n';
	}

	// figure out feat roll
	let featToRoll = 1;
	if (skillFavored(skillVal)) {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to ' + stringutil.makeSmallCaps(stringutil.titleCaseWords(skill)) + ' being favored\n';
	}

	// figure out the skilltn
	let targetTN = charSkillTN(g, charID, skill);

	return [featToRoll, successToRoll, 0, targetTN, debugStringAddition, outputAddition];
}

// look up a protection roll and return the factors needed to roll on it
function torRollLookupProtection(g, charID, locale, targetTN) {

	let armourStats = characterDataFetchHandleNulls(g, charID, 'sheet', 'armour');
	let skillVal = armourStats[0];
	if (!g.characters[charID].shieldsmashed) skillVal += armourStats[2];

	let successToRoll = skillValue(skillVal);
	let debugStringAddition = ':anger: Armour lookup raised success dice to ' + successToRoll + '\n';

	// handle factors that give a bonus die
	if (hasTrait(g, charID, 'virtue', 'skin-coat') && characterDataFetchHandleNumbers(g, key, 'sheet', 'armour') <= 2) {
		successToRoll++;
		debugStringAddition += ':anger: Success dice raised to ' + successToRoll + ' due to Skin-coat virtue\n';
	}

	if (armourStats[1] > 0) {
		debugStringAddition += ':anger: Protection bonus from gear set to ' + armourStats[1] + '\n';
	}

	// figure out feat roll
	let featToRoll = 1;
	if (hasCulturalBlessing(g, charID, 'furious') && characterDataFetchHandleNulls(g, charID, 'wounded', null) == 'TRUE') {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to Furious cultural blessing\n';
	} else if (hasTrait(g, charID, 'virtue', 'stone-hard')) {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to Stone-hard virtue\n';
	}

	return [featToRoll, successToRoll, armourStats[1], targetTN, debugStringAddition, ''];
}

// look up a particular shadow test and return the factors needed to roll on it
function torRollLookupShadowTest(g, charID, test, locale) {

	let attribute = 'wisdom';
	if (test == 'dread') attribute = 'valour';

	let successToRoll = characterDataFetchHandleNumbers(g, charID, 'sheet', attribute);
	let debugStringAddition = ':anger: Attribute lookup for ' + stringutil.titleCaseWords(test) + ' (' + stringutil.titleCaseWords(attribute) + ') raised success dice to ' + successToRoll + '\n';

	// handle factors that give a bonus die
	if (hasCulturalBlessing(g, charID, 'hobbit-sense') && test == 'greed') {
		successToRoll++;
		debugStringAddition += ':anger: Success dice raised to ' + successToRoll + ' due to Hobbit-sense cultural blessing\n';
	}
	if (hasTrait(g, charID, 'virtue', 'strength of will') && test == 'dread') {
		successToRoll++;
		debugStringAddition += ':anger: Success dice raised to ' + successToRoll + ' due to Strength of Will virtue\n';
	}
	if (hasTrait(g, charID, 'virtue', 'untameable spirit') && test == 'sorcery') {
		successToRoll++;
		debugStringAddition += ':anger: Success dice raised to ' + successToRoll + ' due to Untameable Spirit virtue\n';
	}

	// figure out feat roll
	let featToRoll = 1;
	if (hasTrait(g, charID, 'virtue', 'against the unseen') && test == 'dread') {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to Against the Unseen virtue\n';
	}
	if (hasCulturalBlessing(g, charID, 'stout-hearted') && attribute == 'valour') {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to Stout-hearted cultural blessing\n';
	}
	if (hasCulturalBlessing(g, charID, 'hobbit-sense') && attribute == 'wisdom') {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to Hobbit-sense cultural blessing\n';
	}

	// figure out the skilltn
	let targetTN = charSkillTN(g, charID, attribute);

	return [featToRoll, successToRoll, 0, targetTN, debugStringAddition, ''];
}

// look up a particular attribute and return the factors needed to roll on it
function torRollLookupAttribute(g, charID, test, locale) {

	let successToRoll = characterDataFetchHandleNumbers(g, charID, 'sheet', test);
	let debugStringAddition = ':anger: Attribute lookup ' + stringutil.titleCaseWords(test) + ' raised success dice to ' + successToRoll + '\n';

	// figure out feat roll
	let featToRoll = 1;
	if (hasCulturalBlessing(g, charID, 'stout-hearted') && test == 'valour') {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to Stout-hearted cultural blessing\n';
	}
	if (hasCulturalBlessing(g, charID, 'hobbit-sense') && test == 'wisdom') {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to Hobbit-sense cultural blessing\n';
	}

	// figure out the skilltn
	let targetTN = charSkillTN(g, charID, test);

	return [featToRoll, successToRoll, 0, targetTN, debugStringAddition, ''];
}

// look up a particular attribute and return the factors needed to roll on it
function torRollLookupProficiency(g, charID, test, locale) {

	let successToRoll = characterDataFetchHandleNumbers(g, charID, 'sheet', test);
	let debugStringAddition = ':anger: Proficiency lookup ' + stringutil.titleCaseWords(test) + ' raised success dice to ' + successToRoll + '\n';

	// handle factors that give a bonus die
	if (test == 'brawling' && hasTrait(g, charID, 'virtue', 'brother to bears')) {
		successToRoll++; // reverse the -1d
		debugString += ':anger: Brother To Bears raised Brawling success dice to ' + successToRoll + '\n';
	}

	// figure out feat roll
	let featToRoll = 1;
	if (hasTrait(g, charID, 'virtue', 'sure at the mark') && test == 'bows') {
		featToRoll = 2;
		debugStringAddition += ':anger: Feat die favored due to Sure At The Mark virtue\n';
	}

	// figure out the skilltn
	let targetTN = charSkillTN(g, charID, test);

	return [featToRoll, successToRoll, 0, targetTN, debugStringAddition, ''];
}

// explain command -- explain the last roll in this game
function explainCommand(userID, locale) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (g.debugString == '') return 'I\'m sorry I can\'t explain it all. I am tired and very worried, and it\'s a long tale.';
	return '**Explanation**:\n' + g.debugString;
}

// FEAT ROLL FUNCTIONS --------------------------------------------------------

// front ends to converting rolls to the appropriate emoji
function featRollText(roll, diceType) {
	return emoji.customEmoji(roll, 0, 0, diceType);
}

function successRollText(roll, weary, diceType) {
	return emoji.customEmoji(roll, 1, weary && roll <= 3, diceType);
}

// A feat roll with eye and gandalf options
function featRoll() {
	let r = regdice.simpleRoll(12);
	if (r == 11) r = emoji.EYE;
	else if (r == 12) r = emoji.GANDALF;
	return r;
}

// returns the comparative value of a feat roll for comparison purposes (only!) -- used in feat=best/worst
function featRollValue(roll, enemy) {
	if (roll == emoji.EYE && enemy) return 12;
	else if (roll == emoji.EYE && !enemy) return 0;
	else if (roll == emoji.GANDALF && enemy)  return 0;
	else if (roll == emoji.GANDALF && !enemy) return 12;
	else return roll;
}

// TOR DICE ROLLING ----------------------------------------------------------

const skillList = ['Awe','Enhearten','Persuade','Athletics','Travel','Stealth','Awareness',
		   'Insight','Scan','Hunting','Healing','Explore','Song','Courtesy',
		   'Riddle','Craft','Battle','Lore'];

function diceStyle(g) {
	if (!g || !g.dicestyle) return 'full';
	return g.dicestyle;
}

// compares to the target and determines success or failure, returning the results string
function compareToTarget(rollResults, target, locale) {
	let tnstring = ' (TN' + target + ')';
	if (rollResults[1] == -1) return stringutil.titleCaseWords(slashcommands.localizedString(locale, 'roll-autofailure', [])) + tnstring;
	if (rollResults[0] < target && rollResults[1] != 1) return stringutil.titleCaseWords(slashcommands.localizedString(locale, 'roll-failure', [])) + tnstring;
	let modifier = '';
	if (rollResults[2] >= 2) modifier = stringutil.titleCaseWords(slashcommands.localizedString(locale, 'roll-extraordinarysuccess', []));
	else if (rollResults[2] == 1 ) modifier = stringutil.titleCaseWords(slashcommands.localizedString(locale, 'roll-greatsuccess', []))
	let results = '';
	if (rollResults[1] == 1) results = stringutil.titleCaseWords(slashcommands.localizedString(locale, 'roll-autosuccess', []))
	else results = stringutil.titleCaseWords(slashcommands.localizedString(locale, 'roll-success', []))
	let reportjoiner = slashcommands.localizedString(locale, 'report-joiner', []);
	if (reportjoiner == '') {
		// adjective first language, e.g., English
		return modifier + (modifier == '' ? '' : ' ') + results + tnstring;
	}
	// noun first language, e.g., Portuguese:
	return results + (modifier == '' ? '' : ' ') + modifier + tnstring;
}

// if the user didn't specify a weapon, pick the best one, by proficiency and with ties split by damage
function findBestWeapon(g, charID, rangedonly) {
	let highestSkill = 0;
	let highestDamage = 0;
	let weaponName = '';
	for (let w in characterDataFetch(g, charID, 'weapons', null)) {
		let wData = characterDataFetch(g, charID, 'weapons', w);
		if ((!rangedonly || weaponCanBeUsedRanged(wData)) && wData[4] == 'TRUE' && (Number(wData[0]) > highestSkill || (wData[0] == highestSkill && Number(wData[1]) > highestDamage))) {
			weaponName = w;
			highestSkill = Number(wData[0]);
			highestDamage = Number(wData[1]);
		}
	}
	return weaponName;
}

// given a 2e skill returns which attribute it goes under
function skillAttribute(skill) {
	skill = stringutil.lowerCase(skill);
	if (['awe', 'athletics', 'awareness', 'hunting', 'song', 'craft', 'axes', 'bows', 'spears', 'swords', 'brawling'].includes(skill)) return 'strength';
	if (['enhearten', 'travel', 'insight', 'healing', 'courtesy', 'battle', 'valour'].includes(skill)) return 'heart';
	if (['persuade', 'stealth', 'scan', 'explore', 'riddle', 'lore', 'wisdom'].includes(skill)) return 'wits';
	return null;
}

// given a skill, look up and return the relevant TN, accounting for misfortune
function charSkillTN(g, charID, skill) {
	let attr = skillAttribute(skill);
	if (attr == null) return 0;
	let r = characterDataFetchHandleNumbers(g, charID, attr + 'tn', null);
	if (r == null || r == 0) { // happens on 1e legacy characters in a 2e game or in various failure states
		r = 20 - characterDataFetchHandleNumbers(g, charID, 'sheet', attr);
	}
	if (g.characters[charID].misfortune) r += characterDataFetchHandleNumbers(g, charID, 'shadow', '') + characterDataFetchHandleNumbers(g, charID, 'shadowscars', '');
	return r;
}

// GAMES AND SELECTION ---------------------------------------------------------

var gameData = {};		// the list of games themselves and all associated data
var userConfig = {};	// saves language, prefix, etc. for each Narvi user

// load the saved game data from a JSON file
gameData = filehandling.loadGameData();
googlesheets.initiate(gameData, notifyHostUser, botSendMessage, config.hostChannel);
preloadExternalCaches();

// replace a selected game for all players
function replaceSelectedGame(oldGame, newGame) {
	for (let p in userConfig) {
		if (userConfig[p].selectedGame == oldGame) userConfig[p].selectedGame = newGame;
	}
}

// look up a game
function getGameData(gameName) {
	if (gameName == '' || gameName == undefined) return null;
	gameName = stringutil.titleCase(gameName);
	let r = gameData[gameName];
	if (r == undefined) return null;
	return r;
}

function getGameName(g) {
	for (let game in gameData) {
		if (gameData[game] == g) return game;
	}
	return null;
}

// find my selected game
function mySelectedGame(userID) {
	return getGameData(stringutil.titleCase(getUserConfig(userID,'selectedGame')));
}

// is this player a part of this game?
function isPlayerInGame(userID, game) {
	if (game.loremaster == userID) return true;
	for (let key in game.players) if (key == userID) return true;
	return false;
}

// is this player the loremaster of this game?
function isPlayerLoremasterOfGame(userID, game) {
	if (game == null) return false;
	if (game.loremaster == userID) return true;
	if (gameHasConfigFlag(game, 'unlocked-commands') && isPlayerInGame(userID, game)) return true;
	return false;
}

// go through all games and if a person is only in one, automatically make it their selected game
function autoSelectGames() {
	let playerInvolvement = {};
	let p, g;
	// for each game
	for (g in gameData) {
		// for each player
		for (p in gameData[g].players) {
			// if already in list, set value to '' which means in two or more games
			if (playerInvolvement[p] != null) playerInvolvement[p] = '';
			// else add to list, with value set to this game
			else playerInvolvement[p] = g;
		}
		p = gameData[g].loremaster;
		// if already in list, set value to '' which means in two or more games
		if (playerInvolvement[p] != null) playerInvolvement[p] = '';
		// else add to list, with value set to this game
		else playerInvolvement[p] = g;
	}
	for (p in playerInvolvement) {
		if (playerInvolvement[p] != '') setUserConfig(p, 'selectedGame', playerInvolvement[p]);
	}
	filehandling.saveStateData(userConfig);
	return;
}

function gameCharactersForPlayer(g, userID) {
	if (g.loremaster == userID) return g.lmCharacters;
	if (g.players[userID] != null) return g.players[userID].characters;
	return [];
}

// regex for valid game, character, and secret names
const validName = /^[a-zA-Z\u00C0-\u024F][0-9a-zA-Z\u00C0-\u024F\_]+$/;

// main game command parser/dispatcher
function gameCommand(interaction, userID, channelID, guildID, locale, data, userRealName) {
	let cmd = data.options[0].name;

	if (cmd == 'list') return gameCommandList(userID, channelID, locale, data, userRealName);

	let val = null;
	if (data.options && data.options[0].options && data.options[0].options[0] && data.options[0].options[0].value) val = data.options[0].options[0].value;

	// commands that don't depend on a selected game
	if (cmd == 'create') return gameCommandCreate(val, userID, guildID, locale, data, userRealName);
	if (cmd == 'select') return gameCommandSelect(val, userID, locale);

	// all other commands depend on a selected game, so fetch it
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);

	// commands that do depend on a selected game
	if (cmd == 'show') return gameCommandShow(userID, channelID, locale, data, userRealName);
	if (cmd == 'refresh') return gameCommandRefresh(userID, channelID, locale, data, userRealName);
	if (cmd == 'add') return gameCommandAdd(interaction, g, val, userID, guildID, channelID, locale, data, userRealName);
	if (cmd == 'remove') return gameCommandRemove(interaction, g, val, userID, guildID, channelID, locale, data, userRealName);
	if (cmd == 'rename') return gameCommandRename(g, val, userID, channelID, locale, data, userRealName);
	if (cmd == 'reassign') return gameCommandReassign(interaction, g, val, userID, guildID, channelID, locale, data, userRealName);
	if (cmd == 'give') return gameCommandGive(interaction, g, val, userID, guildID, channelID, locale, data, userRealName);
	if (cmd == 'end') return gameCommandEnd(g, val, userID, channelID, locale, data, userRealName);
	if (cmd == 'system') return gameCommandSystem(g, val, userID, channelID, locale, data, userRealName);
	if (cmd == 'dicestyle') return gameCommandDicestyle(g, val, userID, channelID, locale, data, userRealName);
	if (cmd == 'lmchannel') return gameCommandLMChannel(g, val, userID, channelID, locale, data, userRealName);
	if (cmd == 'config') return gameCommandConfig(g, val, userID, channelID, locale, data, userRealName);
	if (cmd == 'leave') return gameCommandLeave(g, val, userID, channelID, locale, data, userRealName);
	if (cmd == 'journeys') {
		if (g.journey.journeys.length == 0) return slashcommands.localizedString(locale, 'journey-noneingame', []);
		return journeys.journeys_display_list(g.journey.journeys, locale);
	}
}

// show: show information about your selected game
function gameCommandShow(userID, channelID, locale, data, userRealName) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);

	// heading
	let r = '**' + stringutil.titleCase(getUserConfig(userID,'selectedGame')) + '**:\n`';

	// loremaster
	r += stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'loremaster', [])),15,' ') + ':` ' + g.lmName;
	if (g.loremaster == userID) r += ' (' + slashcommands.localizedString(locale, 'you', []) + '!)';
	r += ' [';
	g.lmCharacters.forEach(charID => {
		r += characterDataFetchHandleNulls(g, charID, 'name', null) + ', ';
	});
	if (g.lmCharacters.length == 0) r += 'none, ';
	r = r.slice(0, -2) + ']\n';

	// system
	r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game','system')),15,' ') + ':` ' + gameEdition(g) + '\n';

	// config
	if (g.config.length > 0) r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game','config')),15,' ') + ':` ' + gameConfigShow(g, ', ', locale) + '\n';

	// channelname 
	let channelName = slashcommands.localizedOption(locale, 'game','lmchannel', 'direct');
	if (g.lmchannel != '') channelName = '#' + botChannelName(g.lmchannel);
	r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game', 'lmchannel')),15,' ') + ':` ' + channelName + '\n';

	// dicestyle
	r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game', 'dicestyle')),15,' ') + ':` ' + slashcommands.localizedOption(locale, 'game','config',g.dicestyle) + '\n';

	// pool
	r += showPool(g, locale) + '\n';

	// light
	if (g.combat.active && g.combat.light) {
		r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'start', 'light')),15,' ') + ':` ' + slashcommands.localizedOption(locale, 'start','light',g.combat.light) + '\n';
	}
	
	// players
	r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'players',[])),15,' ') + ':` ';
	for (player in g.players) {
		r += userNameFromID(player);
		if (userID == config.hostUser || isPlayerLoremasterOfGame(userID, g)) r += ' (' + String(player) + ')';
		r += ' [';
		g.players[player].characters.forEach(charID => {
			r += characterDataFetchHandleNulls(g, charID, 'name', null) + ', ';
		});
		if (g.players[player].characters.length == 0) r += slashcommands.localizedString(locale, 'none',[]) + ', ';
		r = r.slice(0, -2) + '], ';
	}
	if (Object.keys(g.players).length == 0) r += slashcommands.localizedString(locale, 'none',[]) + ', ';
	r = r.slice(0, -2) + '\n';
	
	// council
	if (councilActive(g)) {
		r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'council')),15,' ') + ':` ' + councilShow(g, locale, true) + '\n';
	}

	// endeavour
	if (endeavourActive(g)) {
		r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'endeavour')),15,' ') + ':` ' + endeavourShow(g, locale, true) + '\n';
	}

	// journeys
	if (g.journey.journeys.length != 0) {
		r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'journey')),15,' ') + ':` ' + g.journey.journeys.join(', ') + '\n';
	}
	// journey
	if (g.journey.active != '') r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'journey-status',[])),15,' ') + ':` ' + journeys.journeys_status(g, locale) + '\n';

	// combat
	if (g.combat.active) r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'combat-combat',[])),15,' ') + ':` ' + (g.combat.title != '' ? ('**' + g.combat.title + '**: ') : '') + showCombatPhaseOrAll(g, g.combat.phase, locale, true, true) + (g.combat.ambush ? ' [' + slashcommands.localizedSubcommand(locale, 'start', 'ambush') + '!]' : '') + '\n';
	if (g.combatMods.length > 0) r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'combat-mods',[])),15,' ') + ':` ' + showCombatMods(g,locale,false) + '\n';

	return r;
}

// show: show information about your selected game
function gameCommandRefresh(userID, channelID, locale, data, userRealName) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	for (c in g.characters) {
		loadExternalCacheSheet(g.characters[c].keyType, g.characters[c].repositoryKey, true, channelID);
	}
	return slashcommands.localizedString(locale, 'game-refresh', [stringutil.titleCase(getUserConfig(userID,'selectedGame'))]);
}

// show: show information about your selected game
function gameCommandList(userID, channelID, locale, data, userRealName) {
	let g = mySelectedGame(userID);
	let gamesShown = 0, selectedShown = 0, games2e = 0;
	let r = '**' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'game-list',[])) + '**:\n';
	r += ':white_large_square: __`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'game','create','name')),30,' ') + '`__ __`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game','system')),7,' ')+ '`__ __**' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'character',[])) + '**__\n';
	for (key in gameData) {
		if (isPlayerInGame(userID, gameData[key])) {
			gamesShown++;
			if (g == gameData[key]) {
				r += ':white_check_mark: ';
				selectedShown = 1;
			} else r += ':green_square: ';
			r += '`';
			r += stringutil.padRight(stringutil.titleCase(key), 30, ' ') + ' ' + stringutil.padRight(slashcommands.localizedOption(locale, 'game','system',gameEdition(gameData[key])), 7, ' ') + '` ';
			if (gameData[key].loremaster == userID) r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'loremaster', []));
			else {
				for (let player in gameData[key].players) {
					if (player == userID) r += characterDataFetchHandleNulls(gameData[key], gameData[key].players[player].currCharacter, 'name', null);
				}
			}
			r += '\n';
		}
	}
	if (gamesShown == 0) return slashcommands.localizedString(locale, 'game-none', []);
	return r + '**' + slashcommands.localizedString(locale, 'game-listed', []) + '**: ' + gamesShown + (selectedShown ? ' (' + slashcommands.localizedString(locale, 'game-checked', [':white_check_mark:']) + ')' : '');
}

// create: makes a new game
function gameCommandCreate(val, userID, guildID, locale, data, userRealName) {
	let firstGame = true;
	if (findGamesOnServer(guildID, userID).length != 0) firstGame = false;
	val = stringutil.lowerCase(val);
	// verify it's a valid name
	if (!val.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [val]);
	// verify no game with that name already exists
	if (getGameData(val) != null) return slashcommands.localizedString(locale, 'game-exists', [stringutil.titleCase(val)]);
	// create the game
	val = stringutil.titleCase(val);
	gameData[val] = {
		'pool': 0,
		'maxpool': 0,
		'loremaster': userID,
		'lmName': stringutil.sanitizeString(userRealName),
		'server': guildID,
		'lmchannel': '',
		'edition': 'tor',
		'players': {},
		'characters': {},
		'counters': {},
		'secrets': {},
		'lmCharacters': [],
		'lmCurrCharacter': null,
		'songbook': {},
		'dicestyle': 'full',
		'config': [],
		'rallycurrent': 0,
		'rallynext': 0,
		'round': 1,
		'initiative': [],
		'weather': 9,
		'hexflowers': {},
		'eye': {
			'base': 0,
			'current': 0,
			'threshold': 0
		},
		'encounters': {},
		'selectedEncounter': '',
		'combat': {
			'active': false,
			'round': 0,
			'phase': 0,
			'ambush': null,
			'advsurprise': false,
			'light': null,
			'encounter': '',
			'actors': [],
			'log': '',
			'name': ''
		},
		'council': {
			'time': 0,
			'timelimit': 0,
			'score': 0,
			'resistance': 0,
			'request': '',
			'attitude': 'open',
			'attitudemod': 0,
			'introduction': 0,
			'rules': 'core',
			'undo': {}
		},
		'endeavour': {
			'time': 0,
			'timelimit': 0,
			'score': 0,
			'resistance': 0,
			'request': '',
			'undo': {}
		},
		'journey': {
			'journeys': [],
			'active': '',
			'forced': false,
			'season': '',
			'eventtype': 'journeyevents',
			'progress': 0,
			'duration': 0,
			'underground': false,
			'unmounted': false,
			'pendingPendingFatigue': {},
			'log': '',
			'undo': '',
			'peril': 0,			// number of peril events left to endure
			'event': '',		// the code from journeyevents of the last event, or 'marching' for marching test
			'skill': '',		// the skill that will be tested next
			'typeoverride': '',	// for things like Gilraen's patron power
			'favourable': false,// for Favourable Circumstances bonus
			'pendingPendingFatigue': {},
			'featmod': 0		// for things like Ponder Storied and Figured Maps
		},
		'combatMods': [],
		'songUsed': '',
		'songBonus': 0,
		'songCharacters': [],
		'debugString': '', // for explain command
		'lastUsed': Date.now()
	};
	// save the game file
	filehandling.saveGameData(gameData);
	// make it your selected game
	setUserConfig(userID,'selectedGame', val);
	botPresence();
	return slashcommands.localizedString(locale, 'game-created', [val]) + (!firstGame ? '\n\n:light_bulb: Narvi is 100% free to use and always will be; but [contributing a dollar](https://ko-fi.com/V7V44RKAI) to its hosting costs would really help. Thanks for using Narvi!' : '');
}

// select: choose a game
function gameCommandSelect(val, userID, locale) {
	val = stringutil.titleCase(val);
	if (stringutil.titleCase(slashcommands.localizedString(locale, 'none', [])) == val) {
		setUserConfig(userID,'selectedGame','');
		return slashcommands.localizedString(locale, 'game-selected', [val]);
	}

	let g = getGameData(val);
	if (g == null) {
		for (gName in gameData) {
			if (isPlayerInGame(userID, gameData[gName]) && gName.startsWith(val)) {
				g = getGameData(gName);
				val = gName;
			}
		}
	}
	if (g == null || (userID != config.hostUser && !isPlayerInGame(userID, g))) return slashcommands.localizedString(locale, 'error-gamenotfound', [val]);
	setUserConfig(userID,'selectedGame',val);
	return slashcommands.localizedString(locale, 'game-selected', [val]);
}

// add: adds a player to an existing game
function gameCommandAdd(interaction, g, val, userID, guildID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g) && userID != config.hostUser) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// since this has to use a promise, it will handle all the rest of the functionality within it
	// including the display of the outcome, which is why interaction got passed in
	bot.searchGuildMembers(guildID,val,1).then(r => {
		if (r == null || r == '') {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-nouser', [val]));
		}
		// see if they're already in the players list, or the loremaster
		let playerID = r[0].id;
		if (playerID == g.loremaster || g.players[playerID] != null) {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-alreadyuser', [val]));
		}

		g.players[playerID] = {
			'characters': [],
			'currCharacter': ''
		}
		filehandling.saveGameData(gameData);

		if (getUserConfig(playerID,'selectedGame') == null) {
			setUserConfig(playerID,'selectedGame', getUserConfig(userID,'selectedGame'));
		}

		return interaction.createMessage(slashcommands.localizedString(locale, 'game-addeduser', [val]));
	})
	.catch(error => {
		notifyHostUser('API error attempting to add user ' + val + ': ' + error, true);
		if (guildID == undefined) return interaction.createMessage(slashcommands.localizedString(locale, 'error-noserver', []));
		else return interaction.createMessage(slashcommands.localizedString(locale, 'error-internal', [error]));
	});
	return '';
}

// helper function for gameCommandRemove, so the actual removal is in one place whether it's done by ID or name
function gameCommandRemoveByUserID(game, userID, userName, locale) {

	let r = '';

	// remove all their characters
	let clist = gameCharactersForPlayer(game, userID);
	clist.forEach(p => {
		r += characterDataFetchHandleNulls(game, p, 'name', null) + ', ';
		delete game.characters[p];
	});
	if (r != '') r = r.slice(0,-2); else r = slashcommands.localizedString(locale, 'none', []);

	// delete the player record itself
	delete game.players[userID];
	filehandling.saveGameData(gameData);

	// if this is their selected game, unselect it
	if (getGameData(getUserConfig(userID,'selectedGame')) == game) {
		setUserConfig(userID,'selectedGame', null);
	}

	return slashcommands.localizedString(locale, 'game-removeduser', [userName, r]);
}

// remove: removes a player from an existing game
function gameCommandRemove(interaction, g, val, userID, guildID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g) && userID != config.hostUser) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// what if the player is no longer in the server? how would we be able to identify them? allow "Unknown User <num>" or "<num>"
	if (val.startsWith('Unknown User ')) val = val.slice(13);
	if (!isNaN(val)) {
		// assume it is a userID
		return gameCommandRemoveByUserID(g, val, val, locale);
	}

	// since this has to use a promise, it will handle all the rest of the functionality within it
	// including the display of the outcome, which is why interaction got passed in
	bot.searchGuildMembers(guildID,val,1).then(r => {
		if (r == null || r == '') {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-nouser', [val]));
		}
		// see if they're not in the players list, or the loremaster
		let playerID = r[0].id;
		if (playerID == userID) {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-removelm', [val]));
		}
		if (g.players[playerID] == null) {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-notingame', [val]));
		}

		return interaction.createMessage(gameCommandRemoveByUserID(g, playerID, val, locale));
	})
	.catch(error => {
		notifyHostUser('API error attempting to remove user ' + val + ': ' + error, true);
		return interaction.createMessage(slashcommands.localizedString(locale, 'error-internal', [error]));
	});
	return '';
}

// leave: a player removes themselves from an existing game
function gameCommandLeave(g, val, userID, channelID, locale, data, userRealName) {
	if (isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'game-removelm', [val]);
	if (!val) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'game') + ' ' + slashcommands.localizedSubcommand(locale, 'game', 'leave'), null, data, 'gameleave');

	// unselect the game for you
	setUserConfig(userID, 'selectedGame', null);

	return gameCommandRemoveByUserID(g, userID, userNameFromID(userID), locale);
}

// rename: rename a game
function gameCommandRename(g, val, userID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g) && userID != config.hostUser) return slashcommands.localizedString(locale, 'error-lmonly', []);

	val = stringutil.lowerCase(val);
	// verify it's a valid name
	if (!val.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [val]);
	// verify no game with that name already exists
	if (getGameData(val) != null) return slashcommands.localizedString(locale, 'game-exists', [stringutil.titleCase(val)]);

	// create the game
	let oldName = getUserConfig(userID,'selectedGame');
	val = stringutil.titleCase(val);

	gameData[val] = gameData[oldName];
	delete gameData[oldName];
	filehandling.saveGameData(gameData);

	// change everyone's selected game accordingly
	replaceSelectedGame(oldName, val);
	filehandling.saveStateData(userConfig);

	return slashcommands.localizedString(locale, 'game-renamed', [oldName, val]);
}

// reassign: reassign a character to a player
function gameCommandReassign(interaction, g, val, userID, guildID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// parse options
	let charID = null, userName = null;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'username') userName = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'character') {
			charID = findCharacterInGame(g, data.options[0].options[i].value);
		}
	}
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);

	let oldName = characterDataFetchHandleNulls(g,charID,'name',null);

	// find the player recipient
	bot.searchGuildMembers(guildID,userName,1).then(r => {
		if (r == null || r == '') {
			 return interaction.createMessage(slashcommands.localizedString(locale, 'game-nouser', [userName]));
		}
		// see if they're not in the players list
		let recipientID = r[0].id;
		userName = userNameFromID(recipientID);
		if (g.players[recipientID] == null && !isPlayerLoremasterOfGame(recipientID, g)) {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-notingame', [userName]));
		}
		// find who currently owns the character
		let formerOwner = null;
		if (g.lmCharacters.includes(charID)) formerOwner = g.loremaster;
		for (let p2 in g.players) {
			if (g.players[p2].characters.includes(charID)) formerOwner = p2;
		}
		if (formerOwner == recipientID) {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-alreadyowned', []));
		}

		transferCharacterOwnership(g, charID, recipientID, formerOwner, locale);
		return interaction.createMessage(slashcommands.localizedString(locale, 'game-reassigned', [oldName, userName, characterDataFetchHandleNulls(g, currentCharacter(userID), 'name', null)]));
	})
	.catch(error => {
		notifyHostUser('API error attempting to reassign user ' + val + ': ' + error, true);
		return interaction.createMessage(slashcommands.localizedString(locale, 'error-internal', [error]));
	});
	return '';
}

// give: give the whole game to another person
function gameCommandGive(interaction, g, val, userID, guildID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g) && userID != config.hostUser) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// find the recipient
	bot.searchGuildMembers(guildID,val,1).then(r => {
		if (r == null || r == '') {
			 return interaction.createMessage(slashcommands.localizedString(locale, 'game-nouser', [val]));
			 return '';
		}
		let newOwner = r[0].id;
		// if it's yourself
		if (g.loremaster == newOwner) {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-alreadylm', []));
			return '';
		}

		// make them the loremaster
		g.loremaster = newOwner;
		g.lmName = stringutil.sanitizeString(userNameFromID(newOwner));

		// if you had lmCharacters
		if (g.lmCharacters.length > 0) {
			// add you as a player and make them your characters
			g.players[userID] = {
				'characters': [],
				'currCharacter': ''
			}
			g.lmCharacters.forEach(c => {
				g.players[userID].characters.push(c);
			});
			g.players[userID].currCharacter = g.lmCurrCharacter;
			g.lmCharacters = [];
			g.lmCurrCharacter = null;
		} else {
			// since you're not going to end up in this game, it's no longer your selected game
			setUserConfig(userID, 'selectedGame', '');
		}

		// if the new owner is already a player
		if (g.players[newOwner] != null) {
			// move all their characters to the lmChars
			g.players[newOwner].characters.forEach(c => {
				g.lmCharacters.push(c);
			});
			g.lmCurrCharacter = g.players[newOwner].currCharacter;
			// remove them as a player
			delete g.players[newOwner];
		}

		filehandling.saveGameData(gameData);
		return interaction.createMessage(slashcommands.localizedString(locale, 'game-given', [val]));
	})
	.catch(error => {
		notifyHostUser('API error attempting to give game ' + val + ': ' + error, true);
		return interaction.createMessage(slashcommands.localizedString(locale, 'error-internal', [error]));
	});
	return '';
}

// end: deletes a game and removes all its data
function gameCommandEnd(g, val, userID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g) && userID != config.hostUser) return slashcommands.localizedString(locale, 'error-lmonly', []);
	if (!val) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'game') + ' ' + slashcommands.localizedSubcommand(locale, 'game', 'end'), null, data, 'gameend');

	let gameName = getUserConfig(userID,'selectedGame');
	gameData[gameName].journey.journeys.forEach(j => {journey_orphaned(j)});
	delete gameData[gameName];
	filehandling.saveGameData(gameData);
	replaceSelectedGame(gameName, null);
	filehandling.saveStateData(userConfig);
	botPresence();
	botSendMessage(channelID, slashcommands.localizedString(locale, 'game-ended', [gameName]));
}

// set the edition a game uses
function gameCommandSystem(g, val, userID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
	g.edition = val;
	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'game-system', [gameEdition(g)]);
}

// look up the current edition based on the userID or game
function gameEdition(g) {
	if (g == null || g.edition == null || g.edition == '') return 'none';
	if (g.edition == '2') return 'tor';
	return g.edition;
}

function gameEditionNumeric(g) {
	let e = gameEdition(g);
	if (e == '2' || e == '2e' || e == 'tor' || e == 'strider') return 2;
	return 0;
}

function isTOR(g) {
	return (gameEdition(g) == 'strider' || gameEdition(g) == 'tor');
}

function isStriderMode(g) {
	return (gameEdition(g) == 'strider');
}

// set the dice style a game uses
function gameCommandDicestyle(g, val, userID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
	g.dicestyle = val;
	filehandling.saveGameData(gameData);
	return '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game','dicestyle')),15,' ') + ':` ' + slashcommands.localizedOption(locale, 'game','config', g.dicestyle) + '\n';
}

// set the dice style a game uses
function gameCommandLMChannel(g, val, userID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
	let channelName = slashcommands.localizedOption(locale, 'game','lmchannel', 'direct');
	if (val == 'here') {
		g.lmchannel = channelID;
		channelName = '#' + botChannelName(channelID);
	} else {
		g.lmchannel = '';
	}
	filehandling.saveGameData(gameData);
	return '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game','lmchannel')),15,' ') + ':` ' + channelName + '\n';
}

// set a game configuration entry
function gameCommandConfig(g, val, userID, channelID, locale, data, userRealName) {
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// parse options
	let configEntry = null, configValue = true;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'game' ||
			data.options[0].options[i].name == 'player' ||
			data.options[0].options[i].name == 'combat' ||
			data.options[0].options[i].name == 'endeavours') configEntry = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'value') configValue = data.options[0].options[i].value;
	}

	if (!configEntry) return '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game','config')),15,' ') + ':` ' + gameConfigShow(g,', ', locale) + '\n';

	if (g.config == undefined || !Array.isArray(g.config)) g.config = [];
	let translated = slashcommands.localizedSuboption(locale, 'game', 'config', configEntry);

	if (configValue) {
		if (g.config.includes(configEntry)) {
			return slashcommands.localizedString(locale, 'game-config-exists',[translated]);
		}
		g.config.push(configEntry);
	} else {
		if (!g.config.includes(configEntry)) {
			return slashcommands.localizedString(locale, 'game-config-notexists',[translated]);
		}
		g.config = g.config.filter(x => x != configEntry);
	}
	filehandling.saveGameData(gameData);
	return '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'game','config')),15,' ') + ':` ' + gameConfigShow(g,', ', locale) + '\n';
}

function gameConfigShow(g, s, locale) {
	let r = [];
	if (g && g.config && g.config.length > 0) g.config.forEach(cfg => {
		r.push(slashcommands.localizedSuboption(locale, 'game', 'config', cfg));
	});
	return r.join(s);
}

function gameHasConfigFlag(g, flag) {
	if (!g) return false;
	return g.config.includes(stringutil.lowerCase(flag));
}

// CHARACTERS AND SELECTION ----------------------------------------------------

function characterActiveIndicator(g,p,locale,before,after) {
	if (g.characters[p].active) return '';
	return (before ? ' ' : '') + '_(' + slashcommands.localizedString(locale, 'inactive',[]) + ')_' + (after ? ' ' : '');
}

// main character command parser/dispatcher
function characterCommand(interaction, userID, channelID, guildID, locale, data, userRealName) {
	let cmd = data.options[0].name;

	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (g.players[userID] == null && g.loremaster != userID) return slashcommands.localizedString(locale, 'error-nogame', []);

	let val = null;
	if (data.options && data.options[0].options && data.options[0].options[0] && data.options[0].options[0].value) val = data.options[0].options[0].value;

	// commands that don't depend on a selected character
	if (cmd == 'create') return characterCommandCreate(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
	if (cmd == 'select') return characterCommandSelect(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
	if (cmd == 'list') return characterCommandList(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
	if (cmd == 'remove') return characterCommandRemove(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
	if (cmd == 'active') return characterCommandActive(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);

	// if no character selected and the above didn't work, this is an error
	if (currentCharacter(userID) == null) return slashcommands.localizedString(locale, 'char-unselected', [stringutil.titleCase(getUserConfig(userID,'selectedGame'))]);

	// commands that do depend on a selected character
	if (cmd == 'show') return characterCommandShow(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
	if (cmd == 'refresh') return characterCommandRefresh(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
	if (cmd == 'key') return characterCommandKey(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
	if (cmd == 'image') return characterCommandImage(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
	if (cmd == 'reassign') return characterCommandReassign(interaction, userID, channelID, guildID, locale, data, userRealName, g, val);
}

// create a new character
function characterCommandCreate(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	// generate a character ID
	let nextCharID = filehandling.generateUniqueCharacterId(gameData);
	let charName = '', keyType = '', repositoryKey = '';

	if (isTOR(g)) {
		/* someday when we have multiple character sheet types we need a keyType to be determined by checking a second, optional, parameter, but since we don't, I'm leaving that code out entirely now. */
		keyType = 'googlesheets';

		repositoryKey = cleanRepositoryKey(keyType, val);
		if (repositoryKey == '') return slashcommands.localizedString(locale, 'char-badkey', []);
		for (ch in g.characters) {
			if (g.characters[ch].repositoryKey == repositoryKey && g.characters[ch].keyType == keyType) {
				return slashcommands.localizedString(locale, 'char-duplicate', []);
			}
		}
		loadExternalCacheSheet(keyType, repositoryKey, true, channelID);
	} else {
		// for non-TOR games all we need is a name
		charName = stringutil.sanitizeString(val);
	}

	// add them with default values and the specified character name or key information
	g.characters[nextCharID] = {
		'name': charName,
		'handedness': 1,
		'dmgReceivedThisRound': 0,
		'tempparry': 0,
		'pendingfatigue': 0,
		'daunted': false,
		'shieldsmashed': false,
		'seized': false,
		'bitten': false,
		'drained': false,
		'drainedTN': 14,
		'drainedAdv': 'unknown',
		'dreaming': false,
		'misfortune': false,
		'dismayed': false,
		'haunted': false,
		'weary': false,
		'unweary': false,
		'miserable': false,
		'bleeding': 0,
		'poisoned': 0,
		'poisontreated': false,
		'tengwar': 0,
		'protectionTN': null,
		'fendParry;': 0,
		'numEngaged': 0,
		'damageDone': 0,
		'shadowTests': [],
		'protectionTests': [],
		'curWeapon': '',
		'stance':'open',
		'knockback': false,
		'journeyRole': [],
		'repositoryKey': repositoryKey,
		'keyType': keyType,
		'active': true,
		'charImage':'',
		'scribbles': {}
	};
	if (isPlayerLoremasterOfGame(userID, g))	{
		// add to this player's character array
		g.lmCharacters.push(nextCharID);
		// make it this player's default selected character
		g.lmCurrCharacter = nextCharID;
	} else {
		// add to this player's character array
		g.players[userID].characters.push(nextCharID);
		// make it this player's default selected character
		g.players[userID].currCharacter = nextCharID;
	}
	filehandling.saveGameData(gameData);
	// return a message about it
	return slashcommands.localizedString(locale, 'char-created', [stringutil.titleCase(getUserConfig(userID,'selectedGame'))]);
}

// switch a character
function characterCommandSelect(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	let p = characterPlayerFindByName(g, userID, stringutil.lowerCase(val));
	if (p == null) return slashcommands.localizedString(locale, 'error-charnotfound', [val]);
	if (isPlayerLoremasterOfGame(userID, g)) g.lmCurrCharacter = p;
	else g.players[userID].currCharacter = p;
	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'char-selected', [characterDataFetchHandleNulls(g, currentCharacter(userID), 'name', null),stringutil.titleCase(getUserConfig(userID,'selectedGame'))]);
}

// show the player a list of their characters in the current game
function characterCommandList(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	let r = slashcommands.localizedString(locale, 'char-list', [stringutil.titleCase(getUserConfig(userID,'selectedGame'))]) + '\n';
	let found = false;
	gameCharactersForPlayer(g, userID).forEach(p => {
		found = true;
		if (p == currentCharacter(userID)) r += ':heavy_check_mark:';
			else r += ':black_square_button:';
		r += ' **' + characterDataFetchHandleNulls(g, p, 'name', null) + '**: ' + characterActiveIndicator(g,p,locale,false,true);
		r += characterDataFetchHandleNulls(g, p, 'culture', null) + ', ' + characterDataFetchHandleNulls(g, p, 'calling', null);
		r += '\n';
	});
	if (!found) r += ':x: ' + slashcommands.localizedString(locale, 'none', []) + '.';
	return r;
}

// show the current character
function characterCommandShow(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	let r = slashcommands.localizedString(locale, 'char-selected', [characterDataFetchHandleNulls(g, currentCharacter(userID), 'name', null) + characterActiveIndicator(g,currentCharacter(userID),locale,true, false),stringutil.titleCase(getUserConfig(userID,'selectedGame'))]);

	var foundOthers = false;
	gameCharactersForPlayer(g, userID).forEach(p => {
		if (p != currentCharacter(userID)) {
			if (!foundOthers) {
				r += '\n' + slashcommands.localizedString(locale, 'char-others', []) + ': ';
				foundOthers = true;
			}
			r += characterDataFetchHandleNulls(g, p, 'name', null) + ', ';
		}
	});
	if (foundOthers) r = r.slice(0,-2) + '.'
	return r;
}

// ask for a refresh for an externally cached character
function characterCommandRefresh(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	if (!currentCharacter(userID) || !g.characters[currentCharacter(userID)]) return slashcommands.localizedString(locale, 'char-unselected', [stringutil.titleCase(getUserConfig(userID,'selectedGame'))]);
	loadExternalCacheSheet(g.characters[currentCharacter(userID)].keyType, g.characters[currentCharacter(userID)].repositoryKey, true, channelID);
	return slashcommands.localizedString(locale, 'char-refresh', [characterDataFetchHandleNulls(g, currentCharacter(userID), 'name', null)]);
}

// ask for a refresh for an externally cached character
function characterCommandKey(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	let p = currentCharacter(userID);
	if (g.characters[p].keyType == '') g.characters[p].keyType = 'googlesheets';
	val = cleanRepositoryKey(g.characters[p].keyType, val);
	if (val == '') return slashcommands.localizedString(locale, 'char-badkey', []);
	for (ch in g.characters) {
		if (g.characters[ch].repositoryKey == val && g.characters[ch].keyType == g.characters[p].keyType) {
			return slashcommands.localizedString(locale, 'char-duplicate', []);
		}
	}
	g.characters[p].repositoryKey = val;
	loadExternalCacheSheet(g.characters[p].keyType, val, true, channelID);
	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'char-refresh', [characterDataFetchHandleNulls(g, currentCharacter(userID), 'name', null)]);
}

// set the image of the character for rolls
function characterCommandImage(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	let p = currentCharacter(userID);
	let url = stringutil.lowerCase(val);
	if (!url.startsWith('http://') && !url.startsWith('https://' )) return slashcommands.localizedString(locale, 'error-badurl', [val]);
	characterDataUpdate(g, p, 'charImage', null, '=', val, null, null, channelID);
	filehandling.saveGameData(gameData);
	let theEmbed = embeds.buildEmbed(
		characterDataFetchHandleNulls(g, p, 'name', null),
		val,
		getCharLink(g, p),
		'',
		'',
		'',
		bot, null,
		getUserConfig(userID,'selectedGame'),
		embeds.BUTTON_GREEN,
		userID
		);
	return {embed: theEmbed};
}

// set the active flag for the character
function characterCommandActive(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	let charName = null, charID = currentCharacter(userID);

	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'character') charName = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'value') val = data.options[0].options[i].value;
	}
	if (charName) {
		if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
		charID = findCharacterInGame(g, charName);
	}
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	
	g.characters[charID].active = val;
	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'char-active', [characterDataFetchHandleNulls(g, charID, 'name', null), showOnOrOff(locale, val)]);
}

// find a character for this player
function characterPlayerFindByName(g, userID, charName) {
	let r = null;
	gameCharactersForPlayer(g, userID).forEach(p => {
		if (stringutil.lowerCase(characterDataFetchHandleNulls(g,p,'name',null)) == charName) r = p;
	});
	if (r) return r;
	gameCharactersForPlayer(g, userID).forEach(p => {
		if (stringutil.lowerCase(characterDataFetchHandleNulls(g,p,'name',null)).startsWith(charName)) r = p;
	});
	return r;
}

// if confirmed delete a character from the game entirely
function characterCommandRemove(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	// parse options
	let confirm = null, charID = currentCharacter(userID);
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'confirm') confirm = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[0].options[i].value);
		}
	}
	if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'character') + ' ' + slashcommands.localizedSubcommand(locale, 'character', 'remove'), null, data, 'charremove');
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);

	// remove from the characters structure
	let oldName = characterDataFetchHandleNulls(g,charID,'name',null);
	delete g.characters[charID];

	// if the character belonged to the Loremaster, take it away and maybe null the selected character
	if (g.lmCharacters.includes(charID)) {
		g.lmCharacters = g.lmCharacters.filter(p => p != charID);
		if (g.lmCurrCharacter == charID) g.lmCurrCharacter = null;
	}
	
	// same thing for players
	for (player in g.players) {
		if (g.players[player].characters.includes(charID)) {
			g.players[player].characters = g.players[player].characters.filter(p => p != charID);
			if (g.players[player].currCharacter == charID) g.players[player].currCharacter = null;
		}
	}

	// if any adversaries in the current encounter have them engaged, disengage
	if (g.combat.encounter && g.encounters[g.combat.encounter].adversaries && g.encounters[g.combat.encounter].adversaries.length > 0) {
		g.encounters[g.combat.encounter].adversaries.forEach(adv => {
			g.encounters[g.combat.encounter].adversaries[adv].engagement = g.encounters[g.combat.encounter].adversaries[adv].engagement.filter((c) => c != charID);
		});
	}

	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'char-removed', [oldName]);
}

// transfer ownership of a character to another player
function characterCommandReassign(interaction, userID, channelID, guildID, locale, data, userRealName, g, val) {
	let charID = currentCharacter(userID);
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	let oldName = characterDataFetchHandleNulls(g,charID,'name',null);

	// since this has to use a promise, it will handle all the rest of the functionality within it
	// including the display of the outcome, which is why channelID got passed in
	bot.searchGuildMembers(guildID,val,1).then(r => {
		if (r == null || r == '') {
			 return interaction.createMessage(slashcommands.localizedString(locale, 'game-nouser', [val]));
		}
		// see if they're in the players list, or the person doing the command
		let playerID = r[0].id;
		val = userNameFromID(playerID);
		if (playerID == userID) {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-alreadyowned', [val]));
		}
		if (g.players[playerID] == null && g.loremaster != playerID) {
			return interaction.createMessage(slashcommands.localizedString(locale, 'game-notingame', [val]));
		}

		let newName = transferCharacterOwnership(g, charID, playerID, userID, locale);
		return interaction.createMessage(slashcommands.localizedString(locale, 'game-reassigned', [oldName, val, characterDataFetchHandleNulls(g, currentCharacter(userID), 'name', null)]));
	})
	.catch(error => {
		notifyHostUser('API error attempting to reassign character ' + val + ': ' + error, true);
		return interaction.createMessage(slashcommands.localizedString(locale, 'error-internal', [error]));
	});
	return '';
}

// reassign character ownership
function transferCharacterOwnership(g, charToTransfer, playerReceivingCharacter, formerOwner, locale) {

	// add the character to the other player's array
	if (g.loremaster == playerReceivingCharacter) g.lmCharacters.push(charToTransfer);
	else g.players[playerReceivingCharacter].characters.push(charToTransfer);

	// remove the character from this player's array
	if (g.loremaster == formerOwner) g.lmCharacters = g.lmCharacters.filter(p => p != charToTransfer);
	else if (formerOwner && g.players[formerOwner]) g.players[formerOwner].characters = g.players[formerOwner].characters.filter(p => p != charToTransfer);

	// if it had no former player a quick exit
	if (!formerOwner) {
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'none', []);
	}

	// otherwise figure out the name of the character
	let newName = '';
	if (gameCharactersForPlayer(g, formerOwner).length == 0) {
		if (isPlayerLoremasterOfGame(formerOwner, g)) {
			// set currCharacter to null
			g.lmCurrCharacter = null;
		} else {
			// set currCharacter to null
			g.players[formerOwner].currCharacter = null;
		}
		newName = slashcommands.localizedString(locale, 'none', []);
	} else {
		if (isPlayerLoremasterOfGame(formerOwner, g)) {
			// set currCharacter to null
			g.lmCurrCharacter = g.lmCharacters[0];
		} else {
			// set currCharacter to null
			g.players[formerOwner].currCharacter = g.players[formerOwner].characters[0];
		}
		newName = characterDataFetchHandleNulls(g,currentCharacter(formerOwner),'name',null);
	}
	filehandling.saveGameData(gameData);
	return newName;
}

function findCharacterInGame(g, charName) {
	if (g == null || charName == null || charName == undefined || charName == '') return null;
	for (let k in g.characters) {
		if (stringutil.lowerCase(stringutil.wordWithoutSpaces(characterDataFetchHandleNulls(g, k, 'name', null))) == stringutil.lowerCase(stringutil.wordWithoutSpaces(charName))) return Number(k);
	}
	// if nothing was found, try finding a shortened name
	for (let k in g.characters) {
		if (stringutil.lowerCase(stringutil.wordWithoutSpaces(characterDataFetchHandleNulls(g, k, 'name', null))).substring(0, charName.length) == stringutil.lowerCase(stringutil.wordWithoutSpaces(charName))) return Number(k);
	}
	return null;
}

// TABLE COMMAND --------------------------------------------------------------

// the LM of my current game, so I can be in their tables
function usersLM(userID) {
	let g = mySelectedGame(userID);
	if (!g) return null;
	return g.loremaster;
}

// lookup tables
function tableCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);

	let cmd = data.options[0].name;

	// parse options
	let shortname = null, tag = null, searchtext = null, mine = null, published = null, modifier = '',
		feat = null, natural = true, roll = null, value = null, range = null, text = null, confirm = null, tables = null;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'shortname') shortname = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'tag') tag = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'searchtext') searchtext = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'mine') mine = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'published') published = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'modifier') modifier = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'feat') feat = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'natural') natural = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'roll') roll = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'value') value = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'range') range = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'text') text = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'confirm') confirm = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'tables') tables = data.options[0].options[i].value;
	}

	let tableName = getUserConfig(userID, 'currentTable');

	if (cmd == 'list') { // List available tables
		return lookuptables.tables_list(userID, config.hostUser, usersLM(userID), locale, mine, published, tag, searchtext);
	}

	if (cmd == 'creategroup') { // Create a table group
		shortname = stringutil.sanitizeString(shortname);
		if (!shortname.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [shortname]);
		let r = lookuptables.tablegroups_create(shortname, userID, config.hostUser, usersLM(userID), tables);
		if (r == 'table-exists' || r == 'table-groupcreated') return slashcommands.localizedString(locale, r, [stringutil.lowerCase(shortname), tables]);
		return slashcommands.localizedString(locale, 'table-notexists', [r]);
	}

	if (cmd == 'deletegroup') { // Delete this table group permanently
		if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'table') + ' ' + slashcommands.localizedSubcommand(locale, 'table', 'deletegroup'), null, data, 'tabledeletegroup');
		let r = lookuptables.tablegroups_delete(shortname, userID, config.hostUser);
		if (r != '') return slashcommands.localizedString(locale, r, [shortname]);
		return slashcommands.localizedString(locale, 'table-groupdeleted', [shortname]);
	}

	if (cmd == 'create') { // Create a table
		shortname = stringutil.sanitizeString(shortname);
		if (!shortname.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [shortname]);
		if (!lookuptables.tables_create(shortname,userID)) return ':warning: ' + slashcommands.localizedString(locale,'table-exists',[shortname]);
		// set the currentTable to this name
		setUserConfig(userID,'currentTable', stringutil.lowerCase(shortname));
		return slashcommands.localizedString(locale, 'table-created', [stringutil.lowerCase(shortname)]);
	}

	if (cmd == 'select') { // Select an existing table to edit or roll on
		if (lookuptables.tables_accessible(shortname, userID, config.hostUser, usersLM(userID), 'read') == false) return ':warning: ' + slashcommands.localizedString(locale,'table-notexists',[shortname]);
		setUserConfig(userID,'currentTable', stringutil.lowerCase(shortname));
		return slashcommands.localizedString(locale,'table-selected', [stringutil.lowerCase(shortname)]);
	}

	if (shortname != null) tableName = stringutil.lowerCase(shortname);
	if (tableName == null || tableName == '') return slashcommands.localizedString(locale,'table-notselected',[]);

	if (cmd == 'show') { // Show an existing table to edit or roll on
		return lookuptables.tables_show(tableName, locale, userID, config.hostUser, userNameFromID);
	}

	if (cmd == 'roll') { // Make a roll on a table
		// first try for a group
		let r = lookuptables.tablegroups_roll(tableName, locale, modifier, feat, !natural, torRollDice);
		if (r != null) {
			let result = [];
			r.forEach(row => {
				let [t, error, range, code, text, finalroll, rollstrings] = row;
				if (error != null) result.push(slashcommands.localizedString(locale, error, [t]));
				else {
					result = result.concat(rollstrings);
					result.push(tables_formatted_result(t, finalroll, range, code, text));
				}
			});
			return result;
		}

		if (lookuptables.tables_accessible(tableName, userID, config.hostUser, usersLM(userID), 'read') == false) return slashcommands.localizedString(locale,'table-notexists',[shortname]);

		let [error, range, code, text, finalroll, rollstrings] = lookuptables.tables_roll(tableName, locale, modifier, feat, !natural, torRollDice);
		if (error != null) return slashcommands.localizedString(locale, error, [tableName]);
		rollstrings.push(tables_formatted_result(tableName, finalroll, range, code, text));
		if (tableName == 'journeytarget' && mySelectedGame(userID)) {
			// add to rollstrings a list of people in that role
			let peopleInRole = charactersInJourneyRole(mySelectedGame(userID), code);
			if (peopleInRole.length == 0) rollstrings.push(slashcommands.localizedString(locale, 'role-listempty', [code]));
			else rollstrings.push(slashcommands.localizedString(locale, 'role-list', [code, peopleInRole.join(', ')]));
		}
		return rollstrings;
	}

	if (lookuptables.tables_accessible(tableName, userID, config.hostUser, usersLM(userID), 'read') == false) return slashcommands.localizedString(locale,'table-notexists',[shortname]);

	if (cmd == 'lookup') { // Look up a row from a table
		let [error, range, code, text] = lookuptables.tables_result(tableName, roll);
		if (error != null) return slashcommands.localizedString(locale, error, [roll]);
		return tables_formatted_result(tableName, roll, range, code, text);
	}

	if (cmd == 'name' || cmd == 'notes' || cmd == 'tags' || cmd == 'rolltype') { // Set various things used by the table
		let r = lookuptables.tables_edit(tableName, userID, config.hostUser, locale, cmd, value);
		if (r != '') return slashcommands.localizedString(locale, r, [value]) +
			((r == 'error-unknowntag') ? '\n' + slashcommands.localizedString(locale, 'error-tagsknown', [lookuptables.tables_tag_list().join(', ')]) : '');
		return slashcommands.localizedString(locale, 'table-changed', [tableName, cmd, value]);
	}

	if (cmd == 'add') { // Add a row to the table
		//console.log(tableName +','+ userID+','+ config.hostUser+','+ locale+','+ range+','+ text);
		let [r1, r2] = lookuptables.tables_add_row(tableName, userID, config.hostUser, locale, range, text);
		if (r1 != '') return ':warning: ' + slashcommands.localizedString(locale, r1, [tableName, range, text]);
		return slashcommands.localizedString(locale, 'table-added', [tableName, range]) + '\n' + r2;
	}

	if (cmd == 'remove') { // Remove a row from the table
		let [r1, r2] = lookuptables.tables_remove_row(tableName, userID, config.hostUser, locale, range);
		if (r1 != '') return ':warning: ' + slashcommands.localizedString(locale, r1, [range]);
		return slashcommands.localizedString(locale, 'table-removed', [tableName, range]) + '\n' + r2;
	}

	if (cmd == 'clear') { // Clear all rows in this table
		if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'table') + ' ' + slashcommands.localizedSubcommand(locale, 'table', 'clear'), null, data, 'tableclear');
		let r = lookuptables.tables_clear_rows(tableName, userID, config.hostUser);
		if (r != '') return ':warning: ' + slashcommands.localizedString(locale, 'error', r, []);
		return slashcommands.localizedString(locale, 'table-cleared', [tableName]);
	}

	if (cmd == 'publish') { // Should this table be published so all other Narvi users can use it?
		let r = lookuptables.tables_edit(tableName, userID, config.hostUser, locale, 'published', value);
		if (r != '') return ':warning: ' + slashcommands.localizedString(locale, r, [lookuptables.tables_validation(tableName, locale)]);
		return slashcommands.localizedString(locale, (value ? 'table-published' : 'table-unpublished'), [tableName]);
	}

	if (cmd == 'delete') { // Delete this table permanently
		if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'table') + ' ' + slashcommands.localizedSubcommand(locale, 'table', 'delete'), null, data, 'tabledelete');
		let r = lookuptables.tables_delete(tableName, userID, config.hostUser);
		if (r != '') return slashcommands.localizedString(locale, r, []);
		setUserConfig(userID,'currentTable', '');
		return slashcommands.localizedString(locale, 'table-deleted', [tableName]);
	}
}

// formats the table roll result nicely
function tables_formatted_result(tableName, roll, range, code, text) {
	return '__' + tableName + '__: `' + roll + '`' + (roll != range ? ' (' + range + ')' : '') + ' **' + text + '**';
}

// hex flowers
function hexflowerCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);

	let cmd = data.options[0].name;

	// parse options
	let shortname = null, tag = null, searchtext = null, mine = null, published = null, hex = null,
		description = '', value = null, text = null, image = '', confirm = null;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'shortname') shortname = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'searchtext') searchtext = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'mine') mine = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'published') published = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'value') value = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'description') description = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'hex') hex = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'text') text = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'image') image = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'confirm') confirm = data.options[0].options[i].value;
	}

	let hexflowerName = getUserConfig(userID, 'currentHexflower');

	if (cmd == 'list') { // List available tables
		return hexflower.list(userID, config.hostUser, locale, mine, published, searchtext);
	}

	if (cmd == 'create') { // Create a hexflower
		shortname = stringutil.sanitizeString(shortname);
		if (!shortname.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [shortname]);
		if (!hexflower.create(shortname,userID, description)) return slashcommands.localizedString(locale,'hexflower-exists',[shortname]);
		// set the currentHexflower to this name
		setUserConfig(userID,'currentHexflower', stringutil.lowerCase(shortname));
		return slashcommands.localizedString(locale, 'hexflower-created', [stringutil.lowerCase(shortname)]);
	}

	if (cmd == 'select') { // Select an existing hexflower to edit or roll on
		if (hexflower.accessible(shortname, userID, config.hostUser, 'read') == false) return slashcommands.localizedString(locale,'hexflower-notexists',[shortname]);
		setUserConfig(userID,'currentHexflower', stringutil.lowerCase(shortname));
		return slashcommands.localizedString(locale,'hexflower-selected', [stringutil.lowerCase(shortname)]);
	}

	if (shortname != null) hexflowerName = shortname;
	if (hexflowerName == null || hexflowerName == '') return slashcommands.localizedString(locale,'hexflower-notselected',[]);
	if (hexflower.accessible(hexflowerName, userID, config.hostUser, 'read') == false) return slashcommands.localizedString(locale,'hexflower-notexists',[shortname]);

	// hex
	if (cmd == 'hex') { // Set various things used by the table
		if (image != '' && (!image.startsWith('http://') && !image.startsWith('https://' ))) return slashcommands.localizedString(locale, 'error-badurl', [val]);
		let r = hexflower.hex(hexflowerName, userID, config.hostUser, locale, hex, text, image);
		if (r != '') return slashcommands.localizedString(locale, r, [text]);
		return slashcommands.localizedString(locale, 'hexflower-added', [hexflowerName, hex, text]);
	}

	// show
	if (cmd == 'show') { // Show an existing hexflower to edit or roll on
		return hexflower.show(hexflowerName, locale, userID, config.hostUser, userNameFromID, (g ? g.hexflowers[hexflowerName] : null));
	}

	if (cmd == 'publish') { // Should this hex flower be published so all other Narvi users can use it?
		let r = hexflower.publish(hexflowerName, userID, config.hostUser, locale, value);
		if (r != '') return r;
		return slashcommands.localizedString(locale, (value ? 'hexflower-published' : 'hexflower-unpublished'), [hexflowerName]);
	}

	if (cmd == 'delete') { // Delete this hex flower permanently
		if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'hexflower') + ' ' + slashcommands.localizedSubcommand(locale, 'hexflower', 'delete'), null, data, 'hexflowerdelete');
		let r = hexflower.del(hexflowerName, userID, config.hostUser);
		if (r != '') return slashcommands.localizedString(locale, r, []);
		setUserConfig(userID,'currentHexflower', '');
		return slashcommands.localizedString(locale, 'hexflower-deleted', [hexflowerName]);
	}

	// all remaining require a game
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);

	// current, next, set
	if (cmd == 'set' && !isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
	if (cmd == 'set' || cmd == 'current' || cmd == 'next') {
		let rollLine = '';
		if (g.hexflowers[hexflowerName] == undefined) g.hexflowers[hexflowerName] = 9;
		if (cmd == 'set' && Number(data.options[0].options[0].value) >= 0 && Number(data.options[0].options[0].value) <= 18) {
			g.hexflowers[hexflowerName] = Number(data.options[0].options[0].value);
		} else if (cmd == 'next') {
			let roll, r, x;
			[roll, rollLine, r, x] = regdice.polyhedralDieRoll(['2d6'], '', '', locale);
			g.hexflowers[hexflowerName] = hexflower.nextHex(g.hexflowers[hexflowerName], roll);
		}
		filehandling.saveGameData(gameData);
		return [rollLine, hexflower.showHex(hexflowerName, g.hexflowers[hexflowerName],stringutil.titleCase(getUserConfig(userID,'selectedGame')))];
	}

}

// JOURNEY COMMANDS -----------------------------------------------------------

const journeyRoleSkills = {
	'guide':'travel',
	'scout':'explore',
	'hunter':'hunting',
	'delver':'craft',
	'lookout':'awareness',
	'absent':''
}

// journey role tracking
function roleCommand(userID, channelID, userName, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID), role = null, multiple = false;
	// parse options
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'role') role = data.options[i].value;
		if (data.options[i].name == 'multiple') multiple = data.options[i].value;
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[i].value);
		}
	}
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);

	if (g.characters[charID].journeyRole == null || g.characters[charID].journeyRole.length == 0) g.characters[charID].journeyRole = ['absent'];
	if (typeof g.characters[charID].journeyRole == 'string') {
		let oldRole = g.characters[charID].journeyRole;
		g.characters[charID].journeyRole = [oldRole];
	}

	if (role != null) {
		// we're setting a role
		role = stringutil.lowerCase(role);
		if (multiple && role != 'absent') {
			// complex version, toggle the role
			if (g.characters[charID].journeyRole.includes(role)) {
				// remove it
				g.characters[charID].journeyRole = g.characters[charID].journeyRole.filter(x => x != role);
				// if this leaves a blank list, set absent
				if (g.characters[charID].journeyRole.length == 0) g.characters[charID].journeyRole = ['absent'];
			} else {
				// if absent was present, remove it
				if (g.characters[charID].journeyRole.includes('absent')) g.characters[charID].journeyRole = g.characters[charID].journeyRole.filter(x => x != 'absent');
				// add it
				g.characters[charID].journeyRole.push(role);
			}
		} else {
			// simple version, just set a single role
			g.characters[charID].journeyRole = [role];
		}
		filehandling.saveGameData(gameData);
	}

	let r = '';
	g.characters[charID].journeyRole.forEach(role => {
		let skillName = journeyRoleSkills[stringutil.lowerCase(role)];
		if (skillName == '') skillName = slashcommands.localizedString(locale, 'none',[]);
		else skillName = stringutil.makeSmallCaps(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'skill-' + skillName,[])));

		r += slashcommands.localizedString(locale, 'journey-role', [
			characterDataFetchHandleNulls(g, charID, 'name', null),
			stringutil.titleCaseWords(slashcommands.localizedString(locale, 'role-' + stringutil.lowerCase(role), [])),
			skillName
			]) + '\n';
	});
	return r;
}

// given a game and a role, return a list of player heroes in that role
function charactersInJourneyRole(g, role) {
	let peopleInRole = [];
	role = stringutil.lowerCase(role);
	for (let p in g.characters) {
		if (!g.characters[p].active || g.characters[p].dreaming) continue;
		if (g.edition == 'strider' || g.characters[p].journeyRole.includes(role)) peopleInRole.push(characterDataFetchHandleNulls(g, p, 'name', null));
	}
	return peopleInRole;
}

// editing and creating journeys
function jeditCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);

	let cmd = data.options[0].name;

	// parse options
	let shortname = null, from = null, first = null, second = null, value = null, length = null, type = null,
		terrain = null, peril = 0, comment = '', step = null, confirm = null;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'shortname') shortname = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'from') from = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'first') first = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'second') second = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'value') value = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'length') length = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'type') type = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'terrain') terrain = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'peril') peril = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'comment') comment = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'step') step = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'confirm') confirm = data.options[0].options[i].value;
	}

	let journeyName = getUserConfig(userID, 'jeditName');

	if (cmd == 'select') { // Select an existing journey to edit
		journeyName = stringutil.lowerCase(shortname);
		if (!journeys.journeys_accessible(journeyName, userID, config.hostUser, usersLM(userID), 'read')) return slashcommands.localizedString(locale, 'journey-notexists', [journeyName]);
		setUserConfig(userID, 'jeditName', journeyName);
		return slashcommands.localizedString(locale, 'journey-selected', [journeyName]);
	}

	if (cmd == 'create') { // Create a journey
		journeyName = stringutil.mildSanitizeString(stringutil.lowerCase(shortname));
		if (!journeyName.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [journeyName]);
		if (!journeys.journeys_create(journeyName,userID)) return slashcommands.localizedString(locale, 'journey-exists', [journeyName]);
		// if you are the LM of your currently selected game, add this shortname to the game's .journey.journeys
		if (g && isPlayerLoremasterOfGame(userID, g)) {
			g.journey.journeys.push(journeyName);
			filehandling.saveGameData(gameData);
		}
		setUserConfig(userID, 'jeditName', journeyName);
		return slashcommands.localizedString(locale, 'journey-created', [journeyName]);
	}

	if (cmd == 'copy' || cmd == 'reverse') { // Create a journey by copying and possibly reversing the order of steps in an existing one
		if (!journeys.journeys_accessible(from, userID, config.hostUser, usersLM(userID), 'read')) return slashcommands.localizedString(locale, 'journey-notexists', [from]);
		let newJourneyName = stringutil.mildSanitizeString(shortname);
		let r = journeys.journeys_copy(from, newJourneyName, userID, locale, cmd == 'reverse');
		if (r != '') return slashcommands.localizedString(locale, r, [newJourneyName]);
		if (g && isPlayerLoremasterOfGame(userID, g)) {
			g.journey.journeys.push(newJourneyName);
			filehandling.saveGameData(gameData);
		}
		setUserConfig(userID, 'jeditName', newJourneyName);
		return slashcommands.localizedString(locale, ((cmd == 'reverse') ? 'journey-reversed' : 'journey-copied'), [newJourneyName, from]);
	}

	if (cmd == 'combine') { // Create a journey by combining two existing ones
		if (!journeys.journeys_accessible(first, userID, config.hostUser, usersLM(userID), 'read')) return slashcommands.localizedString(locale, 'journey-notexists', [first]);
		if (!journeys.journeys_accessible(second, userID, config.hostUser, usersLM(userID), 'read')) return slashcommands.localizedString(locale, 'journey-notexists', [second]);
		let newJourneyName = stringutil.mildSanitizeString(shortname);
		let r = journeys.journeys_combine(first, second, newJourneyName, userID, locale);
		if (r != '') return slashcommands.localizedString(locale, r, [newJourneyName]);
		if (g && isPlayerLoremasterOfGame(userID, g)) {
			g.journey.journeys.push(newJourneyName);
			filehandling.saveGameData(gameData);
		}
		setUserConfig(userID, 'jeditName', newJourneyName);
		return slashcommands.localizedString(locale, 'journey-combined', [newJourneyName, first, second]);
	}

	if (journeyName == '' || !journeys.journeys_accessible(journeyName, userID, config.hostUser, usersLM(userID), 'read')) {
		return slashcommands.localizedString(locale, 'journey-noneselected', []);
	}

	if (cmd == 'show') { // Show the currently selected journey
		return journeys.journeys_show(journeyName, locale, userID, config.hostUser, userNameFromID);
	}

	if (!journeys.journeys_accessible(journeyName, userID, config.hostUser, usersLM(userID), 'write')) return slashcommands.localizedString(locale, 'journey-notexists', [journeyName]);

	if (cmd == 'name' || cmd == 'notes' || cmd == 'origin' || cmd == 'destination' || cmd == 'eventtype' || cmd == 'image' || cmd == 'tags') { // Set some value for a journey
		if (journey_active_anywhere(journeyName, null)) return slashcommands.localizedString(locale, 'journey-inuse',[journeyName]);
		let r = journeys.journeys_edit(journeyName, userID, config.hostUser, cmd, value);
		if (r != '') return slashcommands.localizedString(locale, r, [value]) +
			((r == 'error-unknowntag') ? slashcommands.localizedString(locale, 'error-tagsknown', [journeys.journeys_tag_list().join(', ')]) : '');
		return slashcommands.localizedString(locale, 'journey-changed', [journeyName, cmd, value]);
	}

	if (cmd == 'add') { // Add a step (a number of hexes of the same type) to the journey
		let r = journeys.journeys_add_step(journeyName, userID, config.hostUser, length, type, terrain, peril, comment, false);
		if (!isNaN(r)) {
			return slashcommands.localizedString(locale, 'journey-added', [journeyName, r]) + '\n';
		} else {
			return slashcommands.localizedString(locale, r, [journeyName]);
		}
	}

	if (cmd == 'steps') { // Show the steps defined so far for this journey
		return journeys.journeys_show_steps(journeyName, userID, config.hostUser, locale);
	}

	if (cmd == 'remove') { // Remove a step in this journey
		let r = journeys.journeys_remove_step(journeyName, userID, config.hostUser, step);
		if (!isNaN(r)) {
			return slashcommands.localizedString(locale, 'journey-removedstep', [journeyName, r]) + '\n';
		} else {
			return slashcommands.localizedString(locale, r, [step, journeyName]);
		}
	}

	if (cmd == 'clear') { // Clear all steps in this journey
		if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'jedit') + ' ' + slashcommands.localizedSubcommand(locale, 'jedit', 'clear'), null, data, 'journeyclear');
		let r = journeys.journeys_clear_steps(journeyName, userID, config.hostUser);
		if (r != '') return slashcommands.localizedString(locale, r, []);
		return slashcommands.localizedString(locale, 'journey-cleared', [journeyName]);
	}

	if (cmd == 'publish') { // Should this journey be published so all other Narvi users can use it?
		if (!value && journey_in_any_game(journeyName,getUserConfig(userID,'selectedGame'))) { 
			return slashcommands.localizedString(locale, 'journey-inuse',[journeyName]);
		}
		let r = journeys.journeys_edit(journeyName, userID, config.hostUser, 'published', value);
		if (r != '') return slashcommands.localizedString(locale, r, [value]);
		return slashcommands.localizedString(locale, (value ? 'journey-published' : 'journey-unpublished'), [journeyName]);	
	}

	if (cmd == 'delete') { // Delete this journey permanently
		if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'jedit') + ' ' + slashcommands.localizedSubcommand(locale, 'jedit', 'delete'), null, data, 'journeydelete');
		if (journey_active_anywhere(journeyName, null)) return slashcommands.localizedString(locale, 'journey-inuse',[journeyName]);
		let r = journeys.journeys_delete(journeyName, userID, config.hostUser);
		if (r != '') return slashcommands.localizedString(locale, r, []);
		// if you are the LM of your currently selected game, remove this shortname from the game's .journey.journeys
		if (g && isPlayerLoremasterOfGame(userID, g)) {
			g.journey.journeys = g.journey.journeys.filter(x => (x != journeyName));
			if (g.journey.active == journeyName) {
				g.journey.active = '';
				g.songUsed = '';
				g.songBonus = 0;
				g.songCharacters = [];
				clearUnweary(g);
			}
			filehandling.saveGameData(gameData);
		}
		setUserConfig(userID, 'jeditName', '');
		return slashcommands.localizedString(locale, 'journey-deleted', [journeyName]);
	}
}

// running journeys
function journeyCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);

	let cmd = data.options[0].name;

	// parse options
	let tag = null, searchtext = null, mine = null, published = null, shortname = null, season = 'summer',
		type = '', length = null, terrain = null, peril = 0, modifier = 0, forced = false, charID = null,
		successes = 1, deleteIt = false, eventtype = null, unmounted = false;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'tag') tag = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'searchtext') searchtext = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'mine') mine = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'published') published = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'shortname') shortname = stringutil.lowerCase(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'eventtype') eventtype = stringutil.lowerCase(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'season') season = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'type') type = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'modifier') modifier = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'successes') successes = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'forced') forced = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'unmounted') unmounted = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'delete') deleteIt = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[0].options[i].value);
		}
		if (data.options[0].options[i].name == 'length') length = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'terrain') terrain = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'peril') peril = Number(data.options[0].options[i].value);
	}

	if (cmd == 'list') { // List available journeys
		return journeys.journeys_list(userID, config.hostUser, usersLM(userID), locale, mine, published, searchtext, tag);
	}

	// verify a game -- all other commands require this
	if (!g) return slashcommands.localizedString(locale, 'error-nogame', []);

	if (cmd == 'use') { // Add this journey to your current game so you can run it in this game
		if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
		if (g.journey.journeys.includes(shortname)) return slashcommands.localizedString(locale, 'journey-alreadyingame', [shortname]);
		if (!journeys.journeys_accessible(shortname, userID, config.hostUser, usersLM(userID), 'read')) return slashcommands.localizedString(locale, 'journey-notexists',[shortname]);
		g.journey.journeys.push(shortname);
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'journey-used',[shortname]);
	}

	if (cmd == 'remove') { // Remove this journey from your current game
		if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
		if (!g.journey.journeys.includes(shortname)) return slashcommands.localizedString(locale, 'journey-notingame', [shortname]);
		g.journey.journeys = g.journey.journeys.filter(x => (x != shortname));
		filehandling.saveGameData(gameData);
		journey_orphaned(shortname);
		return slashcommands.localizedString(locale, 'journey-removed',[shortname]);
	}

	if (cmd == 'start') { // Start a journey in your current game
		// make sure you have a game, and you're the LM
		if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
		// make sure there isn't already an active journey
		if (g.journey.active != '') {
			return slashcommands.localizedString(locale, 'journey-active', [g.journey.active]);
		}
		// make sure this journey is in your game
		if (!g.journey.journeys.includes(shortname)) {
			return slashcommands.localizedString(locale, 'journey-notingame', [shortname]);
		}
		if (!journeys.journeys_accessible(shortname, userID, config.hostUser, usersLM(userID), 'read')) return slashcommands.localizedString(locale, 'journey-notexists', [shortname]);
		if (journeys.journeys_count_steps(shortname) <= 0) {
			return slashcommands.localizedString(locale, 'journey-nosteps', [shortname]);
		}
		// choose an events type table
		if (eventtype == null) {
			eventtype = journeys.journeys_lookup(shortname, 'eventtype');
			if (isStriderMode(g) && eventtype == 'journeyevents') eventtype = 'solojourney';
		}
		if (lookuptables.tables_accessible(eventtype, userID, config.hostUser, usersLM(userID), 'read') == false) return slashcommands.localizedString(locale,'table-notexists',[eventtype]);
		if (lookuptables.tables_rolltype(eventtype, locale) != 'feat') return slashcommands.localizedString(locale, 'journey-badeventtable', [shortname]);
		// show the picture
		if (journeys.journeys_image(shortname) != '') botSendMessage(channelID, journeys.journeys_image(shortname));
		// sets active, forced, season, progress, log, undo, peril, event=marching
		g.journey.active = shortname;
		g.journey.eventtype = eventtype;
		g.journey.progress = 0;
		g.journey.duration = 0;
		g.journey.forced = forced;
		g.journey.season = season;
		g.journey.log = '';
		g.journey.peril = -1; // what if the first step is a peril? well, the next function will get it
		g.journey.event = 'marching';
		g.journey.skill = 'travel';
		g.journey.undo = '';
		g.journey.favourable = false;
		g.journey.pendingPendingFatigue = {};
		g.journey.typeoverride = type;
		g.journey.underground = journeys.journeys_has_tag(shortname, 'underground');
		g.journey.unmounted = unmounted;
		g.journey.featmod = modifier;
		for (let p in g.characters) {
			g.characters[p].pendingFatigue = 0;
		}
		journey_log(g, locale, 'start', [shortname]);
		return journeys.journeys_next(g, locale, false);
	}

	if (cmd == 'quick') { // Create and start a quick journey in your current game
		// make sure you have a game, and you're the LM
		if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
		// make sure there isn't already an active journey
		if (g.journey.active != '') {
			return slashcommands.localizedString(locale, 'journey-active', [g.journey.active]);
		}
		if (eventtype == null) {
			if (type == 'moria') {
				eventtype = 'moriajourneyevents';
			} else {
				if (isStriderMode(g)) eventtype = 'solojourney';
				else eventtype = 'journeyevents';
			}
		}
		if (lookuptables.tables_accessible(eventtype, userID, config.hostUser, usersLM(userID), 'read') == false) return slashcommands.localizedString(locale,'table-notexists',[eventtype]);
		if (lookuptables.tables_rolltype(eventtype, locale) != 'feat') return slashcommands.localizedString(locale, 'journey-badeventtable', [shortname]);
		journeyName = journeys.journeys_create_quick(userID, length, type, terrain, peril, eventtype);
		g.journey.journeys.push(journeyName);
		filehandling.saveGameData(gameData);
		setUserConfig(userID, 'jeditName', journeyName);
		// sets active, forced, season, progress, log, undo, peril, event=marching
		g.journey.active = journeyName;
		g.journey.eventtype = eventtype;
		g.journey.progress = 0;
		g.journey.duration = 0;
		g.journey.forced = forced;
		g.journey.season = season;
		g.journey.log = '';
		g.journey.peril = -1; // what if the first step is a peril? well, the next function will get it
		g.journey.event = 'marching';
		g.journey.skill = 'travel';
		g.journey.undo = '';
		g.journey.underground = false;
		g.journey.unmounted = unmounted;
		g.journey.favourable = false;
		g.journey.pendingPendingFatigue = {};
		g.journey.typeoverride = type;
		g.journey.featmod = modifier;
		for (let p in g.characters) {
			g.characters[p].pendingFatigue = 0;
		}
		journey_log(g, locale, 'start', [journeyName]);
		return journeys.journeys_next(g, locale, false);
	}

	if (cmd == 'log') { // Show a log of the current (or most recent) journey
		if (g.journey.log == '') return slashcommands.localizedString(locale, 'journey-inactive', []);
		return g.journey.log;
	}

	if (cmd == 'undo') { // Undo the last action in the current journey
		if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
		// should work even if the journey isn't active, because we might be undoing whatever ended it
		if (g.journey.undo == '') return slashcommands.localizedString(locale, 'journey-nothingtoundo', []);
		// if the journey no longer exists we can't undo
		if (!journeys.journeys_accessible(g.journey.undo.active, userID, config.hostUser, usersLM(userID), 'read')) return slashcommands.localizedString(locale, 'journey-notexists', [g.journey.undo.active]);
		// restores the variables saved previously
		g.journey.active = g.journey.undo.active;
		g.journey.progress = g.journey.undo.progress;
		g.journey.duration = g.journey.undo.duration;
		g.journey.peril = g.journey.undo.peril;
		g.journey.event = g.journey.undo.event;
		g.journey.favourable = g.journey.undo.favourable;
		g.journey.skill = g.journey.undo.skill;
		g.journey.pendingPendingFatigue = g.journey.undo.pendingPendingFatigue;
		for (let p in g.journey.undo.pendingFatigue) {
			g.characters[p].pendingFatigue = g.journey.undo.pendingFatigue[p];
		}
		for (let p in g.journey.undo.hopes) {
			characterDataUpdate(g, p, 'hope', null, '=', g.journey.undo.hopes[p], 0, 'maxhope', channelID);
		}
		// sets undo back to '' because there's nothing else to undo right now
		g.journey.undo = '';
		filehandling.saveGameData(gameData);
		journey_log(g, locale, 'undone', []);
		// reports what happened, then the new current status
		return slashcommands.localizedString(locale, 'journey-log-undone', []) + '\n' + journeys.journeys_status(g, locale);
	}

	// verify a started journey -- all other commands require this
	if (g.journey.active == '') {
		return slashcommands.localizedString(locale, 'journey-inactive', []);
	}

	if (cmd == 'next') { // Show what has to happen next
		return journeys.journeys_next(g, locale, true);
	}

	if (cmd == 'show') { // Show the status of the journey and what has to happen next
		return journeys.journeys_show(g.journey.active, locale, userID, config.hostUser, userNameFromID);
	}

	// everything here down is LM-only
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	if (cmd == 'pass' || cmd == 'fail') { // Record a passing or failing test in the current journey
		if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);
		
		// identify number of successes
		if (cmd == 'fail') successes = 0;

		r = journeys.journeys_apply_roll(g, charID, characterDataFetchHandleNulls(g, charID, 'name', null), successes, locale, channelID);
		filehandling.saveGameData(gameData);
		return r;
	}

	if (cmd == 'reset') { // Reset the current journey back to the beginning
		g.journey.progress = 0;
		g.journey.duration = 0;
		g.journey.peril = -1; // what if the first step is a peril? well, the next function will get it
		g.journey.event = 'marching';
		g.journey.skill = 'travel';
		g.journey.favourable = false;
		g.journey.undo = '';
		g.journey.pendingPendingFatigue = {};
		for (let p in g.characters) {
			g.characters[p].pendingFatigue = 0;
		}
		journey_log(g, locale, 'reset', [g.journey.active]);
		filehandling.saveGameData(gameData);
		return journeys.journeys_next(g, locale, false);
	}

	if (cmd == 'abort' || cmd == 'end') { // End the journey early
		shortname = g.journey.active;
		r = journeys.journeys_end(g, slashcommands.localizedString(locale,'loremaster',[]), locale, cmd == 'abort', channelID);
		// if delete, do that too
		if (deleteIt && journeys.journeys_accessible(shortname, userID, config.hostUser, usersLM(userID), 'write')) {
			let r2 = journeys.journeys_delete(shortname, userID, config.hostUser);
			if (r2 != '') r += slashcommands.localizedString(locale, r2, []);
			else {
				g.journey.journeys = g.journey.journeys.filter(x => (x != shortname));
				r = [r, slashcommands.localizedString(locale,'journey-deleted', [shortname])];
			}
		}
		filehandling.saveGameData(gameData);
		return r;
	}
}

// called whenever a journey might be an orphan and need deleting
function journey_orphaned(journeyName) {
	// a published journey is never deleted as an orphan
	if (journeys.journeys_lookup(journeyName,'published')) return;
	// see if any game has it
	if (journey_in_any_game(journeyName, '')) return;
	// if it's not published or attached to any game, delete it as an orphan
	journeys.journeys_delete_immediately(journeyName);
}

// check if a journey is associated with any game (possibly excluding a particular one)
function journey_in_any_game(journeyName, exceptGame) {
	for (g in gameData) {
		if (g != exceptGame && gameData[g].journey.journeys.includes(journeyName)) return true;
	}
	return false;
}

// check if a journey is associated with any game (possibly excluding a particular one)
function journey_active_anywhere(journeyName, exceptGame) {
	for (g in gameData) {
		if (g != exceptGame && gameData[g].journey.active == journeyName) return true;
	}
	return false;
}

// add something to the journey log
function journey_log(g, locale, event, params) {
	g.journey.log += '<t:' + String(Math.floor(Date.now()/1000)) + ':d> ';
	g.journey.log += '<t:' + String(Math.floor(Date.now()/1000)) + ':T>: ';
	g.journey.log += slashcommands.localizedString(locale,  'journey-log-' + event, params) + '\n';
	filehandling.saveGameData(gameData);
}

// add something to the combat log
function combat_log(g, s) {
	g.combat.log += '<t:' + String(Math.floor(Date.now()/1000)) + ':d> ';
	g.combat.log += '<t:' + String(Math.floor(Date.now()/1000)) + ':T>: ';
	s = stringutil.stringWithoutNewlines(s).trim();
	if (s.endsWith(';')) s = s.slice(0, -1);
	g.combat.log += s + '\n';
	filehandling.saveGameData(gameData);
}

function journeyActive(g) {
	return g.journey.active != '';
}

// POOL COMMAND ---------------------------------------------------------------

function showPool(g, locale) {
	return emoji.showStatusLineWithMax(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-pool', [])), g.pool, g.maxpool, 'pool', false);
}

function poolDistributeAutomatic(g, locale, userID, channelID, reset) {
	// make a list of characters who are not at full hope, and how many they are down by, and sort by highest number
	let needingHope = [], r = '';
	for (let p2 in g.characters) {
		if (characterDataFetchHandleNumbers(g, p2, 'hope', null) < characterDataFetchHandleNumbers(g, p2, 'maxhope', null) && g.characters[p2].active) {
			let need = characterDataFetchHandleNumbers(g, p2, 'maxhope', null) - characterDataFetchHandleNumbers(g, p2, 'hope', null);
			if (need > 0 && hasTrait(g, p2, 'virtue', 'the art of smoking')) need--;
			let atZero = 'a';
			if (characterDataFetchHandleNumbers(g, p2, 'hope', null) == 0) atZero = 'b'; // prioritize anyone at zero hope
			needingHope.push(stringutil.padLeft(String(need),3,'0') + ' ' + atZero + ' ' + p2);
		}
	}
	// while there is still pool to distribute, and someone needing some
	let gotHope = {};
	let p;
	while (g.pool > 0 && needingHope.length > 0) {
		// sort by need
		needingHope = needingHope.sort();
		// give one hope to whoever has the highest need
		p = needingHope[needingHope.length - 1];
		let need = p.split(' ')[0];
		p = p.split(' ')[2];
		// remove one pool
		g.pool = Number(g.pool) - 1;
		// update that needHope entry
		if (need <= 1) {
			// they're full; remove them from the array entirely
			needingHope.splice(needingHope.length - 1,1);
		} else {
			// update their need level
			needingHope[needingHope.length - 1] = stringutil.padLeft(String(need-1),3,'0') + ' a ' + p;
		}
		if (gotHope[p] == null)	{
			gotHope[p] = 1;
		} else {
			gotHope[p] = gotHope[p] + 1;
		}
	}
	// save the file
	if (reset) g.pool = g.maxpool;
	filehandling.saveGameData(gameData);
	// report the results
	r += slashcommands.localizedString(locale, 'pool-distributed', []) + ': ';
	for (let p2 in gotHope)	{
		let gain = gotHope[p2];
		if (gain > 0 && hasTrait(g, p2, 'virtue', 'the art of smoking')) gain++;
		characterDataUpdate(g, p2, 'hope', null, '+', gain, 0, 'maxhope', channelID);
		r += characterDataFetchHandleNulls(g, p2, 'name', null) + ': ' + gain + ', ';
	}
	r = r.slice(0,-2) + '.\n';
	// append a hope report
	return r + reports.tokenReport(locale, g, 'hope');
}

function poolDistributeManually(g, locale, args, userID, channelID, reset) {
	let gotHope = {}, r = '';
	console.log('TRACE: /pool distribute with args: ' + args.join(' '));
	args.forEach(arg => {
		if (g.pool > 0) {
			let a = arg.split('=');
			// identify the amount (default is 1)
			let hopeGain;
			if (a.length == 1) {
				hopeGain = 1;
			} else if (!isNaN(a[1])) {
				hopeGain = Number(a[1]);
			} else {
				return slashcommands.localizedString(locale, 'error-numeric', [a[1]]);
			}
			console.log('  trying to give ' + hopeGain);
			if (hopeGain > Number(g.pool)) {
				hopeGain = Number(g.pool);
				console.log('    reduced because of pool running out; now giving ' + hopeGain);
			}
			// identify the player record
			let p = findCharacterInGame(g, a[0]);
			// transfer the hope
			let cur = characterDataFetchHandleNumbers(g, p, 'hope', null), max = characterDataFetchHandleNumbers(g, p, 'maxhope', null);
			console.log('    target of the gain is ' + characterDataFetchHandleNulls(g, p, 'name', null) + ' cur=' + cur + ' max=' + max);
			if (hopeGain > (max - cur)) {
				hopeGain = max - cur;
				console.log('    reduced to ' + hopeGain + ' to avoid going over max of ' + max);
			}
			if (hopeGain > 0) {
				let gain = hopeGain;
				console.log('gain is originally ' + gain);
				if (gain > 0 && hasTrait(g, p, 'virtue', 'the art of smoking')) gain++;
				console.log('gain is now ' + gain);
				characterDataUpdate(g, p, 'hope', null, '+', gain, 0, 'maxhope', channelID);
				console.log('    after the update, new value is ' + characterDataFetchHandleNumbers(g, p, 'hope', null));
				g.pool -= hopeGain;
				if (gotHope[p] == null)	{
					gotHope[p] = hopeGain;
					console.log('    adding to the list for display purposes');
				} else {
					gotHope[p] = gotHope[p] + hopeGain;
					console.log('    accumulating to the list for display purposes');
				}
			}
		}
	});
	// save the file
	if (reset) g.pool = g.maxpool;
	filehandling.saveGameData(gameData);
	// return the messages so far plus report hope
	r += slashcommands.localizedString(locale, 'pool-distributed', []) + ': ';
	for (let p2 in gotHope)	{
		r += characterDataFetchHandleNulls(g, p2, 'name', null) + ': ' + gotHope[p2] + ', ';
	}
	r = r.slice(0,-2) + '.\n';
	console.log('  gave out: ' + r);
	// append do a hope report
	return r + reports.tokenReport(locale, g, 'hope');
}

// pool command -- function to set and manipulate the Fellowship Pool
function poolCommand(userID, channelID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let cmd = data.options[0].name;

	// show: show the current pool
	if (cmd == 'show') return showPool(g, locale);

	let points = null, distribution = '', reset = false;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'points') points = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'distribution') distribution = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'reset') reset = data.options[0].options[i].value;
	}

	// use and claim: use up some of the pool, possibly transferring it to your own hope
	if (cmd == 'use' || cmd == 'claim') {
		if (points == '' || points == null) points = 1;
		if (g.pool < points) return slashcommands.localizedString(locale, 'error-poolonly', [g.pool]);
		let r = '';
		if (cmd == 'claim') {
			// make sure I have an active character
			let p = currentCharacter(userID);
			if (p == null) return slashcommands.localizedString(locale, 'error-nocharacter', []);
			// reduce points to the diff with maxhope if necessary
			let hopeNeeded = Number(characterDataFetchHandleNumbers(g, p, 'maxhope', null)) - Number(characterDataFetchHandleNumbers(g, p, 'hope', null));
			if (hopeNeeded == 0) return slashcommands.localizedString(locale, 'error-full', [slashcommands.localizedString(locale, 'token-hope', [])]);
			if (points > hopeNeeded) points = hopeNeeded;
			if (hasTrait(g, p, 'virtue', 'the art of smoking')) points--;
			let gain = points;
			if (gain > 0 && hasTrait(g, p, 'virtue', 'the art of smoking')) gain++;
			characterDataUpdate(g, p, 'hope', null, '+', gain, null, 'maxhope', channelID);
			r = emoji.showStatusLineWithMax(stringutil.titleCase(slashcommands.localizedString(locale, 'token-hope', []), false), characterDataFetchHandleNumbers(g, p, 'hope', null), characterDataFetchHandleNumbers(g, p, 'maxhope', null), 'hope') + '\n';
		}
		g.pool -= points;
		filehandling.saveGameData(gameData);
		return r + showPool(g, locale);
	}

	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// set: set the current pool value
	if (cmd == 'set') {
		if (points != null) g.pool = Number(points);
		filehandling.saveGameData(gameData);
		return showPool(g, locale);
	}
	if (cmd == 'max') {
		if (points != null) g.maxpool = Number(points);
		filehandling.saveGameData(gameData);
		return showPool(g, locale);
	}
	if (cmd == 'reset') {
		g.pool = g.maxpool;
		filehandling.saveGameData(gameData);
		return showPool(g, locale);
	}
	// distribute: distribute the pool to the company manually or automatically
	if (cmd == 'distribute') {
		if (g.pool == 0) return slashcommands.localizedString(locale, 'error-poolonly', [0]);
		if (distribution == '') return poolDistributeAutomatic(g, locale, userID, channelID, reset);
		// DWIM to try to work with other formats, like "Bilbo 2, Frodo 3, Sam 5"
		// change commas to spaces
		distribution = distribution.trim().replace(/,/g,' ');
		// change dashes to equal signs
		distribution = distribution.replace(/-/g,'=');
		// take out spaces around equal signs
		distribution = distribution.replace(/ ?= ?/g,'=');
		// collapse multiple spaces
		distribution = distribution.replace(/  +/g, ' ');
		// put in missing equal signs
		let m = distribution.match(/\w+ \d/);
		while (m) {
			distribution = distribution.substring(0, m.index) + m[0].replace(/ /g,'=') + distribution.substring(m.index + m[0].length);
			m = distribution.match(/\w+ \d/);
		}
		return poolDistributeManually(g, locale, distribution.split(' '), userID, channelID, reset);
	}
}

// COUNTERS -------------------------------------------------------------------

// fetch the appropriate special value (counter or secret)
function getSpecialValueByType(g, secret, name) {
	if (secret) return g.secrets[name];
	return g.counters[name];
}
function getSpecialValue(g, name) {
	if (g.secrets[name] != undefined) return [true, g.secrets[name]];
	return [false, g.counters[name]];
}

// set the appropriate special value (counter or secret)
function setSpecialValue(g, secret, name, value) {
	if (secret) g.secrets[name] = value;
	else g.counters[name] = value;
	return value;
}

function getSpecialLabel(secret,locale) {
	if (secret) return stringutil.titleCase(slashcommands.localizedString(locale, 'secret', []));
	return stringutil.titleCase(slashcommands.localizedCommand(locale, 'counter'));
}

// if secret, send in DM, otherwise simply return
function sendSpecialMessage(secret, userID, g, locale, message, silent) {
	if (secret) {
		botSendLMPrivateMessage(g, userID, g.lmchannel, message);
		if (silent) return '';
		return userLocalizedString(userID, 'answering-privately', []);
	}
	return message;
}

function specialValueList(g, locale, secret) {
	let r = '**';
	if (secret) r += stringutil.titleCase(slashcommands.localizedString(locale, 'secret', [])) + ' ';
	r += stringutil.titleCase(slashcommands.localizedString(locale, 'counter-heading', []));
	r += '**:\n';

	let c = 0;
	for (let valName in (secret ? g.secrets : g.counters)) {
		r += emoji.headingValueAndTokens(valName, getSpecialValueByType(g, secret, valName), '', false) + '\n';
		c++;
	}
	r += slashcommands.localizedString(locale, 'values-count', [c]);
	return r;
}

// special values -- user-defined values by the loremaster
function counterCommand(userID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (g.secrets == null) g.secrets = {};
	if (g.counters == null) g.counters = {};

	let cmd = data.options[0].name;

	let valName = null, change = 0, secret = null;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'name') valName = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'value') change = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'secret') secret = data.options[0].options[i].value;
	}

	// show: show public, then if the loremaster, also show private privately
	if (cmd == 'show') {
		if (isPlayerLoremasterOfGame(userID, g) && Object.keys(g.secrets).length != 0) sendSpecialMessage(true, g.loremaster, g, locale, specialValueList(g, locale, true), true);
		return specialValueList(g, locale, false);
	}

	let r = '';
	valName = stringutil.titleCase(stringutil.lowerCase(valName));
	if (!valName.match(/^[a-zA-Z][a-z0-9]+$/)) return slashcommands.localizedString(locale, 'error-alphanumeric', [valName]);
	let wasSecret, curVal;
	[wasSecret, curVal] = getSpecialValue(g, valName);
	if (secret == null) secret = wasSecret;

	// fail if not loremaster
	if (!isPlayerLoremasterOfGame(userID, g) && (!gameHasConfigFlag(g, 'unlocked-counters') || secret)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// delete 
	if (cmd == 'delete') {
		// if it doesn't exist, abort with an error
		if (curVal == undefined) return slashcommands.localizedString(locale, 'counter-nonexistent', [valName]);

		// delete it
		if (secret) delete g.secrets[valName];
		else delete g.counters[valName];
		filehandling.saveGameData(gameData);

		// return saying it was deleted
		return slashcommands.localizedString(locale, 'counter-deleted', [getSpecialLabel(secret, locale), valName, curVal]);
	}

	// set
	if (cmd == 'set') {
		// if no current value, start at 0
		let newVal = curVal;
		if (curVal == undefined) newVal = 0;

		// if this is a delta, set the new value to the sum, otherwise set to the new value
		//let change = data.options[0].options[1].value;
		if (change.startsWith('+') || change.startsWith('-')) {
			newVal += Number(change);
		} else if (!isNaN(change)) {
			newVal = Number(change);
		} else return slashcommands.localizedString(locale, 'error-numeric', [change]);

		if (curVal == undefined) curVal = 0;
		if (secret == null) secret = false;

		// set the value
		setSpecialValue(g, secret, valName, newVal);
		filehandling.saveGameData(gameData);
		let r = '';

		// handle special interpretation of one named Churn hitting certain values
		if (stringutil.lowerCase(valName) == 'churn') {
			if (curVal < 10 && newVal >= 10) r = '**Churn**: Reached 10! Roll on the __Churn Over__ table (p189)!\n';
			else if (curVal < 20 && newVal >= 20) r = '**Churn**: Reached 20! Roll on the __Churn Over__ table (p189)!\n';
			else if (newVal >= 30) {
				r = '**Churn**: Epic Churn! (p189)! Resetting Churn to 0.\n';
				setSpecialValue(g, secret, valName, 0);
				filehandling.saveGameData(gameData);
			}
		}
		// return private or public
		return r + sendSpecialMessage(secret, userID, g, locale, emoji.headingValueAndTokens(valName + ' ' + slashcommands.localizedString(locale, 'setto', [change]), newVal, '', false), false);
	}
}

// EYE ------------------------------------------------------------------------

const regularRewards = ['close-fitting','closefitting','close fitting','cunning make','cunning-make','cunningmake','reinforced','fell','grievous','keen'];

// Eye of Mordor awareness
function eyeCommand(userID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	let showEye = gameHasConfigFlag(g, 'show-eye');
	if (!isPlayerLoremasterOfGame(userID, g)) {
		if (showEye) return eyeShow(g, true);
		return slashcommands.localizedString(locale, 'error-lmonly', []);
	}

	let cmd = data.options[0].name;
	let r = '';

	if (cmd == 'base') {
		let val = 0;
		if (data.options && data.options[0] && data.options[0].options && data.options[0].options[0] && data.options[0].options[0].value) val = Number(data.options[0].options[0].value);
		else { // do our best guess to calculate the base
			for (let p in g.characters) {
				if (!g.characters[p].active) continue;
				// culture: Dwarves 1, Rangers or Elves 2, High Elves 3
				let thisVal = 0;
				if (isCulture(g, p, 'dwarf') || isCulture(g, p, 'dwarves')) thisVal = 1;
				if (isCulture(g, p, 'elf') || isCulture(g, p, 'ranger')) thisVal = 2;
				if (isCulture(g, p, 'high elf')) thisVal = 3;
				if (thisVal > val) val = thisVal;
			}
			r += ':anger: Eye base starts at ' + val + ' based on cultures of active player-heroes\n';
			for (let p in g.characters) {
				if (!g.characters[p].active) continue;
				let name = characterDataFetchHandleNulls(g, p, 'name', null);
				// valour: +1 for everyone with Valour 4 or more
				if (characterDataFetchHandleNumbers(g, p, 'sheet','valour') >= 4) {
					r += ':anger: Adding 1 because ' + name + ' has high Valour\n';
					val++;
				}
				// famous weapons: +1 for each
				for (weaponName in characterDataFetchHandleNulls(g, p, 'weapons', null)) {
					let weaponArray = characterDataFetchHandleNulls(g, p, 'weapons', weaponName);
					if (weaponArray[4] == 'FALSE') continue;
					let qualities = 0, specialQualities = 0;
					for (let i = 12; i <= 14 ; i++) {
						let wq = stringutil.lowerCase(weaponArray[i]);
						if (wq && wq != '') qualities++;
						if (wq && wq != '' && !regularRewards.includes(wq)) specialQualities++;
					}
					if (qualities > 1 || specialQualities > 0) {
						r += ':anger: Adding 1 for ' + name + '\'s Famous Weapon ' + weaponName + '\n';
						val++;
					}
				}
				// famous armour: +1 for each
				let armour = characterDataFetchHandleNulls(g, p, 'armourqualities', null);
				for (a in armour) {
					let qualities = 0, specialQualities = 0;
					armour[a].forEach(wq => {
						if (wq && wq != '') qualities++;
						if (wq && wq != '' && !regularRewards.includes(wq)) specialQualities++;
					});
					if (qualities > 1 || specialQualities > 0) {
						r += ':anger: Adding 1 for ' + name + '\'s Famous Armour ' + a + '\n';
						val++;
					}
				}
			}
		}
		r += eyeSetValue(g, null, null, val, null);
	} else if (cmd == 'threshold') r += eyeSetValue(g, null, null, null, Number(data.options[0].options[0].value));
	else if (cmd == 'reset') r += eyeSetValue(g, 'reset', null, null, null);
	else if (cmd == 'set') {
		let value = data.options[0].options[0].value;
		if (value.startsWith('+') || value.startsWith('-')) r += eyeSetValue(g, null, Number(value), null, null);
		else if (!isNaN(value)) r += eyeSetValue(g, Number(value), null, null, null);
	}

	r += eyeShow(g, true);

	return sendSpecialMessage(!showEye, userID, g, locale, r, false);
}

// all setting of Eye values pass through this so it can also handle reporting back when an episode happens
function eyeSetValue(g, amount, delta, base, threshold) {

	if (amount == 'reset') {
		g.eye.current = g.eye.base;
	} else if (amount != null) {
		g.eye.current = amount;
	}

	if (delta != null) {
		g.eye.current += delta;
	}

	if (base != null) {
		g.eye.base = base;
		if (g.eye.current < base) g.eye.current = base;
	}

	if (threshold != null) {
		g.eye.threshold = threshold;
	}

	let r = '';
	if (g.eye.base > 0 && g.eye.threshold > 0 && g.eye.current >= g.eye.threshold) {
		r += sendSpecialMessage(!gameHasConfigFlag(g, 'show-eye'), g.loremaster, g, getUserConfig(g.loremaster, 'locale'), userLocalizedString(g.loremaster, 'eye-revelation', []), true);
		if (r != '') r += '\n';
		g.eye.current = g.eye.base;
	}
	filehandling.saveGameData(gameData);
	return r;
}

// show eye status: uses brown, red, and white squares to represent base, current, and up to threshold, with numbers after
function eyeShow(g, always) {
	if (always || gameHasConfigFlag(g, 'show-eye'))	return emoji.eyeDisplay(g.eye.base, g.eye.current, g.eye.threshold) + '\n';
	return '';
}

// increase eye, useful for various things that do that, including in journeys
function increaseEyeRating(g, points) {
	let r = eyeSetValue(g, null, points, null, null);
	r += eyeShow(g, false);
	return r;
}

// TOKEN TRACKING -------------------------------------------------------------

// function to retrieve a key's value for a particular user
function tokenValue(g, charID, token) {
	return characterDataFetchHandleNumbers(g, charID, token, null);
}

// determine if the character is weary
function characterWeary(g, charID, test) {
	if (g != null && charID != null && tokenValue(g, charID, 'endurance') != null && g.characters[charID]) {
		if (g.characters[charID].unweary) return false;
		if (g.characters[charID].weary) return true;
		// if the character never got set up, don't default to weariness
		if (Number(tokenValue(g, charID, 'endurance')) == 0 && Number(tokenValue(g, charID, 'maxendurance')) == 0) return false;
		let net = Number(tokenValue(g, charID, 'endurance')) - Number(tokenValue(g, charID, 'encumbrance')) - Number(tokenValue(g, charID, 'fatigue'));
		if (test == 'arrival') net += g.characters[charID].pendingFatigue;
		if (net <= 0) return true;
	}
	return false;
}

// accrual of shadow can trigger eye gains too
function characterGainShadow(g, charID, amount, channelID) {
	let r = '';
	characterDataUpdate(g, charID, 'shadow', null, '+', amount, 0, null, channelID);
	if (amount > 0 && g && gameHasConfigFlag(g, 'shadow-eye') && !g.combat.active) {
		r += increaseEyeRating(g, amount);
	}
	return r;
}

// determine if the character is miserable
function characterMiserable(g, charID) {
	if (g != null && charID != null && tokenValue(g, charID, 'hope') != null) {
		if (g.characters[charID].miserable) return true;
		// if the character never got set up, don't default to misery
		if (Number(tokenValue(g, charID, 'hope')) == 0 && Number(tokenValue(g, charID, 'maxhope')) == 0) return false;
		let net = Number(tokenValue(g, charID, 'hope')) - Number(tokenValue(g, charID, 'shadow')) - Number(tokenValue(g, charID, 'shadowscars'));
		if (net <= 0) return true;
	}
	return false;
}

// common function for resetting all wounds-related settings
function clearWounds(g, p, channelID) {
	characterDataUpdate(g, p, 'wounded', null, '=', 'FALSE', null, null, channelID);
	characterDataUpdate(g, p, 'woundtreated', null, '=', 'untreated', null, null, channelID);
	characterDataUpdate(g, p, 'wounddaysleft', null, '=', '', null, null, channelID);
}

function hopeStatus(g, p, locale) {
	let r = '';
	if (Number(tokenValue(g, p, 'shadow')) + Number(tokenValue(g, p, 'shadowscars')) >= Number(tokenValue(g, p, 'maxhope'))) r = ' **(' + slashcommands.localizedOption(locale, 'feat', 'feat', 'illfavoured') + ', ' + slashcommands.localizedString(locale, 'condition-miserable', []) + ')**';
	else if (characterMiserable(g,p)) r = ' **(' + slashcommands.localizedString(locale, 'condition-miserable', []) + ')**';
	return r;
}

// a common source for displaying a particular token in context
function showStackedStatusLine(g, p, token, heading, locale, neverhalve) {
	var header;
	if (heading == 'name') {
		header = characterDataFetchHandleNulls(g, p, 'name', '');
	} else if (heading == 'token') {
		header = stringutil.titleCaseWords(token);
	} else {
		header = heading;
	}
	if (heading == 'none') {
		header = '';
	} else {
		header = stringutil.trimRight(header, 15);
	}

	if (token == 'endurance' || token == 'maxendurance') {
		let net = characterDataFetchHandleNumbers(g, p, 'endurance', null) - characterDataFetchHandleNumbers(g, p, 'encumbrance', null) - characterDataFetchHandleNumbers(g, p, 'fatigue', null);
		return emoji.showStatusLineWithMax(header, characterDataFetchHandleNumbers(g, p, 'endurance', null), characterDataFetchHandleNumbers(g, p, 'maxendurance', null), 'endurance', neverhalve)
			+ ' **(' + slashcommands.localizedString(locale, 'token-net', []) + ' ' + String(net)
			+ (characterWeary(g,p,null) ? ', ' + slashcommands.localizedString(locale, 'condition-weary', []) + '!' : '')
			+ ')**';
	}

	if (token == 'hope' || token == 'maxhope')
		return emoji.showStatusLineWithMax(header, characterDataFetchHandleNumbers(g, p, 'hope', null), characterDataFetchHandleNumbers(g, p, 'maxhope', null), 'hope', neverhalve) + hopeStatus(g, p, locale);
	
	if (token == 'shadow' || token == 'shadowscars')
		return emoji.showStatusLineStacked(header, characterDataFetchHandleNumbers(g, p, 'shadowscars', null), 'shadowscar', characterDataFetchHandleNumbers(g, p, 'shadow', null), 'shadow',neverhalve) + hopeStatus(g, p, locale);

	if (token == 'fatigue' || token == 'encumbrance' || token == 'load')
		return emoji.showStatusLineStacked(header, characterDataFetchHandleNumbers(g, p, 'encumbrance', null), 'encumbrance', characterDataFetchHandleNumbers(g, p, 'fatigue', null), 'fatigue',neverhalve) + (characterWeary(g,p,null) ? ' **(' + slashcommands.localizedString(locale, 'condition-weary', []) + ')**' : '');

	if (token == 'wounds') {
		let r = '';
		if (header != '') r = '`' + stringutil.padRight(header, 15, ' ') + '` `';
		if (characterDataFetchHandleNulls(g, p, 'wounded', null) == 'FALSE') {
			r += '  0` :o: ' + slashcommands.localizedString(locale, 'token-unwounded', []);
		} else {
			r += '  1` ' + emoji.tokenEmoji(characterDataFetchHandleNulls(g, p, 'woundtreated', ''));
			r += ' (' + String(characterDataFetchHandleNumbers(g, p, 'wounddaysleft', null)) + ' ' + slashcommands.localizedString(locale, 'token-daysleft', []) + ')';
		}
		return r;
	}

	return emoji.headingValueAndTokens(header, characterDataFetchHandleNumbers(g, p, token, null), token, false);
}

// token commands are responsible for parsing data, then calling tokenCommandExecute()
function tokenCommand(cmd, userID, channelID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let amt = 1, modifier = '=', token, knockback = false;
	let p = currentCharacter(userID);
	if (cmd == 'damage') {
		token = 'endurance';
		modifier = '-';
	} else if (cmd == 'heal') {
		token = 'endurance';
		modifier = '+';
	}
	if (cmd == 'gain') modifier = '+';
	else if (cmd == 'lose') modifier = '-';

	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'token') token = data.options[i].value;
		if (data.options[i].name == 'knockback') knockback = data.options[i].value;
		if (data.options[i].name == 'value') {
			amt = data.options[i].value;
			if (cmd == 'set' && (String(amt).startsWith('+') || String(amt).startsWith('-'))) {
				modifier = amt.charAt(0);
				amt = Number(amt.slice(1));
			} else if (stringutil.lowerCase(amt) == 'max') {
				if (token == 'hope' || token == 'endurance') {
					amt = 'max' + token;
				} else {
					return slashcommands.localizedString(locale, 'error-badvalue', [amt, slashcommands.localizedCommand(locale, cmd) + ' ' + slashcommands.localizedPicklist(locale, 'tokens', token)]);
				}
			} else if (!isNaN(amt)) {
				amt = Number(amt);
			} else {
				return slashcommands.localizedString(locale, 'error-badvalue', [amt, slashcommands.localizedCommand(locale, cmd)]);
			}
		}
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			p = findCharacterInGame(g, data.options[i].value);
			if (!p || p == '') return slashcommands.localizedString(locale, 'error-charnotfound', [data.options[i].value]);
		}
	}
	if (!p) return slashcommands.localizedString(locale, 'error-nocharacter', []);

	// validate or modify the values
	if (cmd == 'restore') {
		// deal with restoring things that go to 0 versus to max
		if (token == 'hope' || token == 'endurance') amt = 'max' + token;
		else if (token == 'maxhope' || token == 'maxendurance' || token == 'adventurepointsavailable' || token == 'skillpointsavailable' || token == 'carriedtreasure') return slashcommands.localizedString(locale, 'error-badvalue', [amt, slashcommands.localizedCommand(locale, cmd)]);
		else amt = 0;
	}

	return tokenCommandExecute(cmd, channelID, locale, g, p, token, modifier, amt, knockback);
}

// each token command will figure out the target character, token, new value
// then call this common execute function that does it and returns the new value's gauge
function tokenCommandExecute(verb, channelID, locale, g, charID, token, modifier, val, knockback) {
	//console.log('tokenCommandExecute(' + verb + ',' + channelID + ',' + locale + ',' + g + ',' + charID + ',' + token + ',' + modifier + ',' + val + ',' + knockback + ')');

	// an error if the character is not loaded
	let name = characterDataFetchHandleNulls(g,charID,'name','');
	if (name == '') return slashcommands.localizedString(locale, 'error-charnotloaded', []);

	let r = '';

	let max = null;
	if (token == 'hope' || token == 'endurance') max = 'max' + token;

	let modifiedval = val;
	// if this is an increase in the biggest damage received so far this round (including the first hit), handle possible knockback refunds
	if ((verb == 'damage' || (verb == 'lose' && token == 'endurance')) && val > g.characters[charID].dmgReceivedThisRound) {
		if (g.characters[charID].knockback && gameHasConfigFlag(g, 'highest-knockback')) {
			// if already knocked back, and set to highest-knockback, refund half the difference
			let refund = Math.floor((val - g.characters[charID].dmgReceivedThisRound) / 2);
			characterDataUpdate(g, charID, 'endurance', null, '+', refund, null, 'maxendurance', channelID);
			// add some note to this effect
			r = slashcommands.localizedString(locale, 'combat-kbrefund', [name, refund]) + '\n';
		} else if (knockback && !g.characters[charID].knockback) {
			// if turning on knockback, refund half of the amount and set knockback
			modifiedval = Math.ceil(val / 2);
			g.characters[charID].knockback = true;
			r = slashcommands.localizedString(locale, 'combat-isknockback', [name]) + '\n';
		}
		g.characters[charID].dmgReceivedThisRound = val;
	}

	// hints about more direct commands
	if (regdice.simpleRoll(3) == 1 && verb == 'lose' && token == 'endurance') r += ':bulb: Next time try `/damage` to save a step.\n';
	if (regdice.simpleRoll(3) == 1 && verb == 'gain' && token == 'endurance') r += ':bulb: Next time try `/heal` to save a step.\n';
	
	// do the actual change
	if (token == 'shadow' && modifier == '+') r += characterGainShadow(g, charID, modifiedval, channelID);
	else characterDataUpdate(g, charID, token, null, modifier, modifiedval, 0, max, channelID);
	filehandling.saveGameData(gameData);
	return r + showStackedStatusLine(g, charID, token, 'name', locale, false);
}

// option[0] required is unwounded|untreated|treated|failed; 1 optional is days, 2 optional is character
function woundCommand(userID, channelID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let p = currentCharacter(userID);

	let status = null, feat = null, daysleft = null, charName = null;
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'status') status = data.options[i].value;
		if (data.options[i].name == 'roll') daysleft = data.options[i].value;
		if (data.options[i].name == 'feat') feat = data.options[i].value;
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charName = data.options[i].value;
			p = findCharacterInGame(g, charName);
		}
	}
	if (!p || p == '') return slashcommands.localizedString(locale, 'error-charnotfound', [charName]);

	if (status == 'unwounded') {
		// daysleft, if provided, is ignored
		clearWounds(g, p, channelID);
		filehandling.saveGameData(gameData);
		return showStackedStatusLine(g, p, 'wounds', 'name', locale, false);
	}

	// if the character already has a number of days, and no amount was provided, we don't change it
	if (daysleft == null && characterDataFetchHandleNulls(g, p, 'wounded', null) == 'TRUE' && characterDataFetchHandleNumbers(g, p, 'wounddaysleft', null) != '') {
		daysleft = characterDataFetchHandleNumbers(g, p, 'wounddaysleft', null);
	}
	if (feat == 'favoured') feat = 2;
	else if (feat == 'illfavoured') feat = -2;
	else feat = 1;
	return giveWound(g, p, channelID, locale, status, feat, daysleft, true);
}

function giveWound(g, p, channelID, locale, status, feat, daysleft, override) {
	// figure out how many days are left, either manually or by a roll
	let rollShow = '', r = '';
	if (status == 'untreated' && characterDataFetchHandleNulls(g,p,"wounded", null) == 'TRUE' && !override) status = 'dying';
	if (status == 'dying') { // second wound always jumps to dying
		daysleft = emoji.EYE;
	} else if (daysleft && stringutil.lowerCase(daysleft).startsWith('g')) { // specified G means will recover soon
		daysleft = emoji.GANDALF;
	} else if (daysleft && stringutil.lowerCase(daysleft).startsWith('e')) { // specified Eye means jumps to dying
		daysleft = emoji.EYE;
	} else if (daysleft && !isNaN(daysleft)) { // specified a number of days left (1-10)
		daysleft = Number(daysleft);
	} else { // roll for it
		daysleft = featRoll();
		if (feat == -2 && hasTrait(g, p, 'virtue', 'tough as old tree-roots')) feat = 1;
		else if (hasTrait(g, p, 'virtue', 'tough as old tree-roots')) feat = 2;
		if (feat != 1) {
			// choose the best (or worse) of two rolls
			let secondWounddaysleft = featRoll();
			rollShow = featRollText(daysleft, false) + featRollText(secondWounddaysleft, false);
			if (feat == -2) {
				if (secondWounddaysleft == emoji.EYE ) daysleft = emoji.EYE ;
				else if (secondWounddaysleft != emoji.GANDALF && featRollValue(secondWounddaysleft) > featRollValue(daysleft)) daysleft = secondWounddaysleft;
			} else {
				if (secondWounddaysleft == emoji.GANDALF) daysleft = emoji.GANDALF;
				else if (secondWounddaysleft != emoji.EYE && featRollValue(secondWounddaysleft) < featRollValue(daysleft)) daysleft = secondWounddaysleft;
			}
		}
	}

	// add a message
	r = characterDataFetchHandleNulls(g, p, 'name', null) + ': ' + slashcommands.localizedString(locale, 'token-woundseverity', []) + ' ' + (rollShow == '' ? featRollText(daysleft, false) : rollShow) + ': ';
	// evaluate the consequences
	if (daysleft == emoji.EYE) {
		daysleft = 10 + characterDataFetchHandleNumbers(g,p,"wounddaysleft", null);
		r += slashcommands.localizedString(locale, 'token-wounddying', [])
		g.characters[p].stance = 'absent';
		characterDataUpdate(g,p,'endurance',null,'=',0,null,null, channelID);
	} else if (daysleft == emoji.GANDALF) {
		daysleft = 0;
		r += slashcommands.localizedString(locale, 'token-woundgandalf', [])
	} else {
		r += daysleft + ' ' + slashcommands.localizedString(locale, 'token-daysleft', []);
	}
	r += '\n';
	characterDataUpdate(g, p, 'wounded', null, '=', 'TRUE', 0, null, channelID);
	characterDataUpdate(g, p, 'woundtreated', null, '=', status, 0, null, channelID);
	characterDataUpdate(g, p, 'wounddaysleft', null, '=', daysleft, 0, null, channelID);
	if (g.combat.active) combat_log(g, stringutil.stringWithoutNewlines(r));
	filehandling.saveGameData(gameData);
	return r;
}

// the clear command is used to do mass updates to resetting various things
function clearCommand(userID, channelID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	let cmd = data.options[0].value;

	for (let key in g.characters) {
		if (cmd == 'damage') {
			clearWounds(g, key, channelID);
			characterDataUpdate(g, key, 'endurance', null, '=', 'maxendurance', null, null, channelID);
		} else if (cmd == 'endurance') {
			characterDataUpdate(g, key, 'endurance', null, '=', 'maxendurance', null, null, channelID);
		} else if (cmd == 'hope') {
			characterDataUpdate(g, key, 'hope', null, '=', 'maxhope', null, null, channelID);
		} else if (cmd == 'wounds') {
			clearWounds(g, key, channelID);
		} else if (cmd == 'conditions') {
			g.characters[key].daunted = false;
			g.characters[key].shieldsmashed = false;
			g.characters[key].seized = false;
			g.characters[key].bitten = false;
			g.characters[key].drained = false;
			g.characters[key].misfortune = false;
			g.characters[key].dreaming = false;
			g.characters[key].dismayed = false;
			g.characters[key].haunted = false;
			g.characters[key].bleeding = 0;
			g.characters[key].poisoned = 0;
			g.characters[key].poisontreated = false;
			g.characters[key].weary = false;
			g.characters[key].unweary = false;
			g.characters[key].miserable = false;
		} else if (cmd == 'protection') {
			g.characters[key].protectionTests = [];
		} else if (cmd == 'shadow') {
			g.characters[key].shadowTests = [];
		} else {
			characterDataUpdate(g, key, cmd, null, '=', 0, null, null, channelID);
		}
	}
	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'clear-complete', [slashcommands.localizedOption(locale, 'clear', 'choice', cmd)]);
}

// the environment command is used to do endurance loss (sources of injury) to single or all player-heroes
function environmentCommand(userID, channelID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	let charID = null, wholeCompany = false, type = null, level = null, name = 'unknown';
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'character') {
			name = stringutil.lowerCase(data.options[i].value);

			// first check if this is the word company
			if (name == slashcommands.localizedCommand(locale,'company')) wholeCompany = true;
			else charID = findCharacterInGame(g, name);
		}
		if (data.options[i].name == 'type') type = data.options[i].value;
		if (data.options[i].name == 'level') level = Number(data.options[i].value);
	}
	if (!charID && !wholeCompany) return slashcommands.localizedString(locale, 'error-charnotfound', [name]);

	// make a list of who to affect
	let victims = [];
	if (wholeCompany) {
		for (let key in g.characters) if (g.characters[key].active) victims.push(key);
	} else victims.push(charID);

	let r = '';
	victims.forEach(victim => {
		let [dmg, r2] = enduranceLossDamage(level, type, g, victim, locale, channelID);
		r += slashcommands.localizedOption(locale, 'environment', 'type', type) + r2;
	});

	filehandling.saveGameData(gameData);
	return r;
}

// CHARACTER INFORMATION ------------------------------------------------------

// determine if a character is of a culture
function isCulture(g, p, culture) {
	let c = stringutil.lowerCase(characterDataFetchHandleNulls(g, p, 'culture', null));
	culture = stringutil.lowerCase(culture);
	if (c.startsWith(culture)) return true;
	return false;
}

// determine if a character has a cultural blessing
function hasCulturalBlessing(g, p, blessing) {
	let c = stringutil.lowerCase(characterDataFetchHandleNulls(g, p, 'cultureblessing', null));
	blessing = stringutil.lowerCase(blessing);
	if (c.startsWith(blessing)) return true;
	return false;
}

// determine if a character has a lifepath
function hasLifepath(g, p, lifepath) {
	let c = stringutil.lowerCase(characterDataFetchHandleNulls(g, p, 'lifepath', null));
	lifepath = stringutil.lowerCase(lifepath);
	if (c.startsWith(lifepath)) return true;
	return false;
}

// determine if a character has a virtue or reward
function hasTrait(g, p, traittype, trait) {
	if (!g || !g.characters[p] || !g.characters[p].repositoryKey) return false;
	return (hasTraitFromExternalCache(g.characters[p].keyType, g.characters[p].repositoryKey, traittype, trait, 'startsWith'));
}

// determine if a character has a quality
function hasQuality(g, p, quality) {
	if (!g || !g.characters[p] || !g.characters[p].repositoryKey) return false;
	return (hasQualityFromExternalCache(g.characters[p].keyType, g.characters[p].repositoryKey, 'quality', quality, 'startsWith', false));
}

// determine if a character has a curse
function hasCurse(g, p, curse) {
	if (!g || !g.characters[p] || !g.characters[p].repositoryKey) return false;
	return (hasQualityFromExternalCache(g.characters[p].keyType, g.characters[p].repositoryKey, 'curse', curse, 'startsWith', false));
}

// determine if a weapon has a quality
function weaponHasQuality(wData, quality) {
	if (!wData || wData.length < 14) return false;
	for (let i = 12; i <= 14 ; i++) {
		let wq = stringutil.lowerCase(wData[i]);
		if (wq == quality || wq.startsWith(quality)) return true;
	}
	return false;
}

// determine if a weapon has a curse
function weaponHasCurse(wData, curse) {
	if (!wData || wData.length < 17) return false;
	let wq = stringutil.lowerCase(wData[17]);
	if (wq == curse || wq.startsWith(curse)) return true;
	return false;
}

// determine if a weapon has a bane relevant to a particular target
function weaponHasRelevantBane(wData, abanetype, hrtype) {
	if (!wData || wData.length < 14) return false;
	abanetype = stringutil.lowerCase(abanetype);
	for (let i = 15; i <= 16 ; i++) {
		let wbane = stringutil.lowerCase(wData[i]);
		if (wbane == 'the enemy' && hrtype == 'hate') return true;
		if (wbane == abanetype) return true;
		if (wbane.startsWith(abanetype)) return true;
		if (wbane = 'evil men' && abanetype == 'evil man') return true;
		if (wbane = 'wolves' && abanetype == 'wolf') return true;
	}
	return false;
}

// a diacritical-resistant test for origin, though the origin tested for should never have them
function weaponHasOrigin(wData, origin) {
	let wOrigin= stringutil.lowerCase(wData[11]);
	// strip accents
	wOrigin = wOrigin.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
	if (wOrigin == origin || wOrigin.startsWith(origin)) return true;
	return false;
}


// determine if a weapon has a quality
function armorHasCurse(g, p, curse) {
	if (!g || !g.characters[p] || !g.characters[p].repositoryKey) return false;
	return (hasQualityFromExternalCache(g.characters[p].keyType, g.characters[p].repositoryKey, 'curse', curse, 'startsWith', true));
}

// COMPANY COMMAND ------------------------------------------------------------

// depending on your state and the type of recovery, how much endurance will you recover?
function amountToRecover(wounded, woundtreated, heart, strength, type, cramBonus) {
	if (type == 'short' || type == 'shortsafe') {
		if (!wounded) return strength + cramBonus;
		return 0;
	}
	if (type == 'prolonged' || type == 'safe') {
		if (!wounded) return 99;
		else return strength;
	}
	if (type == 'journey') {
		if (!wounded) return 2;
		if (woundtreated == 'treated') return 1;
		return 0;
	}
	return 0;
}

// handle the passage of one or more days to advance wounds
function advanceWounds(g, key, days, locale, channelID) {
	let r = '';
	// reset wound treatment on prolonged rests
	if (characterDataFetchHandleNulls(g, key, 'woundtreated', null) == 'failed')
		// you can try again on a new day
		characterDataUpdate(g, key, 'woundtreated', null, '=', 'untreated', 0, null, channelID);

	// recover from wounds
	if (characterDataFetchHandleNumbers(g, key, 'wounddaysleft', null) > 0) {
		if (characterDataFetchHandleNumbers(g, key, 'wounddaysleft', null) <= days) {
			// now fully healed
			clearWounds(g, key, channelID);
			r = slashcommands.localizedString(locale, 'token-woundhealed', []);
		} else {
			// decrement the count of days left until healing
			characterDataUpdate(g, key, 'wounddaysleft', null, '-', days, 0, null, channelID);
			r = slashcommands.localizedString(locale, 'token-wounds', []) + ' ' + slashcommands.localizedString(locale, 'token-reduced', []) + ' ' + characterDataFetchHandleNumbers(g, key, 'wounddaysleft', null) + ' ' + slashcommands.localizedString(locale, 'token-daysleft', []);
		}
	}
	return r;
}

// handle the passage of one or more days to advance poisons
function advancePoison(g, key, days, locale, channelID) {
	if (!g.characters[key].poisoned) return '';
	let r = '';

	g.characters[key].poisontreated = false;

	for (let i = 0; i < days; i++) {
		r += slashcommands.localizedString(locale, 'condition-poison-roll', [characterDataFetchHandleNulls(g, key, 'name',null), slashcommands.localizedString(locale, 'condition-severity-' + g.characters[key].poisoned, []) + ' ' + slashcommands.localizedString(locale, 'condition-poison', [])])
		if (days > 1) r += slashcommands.localizedString(locale, 'condition-poison-day', [String(i+1)]);
		r += ': ';

		// roll for endurance loss
		let [poisonRoll, r2] = enduranceLossLevel(g.characters[key].poisoned);
		r += r2 + ': ';

		if (poisonRoll == emoji.GANDALF) { // no longer poisoned
			givePoison(g, key, 0, true, channelID);
			r += slashcommands.localizedString(locale, 'condition-poison-gandalf', []);
			i = days;
		} else if (poisonRoll == emoji.EYE) { // unconscious, zero endurance
			r += slashcommands.localizedString(locale, 'condition-poison-eye', []);
			r += '\n' + tokenCommandExecute('damage', channelID, locale, g, key, 'endurance', '=', 0, false);
		} else { // endurance damage
			r += slashcommands.localizedString(locale, 'condition-poison-damage', [poisonRoll]);
			r += '\n' + tokenCommandExecute('damage', channelID, locale, g, key, 'endurance', '-', poisonRoll, false);
		}
		r += '\n';
	}
	return r;
}

// roll for an environmental damage
function enduranceLossLevel(level) {
	let r = '';
	// roll for endurance loss
	let lossRoll = featRoll(), secondlossRoll = featRoll();
	if (level == 2) {
		r += featRollText(lossRoll, false);
	} else {
		r += emoji.emojiPunctuation('(');
		r += featRollText(lossRoll, false);
		r += featRollText(secondlossRoll, false);
		r += emoji.emojiPunctuation(')');
		lossRoll = betterFeatForInjury(lossRoll, secondlossRoll, level == 1);
		r += featRollText(lossRoll, false);
	}
	return [lossRoll, r];
}

function enduranceLossDamage(level, type, g, charID, locale, channelID) {
	let r = '';
	if (type == 'fire' && hasTrait(g, charID, 'virtue', 'ancient fire')) level--;
	if (level == 0) return [0, ''];
	let oldEndurance = tokenValue(g, charID, 'endurance');
	let [dmg, r2] = enduranceLossLevel(level);
	r += ': ' + r2 + ': ';
	if (dmg == emoji.EYE) {
		r += '\n' + tokenCommandExecute('damage', channelID, locale, g, charID, 'endurance', '=', 0, false);
	} else if (dmg != emoji.GANDALF) {
		r += tokenCommandExecute('damage', channelID, locale, g, charID, 'endurance', '-', dmg, false) + '\n';
	} else {
		r += showStackedStatusLine(g, charID, 'endurance', 'name', locale, false) + '\n';
	}
	// special consequences if you drop to zero during this
	if (oldEndurance > 0 && tokenValue(g, charID, 'endurance') <= 0) {
		if (type == 'cold' || type == 'suffocation' || type == 'poison') r += '\n' + giveWound(g, charID, channelID, locale, 'dying', 1, null, false);
		if (type == 'fall' || type == 'fire') r += '\n' + giveWound(g, charID, channelID, locale, 'untreated', 1, null, false);
	}
	return [dmg, r];
}

// figure out which of two feat rolls is better (or worse) for Sources of Injury, where 1-10's values are inverted
function betterFeatForInjury(first, second, better) {
	if (better) {
		if (featValueForInjury(first) > featValueForInjury(second)) return first;
		return second;
	}
	if (featValueForInjury(first) > featValueForInjury(second)) return second;
	return first;
}

function featValueForInjury(feat) {
	if (feat == emoji.GANDALF) return 99;
	if (feat == emoji.EYE) return -99;
	return 11 - feat; // low numbers are better so this maps 1-10 to 10-1
}

// more mass updates but not just clearing things, also carrying out processes for recovery of various sorts
// 'prolonged','safe','short','shortsafe','hope','shadow','fatigue','endurance','arrival','fellowship','yule','ap','sp','treasure'
function companyCommand(userID, channelID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	let type = data.options[0].value;
	let amt = 1;
	if (data.options[1] && data.options[1].value) amt = Number(data.options[1].value);

	let r = '', after = '';

	if (type == 'fellowship' || type == 'yule') {
		if (amt > 3) amt = 3;
		resetSongUses(g);
		eyeSetValue(g, 'reset', null, null, null);
		r += eyeShow(g, false);
		g.pool = g.maxpool;
	}

	// build a heading
	r += slashcommands.localizedString(locale, 'company-heading', [slashcommands.localizedOption(locale, 'company', 'choice', type)]);
	// if the amount matters,
	if (['hope','shadow','fatigue','endurance','fellowship','yule','ap','sp','session','treasure'].includes(type)) {
		// include it
		r += slashcommands.localizedString(locale, 'company-amount', [amt]);
	}
	r += ':\n';

	// calculate a cram bonus to recovery
	let cramBonus = 0;
	if (type == 'short' || type == 'shortsafe') {
		for (let key in g.characters) {
			if (hasTrait(g, key, 'virtue', 'cram')) {
				if (gameHasConfigFlag(g, 'cram-nostack')) {
					cramBonus = Math.max(cramBonus,characterDataFetchHandleNumbers(g, key, 'sheet', 'wisdom'));
				} else {
					cramBonus += characterDataFetchHandleNumbers(g, key, 'sheet', 'wisdom');
				}
			}
		}
	}
	// convert prolonged to safe with lembas
	let lembasMultiplier = 1;
	if (type == 'prolonged') {
		for (let key in g.characters) {
			if (hasTrait(g, key, 'virtue', 'lembas')) {
				type = 'safe';
				lembasMultiplier = 2;
			}
		}
	}

	for (let key in g.characters) {
		if (!g.characters[key].active) continue;
		after = '';

		if (type == 'hope') {

			let thisAmt = amt;
			if (amt > 0 && hasTrait(g, key, 'virtue', 'the art of smoking')) {
				thisAmt++;
			}
			characterDataUpdate(g, key, 'hope', null, '+', thisAmt, 0, 'maxhope', channelID);
			r += showStackedStatusLine(g, key, 'hope', 'name', locale, false) + '\n';

		} else if (type == 'shadow') {
			
			r += characterGainShadow(g, key, amt, channelID);
			r += showStackedStatusLine(g, key, 'shadow', 'name', locale, false) + '\n';

		} else if (type == 'ap') {

			characterDataUpdate(g, key, 'adventurepointsavailable', null, '+', amt, 0, null, channelID);
			r += '**' + characterDataFetchHandleNulls(g, key, 'name', null) + ' ' + stringutil.upperCase(slashcommands.localizedOption(locale, 'company', 'choice', 'ap')) + '**: '
			r += characterDataFetchHandleNumbers(g, key, 'adventurepointsspent', null) + ' ' + slashcommands.localizedString(locale, 'token-spent', []) + ', ';
			r += characterDataFetchHandleNumbers(g, key, 'adventurepointsavailable', null) + ' ' + slashcommands.localizedString(locale, 'token-available', []) + ', ';
			r += characterDataFetchHandleNumbers(g, key, 'adventurepointstotal', null) + ' ' + slashcommands.localizedString(locale, 'token-total', []) + '\n';

		} else if (type == 'sp') {
			
			characterDataUpdate(g, key, 'skillpointsavailable', null, '+', amt, 0, null, channelID);
			r += '**' + characterDataFetchHandleNulls(g, key, 'name', null) + ' ' + stringutil.upperCase(slashcommands.localizedOption(locale, 'company', 'choice', 'sp')) + '**: '
			r += characterDataFetchHandleNumbers(g, key, 'skillpointsspent', null) + ' ' + slashcommands.localizedString(locale, 'token-spent', []) + ', ';
			r += characterDataFetchHandleNumbers(g, key, 'skillpointsavailable', null) + ' ' + slashcommands.localizedString(locale, 'token-available', []) + ', ';
			r += characterDataFetchHandleNumbers(g, key, 'skillpointstotal', null) + ' ' + slashcommands.localizedString(locale, 'token-total', []) + '\n';

		} else if (type == 'session') {

			characterDataUpdate(g, key, 'adventurepointsavailable', null, '+', amt, 0, null, channelID);
			r += '**' + characterDataFetchHandleNulls(g, key, 'name', null) + ' ' + stringutil.upperCase(slashcommands.localizedOption(locale, 'company', 'choice', 'ap')) + '**: '
			r += characterDataFetchHandleNumbers(g, key, 'adventurepointsspent', null) + ' ' + slashcommands.localizedString(locale, 'token-spent', []) + ', ';
			r += characterDataFetchHandleNumbers(g, key, 'adventurepointsavailable', null) + ' ' + slashcommands.localizedString(locale, 'token-available', []) + ', ';
			r += characterDataFetchHandleNumbers(g, key, 'adventurepointstotal', null) + ' ' + slashcommands.localizedString(locale, 'token-total', []) + '\n';
			characterDataUpdate(g, key, 'skillpointsavailable', null, '+', amt, 0, null, channelID);
			r += '**' + characterDataFetchHandleNulls(g, key, 'name', null) + ' ' + stringutil.upperCase(slashcommands.localizedOption(locale, 'company', 'choice', 'sp')) + '**: '
			r += characterDataFetchHandleNumbers(g, key, 'skillpointsspent', null) + ' ' + slashcommands.localizedString(locale, 'token-spent', []) + ', ';
			r += characterDataFetchHandleNumbers(g, key, 'skillpointsavailable', null) + ' ' + slashcommands.localizedString(locale, 'token-available', []) + ', ';
			r += characterDataFetchHandleNumbers(g, key, 'skillpointstotal', null) + ' ' + slashcommands.localizedString(locale, 'token-total', []) + '\n';

		} else if (type == 'treasure') {
			
			characterDataUpdate(g, key, 'carriedtreasure', null, '+', amt, 0, null, channelID);
			r += '**' + characterDataFetchHandleNulls(g, key, 'name', null) + '**: '
			r += characterDataFetchHandleNumbers(g, key, 'treasure', null) + ' + ' + characterDataFetchHandleNumbers(g, key, 'carriedtreasure', null) + ' (' + characterDataFetchHandleNulls(g, key, 'standardofliving', null) + ')\n';

		} else if (type == 'fellowship' || type == 'yule') {

			console.log('  target of the gain is ' + characterDataFetchHandleNulls(g, key, 'name', null) + ' cur=' + characterDataFetchHandleNumbers(g, key, 'hope', null) + ' max=' + characterDataFetchHandleNumbers(g, key, 'maxhope', null) + ' heart=' + characterDataFetchHandleNumbers(g, key, 'heart', null));

			if (type == 'yule') {
				// yule handling: everyone gets full hope
				characterDataUpdate(g, key, 'hope', null, '=', 'maxhope', 0, 'maxhope', channelID);
				console.log('    yule brings to full: ' + characterDataFetchHandleNumbers(g, key, 'hope', null));
				// everyone also gains Wits in SP
				characterDataUpdate(g, key, 'skillpointsavailable', null, '+', characterDataFetchHandleNumbers(g, key, 'wits', null), 0, null, channelID);
				r += '**' + characterDataFetchHandleNulls(g, key, 'name', null) + '**: '
				r += characterDataFetchHandleNumbers(g, key, 'skillpointsspent', null) + ' ' + slashcommands.localizedString(locale, 'token-spent', []) + ', ';
				r += characterDataFetchHandleNumbers(g, key, 'skillpointsavailable', null) + ' ' + slashcommands.localizedString(locale, 'token-available', []) + ', ';
				r += characterDataFetchHandleNumbers(g, key, 'skillpointstotal', null) + ' ' + slashcommands.localizedString(locale, 'token-total', []) + '\n';
			} else {
				// restore 0 hope to 1 hope
				if ((type == 'prolonged' || type == 'safe') && characterDataFetchHandleNumbers(g, key, 'hope', null) == 0) {
					characterDataUpdate(g, key, 'hope', null, '=', (hasTrait(g, key, 'virtue', 'the art of smoking') ? 2 : 1), 0, 'maxhope', channelID);
					r += slashcommands.localizedString(locale, 'token-hope', []) + ' ' + slashcommands.localizedString(locale, 'token-increased', []) + ' ' + characterDataFetchHandleNumbers(g, key, 'hope', null) + ', ';
				}
				// regular fellowship phase, everyone gets Heart, or Heart/2 for Rangers
				let heart = characterDataFetchHandleNumbers(g, key, 'heart', null);
				if (isCulture(g, key, 'ranger')) heart = Math.ceil(heart/2);
				if (heart > 0 && hasTrait(g, key, 'virtue', 'the art of smoking')) heart++;
				characterDataUpdate(g, key, 'hope', null, '+', heart, 0, 'maxhope', channelID);
				console.log('    gained hope up to: ' + characterDataFetchHandleNumbers(g, key, 'hope', null));
			}
			g.characters[key].daunted = false;
			g.characters[key].shieldsmashed = false;
			g.characters[key].seized = false;
			g.characters[key].bitten = false;
			g.characters[key].drained = false;
			g.characters[key].misfortune = false;
			g.characters[key].dreaming = false;
			g.characters[key].dismayed = false;
			g.characters[key].haunted = false;
			g.characters[key].weary = false;
			g.characters[key].unweary = false;
			g.characters[key].miserable = false;
			g.characters[key].bleeding = 0;
			g.characters[key].poisoned = 0;
			g.characters[key].poisontreated = false;
			r += emoji.showStatusLineWithMax(characterDataFetchHandleNulls(g, key, 'name', null), characterDataFetchHandleNumbers(g, key, 'hope', null), characterDataFetchHandleNumbers(g, key, 'maxhope', null), 'hope', false) + '\n';
			if (amt > 0) {
				let shadowRecovery = amt;
				if (isCulture(g, key, 'elf of lindon') && shadowRecovery > 1) shadowRecovery = 1;
				if (isCulture(g, key, 'high elf') && type != 'yule') shadowRecovery = 0;
				if (characterDataFetchHandleNumbers(g, key, 'shadow', null) + characterDataFetchHandleNumbers(g, key, 'shadowscars', null) >= characterDataFetchHandleNumbers(g, key, 'maxhope', null)) shadowRecovery = 0; // see p139
				if (shadowRecovery > 0) {
					characterDataUpdate(g, key, 'shadow', null, '-', shadowRecovery, 0, null, channelID);
					r += showStackedStatusLine(g, key, 'shadow', ' ' + stringutil.titleCase(slashcommands.localizedString(locale, 'token-shadow', [])), locale, false) + '\n';
					//r += emoji.headingValueAndTokens(, characterDataFetchHandleNumbers(g, key, 'shadow', null), 'shadow', false) + '\n';
				}
			}
			// always reset things that heal up over the fellowship phase
			characterDataUpdate(g, key, 'endurance', null, '=', 'maxendurance', 0, 'maxendurance', channelID);
			characterDataUpdate(g, key, 'fatigue', null, '=', 0, 0, null, channelID);
			characterDataUpdate(g, key, 'wounded', null, '=', 'FALSE', 0, null, channelID);
			characterDataUpdate(g, key, 'woundtreated', null, '=', 'untreated', 0, null, channelID);
			characterDataUpdate(g, key, 'wounddaysleft', null, '=', '', 0, null, channelID);
			g.characters[key].protectionTests = [];
			g.characters[key].shadowTests = [];

		} else if (type == 'fatigue' || type == 'arrival') {

			// if this is arrival, do a Travel roll to see how much to recover
			if (type == 'arrival' && characterDataFetchHandleNumbers(g, key, 'fatigue', null) > 0) {
				// fill in the right values
				let rollResults = torRollDice(skillFavored(characterDataFetchHandleNulls(g, key, 'sheet','travel')) ? 2 : 1, skillValue(characterDataFetchHandleNulls(g, key, 'sheet','travel')), 0, 0, characterWeary(g, key,'arrival'), characterMiserable(g, key), false, false, hasLifepath(g, key, 'favoured'), locale, diceStyle(g), gameHasConfigFlag(g, 'magical-tengwar'));

				amt = (rollResults[0] >= charSkillTN(g, key, 'travel')) ? (rollResults[2] + 1) : 0;
				r += '**' + characterDataFetchHandleNulls(g, key, 'name', null) + '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' **';
				r += stringutil.makeSmallCaps(stringutil.titleCase(slashcommands.localizedString(locale, 'skill-travel', [])));
				r += '**: ' + rollResults[5] + ' = ' + rollResults[0] + ', **' + amt + '** ' + slashcommands.localizedString(locale, 'token-recovery', []) + '\n';
				if (hasTrait(g, key, 'virtue', 'herbal remedies')) {
					rollResults = torRollDice(skillFavored(characterDataFetchHandleNulls(g, key, 'sheet','healing')) ? 2 : 1, skillValue(characterDataFetchHandleNulls(g, key, 'sheet','healing')), 0, 0, characterWeary(g, key,null), characterMiserable(g, key), false, false, hasLifepath(g, key, 'favoured'), locale, diceStyle(g), gameHasConfigFlag(g, 'magical-tengwar'));
					let hamt = (rollResults[0] >= charSkillTN(g, key, 'healing')) ? (rollResults[2] + 1) : 0;
					r += '**' + characterDataFetchHandleNulls(g, key, 'name', null) + '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' **' + stringutil.makeSmallCaps(stringutil.titleCase(slashcommands.localizedString(locale, 'skill-healing', []))) + '**: ' + rollResults[5] + ' = ' + rollResults[0] + ', **' + hamt + '** ' + slashcommands.localizedString(locale, 'token-recovery', []) + '\n';
					amt += hamt;
				}
				amt += characterDataFetchHandleNumbers(g, key, 'steedvigour', null);
				amt *= -1; 
			}

			let thisAmt = amt;
			if (amt > 0 && hasTrait(g, key, 'virtue', 'cram')) {
				thisAmt--;
			}
			if (amt > 0 && hasTrait(g, key, 'virtue', 'lembas')) {
				thisAmt--;
			}
			if (amt > 0 && hasTrait(g, key, 'virtue', 'over dangerous leagues')) {
				thisAmt--;
			}
			if (thisAmt) characterDataUpdate(g, key, 'fatigue', null, '+', thisAmt, 0, null, channelID);
			r += emoji.showStatusLineStacked(characterDataFetchHandleNulls(g, key, 'name', null), characterDataFetchHandleNumbers(g, key, 'encumbrance', null), 'encumbrance', characterDataFetchHandleNumbers(g, key, 'fatigue', null), 'fatigue',false);
			r += ' **(' + slashcommands.localizedString(locale, 'token-net', []) + ' ' + String(characterDataFetchHandleNumbers(g, key, 'endurance', null) - characterDataFetchHandleNumbers(g, key, 'encumbrance', null) - characterDataFetchHandleNumbers(g, key, 'fatigue', null)) + (characterDataFetchHandleNumbers(g, key, 'endurance', null) <= (characterDataFetchHandleNumbers(g, key, 'encumbrance', null) + characterDataFetchHandleNumbers(g, key, 'fatigue', null)) ? ': ' + slashcommands.localizedString(locale, 'condition-weary', []) + '!' : '') +  ')**\n';

		} else { // everything else is restoring endurance, and possibly other things

			g.characters[key].daunted = false;
			g.characters[key].seized = false;
			g.characters[key].bitten = false;
			g.characters[key].drained = false;
			g.characters[key].misfortune = false;
			g.characters[key].dreaming = false;
			g.characters[key].dismayed = false;
			g.characters[key].haunted = false;
			g.characters[key].weary = false;
			g.characters[key].unweary = false;
			g.characters[key].miserable = false;
			g.characters[key].bleeding = 0;

			// fatigue recovery during rests
			if (characterDataFetchHandleNumbers(g, key, 'fatigue', null) > 0 && g.characters[key].poisoned == 0 && ((type == 'safe') || (type == 'shortsafe' && (hasTrait(g, key, 'virtue', 'elvish dreams') || hasTrait(g, key, 'virtue', 'elven dreams'))))) {
				characterDataUpdate(g, key, 'fatigue', null, '-', amt * lembasMultiplier, 0, null, channelID);
				after +=  ', '+ slashcommands.localizedString(locale, 'token-fatigue', []) + ' ' + slashcommands.localizedString(locale, 'token-reduced', []) + ' ' + characterDataFetchHandleNumbers(g, key, 'fatigue', null);
			}

			// advance wounds and poison
			if (type == 'prolonged' || type == 'safe') {
				let woundsText = advanceWounds(g, key, amt, locale, channelID);
				if (woundsText != '') after += ', ' + woundsText;
				let poisonText = advancePoison(g, key, amt, locale, channelID);
				if (poisonText != '') after += '\n' + poisonText;
			}

			// endurance recovery
			if (characterDataFetchHandleNumbers(g, key, 'endurance', null) >= characterDataFetchHandleNumbers(g, key, 'maxendurance', null) && amt > 0) {
				r += showStackedStatusLine(g, key, 'endurance', 'name', locale, false);
				r += ' ' + slashcommands.localizedString(locale, 'token-full', []) + after + '\n';
			} else {

				let healType = type;

				// restore 0 hope to 1 hope
				if ((healType == 'prolonged' || healType == 'safe') && characterDataFetchHandleNumbers(g, key, 'hope', null) == 0) {
					characterDataUpdate(g, key, 'hope', null, '=', (hasTrait(g, key, 'virtue', 'the art of smoking') ? 2 : 1), 0, 'maxhope', channelID);
					r += slashcommands.localizedString(locale, 'token-hope', []) + ' ' + slashcommands.localizedString(locale, 'token-increased', []) + ' ' + characterDataFetchHandleNumbers(g, key, 'hope', null) + ', ';
				}

				// calculate how much endurance to recover
				let thisHealType = healType;
				if ((hasTrait(g, key, 'virtue', 'elvish dreams') || hasTrait(g, key, 'virtue', 'elven dreams')) && (healType == 'short' || healType == 'shortsafe')) thisHealType = 'prolonged';
				let strength = characterDataFetchHandleNumbers(g, key, 'sheet','strength');
				if (hasTrait(g, key, 'virtue', 'tough as old tree-roots')) strength *= 2;
				let healAmount = (type == 'endurance') ? amt : amountToRecover(characterDataFetchHandleNulls(g, key, 'wounded', null) == 'TRUE', characterDataFetchHandleNulls(g, key, 'woundtreated', null), characterDataFetchHandleNumbers(g, key, 'sheet','heart'), strength, thisHealType, cramBonus);
				// recover from unconsciousness even if wounded
				if (healAmount == 0 && characterDataFetchHandleNumbers(g, key, 'endurance','') == 0) healAmount = 1;
				//if ((characterDataFetchHandleNulls(g, key, 'wounded', null) == 'TRUE' && characterDataFetchHandleNulls(g, key, 'woundtreated', null) != 'TRUE') || g.characters[key].poisoned) healAmount = 0;
				if (g.characters[key].poisoned) healAmount = 0;

				if (healAmount == 0) {
					r += characterDataFetchHandleNulls(g, key, 'name', null) + ' ' + slashcommands.localizedString(locale, 'company-noheal', []);
				} else {
					healAmount *= amt; // scale by days
					let endurance = characterDataFetchHandleNumbers(g, key, 'endurance', null);
					let maxendurance = characterDataFetchHandleNumbers(g, key, 'maxendurance', null);
					characterDataUpdate(g, key, 'endurance', null, '+', healAmount, 0, 'maxendurance', channelID);
					r += showStackedStatusLine(g, key, 'endurance', 'name', locale, false);
					if ((endurance + healAmount >= maxendurance) &&	(maxendurance > 0)) {
						r += ' (' + slashcommands.localizedString(locale, 'token-full', []) + ')';
					} else {
						r += ' ' + healAmount + ' ' + slashcommands.localizedString(locale, 'token-recovery', []);
					}
				}
				r += after + '\n';
			}
		}
	}
	if (type == 'session') r += poolDistributeAutomatic(g, locale, userID, channelID, true);
	
	filehandling.saveGameData(gameData);
	return r;
}

// STATUS COMMAND -------------------------------------------------------------

// the status command is a quick visual reference to character tokens -- also appears on the end of 2e sheets
function tokenStatusDisplay(g, charID, locale, omitwounds) {
	if (!g || !g.characters[charID]) return '';

	let r = emoji.headingValueAndTokens(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-pool', [])), g.pool, 'pool', false) + '\n';
	r += showStackedStatusLine(g, charID, 'hope', 'token', locale, false) + '\n';
	r += showStackedStatusLine(g, charID, 'shadow', 'token', locale, false) + '\n';
	r += showStackedStatusLine(g, charID, 'endurance', 'token', locale, false) + '\n';
	r += showStackedStatusLine(g, charID, 'encumbrance', 'token', locale, false) + '\n';
	if (!omitwounds) r += showStackedStatusLine(g, charID, 'wounds', 'token', locale, false) + '\n';

	if (g.characters[charID].pendingFatigue && g.journey.active) {
		r += emoji.headingValueAndTokens(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-pendingfatigue', [])), g.characters[charID].pendingFatigue, 'fatigue', false) + '\n';
	}
	if (g.characters[charID].tempbonus) {
		r += emoji.headingValueAndTokens(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-tempbonus', [])), g.characters[charID].tempbonus, 'tempbonus', false) + '\n';
	}
	let stancenum = stanceList.indexOf(charEffectiveStance(g, charID)) + 1;
	let bonusesLine = [];
	if (stancenum <= g.rallycurrent) bonusesLine.push('+1d (' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'action-rally', [])) + ')');
	if (charEffectiveStance(g, charID) == 'forward') bonusesLine.push('+1d (' + slashcommands.localizedString(locale, 'stance-forward', []) + ')');
	if (charEffectiveStance(g, charID) == 'defensive') bonusesLine.push('-' + encounters.count_engagement(g,charID) + 'd (' + slashcommands.localizedString(locale, 'stance-defensive', []) + ')');
	if (charEffectiveStance(g, charID) == 'skirmish') bonusesLine.push('-1d (' + slashcommands.localizedString(locale, 'stance-skirmish', []) + ')');
	if (bonusesLine.length != 0) {
		r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-modifiers', [])), 15, ' ') + '` ' + bonusesLine.join('; ') + '\n';
	}

	if (g.characters[charID].tempparry) {
		r += emoji.headingValueAndTokens(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-tempparry', [])), g.characters[charID].tempparry, 'tempparry', false) + '\n';
	}

	if (g.characters[charID].fendParry) {
		r += emoji.headingValueAndTokens(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-fendparry', [])), g.characters[charID].fendParry, 'tempparry', false) + '\n';
	}

	if (g.characters[charID].bleeding) {
		r += emoji.headingValueAndTokens(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-bleeding', [])), g.characters[charID].bleeding, 'bleeding', false) + '\n';
	}

	let conditions = [];
	['knockback','daunted','shieldsmashed','seized','bitten','drained','dreaming','misfortune','dismayed','haunted','bleeding','poisoned','weary','miserable','unweary'].forEach(condition => {
		if (g.characters[charID][condition]) {
			if (condition == 'poisoned') {
				conditions.push(slashcommands.localizedString(locale, 'condition-severity-' + g.characters[charID][condition], []) + ' ' + slashcommands.localizedString(locale, 'condition-poison', []) + (g.characters[charID].poisontreated ? ' (' + slashcommands.localizedString(locale, 'token-treated', []) + ')' : ''));
			}
			else conditions.push(slashcommands.localizedString(locale, 'condition-' + condition, []));
		}
	});
	if (conditions.length != 0) {
		r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'condition-heading', [])), 15, ' ') + '` ' + conditions.join(', ') + '\n';
	}

	if (g.characters[charID].protectionTests.length != 0) {
		let tests = [];
		g.characters[charID].protectionTests.forEach(test => {
			let testText = stringutil.upperCase(slashcommands.localizedOption(locale, 'test', 'add', 'tn')) + test.injury;
			if (test.responsible != '' && test.responsible != 'loremaster' && test.responsible != 'journey') testText += ' ' + slashcommands.localizedString(locale, 'by', []) + ' ' + test.responsible;
			if (test.deadly) testText += ' ' + slashcommands.localizedOption(locale, 'test', 'add', 'deadly');
			tests.push(testText);
		});
		r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'test-prottests', [])), 15, ' ') + '` ' + tests.join(', ') + '\n';
	}

	if (g.characters[charID].shadowTests.length != 0) {
		let tests = [];
		g.characters[charID].shadowTests.forEach(test => {
			let testText = test.type + ' (' + test.points;
			if (test.condition != '') testText += ', ' + test.condition;
			testText += ')';
			tests.push(testText);
		});
		r += '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'test-shadowtests', [])), 15, ' ') + '` ' + tests.join(', ') + '\n';
	}

	if (g.characters[charID].tengwar) {
		r += emoji.headingValueAndTokens(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-tengwar', [])), g.characters[charID].tengwar, 'tengwar', false) + '\n';
	}

	return r;
}

// show the status for a specified userID -- most of it is in the above function so it can also be used by 2e sheets
function statusCommand(userID, channelID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID), statusToSet = false, value = true;
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'character') {
			charID = findCharacterInGame(g, data.options[i].value);
			if (!charID || charID == '') return slashcommands.localizedString(locale, 'error-charnotfound', [data.options[i].value]);
			if (!isPlayerLoremasterOfGame(userID, g) && charID != currentCharacter(userID)) return slashcommands.localizedString(locale, 'error-lmonly', []);
		}
		if (data.options[i].name == 'status') statusToSet = data.options[i].value;
		if (data.options[i].name == 'value') value = Number(data.options[i].value);
	}
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);

	// handle value setting
	let r = '';
	if (statusToSet != null) {
		if (['knockback','daunted','shieldsmashed','seized','bitten','drained','misfortune','dismayed','dreaming','haunted','weary','miserable','weary'].includes(statusToSet)) {
			if (value == 0) value = false; else value = true;
		} else if (statusToSet != 'tempbonus' && value < 0) value = 0;

		// handle the refund from knockback
		if (statusToSet == 'knockback' && value == true && !g.characters[charID].knockback && g.characters[charID].dmgReceivedThisRound != 0) {
			let refund = Math.floor(g.characters[charID].dmgReceivedThisRound / 2);
			characterDataUpdate(g, charID, 'endurance', null, '+', refund, null, 'maxendurance', channelID);
			// add some note to this effect
			r = slashcommands.localizedString(locale, 'combat-kbrefund', [characterDataFetchHandleNulls(g, charID, 'name', null), refund]) + '\n';
			if (g.combat.active) combat_log(g, r);
		}

		// set the value
		if (statusToSet == 'poisoned') givePoison(g, charID, value, true, channelID);
		else g.characters[charID][statusToSet] = value;

		filehandling.saveGameData(gameData);
	}

	r += '**' + characterDataFetchHandleNulls(g, charID, 'name', null) + '**:\n';
	r += tokenStatusDisplay(g, charID, locale, false);
	return r;
}

// some things override the selected stance
function charEffectiveStance(g, charID) {
	if (!g || !g.characters[charID]) return 'absent';
	if (g.characters[charID].seized || g.characters[charID].bitten || g.characters[charID].drained) return 'forward';
	if (g.characters[charID].knockback && gameHasConfigFlag(g, 'knockback-defensive')) return 'defensive';
	if (g.characters[charID].dreaming) return 'absent';
	return g.characters[charID].stance;
}

// REPORT COMMAND -------------------------------------------------------------

// all reports
function reportCommand(userID, channelID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	// figure out the report type -- whichever is the first one chosen
	let type = null;
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (type == null && data.options && data.options[i] && data.options[i].value) type = data.options[i].value;
	}

	if (type == null) {
		// nothing chosen -- make a list of the available reports to choose
		let reportTypes = slashcommands.dumpOptions('report', locale);
		let r = '**' + slashcommands.localizedString(locale, 'report-available', []) + '**:\n';
		for (let rType in reportTypes) {
			r += ':large_orange_diamond: **' + rType + '**:\n';
			reportTypes[rType].forEach(rpt => {
				r += '  :small_orange_diamond:' + rpt + '\n';
			});
		}
		return r;
	}
	return reports.generateReport(locale, g, type, isPlayerLoremasterOfGame(userID, g));
}

// MACRO COMMAND --------------------------------------------------------------

function macroCommand(interaction, userID, locale, data) {
	if (getUserConfig(userID, 'saveLast') == false)	return slashcommands.localizedString(locale, 'macros-nosavelast', []);
	if (userConfig[userID] == null || userConfig[userID] == undefined) userConfig[userID] = {};
	if (userConfig[userID].macros == null || userConfig[userID].macros == undefined) userConfig[userID].macros = {};

	let cmd = data.options[0].name;
	if (cmd == 'list') {
		if (isEmpty(userConfig[userID].macros)) return slashcommands.localizedString(locale, 'macros-none', []);
		let r = slashcommands.localizedString(locale, 'macros-list', []) + '\n';
		for (let m in userConfig[userID].macros) {
			r += '**`' + stringutil.padRight(m,20,' ') + '`** : `' + macroDisplayable(userConfig[userID].macros[m]) + '`\n';
		}
		return r;
	}
	
	let name = null;
	if (data.options && data.options[0].options && data.options[0].options[0] && data.options[0].options[0].value) name = data.options[0].options[0].value;
	if (!name.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [name]);
	name = stringutil.lowerCase(name);

	if (cmd == 'create') {
		if (userConfig[userID].macros[name] != null) return slashcommands.localizedString(locale, 'macros-exists', [name, macroDisplayable(userConfig[userID].macros[name])]);
		let lastCmd = getUserConfig(userID,'lastSlashCmd');
		if (lastCmd == null) {
			return slashcommands.localizedString(locale, 'error-nolastcmd', []);
		}
		userConfig[userID].macros[name] = [lastCmd];
		filehandling.saveStateData(userConfig);
		return slashcommands.localizedString(locale, 'macros-created', [name, macroDisplayable(userConfig[userID].macros[name])]);
	}

	if (cmd == 'delete') {
		// no partial match; for delete you have to get it exactly
		if (userConfig[userID].macros[name] == null) return slashcommands.localizedString(locale, 'macros-notexists', [name]);
		let r = macroDisplayable(userConfig[userID].macros[name]);
		delete userConfig[userID].macros[name];
		filehandling.saveStateData(userConfig);
		return slashcommands.localizedString(locale, 'macros-deleted', [name, r]);
	}

	// look for partial matches
	if (userConfig[userID].macros[name] == null) {
		let newName = stringutil.findMatchInList(Object.keys(userConfig[userID].macros), name);
		if (newName == '') return slashcommands.localizedString(locale, 'macros-notexists', [name]);
		name = newName;
	}

	if (cmd == 'run') {
		let r = [slashcommands.localizedString(locale, 'macros-executed', [name, macroDisplayable(userConfig[userID].macros[name])])];
		userConfig[userID].macros[name].forEach(d => {
			let thisInteraction = {}; 
			thisInteraction = Object.assign(thisInteraction,interaction);
			thisInteraction.data = Object.assign(thisInteraction.data,d);
			let response = executeSlashCommand(thisInteraction, true, null);
			if (Array.isArray(response)) response.forEach(s => {r.push(s)});
			else r.push(response);
		});
		return r;
	}

	if (cmd == 'append') {
		let lastCmd = getUserConfig(userID,'lastSlashCmd');
		if (lastCmd == null) {
			return slashcommands.localizedString(locale, 'error-nolastcmd', []);
		}
		userConfig[userID].macros[name].push(lastCmd);
		filehandling.saveStateData(userConfig);
		return slashcommands.localizedString(locale, 'macros-appended', [name, macroDisplayable(userConfig[userID].macros[name])]);
	}
}

// takes a macro (an array of interaction.data) and provides a display form
function macroDisplayable(macro) {
	let r = [];
	macro.forEach(slashcmd => {
		r.push(slashCmdDisplayable(slashcmd));
	});
	return r.join('; ');
}

// takes an interaction.data and provides a display form
function slashCmdDisplayable(slashcmd) {
	let r = '/' + slashcmd.name;
	if (slashcmd.options) for (let i = 0; i < slashcmd.options.length; i++) {
		if (slashcmd.options[i].name) r += ' ' + slashcmd.options[i].name;
		if (slashcmd.options[i].value) r += ' ' + slashcmd.options[i].value;
		if (slashcmd.options[i].options) for (let j = 0; j < slashcmd.options[i].options.length; j++) {
			if (slashcmd.options[i].options[j].name) r += ' ' + slashcmd.options[i].options[j].name;
			if (slashcmd.options[i].options[j].value) r += ' ' + slashcmd.options[i].options[j].value;
			// no need to go another level since no Narvi slash command goes another level deep
		}
	}
	return r;
}

// SKILL FUNCTIONS -------------------------------------------------------------

function skillNameDisplay(skillName, favored, width) {
	if (skillName == undefined) skillName = '';
	return (favored ? '__' : '') + '`' + stringutil.padRight(skillName, width, ' ') + '`' + (favored ? '__' : '') + ' ';
}

// extract the numeric skill value ignoring if favored
function skillValue(skillScore) {
	if (skillScore == '' || skillScore == undefined) return 0;
	if (String(skillScore).includes('f')) return Number(skillScore.slice(0,-1));
	return Number(skillScore);
}

// extract whether the skill is favored
function skillFavored(skillScore) {
	if (skillScore == '' || skillScore == undefined) return false;
	if (String(skillScore).includes('f')) return true;
	return false;
}

// calculate odds of autosuccess, autofailure, and approximate odds of success if neither happens
function calculateDiceOdds(favored, success, tn, weary, miserable) {
	// reduce the TN by 3.5 per success, or 2.5 if weary -- a simplistic algorithm but adequate I hope for these purposes
	let featTN = tn - (weary ? 2.5 : 3.5) * success;
	// now figure out the odds of the feat roll beating whatever's left
	let odds = (12-featTN) / 12;
	if (odds < 0) odds = 0;
	if (odds > 1) odds = 1;
	return [(favored ? .1597 : .0833), (miserable ? 0 : (favored ? .0069 : .0833)), odds];
}

// determines if one skill has a better value than the other; true means first is better, false means second is
function betterSkill(g, charID, skill1, skill2, doTengwarsMatter, isEyeCatastrophic, bonus, weary, miserable, tn, feat, noitem) {
	let favored1, success1, tn1, favored2, success2, tn2;

	// if not overriden, determine character status
	if (weary === null) weary = characterWeary(g, charID,null);
	if (miserable === null) miserable = characterMiserable(g, charID);

	// look up skills
	let skillScore1 = characterDataFetch(g, charID, 'sheet', skill1);
	let skillScore2 = characterDataFetch(g, charID, 'sheet', skill2);

	// figure out favored -- but we don't care about ill-favored since it would only happen equally to both
	if (feat == 'favoured') {
		favored1 = true;
		favored2 = true;
	} else if (feat == 'illfavoured' || feat == 'unfavoured') {
		favored1 = false;
		favored2 = false;
	} else {
		favored1 = skillFavored(skillScore1);
		favored2 = skillFavored(skillScore2);
	}

	// figure out success dice
	success1 = skillValue(skillScore1) + bonus;
	if (!noitem && getUsefulItemFromExternalCache(g.characters[charID].keyType, g.characters[charID].repositoryKey, skill1) != '') success1++;
	if (getBlessingFromExternalCache(g.characters[charID].keyType, g.characters[charID].repositoryKey, skill1) != '') success1 += 2;
	success2 = skillValue(skillScore2) + bonus;
	if (!noitem && getUsefulItemFromExternalCache(g.characters[charID].keyType, g.characters[charID].repositoryKey, skill2) != '') success2++;
	if (getBlessingFromExternalCache(g.characters[charID].keyType, g.characters[charID].repositoryKey, skill2) != '') success2 += 2;

	// figure out TNs
	if (tn === null) tn1 = charSkillTN(g, charID, skill1); else tn1 = tn;
	if (tn === null) tn2 = charSkillTN(g, charID, skill2); else tn1 = tn;

	// if one is favored and the other not, and eyes are catastrophic
	// that is always the most important option, since it drops the odds of an Eye from 8.3% to 0.7%
	if (isEyeCatastrophic) {
		if (favored1 && !favored2) return true;
		if (!favored1 && favored2) return false;
	}

	// calculate the odds of a success, and the average number of tengwar if they matter
	let [autosuccess1, autofailure1, succeed1] = calculateDiceOdds(favored1, success1, tn1, weary, miserable);
	let [autosuccess2, autofailure2, succeed2] = calculateDiceOdds(favored2, success2, tn2, weary, miserable);

	// calculate a final score for each: odds of success minus odds of autofailure
	let finalScore1 = autosuccess1 + succeed1 - autofailure1;
	let finalScore2 = autosuccess2 + succeed2 - autofailure2;
	if (doTengwarsMatter) { // a tengwar is worth half as much as success
		finalScore1 += success1 / 12;
		finalScore2 += success2 / 12;
	}
	//console.log('comparing **' + skill1 + '** (' + favored1 + ',' + success1 + ',' + tn1 + ') = **' + finalScore1 + '** to **' + skill2 + ' (' + favored2 + ',' + success2 + ',' + tn2 + ') = **' + finalScore2 + '**');
	return (finalScore1 > finalScore2);
}

// SHEET COMMANDS --------------------------------------------------------------

// handles the character sheet command
function sheetCommand(userID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);

	let charID = currentCharacter(userID), show = 'default';
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'show') show = data.options[i].value;
		if (data.options[i].name == 'character') {
			charID = findCharacterInGame(g, data.options[i].value);
			if (!charID) return slashcommands.localizedString(locale, 'error-charnotfound', [data.options[i].value]);
			if (!isPlayerLoremasterOfGame(userID, g) && charID != currentCharacter(userID)) return slashcommands.localizedString(locale, 'error-lmonly', []);
		}
	}
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	if (!isTOR(g)) return scribblesDisplay(g, charID, locale, true);

	/* for testing betterSkill
	for (let i = 0; i < skillList.length; i++) {
		for (let j = i + 1; j < skillList.length; j++) {
			betterSkill(g, charID, skillList[i], skillList[j], true, false, 0, null, null, null, null);
		}
	}
	*/

	if (show == 'virtues') return sheetDisplay2eVirtuesRewards(g, charID, locale, true, true);
	if (show == 'rewards') return sheetDisplay2eVirtuesRewards(g, charID, locale, false, true);
	if (show == 'useful') return sheetDisplay2eUsefulItems(g, charID, locale, true);
	if (show == 'weapons') return sheetWeaponList(g, charID, locale);
	if (show == 'compact') return sheetDisplay2e(g, charID, locale, true, false, true);
	return sheetDisplay2e(g, charID, locale, false, show == 'skills');
}

// SHEET 2E DISPLAY -----------------------------------------------------------
// 7 emoji take up the same width as 20 fixed width characters almost exactly.
// 3 emoji and four spaces take the same as 10 fixed width characters almost exactly.
// 6 pips plus a space = 18 characters in width almost exactly.

// figure out the widest this column will get (for full, do not include the emoji that starts every line)
function sheetDisplay2eColumnWidths(g, p, locale, column, compact, skillsOnly) {
	let width = 0;
	if (column == 1) {
		if (!skillsOnly) {
			width = Math.max(width, characterDataFetchHandleNulls(g, p, 'culture', null).length);
			width = Math.max(width, characterDataFetchHandleNulls(g, p, 'cultureblessing', null).length);
			width = Math.max(width, characterDataFetchHandleNulls(g, p, 'calling', null).length);
			width = Math.max(width, slashcommands.localizedString(locale, 'stat-strength', []).length + 9);
			width = Math.max(width, slashcommands.localizedString(locale, 'stat-parry', []).length + 3);
			proficiencyList.forEach(prof => {
				width = Math.max(width, slashcommands.localizedString(locale, 'proficiency-'+stringutil.lowerCase(prof), []).length + (compact ? 2 : 18));
			});
		}
		for (let i=0; i < skillList.length; i += 3) {
			width = Math.max(width, slashcommands.localizedString(locale, 'skill-'+stringutil.lowerCase(skillList[i]), []).length + (compact ? 4 : 18));
		}
	} else {
		if (!skillsOnly) {
			if (characterDataFetchHandleNulls(g, p, 'woundtreated', null) == 'treated' || characterDataFetchHandleNulls(g, p, 'woundtreated', null) == 'failed') {
				width = Math.max(width, slashcommands.localizedString(locale, 'token-treated', []).length + 6 + slashcommands.localizedString(locale, 'token-daysleft', []).length);
			} else if (characterDataFetchHandleNulls(g, p, 'wounded', null) == 'TRUE') {
				width = Math.max(width, slashcommands.localizedString(locale, 'token-wounded', []).length + 6 + slashcommands.localizedString(locale, 'token-daysleft', []).length);
			}
			width = Math.max(width, characterDataFetchHandleNulls(g, p, 'shadowpath', null).length);
			width = Math.max(width, characterDataFetchHandleNulls(g, p, 'standardofliving', null).length + 6);
			width = Math.max(width, slashcommands.localizedString(locale, 'stat-heart', []).length + 8);
			width = Math.max(width, slashcommands.localizedString(locale, 'stat-valour', []).length + 3);
			for (let w in characterDataFetchHandleNulls(g, p, 'weapons', null)) {
				width = Math.max(width, w.length + 14);
			}
		}
		for (let i=1; i < skillList.length; i += 3) {
			width = Math.max(width, slashcommands.localizedString(locale, 'skill-'+stringutil.lowerCase(skillList[i]), []).length + (compact ? 4 : 18));
		}
	}
	return width;
}

// do a padded display with a character data lookup
function sheetDisplay2ePaddedValue(g, p, prefix, field, suffix, width) {
	let r = prefix + characterDataFetchHandleNulls(g, p, field, null) + suffix;
	return stringutil.padRightIgnoreBold(r, width, ' ');
}

// display the attributes line
function sheetDisplay2eAttributes(g, p, locale, col1, col2, compact) {
	let r = '';
	if (!compact) r += ':muscle:';
	r += '`' + sheetDisplay2ePaddedValue(g, p, stringutil.titleCase(slashcommands.localizedString(locale, 'stat-strength', [])) + ' ', 'strength', ' (TN' + characterDataFetchHandleNulls(g, p, 'strengthtn', null) + ')', col1) ;
	if (!compact) r += '`:heart:`';
	r += sheetDisplay2ePaddedValue(g, p, stringutil.titleCase(slashcommands.localizedString(locale, 'stat-heart', [])) + ' ', 'heart', ' (TN' + characterDataFetchHandleNulls(g, p, 'hearttn', null) + ')', col2);
	if (!compact) r += '`:candle:`';
	r += stringutil.titleCase(slashcommands.localizedString(locale, 'stat-wits', [])) + ' ' + characterDataFetchHandleNulls(g, p, 'wits', null) + ' (TN' + characterDataFetchHandleNulls(g, p, 'witstn', null) + ')`';
	return r;
}

// determine the longest length of a particular kind of war gear (weapon or armor)
function sheetDisplay2eWargearWidth(g,p,weapon) {
	let w = 0;
	for (let i=1; i<=4; i++) {
		itemname = characterDataFetchHandleNulls(g,p, (weapon ? 'weapon' : 'armour') + String(i) + 'name', null);
		if (itemname && itemname.length > w) w = itemname.length;
	}
	return w;
}

// determine the longest length of a particular column of skills
function sheetDisplay2eSkillWidth(g,p,locale,column) {
	let w = 0;
	for (let i=column-1; i<skillList.length; i += 3) {
		w = Math.max(w, slashcommands.localizedString(locale, 'skill-'+stringutil.lowerCase(skillList[i]), []).length);
	}
	return w;
}

// display a weapon
function sheetDisplay2eWeapon(g,p,i,width) {
	let r = stringutil.padRight(characterDataFetchHandleNulls(g,p, 'weapon' + String(i) + 'name', null), width + 1, ' ');
	r += characterDataFetchHandleNulls(g,p, 'weapon' + String(i) + 'dmg', null) + ' ';
	r += stringutil.padLeft(characterDataFetchHandleNulls(g,p, 'weapon' + String(i) + 'edge', null), 2, ' ') + ' ';
	r += stringutil.padLeft(characterDataFetchHandleNulls(g,p, 'weapon' + String(i) + 'injury', null), 2, ' ');
	return r;
}

// display a piece of armor
function sheetDisplay2eArmour(g,p,i,width) {
	let r = stringutil.padRight(characterDataFetchHandleNulls(g,p, 'armour' + String(i) + 'name', null), width + 1, ' ');
	r += characterDataFetchHandleNulls(g,p, 'armour' + String(i) + 'protect', null) + ' ';
	r += characterDataFetchHandleNulls(g,p, 'armour' + String(i) + 'bonus', null);
	return r;
}

// main function that handles compact, regular, and skillsOnly 2e character sheet displays
function sheetDisplay2e(g, p, locale, compact, skillsOnly, full) {
	if (characterDataFetchHandleNulls(g, p, 'name', null) == '') return slashcommands.localizedString(locale, 'error-charnotloaded', []);

	let col1 = sheetDisplay2eColumnWidths(g, p, locale, 1, compact, skillsOnly) + 1;
	let col2 = sheetDisplay2eColumnWidths(g, p, locale, 2, compact, skillsOnly);

	let r = '';
	if (!compact) r += g.characters[p].active ? ':scroll:' : ':x:';
	r += '**' + characterDataFetchHandleNulls(g, p, 'name', null) + '**\n';

	if (!skillsOnly) {
		if (!compact) r += ':flag_white:';
		r += '`' + sheetDisplay2ePaddedValue(g, p, '', 'culture', '', col1);
		let wounded = '';
		if (characterDataFetchHandleNulls(g, p, 'wounded', null) == 'TRUE') {
			if (!compact) {
				r += '`';
				let treated = characterDataFetchHandleNulls(g, p, 'woundtreated', null);
				if (treated == 'treated') r += ':adhesive_bandage:';
				else if (treated == 'failed') r += ':drop_of_blood:';
				else r += ':red_circle:';
				r += '`';
			}
			wounded = stringutil.titleCase(slashcommands.localizedString(locale, 'token-wounded', []));
			if (characterDataFetchHandleNulls(g, p, 'woundtreated', null) == 'TRUE') wounded = stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-treated', []));
			wounded += ' (' + characterDataFetchHandleNulls(g, p, 'wounddaysleft', null) + ' ' + slashcommands.localizedString(locale, 'token-daysleft', []) + ')';
		} else if (!compact) r += '`:white_large_square:`';
		r += stringutil.padRight(wounded, col2, ' ');
		if (!compact) r += '`:eight_pointed_black_star:`';
		r += characterDataFetchHandleNulls(g, p, 'feature1', null) + '`\n';

		if (!compact) r += ':busts_in_silhouette:';
		r += '`' + sheetDisplay2ePaddedValue(g, p, '', 'cultureblessing', '', col1);
		if (!compact) r += '`:volcano:`';
		r += sheetDisplay2ePaddedValue(g, p, '', 'shadowpath', '', col2);
		if (!compact) r += '`:eight_pointed_black_star:`';
		r += characterDataFetchHandleNulls(g, p, 'feature2', null) + '`\n';

		if (!compact) r += ':flag_black:';
		r += '`' + sheetDisplay2ePaddedValue(g, p, '', 'calling', '', col1);
		if (!compact) r += '`:moneybag:`';
		r += sheetDisplay2ePaddedValue(g, p, '', 'standardofliving', ' (' + characterDataFetchHandleNulls(g, p, 'treasure', null) + ')', col2);
		if (!compact) r += '`:eight_pointed_black_star:`';
		r += characterDataFetchHandleNulls(g, p, 'feature3', null) + '`\n';

		r += sheetDisplay2eAttributes(g, p, locale, col1, col2, compact) + '\n';

		if (!compact) r += ':fencer:';
		r += '`' + sheetDisplay2ePaddedValue(g, p, stringutil.titleCase(slashcommands.localizedString(locale, 'stat-parry', [])) + ' ', 'parry', '', col1);
		if (!compact) r += '`:lion:`';
		r += sheetDisplay2ePaddedValue(g, p, stringutil.titleCase(slashcommands.localizedString(locale, 'stat-valour', [])) + ' ', 'valour', '', col2);
		if (!compact) r += '`:owl:`';
		r += stringutil.titleCase(slashcommands.localizedString(locale, 'stat-wisdom', [])) + ' ' + characterDataFetchHandleNulls(g, p, 'wisdom', null) + '`\n';
	}

	let col = 1;
	let swidth = [0,0,0];
	swidth[0] = sheetDisplay2eSkillWidth(g,p,locale,1);
	swidth[1] = sheetDisplay2eSkillWidth(g,p,locale,2);
	swidth[2] = sheetDisplay2eSkillWidth(g,p,locale,3);
	for (let i=0; i < skillList.length; i++) {
		let skillVal = characterDataFetchHandleNulls(g, p, skillList[i], null);
		let skillFavored = skillVal.endsWith('f');
		let skillName = stringutil.titleCase(slashcommands.localizedString(locale, 'skill-'+stringutil.lowerCase(skillList[i]), []));
		if (compact) {
			if (col == 1) r += '`';
			if (skillFavored) r += '\u221A '; else r += '_ ';
			if (col != 3)
				r += stringutil.padRight(skillName + ' ' + skillValue(skillVal), (col == 1 ? col1 : col2) - 2, ' ');
			else
				r += skillName + ' ' + skillValue(skillVal) + '`';
		} else {
			if (skillFavored) r += ':white_check_mark:'; else r += ':green_square:';
			r += '`'+ stringutil.padRight(skillName, swidth[col-1], ' ') + '` ';
			r += emoji.sheetSkillPips(skillValue(skillVal));
			if (col == 1 && col1 > col[0]+18) r += repeatString(' ', col1-col[0]-18);
			if (col == 2 && col2 > col[1]+18) r += repeatString(' ', col2-col[1]-18);
		}
		if (col == 3) r += '\n';
		col++;
		if (col > 3) col = 1;
	}

	if (!skillsOnly) {
		let row = 1;
		let weaponWidth = sheetDisplay2eWargearWidth(g,p,true);
		let armourWidth = sheetDisplay2eWargearWidth(g,p,false);
		let profWidth = swidth[0];
		proficiencyList.forEach(p => {
			profWidth = Math.max(profWidth, slashcommands.localizedString(locale, 'proficiency-'+stringutil.lowerCase(p), []).length);
		});
		let profEmoji = [':axe:',':bow_and_arrow:',':oden:',':crossed_swords:'];
		proficiencyList.forEach(proficiency => {
			let pName = stringutil.titleCase(slashcommands.localizedString(locale, 'proficiency-'+stringutil.lowerCase(proficiency), []));
			if (compact) {
				r += '`' + sheetDisplay2ePaddedValue(g, p, pName + ' ', pName, '', col1);
			} else {
				r += profEmoji.shift();
				r += '`'+ stringutil.padRight(pName, profWidth, ' ') + '`';
				r += emoji.sheetSkillPips(characterDataFetchHandleNulls(g,p,proficiency,null)) + ' ';
			}
			if (!compact) r += ':dagger:`';
			r += stringutil.padRight(sheetDisplay2eWeapon(g,p,row,weaponWidth), col2, ' ');
			if (!compact) r += '`:shield:`';
			r += sheetDisplay2eArmour(g,p,row,armourWidth) + '`\n'; 
			row++;
		});

		if (compact) r += sheetDisplay2eStatusCompact(g, p, locale);
		else r += tokenStatusDisplay(g, p, locale, true);
	}

	if (full) {
		r += sheetDisplay2eVirtuesRewards(g,p,locale,true, false);
		r += sheetDisplay2eVirtuesRewards(g,p,locale,false,false);
		r += sheetDisplay2eUsefulItems(g,p,locale,false);
	}

	// add any scribbles
	if (!compact && !skillsOnly && Object.keys(g.characters[p].scribbles).length > 0) r += '\n**' + stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'scribbles')) + '**:\n' + scribblesDisplay(g, p, locale, false);

	return r;
}

// a compact form of the hope and endurance lines for the compact 2e sheet
function sheetDisplay2eStatusCompact(g, p, locale) {
	let r = '';

	r += '`' + stringutil.titleCase(slashcommands.localizedString(locale, 'token-endurance', [])) + ': ';
	r += slashcommands.localizedString(locale, 'maximum', []) + ' ' + characterDataFetchHandleNulls(g,p,'maxendurance',null) + ', ';
	r += slashcommands.localizedString(locale, 'token-fatigue', []) + ' ' + characterDataFetchHandleNulls(g,p,'fatigue',null) + ', ';
	r += slashcommands.localizedString(locale, 'token-encumbrance', []) + ' ' + characterDataFetchHandleNulls(g,p,'encumbrance',null) + ', ';
	r += slashcommands.localizedString(locale, 'token-load', []) + ' ' + characterDataFetchHandleNulls(g,p,'load',null) + ', ';
	r += slashcommands.localizedString(locale, 'current', []) + ' ' + characterDataFetchHandleNulls(g,p,'endurance',null) + ', ';
	r += slashcommands.localizedString(locale, 'available', []) + ' ' + characterDataFetchHandleNulls(g,p,'availendurance',null);
	if (characterWeary(g,p,null)) r += ', '+ slashcommands.localizedString(locale, 'condition-weary', []);
	r += '`\n';

	r += '`' + stringutil.titleCase(slashcommands.localizedString(locale, 'token-hope', [])) + ': ';
	r += slashcommands.localizedString(locale, 'maximum', []) + ' ' + characterDataFetchHandleNulls(g,p,'maxhope',null) + ', ';
	r += slashcommands.localizedString(locale, 'token-shadowscars', []) + ' ' + characterDataFetchHandleNulls(g,p,'shadowscars',null) + ', ';
	r += slashcommands.localizedString(locale, 'token-shadow', []) + ' ' + characterDataFetchHandleNulls(g,p,'shadow',null) + ', ';
	r += slashcommands.localizedString(locale, 'total', []) + ' ' + characterDataFetchHandleNulls(g,p,'totalshadow',null) + ', ';
	r += slashcommands.localizedString(locale, 'current', []) + ' ' + characterDataFetchHandleNulls(g,p,'hope',null) + ', ';
	r += slashcommands.localizedString(locale, 'available', []) + ' ' + characterDataFetchHandleNulls(g,p,'availhope',null);
	r += '`' + hopeStatus(g, p, locale) + '\n';
	return r;

}

// display of just virtues or rewards
function sheetDisplay2eVirtuesRewards(g,p,locale,virtues,complete) {
	if (characterDataFetchHandleNulls(g, p, 'name', null) == '') return slashcommands.localizedString(locale, 'error-charnotloaded', []);

	let r = '';
	if (complete) r += '**' + characterDataFetchHandleNulls(g, p, 'name', null) + '** ';
	r += '**' + stringutil.titleCase(slashcommands.localizedString(locale, 'stat-' + (virtues ? 'virtues' : 'rewards'), [])) + '**:\n';
	for (let i=1; i<=8; i++) {
		let s = characterDataFetchHandleNulls(g, p, (virtues ? 'virtue' : 'reward') + String(i), null);
		if (s != '' && s != undefined) r += ':eight_pointed_black_star: ' + s + '\n';
	}
	return r;
}

function skillPipsForItems(g, p, skill, abbreviated) {
	let r = emoji.sheetSkillPips(skillValue(characterDataFetchHandleNulls(g, p, 'sheet',skill)),abbreviated);
	let f = ':green_square:';
	if (skillFavored(characterDataFetchHandleNulls(g, p, 'sheet',skill))) f = ':white_check_mark:';
	let item = getUsefulItemFromExternalCache(g.characters[p].keyType, g.characters[p].repositoryKey, skill);
	if (item != '') r += ':tools:';
	item = getBlessingFromExternalCache(g.characters[p].keyType, g.characters[p].repositoryKey, skill);
	if (item != '') r += ':magic_wand:';
	return f + r;
}

// display of just useful items
function sheetDisplay2eUsefulItems(g,p,locale,complete) {
	if (characterDataFetchHandleNulls(g, p, 'name', null) == '') return slashcommands.localizedString(locale, 'error-charnotloaded', []);

	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'sheet-usefulitem', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'skill', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'value', [])), emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	for (let i = 1; i <= 4; i++) {
		let item = characterDataFetchHandleNulls(g, p, 'usefulitem' + i, null);
		if (item == '') continue;
		reportData.push([
			item,
			characterDataFetchHandleNulls(g, p, 'usefulitem' + i + 'skill', null),
			skillPipsForItems(g, p, characterDataFetchHandleNulls(g, p, 'usefulitem' + i + 'skill', null))
		]);
	}
	return (complete ? ('**' + characterDataFetchHandleNulls(g, p, 'name', null) + '** ' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'sheet-usefulitem', [])) + ':\n') : '') + table.buildTable(reportHeadings, reportData, 2);
}

// handles the \sheet weapon command
function sheetWeaponList(g, p, locale) {
	let r = '';
	
	r = slashcommands.localizedString(locale, 'sheet-weaponlist', [characterDataFetchHandleNulls(g, p, 'name', null)]) + ':\n';
	let w = 0;
	for (let weaponName in characterDataFetch(g, p, 'weapons', null)) {
		w = Math.max(w,weaponName.length);
	}
	for (let weaponName in characterDataFetch(g, p, 'weapons', null)) {
		r += sheetWeaponDisplay(g, p, weaponName, false, locale, w) + '\n';
	}
	return r;
}

// displays a single weapon for either character sheet form
function sheetWeaponDisplay(g, p, weaponName, compact, locale, width) {
	let r;
	let weaponArray = characterDataFetch(g, p, 'weapons', weaponName);
	if (weaponArray == null) return;
	let w = 0;
	for (let i=0; i < skillList.length; i++) {
		w = Math.max(w, slashcommands.localizedString(locale, 'skill-'+stringutil.lowerCase(skillList[i]), []).length);
	}

	r = skillNameDisplay(weaponName, String(weaponArray[0]).includes('f'), ((width != 0) ? width : w));
	if (compact) {
		r += ': **`' + skillValue(weaponArray[0]) + '`** :white_small_square: ';
	} else {
		if (weaponName == '' && weaponArray[0] == 0) {
			r += stringutil.repeatString(' ',37);
		} else {
			r += emoji.sheetSkillPips(skillValue(weaponArray[0]));
		}
	}
	r += ' ';
	// dmg edge injury
	r += '`' + slashcommands.localizedString(locale, 'sheet-dmg', []) + ' ' + stringutil.padRight(String(weaponArray[1]),2,' ') + '  ';
	r += slashcommands.localizedString(locale, 'sheet-edge', []) + ' ' + (stringutil.lowerCase(weaponArray[2]) == stringutil.lowerCase(emoji.GANDALF) ? '`'+emoji.customEmoji(emoji.GANDALF, 1, 0, false)+' `' : stringutil.padRight(String(weaponArray[2]),4,' ')) + '  ';
	r += slashcommands.localizedString(locale, 'sheet-inj', []) + ' ' + stringutil.padRight(String(weaponArray[3]),2,' ');
	let l = slashcommands.localizedString(locale, 'sheet-dmg', []).length + slashcommands.localizedString(locale, 'sheet-edge', []).length + slashcommands.localizedString(locale, 'sheet-inj', []).length;
	if (l < 13) r += stringutil.repeatString(' ',13-l);
	r += '`';
	return r;
}

// SCRIBBLES ------------------------------------------------------------------

// handles the character sheet command
function scribblesCommand(userID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);

	let cmd = data.options[0].name;
	if (!currentCharacter(userID)) return slashcommands.localizedString(locale, 'error-nocharacter', []);

	if (cmd == 'set' || cmd == 'gain' || cmd == 'lose') return scribblesSet(g, currentCharacter(userID), locale, data, cmd);
	if (cmd == 'delete') return scribblesDelete(g, currentCharacter(userID), locale, data);
	if (cmd == 'list') {
		let r = scribblesDisplay(g, currentCharacter(userID), locale, true);
		if (r != '') return r;
		return slashcommands.localizedString(locale, 'scribbles-none', []);
	}
}

// find a scribble
function scribblesFind(g, charID, scribbleName, substringMatch) {

	// exact match
	for (let scribble in g.characters[charID].scribbles) {
		if (stringutil.caseInsensitiveMatch(scribble,scribbleName)) return scribble;
	}

	// substring match
	if (!substringMatch) return null;
	for (let scribble in g.characters[charID].scribbles) {
		if (stringutil.lowerCase(scribble).startsWith(stringutil.lowerCase(scribbleName))) return scribble;
	}

	return null;
}

// create or update scribble values
function scribblesSet(g, charID, locale, data, cmd) {

	// parse the arguments
	let scribbleName = null, scribbleValue = 1;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'name') scribbleName = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'value') scribbleValue = data.options[0].options[i].value;
	}

	if (!scribbleName.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [scribbleName]);
	if (cmd == 'gain') scribbleValue = Number(scribbleValue);
	if (cmd == 'lose') scribbleValue = -1 * Number(scribbleValue);

	// look for an existing value with the same name to update
	let findScribble = scribblesFind(g, charID, scribbleName, true);
	if (findScribble != null) {
		if (cmd != 'set' && !isNaN(g.characters[charID].scribbles[findScribble])) scribbleValue += Number(g.characters[charID].scribbles[findScribble]);
		g.characters[charID].scribbles[findScribble] = stringutil.mildSanitizeString(String(scribbleValue));
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'scribble-set', [characterDataFetchHandleNulls(g, charID, 'name', null), findScribble, scribbleValue]);
	}

	// add a new value, save, and return a corresponding message
	g.characters[charID].scribbles[scribbleName] = stringutil.mildSanitizeString(String(scribbleValue));
	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'scribble-set', [characterDataFetchHandleNulls(g, charID, 'name', null), scribbleName, scribbleValue]);
}

// delete scribble values
function scribblesDelete(g, charID, locale, data) {
	let scribbleName = null;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'name') scribbleName = data.options[0].options[i].value;
	}

	// look for an existing value with this name
	for (let scribble in g.characters[charID].scribbles) {
		if (stringutil.caseInsensitiveMatch(scribble,scribbleName)) {
			let scribbleValue = g.characters[charID].scribbles[scribble];
			delete g.characters[charID].scribbles[scribble];
			filehandling.saveGameData(gameData);
			return slashcommands.localizedString(locale, 'scribble-deleted', [characterDataFetchHandleNulls(g, charID, 'name', null), scribble, scribbleValue]);
		}
	}
	return slashcommands.localizedString(locale, 'scribble-notfound', [characterDataFetchHandleNulls(g, charID, 'name', null), scribbleName]);
}

// display scribble values on a sheet or standalone
function scribblesDisplay(g, charID, locale, withHeader) {
	let reportHeadings = [
		{title:'', emoji:false, minWidth:0, align:'left'},
		{title:'', emoji:false, minWidth:0, align:'left'}
		]; // blank heading so it won't display but still defines the columns
	let reportData = [];
	for (let scribble in g.characters[charID].scribbles) {
		reportData.push([
			scribble, g.characters[charID].scribbles[scribble]
		]);
	}
	// if there are no scribbles on this character just return ''
	if (reportData.length == 0) return '';
	// if a header is specified, add a name header (this is a stand-alone display)
	return (withHeader ? ('**' + characterDataFetchHandleNulls(g, charID, 'name', null) + ' ' + slashcommands.localizedCommand(locale, 'scribbles') + '**:\n') : '') + table.buildTable(reportHeadings, reportData, 2);
}

// handle scribble substitutions in a command string
function scribblesSubstitutions(g, charID, command, open, close) {
	command = command.join(' ').trim();
	while (command.includes(open)) {
		// split the string into the bit before the first {, the bit between it and the }, and the remainder
		let openPos = command.indexOf(open);
		let left = command.substring(0,openPos);
		let closePos = command.indexOf(close,openPos);
		if (closePos == -1) break;
		let middle = command.substring(openPos+1,closePos);
		let right = command.substring(closePos+1);

		// find a scribble match for the contents, with substring matching
		let value = scribblesFind(g, charID, middle, true);
		if (value != null) {
			command = left + g.characters[charID].scribbles[value] + right;
		} else {
			command = left + middle + right;
		}
	}
	return command.split(' ');
}

// INITIATIVE COMMANDS ---------------------------------------------------------

const stanceList = ['forward','open','defensive','skirmish','rearward','absent'];

// stance tracking
function stanceCommand(userID, channelID, userName, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID), stance = null;
	// parse options
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'stance') stance = data.options[i].value;
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[i].value);
		}
	}
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);

	let actionDone = '';
	if (stance != null) {
		// can't be skirmish unless strider mode or flag
		if (stance == 'skirmish' && !isStriderMode(g) && !gameHasConfigFlag(g, 'allow-skirmish')) return slashcommands.localizedString(locale, 'error-strideronly', []);
		// can't be changed if combat active and not in phase for stance changes
		if (g.combat.active && g.combat.phase != initiative.PHASE_CHANGESTANCES && g.combat.phase != initiative.PHASE_OPENINGVOLLEY && !gameHasConfigFlag(g, 'unphased-initiative') && !isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'stance-wrongphase', []);
		// check off the action if relevant
		actionDone = initiative.takeInitiativeAction(locale, g, charID, 'stance');
		// set it
		if (stance != 'unchanged') {
			// add to or subtract from actors if relevant
			if (g.combat.active && g.combat.phase == initiative.stanceNameToPhase[stringutil.lowerCase(stance)]) g.combat.actors.push(String(charID));
			if (g.combat.active && g.combat.phase == initiative.stanceNameToPhase[stringutil.lowerCase(g.characters[charID].stance)] && g.combat.actors.includes(String(charID))) g.combat.actors = g.combat.actors.filter(x => x != String(charID));
			// set the stance
			g.characters[charID].stance = stance;
		}
		filehandling.saveGameData(gameData);
	}

	let r = slashcommands.localizedString(locale, 'stance-set', [characterDataFetchHandleNulls(g, charID, 'name', null), stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'stance', 'stance', g.characters[charID].stance))]);
	if (g.combat.active) combat_log(g, r);

	if (actionDone != '') return [r, actionDone];
	return r;
}

// determines if a mod is active
function combatModActive(mod,charID) {
	if (!mod.started && mod.duration != 0) return false;
	if (charID == 0) { // adversaries check
		if (!mod.characters || mod.characters.length == 0 || !mod.characters.includes(0)) return false;
		return true;
	}
	if (charID != null && mod.characters && mod.characters.length > 0 && !mod.characters.includes(Number(charID))) return false;
	return true;
}

function stancePhaseToName(phase) {
	for (let phaseName in initiative.stanceNameToPhase) {
		if (initiative.stanceNameToPhase[phaseName] == phase) return phaseName;
	}
	return String(phase);
}

// a displayable list of active combat mods for a game
function showCombatMods(g, locale, listform) {
	if (g.combatMods.length == 0) return slashcommands.localizedString(locale, 'none', [])
	let r = [];
	let i = 1;
	g.combatMods.forEach(mod => {
		let s = '';
		if (listform) s += '`[' + String(i) + ']` ';
		if (mod.modifier > 0) s += '+';
		s += String(mod.modifier) + 'd';
		let notes = [];
		if (mod.duration > 0) notes.push(String(mod.duration) + ' ' + slashcommands.localizedString(locale, 'combat-rounds', []));
		if (!mod.started) notes.push(slashcommands.localizedString(locale, 'combat-notyet', [stringutil.titleCaseWords(stancePhaseToName(mod.phase))]));
		if (mod.characters && mod.characters.length > 0) {
			let chars = [];
			mod.characters.forEach(p => {
				if (p == 0) notes.push(slashcommands.localizedString(locale, 'adversary-adversaries', []));
				else chars.push(characterDataFetchHandleNulls(g, p, 'name', null))
				});
			if (chars.length > 0) notes.push(chars.join(', '));
		}
		if (notes.length > 0) s += ' (' + notes.join('; ') + ')';
		r.push(s);
		i++;
	});
	if (listform) return r.join('\n');
	return r.join('; ');
}

// commands for modifiers (advantages and complications)
function modifierCommand(userID, channelID, userName, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!g.combat.active) return slashcommands.localizedString(locale, 'error-nocombat', []);

	let cmd = data.options[0].name;

	// if no parameters just show
	if (cmd == 'list') return slashcommands.localizedString(locale, 'combat-modifiers', []) + ':\n' + showCombatMods(g,locale,true);

	// for everything else, must be loremaster
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// clear wipes all modifiers in a game
	if (cmd == 'clear') {
		let snapshot = slashcommands.localizedString(locale, 'combat-modifiers', []) + ': ' + showCombatMods(g,locale,false);
		g.combatMods = [];
		filehandling.saveGameData(gameData);
		return snapshot + '\n' + slashcommands.localizedString(locale, 'combat-modscleared', [])
	}

	let amount = null, duration = null, phase = 1, number = null, chars = [];
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'amount') amount = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'duration') duration = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'phase') phase = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'number') number = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'characters') {
			let charList = stringutil.lowerCase(data.options[0].options[i].value);
			// change commas to spaces
			charList = charList.trim().replace(/,/g,' ');
			// collapse multiple spaces
			charList = charList.replace(/  +/g, ' ');
			// match names and add the IDs
			charList.split(' ').forEach(ch => {
				let charID;
				if (ch == 'adversary' || ch == 'adversaries' || ch == 'foe' || ch == 'foes'
					|| ch == slashcommands.localizedString(locale, 'adversary-adversaries', [])
					|| ch == slashcommands.localizedCommand(locale, 'foe')) charID = 0;
				else charID = findCharacterInGame(g, ch);
				if (charID != null && !chars.includes(charID)) chars.push(Number(charID));
			});
		}
	}

	if (cmd == 'delete') {
		if (number >= 1 && number <= g.combatMods.length) {
			g.combatMods.splice(number - 1, 1);
		}
	} else {
		let started = false;
		if (phase == 0) {
			phase = g.combat.phase;
			started = true;
		}
		g.combatMods.push({'modifier': amount, 'duration': duration, 'phase': phase, 'started': started, 'characters': chars});
	}
	if (g.combat.active) combat_log(g, slashcommands.localizedString(locale, 'combat-modifiers', []) + ':\n' + showCombatMods(g,locale,true));
	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'combat-modifiers', []) + ':\n' + showCombatMods(g,locale,true);
}

// figure out the available combat tasks from a stance, returns [action, skill, error]
function actionsFromStance(g, charID, stance) {
	switch (stance) {
		case 'forward': return ['intimidate', 'awe', null];
		case 'open': return ['rally', 'enhearten', null];
		case 'defensive': return ['protect', 'athletics', null];
		case 'rearward': return ['prepare', 'scan', null];
		case 'skirmish':
			let skill = 'scan';
			if (betterSkill(g, charID, 'athletics', 'scan', true, false, 0, null, null, null, null, true)) skill = 'athletics';
			return ['gainground', skill, null];
		default:
			return [null,null,'action-none'];
	}
}

// stance-related actions lists, and handling the resulting actions themselves
function actionCommand(cmd, userID, channelID, userName, locale, data) {
	let charName = userName, charImage = '', charLink = '';
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID);
	g.lastUsed = Date.now();
	let stance = null;

	// parse the options
	let ally = null, bonus = 0, noitem = true, magical = false, skill = null, riddle = false;
	let dice = null, feat = null, hope = null, modifier = 0, weary = null, miserable = null, lmdice = null, tn = null, comment = '';
	let r = '';

	// parse options
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'ally') ally = data.options[i].value;
		if (data.options[i].name == 'skill') skill = data.options[i].value;
		if (data.options[i].name == 'bonus') bonus = Number(data.options[i].value);
		if (data.options[i].name == 'noitem') noitem = (data.options[i].value == 'no');
		if (data.options[i].name == 'feat') feat = data.options[i].value;
		if (data.options[i].name == 'hope') hope = data.options[i].value;
		if (data.options[i].name == 'modifier') modifier = Number(data.options[i].value);
		if (data.options[i].name == 'weary') weary = data.options[i].value;
		if (data.options[i].name == 'miserable') miserable = data.options[i].value;
		if (data.options[i].name == 'lmdice') lmdice = data.options[i].value;
		if (data.options[i].name == 'riddle') riddle = data.options[i].value;
		if (data.options[i].name == 'tn') tn = Number(data.options[i].value);
		if (data.options[i].name == 'comment') comment = data.options[i].value;
		if (data.options[i].name == 'stance') stance = data.options[i].value;
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[i].value);
		}
	}
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	if (stance == null) stance = charEffectiveStance(g, charID);

	if (cmd == 'actions') {
		r += slashcommands.localizedString(locale, 'action-list', [stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'action', 'list', stance))]) + '\n';
		if (stance == 'absent') r += ' :small_blue_diamond: ' +  slashcommands.localizedString(locale, 'none', []) + '\n';
		else r += ' :small_blue_diamond: ' +  slashcommands.localizedString(locale, 'action-' + stance, []) + '\n';
		return r;
	}

	// now look up the character info, after we have determined who it is
	[charName, charImage, charLink] = getRollCharacterInfo(g, charID, userID);

	// determine if it's the right action for the stance
	let action = null, defaultSkill = null, errorMsg = null;
	[action, defaultSkill, errorMsg] = actionsFromStance(g, charID, stance);
	if (errorMsg) return slashcommands.localizedString(locale, errorMsg, []);
	if (stance != 'skirmish' || skill == null) skill = defaultSkill;
	if (cmd == 'intimidate' && riddle) skill = 'riddle';
	if (cmd != action) {
		return slashcommands.localizedString(locale, 'action-wrong', [slashcommands.localizedOption(locale, 'action', 'list', stance), slashcommands.localizedSubcommand(locale, 'action', cmd), slashcommands.localizedSubcommand(locale, 'action', action)]);
	}
	if (cmd == 'rally' && g.rallynext != 0) return slashcommands.localizedString(locale, 'action-rallyalready',[]);
	if (cmd == 'protect') {
		if (ally) ally = findCharacterInGame(g, ally);
		if (ally == null || ally == charID) return slashcommands.localizedString(locale, 'action-protectnoone',[]);
		if (charEffectiveStance(g, ally) == 'rearward' || charEffectiveStance(g, ally) == 'skirmish' || charEffectiveStance(g, ally) == 'absent') return slashcommands.localizedString(locale, 'action-protectmelee',[]);
	}
	if (cmd == 'prepare' || cmd == 'gainground') {
		if (cmd == 'gainground' && !isStriderMode(g) && !gameHasConfigFlag(g, 'allow-skirmish')) return slashcommands.localizedString(locale, 'error-strideronly',[]);
	}
	if (comment == '') comment = slashcommands.localizedString(locale, 'action-desc-' + cmd,[]);

	// determine the favored, bonus, tn based on the kind of test
	let debugString = '';
	let featToRoll = 1, successToRoll = 0, mods = 0, targetTN = 0, debugStringAddition = '', outputAddition = '';
	[featToRoll, successToRoll, mods, targetTN, debugStringAddition, outputAddition] = torRollLookupSkill(g, charID, skill, noitem, locale);
	debugString += debugStringAddition;
	if (lmdice == null) lmdice = false; // always default to false since even if the LM is rolling, they're rolling for a character

	// handle options common to different kinds of rolls
	[featToRoll, debugString] = torRollHandleFeat(feat, featToRoll, debugString, g.characters[charID].dismayed);
	[successToRoll, mods, targetTN, debugString] = torRollHandleBonuses(bonus, noitem, modifier, tn, targetTN, g, charID, successToRoll, mods, debugString);

	// handle weary and miserable
	[weary, miserable, debugString] = torRollHandleWearyMiserable(g, charID, weary, miserable, debugString, channelID, 'action');

	// handle hope spends
	let warnings = '';
	[hope, magical, successToRoll, warnings, debugString] = torRollHandleHope(g, charID, hope, magical, successToRoll, warnings, debugString, skill, locale);

	let consequences = [], loremasterOutcome = '', result = null;
	let s = initiative.takeInitiativeAction(locale, g, charID, cmd);
	if (s != '') consequences.push(s);

	// do the roll
	let rollResults, rollLine = '**'+ charName + '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' ';
	rollLine += stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'roll', 'skill', skill));
	rollLine += ': ';
	rollResults = torRollDice(featToRoll, successToRoll, 0, mods, weary, miserable, magical, lmdice, hasLifepath(g, charID, 'favoured'), locale, g.dicestyle, gameHasConfigFlag(g, 'magical-tengwar'));
	rollLine += rollResults[3];
	let successes = 0;
	if (rollResults[1] >= 0 && targetTN && (rollResults[0] >= targetTN || rollResults[1] == 1)) successes = rollResults[2] + 1;

	// apply various consequences and effects
	// eye accrual due to rolling Eye
	let [commentadd, radd] = torRollHandleEyeAccrual(g, lmdice, magical, rollResults[4], locale);
	comment += commentadd;
	r += radd;
	// if there's a target, put in a success or failure
	if (targetTN && targetTN != 0) r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', [])) + '__**: ' + compareToTarget(rollResults, targetTN, locale);

	// the action itself
	if (successes > 0) {
		switch (cmd) {
		case 'rally':
			g.rallynext = Math.min(3,successes);
			result = [];
			stanceList.slice(0,successes).forEach(s => {
				result.push(slashcommands.localizedString(locale, 'stance-' + s, []))
			});
			result = result.join(', ');
			consequences.push(slashcommands.localizedString(locale, 'action-rally-success', [result]));
			break;
		case 'protect':
			g.characters[ally].tempparry += successes;
			consequences.push(slashcommands.localizedString(locale, 'action-protect-success', [characterDataFetchHandleNulls(g, ally, 'name', null), successes]));
			break;
		case 'prepare':
		case 'gainground':
			g.characters[charID].tempbonus += successes;
			consequences.push(slashcommands.localizedString(locale, 'action-prepare-success', [successes]));
			break;
		case 'intimidate':
			if (successes == 1) result = 'success';
			else if (successes == 2) result = 'multiple';
			else { 
				result = 'all';
				if (skill != 'riddle') successes = 99; 
				}
			if (g.combat.active && g.combat.encounter != '' && Object.keys(g.encounters[g.combat.encounter].adversaries).length > 0) {
				for (a in g.encounters[g.combat.encounter].adversaries)	{
					if (skill == 'riddle' && encounters.encounters_adversary_has_ability(g, g.combat.encounter, a, 'dull-witted')) {
						g.encounters[g.combat.encounter].adversaries[a].curhrscore -= successes;
						if (g.encounters[g.combat.encounter].adversaries[a].curhrscore < 0) g.encounters[g.combat.encounter].adversaries[a].curhrscore = 0;
						loremasterOutcome += slashcommands.localizedString(locale, 'attack-lm-hrloss', [charName, a, successes, g.encounters[g.combat.encounter].adversaries[a].curhrscore, g.encounters[g.combat.encounter].adversaries[a].hrscore]) + '\n';
					} else if (skill != 'riddle') {
						let might = g.encounters[g.combat.encounter].adversaries[a].might;
						if (encounters.encounters_adversary_has_ability(g, g.combat.encounter, a, 'fearless')) might++;
						if (might <= successes && (magical || !encounters.encounters_adversary_has_ability(g, g.combat.encounter, a, 'heartless'))) {
							g.encounters[g.combat.encounter].adversaries[a].weary++;
							if (encounters.encounters_adversary_has_ability(g, g.combat.encounter, a, 'craven')) {
								encounters.encounters_adversary_damage(g, g.combat.encounter, a, 'curhrscore', 1);
								loremasterOutcome += slashcommands.localizedString(locale, 'attack-lm-hrloss', [charName, a, 2, g.encounters[g.combat.encounter].adversaries[a].curhrscore, g.encounters[g.combat.encounter].adversaries[a].hrscore]) + '\n';
							}
						}
					}


				}
			}
			if (skill == 'riddle') consequences.push(slashcommands.localizedString(locale, 'action-intimidate-riddle', [String(successes)]));
			else consequences.push(slashcommands.localizedString(locale, 'action-intimidate-' + result, []));
			break;
		}
	} else {
		consequences.push(slashcommands.localizedString(locale, 'action-' + cmd + '-failure', []));		
	}
	filehandling.saveGameData(gameData);

	// add any comment
	if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []))  + '__**: ' + comment;

	if (debugString != '') g.debugString = debugString;
	if (gameHasConfigFlag(g, 'always-explain')) consequences.push('**Explanation**:\n' + g.debugString);
	if (loremasterOutcome != '') botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, loremasterOutcome);

	combat_log(g, rollLine + ': ' + r + '\n' + warnings + '\n' + consequences);

	let theEmbed = embeds.buildEmbed(
		charName,
		charImage,
		charLink,
		rollLine,
		r + '\n' + warnings,
		'',
		bot, null,
		getUserConfig(userID,'selectedGame'),
		embeds.BUTTON_GREEN,
		userID
		);

	return [rollResults[5], {embed: theEmbed}, ...consequences];
}

// clears temporary bonuses at the start or end of combat
function wipeTemporaryBonuses(g, end) {
	for (let p2 in g.characters) {
		if (g.characters[p2].stance == null) g.characters[p2].stance = 'open';
		g.characters[p2].tempbonus = 0;
		g.characters[p2].tempparry = 0;
		g.characters[p2].knockback = false;
		g.characters[p2].handedness = '1h';
		g.characters[p2].tengwar = 0;
		g.characters[p2].fendParry = 0;
		g.characters[p2].damageDone = 0;
		g.characters[p2].numEngaged = 0;
		g.characters[p2].dmgReceivedThisRound = 0;
		if (end) {
			g.characters[p2].misfortune = false;
			g.characters[p2].seized = false;
			g.characters[p2].bitten = false;
			g.characters[p2].drained = false;
			g.characters[p2].daunted = false;
			g.characters[p2].haunted = false;
			g.characters[p2].dismayed = false;
			g.characters[p2].bleeding = 0;
			g.characters[p2].weary = 0;
			g.characters[p2].unweary = 0;
			g.characters[p2].miserable = 0;
		}
	}
	g.combatMods = [];
	g.songUsed = '';
	g.songBonus = 0;
	g.songCharacters = [];
}

// clears temporary bonuses at the start or end of combat
function clearUnweary(g) {
	for (let p2 in g.characters) {
		g.characters[p2].unweary = 0;
	}
}

// command for starting an initiative
function startCommand(userID, channelID, userName, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
	if (g.combat.active) return slashcommands.localizedString(locale, 'error-alreadycombat', []);

	// parse options
	let amount = null, duration = null, ambush = null, title = '', encounter = null, copyencounter = false, light = null, surprised = '';
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'ambush') ambush = data.options[i].value;
		if (data.options[i].name == 'title') title = data.options[i].value;
		if (data.options[i].name == 'surprised') surprised = stringutil.lowerCase(data.options[i].value);
		if (data.options[i].name == 'encounter') encounter = data.options[i].value;
		if (data.options[i].name == 'copyencounter') copyencounter = data.options[i].value;
		if (data.options[i].name == 'modifier') amount = Number(data.options[i].value);
		if (data.options[i].name == 'duration') duration = Number(data.options[i].value);
		if (data.options[i].name == 'light') light = data.options[i].value;
	}

	if (encounter != null) {
		// verify it exists
		encounter = stringutil.lowerCase(encounter);
		if (g.encounters[encounter] == null) return slashcommands.localizedString(locale, 'encounter-notexists', [encounter]);
	}

	// start the combat
	g.combat.active = true;
	g.combat.round = 0;
	if (light == 'neither') light = null;
	g.combat.light = light;
	wipeTemporaryBonuses(g, false);

	// if an encounter provided, load it into the combat
	if (encounter != null) {
		// if copyencounter, copy it and switch to the copy
		if (copyencounter) {
			// make a unique name for the copy by postpending a number
			let i = 2, newEncounterName;
			do {
				newEncounterName = encounter + String(i);
				i++;
			} while (g.encounters[newEncounterName] != null);
			let r = encounters.encounters_copy(g, encounter, newEncounterName, locale);
			if (r != '') return slashcommands.localizedString(locale, r, [newEncounterName]);
			encounter = newEncounterName;
			filehandling.saveGameData(gameData);
		}
		// set g.encounter to it, and select it
		g.combat.encounter = encounter;
		g.selectedEncounter = encounter;
		// set title and, if present, modifier
		g.combat.title = g.encounters[encounter].title;
		if (g.encounters[encounter].modifier.modifier != null) {
			g.combatMods.push({'modifier': g.encounters[encounter].modifier.modifier, 'duration': g.encounters[encounter].modifier.duration, 'phase': 1, 'started': false, 'characters': []});
		}
		if (g.encounters[encounter].image) {
			botSendMessage(channelID, g.encounters[encounter].image);
		}
	} else {
		g.combat.encounter = '';
	}

	// if title or modifier provided, set it
	if (title != '' || encounter == null) g.combat.title = title;
	if (amount)	{
		g.combatMods.push({'modifier': amount, 'duration': duration, 'phase': 1, 'started': false, 'characters': []});
	}

	// choose the starting phase
	g.rallycurrent = 0;
	g.rallynext = 0;
	let phase = initiative.PHASE_OPENINGVOLLEY;
	g.combat.log = '';
	combat_log(g, slashcommands.localizedString(locale, 'combat-started', [g.combat.title + (g.combat.title == '' ? '' : ' ')]) + ' ' + stringutil.titleCase(slashcommands.localizedCommand(locale,'encounter')) + ': `' + (g.combat.encounter != '' ? g.combat.encounter : slashcommands.localizedString(locale, 'none',[])) + '` ');
	g.combat.ambush = false;
	g.combat.advsurprise = false;
	if (ambush == 'players') {
		let surprisedChars = [];
		// change commas to spaces
		surprised = surprised.trim().replace(/,/g,' ');
		// collapse multiple spaces
		surprised = surprised.replace(/  +/g, ' ');
		// match names and add the IDs
		surprised.split(' ').forEach(ch => {
			let charID = findCharacterInGame(g, ch);
			if (charID != null && !surprisedChars.includes(charID)) surprisedChars.push(Number(charID));
		});
		if (surprisedChars.length == 0)	{
			// if only some are surprised, just set the ambush flag
			g.combat.ambush = true;
		} else {
			// set those people knocked back but leave the flag off
			surprisedChars.forEach(charID => {
				g.characters[charID].knockback = true;
			});
		}
	}
	// we used to skip to Change Stances on ambush, but what if there are adversaries that have ranged attacks?
	//if (ambush == 'players' || initiative.charactersInPhase(g, phase, false, false).length == 0) phase = initiative.PHASE_CHANGESTANCES;
	if (ambush == 'adversaries') g.combat.advsurprise = true;

	// enter that phase and report what it looks like
	let r = slashcommands.localizedString(locale, 'combat-started', [g.combat.title + (g.combat.title == '' ? '' : ' ')]) + '\n' + initiative.enterCombatPhase(g, locale, channelID, phase, false) + combatHeading(g, locale);
	filehandling.saveGameData(gameData);
	if (encounter) r += reports.generateReport(locale, g, 'adversaries', false);
	return r + '\n' + showCombatPhaseOrAll(g, phase, locale, false, true);
}

// build the top line of a combat display
function combatHeading(g, locale) {
	let r = '__**';
	if (g.combat.title != '') r += g.combat.title;
	else r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'combat-combat',[]));
	r += '**__ (' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'round',[])) + ' **' + String(g.combat.round) + '**)';
	if (g.combatMods.length != 0) r += ' ' + showCombatMods(g, locale, false);
	r += '\n';
	return r;
}

function showCombatPhaseOrAll(g, phase, locale, brief, alwaysShow) {
	if (gameHasConfigFlag(g, 'unphased-initiative') && phase != initiative.PHASE_OPENINGVOLLEY && phase != initiative.PHASE_ADVERSARY)
		return initiative.initiativeReport(locale, g, brief);
	return initiative.showCombatPhase(g, phase, locale, brief, alwaysShow)
}

// command for ending an initiative
function endCommand(userID, channelID, userName, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
	if (!g.combat.active) return slashcommands.localizedString(locale, 'error-nocombat', []);
	// parse options
	let confirmed = false, deleteencounter = false;
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'confirm') confirmed = data.options[i].value;
		if (data.options[i].name == 'deleteencounter') deleteencounter = data.options[i].value;
	}
	if (!confirmed) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'end'), null, data, 'end');

	let r = '';

	// delete the encounter if there is one and it was specified to do so
	if (deleteencounter && g.combat.encounter != '') {
		r = encounters.encounters_delete(g, g.combat.encounter);
		if (r != '') return slashcommands.localizedString(locale, r, [g.combat.encounter]);
		r += slashcommands.localizedString(locale, 'encounter-deleted', [g.combat.encounter]) + '\n';
		g.selectedEncounter = '';
		g.combat.encounter = '';
	}

	wipeTemporaryBonuses(g, true);
	g.songUsed = '';
	g.songBonus = 0;
	g.songCharacters = [];
	g.combat.active = false;
	g.combatMods = [];
	g.rallycurrent = 0;
	g.rallynext = 0;

	// clear all wounds that are set to 0
	for (let p in g.characters) {
		if (characterDataFetchHandleNulls(g, p, 'wounded', null) == 'TRUE' && characterDataFetchHandleNumbers(g, p, 'wounddaysleft', null) == 0) {
			clearWounds(g, p, channelID);
			r += characterDataFetchHandleNulls(g, p, 'name', null) + ' ' + slashcommands.localizedString(locale, 'token-woundhealed', []) + '\n';
		}
		if (hasTrait(g, p, 'virtue', 'defiance') && (characterDataFetchHandleNumbers(g, p, 'endurance', null) < characterDataFetchHandleNumbers(g, p, 'maxendurance', null))) {
			characterDataUpdate(g, p, 'endurance', null, '+', Math.max(characterDataFetchHandleNumbers(g, p, 'sheet', 'heart'), characterDataFetchHandleNumbers(g, p, 'sheet', 'wisdom')), null, 'maxendurance', channelID);
			r += showStackedStatusLine(g, p, 'endurance', 'name', locale, false) + '\n';
		}
	}
	combat_log(g, slashcommands.localizedString(locale, 'combat-ended', [g.combat.title + (g.combat.title == '' ? '' : ' '), g.combat.round]));
	filehandling.saveGameData(gameData);
	return r + slashcommands.localizedString(locale, 'combat-ended', [g.combat.title + (g.combat.title == '' ? '' : ' '), g.combat.round]);
}

// command for showing the current phase
function currentCommand(userID, channelID, userName, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!g.combat.active) return slashcommands.localizedString(locale, 'error-nocombat', []);
	return combatHeading(g, locale) + showCombatPhaseOrAll(g, g.combat.phase, locale, false, true);
	
}

// command for showing the next action and handling round ends
function nextCommand(userID, channelID, userName, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!g.combat.active) return slashcommands.localizedString(locale, 'error-nocombat', []);
	if (!isPlayerLoremasterOfGame(userID, g)) {
		let s = initiative.takeInitiativeAction(locale, g, currentCharacter(userID), 'all');
		if (s != '') return s;
		return slashcommands.localizedString(locale, 'initiative-advanced', []);
	}

	// if a stance phase and there are pending actors, and no confirm, exit with error message showing who hasn't acted
	let confirmed = false;
	if (data.options && data.options[0] && data.options[0].value) confirmed = true;
	if (g.combat.phase != initiative.PHASE_CHANGESTANCES && g.combat.phase != initiative.PHASE_ADVERSARY && g.combat.actors.length != 0 && confirmed != true) {
		let names = [];
		g.combat.actors.forEach(actor => {
			if (g.characters[actor] && !g.characters[actor].knockback) names.push(characterDataFetchHandleNulls(g, actor, 'name', null));
		});
		if (names.length != 0) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'next'), slashcommands.localizedString(locale, 'combat-confirmnext', [names.join(', ')]), data, 'next');
			//return slashcommands.localizedString(locale, 'combat-confirmnext', [names.join(', ')]);
	}
	// same thing for adversary phase
	if (g.combat.phase == initiative.PHASE_ADVERSARY && g.combat.encounter && confirmed != true) {
		let names = [];
		for (adv in g.encounters[g.combat.encounter].adversaries) {
			if (!encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[adv]) && !g.encounters[g.combat.encounter].adversaries[adv].knockback && g.encounters[g.combat.encounter].adversaries[adv].actionsThisRound < initiative.adversaryCombatActions(g, adv)) {
				names.push(adv);
			}
		}
		if (names.length != 0) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'next'), slashcommands.localizedString(locale, 'combat-confirmnext', [names.join(', ')]), data, 'next');
			// return slashcommands.localizedString(locale, 'combat-confirmnext', [names.join(', ')]);
	}

	let phase = initiative.nextCombatPhase(g);
	let r = initiative.enterCombatPhase(g, locale, channelID, phase, false) + combatHeading(g, locale);
	if (g.combat.phase == initiative.PHASE_CHANGESTANCES && gameHasConfigFlag(g, 'remind-engagement') && g.combat.round == 1)
		r += ':warning: ' + slashcommands.localizedString(locale, 'initiative-remindengagement', []) + '\n';
	combat_log(g, combatHeading(g, locale) + stancePhaseToName(phase));
	filehandling.saveGameData(gameData);
	return r + showCombatPhaseOrAll(g, g.combat.phase, locale, false, true);
}

// spend command used for tengwar (also diverts to spending hope et al.)
function spendCommand(userID, channelID, userName, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID), value = null;
	// parse options
	let choice = null;
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'choice') choice = data.options[i].value;
		if (data.options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[i].value);
		}
	}
	if (!charID) return slashcommands.localizedString(locale, 'error-nocharacter', []);

	// if blank, shows tengwar and a list of options for how it can be spent
	if (choice == null || g.characters[charID].tengwar == 0) {
		let r = emoji.headingValueAndTokens('Tengwar', g.characters[charID].tengwar, 'tengwar', false);
		if (g.characters[charID].tengwar != 0) {
			r += '\n';
			['h','f','p','s','u'].forEach(i => {
				r += ' :small_blue_diamond: ' +  slashcommands.localizedString(locale, 'spend-' + i, []) + '\n';
			});
		}
		return r;
	}

	return spendOneTengwar(g, charID, choice, locale);
}

function getWeaponData(g, charID, weapon) {
	if (weapon == 'stone' && hasTrait(g, charID, 'virtue', 'sure at the mark')) {
		wData = [characterDataFetch(g, charID, 'brawling',null), 1, emoji.GANDALF, 12, 1, 'Brawling', '', 0, '1H', 'FALSE', 'TRUE', '', '', '', '', '', '', ''];
	} else if (weapon == 'stone') {
		wData = [characterDataFetch(g, charID, 'brawling',null), 1, 99, 0, 1, 'Brawling', '', 0, '1H', 'FALSE', 'TRUE', '', '', '', '', '', '', ''];
	} else if (weapon == 'unarmed' || weapon == 'brawling') {
		wData = [characterDataFetch(g, charID, 'brawling',null), 1, 99, 0, 1, 'Brawling', '', 0, '1H', 'TRUE', 'FALSE', '', '', '', '', '', '', ''];
		weapon = 'unarmed';
	} else {
		wData = characterDataFetch(g, charID, 'weapons', weapon);
		if (wData == null) return [null, null];
		weapon = wData[18];
	}
	return [weapon, wData];
}

// calculate how much heavy damage a player attack does
function spendCalculateHeavy(g, charID) {
	let delta = 0;
	delta += characterDataFetchHandleNumbers(g, charID, 'strength',null);
	if (g.characters[charID].handedness == 2) delta++;
	if (hasTrait(g, charID, 'virtue', 'dour-handed')) delta++;
	if (hasTrait(g, charID, 'virtue', 'great strength')) delta += 2;
	return delta;
}

// calculate how much fend bonus a player attack does
function spendCalculateFend(g, charID) {
	let weapon, wData;
	[weapon, wData] = getWeaponData(g, charID, g.characters[charID].curWeapon);
	if (wData == null) return null;
	if (stringutil.lowerCase(wData[5]) == 'axes' || stringutil.lowerCase(wData[5]) == 'brawling') return 1;
	if (stringutil.lowerCase(wData[5]) == 'swords') return 2;
	if (stringutil.lowerCase(wData[5]) == 'spears') return 3;
	return null;
}

// calculate how much pierce improvement a player attack does
function spendCalculatePierce(g, charID) {
	let weapon, wData, delta = 0;
	[weapon, wData] = getWeaponData(g, charID, g.characters[charID].curWeapon);
	if (wData == null) return null;
	if (stringutil.lowerCase(wData[5]) == 'swords') delta += 1;
	else if (stringutil.lowerCase(wData[5]) == 'bows') delta += 2;
	else if (stringutil.lowerCase(wData[5]) == 'spears') delta += 3;
	else if (stringutil.lowerCase(wData[18]).includes('dagger')) delta += 1;
	else return null;
	if (hasTrait(g, charID, 'virtue', 'dour-handed')) delta++;
	return delta;
}

// actually does the spend -- so it can be called by either spendCommand or an emoji or button click
function spendOneTengwar(g, charID, choice, locale) {
	let weapon, wData, charName = characterDataFetchHandleNulls(g, charID, 'name','');
	[weapon, wData] = getWeaponData(g, charID, g.characters[charID].curWeapon);
	if (wData == null) return '';
	// 'skill', 'dmg', 'edge', 'injury', 'equipped', 'proficiency', 'notes', 'enc', 'handedness', 'melee', 'ranged'

	let delta = 0;
	let r = '', loremasterOutcome = '';
	if (g.characters[charID].tengwar <= 0) {
		// error message that you're out of tengwar
		return charName + ': ' + slashcommands.localizedString(locale, 'spend-notengwar', [slashcommands.localizedOption(locale, 'spend', 'choice', choice)]);
	}
	
	if (choice == 'heavy') {
		// heavy: increase damage; equal to Strength, +1 if it's a 2H weapon (including bows)
		delta = spendCalculateHeavy(g, charID);
		// heavy: update and display damageDone
		if (g.characters[charID].damageDone == undefined) g.characters[charID].damageDone = 0;
		r += slashcommands.localizedString(locale, 'spend-heavy', [g.characters[charID].damageDone, delta, String(g.characters[charID].damageDone + delta)]);
		g.characters[charID].damageDone += delta;
		if (g.characters[charID].protectionTN && g.characters[charID].protectionTN.adversary) {
			let [lmAddition, rAddition] = damageAdversary(g, g.characters[charID].protectionTN.adversary, delta, locale, charID, characterDataFetchHandleNulls(g, charID, 'name',''), wData, wData[9], 1);
			loremasterOutcome += lmAddition;
			r += (g.characters[charID].protectionTN.adversary ? ' (**`' + g.characters[charID].protectionTN.adversary + '`**)' : '') + '\n' + rAddition;

		}
		r += '\n';
	}
	
	if (choice == 'fend') {
		// fend: added parry equal to 1 with Axe or Brawling, 2 with Sword, 3 with Spear, else disallowed
		delta = spendCalculateFend(g, charID);
		if (delta == null) {
			console.log('TRACE: spenddisallowed reported for ' + arg + ', weapon ' + g.characters[charID].curWeapon + ' proficiency ' + wData[5]);
			notifyHostUser('TRACE: spenddisallowed reported for ' + arg + ', weapon ' + g.characters[charID].curWeapon + ' proficiency ' + wData[5],false);
			return slashcommands.localizedString(locale, 'spend-fail', [slashcommands.localizedOption(locale, 'spend', 'choice', choice), g.characters[charID].curWeapon, wData[5]]);
		}
		// fend: update and display fendParry
		if (g.characters[charID].fendParry == undefined) g.characters[charID].fendParry = 0;
		r += slashcommands.localizedString(locale, 'spend-fend', [g.characters[charID].fendParry, delta, String(g.characters[charID].fendParry + delta)]) + '\n';
		g.characters[charID].fendParry += delta;
	}
	
	if (choice == 'pierce') {
		if (g.characters[charID].protectionTN == null) return slashcommands.localizedString(locale, 'spend-pierce-cannot', [prevRoll]);
		// pierce: increase feat roll +1 Sword, +2 Bow, +3 Spear, else disallowed
		if (g.characters[charID].protectionTN.attackFeatRoll <= 0) { // verify we're not adding to an eye roll, or a favoured by the Grey Wizard 11
			let prevRoll = emoji.customEmoji(emoji.EYE, false, false, false);
			if (g.characters[charID].protectionTN.attackFeatRoll == -99) prevRoll = emoji.customEmoji(11, false, false, false);
			return slashcommands.localizedString(locale, 'spend-pierce-cannot', [prevRoll]);
		}
		delta = spendCalculatePierce(g, charID);
		if (delta == null) return slashcommands.localizedString(locale, 'spend-fail', [slashcommands.localizedOption(locale, 'spend', 'choice', choice), g.characters[charID].curWeapon, wData[5]]);

		// pierce: update and display attackFeatRoll; if now made edge, report protectionTN
		// figure out the new feat roll (stopping at Gandalf)
		if (g.characters[charID].protectionTN.attackFeatRoll == undefined) g.characters[charID].protectionTN.attackFeatRoll = 0;
		let newFeat = g.characters[charID].protectionTN.attackFeatRoll + delta;
		if (newFeat > 10) newFeat = 10;
		// add a message saying the addition: change this to show actual dice faces
		r += slashcommands.localizedString(locale, 'spend-pierce', [
			emoji.customEmoji(g.characters[charID].protectionTN.attackFeatRoll, false, false, false), 
			delta, 
			emoji.customEmoji(newFeat, false, false, false)
			]);
		g.characters[charID].protectionTN.attackFeatRoll = newFeat;
		// if this now makes edge, add something saying that, showing the edge and the protectionTN
		if ((!isNaN(g.characters[charID].protectionTN.edge) && newFeat >= Number(g.characters[charID].protectionTN.edge)) || (stringutil.lowerCase(g.characters[charID].protectionTN.edge) == 'g' && newFeat >= 12)) {
			r += slashcommands.localizedString(locale, 'spend-pierce-edge', [String(g.characters[charID].protectionTN.edge), String(g.characters[charID].protectionTN.injury)]);
			if (g.characters[charID].protectionTN && g.characters[charID].protectionTN.adversary) {
				// save the protection roll for the target
				g.encounters[g.combat.encounter].adversaries[g.characters[charID].protectionTN.adversary].protectionTests.push({'injury': g.characters[charID].protectionTN.injury, 'responsible': charID, 'feat': g.characters[charID].protectionTN.feat, 'bonus': 0, 'modifier': 0, 'condition': null, 'poison': 0, 'hideoustoughnessroll': 0});
				filehandling.saveGameData(gameData);
				loremasterOutcome += g.characters[charID].protectionTN.adversary + ': ' + characterDataFetchHandleNulls(g, charID, 'name','') + ' ' + slashcommands.localizedString(locale, 'attack-madeedge', [g.characters[charID].protectionTN.injury]) + '\n';
				showLMprotTestPanel(g, g.loremaster, locale);
			}
		} else {
			r += slashcommands.localizedString(locale, 'spend-pierce-fail', [String(g.characters[charID].protectionTN.edge)]);
		}
		if (g.characters[charID].protectionTN.adversary) r += ' (**`' + g.characters[charID].protectionTN.adversary + '`**)\n';
	}
	
	if (choice == 'shield') {
		// shield: if Strength > attribute, foe loses 1d for the round; must be 1h and have a shield
		if (g.characters[charID].handedness != 1 || characterDataFetchHandleNumbers(g, charID, 'sheet','shield') == 0 || g.characters[charID].shieldsmashed) {
			// verify 1h and with shield
			return slashcommands.localizedString(locale, 'spend-shield-fail', []);
		} else delta++;
		// shield: if Strength >= attribute, foe loses 1d for the round
		if (g.characters[charID].protectionTN && g.characters[charID].protectionTN.adversary) {
			if (characterDataFetchHandleNumbers(g, charID, 'sheet','strength') <= g.encounters[g.combat.encounter].adversaries[g.characters[charID].protectionTN.adversary].attribute || g.encounters[g.combat.encounter].adversaries[g.characters[charID].protectionTN.adversary].pushback) {
			return slashcommands.localizedString(locale, 'spend-shield-fail', []);
			}
			g.encounters[g.combat.encounter].adversaries[g.characters[charID].protectionTN.adversary].pushback = true;
		}
		r += slashcommands.localizedString(locale, 'spend-shield', [delta, String(characterDataFetchHandleNumbers(g, charID, 'strength',null))]) + (g.characters[charID].protectionTN.adversary ? ' (**`' + g.characters[charID].protectionTN.adversary + '`**)' : '') + '\n';
	}

	if (choice == 'escape') {
		// escape: no longer seized
		g.characters[charID].seized = false;
		g.characters[charID].bitten = false;
		g.characters[charID].drained = false;
		g.characters[charID].bleeding = 0;
		r += slashcommands.localizedString(locale, 'condition-freed', [characterDataFetchHandleNulls(g, charID, 'name','')]) + '\n';
	}

	if (loremasterOutcome != '') botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, charName + ': ' + loremasterOutcome);

	g.characters[charID].tengwar--;
	if (g.combat.active) combat_log(g, r);
	filehandling.saveGameData(gameData);
	r += emoji.headingValueAndTokens('Tengwar', g.characters[charID].tengwar, 'tengwar', false);
	return '**' + charName + '**: ' + r;
}

// adversary button press
function adversaryButtonClick(g, advName, choice, locale, channelID) {
	// find the adversary
	let adversary = advName.split('(')[1].slice(0,-1);
	if (!g || !g.combat.active || g.combat.encounter == '' || !g.encounters[g.combat.encounter] || !g.encounters[g.combat.encounter].adversaries[adversary]) return slashcommands.localizedString(locale, 'adversary-notexists', [adversary]);
	let adv = g.encounters[g.combat.encounter].adversaries[adversary], r = '';

	// parse the choice and the charID
	let charID = Number(choice.split('_')[1]);
	choice = choice.split('_')[0];
	let charName = characterDataFetchHandleNulls(g, charID, 'name','');

	// verify number of tengwar -- and this isn't being saved anywhere!!
	if (adv.tengwar[charID] == undefined || adv.tengwar[charID].current <= 0) return slashcommands.localizedString(locale, 'spend-notengwar', [choice]);

	if (choice == 'heavy') {
		r += slashcommands.localizedString(locale, 'spend-adv-heavyblow', [adversary, charName, String(adv.attribute)]) + '\n';
		r += tokenCommandExecute('damage', channelID, locale, g, charID, 'endurance', '-', adv.attribute, false) + '\n';
	}
	
	if (choice == 'pierce') {
		let newFeat = adv.tengwar[charID].roll + 2;
		if (newFeat > 12) newFeat = 12;
		r += slashcommands.localizedString(locale, 'spend-pierce', [
			emoji.customEmoji(adv.tengwar[charID].roll, false, false, true), 
			2, 
			emoji.customEmoji(newFeat, false, false, true)
			]);
		adv.tengwar[charID].roll += 2;
		if (adv.tengwar[charID].roll >= 10) {
			r += slashcommands.localizedString(locale, 'spend-pierce-edge', ['10', String(adv.tengwar[charID].injury)]);
			r += '\n' + adversaryPiercingBlow(g, adversary, charID, adv.tengwar[charID].injury, adv.tengwar[charID].total, locale, true);
		} else r += '.\n';
	}
	
	if (choice == 'breakshield') {
		g.characters[charID].shieldsmashed = true;
		r += slashcommands.localizedString(locale, 'spend-adv-breakshield', [adversary, charName]) + '\n';
	}
	
	if (choice.startsWith('fieryblow')) { // severity is the last character of the choice
		let severity = Number(choice.slice(-1));
		r += slashcommands.localizedString(locale, 'condition-poison-roll', [characterDataFetchHandleNulls(g, charID, 'name',null), slashcommands.localizedString(locale, 'condition-severity-' + String(severity), []) + ' ' + slashcommands.localizedString(locale, 'condition-fiery', [])])
		//let [dmg, r2] = enduranceLossLevel(severity);
		let [dmg, r2] = enduranceLossDamage(severity, 'fire', g, charID, locale, channelID);

		r += r2;
		if (dmg == emoji.GANDALF) {
			r += slashcommands.localizedString(locale, 'spend-adv-fieryblow', [adversary, '0']) + '\n';
		} else if (dmg == emoji.EYE) {
			r += charName + ' ' + slashcommands.localizedString(locale, 'condition-poison-eye', []);
			//r += '\n' + tokenCommandExecute('damage', channelID, locale, g, charID, 'endurance', '=', 0, false);
		} else {
			r += slashcommands.localizedString(locale, 'spend-adv-fieryblow', [adversary, String(dmg)]) + '\n';
			//r += tokenCommandExecute('damage', channelID, locale, g, charID, 'endurance', '-', dmg, false) + '\n';
		}
	}
	
	if (choice == 'seize') {
		g.characters[charID].seized = true;
		r += slashcommands.localizedString(locale, 'spend-adv-seize', [adversary, charName]) + '\n';
	}

	if (choice == 'bitten') {
		g.characters[charID].bitten = true;
		r += slashcommands.localizedString(locale, 'spend-adv-bitten', [adversary, charName]) + '\n';
	}

	if (choice == 'drain') {
		g.characters[charID].drained = true;
		let tn = 14;
		g.encounters[g.combat.encounter].adversaries[adversary].attacks.forEach(attack => {
			if (attack.special == 'drain') tn = attack.injury;
		});
		g.characters[charID].drainedTN = tn;
		g.characters[charID].drainedAdv = adversary;
		r += slashcommands.localizedString(locale, 'spend-adv-drain', [adversary, charName]) + '\n';
	}

	adv.tengwar[charID].current--;
	combat_log(g, r);
	r += emoji.headingValueAndTokens('Tengwar', adv.tengwar[charID].current, 'tengwar', false);
	filehandling.saveGameData(gameData);

	return r;
}

// implements an adversary possibly getting a piercing blow
function adversaryPiercingBlow(g, adversary, charID, injury, tengwar, locale, fromButton) {
	let r = '';
	if (injury) {
		if (!fromButton) r += '; ' + slashcommands.localizedString(locale, 'attack-madeedge', [injury]);
		// save protection tests for heroes
		let deadly = false;
		if (adversary && encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'deadly wound')) deadly = true;
		let poison = 0;
		if (g && g.encounters[g.combat.encounter] && g.encounters[g.combat.encounter].adversaries[adversary] && g.encounters[g.combat.encounter].adversaries[adversary].abilities) {
			g.encounters[g.combat.encounter].adversaries[adversary].abilities.forEach(a => {
				if (stringutil.lowerCase(a.name).startsWith('poison') || stringutil.lowerCase(a.name).startsWith('orc-poison')) {
					poison = a.amount;
					if (isNaN(poison) || poison < 1 || poison > 3) poison = 1;
				}
			});
		}
		let theTest = {'injury': injury, 'responsible': adversary, 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': null, 'poison': poison, 'deadly': deadly};
		pushTest(g, charID, 'protection', theTest);
	}
	else r += '; ' + slashcommands.localizedString(locale, 'attack-piercingblow', []);
	if (adversary && encounters.encounters_adversary_has_ability(g, g.combat.encounter, adversary, 'dreadful wrath')) {
		// Dreadful Wrath also queues a shadow test, 1+tengwar, daunted
		theTest = {'type': 'dread', 'tn': null, 'responsible': adversary, 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': 'daunted', 'points': (tengwar+1)};
		pushTest(g, charID, 'shadow', theTest);
		r += '\n' + slashcommands.localizedString(locale, 'attack-dreadfulwrath', [String(tengwar+1)]);
	}
	return r;
}

// COUNCIL ----------------------------------------------------------------------
// main verb for setting up, showing, modifying, and ending councils
function councilCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let cmd = data.options[0].name;

	if (cmd == 'show') {
		if (!councilActive(g)) return slashcommands.localizedString(locale, 'council-inactive', []);
		return councilShow(g, locale, false) + '\n' + councilCompletedOutcome(g, locale);
	}

	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	if (cmd == 'undo') {
		if (Object.keys(g.council.undo).length == 0) return slashcommands.localizedString(locale, 'council-noundo', []);
		g.council.request = g.council.undo.request;
		g.council.introduction = g.council.undo.introduction;
		g.council.time = g.council.undo.time;
		g.council.timelimit = g.council.undo.timelimit;
		g.council.score = g.council.undo.score;
		g.council.undo = {};
		filehandling.saveGameData(gameData);
		return councilShow(g, locale, false) + '\n' + councilCompletedOutcome(g, locale);
	}

	// parse options
	let request = null, attitude = null, rules = 'core', amount = null, successes = 1, charID = null;
	if (gameHasConfigFlag(g, 'council-original')) rules = 'original';
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'request') request = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'attitude') attitude = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'rules') rules = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'amount') amount = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'successes') successes = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[0].options[i].value);
		}
	}

	if (cmd == 'start') {
		if (councilActive(g)) return slashcommands.localizedString(locale, 'council-active', []);
		let resistance = 6;
		if (request) resistance = ['reasonable','bold','outrageous'].indexOf(request) * 3 + 3;

		g.council.resistance = resistance;
		if (resistance == 3) g.council.request = 'reasonable';
		else if (resistance == 6) g.council.request = 'bold';
		else if (resistance == 9) g.council.request = 'outrageous';
		else g.council.request = 'custom';

		// set attitude
		g.council.attitude = attitude;
		if (attitude == 'reluctant') g.council.attitudemod = -1;
		else if (attitude == 'friendly') g.council.attitudemod = 1;
		else g.council.attitudemod = 0;

		// calculate time limit
		let timelimit = 3;
		if (rules == 'original') timelimit = resistance;
		for (let charID in g.characters) {
			if (hasTrait(g, charID, 'virtue', 'friendly and familiar')) timelimit++;
		};
		g.council.timelimit = timelimit;
		g.council.rules = rules;

		// initialize everything else
		g.council.score = 0;
		g.council.time = 0;
		g.council.introduction = 0;
		g.council.undo = {};

		// save game
		filehandling.saveGameData(gameData);

		// return councilShow plus some instructions
		return councilShow(g, locale, false) + '\n\n' + slashcommands.localizedString(locale, 'council-instructions', []);
	}

	if (!councilActive(g)) return slashcommands.localizedString(locale, 'council-inactive', []);

	if (cmd == 'pass' || cmd == 'fail') { // Record a passing or failing test in the current council
		// identify number of successes
		if (cmd == 'fail') successes = 0;
		r = councilApplyRoll(g, locale, successes);
		filehandling.saveGameData(gameData);
		return r;
	}

	// council end
	if (cmd == 'end') {
		let r = councilShow(g, locale, false) + '\n' + councilCompletedOutcome(g, locale);
		g.council.request = '';
		g.songUsed = '';
		g.songBonus = 0;
		g.songCharacters = [];
		clearUnweary(g);
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'council-ending', []) + '\n' + r;
	}

	if (cmd == 'attitude') {
		// set attitude and attitudemod
		g.council.attitude = attitude;
		if (attitude == 'reluctant') g.council.attitudemod = -1;
		else if (attitude == 'friendly') g.council.attitudemod = 1;
		else g.council.attitudemod = 0;
		filehandling.saveGameData(gameData);
		return councilShow(g, locale, false);
	}

	// commands to directly modify the council
	if (cmd == 'time' || cmd == 'timelimit' || cmd == 'score' || cmd == 'resistance') {

		g.council[cmd] = amount;
		filehandling.saveGameData(gameData);

		let r = councilCompletedOutcome(g, locale);
		if (r == '') r = councilShow(g, locale, false);
		return r;
	}
}

// evaluate if an active council is in play
function councilActive(g) {
	if (!g || !g.council || g.council.request == '') return false;
	return true;
}

// display the status of a council
function councilShow(g, locale, brief) {
	if (!councilActive(g)) return '';
	let r = '';
	if (!brief) r += '**' + stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'council')) + '**: ';
	 r += slashcommands.localizedString(locale, 'council-show1',[g.council.request, g.council.attitude, (g.council.attitudemod ? ' (' + (g.council.attitudemod > 0 ? '+' : '') + String(g.council.attitudemod) + 'd)' : '')]);
	if (g.council.introduction == 0) r += slashcommands.localizedString(locale, 'council-intropending',[]);
	if (g.council.introduction == -1) r += slashcommands.localizedString(locale, 'council-introfailed',[]);
	if (g.council.introduction == 1) r += slashcommands.localizedString(locale, 'council-introsucceeded',[]);

	if (brief) r += '; '; else r += '\n';

	if (!brief) r += '**' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'council-timelimit',[])) + '**: ';
	r += slashcommands.localizedString(locale, 'council-show3',[g.council.time, g.council.timelimit]);
	if (brief) r += '; '; else r += ' :large_blue_diamond: **' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'council-score',[])) + '**: ';
	r += slashcommands.localizedString(locale, 'council-show4',[g.council.score, g.council.resistance]);

	return r;
}

// description of a council for display purposes
function councilAttitudeDesc(mod, locale) {
	if (mod < 0) return slashcommands.localizedString(locale, 'council-reluctant', []);
	if (mod > 0) return slashcommands.localizedString(locale, 'council-friendly', []);
	return slashcommands.localizedString(locale, 'council-open', []);
}

// apply a roll's results to a council, and return the outcome as a pose
function councilApplyRoll(g, locale, successes) {

	g.council.undo = {
		'request': g.council.request,
		'introduction': g.council.introduction,
		'time': g.council.time,
		'timelimit': g.council.timelimit,
		'score': g.council.score
	};

	if (g.council.introduction == 0) {
		// an introduction is pending so this determines it
		// increase time limit by number of tengwars
		g.council.timelimit += successes - ((g.council.rules == 'original' && successes > 0) ? 1 : 0);
		// record introduction result
		g.council.introduction = (successes > 0 ? 1 : -1);
	} else {
		// this is an interaction
		// time passes towards time limit
		g.council.time++;
		// add successes to score
		g.council.score += successes;
	}
	filehandling.saveGameData(gameData);

	// call the function to determine if the council is over
	let r = councilCompletedOutcome(g, locale);
	// if it's not done, return a show
	if (r == '') r = councilShow(g, locale, false);
	return r;
}

// determine whether a council is over, and if so, the result
function councilCompletedOutcome(g, locale) {
	if (g.council.score >= g.council.resistance) {
		g.council.request = '';
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'council-outcomesuccess',[]);
	} else if (g.council.time >= g.council.timelimit) {
		g.council.request = '';
		g.songUsed = '';
		g.songBonus = 0;
		g.songCharacters = [];
		clearUnweary(g);
		filehandling.saveGameData(gameData);
		if (g.council.score == 0 || g.council.introduction == -1) return slashcommands.localizedString(locale, 'council-outcomedisaster',[]);
		else return slashcommands.localizedString(locale, 'council-outcomefailure',[]);
	}
	return '';
}

// ENDEAVOUR ------------------------------------------------------------------
// main verb for setting up, showing, modifying, and ending skill endeavours
function endeavourCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let cmd = data.options[0].name;

	if (cmd == 'show') {
		if (!endeavourActive(g)) return slashcommands.localizedString(locale, 'endeavour-inactive', []);
		return endeavourShow(g, locale, false) + '\n' + endeavourCompletedOutcome(g, locale);
	}

	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	if (cmd == 'undo') {
		if (Object.keys(g.endeavour.undo).length == 0) return slashcommands.localizedString(locale, 'endeavour-noundo', []);
		g.endeavour.request = g.endeavour.undo.request;
		g.endeavour.time = g.endeavour.undo.time;
		g.endeavour.timelimit = g.endeavour.undo.timelimit;
		g.endeavour.score = g.endeavour.undo.score;
		g.endeavour.undo = {};
		filehandling.saveGameData(gameData);
		return endeavourShow(g, locale, false) + '\n' + endeavourCompletedOutcome(g, locale);
	}

	// parse options
	let task = null, amount = null, timelimit = null, successes = 1, charID = null;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'task') task = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'timelimit') timelimit = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'amount') amount = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'successes') successes = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			charID = findCharacterInGame(g, data.options[0].options[i].value);
		}
	}

	if (cmd == 'start') {
		if (endeavourActive(g)) return slashcommands.localizedString(locale, 'endeavour-active', []);
		let resistance = 6;
		if (task) resistance = ['simple','laborious','daunting'].indexOf(task) * 3 + 3;

		g.endeavour.resistance = resistance;
		if (resistance == 3) g.endeavour.request = 'simple';
		else if (resistance == 6) g.endeavour.request = 'laborious';
		else if (resistance == 9) g.endeavour.request = 'daunting';
		else g.endeavour.request = 'custom';

		// calculate time limit
		if (timelimit && timelimit.startsWith('+')) timelimit = Number(timelimit) + resistance;
		else timelimit = Number(timelimit);
		if (timelimit < 1) return slashcommands.localizedString(locale, 'error-numeric', [timelimit]);
		g.endeavour.timelimit = timelimit;

		// initialize everything else
		g.endeavour.score = 0;
		g.endeavour.time = 0;
		g.endeavour.undo = {};

		// save game
		filehandling.saveGameData(gameData);

		// return endeavourShow plus some instructions
		return endeavourShow(g, locale, false) + '\n\n' + slashcommands.localizedString(locale, 'endeavour-instructions', []);
	}

	if (!endeavourActive(g)) return slashcommands.localizedString(locale, 'endeavour-inactive', []);

	if (cmd == 'pass' || cmd == 'fail') { // Record a passing or failing test in the current endeavour		
		// identify number of successes
		if (cmd == 'fail') successes = 0;

		r = endeavourApplyRoll(g, locale, successes);
		filehandling.saveGameData(gameData);
		return r;
	}

	// endeavour end
	if (cmd == 'end') {
		let r = endeavourShow(g, locale, false) + '\n' + endeavourCompletedOutcome(g, locale);
		g.endeavour.request = '';
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'endeavour-ending', []) + '\n' + r;
	}

	// commands to directly modify the endeavour
	if (cmd == 'time' || cmd == 'timelimit' || cmd == 'score' || cmd == 'resistance') {

		g.endeavour[cmd] = amount;
		filehandling.saveGameData(gameData);

		let r = endeavourCompletedOutcome(g, locale);
		if (r == '') r = endeavourShow(g, locale, false);
		return r;
	}
}

// evaluate if an active endeavour is in play
function endeavourActive(g) {
	if (!g || !g.endeavour || g.endeavour.request == '') return false;
	return true;
}

// display the status of a endeavour
function endeavourShow(g, locale, brief) {
	if (!endeavourActive(g)) return '';
	let r = '';
	if (!brief) r += '**' + stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'endeavour')) + '**: ';
	 r += slashcommands.localizedString(locale, 'endeavour-show',[g.endeavour.request, g.endeavour.time, g.endeavour.timelimit, g.endeavour.score, g.endeavour.resistance]);

	return r;
}

// apply a roll's results to a endeavour, and return the outcome as a pose
function endeavourApplyRoll(g, locale, successes) {

	g.endeavour.undo = {
		'request': g.endeavour.request,
		'time': g.endeavour.time,
		'timelimit': g.endeavour.timelimit,
		'score': g.endeavour.score
	};

	// time passes towards time limit
	g.endeavour.time++;
	// add successes to score
	g.endeavour.score += successes;
	filehandling.saveGameData(gameData);

	// call the function to determine if the endeavour is over
	let r = endeavourCompletedOutcome(g, locale);
	// if it's not done, return a show
	if (r == '') r = endeavourShow(g, locale, false);
	return r;
}

// determine whether a endeavour is over, and if so, the result
function endeavourCompletedOutcome(g, locale) {
	if (g.endeavour.score >= g.endeavour.resistance) {
		g.endeavour.request = '';
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'endeavour-outcomesuccess',[]);
	} else if (g.endeavour.time >= g.endeavour.timelimit) {
		g.endeavour.request = '';
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'endeavour-outcomefailure',[]);
	}
	return '';
}

// WEATHER ----------------------------------------------------------------------
// hook to the hexflower weather system
function weatherCommand(userID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	let cmd = data.options[0].name;

	// advance the weather only if the LM asks for it without show
	if (isPlayerLoremasterOfGame(userID, g) && cmd != 'show') {
		let rollLine = '';
		if (cmd == 'set' && Number(data.options[0].options[0].value) >= 0 && Number(data.options[0].options[0].value) <= 18) {
			g.weather = Number(data.options[0].options[0].value);
		} else {
			let roll, r, x;
			[roll, rollLine, r, x] = regdice.polyhedralDieRoll(['2d6'], '', '', locale);
			g.weather = hexflower.nextHex(g.weather, roll);
		}
		return [rollLine, hexflower.showHex('weather',g.weather,stringutil.titleCase(getUserConfig(userID,'selectedGame')))];
	}

	// show the weather
	return hexflower.showHex('weather',g.weather,stringutil.titleCase(getUserConfig(userID,'selectedGame')));
}

// SONGBOOK --------------------------------------------------------------------
// determine how many uses a song gets by its type
function songUsesByType(songtype) {
	//if (songtype == 'Elvish') return 2;
	return 1;
}

// determine how many uses a particular song gets
function songUses(song) {
	return songUsesByType(song.type);
}

// find a song with substring matching
function songMatch(songbook, title, exact) {
	for (let song in songbook) {
		if (stringutil.multiwordMatch(song, title)) return song;
	}
	if (!exact) {
		for (let song in songbook) {
			if (stringutil.multiwordMatchSubstring(song, title)) return song;
		}
	}
	return '';
}

// record using a song
function songUse(songbook, title, g) {
	if (songbook[title].usesleft <= 0) return false;
	songbook[title].usesleft--;
	g.songUsed = songbook[title].type;
	g.songBonus = songbook[title].bonus;
	g.songCharacters = [];
	for (let p in g.characters) g.songCharacters.push(String(p));
	filehandling.saveGameData(gameData);
	return true;
}

// reset all the uses of all songs in the songbook
function resetSongUses(g) {
	for (let song in g.songbook) {
		g.songbook[song].usesleft = songUses(g.songbook[song]);
	}
}

// a command for recording songs in a company songbook
function songbookCommand(userID, channelID, guildID, locale, data) {
	let cmd = data.options[0].name;
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	let songbook = g.songbook;
	let r = '';

	if (cmd == 'show') { // show
		if (isEmpty(songbook)) return slashcommands.localizedString(locale, 'songbook-empty', []);

		let reportHeadings = [
			{title:stringutil.titleCase(slashcommands.localizedString(locale, 'title', [])), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedString(locale, 'author',[])), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedString(locale, 'type',[])), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedString(locale, 'bonus',[])), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedString(locale, 'uses',[])), emoji:false, minWidth:0, align:'right'},
			{title:stringutil.titleCase(slashcommands.localizedString(locale, 'left',[])), emoji:false, minWidth:0, align:'right'}
			];
		let reportData = [];
		for (let song in songbook) {
			if (songbook[song].bonus == null || songbook[song].bonus == undefined) songbook[song].bonus = 0;
			reportData.push([
				song,
				songbook[song].author,
				songbook[song].type,
				songbook[song].bonus,
				songUses(songbook[song]),
				songbook[song].usesleft
			]);
		}
		return '**' + stringutil.titleCase(slashcommands.localizedCommand(locale, 'songbook')) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);	
	}

	if (cmd == 'add') {
		let title, author, type, bonus = 0;
		if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
			if (data.options[0].options[i].name == 'songname') title = stringutil.sanitizeString(data.options[0].options[i].value);
			if (data.options[0].options[i].name == 'author') author = data.options[0].options[i].value;
			if (data.options[0].options[i].name == 'type') type = stringutil.titleCase(data.options[0].options[i].value);
			if (data.options[0].options[i].name == 'bonus') bonus = data.options[0].options[i].value;
		}
		let saveTitle = songMatch(songbook, title, true);
		if (saveTitle == '') saveTitle = title;
		g.songbook[saveTitle] = {
			'author': author,
			'type': type,
			'bonus': bonus,
			'usesleft': songUsesByType(type)
		};
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'songbook-created', [saveTitle]);
	}

	let title = stringutil.sanitizeString(data.options[0].options[0].value);
	title = songMatch(songbook, title, false);
	if (title == '') return slashcommands.localizedString(locale, 'songbook-missing',[stringutil.sanitizeString(title)]);
	
	if (cmd == 'remove') {
		// \songbook remove <title>
		delete g.songbook[title];
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'songbook-deleted', [title]);
	}

	if (cmd == 'use') {
		// \songbook use <title>
		if (songUse(songbook, title, g) == false) return slashcommands.localizedString(locale, 'songbook-usedup',[title]);
		return slashcommands.localizedString(locale, 'songbook-used', [title, songbook[title].usesleft]);
	}
}

// ADVERSARIES ----------------------------------------------------------------

// editing and creating adversaries
function aeditCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);

	let cmd = data.options[0].name;

	// parse options
	let shortname = null, from = null, property = null, value = null, name = null, dice = null, damage = null,
		injury = null, ranged = false, special = '', comment = '', type = '', amount = 0, confirm = null,
		tag = null, searchtext = null, mine = null, published = null,
		setup = {
			name: null, tags: null, features: null, attribute: null, hrtype: 'hate', hrscore: 1,
			might: 1, endurance: 12, parry: 0, armour: 1, size: 'Medium', banetype: null
		};
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'tag') tag = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'searchtext') searchtext = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'mine') mine = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'published') published = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'shortname') shortname = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'from') from = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'property') property = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'name') {
			name = data.options[0].options[i].value;
			setup.name = data.options[0].options[i].value;
		}
		if (data.options[0].options[i].name == 'value') value = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'dice') dice = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'damage') damage = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'injury') injury = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'ranged') ranged = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'type') type = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'special') special = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'amount') amount = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'comment') comment = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'confirm') confirm = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'tags') setup.tags = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'features') setup.features = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'hrtype') setup.hrtype = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'size') setup.size = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'banetype') setup.banetype = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'attribute') setup.attribute = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'hrscore') setup.hrscore = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'might') setup.might = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'endurance') setup.endurance = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'parry') setup.parry = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'armour') setup.armour = Number(data.options[0].options[i].value);
	}

	let adversaryName = getUserConfig(userID, 'aeditName');

	if (cmd == 'list') { // List available adversaries
		return adversaries.adversaries_list(userID, config.hostUser, locale, mine, published, searchtext, tag);
	}

	if (cmd == 'select') { // Select an existing adversary to edit
		adversaryName = stringutil.lowerCase(shortname);
		if (!adversaries.adversaries_accessible(adversaryName, userID, config.hostUser, 'read')) return slashcommands.localizedString(locale, 'adversary-notexists', [adversaryName]);
		setUserConfig(userID, 'aeditName', adversaryName);
		return slashcommands.localizedString(locale, 'adversary-selected', [adversaryName]);
	}

	if (cmd == 'create') { // Create an adversary
		adversaryName = stringutil.sanitizeString(shortname);
		if (!adversaryName.match(validName)) return ':warning: ' + slashcommands.localizedString(locale, 'error-alphanumeric', [adversaryName]);
		if (!adversaries.adversaries_create(adversaryName,userID)) return ':warning: ' + slashcommands.localizedString(locale, 'adversary-exists', [adversaryName]);
		setUserConfig(userID, 'aeditName', adversaryName);
		return slashcommands.localizedString(locale, 'adversary-created', [adversaryName]);
	}

	if (cmd == 'copy') { // Create an adversary by copying an existing one
		if (!adversaries.adversaries_accessible(from, userID, config.hostUser, 'read')) return slashcommands.localizedString(locale, 'adversary-notexists', [from]);
		let newAdversaryName = stringutil.mildSanitizeString(shortname);
		let r = adversaries.adversaries_copy(from, newAdversaryName, userID, locale, cmd == 'reverse');
		if (r != '') return slashcommands.localizedString(locale, r, [newAdversaryName]);
		setUserConfig(userID, 'aeditName', newAdversaryName);
		return slashcommands.localizedString(locale, 'adversary-copied', [newAdversaryName, from]);
	}

	if (cmd == 'show') { // Show the currently selected adversary
		if (shortname != null) adversaryName = shortname;
		return adversaries.adversaries_show(adversaryName, locale, userID, config.hostUser, userNameFromID);
	}

	if (adversaryName == '' || !adversaries.adversaries_accessible(adversaryName, userID, config.hostUser, 'read')) {
		return slashcommands.localizedString(locale, 'adversary-noneselected', []);
	}

	if (!adversaries.adversaries_accessible(adversaryName, userID, config.hostUser, 'write')) return slashcommands.localizedString(locale, 'adversary-notexists', [adversaryName]);

	if (cmd == 'set') { // Set some value for an adversary
		let r = adversaries.adversaries_edit(adversaryName, userID, config.hostUser, locale, property, value);
		if (r != '') return slashcommands.localizedString(locale, r, [property, value]) +
			((r == 'error-unknowntag') ? slashcommands.localizedString(locale, 'error-tagsknown', [adversaries.adversaries_tag_list().join(', ')]) : '');
		return slashcommands.localizedString(locale, 'adversary-changed', [adversaryName, property, value]);
	}


	if (cmd == 'setup') { // Set a whole bunch of values for an adversary
		let r = '';
		for (let prop in setup) {
			let r2 = adversaries.adversaries_edit(adversaryName, userID, config.hostUser, locale, prop, setup[prop]);
			if (r != '') r += slashcommands.localizedString(locale, r, [property, value]) +
				((r == 'error-unknowntag') ? slashcommands.localizedString(locale, 'error-tagsknown', [adversaries.adversaries_tag_list().join(', ')]) : '') + '\n';
		}
		if (r == '') return adversaries.adversaries_show(adversaryName, locale, userID, config.hostUser, userNameFromID);
		return r;
	}

	if (cmd == 'publish') { // Should this adversary be published so all other Narvi users can use it?
		let r = adversaries.adversaries_edit(adversaryName, userID, config.hostUser, locale, 'published', value);
		if (r != '') return slashcommands.localizedString(locale, r, [value]);
		return slashcommands.localizedString(locale, (value ? 'adversary-published' : 'adversary-unpublished'), [adversaryName]);	
	}

	if (cmd == 'attack') { // Add an attack to the adversary
		let r = adversaries.adversaries_attack(adversaryName, userID, config.hostUser, name, dice, damage, injury, ranged, special, comment);
		if (!isNaN(r)) {
			return slashcommands.localizedString(locale, 'adversary-addattack', [adversaryName, r]) + '\n';
		} else {
			return slashcommands.localizedString(locale, r, [adversaryName]);
		}
	}

	if (cmd == 'ability') { // Add an ability to the adversary
		let r = adversaries.adversaries_ability(adversaryName, userID, config.hostUser, name, type, amount, comment);
		if (!isNaN(r)) {
			return slashcommands.localizedString(locale, 'adversary-addability', [adversaryName, r]) + '\n';
		} else {
			return slashcommands.localizedString(locale, r, [adversaryName]);
		}
	}

	if (cmd == 'clear') { // Clear all of something in this adversary
		if (!type || type == '') return slashcommands.localizedString(locale, 'error-unconfirmed', []);
		return slashcommands.localizedString(locale, adversaries.adversaries_clear(adversaryName, userID, config.hostUser, type), [adversaryName]);
	}

	if (cmd == 'delete') { // Delete this adversary permanently
		// if (!confirm) return slashcommands.localizedString(locale, 'error-unconfirmed', []);
		if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'aedit') + ' ' + slashcommands.localizedSubcommand(locale, 'aedit', 'delete'), null, data, 'advdelete');
		let r = adversaries.adversaries_delete(adversaryName, userID, config.hostUser);
		if (r != '') return slashcommands.localizedString(locale, r, [adversaryName]);
		setUserConfig(userID, 'aeditName', '');
		return slashcommands.localizedString(locale, 'adversary-deleted', [adversaryName]);
	}

}

// editing and creating encounters
function encounterCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	let cmd = data.options[0].name;

	// parse options
	let shortname = null, catalog = null, confirm = null, value = null, duration = 0, round = 1, phase = 1,
		quantity = 1, vary = null, property = null, name = null, dice = null, damage = null,
		injury = null, ranged = false, special = '', comment = '', type = '', amount = 0, secret = false,
		parry = 0, features = '', size = 'Large', banetype = 'Nameless', level = null,
		feattable = 'orcbandfeat', successtable = 'orcbandsuccess';
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'shortname') shortname = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'feattable') feattable = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'successtable') successtable = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'catalog') catalog = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'confirm') confirm = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'value') value = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'duration') duration = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'round') round = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'phase') phase = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'quantity') quantity = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'vary') vary = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'property') property = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'name') name = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'dice') dice = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'damage') damage = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'injury') injury = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'ranged') ranged = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'type') type = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'level') level = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'special') special = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'secret') secret = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'amount') amount = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'comment') comment = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'parry') parry = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'features') features = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'size') size = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'banetype') banetype = data.options[0].options[i].value;
	}
	let encounterName = g.selectedEncounter;
	if (g.encounters == undefined) g.encounters = {};

	if (cmd == 'create') { // Create an encounter
		encounterName = stringutil.lowerCase(stringutil.sanitizeString(shortname));
		if (!encounterName.match(validName)) return slashcommands.localizedString(locale, 'error-alphanumeric', [encounterName]);
		if (!encounters.encounters_create(g, encounterName, userID)) return slashcommands.localizedString(locale, 'encounter-exists', [encounterName]);
		g.selectedEncounter = encounterName;
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-created', [encounterName]);
	}

	if (cmd == 'list') { // List available encounters
		return encounters.encounters_list(g, userID, locale);
	}

	if (cmd == 'select') { // Select an existing encounter to edit
		encounterName = stringutil.lowerCase(shortname);
		if (g.encounters[encounterName] == null) return slashcommands.localizedString(locale, 'encounter-notexists', [encounterName]);
		g.selectedEncounter = encounterName;
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-selected', [encounterName]);
	}

	if (encounterName == '') return slashcommands.localizedString(locale, 'encounter-noneselected', []);
	if (g.encounters[encounterName] == undefined) return slashcommands.localizedString(locale, 'encounter-notexists', [encounterName]);
	if (g.combat.active && g.combat.encounter && g.combat.encounter != encounterName) botSendMessage(channelID, slashcommands.localizedString(locale, 'encounter-warning', [encounterName, g.combat.encounter]));

	if (cmd == 'show') { // Show the currently selected encounter
		if (shortname != null) {
			let adversaryName = encounters.encounters_adversary_name_match(g, encounterName, shortname, true);
			if (adversaryName == null) return slashcommands.localizedString(locale, 'adversary-notexists', [shortname]);
			let theEmbed = embeds.buildAdversary(locale, adversaryName, g.encounters[encounterName].adversaries[adversaryName]);
			if (g.encounters[encounterName].adversaries[adversaryName].image != '') return [{embed: theEmbed}, g.encounters[encounterName].adversaries[adversaryName].image];
			return {embed: theEmbed};
		}
		return encounters.encounters_show(g, encounterName, locale);
	}

	if (cmd == 'title' || cmd == 'notes' || cmd == 'image') { // Set some value for an encounter
		let r = encounters.encounters_edit(g, encounterName, locale, cmd, value);
		if (r != '') return slashcommands.localizedString(locale, r, [cmd, value]);
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-changed', [encounterName, cmd, value]);
	}

	if (cmd == 'modifier') { // Set a modifier for an encounter
		let r = encounters.encounters_modifier(g, encounterName, locale, value, duration);
		if (r != '') return slashcommands.localizedString(locale, r, [cmd, value]);
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-changed', [encounterName, cmd, value]);
	}

	if (cmd == 'environment') { // Set an environment for an encounter
		let r = encounters.encounters_environment(g, encounterName, locale, type, level, round);
		if (r != '') return slashcommands.localizedString(locale, r, [cmd, value]);
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-changed', [encounterName, cmd, type]);
	}

	if (cmd == 'event') { // Add an event to an encounter
		let r = encounters.encounters_add_event(g, encounterName, locale, round, phase, value, secret);
		if (r != '') return slashcommands.localizedString(locale, r, [cmd, value]);
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-events-added', [encounterName, round, slashcommands.localizedString(locale, 'initiative-phase' + String(phase), []), value]);
	}

	if (cmd == 'clear') { // Clear all of something in this encounter
		if (!type || type == '') return slashcommands.localizedString(locale, 'error-unconfirmed', []);
		return slashcommands.localizedString(locale, encounters.encounters_clear(g, encounterName, type, shortname), [encounterName, shortname]);
		filehandling.saveGameData(gameData);
	}

	if (cmd == 'copy') { // Create an encounter by copying an existing one
		let newEncounterName = stringutil.lowerCase(stringutil.sanitizeString(shortname));
		let r = encounters.encounters_copy(g, encounterName, newEncounterName, locale);
		if (r != '') return slashcommands.localizedString(locale, r, [newEncounterName]);
		g.selectedEncounter = newEncounterName;
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-copied', [newEncounterName, encounterName]);
	}

	if (cmd == 'combine') { // Modify this encounter by combining in another one's events and adversaries
		if (g.encounters[shortname] == null) return slashcommands.localizedString(locale, 'encounter-notexists', [shortname]);
		let r = encounters.encounters_combine(g, encounterName, shortname, locale);
		if (r != '') return slashcommands.localizedString(locale, r, [shortname]);
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-combined', [shortname]);
	}

	if (cmd == 'generate') { // Modify this encounter by combining in another one's events and adversaries
		let r = encounters.encounters_generate(g, encounterName, userID, config.hostUser, feattable, successtable, locale, torRollDice);
		filehandling.saveGameData(gameData);
		return r + slashcommands.localizedString(locale, 'encounter-generated', [feattable, successtable]);
	}

	if (cmd == 'delete') { // Delete this encounter permanently, or an adversary
		if (!confirm) return buildConfirmDialog(locale, g, userID, slashcommands.localizedCommand(locale, 'encounter') + ' ' + slashcommands.localizedSubcommand(locale, 'encounter', 'delete'), null, data, 'encdelete');
		let r;
		if (shortname != null) {
			let adversaryName = encounters.encounters_adversary_name_match(g, encounterName, shortname, false);
			if (adversaryName == null) return slashcommands.localizedString(locale, 'adversary-notexists', [shortname]);
			r = encounters.encounters_delete_adversary(g, encounterName, adversaryName);
			if (r != '') return slashcommands.localizedString(locale, r, [encounterName, adversaryName]);
			filehandling.saveGameData(gameData);
			return slashcommands.localizedString(locale, 'encounter-deleted-adversary', [encounterName, adversaryName]);
		}
		if (encounterName == g.combat.encounter) return slashcommands.localizedString(locale, 'encounter-deleted', [encounterName]);
		r = encounters.encounters_delete(g, encounterName);
		if (r != '') return slashcommands.localizedString(locale, r, [encounterName]);
		g.selectedEncounter = '';
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-deleted', [encounterName]);
	}

	if (cmd == 'adversary') { // Add an adversary from the catalog to an encounter
		let r = encounters.encounters_add_adversary(g, encounterName, locale, userID, config.hostUser, catalog, stringutil.lowerCase(shortname), quantity, vary);
		if (r != '' && isNaN(r)) return slashcommands.localizedString(locale, r, [catalog, shortname]);
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-adversaries-added', [encounterName, catalog, shortname, r]);
	}

	// remaining functions require a valid adversary within the encounter, so validate the shortname
	let adversaryName = encounters.encounters_adversary_name_match(g, encounterName, shortname, true);
	if (adversaryName == null) return slashcommands.localizedString(locale, 'adversary-notexists', [shortname]);

	if (cmd == 'set') { // Set some value for an encounter's adversary
		// validate the values
		let error;
		[value, error] = adversaries.adversaries_edit_validate(locale, property, value);
		if (error) return slashcommands.localizedString(locale, error, [property, value]);
		// also validate for ephemeral values not stored in catalog adversaries:
		if ((property == 'curendurance' && value > g.encounters[encounterName].adversaries[adversaryName].endurance) ||
			(property == 'curhrscore' && value > g.encounters[encounterName].adversaries[adversaryName].hrscore) ||
			(property == 'wounds' && value >= g.encounters[encounterName].adversaries[adversaryName].might))
			return slashcommands.localizedString(locale, 'adversary-valuetoohigh', [property, value]);
		// set and save
		g.encounters[encounterName].adversaries[adversaryName][property] = value;
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'encounter-adversary-changed', [encounterName, adversaryName, property, value]);
	}

	if (cmd == 'attack') { // Add an attack to the encounter
		let r = encounters.encounters_attack(g, encounterName, adversaryName, name, dice, damage, injury, ranged, special, comment);
		filehandling.saveGameData(gameData);
		if (!isNaN(r)) {
			return slashcommands.localizedString(locale, 'encounter-addattack', [adversaryName, encounterName, r]) + '\n';
		} else {
			return slashcommands.localizedString(locale, r, [adversaryName]);
		}
	}

	if (cmd == 'ability') { // Add an ability to an adversary in the encounter
		let r = encounters.encounters_ability(g, encounterName, adversaryName, name, type, amount, comment);
		filehandling.saveGameData(gameData);
		if (!isNaN(r)) {
			return slashcommands.localizedString(locale, 'encounter-addability', [adversaryName, encounterName, r]) + '\n';
		} else {
			return slashcommands.localizedString(locale, r, [encounterName]);
		}
	}
}

// generate a Nameless Thing as a new adversary in the current encounter
function namelessCommand(userID, channelID, guildID, locale, data) {
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);
	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	// parse options
	let shortname = null, parry = 0, features = '', size = 'Large', banetype = 'Nameless';
	if (data.options) for (let i = 0; i < data.options.length; i++) {
		if (data.options[i].name == 'shortname') shortname = data.options[i].value;
		if (data.options[i].name == 'parry') parry = Number(data.options[i].value);
		if (data.options[i].name == 'features') features = data.options[i].value;
		if (data.options[i].name == 'size') size = data.options[i].value;
		if (data.options[i].name == 'banetype') banetype = data.options[i].value;
	}
	let encounterName = g.selectedEncounter;
	if (g.encounters == undefined) g.encounters = {};
	if (encounterName == '') {
		return slashcommands.localizedString(locale, 'encounter-noneselected', []);
	}
	let [err, rollLines, name] = encounters.encounters_add_nameless(g, encounterName, locale, shortname, parry, features, size, banetype, torRollDice);
	if (err != null) return slashcommands.localizedString(locale, err, [shortname]);
	filehandling.saveGameData(gameData);
	return slashcommands.localizedString(locale, 'encounter-adversaries-nameless', [encounterName, shortname, name]) + '\n\n' + rollLines;
}

function fractionToWords(f, locale) {
	if (f == 0) return slashcommands.localizedString(locale, 'none', []);
	if (f == 0.5) return slashcommands.localizedString(locale, 'half', []);
	if (f == 1.0) return slashcommands.localizedString(locale, 'full', []);
	return String(f);
}

// a command for setting and clearing queued tests
function testCommand(userID, channelID, guildID, locale, data) {
	let cmd = data.options[0].name;
	let g = mySelectedGame(userID);
	if (g == null) return slashcommands.localizedString(locale, 'error-nogame', []);
	if (!isTOR(g)) return slashcommands.localizedString(locale, 'error-toronly', []);

	let charID = currentCharacter(userID), adversary = null, name = null, wholeCompany = false, allAdversaries = false;
	let type = null, tn = null, points = 0, condition = null, feat = null, bonus = 0, modifier = 0, deadly = false;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'type') type = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'tn') tn = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'points') points = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'condition') condition = stringutil.lowerCase(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'feat') feat = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'deadly') deadly = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'bonus') bonus = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'modifier') modifier = Number(data.options[0].options[i].value);
		if (data.options[0].options[i].name == 'character') {
			if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);
			name = stringutil.lowerCase(data.options[0].options[i].value);

			// first check if this is the word company
			if (name == slashcommands.localizedCommand(locale,'company') && (cmd == 'add' || cmd == 'list' || cmd == 'clear')) wholeCompany = true;
			if (name == slashcommands.localizedCommand(locale,'adversaries') && cmd == 'list') allAdversaries = true;

			// next check for a perfect match for an adversary shortname
			if (!wholeCompany && !allAdversaries && g.combat.active && g.combat.encounter != '') {
				for (let a in g.encounters[g.combat.encounter].adversaries) {
					if (stringutil.lowerCase(a) == name) {
						adversary = a;
					}
				}
			}

			// next check for a player hero name
			if (!wholeCompany && !allAdversaries && adversary == null) {
				charID = findCharacterInGame(g, name);
			}

			// finally check for a partial match adversary name
			if (!wholeCompany && !allAdversaries && adversary == null && charID == null && g.combat.active && g.combat.encounter != '') {
				for (let a in g.encounters[g.combat.encounter].adversaries) {
					if (stringutil.lowerCase(a).startsWith(name)) {
						adversary = a;
					}
				}
			}
			if (!wholeCompany && !allAdversaries && adversary == null && charID == null) {
				return slashcommands.localizedString(locale, 'adversary-notexists', [name]);
			}
			if (adversary) name = slashcommands.localizedSubcommand(locale, 'encounter','adversary') + ' ' + adversary;
			else if (wholeCompany) name = slashcommands.localizedString(locale, 'test-wholecompany', []);
			else if (allAdversaries) name = slashcommands.localizedString(locale, 'adversary-adversaries', []);
			else name = null;
		}
	}

	if (!wholeCompany && !allAdversaries && !charID && !adversary) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	if (allAdversaries && !g.combat.encounter) return slashcommands.localizedString(locale, 'error-nocharacter', []);
	if (!name) name = characterDataFetchHandleNulls(g, charID, 'name', null);

	if (cmd == 'list') { // show all queued tests

		let reportHeadings = [
			{title:stringutil.titleCase(slashcommands.localizedString(locale, 'test-responsible', [])), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'test', 'add', 'type')), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.upperCase(slashcommands.localizedOption(locale, 'test', 'add', 'tn')), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'test', 'add', 'feat')), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'test', 'add', 'bonus')), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'test', 'add', 'modifier')), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'test', 'add', 'condition')), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'test', 'add', 'points')), emoji:false, minWidth:0, align:'left'},
			{title:stringutil.titleCaseWords((adversary ? slashcommands.localizedString(locale, 'test-hideous', []) : slashcommands.localizedOption(locale, 'test', 'add', 'deadly'))), emoji:false, minWidth:0, align:'left'}
			];
		if (wholeCompany) reportHeadings.unshift({title:stringutil.titleCase(slashcommands.localizedString(locale, 'character', [])), emoji:false, minWidth:0, align:'left'});
		if (allAdversaries) reportHeadings.unshift({title:stringutil.titleCase(slashcommands.localizedString(locale, 'adversary', [])), emoji:false, minWidth:0, align:'left'});
		let reportData = [];

		if (allAdversaries) {
			for (let a in adversaries = g.encounters[g.combat.encounter].adversaries) {
				if (!encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[a])) {
					g.encounters[g.combat.encounter].adversaries[a].protectionTests.forEach(test => {
						reportData.push([
							a,
							!isNaN(test.responsible) ? characterDataFetchHandleNulls(g, test.responsible, 'name', null) : test.responsible,
							slashcommands.localizedString(locale, 'test-protection', []),
							test.injury,
							test.feat || slashcommands.localizedString(locale, 'none', []),
							test.bonus,
							test.modifier,
							test.condition || slashcommands.localizedString(locale, 'none', []),
							test.poison,
							fractionToWords(test.hideoustoughnessroll, locale)
						]);
					});
				}
			}
		} else if (adversary) {
			g.encounters[g.combat.encounter].adversaries[adversary].protectionTests.forEach(test => {
				reportData.push([
					!isNaN(test.responsible) ? characterDataFetchHandleNulls(g, test.responsible, 'name', null) : test.responsible,
					slashcommands.localizedString(locale, 'test-protection', []),
					test.injury,
					test.feat || slashcommands.localizedString(locale, 'none', []),
					test.bonus,
					test.modifier,
					test.condition || slashcommands.localizedString(locale, 'none', []),
					test.poison,
					fractionToWords(test.hideoustoughnessroll, locale)
				]);
			});
		} else if (wholeCompany) {
			for (let p in g.characters) {
				g.characters[p].protectionTests.forEach(test => {
					reportData.push([
						characterDataFetchHandleNulls(g, p, 'name', null),
						test.responsible,
						slashcommands.localizedString(locale, 'test-protection', []),
						test.injury,
						test.feat || slashcommands.localizedString(locale, 'none', []),
						test.bonus,
						test.modifier,
						test.condition || slashcommands.localizedString(locale, 'none', []),
						test.poison,
						test.deadly ? slashcommands.localizedString(locale, 'on', []) : ''
					]);
				});
				g.characters[p].shadowTests.forEach(test => {
					let tn = test.tn;
					if (tn == null || tn == 0) {
						let attribute = 'wisdom';
						if (test.type == 'dread') attribute = 'valour';
						tn = charSkillTN(g, p, attribute);
					}
					reportData.push([
						characterDataFetchHandleNulls(g, p, 'name', null),
						test.responsible,
						test.type,
						tn,
						test.feat || slashcommands.localizedString(locale, 'none', []),
						test.bonus,
						test.modifier,
						test.condition || slashcommands.localizedString(locale, 'none', []),
						test.points,
						''
					]);
				});
				}
		} else {
			g.characters[charID].protectionTests.forEach(test => {
				reportData.push([
					test.responsible,
					slashcommands.localizedString(locale, 'test-protection', []),
					test.injury,
					test.feat || slashcommands.localizedString(locale, 'none', []),
					test.bonus,
					test.modifier,
					test.condition || slashcommands.localizedString(locale, 'none', []),
					test.poison,
					test.deadly ? slashcommands.localizedString(locale, 'on', []) : ''
				]);
			});
			g.characters[charID].shadowTests.forEach(test => {
				let tn = test.tn;
				if (tn == null || tn == 0) {
					let attribute = 'wisdom';
					if (test.type == 'dread') attribute = 'valour';
					tn = charSkillTN(g, charID, attribute);
				}
				reportData.push([
					test.responsible,
					test.type,
					tn,
					test.feat || slashcommands.localizedString(locale, 'none', []),
					test.bonus,
					test.modifier,
					test.condition || slashcommands.localizedString(locale, 'none', []),
					test.points,
					''
				]);
			});
		}

		if (reportData.length == 0) return slashcommands.localizedString(locale, 'test-none', [name]);

		return '**' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'test-heading', [])) + ' ' + name + '**:\n' + table.buildTable(reportHeadings, reportData, 2);	
	}

	if (!isPlayerLoremasterOfGame(userID, g)) return slashcommands.localizedString(locale, 'error-lmonly', []);

	if (cmd == 'clear') {
		if (adversary) {
			g.encounters[g.combat.encounter].adversaries[adversary].protectionTests = [];
		} else {
			if (wholeCompany) {
				for (let p in g.characters) {
					g.characters[p].protectionTests = [];
					g.characters[p].shadowTests = [];
				}
			} else {
				g.characters[charID].protectionTests = [];
				g.characters[charID].shadowTests = [];
			}
		}
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'test-cleared', [name]);
	}

	if (cmd == 'add') {

		if (adversary) {
			if (type != 'protection') return slashcommands.localizedString(locale, 'test-notshadow', []);
			if (tn == null) return slashcommands.localizedString(locale, 'test-notn', []);
			g.encounters[g.combat.encounter].adversaries[adversary].protectionTests.push({'injury': tn, 'responsible': 'loremaster', 'feat': feat, 'bonus': bonus, 'modifier': modifier, 'condition': condition, 'poison': points, 'hideoustoughnessroll': 0});
		} else {
			let theTest;
			if (type == 'protection') {
				if (tn == null) return slashcommands.localizedString(locale, 'test-notn', []);
				theTest = {'injury': tn, 'responsible': 'loremaster', 'feat': feat, 'bonus': bonus, 'modifier': modifier, 'condition': condition, 'poison': points, 'deadly': deadly};
			} else {
				if (points == 0) points = 1;
				theTest = {'type': type, 'tn': tn, 'responsible': 'loremaster', 'feat': feat, 'bonus': bonus, 'modifier': modifier, 'condition': condition, 'points': points};
			}
			if (wholeCompany) {
				for (let p in g.characters) {
					if (!g.characters[p].active) continue;
					pushTest(g, p, type, theTest);
				}
			} else {
				pushTest(g, charID, type, theTest);
			}
		}
		filehandling.saveGameData(gameData);
		return slashcommands.localizedString(locale, 'test-added', [type, name]);
	}
}

function pushTest(g, charID, type, theTest) {
	if (type == 'protection') {
		g.characters[charID].protectionTests.push(theTest);
	} else {
		g.characters[charID].shadowTests.push(theTest);
	}
}

// USER SETTINGS --------------------------------------------------------------

function showOnOrOff(locale, boolValue) {
	if (boolValue) return slashcommands.localizedString(locale,'on',[]);
	return slashcommands.localizedString(locale,'off',[]);
}

// allows users to set a preference to switch games when doing a command in a server
function autoswitchCommand(userID, locale, data) {
	if (data.options) {
		setUserConfig(userID,'autoSwitch', data.options[0].value);
		filehandling.saveStateData(userConfig);
	}
	return slashcommands.localizedString(locale, 'autoswitch', [showOnOrOff(locale, getUserConfig(userID,'autoSwitch'))])
}

// allows users to opt out of having their last commands saved
function saveLastCommand(userID, locale, data) {
	if (data.options) {
		setUserConfig(userID,'saveLast', data.options[0].value);
		if (data.options[0].value == false) setUserConfig(userID,'lastCmd', '');
		filehandling.saveStateData(userConfig);
	}
	return slashcommands.localizedString(locale, 'savelast', [showOnOrOff(locale, getUserConfig(userID,'saveLast'))]);
}

// HOST ONLY STUFF ------------------------------------------------------------

var lastUser = '', lastCommand = '';

// send the author a message when a problem happens
function notifyHostUser(msg, appendLast) {
	console.log('notifyHostUser(' + msg + ')' + (appendLast ? ('\nLast: ' + lastUser + ': ' + lastCommand) : ''));
	botSendMessage(config.hostChannel, '**NOTICE**: ' + String(msg) + 
		(appendLast ? ('\n**Last**: ' + lastUser + ': `' + String(lastCommand) + '`') : ''));
}

// a set of commands that only the host can do -- set config.json to the discord ID of the host
function mellonCommand(userID, channelID, guildID, locale, data) {
	if (userID != config.hostUser) return '**Error**: Annon Edhellen, edro hi ammen!';
	let cmd = data.options[0].name;
	if (cmd == 'restart' || cmd == 'shutdown') { // reload to incorporate code changes
		// the time delay allows Narvi to get that last message back to me
		setTimeout(function(){ process.exit(cmd == 'shutdown' ? 1 : 0); }, 1000);
		return bot.user.username + ' is shutting down' + (cmd == 'shutdown' ? '' : ' and restarting') + ' in one second.';
	}
	if (cmd == 'ready') { // sometimes Discord doesn't send the ready event
		readyEvent();
		return bot.user.username + ' is forcing the ready event.';
	}
	if (cmd == 'refresh') { // force all characters to refresh
		preloadExternalCaches();
		return 'Refreshing all external character caches.';
	}
	if (cmd == 'announce') { // I'm too scared to use this! announce to every server
		bot.guilds.forEach(c => {
			botSendMessage(c.systemChannelID, data.options[0].options[0].value);
		});
		return 'Announcement sent to all servers I\'m on.';
	}
	if (cmd == 'answer') { // Reply to the last \suggest
		if (lastSuggestor == null) return 'I don\'t know of anyone to answer.';
		botSendPrivateMessage(lastSuggestor, data.options[0].options[0].value);
		return 'Answer sent to **' + userNameFromID(lastSuggestor) + '**.';
	}
	if (cmd == 'delgame') { // delete a game by name
		// find a game by that name
		let gameName = data.options[0].options[0].value;
		gameName = stringutil.titleCase(gameName);
		let game = getGameData(gameName);
		if (game == null || (userID != config.hostUser && !isPlayerInGame(userID, game))) return 'Couldn\'t find a game named `' + gameName + '`.';
		// delete it
		delete gameData[gameName];
		filehandling.saveGameData(gameData);
		botPresence();
		replaceSelectedGame(gameName, null);
		return 'Deleted a game named `' + gameName + '`. I sure hope you meant to do that.';
	}
	if (cmd == 'purge') { // delete all zero-player games
		let emptyGames = [], r = '';
		for (key in gameData) {
			if (Object.keys(gameData[key].characters).length == 0) {
				emptyGames.push(key);
			}
		}
		// show how many
		if (emptyGames.length == 0) return 'There are no empty games to purge.';
		r += '**Empty Games**: ' + emptyGames.join(', ');
		emptyGames.forEach(key => {
			delete gameData[key];
			replaceSelectedGame(key, null);
		});
		filehandling.saveGameData(gameData);
		r += '\n**Empty games are purged.**';
		botPresence();
		return r;
	}
	if (cmd == 'servers') { // show a list of servers the bot is in
		setTimeout(function(){
			let r = '';
			bot.guilds.forEach(c => {
				r += c.name + ' (' + c.id + '), ';
				if (r.length > 1900) {
					notifyHostUser(r,0,'');
					r = '';
				}
			});
			if (r != '') notifyHostUser(r.slice(0,-2), 0, '');
		}, 500);
        return 'I\'m on **' + bot.guilds.size + '** servers:';
	}
	if (cmd == 'random') { // do a test of randomization
		let results = [0,0,0,0,0,0,0,0,0,0,0,0];
		for (let i=0; i < 10000000; i++) {
			let r = featRoll();
			if (r == emoji.EYE) r = 0;
			if (r == emoji.GANDALF) r = 11;
			results[r]++;
		}
		let r = 'Distribution of ten million rolls:\n';
		results.forEach((result, i) => {
			r += emoji.customEmoji(i == 0 ? emoji.EYE : (i == 11 ? emoji.GANDALF : i), false, false, false) + ' ' + result + ' (' + Math.floor(result / 1000) / 100 + '%)\n';
		});
		/*
		// 1d odds
		let successes = 0, r = '';
		for (let feat = 0; feat <= 11; feat++) {
			for (let s1 = 1; s1 <= 6; s1++) {
				if (feat == 11) successes++;
				else if (feat + s1 > 16) successes++;
			}
		}
		r += '1d: ' + successes + '/72 = ' + String(successes * 100 / 72) + '%\n';
		// 2d odds
		successes = 0;
		for (let feat = 0; feat <= 11; feat++) {
			for (let s1 = 1; s1 <= 6; s1++) {
				for (let s2 = 1; s2 <= 6; s2++) {
					if (feat == 11) successes++;
					else if (feat + s1 + s2 > 16) successes++;
				}
			}
		}
		r += '2d: ' + successes + '/432 = ' + String(successes * 100 / 432) + '%\n';
		// 3d odds
		successes = 0;
		for (let feat = 0; feat <= 11; feat++) {
			for (let s1 = 1; s1 <= 6; s1++) {
				for (let s2 = 1; s2 <= 6; s2++) {
					for (let s3 = 1; s3 <= 6; s3++) {
						if (feat == 11) successes++;
						else if (feat + s1 + s2 + s3 > 16) successes++;
					}
				}
			}
		}
		r += '3d: ' + successes + '/2592 = ' + String(successes * 100 / 2592) + '%\n';
		// 4d odds
		successes = 0;
		for (let feat = 0; feat <= 11; feat++) {
			for (let s1 = 1; s1 <= 6; s1++) {
				for (let s2 = 1; s2 <= 6; s2++) {
					for (let s3 = 1; s3 <= 6; s3++) {
						for (let s4 = 1; s4 <= 6; s4++) {
							if (feat == 11) successes++;
							else if (feat + s1 + s2 + s3 + s4 > 16) successes++;
						}
					}
				}
			}
		}
		r += '4d: ' + successes + '/15552 = ' + String(successes * 100 / 15552) + '%\n';
		*/
		//const canvas = Canvas.createCanvas(200, 200);
		//const ctx = canvas.getContext('2d');
		//ctx.fillStyle = 'red';
		//ctx.fillRect(0, 0, 200, 200);
		//const buffer = canvas.toBuffer();
		//let testimage = filehandling.readFile('testimage.jpg');
		//bot.createMessage(channelID, {content: 'test image'},{name: 'testimage.jpg', file: testimage});
		return r;
	}
	if (cmd == 'games') {
		let gamesShown = 0, selectedShown = 0, games2e = 0;
		let r = '**All known games**: ';
		for (key in gameData) {
			gamesShown++;
			r += stringutil.titleCase(key);
			if (gameData[key].loremaster == userID) r += ' (loremaster)';
			else {
				for (let player in gameData[key].players) {
					if (player == userID) r += ' (' + characterDataFetchHandleNulls(gameData[key], gameData[key].players[player].currCharacter, 'name', null) + ')';
				}
			}
			if (gameData[key].loremaster != userID && !gameData[key].lmName.startsWith('User ')) r += ' [' + gameData[key].lmName + ']';
			if (isTOR(gameData[key])) games2e++;
			let c = Object.keys(gameData[key].characters).length;
			r += ' (' + (c ? c : ':zero:') + '), ';
		}
		r = r.slice(0,-2) + '\n';
		if (gamesShown == 0) return 'No games are known!';
		return r + '**Games Listed**: ' + gamesShown + ' (' + games2e + ' using TOR, ' + String(gamesShown-games2e) + ' other)';
	}
	if (cmd == 'leave') { // remove the bot from a server
		bot.guilds.forEach(c => {
			if (c.id == data.options[0].options[0].value) {
				r += c.name + ' (' + c.id + '), ';
				bot.leaveGuild(c.id);
				// this will trigger delete messages which will also clean up games
			}
		});
		if (r == '') return 'No server was found matching ' + data.options[0].options[0].value;
		botPresence();
		return 'Left servers: ' + r.slice(0,-2);
	}
	if (cmd == 'delchar') { // delete a character
		let r = '';
		for (g in gameData) {
			for (p in gameData[g].characters) {
				if (data.options[0].options[0].value == p || data.options[0].options[0].value == gameData[g].characters[p].repositoryKey) {
					r = 'Removed character ' + p + ' from game ' + g;
					delete gameData[g].characters[p];
				}
			}
			for (p in gameData[g].players) {
				if (data.options[0].options[0].value == gameData[g].players[p].currCharacter) {
					gameData[g].players[p].currCharacter = null;
				}
				if (gameData[g].players[p].characters.includes(data.options[0].options[0].value)) {
					gameData[g].players[p].characters = gameData[g].players[p].characters.filter(x => x != data.options[0].options[0].value);
				}
			}
			if (data.options[0].options[0].value == gameData[g].lmCurrCharacter) {
				gameData[g].lmCurrCharacter = null;
			}
			if (gameData[g].lmCharacters.includes(data.options[0].options[0].value)) {
				gameData[g].lmCharacters = gameData[g].players[p].lmCharacters.filter(x => x != data.options[0].options[0].value);
			}
		}
		if (r == '') return 'No character was found matching ' + data.options[0].options[0].value;
		filehandling.saveGameData(gameData);
		return r;
	}
	if (cmd == 'characters') {
		let reportHeadings = [
			{title:'Game', emoji:false, minWidth:0, align:'left'},
			{title:'Loremaster', emoji:false, minWidth:0, align:'left'},
			{title:'CharID', emoji:false, minWidth:0, align:'left'},
			{title:'Character', emoji:false, minWidth:0, align:'left'},
			{title:'Key', emoji:false, minWidth:0, align:'left'}
			];
		let reportData = [];
		let count = 0;
		for (g in gameData) {
			for (p in gameData[g].characters) {
				count++;
				reportData.push([
					stringutil.titleCase(g),
					gameData[g].lmName,
					p,
					characterDataFetchHandleNulls(gameData[g], p, 'name', null),
					gameData[g].characters[p].repositoryKey
				]);
			}
		}
		return '**Character Report**:\n' + table.buildTable(reportHeadings, reportData, 2) + '\n**' + count + '** characters using 2e sheets loaded.';
	}

	let type = null, name = null, tablename = null, range = null, code = null;
	if (data.options[0].options) for (let i = 0; i < data.options[0].options.length; i++) {
		if (data.options[0].options[i].name == 'type') type = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'name') name = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'tablename') tablename = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'range') range = data.options[0].options[i].value;
		if (data.options[0].options[i].name == 'code') code = data.options[0].options[i].value;
	}

	if (cmd == 'taglist') { // Display a list of tags
		if (type == 'tables') return '**Known table tags:** ' + lookuptables.tables_tag_list().join(', ');
		if (type == 'journeys') return '**Known journey tags:** ' + journeys.journeys_tag_list().join(', ');
		if (type == 'adversaries') return '**Known adversary tags:** ' + adversaries.adversaries_tag_list().join(', ');
	}
	if (cmd == 'tagadd') { // Add to the list of tags
		if (type == 'tables') {
			if (!lookuptables.tables_tag_add(name)) return '**Error**: Table tag `' + name + '` already known.';
			return '**Known table tags:** ' + lookuptables.tables_tag_list().join(', ');
		}
		if (type == 'journeys') {
			if (!journeys.journeys_tag_add(name)) return '**Error**: Journey tag `' + name + '` already known.';
			return '**Known journey tags:** ' + journeys.journeys_tag_list().join(', ');
		}
		if (type == 'adversaries') {
			if (!adversaries.adversaries_tag_add(name)) return '**Error**: Adversary tag `' + name + '` already known.';
			return '**Known adversary tags:** ' + adversaries.adversaries_tag_list().join(', ');
		}
	}
	if (cmd == 'tagdel') { // Remove from the list of tags
		if (type == 'tables') {
			if (!lookuptables.tables_tag_remove(name)) return '**Error**: Table tag `' + name + '` not known.';
			return '**Known table tags:** ' + lookuptables.tables_tag_list().join(', ');
		}
		if (type == 'journeys') {
			if (!journeys.journeys_tag_remove(name)) return '**Error**: Journey tag `' + name + '` not known.';
			return '**Known journey tags:** ' + journeys.journeys_tag_list().join(', ');
		}
		if (type == 'adversaries') {
			if (!adversaries.adversaries_tag_remove(name)) return '**Error**: Adversary tag `' + name + '` not known.';
			return '**Known adversary tags:** ' + adversaries.adversaries_tag_list().join(', ');
		}
	}
	if (cmd == 'claim') { // Claim ownership of a table or journey
		if (type == 'tables') {
			lookuptables.tables_claim(name, userID);
			return 'You are now the owner of the table `' + name + '`.';
		}
		if (type == 'journeys') {
			journeys.journeys_claim(name, userID);
			return 'You are now the owner of the journey `' + name + '`.';
		}
		if (type == 'adversaries') {
			adversaries.adversaries_claim(name, userID);
			return 'You are now the owner of the adversary `' + name + '`.';
		}
	}
	if (cmd == 'code') {
		let [r1, r2] = lookuptables.tables_code_row(tablename, userID, config.hostUser, locale, range, code);
		if (r1 != '') return slashcommands.localizedString(locale, r1, [tablename, range, code]);
		return slashcommands.localizedString(locale, 'table-added', [tablename, range]) + '\n' + r2;
	}
}

// make a list of all games found on a particular server
function findGamesOnServer(guildID, userID) {
	let r = [];
	for (key in gameData) {
		if (gameData[key].server == guildID && (userID == null || isPlayerInGame(userID, gameData[key]))) r.push(key);
	}
	return r;
}

// GUI COMMANDS ---------------------------------------------------------------
function goCommand(interaction, userID, channelID, guildID, locale, data, userRealName) {

	let interfaceElements = null, buttonColor = embeds.BUTTON_GREYPLE, title = 'Narvi', bodyText = '', link = null;
	let g = mySelectedGame(userID);
	let charID = currentCharacter(userID);

	// select the right panel by going through various situations and choosing the first match
	// each panel is a selection of interaction elements, a title, and body text

	// INTERACTIVE INTERFACE PANELS --------------------------------------------
	// these game types only have one thing to do in most cases, so no interactive panel, just do the thing
	if (g && g.edition == 'gurps') return diceCommand(userID, userRealName, locale, {'options':[{'name':'roll','value':'3d6'}]}, true);
	else if (g && g.edition == 'expanse') return expanseCommand(userID, userRealName, locale, {'options':[]}, true);
	// if no game, just some basic dice rolling buttons
	else if (g == null || g.edition == 'raven' || g.edition == 'gurps') [interfaceElements, buttonColor, title, bodyText, link] = goCommandNoGame(interaction, userID, channelID, guildID, locale, data, userRealName);
	// if game is coyote, cortex -- call function to get buttons for those rolls
	else if (g.edition == 'coyoteandcrow' && !currentCharacter(userID)) return 'You must have a character to roll Coyote & Crow dice.';
	else if (g.edition == 'coyoteandcrow') {
		let dice, change;
		[interfaceElements, title, bodyText, dice, change] = coyoteandcrow.goCommandCoyote(interaction, userID, locale, characterDataFetchHandleNulls(g, currentCharacter(userID), 'name', null));
		setUserConfig(userID, 'gui-coyote-dice', dice);
		setUserConfig(userID, 'gui-coyote-change', change);
	}
	else if (g.edition == 'cortex') [interfaceElements, buttonColor, title, bodyText, link] = goCommandCortex(interaction, userID, channelID, guildID, locale, data, userRealName);

	// LOREMASTER PANELS ------------------------------------------------------
	else if (isPlayerLoremasterOfGame(userID, g)) { // branches for various Loremaster scenarios
		// if any adversary has a pending protection test - panel for test
		if (adversariesWithTests(g).length > 0) [interfaceElements, buttonColor, title, bodyText, link] = goCommandLMProtTests(g, userID, locale);
		else if (g.journey.active) [interfaceElements, buttonColor, title, bodyText, link] = goCommandLMJourney(g, interaction, userID, channelID, guildID, locale, data, userRealName);
		else if (councilActive(g)) [interfaceElements, buttonColor, title, bodyText, link] = goCommandLMEndeavour(g, 'council', userID, locale);
		else if (endeavourActive(g)) [interfaceElements, buttonColor, title, bodyText, link] = goCommandLMEndeavour(g, 'endeavour',userID, locale);
		// else a panel that's just buttons that go to other panels
		else [interfaceElements, buttonColor, title, bodyText, link] = goCommandLMMenu(g, userID, locale);
	}

	// TEST PANELS ------------------------------------------------------------
	// if you have a protection or shadow test pending -- call function to get buttons for those; string is the list of tests
	else if (g.characters[charID] && g.characters[charID].protectionTests.length != 0) [interfaceElements, buttonColor, title, bodyText, link] = goCommandProtectionTest(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);
	else if (g.characters[charID] && g.characters[charID].shadowTests.length != 0) [interfaceElements, buttonColor, title, bodyText, link] = goCommandShadowTest(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);

	// COMBAT PANELS ----------------------------------------------------------
	// if combat is active and it's stance change phase -- call function to get buttons for those
	else if (g.combat.active && g.combat.phase == initiative.PHASE_CHANGESTANCES) [interfaceElements, buttonColor, title, bodyText, link] = goCommandChangeStance(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);
	// if player and combat is active and it's your stance's action phase or opening volley -- call function to get buttons for those
	else if (g.combat.active && (g.combat.phase == initiative.PHASE_OPENINGVOLLEY || g.combat.phase == initiative.stanceNameToPhase[stringutil.lowerCase(g.characters[charID].stance)])) [interfaceElements, buttonColor, title, bodyText, link] = goCommandActionPhase(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);
	// if combat is active and it's adversary phase -- call function to get buttons for those
	else if (g.combat.active && g.combat.phase == initiative.PHASE_ADVERSARY) [interfaceElements, buttonColor, title, bodyText, link] = goCommandAdversaryPhase(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);

	// COUNCIL AND ENDEAVOR PANELS --------------------------------------------
	// if in a council or skill endeavor -- call function to get buttons for those
	else if (councilActive(g)) [interfaceElements, buttonColor, title, bodyText, link] = goCommandEndeavour('council',interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);
	else if (endeavourActive(g)) [interfaceElements, buttonColor, title, bodyText, link] = goCommandEndeavour('endeavour',interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);

	// PLAYER ACTION PANELS ---------------------------------------------------
	// else if player and someone is wounded and untreated and you have pips in Healing -- call function to get buttons for those
	else if (skillValue(characterDataFetchHandleNulls(g, charID, 'healing', null)) > 0 && anyoneICanTreat(g, charID).length > 0) [interfaceElements, buttonColor, title, bodyText, link] = goCommandWoundTreat(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);
	// otherwise a default player panel with basic character info, updates, and skill rolls functions
	else [interfaceElements, buttonColor, title, bodyText, link] = goCommandPlayerDefault(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID);

	if (link == null || link == '') link = 'https://bitbucket.org/HawthornThistleberry/narvi/';
	// return this with an embed on it
	return embeds.buildGUI(
		locale, g, getGameName(g), [userID],
		title,
		'https://cdn.discordapp.com/attachments/848011016612478987/881999824503857212/Narvi_avatar.png',
		link,
		bodyText,
		'',
		buttonColor,
		interfaceElements
		);
}

// produces a list of people that have wounds that can be treated
function anyoneICanTreat(g, charID) {
	let r = [];
	for (let p in g.characters) {
		if (g.characters[p].active && characterDataFetchHandleNulls(g, p, 'wounded', null) == 'TRUE' && characterDataFetchHandleNulls(g, p, 'woundtreated', null) == 'untreated') r.push(p);
	}
	return r;
}

// produces a list of adversaries with a pending protection test
function adversariesWithTests(g) {
	if (!g.combat.active || !g.combat.encounter || Object.keys(g.encounters[g.combat.encounter].adversaries).length == 0) return [];
	let r = [];
	for (let a in g.encounters[g.combat.encounter].adversaries) {
		if (!encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[a]) && g.encounters[g.combat.encounter].adversaries[a].protectionTests.length > 0) r.push(a);
	}
	return r;
}

// allows a shorthand format for buttons
function goCommandButton(cmd, color, s, extratext, emoji) {
	return {
		'command':'gui-' + cmd,
		'color': color,
		'label': s,
		'extratext': extratext,
		'emoji':emoji
	};
}

// allows a shorthand format for menu options
function goCommandOption(value, label, description, emoji) {
	return {
		'label': label,
		'value': value,
		'description': description,
		'emoji':emoji
	};
}

// given a weapon that's due to be added to the action row, add one or both entries
function addActionRowsForWeapon(actionRow, locale, weaponName, wData) {
	if (wData[8] == '2H') addActionRowForWeapon(actionRow, locale, weaponName, wData, 2);
	else if (wData[8] == 'Both') {
		addActionRowForWeapon(actionRow, locale, weaponName, wData, 1);
		addActionRowForWeapon(actionRow, locale, weaponName, wData, 2);
	}
	else addActionRowForWeapon(actionRow, locale, weaponName, wData, 1);
}

// add a single entry to the action row for a weapon for a specific handedness
function addActionRowForWeapon(actionRow, locale, weaponName, wData, handedness) {
	//console.log('addActionRowForWeapon(' + actionRow + ', ' + locale + ', ' + weaponName + ', ' + wData + ', ' + handedness + ')');
	let value = 'weapon-' + handedness + 'h-' + stringutil.wordWithoutSpaces(stringutil.lowerCase(weaponName));
	let label = '=' + weaponName + ' (' + handedness + 'h)';
	if (label.length > 100) label = label.substr(0,99);
	let description = '=' + slashcommands.localizedString(locale, 'gui-desc-weapon', [weaponName, String(handedness)]);
	if (description.length > 100) description = description.substr(0,96) + '...';
	let emoji = GUI_BATTLE;
	if (weaponCanBeUsedRanged(wData)) emoji = GUI_HUNTING;
	if (weaponCanBeUsedBrawling(wData)) emoji = GUI_KNIFE;
	actionRow.push(goCommandOption(value, label, description, emoji));
}

// interaction elements if you have no game: Help, Version; TOR 1, 2, 3, 4, 5; feat, success; d4, d6, d8, d12, d20; d10, d%
function goCommandNoGame(interaction, userID, channelID, guildID, locale, data, userRealName) {

	let interfaceElements = [
		{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('help', 2, '!help', '', GUI_HELP),
				goCommandButton('version', 2, '!version', '', GUI_VERSION)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('tor1', 2, '=TOR +1d', '', BUTTON_ONE),
				goCommandButton('tor2', 2, '=TOR +2d', '', BUTTON_TWO),
				goCommandButton('tor3', 2, '=TOR +3d', '', BUTTON_THREE),
				goCommandButton('tor4', 2, '=TOR +4d', '', BUTTON_FOUR),
				goCommandButton('tor5', 2, '=TOR +5d', '', BUTTON_FIVE)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('feat', 2, '!feat', '', GUI_DICE),
				goCommandButton('success', 2, '!success', '', GUI_DICE),
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('dice4', 2, '=d4', '', GUI_DICE),
				goCommandButton('dice6', 2, '=d6', '', GUI_DICE),
				goCommandButton('dice8', 2, '=d8', '', GUI_DICE),
				goCommandButton('dice12', 2, '=d12', '', GUI_DICE),
				goCommandButton('dice20', 2, '=d20', '', GUI_DICE),
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('dice10', 2, '=d10', '', GUI_DICE),
				goCommandButton('dice100', 2, '=d%', '', GUI_DICE),
			]
		}
	];
	return [interfaceElements, embeds.BUTTON_GREYPLE, 'Narvi', version, null];
}

// interaction elements if you have a Cortex game
function goCommandCortex(interaction, userID, channelID, guildID, locale, data, userRealName) {

	let diceArray = [
		goCommandOption('none', 'none', '', GUI_DICE),
		goCommandOption('d4', '=d4', '', GUI_DICE),
		goCommandOption('d6', '=d6', '', GUI_DICE),
		goCommandOption('d8', '=d8', '', GUI_DICE),
		goCommandOption('d10', '=d10', '', GUI_DICE),
		goCommandOption('d12', '=d12', '', GUI_DICE)
		];

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-cortex1',
			'contents':[...diceArray]
		},{
			'rowtype':'pulldown',
			'id':'gui-cortex2',
			'contents':[...diceArray]
		},{
			'rowtype':'pulldown',
			'id':'gui-cortex3',
			'contents':[...diceArray]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('cortex', 2, 'journey-button-plain', '', GUI_DICE)
			]
		}
	];
	setUserConfig(userID, 'cortex1', 'none');
	setUserConfig(userID, 'cortex2', 'none');
	setUserConfig(userID, 'cortex3', 'none');

	let r = version;
	if (mySelectedGame(userID) && currentCharacter(userID)) {
		r = scribblesDisplay(mySelectedGame(userID), currentCharacter(userID), locale, true);
	}

	return [interfaceElements, embeds.BUTTON_GREYPLE, "Cortex", r, 'https://www.cortexrpg.com/'];
}

// interaction elements if you have a protection test
function goCommandProtectionTest(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID) {

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-bonusdice',
			'contents':[...bonusDiceMenu()]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[...rollButtons('protection',false,false)]
		}
	];
	// set a default to 0 bonus
	setUserConfig(userID, 'gui-bonusdice','0');

	let tests = [];
	g.characters[charID].protectionTests.forEach(test => {
		let testText = stringutil.upperCase(slashcommands.localizedOption(locale, 'test', 'add', 'tn')) + test.injury;
		if (test.responsible != '' && test.responsible != 'loremaster' && test.responsible != 'journey') testText += ' ' + slashcommands.localizedString(locale, 'by', []) + ' ' + test.responsible;
		if (test.deadly) testText += ' ' + slashcommands.localizedOption(locale, 'test', 'add', 'deadly');
		tests.push(testText);
	});
	let r = '**'+ stringutil.titleCaseWords(slashcommands.localizedString(locale, 'test-prottests', [])) + '**: ' + tests.join(', ') + '\n';

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, getCharLink(g, charID)];
}

// interaction elements if you have a shadow test
function goCommandShadowTest(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID) {

	let tests = [], testType = null;
	g.characters[charID].shadowTests.forEach(test => {
		if (testType == null) testType = test.type;
		let testText = test.type + ' (' + test.points;
		if (test.condition != '') testText += ', ' + test.condition;
		testText += ')';
		tests.push(testText);
	});
	let r = '**'+ stringutil.titleCaseWords(slashcommands.localizedString(locale, 'test-shadowtests', [])) + '**: ' + tests.join(', ') + '\n';

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-bonusdice',
			'contents':[...bonusDiceMenu()]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[...rollButtons(testType,false,false)]
		}
	];
	// set a default to 0 bonus
	setUserConfig(userID, 'gui-bonusdice','0');

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, getCharLink(g, charID)];
}

// interaction elements during stance change phase
function goCommandChangeStance(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID) {

	let interfaceElements = [
		{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('stance-forward', 2, '!stance-stance-forward', '', GUI_FORWARD),
				goCommandButton('stance-open', 2, '!stance-stance-open', '', GUI_OPEN),
				goCommandButton('stance-defensive', 2, '!stance-stance-defensive', '', GUI_DEFENSIVE),
				goCommandButton('stance-rearward', 2, '!stance-stance-rearward', '', GUI_REARWARD),
				goCommandButton('stance-skirmish', 2, '!stance-stance-skirmish', '', GUI_SKIRMISH),
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('stance-absent', 2, '!stance-stance-absent', '', GUI_ABSENT),
				goCommandButton('stance-unchanged', 2, '!stance-stance-unchanged', '', GUI_UNCHANGED)
			]
		}
	];

	let r = slashcommands.localizedString(locale, 'stance-set', [characterDataFetchHandleNulls(g, charID, 'name', null), stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'stance', 'stance', g.characters[charID].stance))]);

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, getCharLink(g, charID)];
}

// interaction elements during opening volley or your action
function goCommandActionPhase(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID) {

	let rangedOnly = false, seized = (g.characters[charID].seized || g.characters[charID].bitten || g.characters[charID].drained);
	if (g.combat.phase == initiative.PHASE_OPENINGVOLLEY || g.combat.phase == initiative.PHASE_REARWARD) rangedOnly = true;

	let interfaceElements = [];

	// for the first row, build a pulldown list of actions including:
	// all weapons that apply, 1h and 2h on separate rows, depending on phase, ranged, brawling, seized; value is weapon-<handedness>-<weaponname>
	let weapons = characterDataFetch(g, charID, 'weapons', null);
	let actionRow = [];
	let defaultAction = null;
	if (seized) {
		defaultAction = 'brawling';
		actionRow.push(goCommandOption('brawling', 'proficiency-brawling', 'gui-desc-brawling', GUI_BRAWLING));
		for (let weaponName in weapons) {
			let wData = characterDataFetch(g, charID, 'weapons', weaponName);
			if (weaponCanBeUsedBrawling(wData)) addActionRowsForWeapon(actionRow, locale, weaponName, wData);
		}
	} else {
		let bestWeapon = findBestWeapon(g, charID, rangedOnly);
		if (bestWeapon != '') { // can happen if you have no ranged in a ranged stance, or vice versa
			let wData = characterDataFetch(g, charID, 'weapons', bestWeapon);
			addActionRowsForWeapon(actionRow, locale, bestWeapon, wData);
			if (wData[8] == '2H') defaultAction = 'weapon-2h-' + stringutil.wordWithoutSpaces(stringutil.lowerCase(bestWeapon));
			else defaultAction = 'weapon-1h-' + stringutil.wordWithoutSpaces(stringutil.lowerCase(bestWeapon));
		}
		for (let weaponName in weapons) {
			wData = characterDataFetch(g, charID, 'weapons', weaponName);
			if (weaponCanBeUsedRanged(wData) == rangedOnly && weaponName != bestWeapon && wData[4] == 'TRUE') {
				addActionRowsForWeapon(actionRow, locale, weaponName, wData);
				if (defaultAction == null) {
					if (wData[8] == '2H') defaultAction = 'weapon-2h-' + stringutil.wordWithoutSpaces(stringutil.lowerCase(weaponName));
					else defaultAction = 'weapon-1h-' + stringutil.wordWithoutSpaces(stringutil.lowerCase(weaponName));
				}
			}
		}
	}
	// you can always throw a stone when ranged; icon is :rock:, value is weapon-1h-stone
	if (/*hasTrait(g, charID, 'virtue', 'sure at the mark') &&*/ rangedOnly) {
		actionRow.push(goCommandOption('weapon-1h-stone', 'gui-stone', 'gui-desc-stone', GUI_STONE));
		if (defaultAction == null) defaultAction = 'weapon-1h-stone';
	}
	// Seek Advantage: value is seek-advantage
	actionRow.push(goCommandOption('seek-advantage', 'gui-advantage', 'gui-desc-advantage', GUI_BATTLE));
	if (defaultAction == null) defaultAction = 'seek-advantage';
	// the relevant combat task for your stance: value is action-<action>
	let action = null, skill = null, errorMsg = null;
	[action, skill, errorMsg] = actionsFromStance(g, charID, charEffectiveStance(g, charID));
	if (!errorMsg) {
		if (action == 'protect') { // need multiple ones for various people you could protect
			// for each hero in a close combat stance other than the one acting
			for (let p in g.characters) {
				if (p != charID && charEffectiveStance(g, p) != 'rearward') {
					let hero = characterDataFetchHandleNulls(g, p, 'name', null);
					let label = '=' + slashcommands.localizedString(locale,'action-protect',[]) + ' (' + hero + ')';
					actionRow.push(goCommandOption('action-' + action + '-' + hero, label, 'gui-desc-' + action, GUI_ATHLETICS));
				}
			}
		} else {
			let label = 'action-' + action;
			if (action == 'gainground') label = 'action-gain';
			let emoji = {'awe':GUI_AWE, 'enhearten':GUI_ENHEARTEN, 'athletics':GUI_ATHLETICS, 'scan':GUI_SCAN}[skill];
			actionRow.push(goCommandOption('action-' + action, label, 'gui-desc-' + action, emoji));
		}
	}
	interfaceElements.push({
			'rowtype':'pulldown',
			'id':'gui-combataction',
			'contents':[...actionRow]
	});
	// set a default to whatever's in the first row
	setUserConfig(userID, 'gui-combataction',defaultAction);

	// for the second row, build a pulldown list of targets or parries
	actionRow = [];
	defaultAction = null;
	if (g.combat.encounter && Object.keys(g.encounters[g.combat.encounter].adversaries).length > 0) {
		// if you have adversaries, a list of living adversaries with troll icons, your engaged ones at top, value is adv-<shortname>
		for (let a in g.encounters[g.combat.encounter].adversaries) {
			if (!encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[a]) && g.encounters[g.combat.encounter].adversaries[a].engagement.includes(charID)) {
				if (defaultAction == null) defaultAction = 'adv-' + stringutil.lowerCase(stringutil.wordWithoutSpaces(a));
				actionRow.push(addAdversaryToPulldown(g, a, true));
			}
		}
		for (let a in g.encounters[g.combat.encounter].adversaries) {
			if (!encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[a]) && !g.encounters[g.combat.encounter].adversaries[a].engagement.includes(charID)) {
				if (defaultAction == null) defaultAction = 'adv-' + stringutil.lowerCase(stringutil.wordWithoutSpaces(a));
				actionRow.push(addAdversaryToPulldown(g, a, false));
			}
		}
	}
	// if the above didn't happen or it didn't load anything, make a default list of parries
	if (defaultAction == null) {
		// if not, a list of parry values, value is parry-<amount>
		defaultAction = 'parry-0';
		for (i=0; i<=4; i++) {
			actionRow.push(goCommandOption('parry-' + i, '=' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-parry',[])) + (i == 0 ? ' ' : ' +') + i, '', SPEND_FEND));
		}
	}
	interfaceElements.push({
			'rowtype':'pulldown',
			'id':'gui-combattarget',
			'contents':[...actionRow]
	});
	// set a default to whatever's in the first row
	setUserConfig(userID, 'gui-combattarget',defaultAction);

	// then some roll buttons
	interfaceElements.push({
			'rowtype':'pulldown',
			'id':'gui-bonusdice',
			'contents':[...bonusDiceMenu()]
	});
	interfaceElements.push({
			'rowtype':'buttons',
			'id':'',
			'contents':[...rollButtons('combat',true,true)] // show inspired and magical due to Seek Advantage and Combat Task
	});
	// set a default to 0 bonus
	setUserConfig(userID, 'gui-bonusdice','0');

	// display your current combat status including who you're engaged with and relevant bonus/penalty dice
	let r = '__' + stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'attack')) + '__' + initiative.relevantCombatBonuses(g, charID, g.combat.phase, locale, false);
	// if engaged, add \n and then two spacings and a few more spaces and a list of engaged
	let engaged = encounters.engaged_adversaries(g, charID);
	if (engaged.length > 0) r += '\n__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'combat-engagement', [])) + '__: ' + engaged.join(', ') + '\n';
	if (seized) r += '\n**' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'condition-seized', [])) + '!**';

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, getCharLink(g, charID)];
}

// builds the action row for an adversary
function addAdversaryToPulldown(g, a, engaged) {
	let emoji = GUI_ADVERSARY;
	if (g.config.includes('combat-advhealth')) {
		if (g.encounters[g.combat.encounter].adversaries[a].wounds > 0) emoji = GUI_ADVWOUND;
		else if (g.encounters[g.combat.encounter].adversaries[a].curendurance < (g.encounters[g.combat.encounter].adversaries[a].endurance * 3 / 10)) emoji = GUI_ADVLOW;
		else if (g.encounters[g.combat.encounter].adversaries[a].curendurance < (g.encounters[g.combat.encounter].adversaries[a].endurance * 6 / 10)) emoji = GUI_ADVMED;
		else if (g.encounters[g.combat.encounter].adversaries[a].curendurance < (g.encounters[g.combat.encounter].adversaries[a].endurance * 9 / 10)) emoji = GUI_ADVHIGH;
	}
	return goCommandOption('adv-' + a, '=' + a, '=' + stringutil.trimRight(g.encounters[g.combat.encounter].adversaries[a].name + (engaged ? ' ' + GUI_BATTLE : ''),99), emoji);
}

// interaction elements during adversary phase
function goCommandAdversaryPhase(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID) {

	let interfaceElements = [
		{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('condition-knockback', 2, 'condition-knockback', '', GUI_KNOCKBACK),
				goCommandButton('condition-daunted', 2, 'condition-daunted', '', GUI_DAUNTED),
				goCommandButton('condition-seized', 2, 'condition-seized', '', GUI_SEIZED),
				goCommandButton('condition-misfortune', 2, 'condition-misfortune', '', GUI_MISFORTUNE),
				goCommandButton('condition-dismayed', 2, 'condition-dismayed', '', GUI_DISMAYED)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('condition-dreaming', 2, 'condition-dreaming', '', GUI_DREAMING),
				goCommandButton('condition-haunted', 2, 'condition-haunted', '', GUI_HAUNTED),
				goCommandButton('condition-bleeding', 2, 'token-bleeding', '', GUI_BLEEDING),
				goCommandButton('condition-weary', 2, 'condition-weary', '', GUI_WEARY),
				goCommandButton('condition-miserable', 2, 'condition-miserable', '', GUI_MISERABLE)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('damage2', 2, 'stat-damage', '2', GUI_HURT),
				goCommandButton('damage3', 2, 'stat-damage', '3', GUI_HURT),
				goCommandButton('damage4', 2, 'stat-damage', '4', GUI_HURT),
				goCommandButton('damage5', 2, 'stat-damage', '5', GUI_HURT),
				goCommandButton('damage6', 2, 'stat-damage', '6', GUI_HURT),
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('wound-unwounded', 2, 'token-unwounded', '', GUI_WOUND),
				goCommandButton('wound-untreated', 2, 'gui-wound-untreated', '', GUI_WOUND),
				goCommandButton('wound-treated', 2, 'token-treated', '', GUI_WOUND),
				goCommandButton('wound-failed', 2, 'gui-wound-failed', '', GUI_WOUND),
				goCommandButton('wound-dying', 2, 'gui-wound-dying', '', GUI_WOUND),
			]
		}
	];

	// display your current combat status including who you're engaged with and relevant bonus/penalty dice
	let seized = (g.characters[charID].seized || g.characters[charID].bitten || g.characters[charID].drained);
	let r = '__' + stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'attack')) + '__' + initiative.relevantCombatBonuses(g, charID, g.combat.phase, locale, false);
	// if engaged, add \n and then two spacings and a few more spaces and a list of engaged
	let engaged = encounters.engaged_adversaries(g, charID);
	if (engaged.length > 0) r += '\n__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'combat-engagement', [])) + '__: ' + engaged.join(', ') + '\n';
	if (seized) r += '\n**' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'condition-seized', [])) + '!**';

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, getCharLink(g, charID)];
}

// interaction elements if you have an active council or endeavour
function goCommandEndeavour(endeavourtype, interaction, userID, channelID, guildID, locale, data, userRealName, g, charID) {

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-skillpick',
			'contents':[...skillsCommandOptions(locale, endeavourtype, g, charID)]
		},{
			'rowtype':'pulldown',
			'id':'gui-bonusdice',
			'contents':[...bonusDiceMenu()]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[...rollButtons(endeavourtype,true,true)]
		}
	];
	// set a default to 0 bonus
	setUserConfig(userID, 'gui-bonusdice','0');

	let r;
	if (endeavourtype == 'council') {
		r = councilShow(g, locale, false);
		setUserConfig(userID, 'gui-skillpick', 'persuade');
	} else {
		r = endeavourShow(g, locale, false);
		setUserConfig(userID, 'gui-skillpick', 'awe');
	}
	
	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, getCharLink(g, charID)];
}

// interaction elements during opening volley or your action
function goCommandWoundTreat(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID) {

	let patients = anyoneICanTreat(g, charID);

	let interfaceElements = [];

	// for the first row, build a pulldown list of people who can be treated
	let actionRow = [];
	let defaultAction = null;
	let r = '';
	patients.forEach(patient => {
		if (defaultAction == null) defaultAction = patient;
		let name = characterDataFetchHandleNulls(g, patient, 'name', null),
			daysleft = characterDataFetchHandleNulls(g, patient, 'wounddaysleft', null);
		actionRow.push(goCommandOption(patient, '=' + name, '=' + daysleft + ' ' + slashcommands.localizedString(locale, 'token-daysleft',[]), GUI_WOUND));
		r += name + ': ' + daysleft + ' ' + slashcommands.localizedString(locale, 'token-daysleft',[]) + '\n';
	});
	// set a default to whatever's in the first row
	setUserConfig(userID, 'gui-patient',defaultAction);

	interfaceElements.push({
			'rowtype':'pulldown',
			'id':'gui-patient',
			'contents':[...actionRow]
	});
	// set a default to whatever's in the first row
	setUserConfig(userID, 'gui-patient',defaultAction);

	// then some roll buttons
	interfaceElements.push({
			'rowtype':'pulldown',
			'id':'gui-bonusdice',
			'contents':[...bonusDiceMenu()]
	});
	// set a default to 0 bonus
	setUserConfig(userID, 'gui-bonusdice','0');
	interfaceElements.push({
			'rowtype':'buttons',
			'id':'',
			'contents':[...rollButtons('treat',true,true)]
	});

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, getCharLink(g, charID)];
}

// interaction elements if you have a game but nothing else applies
function goCommandPlayerDefault(interaction, userID, channelID, guildID, locale, data, userRealName, g, charID) {

	let interfaceElements = [
		{
			'rowtype':'buttons',
			'id':'',
			'contents':[ // [Status][Sheet][Refresh][Explain][Help]
				goCommandButton('status', 2, '!status', '', GUI_STATUS),
				goCommandButton('sheet', 2, '!sheet', '', GUI_SHEET),
				goCommandButton('refresh', 2, '!character-refresh', '', GUI_REFRESH),
				goCommandButton('explain', 2, '!explain', '', GUI_EXPLAIN),
				goCommandButton('help', 2, '!help', '', GUI_HELP),
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[ // [Gain Hope][Lose Hope][Gain Shadow][Lose Shadow]
				goCommandButton('gainhope', 2, 'gui-gainhope', '', GUI_GAIN1),
				goCommandButton('losehope', 2, 'gui-losehope', '', GUI_LOSE1),
				goCommandButton('gainshadow', 2, 'gui-gainshadow', '', GUI_GAIN2),
				goCommandButton('loseshadow', 2, 'gui-loseshadow', '', GUI_LOSE2)
			]
		},{
			'rowtype':'pulldown',
			'id':'gui-skillpick',
			'contents':[...skillsCommandOptions(locale, 'skill', g, charID)]
		},{
			'rowtype':'pulldown',
			'id':'gui-bonusdice',
			'contents':[...bonusDiceMenu()]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[...rollButtons('skill',true,true)]
		}
	];
	// set a default to 0 bonus
	setUserConfig(userID, 'gui-bonusdice','0');
	setUserConfig(userID, 'gui-skillpick', 'awe');

	let r = characterDataFetchHandleNulls(g, charID, 'name', '') + ' (' + characterDataFetchHandleNulls(g, charID, 'culture', '') + ', ' + characterDataFetchHandleNulls(g, charID, 'calling', '') + ')';

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, getCharLink(g, charID)];
}

function skillCommandOption(locale, skill, g, charID) {
	let skillVal = '', skillFavored = false;
	if (g && charID) {
		skillVal = characterDataFetchHandleNulls(g, charID, skill, null);
		skillFavored = skillVal.endsWith('f');
	}
	return goCommandOption(
		skill, 
		'=' + stringutil.titleCase(slashcommands.localizedString(locale, 'skill-' + skill)) + (skillVal == '' ? '' : (': ' + (skillFavored ? '\u221A' : '') + ' ' + String(skillValue(skillVal)))), 
		'gui-' + skill + 'desc',
		{
			'awe': GUI_AWE,
			'athletics': GUI_ATHLETICS,
			'awareness': GUI_AWARENESS,
			'hunting': GUI_HUNTING,
			'song': GUI_SONG,
			'craft': GUI_CRAFT,
			'enhearten': GUI_ENHEARTEN,
			'travel': GUI_TRAVEL,
			'insight': GUI_INSIGHT,
			'healing': GUI_HEALING,
			'courtesy': GUI_COURTESY,
			'battle': GUI_BATTLE,
			'persuade': GUI_PERSUADE,
			'stealth': GUI_STEALTH,
			'scan': GUI_SCAN,
			'explore': GUI_EXPLORE,
			'riddle': GUI_RIDDLE,
			'lore': GUI_LORE
		}[skill]
	);
}

function skillsCommandOptions(locale, type, g, charID) { // a pulldown of skills
	if (type == 'council') { // a version with council-type skills at the top
		return [
			skillCommandOption(locale, 'persuade', g, charID), skillCommandOption(locale, 'courtesy', g, charID), skillCommandOption(locale, 'enhearten', g, charID), skillCommandOption(locale, 'awe', g, charID), skillCommandOption(locale, 'riddle', g, charID), skillCommandOption(locale, 'song', g, charID), skillCommandOption(locale, 'insight', g, charID), skillCommandOption(locale, 'athletics', g, charID), skillCommandOption(locale, 'awareness', g, charID), skillCommandOption(locale, 'hunting', g, charID), skillCommandOption(locale, 'craft', g, charID), skillCommandOption(locale, 'travel', g, charID), skillCommandOption(locale, 'healing', g, charID), skillCommandOption(locale, 'battle', g, charID), skillCommandOption(locale, 'stealth', g, charID), skillCommandOption(locale, 'scan', g, charID), skillCommandOption(locale, 'explore', g, charID), skillCommandOption(locale, 'lore', g, charID)
		];
	}
	return [
			skillCommandOption(locale, 'awe', g, charID), skillCommandOption(locale, 'athletics', g, charID), skillCommandOption(locale, 'awareness', g, charID), skillCommandOption(locale, 'hunting', g, charID), skillCommandOption(locale, 'song', g, charID), skillCommandOption(locale, 'craft', g, charID), skillCommandOption(locale, 'enhearten', g, charID), skillCommandOption(locale, 'travel', g, charID), skillCommandOption(locale, 'insight', g, charID), skillCommandOption(locale, 'healing', g, charID), skillCommandOption(locale, 'courtesy', g, charID), skillCommandOption(locale, 'battle', g, charID), skillCommandOption(locale, 'persuade', g, charID), skillCommandOption(locale, 'stealth', g, charID), skillCommandOption(locale, 'scan', g, charID), skillCommandOption(locale, 'explore', g, charID), skillCommandOption(locale, 'riddle', g, charID), skillCommandOption(locale, 'lore', g, charID)
	];
}

function rollButtons(prefix, showInspired, showMagical) {
	let r = [
			goCommandButton(prefix + '-plain', 2, 'journey-button-plain', '', GUI_PLAIN),
			goCommandButton(prefix + '-hope', 2, 'journey-button-hope', '', GUI_HOPE)
			];
	if (showInspired) r.push(goCommandButton(prefix + '-inspired', 2, 'journey-button-inspired', '', GUI_INSPIRED));
	if (showMagical) r.push(goCommandButton(prefix + '-magical', 2, 'journey-button-magical', '', GUI_MAGICAL));
	return r;
}

function bonusDiceMenu() {
	let r = [
			goCommandOption('0', 'none', 'bonus', BUTTON_ZERO),
			goCommandOption('-3', '=-3d', 'bonus', GUI_LOSE3),
			goCommandOption('-2', '=-2d', 'bonus', GUI_LOSE2),
			goCommandOption('-1', '=-1d', 'bonus', GUI_LOSE1),
			goCommandOption('1', '=+1d', 'bonus', GUI_GAIN1),
			goCommandOption('2', '=+2d', 'bonus', GUI_GAIN2),
			goCommandOption('3', '=+3d', 'bonus', GUI_GAIN3)
			];
	return r;
	//-- remove the else bracketing around line 9936
}

// interaction elements for an LM if there's a protection test
function goCommandLMProtTests(g, userID, locale) {

	let interfaceElements = [];
	let actionRow = [];
	let defaultAction = null;
	let title = '', text = [];
	adversariesWithTests(g).forEach(a => {
		if (defaultAction == null) defaultAction = stringutil.lowerCase(stringutil.wordWithoutSpaces(a));
		let testList = [];
		g.encounters[g.combat.encounter].adversaries[a].protectionTests.forEach(test => {
			testList.push('TN' + test.injury + ' ' + slashcommands.localizedString(locale,'by',[]) + ' ' + (!isNaN(test.responsible) ? characterDataFetchHandleNulls(g, test.responsible, 'name', null) : test.responsible));
		});
		actionRow.push(goCommandOption(a, '=' + stringutil.trimRight(a + ' (' + g.encounters[g.combat.encounter].adversaries[a].name + ')',99), '=' + stringutil.trimRight(testList.join('; '),99), GUI_ADVERSARY));
		text.push(a + ' (' + g.encounters[g.combat.encounter].adversaries[a].name + '): ' + testList.join(', '));
	});
	interfaceElements.push({
			'rowtype':'pulldown',
			'id':'gui-lmadversary',
			'contents':[...actionRow]
	});
	setUserConfig(userID, 'gui-lmadversary',defaultAction);
	interfaceElements.push({
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('adv-test-plain', 2, 'journey-button-plain', '', GUI_ADVERSARY),
				goCommandButton('adv-test-hate', 2, 'adversary-hate', '', GUI_HATE)
			]
		});

	title = slashcommands.localizedString(locale,'test-prottests',[]);
	if (g.combat.active) title += ': ' + (g.combat.title || g.encounter);

	return [interfaceElements, embeds.BUTTON_GREYPLE, title, text.join('; '), null];
}

// interaction elements for an LM during a journey
function goCommandLMJourney(g, interaction, userID, channelID, guildID, locale, data, userRealName) {

	let interfaceElements = [
		{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('journey-show', 2, '!journey-show', '', GUI_VERSION),
				goCommandButton('journey-reset', 2, '!journey-reset', '', GUI_REFRESH),
				goCommandButton('journey-end', 2, '!journey-end', '', GUI_ABSENT),
				goCommandButton('journey-abort', 2, '!journey-abort', '', SPEND_REDX)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('journey-undo', 2, '!journey-undo', '', GUI_REARWARD),
				goCommandButton('journey-log', 2, '!journey-log', '', GUI_SHEET)
			]
		}
	];

	return [interfaceElements, embeds.BUTTON_GREYPLE, g.journey.active, journeys.journeys_status(g, locale), null];
}

// interaction elements for an LM during a council or skill endeavour
function goCommandLMEndeavour(g, type, userID, locale) {

	let interfaceElements = [
		{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-' + type + '-show', 2, '!' + type + '-show', '', GUI_VERSION),
				goCommandButton('lm-' + type + '-pass', 2, '!' + type + '-pass', '', GUI_FORWARD),
				goCommandButton('lm-' + type + '-fail', 2, '!' + type + '-fail', '', GUI_DEFENSIVE),
				goCommandButton('lm-' + type + '-undo', 2, '!' + type + '-undo', '', GUI_REARWARD),
				goCommandButton('lm-' + type + '-end', 2, '!' + type + '-end', '', GUI_ABSENT)
			]
		},{
			'rowtype':'pulldown',
			'id':'gui-val-' + type,
			'contents':[
				goCommandOption('1', '1', '', BUTTON_ONE),
				goCommandOption('2', '2', '', BUTTON_TWO),
				goCommandOption('3', '3', '', BUTTON_THREE),
				goCommandOption('4', '4', '', BUTTON_FOUR),
				goCommandOption('5', '5', '', BUTTON_FIVE),
				goCommandOption('6', '6', '', BUTTON_SIX),
				goCommandOption('7', '7', '', BUTTON_SEVEN),
				goCommandOption('8', '8', '', BUTTON_EIGHT),
				goCommandOption('9', '9', '', BUTTON_NINE),
				goCommandOption('10', '10', '', BUTTON_TEN)
				]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-' + type + '-time', 2, '!' + type + '-time', '', GUI_TIME),
				goCommandButton('lm-' + type + '-timelimit', 2, '!' + type + '-timelimit', '', GUI_TIMELIMIT),
				goCommandButton('lm-' + type + '-score', 2, '!' + type + '-score', '', GUI_SCORE),
				goCommandButton('lm-' + type + '-resistance', 2, '!' + type + '-resistance', '', GUI_RESISTANCE)
			]
		}
	];
	setUserConfig(userID, 'gui-val-' + type, '1');
	let r;
	if (type == 'council') {
		r = councilShow(g, locale, false);
	} else {
		r = endeavourShow(g, locale, false);
	}

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r, null];
}

// interaction elements for an LM that offers a menu of other panels
function goCommandLMMenu(g, userID, locale) {

	let interfaceElements = [
		{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-panel-endeavour', 2, 'gui-lmpanel-endeavour', '', GUI_CRAFT),
				goCommandButton('lm-panel-council', 2, 'gui-lmpanel-council', '', GUI_AWE),
				goCommandButton('lm-panel-journey', 2, 'gui-lmpanel-journey', '', GUI_TRAVEL),
				goCommandButton('lm-panel-combat', 2, 'gui-lmpanel-combat', '', GUI_BATTLE)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-panel-tests', 2, 'gui-lmpanel-tests', '', GUI_SHADOW),
				goCommandButton('lm-panel-modifiers', 2, 'gui-lmpanel-modifiers', '', GUI_GAIN1),
				goCommandButton('lm-panel-environment', 2, 'gui-lmpanel-environment', '', GUI_TORNADO),
				goCommandButton('lm-panel-eye', 2, 'gui-lmpanel-eye', '', GUI_EYE),
				goCommandButton('lm-panel-company', 2, 'gui-lmpanel-company', '', GUI_COMPANY)
			]
		}
	];

	if (!g.combat.active) delete interfaceElements[1].contents[1];
	if (g.combat.active) {
		delete interfaceElements[0].contents[3];
		interfaceElements.push({
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-light-neither', 2, '!start-light-neither', '', GUI_NOLIGHT),
				goCommandButton('lm-light-sunlight', 2, '!start-light-sunlight', '', GUI_SUMMER),
				goCommandButton('lm-light-darkness', 2, '!start-light-darkness', '', GUI_DARKNESS)
			]
		});
	}

	let title = getGameName(g), text = gameEdition(g) + ' ' + gameConfigShow(g, ', ', locale);
	if (g.combat.active) {
		title = g.combat.title || g.encounter;
		text = showCombatPhaseOrAll(g, g.combat.phase, locale, true, true);
	}
	return [interfaceElements, embeds.BUTTON_GREYPLE, title, text, null];
}

// interaction elements for an LM that offers options to start an endeavour
function goCommandLMMenuEndeavour(g, userID, locale) {

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-lm-endeavour-timelimit',
			'contents':[
				goCommandOption('3', 'gui-timelimit-3', '', BUTTON_THREE),
				goCommandOption('4', 'gui-timelimit-4', '', BUTTON_FOUR),
				goCommandOption('5', 'gui-timelimit-5', '', BUTTON_FIVE),
				goCommandOption('6', 'gui-timelimit-6', '', BUTTON_SIX),
				goCommandOption('7', 'gui-timelimit-7', '', BUTTON_SEVEN)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-startendeavour-simple', 2, '!endeavor-start-task-simple', '(3)', GUI_EASY),
				goCommandButton('lm-startendeavour-laborious', 2, '!endeavor-start-task-laborious', '(6)', GUI_MEDIUM),
				goCommandButton('lm-startendeavour-daunting', 2, '!endeavor-start-task-daunting', '(9)', GUI_HARD)
			]
		}
	];
	setUserConfig(userID, 'gui-lm-endeavour-timelimit','3');

	return [interfaceElements, embeds.BUTTON_GREYPLE, slashcommands.localizedString(locale, 'gui-lmpanel-endeavour',[]), slashcommands.localizedString(locale, 'gui-lmpanel-endeavour-text',[]), null];
}

// interaction elements for an LM that offers options to start a council
function goCommandLMMenuCouncil(g, userID, locale) {

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-lm-council-attitude',
			'contents':[
				goCommandOption('open', '!council-start-attitude-open', '', GUI_NEUTRAL),
				goCommandOption('reluctant', '!council-start-attitude-reluctant', '(-1d)', GUI_UNFRIENDLY),
				goCommandOption('friendly', '!council-start-attitude-friendly', '(+1d)', GUI_PLAIN)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-startcouncil-reasonable', 2, '!council-start-request-reasonable', '(3)', GUI_EASY),
				goCommandButton('lm-startcouncil-bold', 2, '!council-start-request-bold', '(6)', GUI_MEDIUM),
				goCommandButton('lm-startcouncil-outrageous', 2, '!council-start-request-outrageous', '(9)', GUI_HARD)
			]
		}
	];
	setUserConfig(userID, 'gui-lm-council-attitude','open');

	return [interfaceElements, embeds.BUTTON_GREYPLE, slashcommands.localizedString(locale, 'gui-lmpanel-council',[]), slashcommands.localizedString(locale, 'gui-lmpanel-council-text',[]), null];
}

// interaction elements for an LM that offers options to start a journey
function goCommandLMMenuJourney(g, userID, locale) {

	let journeyMenu = [], defaultJourney = null;
	g.journey.journeys.forEach(j => {
		if (defaultJourney == null) defaultJourney = j;
		journeyMenu.push(goCommandOption(j, '=' + journeys.journeys_fullname(j), '=' + j, GUI_TRAVEL));
	});
	if (defaultJourney == null) return [null, null, null, slashcommands.localizedString(locale, 'journey-noneingame',[])];
	setUserConfig(userID, 'gui-lm-journey', defaultJourney);

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-lm-journey',
			'contents':[...journeyMenu]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-startjourney-summer', 2, '!journey-start-season-summer', '', GUI_SUMMER),
				goCommandButton('lm-startjourney-winter', 2, '!journey-start-season-winter', '', GUI_WINTER)
			]
		}
	];

	return [interfaceElements, embeds.BUTTON_GREYPLE, slashcommands.localizedString(locale, 'gui-lmpanel-journey',[]), slashcommands.localizedString(locale, 'gui-lmpanel-journey-text',[]), null];
}

// interaction elements for an LM that offers options to start a combat
function goCommandLMMenuCombat(g, userID, locale) {

	if (g.combat.active) return [null, null, null, slashcommands.localizedString(locale, 'error-alreadycombat',[])];
	let encounterMenu = [], defaultEncounter = null;
	for (let encounterName in g.encounters) {
		if (defaultEncounter == null) defaultEncounter = encounterName;
		encounterMenu.push(goCommandOption(encounterName, '=' + g.encounters[encounterName].title, '=' + encounterName, GUI_BATTLE));
	}
	if (defaultEncounter == null) return [null, null, null, slashcommands.localizedString(locale, 'encounter-none',[])];
	setUserConfig(userID, 'gui-lm-encounter', defaultEncounter);

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-lm-encounter',
			'contents':[...encounterMenu]
		},{
			'rowtype':'pulldown',
			'id':'gui-lm-lightlevel',
			'contents':[
				goCommandOption('neither', '!start-light-neither', '', GUI_NOLIGHT),
				goCommandOption('sunlight', '!start-light-sunlight', '', GUI_SUMMER),
				goCommandOption('darkness', '!start-light-darkness', '', GUI_DARKNESS)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-startcombat-neither', 2, '!start-ambush-neither', '', GUI_NEUTRAL),
				goCommandButton('lm-startcombat-players', 2, '!start-ambush-players', '', GUI_SURPRISE),
				goCommandButton('lm-startcombat-adversaries', 2, '!start-ambush-adversaries', '', GUI_SURPRISE)
			]
		}
	];
	setUserConfig(userID, 'gui-lm-lightlevel', 'neither');

	return [interfaceElements, embeds.BUTTON_GREYPLE, slashcommands.localizedString(locale, 'gui-lmpanel-combat',[]), slashcommands.localizedString(locale, 'gui-lmpanel-combat-text',[]), null];
}

// interaction elements for an LM that offers options to work with pending tests
function goCommandLMMenuTests(g, userID, locale) {

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-lm-testtarget',
			'contents':[]
		},{
			'rowtype':'pulldown',
			'id':'gui-lm-testcondition',
			'contents':[
				goCommandOption('none', 'none', '', SPEND_REDX),
				goCommandOption('daunted', '?test-add-condition-daunted', '', GUI_DAUNTED),
				goCommandOption('misfortune', '?test-add-condition-misfortune', '', GUI_MISFORTUNE),
				goCommandOption('dismayed', '?test-add-condition-dismayed', '', GUI_DISMAYED),
				goCommandOption('haunted', '?test-add-condition-haunted', '', GUI_HAUNTED),
				goCommandOption('dreaming', '?test-add-condition-dreaming', '', GUI_DREAMING),
				goCommandOption('seized', '?test-add-condition-seized', '', GUI_SEIZED),
				goCommandOption('bitten', '?test-add-condition-bitten', '', GUI_BITTEN),
				goCommandOption('drained', '?test-add-condition-drained', '', GUI_BLEEDING),
				goCommandOption('shieldsmashed', '?test-add-condition-shieldsmashed', '', SPEND_SHIELD),
				goCommandOption('knockback', '?test-add-condition-knockback', '', GUI_KNOCKBACK)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-test-dread-1', 2, '!test-add-type-dread', '(1)', BUTTON_ONE),
				goCommandButton('lm-test-dread-2', 2, '!test-add-type-dread', '(2)', BUTTON_TWO),
				goCommandButton('lm-test-dread-3', 2, '!test-add-type-dread', '(3)', BUTTON_THREE),
				goCommandButton('lm-test-dread-4', 2, '!test-add-type-dread', '(4)', BUTTON_FOUR),
				goCommandButton('lm-test-dread-5', 2, '!test-add-type-dread', '(5)', BUTTON_FIVE)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-test-sorcery-1', 2, '!test-add-type-sorcery', '(1)', BUTTON_ONE),
				goCommandButton('lm-test-sorcery-2', 2, '!test-add-type-sorcery', '(2)', BUTTON_TWO),
				goCommandButton('lm-test-sorcery-3', 2, '!test-add-type-sorcery', '(3)', BUTTON_THREE),
				goCommandButton('lm-test-sorcery-4', 2, '!test-add-type-sorcery', '(4)', BUTTON_FOUR),
				goCommandButton('lm-test-sorcery-5', 2, '!test-add-type-sorcery', '(5)', BUTTON_FIVE)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-test-greed-1', 2, '!test-add-type-greed', '(1)', BUTTON_ONE),
				goCommandButton('lm-test-greed-2', 2, '!test-add-type-greed', '(2)', BUTTON_TWO),
				goCommandButton('lm-test-greed-3', 2, '!test-add-type-greed', '(3)', BUTTON_THREE),
				goCommandButton('lm-test-greed-4', 2, '!test-add-type-greed', '(4)', BUTTON_FOUR),
				goCommandButton('lm-test-greed-5', 2, '!test-add-type-greed', '(5)', BUTTON_FIVE)
			]
		}
	];
	setUserConfig(userID, 'gui-lm-testcondition','none');

	//  fill up with the whole company and each member
	interfaceElements[0].contents.push(goCommandOption('company', 'test-wholecompany', '', GUI_COMPANY));
	for (let p in g.characters) {
		let hero = characterDataFetchHandleNulls(g, p, 'name', null);
		interfaceElements[0].contents.push(goCommandOption(hero, '=' + hero, '', GUI_PERSON));
	}
	setUserConfig(userID, 'gui-lm-testtarget','company');

	let r = [];
	for (let p in g.characters) {
		g.characters[p].shadowTests.forEach(test => {
			let tn = test.tn;
			if (tn == null || tn == 0) {
				let attribute = 'wisdom';
				if (test.type == 'dread') attribute = 'valour';
				tn = charSkillTN(g, p, attribute);
			}
			r.push(characterDataFetchHandleNulls(g, p, 'name', null) + ': ' + test.type + ' ' + test.points + ' TN' + tn + (test.condition ? ' (' + test.condition + ')' : ''));
		});
	}
	if (r.length == 0) r.push(slashcommands.localizedString(locale, 'test-none', [slashcommands.localizedString(locale, 'test-wholecompany', [])]));

	return [interfaceElements, embeds.BUTTON_GREYPLE, getGameName(g), r.join('\n'), null];
}

// interaction elements for an LM that offers options to work with combat modifiers
function goCommandLMMenuModifiers(g, userID, locale) {

	if (!g.combat.active) return [null, null, null, slashcommands.localizedString(locale, 'error-nocombat',[])];

	let interfaceElements = [
		{ // pulldown: all characters, all adversaries, or a character
			'rowtype':'pulldown', 
			'id':'gui-lm-modtarget',
			'contents':[]
		},{ // pulldown: duration: forever, or numbers of rounds
			'rowtype':'pulldown', 
			'id':'gui-lm-modduration',
			'contents':[
				goCommandOption('0', 'gui-duration-wholecombat', '', GUI_TIME),
				goCommandOption('1', '1', '', BUTTON_ONE),
				goCommandOption('2', '2', '', BUTTON_TWO),
				goCommandOption('3', '3', '', BUTTON_THREE),
				goCommandOption('4', '4', '', BUTTON_FOUR),
				goCommandOption('5', '5', '', BUTTON_FIVE)
			]
		},{ // buttons for various plus/minus, Clear
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-modifier--2', 2, 'initiative-complication', '-2d', GUI_LOSE2),
				goCommandButton('lm-modifier--1', 2, 'initiative-complication', '-1d', GUI_LOSE1),
				goCommandButton('lm-modifier-1', 2, 'initiative-advantage', '+1d', GUI_GAIN1),
				goCommandButton('lm-modifier-2', 2, 'initiative-advantage', '+2d', GUI_GAIN2),
				goCommandButton('lm-modifier-clear', 2, '!modifier-clear', '', SPEND_REDX)
			]
		}
	];
	setUserConfig(userID, 'gui-lm-modduration','0');

	//  fill up with the whole company and each member
	interfaceElements[0].contents.push(goCommandOption('company', 'test-wholecompany', '', GUI_COMPANY));
	interfaceElements[0].contents.push(goCommandOption('adversaries', 'adversary-adversaries', '', GUI_ADVERSARY));
	for (let p in g.characters) {
		let hero = characterDataFetchHandleNulls(g, p, 'name', null);
		interfaceElements[0].contents.push(goCommandOption(hero, '=' + hero, '', GUI_PERSON));
	}
	setUserConfig(userID, 'gui-lm-modtarget','company');

	let r = slashcommands.localizedString(locale, 'combat-modifiers', []) + ':\n' + showCombatMods(g,locale,true);

	return [interfaceElements, embeds.BUTTON_GREYPLE, slashcommands.localizedString(locale, 'gui-lmpanel-modifiers',[]), r, null];
}

// interaction elements for an LM that offers options to work with environmental sources of injury
function goCommandLMMenuEnvironment(g, userID, locale) {

	let interfaceElements = [
		{ // pulldown: all characters, or a character
			'rowtype':'pulldown', 
			'id':'gui-lm-environmenttarget',
			'contents':[]
		},{ // pulldown: type
			'rowtype':'pulldown', 
			'id':'gui-lm-environmenttype',
			'contents':[
				goCommandOption('cold', '!environment-type-cold', '', GUI_WINTER),
				goCommandOption('fire', '!environment-type-fire', '', SPEND_FIRE),
				goCommandOption('fall', '!environment-type-fall', '', GUI_FALLING),
				goCommandOption('suffocation', '!environment-type-suffocation', '', GUI_SUFFOCATION),
				goCommandOption('poison', '!environment-type-poison', '', GUI_POISON)
			]
		},{ // buttons for severities
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-environment-1', 2, 'condition-severity-1', '', GUI_LOSE1),
				goCommandButton('lm-environment-2', 2, 'condition-severity-2', '', GUI_LOSE2),
				goCommandButton('lm-environment-3', 2, 'condition-severity-3', '', GUI_LOSE3)
			]
		}
	];
	setUserConfig(userID, 'gui-lm-environmenttype','cold');

	//  fill up with the whole company and each member
	interfaceElements[0].contents.push(goCommandOption('company', 'test-wholecompany', '', GUI_COMPANY));
	for (let p in g.characters) {
		let hero = characterDataFetchHandleNulls(g, p, 'name', null);
		interfaceElements[0].contents.push(goCommandOption(hero, '=' + hero, '', GUI_PERSON));
	}
	setUserConfig(userID, 'gui-lm-environmenttarget','company');

	return [interfaceElements, embeds.BUTTON_GREYPLE, slashcommands.localizedString(locale, 'gui-lmpanel-environment',[]), slashcommands.localizedString(locale, 'gui-lmpanel-environment-text',[]), null];
}

// interaction elements for an LM that offers options to work with eye awareness
function goCommandLMMenuEye(g, userID, locale) {

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-eye-value',
			'contents':[
				goCommandOption('0', '0', '', BUTTON_ZERO),
				goCommandOption('1', '1', '', BUTTON_ONE),
				goCommandOption('2', '2', '', BUTTON_TWO),
				goCommandOption('3', '3', '', BUTTON_THREE),
				goCommandOption('4', '4', '', BUTTON_FOUR),
				goCommandOption('5', '5', '', BUTTON_FIVE),
				goCommandOption('6', '6', '', BUTTON_SIX),
				goCommandOption('7', '7', '', BUTTON_SEVEN),
				goCommandOption('8', '8', '', BUTTON_EIGHT),
				goCommandOption('9', '9', '', BUTTON_NINE),
				goCommandOption('10', '10', '', BUTTON_TEN),
				goCommandOption('11', '11', '', BUTTON_NUMBER),
				goCommandOption('12', '12', '', BUTTON_NUMBER),
				goCommandOption('13', '13', '', BUTTON_NUMBER),
				goCommandOption('14', '14', '', BUTTON_NUMBER),
				goCommandOption('15', '15', '', BUTTON_NUMBER),
				goCommandOption('16', '16', '', BUTTON_NUMBER),
				goCommandOption('17', '17', '', BUTTON_NUMBER),
				goCommandOption('18', '18', '', BUTTON_NUMBER),
				goCommandOption('19', '19', '', BUTTON_NUMBER),
				goCommandOption('20', '20', '', BUTTON_NUMBER)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[    
				goCommandButton('lm-eye-base', 2, '!eye-base', '', GUI_LOSE3),
				goCommandButton('lm-eye-set', 2, 'value', '', GUI_FORWARD),
				goCommandButton('lm-eye-threshhold', 2, '!eye-threshhold', '', GUI_GAIN3),
				goCommandButton('lm-eye-reset', 2, '!eye-reset', '', SPEND_REDX)
			]
		}
	];
	setUserConfig(userID, 'gui-eye-value','0');

	return [interfaceElements, embeds.BUTTON_GREYPLE, slashcommands.localizedString(locale, 'gui-lmpanel-eye',[]), eyeShow(g, true), null];
}

// interaction elements for an LM that offers options to do clear and company commands
function goCommandLMMenuCompany(g, userID, locale) {

	let interfaceElements = [
		{
			'rowtype':'pulldown',
			'id':'gui-company-value',
			'contents':[
				goCommandOption('0', '0', '', BUTTON_ZERO),
				goCommandOption('1', '1', '', BUTTON_ONE),
				goCommandOption('2', '2', '', BUTTON_TWO),
				goCommandOption('3', '3', '', BUTTON_THREE),
				goCommandOption('4', '4', '', BUTTON_FOUR),
				goCommandOption('5', '5', '', BUTTON_FIVE),
				goCommandOption('6', '6', '', BUTTON_SIX),
				goCommandOption('7', '7', '', BUTTON_SEVEN),
				goCommandOption('8', '8', '', BUTTON_EIGHT),
				goCommandOption('9', '9', '', BUTTON_NINE),
				goCommandOption('10', '10', '', BUTTON_TEN),
				goCommandOption('11', '11', '', BUTTON_NUMBER),
				goCommandOption('12', '12', '', BUTTON_NUMBER),
				goCommandOption('13', '13', '', BUTTON_NUMBER),
				goCommandOption('14', '14', '', BUTTON_NUMBER),
				goCommandOption('15', '15', '', BUTTON_NUMBER),
				goCommandOption('16', '16', '', BUTTON_NUMBER),
				goCommandOption('17', '17', '', BUTTON_NUMBER),
				goCommandOption('18', '18', '', BUTTON_NUMBER),
				goCommandOption('19', '19', '', BUTTON_NUMBER),
				goCommandOption('20', '20', '', BUTTON_NUMBER)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-company-short', 2, '!company-short', '', GUI_REST),
				goCommandButton('lm-company-prolonged', 2, '!company-prolonged', '', GUI_REST),
				goCommandButton('lm-company-safe', 2, '!company-safe', '', GUI_REST),
				goCommandButton('lm-company-endurance', 2, '!company-endurance', '', GUI_ENDURANCE),
				goCommandButton('lm-company-fatigue', 2, '!company-fatigue', '', GUI_WEARY)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-company-hope', 2, '!company-hope', '', GUI_HOPETOKEN),
				goCommandButton('lm-pool-set', 2, '!pool-set', '', GUI_POOL),
				goCommandButton('lm-pool-max', 2, '!pool-max', '', GUI_POOL),
				goCommandButton('lm-pool-distribute', 2, '!pool-distribute', '', GUI_POOL),
				goCommandButton('lm-pool-reset', 2, '!pool-reset', '', GUI_POOL)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-company-fellowship', 2, '!company-fellowship', '', GUI_COMPANY),
				goCommandButton('lm-company-yule', 2, '!company-yule', '', GUI_YULE),
				goCommandButton('lm-company-ap', 2, '^company-ap', '', GUI_GAIN1),
				goCommandButton('lm-company-sp', 2, '^company-sp', '', GUI_GAIN1),
				goCommandButton('lm-company-treasure', 2, '!company-treasure', '', GUI_TREASURE)
			]
		},{
			'rowtype':'buttons',
			'id':'',
			'contents':[
				goCommandButton('lm-clear-protection', 2, 'test-prottests', '', SPEND_SHIELD),
				goCommandButton('lm-clear-shadow', 2, 'test-shadowtests', '', GUI_SHADOW),
				goCommandButton('lm-clear-damage', 2, '!clear-damage', '', GUI_HURT),
				goCommandButton('lm-clear-fatigue', 2, '!clear-fatigue', '', GUI_WEARY),
				goCommandButton('lm-clear-conditions', 2, '!clear-conditions', '', GUI_DAUNTED)
			]
		}
	];
	setUserConfig(userID, 'gui-company-value','0');

	return [interfaceElements, embeds.BUTTON_GREYPLE, slashcommands.localizedString(locale, 'gui-lmpanel-company',[]), slashcommands.localizedString(locale, 'gui-lmpanel-company-text',[]), null];
}

// BOT STARTUP ----------------------------------------------------------------

userConfig = filehandling.loadStateData();
if (userConfig == null) setTimeout(function(){ process.exit(); }, 1000);
autoSelectGames();
lookuptables.tables_load();
hexflower.load();
journeys.journeys_load(charactersInJourneyRole, torRollDice, journey_log, hasTrait, characterDataFetch, characterDataFetchHandleNumbers, tokenCommandExecute, giveWound, userMention, advanceWounds, advancePoison, getGameName, setUserConfig, increaseEyeRating, enduranceLossDamage);
adversaries.adversaries_load();
reports.reportsInit(characterDataFetchHandleNulls, characterDataFetchHandleNumbers, showStackedStatusLine, getUsefulItemFromExternalCache, getBlessingFromExternalCache, skillValue, skillFavored, charSkillTN, journeyRoleSkills);
initiative.initiativeInit(characterDataFetchHandleNulls, characterDataFetchHandleNumbers, tokenCommandExecute, combatModActive, userMention, charEffectiveStance, botSendLMPrivateMessage, hasTrait, getGameName, enduranceLossDamage);

// SERVER JOIN AND LEAVE NOTIFICATIONS ----------------------------------------

// notify me when the bot joins a server
bot.on('guildCreate', function (guild) {
    botPresence();
});

// notify me when the bot leaves a server
bot.on('guildDelete', function (guild) {
	deleteGamesFromServer(guild.id, guild.name);
    botPresence();
});

function deleteGamesFromServer(guildID, guildName) {
	let found = false;
	for (let g in gameData) {
		if (gameData[g].server == guildID) {
			notifyHostUser('Automatically deleting game **' + g + '** hosted on server ' + guildName, 0, '');
			delete gameData[g];
			found = true;
			replaceSelectedGame(g, null);
		}
	}
	if (found) {
		filehandling.saveGameData(gameData);
		filehandling.saveStateData(userConfig);
	}
}

// SLASH COMMANDS -------------------------------------------------------------

// take a single message or an array of them, return a single message or array of them, but with nothing over 2000 characters
function splitLargeMessage(message) {
	if (message == undefined || message == null) return null;

	let source, dest = [];

	if (Array.isArray(message))
		source = [...message];
	else
		source = [message];

	let breakpoint;
	source.forEach(element => {
		while (element && element.length > DISCORD_MAX_LENGTH) {

			// find an ideal place to break the element, near the maximum length
			breakpoint = DISCORD_MAX_LENGTH;

			// try to break by finding the previous \n
			while (breakpoint > 0 && element.charAt(breakpoint) != '\n') breakpoint--;

			// if we can't find one, try to find a space instead
			if (breakpoint == 0) {
				breakpoint = DISCORD_MAX_LENGTH;
				while (breakpoint > 0 && element.charAt(breakpoint) != ' ') breakpoint--;
			}

			// if still no joy, just do an arbitrary break
			if (breakpoint == 0) breakpoint = DISCORD_MAX_LENGTH - 1;

			dest.push(element.substring(0,breakpoint));
			element = element.substring(breakpoint);
		}
		dest.push(element);
	});

	if (dest.length == 1) return dest[0];
	return dest;
}

// if an array, show all but the last and then answer the last; otherwise just answer the last
// but if we've already had this one reacted, just show all of them
function handleInteractionReturn(interaction, channelID, result, ephemeral) {
	if (result == '') {
		//return interaction.acknowledge();
		return;
	}
	// split any items more than 2000 into multiple items
	result = splitLargeMessage(result);

	try {
		// if the result is an array, either because it came that way or because splitLargeMessage() divided it up:
		if (Array.isArray(result)) {
			// strip any leading blank items since they won't work as the return
			while (result[0] == '') result.shift();
			interaction.createMessage(result.shift()) // ephemeral can be ignored here
			.then(response => {
				result.forEach(r => {
					if (typeof result == 'object' && ephemeral) r.flags = 64;
					interaction.createFollowup(r);
				});
			});
			return null;
		} else {
			// if there's just one thing, just return it
			if (interaction.acknowledged) return botSendMessage(channelID, result);
			if (ephemeral) {
				if (typeof result == 'object') {
					result.flags = 64;
					return interaction.createMessage(result);
				}
				else return interaction.createMessage({content: result, flags: 64});
			} else return interaction.createMessage(result);
		}
	}
	catch (err) {
		notifyHostUser('Caught error in handleInteractionReturn: _' + err + '_\n in cmd **/' + interaction.data.name + '** with data ' + JSON.stringify(interaction.data) + '\n' + err.stack);
		recordEvilMidnightBombers(userID);
		botSendMessage(channelID, result, bombMessage);
	}
}

bot.on('interactionCreate', (interaction) => {

	let userID = (interaction.member ? interaction.member.id : (interaction.user ? interaction.user.id : null));
	let channelID = (interaction.channel ? interaction.channel.id : null);

	if (isEvilMidnightBomber(userID)) return interaction.createMessage(bombCooldown);

    if (interaction instanceof Eris.ComponentInteraction) {
		return executeComponentButton(interaction, userID);
	}

    if (interaction instanceof Eris.CommandInteraction) {

		// extract the relevant information about the message and its origin
		let userID = (interaction.member ? interaction.member.id : (interaction.user ? interaction.user.id : null));
		let channelID = (interaction.channel ? interaction.channel.id : null);

		try {
			// cache this user's language
			if (interaction && interaction.locale) {
				setUserConfig(userID, 'locale', interaction.locale);
				filehandling.saveStateData(userConfig);
			}

			let r = null;

			// handle autoswitch if necessary
			if (getUserConfig(userID,'autoSwitch') && interaction.data.name != 'game') {
				let serverGames = findGamesOnServer(interaction.guildID, userID);
				if (serverGames.length == 1 && getUserConfig(userID,'selectedGame') != serverGames[0] && isPlayerInGame(userID, gameData[serverGames[0]])) {
					// auto switch to that game
					r = gameCommandSelect(serverGames[0], userID, interaction.locale);
					//botSendLargeMessage(channelID, gameCommandSelect(serverGames[0], userID, interaction.locale));
					filehandling.saveStateData(userConfig);
				}
			}

			return executeSlashCommand(interaction, false, r);
		}
		catch (err) {
			notifyHostUser('Caught error in interactionCreate: ' + err + '\n in cmd **/' + cmd + '** with data ' + JSON.stringify(interaction.data) + '\n' + err.stack);
			recordEvilMidnightBombers(userID);
			return interaction.createMessage(bombMessage);
		}
    }

});

// separated out so macros can run them
function executeSlashCommand(interaction, stringResponse, preceder) {

	let response = null;
	let userRealName = (interaction.member ? interaction.member.username : 'You');
	let userID = (interaction.member ? interaction.member.id : (interaction.user ? interaction.user.id : null));
	let ephemeral = false;

	try {
		let userName = (interaction.member ? (interaction.member.nick ? interaction.member.nick : interaction.member.username) : 'You');
		let channelID = (interaction.channel ? interaction.channel.id : null);
		let guildID = interaction.guildID;
		let cmd = interaction.data.name;
		let data = interaction.data;

		// if this is the /last command, replace it with the last saved interaction
		if (cmd == 'last') {
			let lastCmd = getUserConfig(userID,'lastSlashCmd');
			if (!getUserConfig(userID,'saveLast') || lastCmd == null) {
				return interaction.createMessage(slashcommands.localizedString(interaction.locale, 'error-nolastcmd', []));
			} else {
				data = lastCmd;
				cmd = data.name;
			}
		} else {
			// save this interaction for use with the /last command
			if (getUserConfig(userID,'saveLast') && cmd != 'mellon' && cmd != 'macro') {
				setUserConfig(userID,'lastSlashCmd', interaction.data);
				filehandling.saveStateData(userConfig);
			}
		}

		if (userRealName != 'You') {
			let g = mySelectedGame(userID);
			if (g && isPlayerLoremasterOfGame(userID, g) && g.lmName != userRealName) {
				g.lmName = stringutil.sanitizeString(userRealName);
				filehandling.saveGameData(gameData);
			}
		}

		lastUser = userRealName;
		lastCommand = slashCmdDisplayable(interaction.data);

		filehandling.logUsage('/' + interaction.data.name + '\t' + (interaction.member ? interaction.member.username : 'unknown') + '\t' + JSON.stringify(interaction.data) + '\n');

		// handle the interaction
		switch (cmd) {
			case 'go':
				response = goCommand(interaction, userID, channelID, guildID, interaction.locale, data, userRealName);
				ephemeral = true;
				break;
			case 'dice':
				response = diceCommand(userID, userName, interaction.locale, data, false);
				break;
			case 'gurps':
				response = diceCommand(userID, userName, interaction.locale, data, true);
				break;
			case 'expanse':
				response = expanseCommand(userID, userName, interaction.locale, data);
				break;
			case 'coyote':
				response = coyoteCommand(userID, userName, interaction.locale, data);
				break;
			case 'cortex':
				response = cortexCommand(userID, userName, interaction.locale, data);
				break;
			case 'raven':
				response = ravenCommand(userID, userName, interaction.locale, data);
				break;
			case 'tor':
			case 'feat':
			case 'success':
				response = torCommand(cmd, userID, userName, interaction.locale, data);
				break;
			case 'roll':
			case 'skill':
				response = rollCommand(cmd, userID, channelID, userName, interaction.locale, data, true);
				break;
			case 'treat':
				response = treatCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'attack':
				response = attackCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'foe':
				response = foeCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'explain':
				response = explainCommand(userID, interaction.locale);
				break;
			case 'status':
				response = statusCommand(userID, channelID, interaction.locale, data);
				break;
			case 'sheet':
				response = sheetCommand(userID, interaction.locale, data);
				break;
			case 'scribbles':
				response = scribblesCommand(userID, interaction.locale, data);
				break;
			case 'set':
			case 'gain':
			case 'lose':
			case 'restore':
			case 'damage':
			case 'heal':
				response = tokenCommand(cmd, userID, channelID, interaction.locale, data);
				break;
			case 'wound':
				response = woundCommand(userID, channelID, interaction.locale, data);
				break;
			case 'eye':
				response = eyeCommand(userID, interaction.locale, data);
				break;
			case 'clear':
				response = clearCommand(userID, channelID, interaction.locale, data);
				break;
			case 'company':
				response = companyCommand(userID, channelID, interaction.locale, data);
				break;
			case 'environment':
				response = environmentCommand(userID, channelID, interaction.locale, data);
				break;
			case 'report':
				response = reportCommand(userID, channelID, interaction.locale, data);
				break;
			case 'counter':
				response = counterCommand(userID, interaction.locale, data);
				break;
			case 'game':
				response = gameCommand(interaction, userID, channelID, guildID, interaction.locale, data, userRealName);
				break;
			case 'character':
				response = characterCommand(interaction, userID, channelID, guildID, interaction.locale, data, userRealName);
				break;
			case 'actions':
			case 'intimidate':
			case 'rally':
			case 'protect':
			case 'prepare':
			case 'gainground':
				response = actionCommand(cmd, userID, channelID, userName, interaction.locale, data);
				break;
			case 'test':
				response = testCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'stance':
				response = stanceCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'modifier':
				response = modifierCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'spend':
				response = spendCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'start':
				response = startCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'end':
				response = endCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'next':
				response = nextCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'current':
				response = currentCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'council':
				response = councilCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'endeavour':
				response = endeavourCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'role':
				response = roleCommand(userID, channelID, userName, interaction.locale, data);
				break;
			case 'jedit':
				response = jeditCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'journey':
				response = journeyCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'pool':
				response = poolCommand(userID, channelID, interaction.locale, data);
				break;
			case 'weather':
				response = weatherCommand(userID, interaction.locale, data);
				break;
			case 'songbook':
				response = songbookCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'table':
				response = tableCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'hexflower':
				response = hexflowerCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'aedit':
				response = aeditCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'encounter':
				response = encounterCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'nameless':
				response = namelessCommand(userID, channelID, guildID, interaction.locale, data);
				break;
			case 'mellon':
				response = mellonCommand(userID, channelID, guildID, interaction.locale, data);
				ephemeral = true;
				break;
			case 'autoswitch':
				response = autoswitchCommand(userID, interaction.locale, data);
				break;
			case 'savelast':
				response = saveLastCommand(userID, interaction.locale, data);
				break;
			case 'macro':
				response = macroCommand(interaction, userID, interaction.locale, data);
				break;
			case 'version':
				response = versionString + '\n' + newsString + '\n' + slashcommands.localizedString(interaction.locale, 'translator', []);
				break;
			case 'suggest':
				notifyHostUser('Suggestion from **' + bot.users.get(userID).username + '**: ' + data.options[0].value, 0, '');
				lastSuggestor = userID;
				response = slashcommands.localizedString(interaction.locale, 'suggested', []);
				break;
			case 'help':
				response = {embed: embeds.helpEmbed(version)};
				break;
			default:
				console.log('unknown interaction recieved: ' + interaction.data.name + ' locale=' + interaction.locale + ' id=' + interaction.id + ' data=' + JSON.stringify(interaction.data) + ' member=' + JSON.stringify(interaction.member) + ' appID=' + interaction.applicationID + ' token=' + interaction.token + ' version=' + interaction.version + ' type=' + interaction.type + ' appPermissions=' + interaction.appPermissions);
				response = {embed: embeds.helpEmbed(version)};
				break;
			}

		if (stringResponse) return response;
		if (preceder) {
			if (Array.isArray(response)) response.unshift(preceder);
			else response = [preceder, response];
		}
		return handleInteractionReturn(interaction, channelID, response, ephemeral);
	}
	catch (err) {
		notifyHostUser('Caught error in executeSlashCommand: _' + err + '_\n in cmd **/' + interaction.data.name + '** by `' + (interaction.member ? interaction.member.username : 'unknown') + '` with data ' + JSON.stringify(interaction.data) + '\n' + err.stack);
		response = bombMessage;
		recordEvilMidnightBombers(userID);
		if (stringResponse) return response;
		return handleInteractionReturn(interaction, (interaction.channel ? interaction.channel.id : null), response, ephemeral);
	}
}

// someone has clicked on a button
function executeComponentButton(interaction, userID) {
	let buttonId = 'unknown';
	try {
		if (interaction.message == null || interaction.message.embeds == null || interaction.message.embeds[0] == null) return interaction.acknowledge();
		let idInfo = interaction.data.custom_id.split(' ');
		buttonId = idInfo[0];
		let g;
		if (idInfo[1]) g = gameData[idInfo[1]];
		else g = mySelectedGame(userID);
		//if (g == null) return interaction.acknowledge();
		let embedUserID = null;
		if (idInfo[2] != 'all') embedUserID = idInfo[2].split('|');
		let myCharID = currentCharacter(userID);

		// if this is a menu handle that
		if (g && interaction.data && interaction.data.component_type == 3) {
			// a /go pulldown menu selection
			return handleInteractionReturn(interaction, (interaction.channel ? interaction.channel.id : null, false), pulldownMenuClick(interaction, userID, g, interaction.message.embeds[0].author.name, buttonId, getUserConfig(userID, 'locale'), (interaction.channel ? interaction.channel.id : null), interaction.guildID, embedUserID), true);
		}

		// figure out which kind of button this is
		if (g && interaction.message.embeds[0].color == embeds.BUTTON_RED) {
			// adversary embed: for adversaries spending tengwar
			if (!isPlayerLoremasterOfGame(userID, g)) return interaction.acknowledge();
			return interaction.createMessage(adversaryButtonClick(g, interaction.message.embeds[0].author.name, buttonId, getUserConfig(userID, 'locale'), (interaction.channel ? interaction.channel.id : null)));
		}
		if (g && interaction.message.embeds[0].color == embeds.BUTTON_BLURPLE) {
			// LM command embed: for things like 'you can do /next now' (done by loremasters)
			if (!isPlayerLoremasterOfGame(userID, g)) return interaction.acknowledge();
			return handleInteractionReturn(interaction, (interaction.channel ? interaction.channel.id : null), blurpleButtonClick(userID, g, buttonId, getUserConfig(userID, 'locale'), (interaction.channel ? interaction.channel.id : null)), false);
		}
		if (interaction.message.embeds[0].color == embeds.BUTTON_GREYPLE) {
			// answer embed: for things like 'what handedness did you mean?', confirmations, player buttons not related to tengwar, /go
			if (g && !isPlayerLoremasterOfGame(userID, g) && embedUserID && !embedUserID.includes(userID)) {
				console.log('Failed Greyple click, userID=' + userID + ', charID=' + myCharID + ', allowed=[' + embedUserID.join(',') + ']');
				return interaction.acknowledge();
			}
			let [ephemeral, r] = greypleButtonClick(interaction, userID, g, interaction.message.embeds[0].author.name, buttonId, getUserConfig(userID, 'locale'), (interaction.channel ? interaction.channel.id : null), interaction.guildID, embedUserID)
			return handleInteractionReturn(interaction, (interaction.channel ? interaction.channel.id : null), r, ephemeral);
		}

		// must be a player tengwar button; verify this is a valid click by the right player or the loremaster
		if (!g || !embedUserID.includes(userID) && !isPlayerLoremasterOfGame(userID, g)) return interaction.acknowledge();

		// process the spend
		return interaction.createMessage(spendOneTengwar(g, findCharacterInGame(g, interaction.message.embeds[0].author.name), buttonId, getUserConfig(userID, 'locale')));
	}
	catch (err) {
		notifyHostUser('Caught error in executeComponentButton: ' + err + '\n in button **' + buttonId + '** with data ' + JSON.stringify(interaction.data) + '\n' + err.stack);
		recordEvilMidnightBombers(userID);
		return interaction.createMessage(bombMessage);
	}
}

// blurple buttons are Loremaster-only functions
function blurpleButtonClick(userID, g, choice, locale, channelID) {
	// init-advance and init-end
	if (choice == 'init-advance' || choice == 'init-end') {
		if (!g || !g.combat.active) return slashcommands.localizedString(locale, 'error-nocombat', []);
		if (choice == 'init-advance') return nextCommand(userID, channelID, '', locale, {'options':[{'name':'confirm', 'type': 5, 'value':true}]});
		if (choice == 'init-end') return endCommand(userID, channelID, '', locale, {'options':[{'name':'confirm', 'type': 5, 'value':true}]});
	}
}

// greyple buttons are choices that anyone can make
function greypleButtonClick(interaction, userID, g, charName, choice, locale, channelID, guildID, userIDs) {
	// handedness buttons
	if (choice == 'button-1h' || choice == 'button-2h') {
		// recover the appropriate userConfig
		let data = getUserConfig(userID, choice);
		if (data == null) return [false, slashcommands.localizedString(locale, 'error-noaction', [])];		// delete the userConfigs
		delete userConfig[userID]['button-1h'];
		delete userConfig[userID]['button-2h'];
		filehandling.saveStateData(userConfig);
		// rerun the attackCommand
		return [false, attackCommand(userID, channelID, charName, locale, data)];
	}
	if (choice.startsWith('button-journey') || choice.startsWith('button-arrival')) {
		// recover the appropriate userConfig (this one is stored on the LM since it could be any player)
		let data = getUserConfig(g.loremaster, choice);
		if (data == null) return [false, slashcommands.localizedString(locale, 'error-noaction', [])];
		data.id = userID;
		data.guild_id = guildID;
		// delete the userConfigs -- not for arrival since that is clicked more than once
		if (choice.startsWith('button-journey')) {
			delete userConfig[g.loremaster]['button-journey-plain'];
			delete userConfig[g.loremaster]['button-journey-hope'];
			delete userConfig[g.loremaster]['button-journey-inspired'];
			delete userConfig[g.loremaster]['button-journey-magical'];
			filehandling.saveStateData(userConfig);
		}
		// run the rollCommand
		return [false, rollCommand('roll', userID, channelID, charName, locale, data, false)];
	}
	if (choice.startsWith('confirm-')) {
		// recover the appropriate userConfig
		let data = getUserConfig(userID, choice);
		if (data == null) return [false, slashcommands.localizedString(locale, 'error-noaction', [])];
		delete userConfig[userID][choice];
		filehandling.saveStateData(userConfig);
		// rerun the appropriate command
		let cmd = choice.substr(8);
		if (cmd == 'end') return [false, endCommand(userID, channelID, charName, locale, data)];
		if (cmd == 'next') return [false, nextCommand(userID, channelID, charName, locale, data)];
		if (cmd == 'gameleave') return [false, gameCommandLeave(g, true, userID, channelID, locale, data, '')];
		if (cmd == 'gameend') return [false, gameCommandEnd(g, true, userID, channelID, locale, data, '')];
		if (cmd == 'charremove') return [false, characterCommandRemove(interaction, userID, channelID, null, locale, data, '', g, true)];
		if (cmd == 'tableclear') return [false, tableCommand(userID, channelID, null, locale, data)];
		if (cmd == 'tabledelete') return [false, tableCommand(userID, channelID, null, locale, data)];
		if (cmd == 'tabledeletegroup') return [false, tableCommand(userID, channelID, null, locale, data)];
		if (cmd == 'hexflowerdelete') return [false, hexflowerCommand(userID, channelID, null, locale, data)];
		if (cmd == 'journeyclear') return [false, tableCommand(userID, channelID, null, locale, data)];
		if (cmd == 'journeydelete') return [false, jeditCommand(userID, channelID, null, locale, data)];
		if (cmd == 'encdelete') return [false, encounterCommand(userID, channelID, null, locale, data)];
	}
	if (choice.startsWith('gui-')) { // Interactive Interface panel commands
		let cmd = choice.substr(4);
		if (cmd == 'help') return [false, {embed: embeds.helpEmbed(version)}];
		if (cmd == 'version') return [false, versionString + '\n' + newsString + '\n' + slashcommands.localizedString(interaction.locale, 'translator', [])];
		if (cmd.startsWith('tor') || cmd == 'feat' || cmd == 'success') {
			let dice = 1;
			if (cmd.startsWith('tor')) {
				dice = Number(cmd.substr(3));
				cmd = 'tor';
			}
			return [false, torCommand(cmd, userID, charName, interaction.locale, {'options':[{'name':'dice','value':dice}]})];
		}
		if (cmd.startsWith('dice')) {
			return [false, diceCommand(userID, charName, interaction.locale, {'options':[{'name':'roll','value':'d' + cmd.substr(4)}]}, false)];
		}
		if (cmd == 'cortex') {
			let data = {'options':[]};
			for (let i=1; i<=3; i++) {
				if (getUserConfig(userID,'cortex' + i) != 'none') data.options.push({'name':'die' + i,'value':getUserConfig(userID,'cortex' + i).slice(1)});
			}
			return [false, cortexCommand(userID, charName, locale, data)];
		}
		if (cmd.startsWith('coyote-')) { // roll, finish, up, down
			let coyoteDice = getUserConfig(userID, 'gui-coyote-dice');
			if (coyoteDice == null) coyoteDice = 1;
			let coyoteChange = getUserConfig(userID, 'gui-coyote-change');
			let data = {'options':[{'name':cmd.slice(7),'options':[]}]};
			if (cmd == 'coyote-roll') {
				data.options[0].options.push({'name':'numdice','value':coyoteDice});
				data.options[0].options.push({'name':'sort','value':true});
			}
			if (cmd == 'coyote-up' || cmd == 'coyote-down') {
				data.options[0].name = 'change';
				data.options[0].options.push({'name':'roll','value':coyoteChange});
				data.options[0].options.push({'name':'newvalue','value':cmd.slice(7)});
			}
			return [false, coyoteCommand(userID, charName, locale, data)];
		}
		if (cmd == 'status') return [false, statusCommand(userID, channelID, locale, {'options':[]})];
		if (cmd == 'sheet') return [false, sheetCommand(userID, locale, {'options':[]})];
		if (cmd == 'refresh') return [false, characterCommandRefresh(interaction, userID, channelID, guildID, locale, {'options':[]}, charName, mySelectedGame(userID), null)];
		if (cmd == 'explain') return [false, '**Explanation**:\n' + g.debugString];
		if (cmd == 'gainhope') return [false, tokenCommand('gain', userID, channelID, locale, {'options':[{'name':'token','value':'hope'},{'name':'value','value':1}]})];
		if (cmd == 'losehope') return [false, tokenCommand('lose', userID, channelID, locale, {'options':[{'name':'token','value':'hope'},{'name':'value','value':1}]})];
		if (cmd == 'gainshadow') return [false, tokenCommand('gain', userID, channelID, locale, {'options':[{'name':'token','value':'shadow'},{'name':'value','value':1}]})];
		if (cmd == 'loseshadow') return [false, tokenCommand('lose', userID, channelID, locale, {'options':[{'name':'token','value':'shadow'},{'name':'value','value':1}]})];
		if (cmd.startsWith('skill-')) return [false, rollCommand('roll', userID, channelID, charName, locale, rollData('skill', getUserConfig(userID,'gui-skillpick'), cmd.slice(6), getUserConfig(userID, "gui-bonusdice"),null), false)];
		if (cmd.startsWith('protection-')) return [false, rollCommand('roll', userID, channelID, charName, locale, rollData('protection', '', cmd.slice(11), getUserConfig(userID, "gui-bonusdice"),null), false)];
		if (cmd.startsWith('dread-')) return [false, rollCommand('roll', userID, channelID, charName, locale, rollData('dread', '', cmd.slice(6), null), false)];
		if (cmd.startsWith('sorcery-')) return [false, rollCommand('roll', userID, channelID, charName, locale, rollData('sorcery', '', cmd.slice(8), getUserConfig(userID, "gui-bonusdice"),null), false)];
		if (cmd.startsWith('greed-')) return [false, rollCommand('roll', userID, channelID, charName, locale, rollData('greed', '', cmd.slice(6), getUserConfig(userID, "gui-bonusdice"),null), false)];
		if (cmd.startsWith('stance-')) return [false, stanceCommand(userID, channelID, charName, locale, {'options':[{'name':'stance','value':cmd.slice(7)}]})];
		if (cmd.startsWith('combat-')) { // any action during an opening volley or your action in combat
			let action = getUserConfig(userID, 'gui-combataction');
			let target = getUserConfig(userID, 'gui-combattarget');
			if (action == 'seek-advantage')	return [false, rollCommand('roll', userID, channelID, charName, locale, rollData('skill', 'battle', cmd.slice(7), getUserConfig(userID, "gui-bonusdice"),null), false)];
			else if (action.startsWith('action-protect-')) {
				return [false, actionCommand('protect', userID, channelID, charName, locale, {"options":[{"value":cmd.slice(15),"type":3,"name":"hope"},{"value":getUserConfig(userID, "gui-bonusdice"),"type":4,"name":"bonus"},{"value":action.slice(15),"type":3,"name":"ally"}]})];
			}
			else if (action.startsWith('action-')) {
				return [false, actionCommand(action.slice(7), userID, channelID, charName, locale, {"options":[{"value":cmd.slice(7),"type":3,"name":"hope"},{"value":getUserConfig(userID, "gui-bonusdice"),"type":4,"name":"bonus"}]})];
			}
			else if (action == 'brawling' || action.startsWith('weapon-')) {
				let data = {"options":[{"value":cmd.slice(7),"type":3,"name":"hope"},{"value":getUserConfig(userID, "gui-bonusdice"),"type":4,"name":"bonus"}]};
				if (target.startsWith('parry-')) data.options.push({"value":target.slice(6),"name":"parry"});
				else if (target.startsWith('adv-')) data.options.push({"value":target.slice(4),"name":"target"});
				if (action == 'brawling') data.options.push({"value":'brawling',"name":"weapon"});
				else {
					data.options.push({"value":action.split('-')[2],"name":"weapon"});
					data.options.push({"value":action.split('-')[1],"name":"handedness"});
					console.log('weapon name: ' + action.split('-')[2]);
				}
				return [false, attackCommand(userID, channelID, charName, locale, data)];
			}
		}
		if (cmd.startsWith('damage')) return [false, tokenCommand('damage', userID, channelID, locale, {'options':[{'name':'token','value':'endurance'},{'name':'value','value':Number(cmd.slice(6))}]})];
		if (cmd.startsWith('condition-')) return [false, statusCommand(userID, channelID, locale, {'options':[{'name':'status','value':cmd.slice(10)},{'name':'value','value':1}]})];
		if (cmd.startsWith('wound-')) return [false, woundCommand(userID, channelID, locale, {'options':[{'name':'status','value':cmd.slice(6)}]})];
		if (cmd.startsWith('council-')) return [false, rollCommand('roll', userID, channelID, charName, locale, rollData('skill', getUserConfig(userID,'gui-skillpick'), cmd.slice(6), getUserConfig(userID, "gui-bonusdice"),'council'), false)];
		if (cmd.startsWith('endeavour-')) return [false, rollCommand('roll', userID, channelID, charName, locale, rollData('skill', getUserConfig(userID,'gui-skillpick'), cmd.slice(6), getUserConfig(userID, "gui-bonusdice"),'endeavour'), false)];
		if (cmd.startsWith('treat-')) return [false, treatCommand(userID, channelID, charName, locale, {'options':[{'name':'patient','value':characterDataFetchHandleNulls(g,getUserConfig(userID, 'gui-patient'), 'name', null)}, {"value":cmd.slice(6),"type":3,"name":"hope"},{"value":getUserConfig(userID, "gui-bonusdice"),"type":4,"name":"bonus"}]}, false)];
		if (cmd.startsWith('journey-')) return [false, journeyCommand(userID, channelID, guildID, locale, {'options':[{'name':cmd.slice(8),'options':[]}]})];
		if (cmd.startsWith('adv-test-')) return [false, foeCommand(userID, channelID, charName, locale, {'options':[{'name':'protection','options':[{'name':'hate','value':(cmd.slice(9) == 'hate')},{'name':'adversary','value':getUserConfig(userID,'gui-lmadversary')}]}]})];
		if (cmd.startsWith('lm-council-')) return [false, councilCommand(userID, channelID, guildID, locale, {'options':[{'name':cmd.slice(11),'options':[{'name':'amount','value':getUserConfig(userID, 'gui-val-council')}]}]})];
		if (cmd.startsWith('lm-endeavour-')) return [false, endeavourCommand(userID, channelID, guildID, locale, {'options':[{'name':cmd.slice(13),'options':[{'name':'amount','value':getUserConfig(userID, 'gui-val-endeavour')}]}]})];
		if (cmd.startsWith('lm-panel-')) {
			let interfaceElements = null, buttonColor = embeds.BUTTON_GREYPLE, title = 'Narvi', bodyText = '';
			switch(cmd.slice(9)) {
				case 'endeavour': [interfaceElements, buttonColor, title, bodyText] = goCommandLMMenuEndeavour(g, userID, locale); break;
				case 'council': [interfaceElements, buttonColor, title, bodyText] = goCommandLMMenuCouncil(g, userID, locale); break;
				case 'journey': [interfaceElements, buttonColor, title, bodyText] = goCommandLMMenuJourney(g, userID, locale); break;
				case 'combat': [interfaceElements, buttonColor, title, bodyText] = goCommandLMMenuCombat(g, userID, locale); break;
				case 'tests': [interfaceElements, buttonColor, title, bodyText]= goCommandLMMenuTests(g, userID, locale); break;
				case 'modifiers': [interfaceElements, buttonColor, title, bodyText] = goCommandLMMenuModifiers(g, userID, locale); break;
				case 'environment': [interfaceElements, buttonColor, title, bodyText] = goCommandLMMenuEnvironment(g, userID, locale); break;
				case 'eye': [interfaceElements, buttonColor, title, bodyText] = goCommandLMMenuEye(g, userID, locale); break;
				case 'company': [interfaceElements, buttonColor, title, bodyText] = goCommandLMMenuCompany(g, userID, locale); break;
				console.log('Error: invalid greypleButtonClick cmd ' + cmd);
				return [false, ':bomb: Something went wrong. Please `/suggest` to tell us what just happened.'];
			}
			if (interfaceElements == null) return [false, bodyText];
			return [true, embeds.buildGUI(
				locale, g, getGameName(g), [userID],
				title,
				'https://cdn.discordapp.com/attachments/848011016612478987/881999824503857212/Narvi_avatar.png',
				'https://bitbucket.org/HawthornThistleberry/narvi/',
				bodyText,
				'',
				buttonColor,
				interfaceElements
			)];
		}
		if (cmd.startsWith('lm-light-')) {
			let light = cmd.slice(9);
			if (light == 'neither') light = null;
			g.combat.light = light;
			return [false, '`' + stringutil.padRight(stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'start', 'light')),15,' ') + ':` ' + slashcommands.localizedOption(locale, 'start','light', (g.combat.light == null ? slashcommands.localizedOption(locale, 'start','light','neither') : g.combat.light)) + '\n'];
		}
		if (cmd.startsWith('lm-test-')) {
			data = {'options':[{'name':'add','options':[{'name':'character','value':getUserConfig(userID, 'gui-lm-testtarget')},{'name':'type','value':cmd.split('-')[2]},{'name':'points','value':cmd.split('-')[3]}]}]}
			if (getUserConfig(userID, 'gui-lm-testcondition') != 'none') data.options[0].options.push({'name':'condition','value':getUserConfig(userID, 'gui-lm-testcondition')});
			return [false, testCommand(userID, channelID, guildID, locale, data)];
		}
		if (cmd.startsWith('lm-startendeavour-')) return [false, endeavourCommand(userID, channelID, guildID, locale, {'options':[{'name':'start','options':[{'name':'task','value':cmd.slice(18)},{'name':'timelimit','value':getUserConfig(userID, 'gui-lm-endeavour-timelimit')}]}]})];
		if (cmd.startsWith('lm-startcouncil-')) return [false, councilCommand(userID, channelID, guildID, locale, {'options':[{'name':'start','options':[{'name':'request','value':cmd.slice(16)},{'name':'attitude','value':getUserConfig(userID, 'gui-lm-council-attitude')}]}]})];
		if (cmd.startsWith('lm-startjourney-')) return [false, journeyCommand(userID, channelID, guildID, locale, {'options':[{'name':'start','options':[{'name':'season','value':cmd.slice(16)},{'name':'shortname','value':getUserConfig(userID, 'gui-lm-journey')}]}]})];
		if (cmd.startsWith('lm-startcombat-')) return [false, startCommand(userID, channelID, guildID, locale, {'options':[{'name':'ambush','value':cmd.slice(15)},{'name':'encounter','value':getUserConfig(userID, 'gui-lm-encounter')},{'name':'light','value':getUserConfig(userID, 'gui-lm-lightlevel')}]})];
		if (cmd == 'lm-modifier-clear') return [false, modifierCommand(userID, channelID, charName, locale, {'options':[{'name':'clear','options':[]}]})];
		if (cmd.startsWith('lm-modifier-')) return [false, modifierCommand(userID, channelID, charName, locale, {'options':[{'name':'set','options':[{'name':'amount','value':cmd.slice(12)},{'name':'characters','value':getUserConfig(userID, 'gui-lm-modtarget')},{'name':'duration','value':getUserConfig(userID, 'gui-lm-modduration')}]}]})];
		if (cmd.startsWith('lm-environment-')) return [false, environmentCommand(userID, channelID, locale, {'options':[{'name':'type', 'value':getUserConfig(userID, 'gui-lm-environmenttype')},{'name':'level', 'value':cmd.slice(15)},{'name':'character','value':getUserConfig(userID, 'gui-lm-environmenttarget')}]})];
		if (cmd.startsWith('lm-eye-')) return [false, eyeCommand(userID, locale, {'options':[{'name':cmd.slice(7),'options':[{'name':'value','value':getUserConfig(userID, 'gui-eye-value')}]}]})];
		if (cmd.startsWith('lm-company-')) return [false, companyCommand(userID, channelID, locale, {'options':[{'name':'choice', 'value':cmd.slice(11)},{'name':'value','value':getUserConfig(userID, 'gui-company-value')}]})];
		if (cmd.startsWith('lm-pool-')) return [false, poolCommand(userID, channelID, locale, {'options':[{'name':cmd.slice(8),'options':[{'name':'points','value':getUserConfig(userID, 'gui-company-value')}]}]})];
		if (cmd.startsWith('lm-clear-')) return [false, clearCommand(userID, channelID, locale, {'options':[{'name':'choice', 'value':cmd.slice(9)}]})];
		console.log('Error: invalid greypleButtonClick cmd ' + cmd);
		return [false, ':bomb: Something went wrong. Please `/suggest` to tell us what just happened.'];
	}
}

// assembles the data options for a rollCommand for various kinds of rolls executed by buttons
function rollData(test, skill, hope, bonus, affects) {
	let data = {"options":[{"value":test,"type":3,"name":"test"}]};
	if (skill != '') data.options.push({"value":skill,"type":3,"name":"skill"});
	if (hope != '' && hope != 'plain') data.options.push({"value":hope,"type":3,"name":"hope"});
	if (bonus != '' && bonus != '0') data.options.push({"value":bonus,"type":4,"name":"bonus"});
	if (affects != null) data.options.push({"value":affects,"type":3,"name":"affects"});
	return data;
}

// pulldown menu click
function pulldownMenuClick(interaction, userID, g, charName, choice, locale, channelID, guildID, userIDs) {
	let values = interaction.data.values; // this is an array in case of multiselects

	if (choice == 'gui-skillpick') {
		setUserConfig(userID, 'gui-skillpick', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedskill', [stringutil.titleCaseWords(slashcommands.localizedString(locale, 'skill-' + stringutil.lowerCase(values[0]), []))]);
	}
	let s = '?'; 
	if (choice == 'gui-combataction') {
		setUserConfig(userID, 'gui-combataction', values[0]);
		// make the values passed in as parameters displayable: weapon-1h-name, seek-advantage, brawling, action-task
		if (values[0].startsWith('weapon-')) {
			s = values[0].split('-')[2];
			let wData = characterDataFetch(g, currentCharacter(userID), 'weapons', s);
			if (wData) s = wData[18];
			s += ' (' + values[0].split('-')[1] + ')';
		}
		if (values[0] == 'brawling') s = slashcommands.localizedString(locale, 'proficiency-brawling', []);
		if (values[0] == 'seek-advantage') s = slashcommands.localizedString(locale, 'gui-advantage', []);
		if (values[0].startsWith('action-protect-')) s = slashcommands.localizedString(locale, 'action-protect', []) + ' (' + values[0].slice(15) + ')';
		else if (values[0].startsWith('action-')) s = slashcommands.localizedString(locale, values[0], []);
		return slashcommands.localizedString(locale, 'gui-selectedaction', [s]);
	}
	if (choice == 'gui-combattarget') {
		setUserConfig(userID, 'gui-combattarget', values[0]);
		let s = '?';
		if (values[0].startsWith('adv-')) s = values[0].slice(4);
		if (values[0].startsWith('parry-')) s = slashcommands.localizedString(locale, 'stat-parry', []) + (values[0].slice(6) == '0' ? ' ' : ' +') + values[0].slice(6);
		return slashcommands.localizedString(locale, 'gui-selectedtarget', [s]);
	}
	if (choice == 'gui-patient') {
		setUserConfig(userID, 'gui-patient', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedpatient', [characterDataFetchHandleNulls(g, values[0], 'name', null)]);
	}
	if (choice == 'gui-cortex') {
		setUserConfig(userID, choice.slice(4), values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedcortex', [getUserConfig(userID, 'cortex1'),getUserConfig(userID, 'cortex2'),getUserConfig(userID, 'cortex3')]);
	}
	if (choice == 'gui-coyote-dice') {
		setUserConfig(userID, 'gui-coyote-dice', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedcoyote-dice', [getUserConfig(userID, 'gui-coyote-dice')]);
	}
	if (choice == 'gui-coyote-change') {
		setUserConfig(userID, 'gui-coyote-change', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedcoyote-change', [getUserConfig(userID, 'gui-coyote-change')]);
	}
	if (choice == 'gui-lmadversary') {
		setUserConfig(userID, 'gui-lmadversary', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedtarget', [values[0]]);
	}
	if (choice == 'gui-lm-testcondition') {
		setUserConfig(userID, 'gui-lm-testcondition', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedtest', [values[0]]);
	}
	if (choice == 'gui-lm-testtarget') {
		setUserConfig(userID, 'gui-lm-testtarget', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedtarget', [values[0]]);
	}
	if (choice == 'gui-lm-endeavour-timelimit') {
		setUserConfig(userID, 'gui-lm-endeavour-timelimit', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedtimelimit', [values[0]]);
	}
	if (choice == 'gui-lm-council-attitude') {
		setUserConfig(userID, 'gui-lm-council-attitude', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedattitude', [values[0]]);
	}
	if (choice == 'gui-lm-journey') {
		setUserConfig(userID, 'gui-lm-journey', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedjourney', [values[0]]);
	}
	if (choice == 'gui-lm-encounter') {
		setUserConfig(userID, 'gui-lm-encounter', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedencounter', [values[0]]);
	}
	if (choice == 'gui-lm-lightlevel') {
		setUserConfig(userID, 'gui-lm-lightlevel', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedlightlevel', [values[0]]);
	}
	if (choice == 'gui-lm-modtarget') {
		setUserConfig(userID, 'gui-lm-modtarget', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedtarget', [values[0]]);
	}
	if (choice == 'gui-lm-modduration') {
		setUserConfig(userID, 'gui-lm-modduration', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedduration', [values[0] == 0 ? slashcommands.localizedString(locale, 'gui-duration-wholecombat', []) : values[0]]);
	}
	if (choice == 'gui-lm-environmenttarget') {
		setUserConfig(userID, 'gui-lm-environmenttarget', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedtarget', [values[0]]);
	}
	if (choice == 'gui-lm-environmenttype') {
		setUserConfig(userID, 'gui-lm-environmenttype', values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedtype', [values[0]]);
	}
	if (choice.startsWith('gui-val-') || choice == 'gui-eye-value' || choice == 'gui-company-value') {
		setUserConfig(userID, choice, values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedvalue', [values[0]]);
	}
	if (choice == 'gui-bonusdice') {
		setUserConfig(userID, choice, values[0]);
		return slashcommands.localizedString(locale, 'gui-selectedbonus', [(values[0] >= 0 ? '+' : '') + values[0]]);
	}
	console.log('Error: invalid pulldownMenuClick choice ' + choice + ' = ' + values.join(', '));
	return ':bomb: Something went wrong. Please `/suggest` to tell us what just happened.';
}

// a standard dialog for all 'you must confirm' errors
function buildConfirmDialog(locale, g, userID, title, text, data, cmd) {
	// build and save the confirmed version of the command
	let dataconfirm = JSON.parse(JSON.stringify(data));
	if (dataconfirm.options == undefined) dataconfirm.options = [];
	if (cmd == 'encdelete' || cmd == 'advdelete' || cmd == 'journeydelete' || cmd == 'journeyclear' || cmd == 'hexflowerdelete'  || cmd == 'tabledelete' || cmd == 'tabledeletegroup') dataconfirm.options[0].options.push({'value': true, 'type': 5, 'name': 'confirm'});
	else dataconfirm.options.push({'value': true, 'type': 5, 'name': 'confirm'});
	setUserConfig(userID, 'confirm-' + cmd, dataconfirm);

	// build and return the embed with the button that will later refer to it
	return embeds.buildDialog(
		locale, g, getGameName(g), [userID],
		stringutil.titleCaseWords(title),
		'https://pbs.twimg.com/profile_images/1323438926753865728/2dctLWe8_400x400.jpg',
		'https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md',
		text ? text : slashcommands.localizedString(locale, 'error-unconfirmed', []),
		'',
		embeds.BUTTON_GREYPLE,
		[
			{
				'command':'confirm-' + cmd,
				'color':2,
				'label':'confirm',
				'extratext':'',
				'emoji':BUTTON_CONFIRM
			}
		]);
}