//------------------------------------------------------------------------------
// Dice rolls for Coyote & Crow
//

var embeds = require('./embeds.js');

var savedRolls = {};
/*
this is used to store rolls so that subsequent commands can modify them
there is no need to persist this across sessions though
each entry will be indexed by a charName, then contain something like this:
{ 
	white: [3, 5, 8, 4],
	black: [11],
	sn: 13,
	comment: "comment on the roll"
}
*/

// A simple linear die roll
function simpleRoll(n) {
	return Math.floor(Math.random() * n + 1);
}

// validation of 1-12 for values and SNs
function validated(n) {
	if (n < 1) return 1;
	if (n > 12) return 12;
	return n;
}

// dispatcher function simply branches to the appropriate subcommand
function coyoteAndCrowCommand(locale, charName, charImage, cmd, numdice, sn, criticals, sort, finish, comment, roll, newvalue) {
	if (savedRolls == null) savedRolls = {};
	if (numdice > 50) numdice = 50;
	if (cmd == 'roll') return coyoteAndCrowCommandRoll(locale, charName, charImage, numdice, sn, criticals, sort, finish, comment);
	if (cmd == 'change') return coyoteAndCrowCommandChange(locale, charName, charImage, finish, comment, roll, newvalue);
	if (cmd == 'finish') return coyoteAndCrowCommandFinish(locale, charName, charImage, comment);
	return [0, '**Error**: Internal error with `/coyote ' + cmd + '`', null, null, null];
}

// handle the initial roll, possibly with sorting
function coyoteAndCrowCommandRoll(locale, charName, charImage, numdice, sn, criticals, sort, finish, comment) {
	if (numdice <= 0) {
		return [0, '**Error**: You must specify at least one number of pool (white) dice.', '', null, null];
	}

	// build a base saved roll
	let savedRoll = {
		'white': [],
		'black': [],
		'sn': sn,
		'comment': comment
	};

	// do the rolls
	for (let i=0; i<numdice; i++) {
		savedRoll.white.push(simpleRoll(12));
	}

	for (let i=0; i<criticals; i++) {
		savedRoll.black.push(simpleRoll(12));
	}
	// sort the rolls if specified
	if (sort) {
		savedRoll.white.sort(function(a, b) {return a - b;});
		savedRoll.black.sort(function(a, b) {return a - b;});
	}

	// save the roll
	savedRolls[charName] = savedRoll;

	// if finish, jump to the finish function with this roll
	if (finish) return coyoteAndCrowCommandFinish(locale, charName, charImage, comment);

	return buildResults(charName, charImage, savedRoll, 'initial', '');
}

function coyoteAndCrowCommandChange(locale, charName, charImage, finish, comment, roll, newvalue) {
	// find the saved roll
	let savedRoll = savedRolls[charName];
	if (savedRoll == undefined) {
		return [0, '**Error**: You have no previous roll saved to be changed.', null, null, null];
	}

	// find the die to change
	let i = savedRoll.white.indexOf(roll);
	if (i == -1) {
		return [0, '**Error**: Cannot find a '+ coyoteAndCrowDieFace(roll, 'white') + ' to change.', null, null, null];
	}

	// figure out the change
	let change = null;
	if (newvalue.toLowerCase() == 'up') {
		change = savedRoll.white[i] + 1;
	} else if (newvalue.toLowerCase() == 'down') {
		change = savedRoll.white[i] - 1;
	} else if (!isNaN(newvalue)) {
		change = newvalue;
	} else {
		return [0, '**Error**: The new value must either be a number from 1-12, or the word `up` or `down`.', null, null, null];
	}
	change = validated(change);

	// change it
	let msg = 'changed a ' + coyoteAndCrowDieFace(savedRoll.white[i], 'white') + ' to ' + coyoteAndCrowDieFace(change,'white');
	savedRoll.white[i] = change;
	if (comment != '') savedRoll.comment = comment;

	// save over the saved roll
	savedRolls[charName] = savedRoll;

	// if finish, jump to the finish function with this roll
	if (finish) return coyoteAndCrowCommandFinish(locale, charName, charImage, comment);

	// display the result again
	return buildResults(charName, charImage, savedRoll, 'change', msg);
}

function coyoteAndCrowCommandFinish(locale, charName, charImage, comment) {
	// find the saved roll
	let savedRoll = savedRolls[charName];
	if (savedRoll == undefined) {
		return [0, '**Error**: You have no previous roll saved to be finished.', null, null, null];
	}
	if (comment != '') savedRoll.comment = comment;
	
	// handle all the exploding critical dice
	// for all white rolls, explode 12s
	savedRoll.white.forEach(r => {
		if (r == 12) savedRoll.black.push(simpleRoll(12));
	});

	// now iteratively explode black roll 12s
	for (i = 0; i < savedRoll.black.length; i++) {
		if (savedRoll.black[i] == 12) savedRoll.black.push(simpleRoll(12));
		// the push will make the loop continue out so the new roll will also be checked for a 12, since savedRoll.black.length gets reevaluated every time through the loop
	}

	// clear the saved roll
	delete savedRolls[charName];

	// display an embed output
	return buildResults(charName, charImage, savedRoll, 'final', '');
}


// build the results from a saved roll, whether it's an initial, changed, or final
function buildResults(charName, charImage, savedRoll, rollType, bonusText) {

	// build the basic line of dice
	let rollLine = '';
	savedRoll.white.forEach(r => {
		rollLine += coyoteAndCrowDieFace(r, 'white');
	});
	savedRoll.black.forEach(r => {
		rollLine += coyoteAndCrowDieFace(r, 'black');
	});

	// add any explanatory text for change or final rolls
	let results = '**Result**: ';
	if (rollType == 'final') results = '**Final Results**: ';
	let successes = coyoteAndCrowSuccesses(savedRoll);
	if (successes < 0) results += '__Critical Failure__';
	else if (successes == 0) results += '__Failure__';
	else results += '__Success__';
	results += '  (' + String(successes) + ' successes)\n';

	let extraText = '';
	if (rollType == 'change') {
		extraText += '**Changes**: ' + bonusText + '\n';
	}
	if (savedRoll.comment != '') {
		extraText += '**Comment**: ' + savedRoll.comment + '\n';
	}
	if (rollType == 'initial' || rollType == 'change') {
		extraText += '\nUse `/coyote change` to change rolls or `/coyote finish` to complete the roll.\n';
	}

	// build the return
	let theEmbed = embeds.buildEmbed(
		charName,
		charImage,
		'https://coyoteandcrow.net/',
		results,
		extraText,
		null,
		null,
		null,
		null,
		embeds.BUTTON_GREEN,
		null
	);
	return [successes, rollLine, {embed: theEmbed}, null, null];
}

// choose appropriate emoji for dice roll displays
function coyoteAndCrowDieFace(roll, color) {
	if (color == 'white') {
		if (roll == 1) return '<:white01:927612093065220096>';
		if (roll == 2) return '<:white02:927612093291696158>';
		if (roll == 3) return '<:white03:927612093509816380>';
		if (roll == 4) return '<:white04:927612093618872330>';
		if (roll == 5) return '<:white05:927612093417529445>';
		if (roll == 6) return '<:white06:927612094034100254>';
		if (roll == 7) return '<:white07:927612094256410664>';
		if (roll == 8) return '<:white08:927612094466129941>';
		if (roll == 9) return '<:white09:927612094650667028>';
		if (roll == 10) return '<:white10:927612094495481928>';
		if (roll == 11) return '<:white11:927612094998806548>';
		if (roll == 12) return '<:white12:927612095158173736>';
	}
	if (color == 'black') {
		if (roll == 1) return '<:black01:927612091454586921>';
		if (roll == 2) return '<:black02:927612091483975720>';
		if (roll == 3) return '<:black03:927612091655942274>';
		if (roll == 4) return '<:black04:927612093715324938>';
		if (roll == 5) return '<:black05:927612091874041886>';
		if (roll == 6) return '<:black06:927612092050190406>';
		if (roll == 7) return '<:black07:927612091907592253>';
		if (roll == 8) return '<:black08:927612092289269820>';
		if (roll == 9) return '<:black09:927612092431892490>';
		if (roll == 10) return '<:black10:927612092599648296>';
		if (roll == 11) return '<:black11:927612092738076682>';
		if (roll == 12) return '<:black12:927612092842926120>';
	}
	return roll; 
}

// tally up a saved roll and count resulting successes
function coyoteAndCrowSuccesses(savedRoll) {
	let result = 0;
	savedRoll.white.forEach(r => {
		if (r == 12 || r >= savedRoll.sn) result++;
		else if (r == 1) result--;
	});
	savedRoll.black.forEach(r => {
		if (r >= savedRoll.sn) result += 2;
		else result++;
	});
	return result;
}

// interaction elements for a Coyote and Crow game's dice rolls
function goCommandCoyote(interaction, userID, locale, charName) {

	let savedRoll = savedRolls[charName];

	let interfaceElements = [], r = '', defaultChange = null;

	if (savedRoll == undefined) { // if no roll is underway, provide the panel for starting one
		r = 'Start a Coyote & Crow roll by specifying a number of dice and clicking **Start A Roll**';
		interfaceElements.push({
			'rowtype':'pulldown',
			'id':'gui-coyote-dice',
			'contents':[
				{'label':'=1 die','value':'1','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=2 dice','value':'2','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=3 dice','value':'3','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=4 dice','value':'4','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=5 dice','value':'5','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=6 dice','value':'6','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=7 dice','value':'7','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=8 dice','value':'8','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=9 dice','value':'9','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=10 dice','value':'10','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=11 dice','value':'11','description':'','emoji':'\uD83C\uDFB2'},
				{'label':'=12 dice','value':'12','description':'','emoji':'\uD83C\uDFB2'}
			]
		});
		interfaceElements.push({
			'rowtype':'buttons',
			'id':'',
			'contents':[
				{'command':'gui-coyote-roll', 'color': 2, 'label': '=Start The Roll', 'extratext': '', 'emoji':'\uD83C\uDFB2'}
			]
		});
	} else { // if a roll is underway, provide an interface for changing or finishing one
		// first create a row that lets you select your existing rolls
		let whiteOptions = [], alreadyAdded = [];
		savedRoll.white.forEach(w => {
			if (defaultChange == null) defaultChange = w;
			if (!alreadyAdded.includes(w)) {
				alreadyAdded.push(w);
				whiteOptions.push({'label':'=' + w,'value':w,'description':'','emoji':'\uD83C\uDFB2'});
			}
		});
		interfaceElements.push({
			'rowtype':'pulldown',
			'id':'gui-coyote-change',
			'contents':[...whiteOptions]
		});
		// add a row with buttons to change it up and down, and finish the roll
		interfaceElements.push({
			'rowtype':'buttons',
			'id':'',
			'contents':[
				{'command':'gui-coyote-up', 'color': 2, 'label': '=Move Up', 'extratext': '', 'emoji':'\uD83D\uDD3C'},
				{'command':'gui-coyote-down', 'color': 2, 'label': '=Move Down', 'extratext': '', 'emoji':'\uD83D\uDD3D'},
				{'command':'gui-coyote-finish', 'color': 2, 'label': '=Finish The Roll', 'extratext': '', 'emoji':'\uD83C\uDFB2'}
			]
		});
		// And show your current roll in a brief form
		r = '**Current Roll**: :white_circle: ' + savedRoll.white.join(',') + ' :black_circle: ' + savedRoll.black.join(',');
	}

	return [interfaceElements, 'Coyote & Crow', r, 1, defaultChange];
}

module.exports = Object.assign({ coyoteAndCrowCommand, goCommandCoyote });

//-- someday maybe handle SN>12 rules
