// EMBEDS ----------------------------------------------------------------------
// builds an embed and optionally sends it to a channel

var slashcommands = require('./slashcommands.js');
var stringutil = require('./stringutil.js');

const BUTTON_GREEN   = 2915404;
const BUTTON_RED     = 15548997;
const BUTTON_BLURPLE = 5793266;
const BUTTON_GREYPLE = 7634829;

// this embed is used for rolls
function buildEmbed(charName, charImage, linkURL, titleText, descText, footer, bot, channelID, gameName, color, userID) {
	if (titleText == null || titleText == '') titleText = '';
	if (descText == null || descText == '') descText = 'Narvi';
	if (charImage == null || charImage == '') charImage = 'https://cdn.discordapp.com/attachments/848011016612478987/881999824503857212/Narvi_avatar.png';
	if (linkURL == null || linkURL == '') linkURL = 'https://bitbucket.org/HawthornThistleberry/narvi/';
	if (charName == null || charName == '') charName = 'The One Ring';

	if (titleText.length >= 256) {
		descText = titleText + '\n' + descText;
		titleText = '';
	}
	let theEmbed = {
		'title': titleText,
		'description': descText,
		'url': linkURL,
		'color': color,
		'thumbnail': {
			'url': charImage
		},
		'author': {
			'name': charName,
			'url': linkURL,
			'icon_url': 'https://cdn.discordapp.com/attachments/848011016612478987/881999824503857212/Narvi_avatar.png'
		},
		'image': {
			'url': 'https://i.stack.imgur.com/Fzh0w.png'
		},
		'footer': {
			'text': footer
		}
	};
	if (bot && channelID) return bot.createMessage(channelID, {embed: theEmbed});
	else return theEmbed;
}

function buildEmbedLink(keyType, keyValue) {
	if (keyType == 'googlesheets') return 'https://docs.google.com/spreadsheets/d/' + keyValue + '/';
	return '';
}

// the embed returned from /help
function helpEmbed(version) {
	let theEmbed = {
		'title': 'Narvi ' + version,
		'description': 'Instructions for how to use Narvi can be found here:',
		'url': 'https://bitbucket.org/HawthornThistleberry/narvi/',
		'color': 527348,
		'thumbnail': {
			'url': 'https://cdn.discordapp.com/attachments/848011016612478987/881999824503857212/Narvi_avatar.png'
		},
		'fields': [
		{
			'name': ':game_die: Player Guide',
			'value': '[A how-to guide with only the information players need](https://bitbucket.org/HawthornThistleberry/narvi/src/master/PlayerGuide.md)',
			'inline': false
		},
		{
			'name': ':ledger: How To',
			'value': '[Step-by-step guide to using Narvi for anything](https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md)',
			'inline': false
		},
		{
			'name': ':books: Full User Manual',
			'value': '[The complete documentation for all Narvi functions](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md)',
			'inline': false
		},
		{
			'name': ':troll: Adversaries',
			'value': '[How to create adversaries and encounters](https://bitbucket.org/HawthornThistleberry/narvi/src/master/Adversaries.md)',
			'inline': false
		},
		{
			'name': ':bookmark_tabs: Lookup Tables',
			'value': '[How to build and share your own lookup tables](https://bitbucket.org/HawthornThistleberry/narvi/src/master/LookupTables.md)',
			'inline': false
		},
		{
			'name': '<:d20_20:934233380567011360> Other Games',
			'value': '[Using Narvi with games other than TOR](https://bitbucket.org/HawthornThistleberry/narvi/src/master/OtherGames.md)',
			'inline': false
		},
		{
			'name': ':scroll: Version History',
			'value': '[A list of every update in Narvi\'s long history](https://bitbucket.org/HawthornThistleberry/narvi/src/master/VersionHistory.md)',
			'inline': false
		}
		]
		};
	let locales = slashcommands.localesKnownNames();
	if (locales.length > 1) {
		theEmbed.fields.push(
		{
			'name': ':checkered_flag: Known Languages',
			'value': locales.join('; '),
			'inline': false
		}
		);
	}
	return theEmbed;
}

// localize something that'll be displayed on a button or in a menu
function localizeLabel(locale, labelId, words) {
	let label;
	if (labelId == undefined) labelId = 'Go';
	if ((labelId.startsWith('!') || labelId.startsWith('^')) && labelId.includes('-')) {
		let l = labelId.slice(1).split('-');
		if (l.length == 3) label = slashcommands.localizedOption(locale, l[0], l[1], l[2]);
		else if (l.length == 4) label = slashcommands.localizedSubcommandOption(locale, l[0], l[1], l[2], l[3]);
		else label = slashcommands.localizedSubcommand(locale, l[0], l[1]);
		if (labelId.startsWith('^')) label = stringutil.upperCase(label);
	}
	else if (labelId.startsWith('?') && labelId.includes('-')) {
		let l = labelId.slice(1).split('-');
		label = slashcommands.localizedOptionDescription(locale, l);
	}
	else if (labelId.startsWith('!')) label = slashcommands.localizedCommand(locale, labelId.slice(1));
	else if (labelId.startsWith('=')) label = labelId.slice(1);
	else if (labelId == '') label = labelId;
	else label = slashcommands.localizedString(locale, labelId, []);
	if (label.startsWith('__**')) {
		label = label.slice(4).split('**__')[0];
	}
	if (words && !labelId.startsWith('=') && !labelId.startsWith('^') && !labelId.startsWith('?')) label = ' ' + stringutil.titleCaseWords(label);
	return label;
}

// used for buttons for spending tengwar
function buildButton(locale, id, style, labelId, labelSuffix, emoji, gameName, userIDs) {
	let label = localizeLabel(locale, labelId, true);
	if (labelSuffix) label += ' ' + labelSuffix;
	let customID = id + ' ' + gameName + ' ' + userIDs.join('|');
	if (customID.length > 100) customID = id + ' ' + gameName + ' all';

	let theButton = {
		"type": 2,
		"label": label,
		"style": style,
		"emoji": {
			"name": emoji,
			"id": null
			},
		"custom_id": customID
		};
	return theButton;
}

// builds a rich display of an adversary in an embed layout
function dashOrNumber(val) {
	return (val == 0 ? '-' : String(val));
}

function specialAttackName(special, locale) {
	if (special == 'breakshield') return 'Break Shield';
	if (special.startsWith('fieryblow')) return 'Fiery Blow (' + slashcommands.localizedString(locale, 'condition-severity-' + special.slice(-1), []) + ')';
	return stringutil.titleCaseWords(special);
}

function buildAdversary(locale, shortname, adversary) {
	let title = adversary.name + ' (`' + shortname + '`)';
	if (adversary.published) title += ' :ledger:';
	if (adversary.catalog && adversary.catalog != '') title += ' :ledger: `' + adversary.catalog + '`';

	let description = '_' + adversary.description + '_';
	if (adversary.description == '') description = '';
	if (adversary.features != '') description += '\n\n**' + adversary.features + '**';

	if (adversary.knockback != undefined) {
		// this is an encounter adversary who might have status, so show it at the end of the description
		let status = [];
		if (adversary.knockback > 0) status.push(slashcommands.localizedString(locale, 'condition-knockback', []));
		if (adversary.pushback > 0) status.push(slashcommands.localizedString(locale, 'condition-pushback', []));
		if (adversary.weary > 0 || adversary.curhrscore <= 0) status.push(slashcommands.localizedString(locale, 'condition-weary', []));
		if (adversary.wounds == 1) status.push(slashcommands.localizedString(locale, 'token-wounded', []));
		if (adversary.wounds > 1) status.push(adversary.wounds + ' ' + slashcommands.localizedString(locale, 'token-wounds', []));
		if (status.length > 0) {
			description += '\n\n**' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'condition-heading', [])) + '**: ' + status.join(', ');
		}
	}

	let hrtype = slashcommands.localizedString(locale, 'adversary-' + adversary.hrtype, []);

	// removed since Discord seems to be ignoring them when done this way - pity, it looked awesome when it did work
	//let image = adversary.image;
	//if (image == '') image = 'https://i.stack.imgur.com/Fzh0w.png';

	let attacks = '';
	adversary.attacks.forEach(attack => {
		attacks += ':dagger: **' + attack.name + (attack.ranged ? ':bow_and_arrow:' : '') + '** ' + attack.dice + ' (' + dashOrNumber(attack.damage) + '/' + dashOrNumber(attack.injury);
		if (attack.special != '') attacks += ', ' + specialAttackName(attack.special, locale);
		attacks += ')';
		if (attack.comment != '') attacks += ': ' + attack.comment;
		attacks += '\n';
	});
	if (attacks == '') attacks = slashcommands.localizedString(locale, 'none', []);

	let abilities = '';
	adversary.abilities.forEach(ability => {
		abilities += ':magic_wand: **' + ability.name + '**';
		if (ability.type != '') abilities += ' (' + ability.type + ')';
		if (ability.amount != '') abilities += ' [' + ability.amount + ']';
		if (ability.comment != '') abilities += ': ' + ability.comment;
		abilities += '\n';
	});
	if (abilities == '') abilities = slashcommands.localizedString(locale, 'none', []);
	if (abilities.length > 1024) {
		abilities = '';
		adversary.abilities.forEach(ability => {
			abilities += ':magic_wand: **' + ability.name + '**';
			if (ability.type != '') abilities += ' (' + ability.type + ')';
			if (ability.amount != '') abilities += ' [' + ability.amount + ']';
			abilities += '\n';
		});
	}

	let footer = '';
	if (adversary.tags && adversary.tags.length > 0) footer += stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale,'table','tags')) + ': ' + adversary.tags.join(', ') + '\n';
	if (adversary.banetype != '') footer += stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'aedit','set','banetype')) + ': ' + adversary.banetype + '\n';
	if (adversary.size != '') footer += stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'aedit','set','size')) + ': ' + adversary.size + '\n';

	let endurance = adversary.endurance;
	if (adversary.curendurance && adversary.curendurance < adversary.endurance) endurance = adversary.curendurance + '/' + endurance;
	let hrscore = adversary.hrscore;
	if (adversary.curhrscore && adversary.curhrscore < adversary.endurance) hrscore = adversary.curhrscore + '/' + hrscore;
	let might = adversary.might;
	if (adversary.wounds && adversary.wounds > 0) might += '  / :red_circle: ' + adversary.wounds;

	let theEmbed = {
		//'type': 'rich',
		'title': title,
		'description': description,
		'color': 527348,
		'fields': [{
			'name': stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'aedit','set','attribute')),
			'value': ':bust_in_silhouette: ' + adversary.attribute,
			'inline': true
		},{
			'name': stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'aedit','set','endurance')),
			'value': ':drop_of_blood: ' + endurance,
			'inline': true
		},{
			'name': stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'aedit','set','might')),
			'value': ':muscle: ' + might,
			'inline': true
		},{
			'name': stringutil.titleCaseWords(hrtype),
			'value': ':rage: ' + hrscore,
			'inline': true
		},{
			'name': stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'aedit','set','parry')),
			'value': ':fencer: ' + (adversary.parry > 0 ? '+' : '') + dashOrNumber(adversary.parry),
			'inline': true
		},{
			'name': stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'aedit','set','armour')),
			'value': ':shield: ' + adversary.armour,
			'inline': true
		},{
			'name': stringutil.titleCaseWords(slashcommands.localizedString(locale, 'adversary-attacks', [])),
			'value': attacks
		},{
			'name': stringutil.titleCaseWords(slashcommands.localizedString(locale, 'adversary-abilities', [])),
			'value': abilities
		}],
		// removed since Discord seems to be ignoring them when done this way
		//'image': {
		//	'url': image
		//	},
		'footer': {
			'text': footer
			}
	};
	return theEmbed;
}

// build a pretty display of an encounter
function buildEncounter(locale, shortname, encounter) {

	let title = encounter.title + ' (`' + shortname + '`)';

	let description = '_' + encounter.notes + '_';
	if (encounter.notes == '') description = '';

	// removed since Discord seems to be ignoring them when done this way
	//let image = encounter.image;
	//if (image == '') image = 'https://i.stack.imgur.com/Fzh0w.png';

	let adversariesArray = [];
	for (adversary in encounter.adversaries) {
		let display = adversary + ': ' + encounter.adversaries[adversary].name + ' (';
		display += stringutil.upperCase(slashcommands.localizedOption(locale, 'aedit','set','attribute').charAt(0)) + encounter.adversaries[adversary].attribute + ', ';
		display += stringutil.upperCase(slashcommands.localizedOption(locale, 'aedit','set','might').charAt(0)) + encounter.adversaries[adversary].might + ', ';
		display += stringutil.upperCase(slashcommands.localizedOption(locale, 'aedit','set','endurance').charAt(0));
		if (encounter.adversaries[adversary].curendurance < encounter.adversaries[adversary].endurance)
			display += encounter.adversaries[adversary].curendurance + '/';
		display += encounter.adversaries[adversary].endurance + ', ';
		display += stringutil.upperCase(slashcommands.localizedString(locale, 'adversary-' + encounter.adversaries[adversary].hrtype).charAt(0));
		if (encounter.adversaries[adversary].curhrscore < encounter.adversaries[adversary].hrscore)
			display += encounter.adversaries[adversary].curhrscore + '/';
		display += encounter.adversaries[adversary].hrscore + ', ';
		display += stringutil.upperCase(slashcommands.localizedOption(locale, 'aedit','set','parry').charAt(0)) + encounter.adversaries[adversary].parry + ', ';
		display += stringutil.upperCase(slashcommands.localizedOption(locale, 'aedit','set','armour').charAt(0)) + encounter.adversaries[adversary].armour + ')';
		adversariesArray.push(display);
	}
	let adversariesText = slashcommands.localizedString(locale, 'none', []);
	if (adversariesArray.length != 0) adversariesText = adversariesArray.join('\n');
	if (adversariesText.length > 1000) {
		adversariesArray = [];
		for (adversary in encounter.adversaries) {
			let display = adversary + ': ' + encounter.adversaries[adversary].name;
			adversariesArray.push(display);
		}
		adversariesText = adversariesArray.join('\n');
		if (adversariesText.length > 1024) adversariesText = adversariesText.substr(0,1000) + '...';
	}

	let eventsArray = [];
	encounter.events.forEach(event => {
		let display = stringutil.upperCase(slashcommands.localizedOption(locale, 'encounter','event','round').charAt(0));
		display += String(event.round) + ' ';
		display += slashcommands.localizedString(locale, 'initiative-phase' + String(event.phase), []);
		display += ': ';
		if (event.secret === true) display += ':mute:';
		display += event.text;
		eventsArray.push(display);
	});
	let eventsText = slashcommands.localizedString(locale, 'none', []);
	if (eventsArray.length != 0) eventsText = eventsArray.join('\n');
	if (eventsText.length > 1024) eventsText = eventsText.substr(0,1000) + '...';

	let modifierText = slashcommands.localizedString(locale, 'none', []);
	if (encounter.modifier.modifier != null && encounter.modifier.modifier != 0) {
		modifierText = (encounter.modifier.modifier > 0 ? '+': '') + String(encounter.modifier.modifier);
		if (encounter.modifier.duration > 0) modifierText += 'd (' + String(encounter.modifier.duration) + ' ' + slashcommands.localizedString(locale, 'combat-rounds', []) + ')';
	}
	if (modifierText.length > 1024) modifierText = modifierText.substr(0,1000) + '...';

	let theEmbed = {
		//'type': 'rich',
		'title': title,
		'description': description,
		'color': 527348,
		'fields': [{
			'name': stringutil.titleCase(slashcommands.localizedString(locale, 'adversary-adversaries',[])),
			'value': adversariesText
		},{
			'name': stringutil.titleCase(slashcommands.localizedString(locale, 'encounter-events',[])),
			'value': eventsText
		},{
			'name': stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'encounter','modifier')),
			'value': modifierText
		}],
		// removed since Discord seems to be ignoring them when done this way
		//'image': {
		//	'url': image
		//}
	};
	return theEmbed;
}

function buildDialog(locale, g, gameName, userIDs, title, image, link, text, footer, color, buttons) {
	let theEmbed = buildEmbed(
		title,
		image,
		link,
		text,
		footer,
		null,
		null,
		null,
		gameName,
		color,
		userIDs[0]
	);
	if (buttons.length == 0) return {embed: theEmbed};
	let theButtons = [];
	buttons.forEach(button => {
		theButtons.push(buildButton(locale, button.command, button.color, button.label, button.extratext, button.emoji, gameName, userIDs));
	});
	let resultObject = {embed: theEmbed};
	resultObject.components = [{"type": 1,"components": [...theButtons]}];
	return resultObject;
}

// like build dialog but supports multiple rows of buttons
function buildGUI(locale, g, gameName, userIDs, title, image, link, text, footer, color, rows) {
	let theEmbed = buildEmbed(
		title,
		image,
		link,
		text,
		footer,
		null,
		null,
		null,
		gameName,
		color,
		userIDs[0]
	);
	if (!rows || rows.length == 0) return {embed: theEmbed};
	let resultObject = {embed: theEmbed};
	resultObject.components = [];
	rows.forEach(row => {
		let components = [];
		if (row.rowtype == 'buttons') {
			row.contents.forEach(button => {
				components.push(buildButton(locale, button.command, button.color, button.label, button.extratext, button.emoji, gameName, userIDs));
			});
		} else if (row.rowtype == 'pulldown') {
			let pulldown = {'type':3, 'custom_id': row.id + ' ' + gameName + ' ' + userIDs.join('|'), 'options': []};
			let defaultOption = true;
			row.contents.forEach(option => {
				pulldown.options.push({
					'label':localizeLabel(locale, option.label, true),
					'value': option.value,
					'description': localizeLabel(locale, option.description, false),
					'emoji': {'name': option.emoji, 'id':null},
					'default': defaultOption
				});
				defaultOption = false;
			});
			components.push(pulldown);
		}
		resultObject.components.push({"type": 1,"components": [...components]});
	});
	//console.log(JSON.stringify(resultObject));
	return resultObject;
}


module.exports = Object.assign({ buildEmbed, buildEmbedLink, helpEmbed, buildButton, buildAdversary, buildEncounter, buildDialog, buildGUI, BUTTON_GREEN, BUTTON_RED, BUTTON_BLURPLE, BUTTON_GREYPLE });
