// DICE AND EMOJIS -------------------------------------------------------------
// returns the right custom emoji

var stringutil = require('./stringutil.js');

const EYE = 'E';
const GANDALF = 'G';
const TENGWAR = 't';

function customEmoji(roll, success, weary, enemy) {
	if (enemy) return customEmojiSauronic(roll, success, weary);
	return customEmojiRegular(roll, success, weary);
}

function emojiPunctuation(c) {
	if (c == '(') return '<:openparens:869703153937944697>';
	if (c == ')') return '<:closeparens:869703153803722753>';
	return c;
}

function customEmojiRegular(roll, success, weary) {
	if (roll == GANDALF && success) return '<:gandalf:866651302234685440>';
	if (roll == TENGWAR) return '<:tengwar:866651280315383808>';
	if (roll == 1 && (weary || success)) return '<:success1:867876314883031061>';
	if (roll == 2 && (weary || success)) return '<:success2:867876315410595900>';
	if (roll == 3 && (weary || success)) return '<:success3:867876315336146996>';
	if (roll == 4 && success) return '<:success4:867876315658846268>';
	if (roll == 5 && success) return '<:success5:867876315771699230>';
	if (roll == 6 && success) return '<:success6:867876315910635550>';
	if (roll == 1) return '<:feat01:867874159543451718>';
	if (roll == 2) return '<:feat02:867874159261777932>';
	if (roll == 3) return '<:feat03:867874159639003176>';
	if (roll == 4) return '<:feat04:867874159837446144>';
	if (roll == 5) return '<:feat05:867874159551447092>';
	if (roll == 6) return '<:feat06:867874160112828446>';
	if (roll == 7) return '<:feat07:867874159991717930>';
	if (roll == 8) return '<:feat08:867874160322936873>';
	if (roll == 9) return '<:feat09:867874160407740427>';
	if (roll == 10) return '<:feat10:867874160763207720>';
	if (roll == 11) return '<:feat11:931216326016524308>';
	if (roll == EYE || roll == 0) return '<:feateye:867874160944349234>';
	if (roll == GANDALF || roll == 11 || roll == 12) return '<:featgandalf:867874161216192572>';
	return roll; 
}

function customEmojiSauronic(roll, success, weary) {
	if (roll == GANDALF && success) return '<:gandalf:866651302234685440>';
	if (roll == TENGWAR) return '<:tengwar:866651280315383808>';
	if (roll == 1 && (weary || success)) return '<:ssuccess1:938511333278777344>';
	if (roll == 2 && (weary || success)) return '<:ssuccess2:938511333333299210>';
	if (roll == 3 && (weary || success)) return '<:ssuccess3:938511333291352155>';
	if (roll == 4 && success) return '<:ssuccess4:938511333396213831>';
	if (roll == 5 && success) return '<:ssuccess5:938511333245202563>';
	if (roll == 6 && success) return '<:ssuccess6:938511333807235183>';
	if (roll == 1) return '<:sfeat1:938511332033060874>';
	if (roll == 2) return '<:sfeat2:938511332116938792>';
	if (roll == 3) return '<:sfeat3:938511332175646720>';
	if (roll == 4) return '<:sfeat4:938511332049813545>';
	if (roll == 5) return '<:sfeat5:938511332020461569>';
	if (roll == 6) return '<:sfeat6:938511332351811666>';
	if (roll == 7) return '<:sfeat7:938511332423131166>';
	if (roll == 8) return '<:sfeat8:938511332519604284>';
	if (roll == 9) return '<:sfeat9:938511332754473000>';
	if (roll == 10) return '<:sfeat10:938511332863516752>';
	if (roll == 11) return '<:sfeat11:974093183464116254>';
	if (roll == EYE || roll == 12) return '<:sfeateye:938511332997754992>';
	if (roll == GANDALF) return '<:sfeatgandalf:938511333031288832>';
	return roll; 
}

// when a value gets really long, use these special emoji to show it in less space
function halvedEmojiColor(token) {
	if (token == 'endurance' || token == 'maxendurance') return 'blue';
	if (token == 'hope' || token == 'maxhope') return 'white';
	if (token == 'fatigue') return 'fatigue';
	if (token == 'encumbrance') return 'encumbrance';
	return 'orange';
}

function halvedEmoji(color, number) {
	if (color == 'blue' && number == 1) return ':blue_circle:';
	if (color == 'blue' && number == 2) return '<:blue2:848239384910233652>';
	if (color == 'white' && number == 1) return ':white_circle:';
	if (color == 'white' && number == 2) return '<:white2:848239457978286101>';
	if (color == 'empty' && number == 1) return ':o:';
	if (color == 'empty' && number == 2) return '<:empty2:848239384868028447>';
	if (color == 'fatigue' && number == 1) return '<:fatigue:908739537059061862>';
	if (color == 'fatigue' && number == 2) return '<:fatigue2:908739537272987658>';
	if (color == 'encumbrance' && number == 1) return '<:enc2:908739536778055710>';
	if (color == 'encumbrance' && number == 2) return ':radio_button:';
	if (number == 1) return ':orange_circle:';
	return '<:orange2:848239458029273128>';
}

function halvedCircles(color, value, maxvalue) {
	let r = '';
	if (value % 2 != 0) r += halvedEmoji(color, 1);
	r += stringutil.repeatString(halvedEmoji(color, 2), Math.floor(value/2));
	if (maxvalue > value && maxvalue != 0) {
		let empty = maxvalue - value;
		if (empty % 2 != 0) r += halvedEmoji('empty', 1);
		r += stringutil.repeatString(halvedEmoji('empty', 2), Math.floor(empty/2));
	}
	return r;
}

// show a particular value and its tokens
function valueAndTokens(value, token, max, neverhalve) {
	let r = '`' + stringutil.padLeft(value, 3,' ') + '` ';
	if (!neverhalve && value > 50) return r + ':bangbang:';
	if (!neverhalve && value > 25) return r + halvedCircles(halvedEmojiColor(token), value, max);
	r += tokenEmojis(token, value);
	if (max > value && max != 0) r += stringutil.repeatString(':o:', max-value);
	return r;
}

function statusLineWithMax(value, maxvalue, token, neverhalve) {
	if (value < 0) {
		if (token == 'endurance') return '`' + stringutil.padLeft(value, 3,' ') + '` ' + stringutil.repeatString(':large_blue_diamond:',-value);
		return '`' + stringutil.padLeft(value, 3,' ') + '` ' + stringutil.repeatString(':large_orange_diamond:', -value);
	}
	if (!neverhalve && (value > 50 || maxvalue > 50)) {
		return '`' + stringutil.padLeft(value, 3,' ') + '` :bangbang:';
	}
	if (!neverhalve && (value > 25 || maxvalue > 25)) {
		return '`' + stringutil.padLeft(value, 3,' ') + '` ' + halvedCircles(halvedEmojiColor(token), value, maxvalue);
	}
	return valueAndTokens(heading, value, token, maxvalue, neverhalve);
}

function headingValueAndTokens(heading, value, token, neverhalve) {
	var header = heading;
	if (header != '') header = '`' + stringutil.padRight(heading, 15, ' ') + '` ';
	return header + valueAndTokens(value, token, 0, neverhalve);
}

function showStatusLineWithMax(heading, value, maxvalue, token, neverhalve) {
	var header = heading;
	if (header != '') header = '`' + stringutil.padRight(heading, 15, ' ') + '` ';
	if (value < 0) {
		return header + statusLineWithMax(value, maxvalue, token, neverhalve);
	}
	if (!neverhalve && (value > 50 || maxvalue > 50)) {
		return header + '`' + stringutil.padLeft(value, 3,' ') + '` :bangbang:';
	}
	if (!neverhalve && (value > 25 || maxvalue > 25)) {
		return header + '`' + stringutil.padLeft(value, 3,' ') + '` ' + halvedCircles(halvedEmojiColor(token), value, maxvalue);
	}
	return headingValueAndTokens(heading, value, token, neverhalve) + stringutil.repeatString(':o:', maxvalue-value);
}

function showStatusLineStacked(heading, firstvalue, firsttoken, secondvalue, secondtoken, neverhalve) {
	var header = heading;
	if (header != '') header = '`' + stringutil.padRight(heading, 15, ' ') + '` ';
	if (!neverhalve && firstvalue + secondvalue > 50) {
		return header + '`' + stringutil.padLeft(firstvalue + secondvalue, 3,' ') + '` :bangbang:';
	}
	// handle if the line is getting too long with halved tokens maybe
	if (!neverhalve && firstvalue + secondvalue > 25) {
		return header + '`' + stringutil.padLeft(firstvalue + secondvalue, 3,' ') + '` ' + halvedCircles(halvedEmojiColor(firsttoken), firstvalue, firstvalue) + halvedCircles(halvedEmojiColor(secondtoken), secondvalue, secondvalue);
	}
	return header + '`' + 
			stringutil.padLeft(firstvalue + secondvalue, 3,' ') + '` '+ 
			tokenEmojis(firsttoken, firstvalue) +
			tokenEmojis(secondtoken, secondvalue);
}

// each token's emoji is a circle of a specific color
function tokenEmoji(token) {
	if (token == 'pool') return ':purple_circle:';
	if (token == 'ap') return ':large_orange_diamond:';
	if (token == 'noap') return ':small_orange_diamond:';
	if (token == 'hope' || token == 'maxhope') return ':white_circle:';
	if (token == 'shadow') return ':brown_circle:';
	if (token == 'shadowscar' || token == 'shadowscars') return '<:shadowscar:887515777333354496>';
	if (token == 'wounds' || token == 'untreated') return ':red_circle:';
	if (token == 'endurance' || token == 'maxendurance') return ':blue_circle:';
	if (token == 'encumbrance') return ':radio_button:';
	if (token == 'enhearten') return ':high_brightness:';
	if (token == 'fatigue') return '<:fatigue:908739537059061862>';
	if (token == 'bonus') return ':green_circle:';
	if (token == 'tempbonus') return ':green_circle:';
	if (token == 'tempparry') return ':shield:';
	if (token == 'bleeding') return ':drop_of_blood:';
	if (token == 'tengwar') return customEmojiRegular(TENGWAR, 0, 0);
	if (token == 'treated') return ':adhesive_bandage:';
	if (token == 'failed') return ':thermometer:';
	if (token == 'strength') return ':muscle:';
	if (token == 'heart') return ':heart:';
	if (token == 'wits') return ':candle:';
	if (token == 'valour') return ':lion:';
	if (token == 'wisdom') return ':owl:';
	return ':orange_circle:';
}

// make a series of emoji tokens to count something
function tokenEmojis(token, number) {
	if (token != 'endurance' && number < 0) return stringutil.repeatString(':large_orange_diamond:',-number);
	return stringutil.repeatString(tokenEmoji(token), number);
}

function sheetSkillPips(skillScore, abbreviated) {
	let r = '';
	for (let i=0; i<skillScore; i++) r += ':large_blue_diamond:';
	if (!abbreviated) for (let i=skillScore; i<6; i++) r += ':small_blue_diamond:';
	return r;
}

function eyeDisplay(base, current, threshold) {
	let r = customEmojiSauronic(EYE, false, false) + ' ';
	r += stringutil.repeatString(':brown_square:',base);
	if (current > base) r += stringutil.repeatString(':red_square:',current-base);
	if (threshold > current) r += stringutil.repeatString(':white_large_square:',threshold-current);
	r += ' ' + String(current) + '/' + String(threshold);
	return r;
}

function progressBar(current, maximum, length) {
	let num = Math.floor(((current * length) / maximum) + 0.5);
	return stringutil.repeatString(':red_square:', num) + stringutil.repeatString(':yellow_square:', length-num);
}

module.exports = Object.assign({ customEmoji, emojiPunctuation, EYE, GANDALF, TENGWAR, halvedCircles, valueAndTokens, headingValueAndTokens, showStatusLineWithMax, showStatusLineStacked, tokenEmoji, tokenEmojis, sheetSkillPips, eyeDisplay, progressBar });
