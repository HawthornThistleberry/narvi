// ENCOUNTERS -----------------------------------------------------------------

const stringutil = require('./stringutil.js');
const table = require('./table.js');
const slashcommands = require('./slashcommands.js');
const embeds = require('./embeds.js');
const regdice = require('./regdice.js');
const adversaries = require('./adversaries.js');
const lookuptables = require('./lookuptables.js');

// create an encounter with nothing much in it
function encounters_create(g, encounterName, userID) {
	encounterName = stringutil.lowerCase(encounterName);

	// if an encounter already exists with that name, return false
	if (g.encounters[encounterName] != null) return false;

	// create an encounter with that name and fill in some basic info:
	g.encounters[encounterName] = {
		title: encounterName,
		notes: '',
		image: '',
		modifier: {},
		environment: {},
		events: [],
		adversaries: {}
	};
	return true;
}

// list encounters in your game
function encounters_list(g, userID, locale) {
	let reportHeadings = [
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'encounter-encounter',[])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'encounter', 'title')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'encounter-events',[])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'adversary-adversaries',[])), emoji:false, minWidth:0, align:'left'}
		];
	let reportData = [];
	for (let encounterName in g.encounters) {
		reportData.push([
			encounterName, 
			g.encounters[encounterName].title,
			String(g.encounters[encounterName].events.length),
			String(Object.keys(g.encounters[encounterName].adversaries).length),
		]);
	}
	if (reportData.length == 0) return slashcommands.localizedString(locale, 'encounter-none',[]);
	return table.buildTable(reportHeadings, reportData, 2);
}

// show an encounter
function encounters_show(g, encounterName, locale) {
	let theEmbed = embeds.buildEncounter(locale, encounterName, g.encounters[encounterName]);
	if (g.encounters[encounterName].image != '') return [{embed: theEmbed}, g.encounters[encounterName].image];
	return {embed: theEmbed};
}

// basic encounter fields editing
function encounters_edit(g, encounterName, locale, field, value) {
	// validation
	if (g.encounters[encounterName] == undefined) return 'encounter-inactive';
	if (field == 'title' || field == 'notes') value = stringutil.mildSanitizeString(value);
	if (field == 'image' && value == slashcommands.localizedString(locale, 'none',[])) value = '';
	if (field == 'image' && value != '' && !value.startsWith('http://') && !value.startsWith('https://' )) return 'error-badurl';
	if (field == 'title' && value.length > 120) return 'encounter-titletoolong';
	g.encounters[encounterName][field] = value;
	return '';
}

// setting a modifier
function encounters_modifier(g, encounterName, locale, value, duration) {
	g.encounters[encounterName].modifier = {'modifier': value, 'duration': duration, 'phase': 1, 'started': false, 'characters': []};
	return '';
}

// setting an environment
function encounters_environment(g, encounterName, locale, type, level, round) {
	g.encounters[encounterName].environment = {'type': type, 'level': level, 'round': round};
	return '';
}

// adding an event
function encounters_add_event(g, encounterName, locale, round, phase, text, secret) {
	g.encounters[encounterName].events.push({'round': round, 'phase': phase, 'text': text, 'secret': secret});
	return '';
}

// given a shortname and the iteration, build a new name
function addedAdversaryName(basename, quantity, index) {
	// build a new shortname: if quantity=1 use shortname, else use shortname + a space if shortname has any spaces + index
	let r = basename;
	if (quantity == 1) return r;
	if (basename.includes(' ')) r += ' '; // so a basename like "Orc Warrior" becomes "Orc Warrior 1", while a basename like "OW" becomes "OW1"
	r += String(index+1);
	return r;
}

// implement varying scores
function varyScore(baseValue, field, varytype) {
	if (varytype != field && varytype != 'all') return baseValue;
	// multiply the base value by (1 + (5d21-55)/100) and round off
	// this gives a tight bell curve; for instance, for a 20 endurance, 11 and 29 are possible, but only in 0.02% of cases each
	// while a 20 has about a 14.3% chance, 19 or 21 each have about a 13.5% chance, and so on
	// to see the graph, go to anydice and try something like this: output ((20 * (100 + 5d21 - 55)) + 50)/100
	// (most of the complexity in there is tricking anydice into doing float calculations and rounding off, not down)
	// or just look here: https://cdn.discordapp.com/attachments/866659701840412692/1113561289021722664/image.png
	let roll = regdice.simpleRoll(21) + regdice.simpleRoll(21) + regdice.simpleRoll(21) + regdice.simpleRoll(21) + regdice.simpleRoll(21);
	roll -= 55;
	roll = 1.0 + (roll / 100.0);
	return Math.floor(baseValue * roll + 0.5);
}

// adding an adversary
function encounters_add_adversary(g, encounterName, locale, userID, hostUser, catalog, shortname, quantity, vary) {
	// can we read the adversary in the catalog
	if (!adversaries.adversaries_accessible(catalog, userID, hostUser, 'read')) return 'adversary-notexists';
	// someone somehow got here without a valid adversaries structure so catch that
	if (!g.encounters || !g.encounters[encounterName]) return 'encounter-notexists';
	if (!g.encounters[encounterName].adversaries) g.encounters[encounterName].adversaries = {};
	// check if any of the adds will make a duplicate name; if so, abort without adding any, to make this atomic
	for (let i = 0; i < quantity; i++) {
		if (encounters_adversary_name_match(g, encounterName, addedAdversaryName(shortname, quantity, i), false) != null)
			return 'adversary-exists';
	}
	// check for whether the adversary is lacking any attacks -- but still let it go through if you are adding only one
	let adv = adversaries.adversaries_fetch(catalog);
	if (adv.attacks.length == 0 && quantity != 1) {
		return 'adversary-noattacks';
	}
	// now iterate again, this time actually adding the adversaries
	for (let i = 0; i < quantity; i++) {
		let aname = addedAdversaryName(shortname, quantity, i);
		g.encounters[encounterName].adversaries[aname] = {
			'catalog': catalog,
			'name': addedAdversaryName(adv.name, quantity, i),
			'description': adv.description,
			'image': adv.image,
			'features': adv.features,
			'attribute': adv.attribute,
			'hrtype': adv.hrtype,
			'hrscore': varyScore(adv.hrscore, 'hrscore', vary),
			'might': adv.might,
			'endurance': varyScore(adv.endurance, 'endurance', vary),
			'parry': varyScore(adv.parry, 'parry', vary),
			'armour': varyScore(adv.armour, 'armour', vary),
			'banetype': adv.banetype,
			'size': adv.size,
			'attacks': [...adv.attacks],
			'abilities': [...adv.abilities],
			'curendurance': 0,
			'curhrscore': 0,
			'knockback': 0,
			'pushback': false,
			'weary': 0,
			'wounds': 0,
			'protectionTests': [],
			'engagement': [],
			'standBack': false,
			'snakeLike': -1,
			'actionsThisRound': 0,
			'hateSpent': 0,
			'tengwar': {}
		};
		g.encounters[encounterName].adversaries[aname].curendurance = g.encounters[encounterName].adversaries[aname].endurance;
		g.encounters[encounterName].adversaries[aname].curhrscore = g.encounters[encounterName].adversaries[aname].hrscore;
	}
	return quantity;
}

// find a case-insensitive match to an adversary, and return the actual stored name or null if there is no match
function encounters_adversary_name_match(g, encounterName, shortname, partials) {
	if (g.encounters[encounterName] == undefined) return null;
	shortname = stringutil.lowerCase(shortname);
	for (adversary in g.encounters[encounterName].adversaries) {
		if (shortname == stringutil.lowerCase(adversary)) return adversary;
	}
	if (!partials) return null;
	for (adversary in g.encounters[encounterName].adversaries) {
		if (stringutil.lowerCase(adversary).startsWith(shortname)) return adversary;
	}
	return null;
}

// copy an entire encounter
function encounters_copy(g, encounterName, newEncounterName, locale) {
	if (g.encounters[newEncounterName] != null) return 'encounter-exists';
	// build the copy
	g.encounters[newEncounterName] = {
		title: g.encounters[encounterName].title,
		notes: (g.encounters[encounterName].notes + ' ' + slashcommands.localizedString(locale, 'encounter-copynote',[encounterName])).trim(),
		image: g.encounters[encounterName].image,
		modifier: g.encounters[encounterName].modifier,
		environment: g.encounters[encounterName].environment,
		events: [...g.encounters[encounterName].events],
		adversaries: Object.assign({}, g.encounters[encounterName].adversaries)
	};
	return '';
}

// given a base name, generate a unique name by appending letters or numbers
function generateUniqueName(g, encounterName, adv, tryNumbers) {
	if (!encounters_adversary_name_match(g, encounterName, adv, false)) return adv;
	if (tryNumbers) {
		for (let i=2; i<=99; i++) {
			if (!encounters_adversary_name_match(g, encounterName, adv + String(i), false)) return adv + String(i);
		}
	}
	for (let i=1; i<=26; i++) {
		if (!encounters_adversary_name_match(g, encounterName, adv + String.fromCharCode(96 + i), false)) return adv + String.fromCharCode(96 + i);
	}
	return null;
}

// merge another encounter's bits into this one
function encounters_combine(g, encounterName, source, locale) {
	if (g.encounters[encounterName].title == '' || g.encounters[encounterName].title == encounterName) {
		g.encounters[encounterName].title = g.encounters[source].title;
	}
	if (g.encounters[encounterName].notes == '') {
		g.encounters[encounterName].notes = g.encounters[source].notes;
	}
	if (g.encounters[encounterName].modifier.modifier == null || g.encounters[encounterName].modifier.modifier == 0) {
		g.encounters[encounterName].modifier = Object.assign({}, g.encounters[source].modifier);
	}
	if (g.encounters[encounterName].environment.type == null || g.encounters[encounterName].environment.level == 0) {
		g.encounters[encounterName].environment = Object.assign({}, g.encounters[source].environment);
	}
	g.encounters[encounterName].events = g.encounters[encounterName].events.concat(g.encounters[source].events);

	// merge adversaries handling duplicated names
	for (adv in g.encounters[source].adversaries) {
		let newname = generateUniqueName(g, encounterName, adv, false);
		if (!newname) return 'adversary-exists';
		g.encounters[encounterName].adversaries[newname] = Object.assign({},g.encounters[source].adversaries[adv]);
	}

	return '';
}

// generate adversaries into the current encounter using tables
function encounters_generate(g, encounterName, userID, hostUser, feattable, successtable, locale, f_torRollDice) {

	// roll once on the feattable
	let r = encounters_generate_once(g, encounterName, userID, hostUser, feattable, locale, f_torRollDice);
	// roll once per player-hero on the success table
	let playerHeroesCount = 0;
	for (let p in g.characters) {
		if (g.characters[p].active) playerHeroesCount++;
	}
	for (let i=0; i<playerHeroesCount; i++) {
		r += encounters_generate_once(g, encounterName, userID, hostUser, successtable, locale, f_torRollDice);
	}
	return r;
}

// do one roll on an encounter generation table and add a corresponding adversary or group of adversaries
function encounters_generate_once(g, encounterName, userID, hostUser, tableshortname, locale, f_torRollDice) {

	//console.log('encounters_generate_once(g, ' + encounterName +', '+ userID +', '+ hostUser +', '+ tableshortname +', '+ locale +', f_torRollDice);');

	// roll on the table
	let [error, range, code, text, finalroll, rollstrings] = lookuptables.tables_roll(tableshortname, locale, '', null, false, f_torRollDice);
	if (error != null) return error;
	if (code == null || code == 'x') return slashcommands.localizedString(locale, 'encounter-generate-badtable',[tableshortname]);
	let r = '__' + tableshortname + '__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
	// a semicolon means a randomized choice
	if (code.includes(';'))	{
		//console.log('randomizing amongst ' + code);
		let codes = code.split(';');
		code = codes[regdice.simpleRoll(codes.length)-1];
	}
	// an ampersand means multiple creatures
	let creatures = code.split('&');
	creatures.forEach(creature => {
		// if it starts with a number, that's a quantity, otherwise the quantity is 1
		let quantity = 1;
		if (creature.match(/\d+\w+/)) {
			// parse into quantity and creature
			let parse = creature.match(/(\d+)(\w+)/);
			//console.log('parsing ' + creature +' into '+ JSON.stringify(parse));
			quantity = Number(parse[1]);
			creature = parse[2];
		}
		// generate a base name adv
		let adversary = adversaries.adversaries_fetch(creature);
		let adv = '';
		adversary.name.split(' ').forEach(a => {
			adv += a.charAt(0).toLowerCase();
		});
		// avoid duplicates
		let newname = generateUniqueName(g, encounterName, adv, true);
		if (!newname) return slashcommands.localizedString(locale, 'adversary-exists', [adv]);
		// add the adversary
		let addr = encounters_add_adversary(g, encounterName, locale, userID, hostUser, creature, newname, quantity, false);
		// build a return indicating success
		if (isNaN(addr)) return slashcommands.localizedString(locale, addr, [adv]);
		r += slashcommands.localizedString(locale, 'encounter-adversaries-added', [encounterName, creature, newname, addr]) + '\n';
	});
	return r;
}

// clear something from an encounter
function encounters_clear(g, encounterName, type, shortname) {
	if (type == 'events') {
		g.encounters[encounterName].events = [];
		return 'encounter-cleared-events';
	}
	if (type == 'adversaries') {
		g.encounters[encounterName].adversaries = {};
		return 'encounter-cleared-adversaries';
	}
	// verify this shortname is valid
	let adversaryName = encounters_adversary_name_match(g, encounterName, shortname);
	if (adversaryName == null) return 'adversary-listnomatch';
	if (type == 'attacks') {
		g.encounters[encounterName].adversaries[adversaryName].attacks = [];
		return 'encounter-cleared-attacks';
	}
	if (type == 'abilities') {
		g.encounters[encounterName].adversaries[adversaryName].abilities = [];
		return 'encounter-cleared-abilities';
	}
}

// deleting an encounter
function encounters_delete(g, encounterName) {
	delete g.encounters[encounterName];
	return '';
}

// deleting an encounter's adversary
function encounters_delete_adversary(g, encounterName, adversaryName) {
	delete g.encounters[encounterName].adversaries[adversaryName];
	return '';
}

// add an attack to an adversary in an encounter
function encounters_attack(g, encounterName, adversaryName, name, dice, damage, injury, ranged, special, comment) {
	// validation
	if (!adversaries.adversaries_attack_validate(dice, damage, injury)) return 'adversary-badattack';

	// check for existing one to replace
	let r = '';
	g.encounters[encounterName].adversaries[adversaryName].attacks.forEach(attack => {
		if (stringutil.caseInsensitiveMatch(attack.name, name)) {
			attack.dice = dice;
			attack.damage = damage;
			attack.injury = injury;
			attack.ranged = ranged;
			attack.special = special;
			attack.comment = stringutil.mildSanitizeString(comment.trim());
			r = g.encounters[encounterName].adversaries[adversaryName].attacks.length;
		}
	});
	if (r != '') return r;

	// create a new one
	let newAttack = {
		'name': stringutil.mildSanitizeString(name),
		'dice': dice,
		'damage': damage,
		'injury': injury,
		'ranged': ranged,
		'special': special,
		'comment': stringutil.mildSanitizeString(comment.trim())
	};
	g.encounters[encounterName].adversaries[adversaryName].attacks.push(newAttack);
	return g.encounters[encounterName].adversaries[adversaryName].attacks.length;
}

// add an ability to an adversary in an encounter
function encounters_ability(g, encounterName, adversaryName, name, type, amount, comment) {
	// validation
	let error;
	[name, type, amount, comment, error] = adversaries.adversaries_ability_validate(name, type, amount, comment);
	if (error) return error;

	// check for existing one to replace
	let r = '';
	g.encounters[encounterName].adversaries[adversaryName].abilities.forEach(ability => {
		if (stringutil.caseInsensitiveMatch(ability.name, name)) {
			ability.type = type;
			ability.amount = amount;
			ability.comment = comment.trim();
			r = g.encounters[encounterName].adversaries[adversaryName].abilities.length;
		}
	});
	if (r != '') return r;

	// create a new one
	let newAbility = {
		'name': name,
		'type': type,
		'amount': amount,
		'comment': comment
	};
	g.encounters[encounterName].adversaries[adversaryName].abilities.push(newAbility);
	return g.encounters[encounterName].adversaries[adversaryName].abilities.length;
}

// do damage to an adversary (can be endurance or hrscore)
function encounters_adversary_damage(g, encounterName, adversaryName, stat, amount) {
	g.encounters[encounterName].adversaries[adversaryName][stat] -= amount;
	if (g.encounters[encounterName].adversaries[adversaryName][stat] < 0) {
		g.encounters[encounterName].adversaries[adversaryName][stat] = 0;
	}
}

// determine if an adversary has an ability
function encounters_adversary_has_ability(g, encounterName, adversaryName, aName) {
	aName = stringutil.lowerCase(aName);
	let found = false;
	g.encounters[encounterName].adversaries[adversaryName].abilities.forEach(ability => {
		if (stringutil.lowerCase(ability.name) == aName) found = true;
	});
	return found;
}

function adversaryOutOfCombat(adv) {
	return (adv.curendurance <= 0 || adv.wounds >= adv.might);
}

// calculate engagement including both old-style and new-style
function count_engagement(g, charID) {
	if (g.combat.active && g.combat.encounter != '') return engaged_adversaries(g, charID).length;
	return g.characters[charID].numEngaged;
}

// create a list of engaged adversaries for a given character
function engaged_adversaries(g, charID) {
	let c = [];
	if (g.combat.active && g.combat.encounter != '') {
		for (let a in g.encounters[g.combat.encounter].adversaries) {
			if (!adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[a]) && g.encounters[g.combat.encounter].adversaries[a].engagement.includes(Number(charID))) c.push(a);
		}
	}
	return c;
}

// handle the /foe command's adjustments
function adjust_adversary(g, adversary, cmd, type, points) {

	if (type == 'all') {
		['hate','endurance','wounds','knockback','pushback','weary'].forEach(t => {adjust_adversary(g, adversary, cmd, t, points)});
		return null;
	}

	// calculate a minimum and maximum value
	let max = 99, field = type;
	switch (type) {
	case 'hate':
		max = g.encounters[g.combat.encounter].adversaries[adversary].hrscore;
		field = 'curhrscore';
		break;
	case 'endurance':
		max = g.encounters[g.combat.encounter].adversaries[adversary].endurance;
		field = 'curendurance';
		break;
	case 'wounds':
		max = g.encounters[g.combat.encounter].adversaries[adversary].might;
		break;
	case 'pushback':
		max = 1;
		break;
	}

	// calculate the new value
	let newVal = g.encounters[g.combat.encounter].adversaries[adversary][field];
	if (cmd == 'clear') {
		if (type == 'hate' || type == 'endurance') newVal = max;
		else newVal = 0;
	} else if (cmd == 'half') {
		newVal = Math.floor(max / 2); // or Math.ceil?
	} else if (cmd == 'gain') {
		newVal += points;
	} else {
		newVal -= points;
	}
	// implement limits
	if (newVal < 0) newVal = 0;
	if (newVal > max) newVal = max;

	// set (pushback is boolean)
	if (type == 'pushback') newVal = (newVal > 0);
	g.encounters[g.combat.encounter].adversaries[adversary][field] = newVal;
	return newVal;
}

// generating a Nameless Thing as an adversary
function encounters_add_nameless(g, encounterName, locale, shortname, parry, features, size, banetype, f_torRollDice) {

	// someone somehow got here without a valid adversaries structure so catch that
	if (!g.encounters || !g.encounters[encounterName]) return ['encounter-notexists',null,null];
	if (!g.encounters[encounterName].adversaries) g.encounters[encounterName].adversaries = {};

	// check if this will make a duplicate shortname
	if (encounters_adversary_name_match(g, encounterName, shortname) != null)
		return ['adversary-exists',null,null];

	// do table rolls on these tables and save the results as well as the rollLines
	let rollLines = '', rollResults = {};
	const namelessTables = ['namelessname1','namelessname2','namelessname3','namelessdescription','namelessbefore','namelessseefirst','namelessrumour','namelessremembered','namelesscharacteristics'];
	namelessTables.forEach(t => {
		// roll on that table and save the result in rollResults[t] and append the roll line itself to rollLines
		let [error, range, code, text, finalroll, rollstrings] = lookuptables.tables_roll(t, locale, '', null, false, f_torRollDice);
		if (error != null) return [error,null,null];
		rollLines += '__' + t + '__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
		if (text.startsWith('...')) text = text.substring(3);
		rollResults[t] = text;
	});

	// parse the output of namelesscharacteristics: Attribute/Hate: 12, Armour: 5d, Endurance: 120, Might: 3, Combat Proficiency: 4, Fell Abilities: 5
	let characteristics = rollResults['namelesscharacteristics'].split(', ');
	characteristics = {
		'attribute': characteristics[0].split(': ')[1],
		'armour': characteristics[1].split(': ')[1],
		'endurance': characteristics[2].split(': ')[1],
		'might': characteristics[3].split(': ')[1],
		'combatproficiency': characteristics[4].split(': ')[1],
		'fellabilities': characteristics[5].split(': ')[1]
	};

	// build a new adversary as follows:
	g.encounters[encounterName].adversaries[shortname] = {
		'catalog': '',
		'name': rollResults['namelessname1'] + ' ' + rollResults['namelessname2'],
		'description': rollResults['namelessdescription'] + '. When first encountered, before you see it, you ' + rollResults['namelessbefore'] + ', and what you see first ' + rollResults['namelessseefirst'] + '. Rumors are told ' + rollResults['namelessname3'] + ' of it: ' + rollResults['namelessrumour'] + '. ' + rollResults['namelessremembered'],
		'image': '',
		'features': features,
		'attribute': characteristics.attribute,
		'hrtype': 'hate',
		'hrscore': characteristics.attribute,
		'might': characteristics.might,
		'endurance': characteristics.endurance,
		'parry': parry,
		'armour': characteristics.armour,
		'banetype': banetype,
		'size': size,
		'attacks': [],
		'abilities': [],
		'curendurance': 0,
		'curhrscore': 0,
		'knockback': 0,
		'pushback': false,
		'weary': 0,
		'wounds': 0,
		'protectionTests': [],
		'engagement': [],
		'actionsThisRound': 0,
		'hateSpent': 0,
		'tengwar': {}
	};
	g.encounters[encounterName].adversaries[shortname].curendurance = g.encounters[encounterName].adversaries[shortname].endurance;
	g.encounters[encounterName].adversaries[shortname].curhrscore = g.encounters[encounterName].adversaries[shortname].hrscore;

	// generate attacks: dice comes from namelesscharacteristics then roll twice on namelessattacks
	// note: the table only has three entries so the smart way would be to randomly pick one value and then do everything else on the table; however, if the table ever grows, this wouldn't work anymore, so the following code should keep working
	let attacks = [];
	for (let i=0; i<2; i++) {
		let attackName, error, range, code, text, finalroll, rollstrings;

		// roll on the table until we get something we haven't already used
		do {
			[error, range, code, text, finalroll, rollstrings] = lookuptables.tables_roll('namelessattacks', locale, '', null, false, f_torRollDice);
			if (error != null) return [error,null,null];
			attackName = text.split('; ')[0]
		} while (attacks.includes(attackName));
		attacks.push(attackName);
		// now add that roll line to the output
		rollLines += '__namelessattacks__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
		// parse roll: Crush (hooves, paws); damage = Attribute level; injury = 12; special damage = break shield
		text = text.split('; ');
		// figure out the damage
		let damage = text[1].split(' = ')[1];
		if (damage.includes('-')) damage = characteristics.attribute - Number(damage.split(' - ')[1]);
		else damage = characteristics.attribute;
		// build the attack
		g.encounters[encounterName].adversaries[shortname].attacks.push({
			'name': attackName,
			'dice': characteristics.combatproficiency,
			'damage': damage,
			'injury': text[2].split(' = ')[1],
			'ranged': false,
			'special': stringutil.wordWithoutSpaces(text[3].split(' = ')[1]),
			'comment': ''
		});
	}

	// generate fell abilities: how many comes from namelesscharacteristics and roll on namelessabilities 
	let abilities = [];
	for (let i=0; i<characteristics.fellabilities; i++) {
		let abilityName, error, range, code, text, finalroll, rollstrings;

		// roll on the table until we get something we haven't already used
		do {
			[error, range, code, text, finalroll, rollstrings] = lookuptables.tables_roll('namelessabilities', locale, '', null, false, f_torRollDice);
			if (error != null) return [error,null,null];
			abilityName = text.split(': ')[0]
		} while (abilities.includes(abilityName));
		abilities.push(abilityName);
		// now add that roll line to the output
		rollLines += '__namelessabilities__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
		// parse roll: Strike Fear: Spend 1 Hate to make all Player-heroes in sight gain a number of Shadow points (Dread) equal to the Might rating of the creature. Those who fail their Shadow test are daunted and cannot spend Hope for the rest of the fight. 
		text = text.split(': ');
		// infer a type and amount when possible
		let type = '', amount = '';
		if (stringutil.lowerCase(text[1]).includes('daunted')) type = 'daunted';
		if (stringutil.lowerCase(text[1]).includes(' shadow points')) {
			let t = stringutil.lowerCase(text[1]).split(' shadow points')[0];
			t = t.split(' ');
			if (!isNaN(t[t.length-1])) amount = t[t.length-1];
		}
		// build the ability
		g.encounters[encounterName].adversaries[shortname].abilities.push({
			'name': abilityName,
			'type': type,
			'amount': amount,
			'comment': text[1]
		});
	}

	return [null, rollLines, g.encounters[encounterName].adversaries[shortname].name];
}

// EXPORTS --------------------------------------------------------------------

module.exports = Object.assign({ encounters_create, encounters_list, encounters_show, encounters_edit, encounters_modifier, encounters_environment, encounters_add_event, encounters_adversary_name_match, encounters_add_adversary, encounters_copy, encounters_combine, encounters_clear, encounters_delete, encounters_delete_adversary, encounters_attack, encounters_ability, encounters_adversary_damage, encounters_adversary_has_ability, adversaryOutOfCombat, count_engagement, engaged_adversaries, adjust_adversary, encounters_add_nameless, encounters_generate });

