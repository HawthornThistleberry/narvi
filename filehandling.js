// FILE HANDLING --------------------------------------------------------------
const fs = require('fs');
var stringutil = require('./stringutil.js');

function writeFile(fileName, fileContents) {
	try {
		fs.writeFileSync(fileName, fileContents);
	}
	catch (err)	{
		return 0;
	}
	return 1;
}

function readFile(fileName) {
	try {
		return fs.readFileSync(fileName);
	}
	catch (err)	{
		return null;
	}
}

function logUsage(s) {
	fs.appendFile('usage.log', s, (err) => {
	  if (err) {
		console.error('Error appending to file:', err);
	  //} else {
		//console.log('Data appended successfully!');
	  }
	});
}

function readJSONFile(fileName) {
	return safeJSONparse(readFile(fileName));
}

function writeJSONFile(fileName, contents) {
	return writeFile(fileName, JSON.stringify(contents));
}

function fileLastModified(fileName) {

	return fs.statSync(fileName).mtime;
	//return fs.stat(fileName).mtime;
}

const saveStateFilename = "savedstate.json";

//function saveStateData(lastCmd, selectedGames, macros, languages, prefixes) {
function saveStateData(userConfig) {
	return writeFile(saveStateFilename, JSON.stringify(userConfig));
}

function safeJSONparse(json) {
	if (json == '' || json == null || typeof json == 'undefined') return {};
	return JSON.parse(json);
}

function loadStateData() {
	let data = readFile(saveStateFilename);
	if (data == null) {
		console.log('Error reading the state data file!');
		return null;
	}
	let loadData = JSON.parse(data);

	/*
	// data conversions here
	for (let u in loadData) {
		// remove any old-style (non-array) macros
		if (loadData[u].macros != null) {
			let oldMacros = [];
			for (m in loadData[u].macros) {
				if (!Array.isArray(loadData[u].macros[m])) oldMacros.push(m);
			}
			oldMacros.forEach(m => {
				delete loadData[u].macros[m];
			});
		}
	}
	*/
	return loadData;
}

const gameDataFilename = "gamedata.json";

// function to save the game data to a JSON file
function saveGameData(gameData) {
	return writeFile(gameDataFilename, JSON.stringify(gameData));
}

// find the first unused character ID
function generateUniqueCharacterId(gameData) {
	let highestID = 1;
	for (let g in gameData) {
		if (gameData[g].characters != null) {
			for (let p in gameData[g].characters) {
				if (Number(p) >= highestID) highestID = Number(p) + 1;
			}
		}
	}
	return highestID;
}

function loadGameData() {
	let data = readFile(gameDataFilename);
	if (data == null) {
		console.log('Error reading the game data file!');
		return 0;
	}
	let gameData = JSON.parse(data);
	let changes = false;

	// any conversions or additions to data for this version go here
	for (let g in gameData) {

	/*
		// updates for all games
		if (gameData[g].combat.log == undefined) {
			gameData[g].combat.log = '';
			changes = true;
		}

		// updates for all encounters
		for (let e in gameData[g].encounters) {
			for (let a in gameData[g].encounters[e].adversaries) {
				if (gameData[g].encounters[e].adversaries[a].standBack == undefined) {
					gameData[g].encounters[e].adversaries[a].standBack = false;
					changes = true;
				}
				gameData[g].encounters[e].adversaries[a].attacks.forEach(att => {
					if (att.ranged == undefined) {
						att.ranged = false;
						changes = true;
						['spear','bow','knife','web'].forEach(w => {
							if (att.name.toLowerCase().includes(w)) att.ranged = true;
						});
					}
				});
			}
		}

		// updates for all players
		for (let p in gameData[g].players) {
			if (gameData[g].players[p].blackrootKey != null) {
				gameData[g].players[p].repositoryKey = gameData[g].players[p].blackrootKey;
				delete gameData[g].players[p].blackrootKey;
				changes = true;
			}
		}

		// updates for all characters
		for (let p in gameData[g].characters) {
			if (gameData[g].characters[p].bitten == undefined) {
				gameData[g].characters[p].bitten = false;
				changes = true;
			}
		}
	*/
	}

	if (changes) saveGameData(gameData);
	return gameData;
}

module.exports = Object.assign({ writeFile, readFile, readJSONFile, writeJSONFile, saveStateData, loadStateData, saveGameData, loadGameData, generateUniqueCharacterId, fileLastModified, logUsage });

