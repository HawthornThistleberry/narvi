const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const mapping = require('./googlesheetsmapping.json');
const stringutil = require('./stringutil.js');
const filehandling = require('./filehandling.js');
const apikey = require('./apikey.json');

const API_DELAY = 2500; // ms between queued requests

const CACHE_FILENAME = 'googleCache.json';

// INITIALIZATION -------------------------------------------------------------

let repositoryCache = {};
var sendDebugInfo, debugChannelID, sendUserMessage;
var pendingAPIrefreshes = [];
var autoRetries = 0;
var preloadingCache = [];

function initiate(gameData, notifyHostUser, botSendMessage, channelID) {
	sendDebugInfo = notifyHostUser;
	sendUserMessage = botSendMessage;
	debugChannelID = channelID;
}

function writeCache() {
	return filehandling.writeJSONFile(CACHE_FILENAME, repositoryCache);
}

// AUTHORIZATION --------------------------------------------------------------

// use the service account to authorize access, then call the callback
// used for both reads and writes
function authorizeGoogleAPI(callback, params) {
	// code from https://isd-soft.com/tech_blog/accessing-google-apis-using-service-account-node-js/
	// configure a JWT auth client
	let jwtClient = new google.auth.JWT(
		apikey.client_email,
		null,
		apikey.private_key,
		['https://www.googleapis.com/auth/spreadsheets']
	);
	// authenticate request
	jwtClient.authorize(function (err, tokens) {
		if (err) {
			console.log('Error authenticating via JWT to Google Sheets: ' + err);
			return;
		} else {
			callback(jwtClient, params);
		}
	});
}

// sometimes Google sends a clean error message and sometimes it sends a huge pile of HTML from which the error can be scraped
function cleanGoogleErrorHTML(err) {
	if (!String(err).includes('<!DOCTYPE html>') || !String(err).includes('<ins>')) return err;
	let a = String(err).split('<ins>')[1];
	a = a.split('\n')[1];
	if (a.includes('<ins>')) a = a.split('<ins>')[0];
	a = a.trim();
	if (a.startsWith('<p>')) a = a.slice(3);
	return a;
}

// CHARACTER LOADING ----------------------------------------------------------

// load a character into the cache (authorize, then use callback)
function loadCharacter(key, channelID, errorCallback) {
	authorizeGoogleAPI(loadCharacterCallback, [key, channelID, errorCallback]);
}

// the callback is fired when authorization is complete and does the actual read into the cache
function loadCharacterCallback(auth, params) {
	const sheets = google.sheets({version: 'v4', auth});
	const [key, channelID, errorCallback] = params;
	// remove from the queue of pending refreshes
	if (pendingAPIrefreshes.includes(key)) {
		pendingAPIrefreshes = pendingAPIrefreshes.filter(item => item !== key);
	}
	if (preloadingCache.includes(key)) {
		preloadingCache = preloadingCache.filter(item => item !== key);
	}
	sheets.spreadsheets.values.get({
		spreadsheetId: params[0],
		range: 'Character!A1:AD35',
	}, (err, res) => {
		if (err) {
			console.log('error loading cache for ' + key + ': ' + err);
			let retriesMsg = '';
			let suggestion = '';

			err = cleanGoogleErrorHTML(err);

			if (String(err).includes('Quota exceeded for quota metric') || String(err).includes('The service is currently unavailable.')) {
				// count autoreloads and abort if something has gone terribly wrong
				autoRetries++;
				if (autoRetries <= 20) {
					// otherwise queue another try, and make it also reduce autoRetries
					console.log('queueing a retry on ' + key + ' making ' + String(autoRetries) + ' retries');
					setTimeout(function(){ 
						loadCharacter(key, channelID, errorCallback);
						autoRetries--;
					}, API_DELAY);
					return;
				} else { // allow the failure and do not retry, but warn about it
					retriesMsg = ' after ' + String(autoRetries) + ' failed retries (aborting retries)';
					suggestion = ' and try again after about ten seconds';
				}
			}
			if (!String(err).includes('Internal error encountered') && !String(err).includes('Resource has been exhausted') && !String(err).includes('The caller does not have permission')) sendDebugInfo('**Google Sheets**: The API returned an error: ' + err + ' while trying to load character ' + key + ' (' + autoRetries + ' retries so far).', false);

			if (String(err).includes('Requested entity was not found')) {
				suggestion = ' and make sure your key is the part of the URL between the `/d/` and the following slash (or just use the whole URL),  and make sure you have set the permissions to allow anyone on the Internet to view the sheet';
			}
			if (String(err).includes('The caller does not have permission')) {
				suggestion = ' and make sure you have set the permissions to allow anyone on the Internet to view the sheet';
			}
			if (String(err).includes('Unable to parse range: Character')) {
				suggestion = ' and make sure this is actually a Narvi-compatible Google Sheets TOR character sheet';
			}
			if (!String(err).includes('Internal error encountered') && !String(err).includes('Resource has been exhausted')) sendUserMessage(channelID, '**Google Sheets**: The API returned an error: **' + err + '** while trying to load character `' + key + '`' + retriesMsg + '.\nReview the [instructions](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md#markdown-header-about-google-sheets)' + suggestion + '.\nThen try `/character key googlesheets <key>` or `/character refresh` to reload.',[]);
			if (errorCallback && typeof errorCallback == 'function') errorCallback('googlesheets', key, err);
			return false;
		}
		if (res.data.values.length == 0) {
			sendDebugInfo('Google Sheets: No data found while trying to load character ' + key, false);
			return false;
		}
		repositoryCache[key] = res.data.values;
		writeCache();
		//console.log('loaded cache for character at ' + key + ' (' + cacheDataFetch(key, 'name', '') + '), length ' + res.data.values.length);
		return true;
	});
}

// front end to loadCharacter that skips it if the key is not already known (for push update use)
function updateCharacter(key, channelID, errorCallback) {
	if (repositoryCache[key] != null) return loadCharacter(key, channelID, errorCallback);
	return false;
}

// on waking up, load the cache with all characters
function preloadCache(gameData, channelID, errorCallback) {
	// load the disk cache first if it's present
	console.log('loading the local Google Sheets cache...');
	repositoryCache = filehandling.readJSONFile(CACHE_FILENAME);
	console.log(Object.keys(repositoryCache).length + ' keys loaded');
	console.log('queueing up loading the Google Sheets cache');
	let timeout = 0;
	for (let g in gameData) {
		for (let c in gameData[g].characters) {
			if (gameData[g].characters[c] && gameData[g].characters[c].repositoryKey != '' && gameData[g].characters[c].keyType == 'googlesheets') {
				// if not already loading, and not already loaded...
				if (!preloadingCache.includes(gameData[g].characters[c].repositoryKey && repositoryCache[gameData[g].characters[c].repositoryKey] != null)) {
					// load it
					preloadingCache.push(gameData[g].characters[c].repositoryKey);
					setTimeout(function(){ loadCharacter(gameData[g].characters[c].repositoryKey, channelID, errorCallback); }, timeout);
					timeout += API_DELAY;
				}
			}
		}
	}
}

// CHARACTER SHEET READ FROM CACHE --------------------------------------------

// a safe check into the array
function dataRowCol(data, row, col) {
	if (row >= data.length) return undefined;
	if (col >= data[row].length) return undefined;
	return data[row][col];
}

// pull data from the API -- this has to remap from Narvi's data structure names to the repository's
function cacheDataFetch(key, field, subfield) {
	if (key == null || key == '') return null;
	let data = repositoryCache[key];
	if (data == null && preloadingCache.includes(key) && field == 'name') return 'Still loading, please wait...';
	if (data == null) return null;
	field = stringutil.lowerCase(field);
	subfield = stringutil.lowerCase(subfield);
	if (field == 'weapons') return weaponLookup(key, subfield);
	if (subfield == 'shield') return armourTotals(data)[3];
	if (subfield == 'armour') return armourTotals(data);
	if (subfield == 'helm') return armourHelm(data);
	if (field == 'armourqualities') return armourQualities(data);
	var fName = subfield ? subfield : field;
	if (skillList.includes(fName)) return skillValue(key, fName);
	if (['axes','bows','spears','swords','brawling'].includes(fName)) return proficiencyValue(data, fName);
	if (mapping[fName] == undefined) fName = field;
	if (mapping[fName] == undefined) return null;
	let [row,col,updatable] = mapping[fName];
	if (dataRowCol(data, row, col) == '#NAME?') {
		data[row][col] = manualCalculation(key, fName);
	}
	return dataRowCol(data, row, col);
}

function numberOrZero(num) {
	if (isNaN(num)) return 0;
	return Number(num);
}

// Sometimes a complex Google Sheets formula won't be up to date when Narvi asks for its value
// and so it returns #NAME? instead. When this happens, the best way around it, clumsy as it is,
// is to replicate the calculation here. This is a terrible solution (what if the formula has
// changed?) but there is no API way to force the calculation or wait for it.
function manualCalculation(key, fName) {
	let r = 0;
	if (fName == 'encumbrance') {
		// weapons
		let redoubtable = (stringutil.lowerCase(cacheDataFetch(key, 'cultureblessing','')) == 'redoubtable');
		for (let i = 1; i <= 4; i++) {
			if (cacheDataFetch(key, 'weapon' + i + 'equipped','') == 'TRUE')
				r += numberOrZero(cacheDataFetch(key, 'weapon' + i + 'enc',''));
		}
		// armor
		for (let i = 1; i <= 4; i++) {
			if (cacheDataFetch(key, 'armour' + i + 'equipped','') == 'TRUE') {
				let enc = numberOrZero(cacheDataFetch(key, 'armour' + i + 'enc',''));
				if (redoubtable && ['buckler','shield','great shield'].includes(stringutil.lowerCase(cacheDataFetch(key, 'armour' + i + 'name','')))) enc = Math.ceil(enc/2);
				r += enc;
			}
		}
		// account for marvellous items
		for (let i = 1; i <= 8; i++) {
			if (cacheDataFetch(key, 'marvellous' + i + 'name','') != '') r++;
		}
		// carried treasure
		r += numberOrZero(cacheDataFetch(key, 'carriedtreasure',''));
	}
	if (fName == 'load') {
		return numberOrZero(cacheDataFetch(key, 'fatigue','')) + numberOrZero(cacheDataFetch(key, 'encumbrance',''));
	}
	return r;
}

// extract the skill from the various fields that add up to it
const skillList = ['awe','enhearten','persuade','athletics','travel','stealth','awareness',
				   'insight','scan','hunting','healing','explore','song','courtesy',
				   'riddle','craft','battle','lore'];
function skillValue(key, skill) {
	let data = repositoryCache[key];
	if (data == null) return null;
	if (mapping['skill' + skill] == undefined) return null;
	let [row,col,updatable] = mapping['skill' + skill];
	let val = 0;
	for (let offset=2; offset <= 7; offset++) {
		if (data[row][col+offset] == 'TRUE') val++;
	}
	return String(val) + ((dataRowCol(data, row, col) == 'TRUE') ? 'f' : '');
}

function weaponNameCompare(w1, w2, fullmatch) {
	w1 = stringutil.lowerCase(stringutil.wordWithoutSpaces(w1));
	w2 = stringutil.lowerCase(stringutil.wordWithoutSpaces(w2));
	if (w1 == w2) return true;
	if (!fullmatch && w1.includes(w2)) return true;
	return false;
}

// returns a weapons structure similar to that in Narvi 1e, but with extra array elements; if subfield is provided, it's an array with just that one weapon, otherwise it's the whole bloody mess
function weaponLookup(key, subfield) {
	let data = repositoryCache[key];
	if (data == null) return null;
	let row, col,updatable;

	// if there's no weapon name, return the entire structure
	if (subfield == null || subfield == '') {
		let r = {};
		for (let w = 1; w <= 4; w++) {
			[row, col, updatable] = mapping['weapon' + w + 'name'];
			if (dataRowCol(data, row, col) != '') {
				r[dataRowCol(data, row, col)] = weaponArray(data, w);
			}
		}
		return r;
	}

	// we are returning the array of values for a single named weapon
	for (let w = 1; w <= 4; w++) {
		[row, col, updatable] = mapping['weapon' + w + 'name'];
		if (weaponNameCompare(dataRowCol(data, row, col), subfield, true)) {
			return weaponArray(data, w);
		}
	}
	// if it wasn't found for full name matches, try partials
	for (let w = 1; w <= 4; w++) {
		[row, col, updatable] = mapping['weapon' + w + 'name'];
		if (weaponNameCompare(dataRowCol(data, row, col), subfield, false)) {
			return weaponArray(data, w);
		}
	}
	return null; // if it was never found
}

// build the array for a weapon
const weaponArrayElements = ['dmg', 'edge', 'injury', 'equipped', 'proficiency', 'notes', 'enc', 'handedness', 'melee', 'ranged', 'origin', 'quality1', 'quality2', 'quality3', 'bane1', 'bane2', 'curse', 'name'];
const proficiencies = ['axes','bows','spears','swords'];
function weaponArray(data, weaponNum) {
	let r = [0]; // save one spot for pips
	let row, col, updatable;

	// build out the rest of the array
	weaponArrayElements.forEach(element => {
		[row, col, updatable] = mapping['weapon' + weaponNum + element];
		r.push(dataRowCol(data, row, col));
	});

	// look up the pips for the corresponding proficiency (also account for Brawling!) and store it in r[0]
	if (stringutil.lowerCase(r[5]) == 'brawling') {
		let highest = 1;
		proficiencies.forEach(prof => {
			let pips = proficiencyValue(data, prof);
			if (pips > highest) highest = pips;
		});
		r[0] = highest - 1;
	} else if (proficiencies.includes(stringutil.lowerCase(r[5]))) {
		r[0] = proficiencyValue(data, stringutil.lowerCase(r[5]));
	}

	// return the array
	return r;
}

// given a proficiency, return the number of pips
function proficiencyValue(data, proficiency) {
	if (proficiency == 'brawling') {
		let highest = 1;
		proficiencies.forEach(prof => {
			let pips = proficiencyValue(data, prof);
			if (pips > highest) highest = pips;
		});
		return highest - 1;
	}
	let row, col,updatable;
	[row, col,updatable] = mapping['proficiency' + proficiency];
	let val = 0;
	for (let offset=0; offset <= 5; offset++) {
		if (data[row][col+offset] == 'TRUE') val++;
	}
	return val;
}

// tally up armor totals;  returns [nonshield protection, nonshield bonus, shield protection, shield bonus]
function armourTotals(data) {
	let r = [0,0,0,0];
	let row, col, updatable;
	for (let i=1; i<=4; i++) {
		[row, col,updatable] = mapping['armour' + i + 'equipped'];
		if (dataRowCol(data, row, col) == 'FALSE') continue; // unequipped things don't protect!
		[row, col,updatable] = mapping['armour' + i + 'name'];
		let name = stringutil.lowerCase(dataRowCol(data, row, col));
		let shield = ((name.includes('shield') || name.includes('buckler')) ? 2 : 0);
		[row, col,updatable] = mapping['armour' + i + 'protect'];
		if (!isNaN(dataRowCol(data, row, col))) r[shield] += Number(dataRowCol(data, row, col));
		[row, col,updatable] = mapping['armour' + i + 'bonus'];
		if (!isNaN(dataRowCol(data, row, col))) r[shield+1] += Number(dataRowCol(data, row, col));
	}
	return r;
}

// checks for a helm and if found returns its protection and bonus
function armourHelm(data) {
	let r = [0,0];
	let row, col, updatable;
	for (let i=1; i<=4; i++) {
		[row, col,updatable] = mapping['armour' + i + 'equipped'];
		if (dataRowCol(data, row, col) == 'FALSE') continue; // unequipped things don't protect!
		[row, col,updatable] = mapping['armour' + i + 'name'];
		let name = stringutil.lowerCase(dataRowCol(data, row, col));
		if (name.includes('helm')) {
			[row, col,updatable] = mapping['armour' + i + 'protect'];
			if (!isNaN(dataRowCol(data, row, col))) r[0] += Number(dataRowCol(data, row, col));
			[row, col,updatable] = mapping['armour' + i + 'bonus'];
			if (!isNaN(dataRowCol(data, row, col))) r[1] += Number(dataRowCol(data, row, col));
		}
	}
	return r;
}

// tally up armor qualities
function armourQualities(data) {
	let r = {};
	let row, col, updatable;
	for (let i=1; i<=4; i++) {
		[row, col, updatable] = mapping['armour' + i + 'name'];
		let name = dataRowCol(data, row, col);
		[row, col, updatable] = mapping['armour' + i + 'equipped'];
		if (dataRowCol(data, row, col) == 'FALSE') continue; // unequipped things don't protect!
		let q = [];
		for (let j = 1; j <= 3; j++) {
			[row, col, updatable] = mapping['armour' + i + 'quality' + j];
			let quality = dataRowCol(data, row, col);
			if (quality && quality != '') q.push(stringutil.lowerCase(quality));
		}
		if (q.length > 0) r[name] = q;
	}
	return r;
}

// return true or false to indicate if the character has a particular trait (feature, reward, lifepath, or virtue)
function characterHasTrait(key, traittype, value, substring) {
	// substring can be 'includes' or 'startsWith'
	let data = repositoryCache[key];
	if (data == null) return false;
	value = stringutil.lowerCase(value);
	if (value.startsWith('the ')) value = value.substring(4);
	value = stringutil.lowerCase(stringutil.wordWithoutSpaces(value)).replace(/-/g, '');
	substring = stringutil.lowerCase(stringutil.wordWithoutSpaces(substring)).replace(/-/g, '');
	let row, col, updatable, i;
	if (traittype == 'lifepath') i = 1;
	else if (traittype == 'feature') i = 3;
	else i = 8;
	for (; i >= 1; i--) {
		[row, col,updatable] = mapping[traittype + (traittype == 'lifepath' ? '' : i)];
		let s = stringutil.lowerCase(dataRowCol(data, row, col));
		if (s.startsWith('the ')) s = s.substring(4);
		s = stringutil.lowerCase(stringutil.wordWithoutSpaces(s)).replace(/-/g, '');
		if (s != '') {
			if (value == s) return true;
			if (substring == 'includes' && s.includes(value)) return true;
			if (substring == 'startswith' && s.startsWith(value)) return true;
		}
	}
	return false;
}

// returns the text of a useful item that matches the skill, or '' if none do
function characterHasUsefulItem(key, skill) {
	let data = repositoryCache[key];
	if (data == null) return false;
	skill = stringutil.lowerCase(skill);
	let row, col, updatable;
	for (let i = 1; i <= 4; i++) {
		[row, col,updatable] = mapping['usefulitem' + i + 'skill'];
		if (data[row] && dataRowCol(data, row, col) && skill == stringutil.lowerCase(dataRowCol(data, row, col))) {
			[row, col,updatable] = mapping['usefulitem' + i];
			return dataRowCol(data, row, col);
		}
	}
	return '';
}

// returns the text of an item with a blessing that matches the skill, or '' if none do
function characterHasBlessing(key, skill) {
	let data = repositoryCache[key];
	if (data == null) return false;
	skill = stringutil.lowerCase(skill);
	let row, col, updatable;
	for (let i = 1; i <= 8; i++) {
		for (let j = 1; j <= 2; j++) {
			[row, col, updatable] = mapping['marvellous' + i + 'blessing' + j];
			if (data[row] && dataRowCol(data, row, col) && skill == stringutil.lowerCase(dataRowCol(data, row, col))) {
				[row, col, updatable] = mapping['marvellous' + i + 'name'];
				return dataRowCol(data, row, col);
			}
		}
	}
	return '';
}

// return true or false to indicate if the character has a particular quality (only if active) or curse (always)
function characterHasQuality(key, type, value, substring, armorOnly) {
	// substring can be 'includes' or 'startsWith'
	let data = repositoryCache[key];
	if (data == null) return false;
	value = stringutil.lowerCase(value);
	if (value.startsWith('the ')) value = value.substring(4);
	substring = stringutil.lowerCase(substring);
	let row, col, updatable;

	// make a list of relevant fields to check
	let toCheck = [];
	for (let i = 1; i <= 4; i++) {
		if (type == 'curse') {
			if (!armorOnly) toCheck.push('weapon' + i + 'curse');
			toCheck.push('armour' + i + 'curse');
		} else if (type == 'quality') {
			for (let j = 1; j <= 3; j++) {
				if (!armorOnly && cacheDataFetch(key, 'weapon' + i + 'equipped','') == 'TRUE') toCheck.push('weapon' + i + 'quality' + j);
				if (cacheDataFetch(key, 'armour' + i + 'equipped','') == 'TRUE') toCheck.push('armour' + i + 'quality' + j);
			}
		}
	}

	// check them
	let s, found = false;
	toCheck.forEach(x => {
		[row, col, updatable] = mapping[x];
		s = dataRowCol(data, row, col);
		if (s) {
			s = stringutil.lowerCase(s);
			if (s.startsWith('the ')) s = s.substring(4);
			if (value == s) found = true;
			if (substring == 'includes' && s.includes(value)) found = true;
			if (substring == 'startswith' && s.startsWith(value)) found = true;
		}
	});
	return found;
}

// if the character has a mithril buckler, return its protection, else 0
function characterHasMithrilBuckler(key) {
	let data = repositoryCache[key];
	if (data == null) return 0;
	let r = 0;
	let row, col, updatable;
	for (let i = 1; i <= 4; i++) {
		let n = cacheDataFetch(key, 'armour' + i + 'name','');
		if (n == null) n = '';
		if (cacheDataFetch(key, 'armour' + i + 'equipped','') == 'TRUE' && n.toLowerCase().includes('buckler')) {
			// check all qualities for mithril shield, if found, add the protection to r
			let mithril = false
			for (let j = 1; j <= 3; j++) {
				let q = cacheDataFetch(key, 'armour' + i + 'quality' + j);
				if (q == null) q = '';
				if (q.toLowerCase() == 'mithril shield') mithril = true;
			}
			if (mithril) r += Number(cacheDataFetch(key, 'armour' + i + 'protect',''));
		}
	}
	return r;
}



// CHARACTER UPDATES ----------------------------------------------------------

// let the calling code attempt to do the update (still says false if it can't)
function cacheDataUpdate(key, field, subfield, val, channelID) {
	return cacheDataTestOrUpdate(key, field, subfield, val, false, false, channelID);
}

// let the calling code determine if a field is updateable without attempting to update it
function cacheDataUpdatable(key, field, subfield) {
	return cacheDataTestOrUpdate(key, field, subfield, 0, true, true, debugChannelID);
}

// main function both for testing whether a field is updatable, and actually updating it
function cacheDataTestOrUpdate(key, field, subfield, val, testonly, norefresh, channelID) {
	let data = repositoryCache[key];
	if (data == null) return false;
	field = stringutil.lowerCase(field);
	subfield = stringutil.lowerCase(subfield);
	if (field == 'weapons') return false;
	if (['shield','armour','headgear','fbody','fwits','fheart','damage','ranged','body'].includes(subfield)) return false;
	var fName = subfield ? subfield : field;
	if (fName == 'search') fName = 'scan';
	if (fName == 'inspire') fName = 'enhearten';
	if (skillList.includes(fName)) return false;
	if (['axes','bows','spears','swords'].includes(fName)) return false;
	if (mapping[fName] == undefined) fName = field;
	if (mapping[fName] == undefined) return false;
	let [row,col,updatable] = mapping[fName];

	// if this is not updateable, we're done either way
	if (!updatable) return false;

	// if it is updatable but all we were doing is testing, we're done
	if (testonly) return true;

	// update my cache
	data[row][col] = val;

	// we're about to ask the spreadsheet for an update in two seconds, so we are guaranteed to get the right values that change as a result of this change, but until then, let's make a reasonably good guess what changes those are, so they'll be reflected correctly
	if (['endurance','maxendurance','fatigue','encumbrance','load','availendurance','weary'].includes(fName)) cacheUpdateRecalculateEndurance(key);
	if (['hope','maxhope','shadowscars','shadow','totalshadow','availhope','miserable'].includes(fName)) cacheUpdateRecalculateHope(key);
	if (fName == 'skillpointsavailable') cacheUpdateRecalculateSkillPoints(key);
	if (fName == 'adventurepointsavailable') cacheUpdateRecalculateAdventurePoints(key);
	if (fName == 'treasure' || fName == 'carriedtreasure') cacheUpdateRecalculateTreasure(key, channelID);

	// now send the update up to Google Sheets
	authorizeGoogleAPI(updateCharacterCallback, [key, row, col, val, channelID]);

	writeCache();

	// see if a refresh is needed, and not already queued
	if (!norefresh && !pendingAPIrefreshes.includes(key)) {
		setTimeout(function(){ updateCharacter(key, debugChannelID, null) }, 2000);
		pendingAPIrefreshes.push(key); // this ensures a series of quick changes will only result in one refresh
	}

	return;
}

// the callback from the update above, that does the actual update after authorization
function updateCharacterCallback(auth, params) {
	const sheets = google.sheets({version: 'v4', auth});
	const [key, row, col, value, channelID] = params;
	let values = [
	  [
		value
	  ]
	];
	const resource = {
	  values,
	};
	sheets.spreadsheets.values.update({
		spreadsheetId: params[0],
		range: 'Character!' + getA1Notation(row,col),
		valueInputOption: 'USER_ENTERED',
		resource,
	}, (err, res) => {
		if (err) {
			err = cleanGoogleErrorHTML(err);
			let suggestion = '';
			if (String(err).includes('Quota exceeded for quota metric') || String(err).includes('temporary error') || String(err).includes('connect ETIMEDOUT') || String(err).includes('The service is currently unavailable.')) {
				// count autoreloads and abort if something has gone terribly wrong
				autoRetries++;
				if (autoRetries <= 20) {
					// otherwise queue another try, and make it also reduce autoRetries
					console.log('queueing a retry on ' + key + ' making ' + String(autoRetries) + ' retries');
					setTimeout(function(){ 
						authorizeGoogleAPI(updateCharacterCallback, params);
						autoRetries--;
					}, API_DELAY);
					return;
				} else { // allow the failure and do not retry, but warn about it
					retriesMsg = ' after ' + String(autoRetries) + ' failed retries (aborting retries)';
					suggestion = ' and try again after about ten seconds';
				}
			}
			if (!String(err).includes('The caller does not have permission')) sendDebugInfo('Google Sheets: The API returned an error: ' + err + ' while trying to update character ' + key, false);
			if (String(err).includes('Requested entity was not found')) {
				suggestion = ' and make sure your key is the part of the URL between the `/d/` and the following slash.';
			}
			if (String(err).includes('The caller does not have permission')) {
				suggestion = ' and make sure you have shared the sheet with the account name listed there as Editor.';
			}
			sendUserMessage(channelID, '**Google Sheets**: The API returned an error: **' + err + '** while trying to update character `' + key + '` (' + cacheDataFetch(key, 'name', null) + ').\nReview the [instructions](https://bitbucket.org/HawthornThistleberry/narvi/src/master/README.md#markdown-header-about-google-sheets)' + suggestion + '\nThen try repeating the update you just made.',[]);
			return false;
		}
		return true;
	});
}

// this function written by Amit Agarwal comes from https://www.labnol.org/convert-column-a1-notation-210601
// given a row,col gives the correct A1-form address
const getA1Notation = (row, column) => {
	const a1Notation = [`${row + 1}`];
	const totalAlphabets = "Z".charCodeAt() - "A".charCodeAt() + 1;
	let block = column;
	while (block >= 0) {
		a1Notation.unshift(
			String.fromCharCode((block % totalAlphabets) + "A".charCodeAt())
		);
		block = Math.floor(block / totalAlphabets) - 1;
	}
	return a1Notation.join("");
};

// internal use only, used in Recalculates, assumes valid key and field name
function cacheQuickRead(key, field) {
	let data = repositoryCache[key];
	let [row,col,updatable] = mapping[field];
	return dataRowCol(data, row, col);
}

// internal use only, used in Recalculates, assumes valid key and field name
function cacheQuickSet(key, field, val) {
	let data = repositoryCache[key];
	let [row,col,updatable] = mapping[field];
	data[row][col] = val;
}

// the refresh should provide updates but until it does, here's a quick update to endurance whenever relevant values change
function cacheUpdateRecalculateEndurance(key) {
	cacheQuickSet(key, 'load', Number(cacheQuickRead(key, 'fatigue')) + Number(cacheQuickRead(key, 'encumbrance')));
	cacheQuickSet(key, 'availendurance', Number(cacheQuickRead(key, 'endurance')) - Number(cacheQuickRead(key, 'load')));
	cacheQuickSet(key, 'weary', Number(cacheQuickRead(key, 'availendurance')) <= 0);
}

// the refresh should provide updates but until it does, here's a quick update to hope whenever relevant values change
function cacheUpdateRecalculateHope(key) {
	cacheQuickSet(key, 'totalshadow', Number(cacheQuickRead(key, 'shadowscars')) + Number(cacheQuickRead(key, 'shadow')));
	cacheQuickSet(key, 'availhope', Number(cacheQuickRead(key, 'hope')) - Number(cacheQuickRead(key, 'totalshadow')));
	cacheQuickSet(key, 'miserable', Number(cacheQuickRead(key, 'availhope')) <= 0);
}

// the refresh should provide updates but until it does, here's a quick update to skill points whenever relevant values change
function cacheUpdateRecalculateSkillPoints(key) {
	cacheQuickSet(key, 'skillpointstotal', Number(cacheQuickRead(key, 'skillpointsspent')) + Number(cacheQuickRead(key, 'skillpointsavailable')));
}

// the refresh should provide updates but until it does, here's a quick update to adventure points whenever relevant values change
function cacheUpdateRecalculateAdventurePoints(key) {
	cacheQuickSet(key, 'adventurepointstotal', Number(cacheQuickRead(key, 'adventurepointsspent')) + Number(cacheQuickRead(key, 'adventurepointsavailable')));
}

// unfortunately the Google Sheet will no longer fire the script that updates standard of living, so /company treasure does not change it, so now we have to do that both in our local cache and in the sheet
function cacheUpdateRecalculateTreasure(key, channelID) {
	let treasure = Number(cacheQuickRead(key, 'treasure')) + Number(cacheQuickRead(key, 'carriedtreasure'));
	let standardofliving = 'Poor';
	if (treasure >= 300) standardofliving = 'Very Rich';
	else if (treasure >= 180) standardofliving = 'Rich';
	else if (treasure >= 90) standardofliving = 'Prosperous';
	else if (treasure >= 30) standardofliving = 'Common';
	else standardofliving = 'Frugal';
	cacheQuickSet(key, 'standardofliving', standardofliving);
	cacheDataTestOrUpdate(key, 'standardofliving', null, standardofliving, false, true, channelID);
}

module.exports = Object.assign({ initiate, loadCharacter, updateCharacter, preloadCache, cacheDataFetch, characterHasTrait, characterHasUsefulItem, characterHasBlessing, characterHasQuality, characterHasMithrilBuckler, cacheDataUpdate, cacheDataUpdatable });
