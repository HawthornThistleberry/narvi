//------------------------------------------------------------------------------
// Hex Flower by Goblin's Henchman
// https://www.drivethrurpg.com/product/367072/Weather-Hex-Flower--Random-weather-generation--by-Goblins-Henchman
//

const filehandling = require('./filehandling.js');
var embeds = require('./embeds.js');
const hexFlowersFilename = 'hexflowers.json';
const stringutil = require('./stringutil.js');
var table = require('./table.js');
var emoji = require('./emoji.js');
var slashcommands = require('./slashcommands.js');

var hexFlowers = {};

/*
hexFlowers: {
	<hexflower>: {
		name: '...',
		description: '...',
		creator: userID,
		createdate: today,
		published: false,
		hexes: [
			[<text>,<url>],
		]
	}
}
       __
    __/7 \__
 __/3 \__/12\__
/0 \__/8 \__/16\
\__/4 \__/13\__/
/1 \__/9 \__/17\
\__/5 \__/14\__/
/2 \__/10\__/18\
\__/6 \__/15\__/
   \__/11\__/
      \__/

moves are NE SE S SW NW N
*/

var hexMoves = [
	[3, 4, 1, 0, 18, 2],
	[4, 5, 2, 12, 15, 0],
	[5, 6, 0, 16, 11, 1],
	[7, 8, 4, 0, 17, 6],
	[8, 9, 5, 1, 0, 3],
	[9, 10, 6, 2, 1, 4],
	[10, 11, 3, 17, 2, 5],
	[7, 12, 8, 3, 7, 7],
	[12, 13, 9, 4, 3, 7],
	[13, 14, 10, 5, 4, 8],
	[14, 15, 11, 6, 5, 9],
	[15, 2, 11, 18, 6, 10],
	[1, 16, 13, 8, 7, 15],
	[16, 17, 14, 9, 8, 12],
	[17, 18, 15, 10, 9, 13],
	[18, 1, 12, 11, 10, 14],
	[2, 16, 17, 13, 12, 18],
	[6, 3, 18, 14, 13, 16],
	[11, 0, 16, 15, 14, 17]
];

// load the hexflower file on startup
function load() {
	hexFlowers = filehandling.readJSONFile(hexFlowersFilename);
	if (hexFlowers == null || Object.keys(hexFlowers).length === 0)	{
		hexFlowers = {};
		save();
	}
}

// save the hexflower file after each change
function save() {
	return filehandling.writeJSONFile(hexFlowersFilename, hexFlowers);
}

//	"create":["create","Create a hex flower",{
//		"shortname":["shortname","The short name for the hex flower to create",{}],
//		"description":["description","The description for the hex flower",{}]
function create(hexflowerName, userID, description) {
	hexflowerName = stringutil.lowerCase(hexflowerName);
	// if a hexflower already exists with that name, return false
	if (hexFlowers[hexflowerName] != null) return false;

	// create a hexflower with that name and fill in some basic info:
	hexFlowers[hexflowerName] = {
		name: hexflowerName,
		description: description,
		creator: userID,
		createdate: Date.now(),
		published: false,
		hexes: [['0',''],['1',''],['2',''],['3',''],['4',''],['5',''],['6',''],['7',''],['8',''],['9',''],
			    ['10',''],['11',''],['12',''],['13',''],['14',''],['15',''],['16',''],['17',''],['18','']]
	};
	save();
	return true;
}

//	"list":["list","List available hex flowers",{
//		"searchtext":["searchtext","Only list hex flowers  that contain the provided text",{}],
//		"mine":["mine","Only list hex flowers  that are (or are not, for False) yours",{}],
//		"published":["published","Only list hex flowers  that are (or are not, for False) published",{}]
function list(userID, hostUser, locale, mine, published, searchTerms) {
	let reportHeadings = [
		{title:stringutil.titleCase(slashcommands.localizedCommand(locale, 'hexflower')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedOption(locale, 'hexflower', 'create','description')), emoji:false, minWidth:0, align:'left'},
		{title:'Pub', emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	let matches = 0;
	for (let hexflowerName in hexFlowers) {
		let listIt = true;
		if (!accessible(hexflowerName, userID, hostUser, 'read')) listIt = false;
		if (mine === true && hexFlowers[hexflowerName].creator != userID) listIt = false;
		if (mine === false && hexFlowers[hexflowerName].creator == userID) listIt = false;
		if (published === true && !hexFlowers[hexflowerName].published) listIt = false;
		if (published === false && hexFlowers[hexflowerName].published) listIt = false;
		if (searchTerms) searchTerms.split(' ').forEach(term => {
			if (!search_term(hexflowerName,term)) listIt = false;
		});
		if (listIt) {
			reportData.push([
				hexflowerName, 
				hexFlowers[hexflowerName].description,
				hexFlowers[hexflowerName].published ? ':ledger:' : ''
			]);
			matches++;
		}
	}
	if (reportData.length == 0) return slashcommands.localizedString(locale, 'hexflower-listnomatch',[]);
	return table.buildTable(reportHeadings, reportData, 2) + '\n' + slashcommands.localizedString(locale,'hexflower-matched',[matches, Object.keys(hexFlowers).length]);
}

// check all the relevant fields of a hex flower for a search term
function search_term(hexflowerName, searchTerm) {
	searchTerm = stringutil.lowerCase(searchTerm);
	if (stringutil.lowerCase(hexflowerName).includes(searchTerm)) return true;
	if (stringutil.lowerCase(hexFlowers[hexflowerName].description).includes(searchTerm)) return true;
	let result = false;
	hexFlowers[hexflowerName].hexes.forEach(row => {
		if (stringutil.lowerCase(row[0]).includes(searchTerm)) result = true;
	});
	return result;
}

//	"select":["select","Select an existing hex flower to edit or roll on",{
//		"shortname":["shortname","The short name for the hex flower",{}]
function accessible(hexflowerName, userID, hostUser, type) {
	// if the hex flower doesn't exist, you can't access it
	if (hexFlowers[hexflowerName] == null) return false;
	// the host user can do everything to all hex flowers
	if (userID == hostUser) return true;
	// if the hex flower is owned by this user, they can do anything
	if (hexFlowers[hexflowerName].creator == userID) return true;
	// if we're asking for write access, and no earlier test passed, they can't do it
	if (type == 'write') return false;
	// so we're asking for read now, and it's not the user's own hex flower
	// otherwise it only depends on whether the hex flower is published, 
	if (hexFlowers[hexflowerName].published) return true;
	return false;
}

//	"hex":["hex","Set the text and image for a hex in the selected hex flower",{
//		"hex":["hex","Which of the hexes you're setting values for (0-18)",{}],
//		"text":["text","The label to be displayed for this hex",{}],
//		"image":["image","The URL of the image for this hex",{}]
function hex(hexflowerName, userID, hostUser, locale, hex, text, image) {
	if (!accessible(hexflowerName, userID, hostUser, 'write')) return 'hexflower-notexists';
	if (hex < 0 || hex > 18) return 'hexflower-badhex';

	// sanitize the code and text
	text = stringutil.mildSanitizeString(text);
	hexFlowers[hexflowerName].hexes[hex] = [text, image];
	save();
	return '';
}

//	"set": ["set","Set the current hex on the selected hex flower",{
//		"value":["value","The new hex (0-18)",{}]
//	"current": ["show","Show current hex on the selected hex flower",{}],
function showHex(flower, currentHex, gameName) {
	let title = stringutil.titleCaseWords(flower);
	if (gameName != undefined) title += ' in ' + gameName;
	title += ' [' + currentHex + ']';
	let theEmbed = embeds.buildEmbed(
		title,
		hexFlowers[flower].hexes[currentHex][1],
		'https://preview.drivethrurpg.com/en/product/295083/hex-flower-cookbook-an-overview-and-some-thoughts-on-hex-flower-game-engines-by-goblin-s-henchman',
		hexFlowers[flower].hexes[currentHex][0],
		'from Hex Flowers by Goblin\'s Henchman',
		null,
		null,
		null,
		gameName,
		embeds.BUTTON_GREYPLE,
		null
	);
	return {embed: theEmbed};
}

//	"next":["next","Roll on the selected hex flower and show the result",{}],
function nextHex(currentHex, roll) {
	let dirIndex;
	if (roll <= 3) dirIndex = 0;
	else if (roll <= 5) dirIndex = 1;
	else if (roll <= 7) dirIndex = 2;
	else if (roll <= 9) dirIndex = 3;
	else if (roll <= 11) dirIndex = 4;
	else dirIndex = 5;
	return hexMoves[currentHex][dirIndex];
}

//	"show":["show","Show an existing hex flower to edit or roll on",{
//		"shortname":["shortname","The short name for the hex flower (will use your selected if not provided)",{}]
function show(hexflowerName, locale, userID, hostUser, userNameFromID, position) {

	if (hexflowerName == '') return slashcommands.localizedString(locale,'hexflower-notselected',[]);

	let t = hexFlowers[hexflowerName];

	let r = '**' + t.description + '** (`' + hexflowerName + '`)' + (t.published ? ' :ledger:' : '') + '\n';
	r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedString(locale,'created',[])) + '__: <t:' + String(Math.floor(t.createdate/1000)) + ':F> by ' + userNameFromID(t.creator) + '\n';
	r += '```\n';
	r += '       __\n';
	r += '    __/7 \\__       0: ' + stringutil.trimPadRight(t.hexes[0][0],25) + '  10: ' + stringutil.trimPadRight(t.hexes[10][0],25) + '\n';
	r += showPawn(' __/3 \\__/12\\__    1: ' + stringutil.trimPadRight(t.hexes[1][0],25) + '  11: ' + stringutil.trimPadRight(t.hexes[11][0],25) + '\n', 7, position, 7);
	r += showPawn(showPawn('/0 \\__/8 \\__/16\\   2: ' + stringutil.trimPadRight(t.hexes[2][0],25) + '  12: ' + stringutil.trimPadRight(t.hexes[12][0],25) + '\n', 3, position, 4), 12, position, 10);
	r += showPawn(showPawn(showPawn('\\__/4 \\__/13\\__/   3: ' + stringutil.trimPadRight(t.hexes[3][0],25) + '  13: ' + stringutil.trimPadRight(t.hexes[13][0],25) + '\n',0, position, 1), 8, position, 7), 16, position, 13);
	r += showPawn(showPawn('/1 \\__/9 \\__/17\\   4: ' + stringutil.trimPadRight(t.hexes[4][0],25) + '  14: ' + stringutil.trimPadRight(t.hexes[14][0],25) + '\n',4, position, 4), 13, position, 10);
	r += showPawn(showPawn(showPawn('\\__/5 \\__/14\\__/   5: ' + stringutil.trimPadRight(t.hexes[5][0],25) + '  15: ' + stringutil.trimPadRight(t.hexes[15][0],25) + '\n',1,position,1), 9,position,7), 17,position,13);
	r += showPawn(showPawn('/2 \\__/10\\__/18\\   6: ' + stringutil.trimPadRight(t.hexes[6][0],25) + '  16: ' + stringutil.trimPadRight(t.hexes[16][0],25) + '\n',5,position,4),14,position,10);
	r += showPawn(showPawn(showPawn('\\__/6 \\__/15\\__/   7: ' + stringutil.trimPadRight(t.hexes[7][0],25) + '  17: ' + stringutil.trimPadRight(t.hexes[17][0],25) + '\n',2,position,1),10,position,7),18,position,13);
	r += showPawn(showPawn('   \\__/11\\__/      8: ' + stringutil.trimPadRight(t.hexes[8][0],25) + '  18: ' + stringutil.trimPadRight(t.hexes[18][0],25) + '\n',6,position,4),15,position,10);
	r += showPawn('      \\__/         9: ' + stringutil.trimPadRight(t.hexes[9][0],25) + '\n',11,position,7);
	r += '```';
	return r;
}

// inserts a @ pawn to indicate the current position, if this is the right place
function showPawn(s, ifvalue, actualvalue, position) {
	if (ifvalue != actualvalue) return s;
	return s.substring(0, position) + '@' + s.substring(position+1);
}

//	"publish":["publish","Should this hex flower be published so all other Narvi users can use it?",{
//		"value":["value","Select True to publish and False to unpublish",{}]
function publish(hexflowerName, locale, userID, hostUser, value) {
	if (!accessible(hexflowerName, userID, hostUser, 'write')) return slashcommands.localizedString(locale, 'hexflower-notexists', []);
	if (value) { // validate if setting true
		for (i = 0; i <= 18; i++) {
			let label = hexFlowers[hexflowerName].hexes[i][0];
			if (label == '' || label == String(i)) return slashcommands.localizedString(locale, 'hexflower-missing', [i]);
		}
	}
	hexFlowers[hexflowerName].published = value;
	save();
	return '';
}

//	"delete":["delete","Delete this hex flower permanently",{
//		"confirm":["confirm","Select True to confirm deleting the hex flower",{}]
function del(hexflowerName, userID, hostUser) {
	if (!accessible(hexflowerName, userID, hostUser, 'write')) return slashcommands.localizedString(locale, 'hexflower-notexists', []);
	delete hexFlowers[tableName];
	save();
	return '';
}

module.exports = Object.assign({ load, create, list, accessible, hex, show, publish, del, showHex, nextHex });
