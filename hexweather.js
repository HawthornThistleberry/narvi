//------------------------------------------------------------------------------
// Hex Weather Flower by Goblin's Henchman
// https://www.drivethrurpg.com/product/367072/Weather-Hex-Flower--Random-weather-generation--by-Goblins-Henchman
//

var embeds = require('./embeds.js');
//var hexflower = require('./hexflower.js');

// moves are NE SE S SW NW N
var hexFlower = [
	{ // 0
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946569669165318154/weather00.png',
		desc: 'Sunny',
		moves: [3, 4, 1, 0, 18, 2]
	},
	{ // 1
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946569939232391259/weather01.png',
		desc: 'Sunny',
		moves: [4, 5, 2, 12, 15, 0]
	},
	{ // 2
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570055662071818/weather02.png',
		desc: 'Partly sunny with rain/snow',
		moves: [5, 6, 0, 16, 11, 1]
	},
	{ // 3
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570141540425838/weather03.png',
		desc: 'Sunny and hot',
		moves: [7, 8, 4, 0, 17, 6]
	},
	{ // 4
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570214403891261/weather04.png',
		desc: 'Sunny',
		moves: [8, 9, 5, 1, 0, 3]
	},
	{ // 5
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570542683684864/weather05.png',
		desc: 'Mostly cloudy',
		moves: [9, 10, 6, 2, 1, 4]
	},
	{ // 6
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570619594604574/weather06.png',
		desc: 'Mostly cloudy',
		moves: [10, 11, 3, 17, 2, 5]
	},
	{ // 7
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570719955927100/weather07.png',
		desc: 'Hazard (e.g., fog, blizzard, tornado, wildfire, flooding)',
		moves: [7, 12, 8, 3, 7, 7]
	},
	{ // 8
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570798603321364/weather08.png',
		desc: 'Partly sunny',
		moves: [12, 13, 9, 4, 3, 7]
	},
	{ // 9
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570868400721971/weather09.png',
		desc: 'Partly sunny with rain/snow',
		moves: [13, 14, 10, 5, 4, 8]
	},
	{ // 10
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570918367469578/weather10.png',
		desc: 'Mostly sunny',
		moves: [14, 15, 11, 6, 5, 9]
	},
	{ // 11
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946570979910500392/weather11.png',
		desc: 'Mostly sunny',
		moves: [15, 2, 11, 18, 6, 10]
	},
	{ // 12
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946571036357439598/weather12.png',
		desc: 'Lightning and rain/snow',
		moves: [1, 16, 13, 8, 7, 15]
	},
	{ // 13
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946571100136038470/weather13.png',
		desc: 'Cloudy',
		moves: [16, 17, 14, 9, 8, 12]
	},
	{ // 14
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946571163025412156/weather14.png',
		desc: 'Clouds and rain/snow',
		moves: [17, 18, 15, 10, 9, 13]
	},
	{ // 15
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946571206054805564/weather15.png',
		desc: 'Cloudy',
		moves: [18, 1, 12, 11, 10, 14]
	},
	{ // 16
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946571264628256818/weather16.png',
		desc: 'Dark clouds and rain/snow',
		moves: [2, 16, 17, 13, 12, 18]
	},
	{ // 17
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946571709580976178/weather17.png',
		desc: 'Heavy clouds and rain/snow',
		moves: [6, 3, 18, 14, 13, 16]
	},
	{ // 18
		image: 'https://cdn.discordapp.com/attachments/848011016612478987/946571754665541723/weather18.png',
		desc: 'Clouds and rain/snow',
		moves: [11, 0, 16, 15, 14, 17]
	}
];

// build an embed showing the weather selection
function showWeather(currentWeather, gameName) {
	// return hexflower.showHex('weather',currentWeather, gameName, 'Weather','https://www.drivethrurpg.com/product/367072/Weather-Hex-Flower--Random-weather-generation--by-Goblins-Henchman');
	let title = 'Weather';
	if (gameName != undefined) title += ' in ' + gameName;
	title += ' [' + currentWeather + ']';
	let theEmbed = embeds.buildEmbed(
		title,
		hexFlower[currentWeather].image,
		'https://www.drivethrurpg.com/product/367072/Weather-Hex-Flower--Random-weather-generation--by-Goblins-Henchman',
		hexFlower[currentWeather].desc,
		'from Weather Hex Flower by Goblin\'s Henchman',
		null,
		null,
		null,
		gameName,
		embeds.BUTTON_GREYPLE,
		null
	);
	return {embed: theEmbed};
}

// moves on the weather hex and returns the new weather value
function nextWeather(currentWeather, roll) {
	// return hexflower.nextHex(currentWeather, roll);
	let dirIndex;
	if (roll <= 3) dirIndex = 0;
	else if (roll <= 5) dirIndex = 1;
	else if (roll <= 7) dirIndex = 2;
	else if (roll <= 9) dirIndex = 3;
	else if (roll <= 11) dirIndex = 4;
	else dirIndex = 5;
	return hexFlower[currentWeather].moves[dirIndex];
}

module.exports = Object.assign({ showWeather, nextWeather });

