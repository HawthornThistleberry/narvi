//------------------------------------------------------------------------------
// Initiative tracking and handling
//

var slashcommands = require('./slashcommands.js');
var stringutil = require('./stringutil.js');
var encounters = require('./encounters.js');
var embeds = require('./embeds.js');

const PHASE_OPENINGVOLLEY	= 0;
const PHASE_CHANGESTANCES	= 1;
const PHASE_FORWARD			= 2;
const PHASE_OPEN			= 3;
const PHASE_DEFENSIVE		= 4;
const PHASE_SKIRMISH		= 5;
const PHASE_REARWARD		= 6;
const PHASE_ADVERSARY		= 7;

// Unicode pairs generated with http://russellcottrell.com/greek/utilities/SurrogatePairCalculator.htm
const BUTTON_ADVANCE = '\u23ED'; // track-next
const BUTTON_END     = '\u23F9'; // track-stop

const stanceNameToPhase = {
	'opening volley': 0,
	'change stances': 1,
	'forward': 2,
	'open': 3,
	'defensive': 4,
	'skirmish': 5,
	'rearward': 6,
	'adversary actions': 7
}

const emojiSizeIntro = '       ';

var f_characterDataFetchHandleNulls;
var f_characterDataFetchHandleNumbers;
var f_characterDataFetchHandleNumbers;
var f_combatModActive;
var f_userMention;
var f_tokenCommandExecute;
var f_botSendLMPrivateMessage;
var f_hasTrait;
var f_getGameName;
var f_enduranceLossDamage;

// save stuff on bot startup
function initiativeInit(characterDataFetchHandleNulls, characterDataFetchHandleNumbers, tokenCommandExecute, combatModActive, userMention, charEffectiveStance, botSendLMPrivateMessage, hasTrait, getGameName, enduranceLossDamage) {
	f_characterDataFetchHandleNulls = characterDataFetchHandleNulls;
	f_characterDataFetchHandleNumbers = characterDataFetchHandleNumbers;
	f_tokenCommandExecute = tokenCommandExecute;
	f_combatModActive = combatModActive;
	f_userMention = userMention;
	f_charEffectiveStance = charEffectiveStance;
	f_botSendLMPrivateMessage = botSendLMPrivateMessage;
	f_hasTrait = hasTrait;
	f_getGameName = getGameName;
	f_enduranceLossDamage = enduranceLossDamage;
	return true;
}

// build a potential mention
function showCharacterName(g, p, brief, curphase) {
	let r = f_characterDataFetchHandleNulls(g, p, 'name', null);
	if (!brief && curphase && g.config.includes('combat-mention')) {
		let userID = 0;
		for (player in g.players) {
			g.players[player].characters.forEach(c => {
				if (c == p) userID = player;
			});
		}
		if (userID == 0) {
			g.lmCharacters.forEach(c => {
				if (c == p) userID = g.loremaster;
			});
		}
		if (userID != 0) {
			let m = f_userMention(userID);
			if (m != '') r += ' [' + m + ']';
		}
	}
	return r;
}

// Builds a rich display of a combat phase
function showCombatPhase(g, phase, locale, brief, alwaysShow) {

	if (g.combat.ambush && isStancePhase(phase)) return '';

	let emojiRound, emojiAction, emojiActionDone, emojiActionKnockback = ':orange_square:';
	if (phase == g.combat.phase || (g.config.includes('unphased-initiative') && isStancePhase(phase) && g.combat.phase != PHASE_OPENINGVOLLEY)) {
		// we're showing the currently active phase, with blue emojis
		emojiRound = ':arrow_forward:';
		emojiAction = ':blue_square:';
		emojiActionDone = ':ballot_box_with_check:';
	} else if (phase < g.combat.phase) {
		// we're showing a past phase, with green emojis
		emojiRound = ':green_circle:';
		emojiAction = ':white_check_mark:';
		emojiActionDone = ':white_check_mark:';
	} else {
		// we're showing a future phase, with yellow emojis
		emojiRound = ':yellow_circle:';
		emojiAction = ':yellow_square:';
		emojiActionDone = ':heavy_check_mark:';
	}

	// build a phrase heading
	let phaseHeading = '';
	if (!brief) phaseHeading = emojiRound + ' ';
	phaseHeading +=  '**' + slashcommands.localizedString(locale, 'initiative-phase' + String(phase), []) + '**';
	if (brief) phaseHeading += ': '; else phaseHeading += '\n';

	// build the body of the phase based on who could act or did act; for current and future phases, show bonuses
	let phaseBody = '';
	if (phase == PHASE_OPENINGVOLLEY) phaseBody = showCombatPhaseOpeningVolley(g, locale, brief, emojiAction, emojiActionDone, emojiActionKnockback);
	else if (phase == PHASE_CHANGESTANCES) phaseBody = showCombatPhaseChangeStances(g, locale, brief, emojiAction, emojiActionDone);
	else if (phase == PHASE_ADVERSARY && g.combat.encounter != '') phaseBody = showCombatPhaseAdversaries(g, phase, locale, brief, emojiAction, emojiActionDone, emojiActionKnockback);
	else if (phase == PHASE_ADVERSARY) return phaseHeading;
	else phaseBody = showCombatPhaseStance(g, phase, locale, brief, emojiAction, emojiActionDone, emojiActionKnockback);

	// add events to phaseBody if in that phase and not secret
	phaseBody += showCombatPhaseEvents(g, g.combat.round, phase, locale, brief, false);

	// add encounter environment if present
	if (phase == PHASE_CHANGESTANCES && g.combat.encounter && g.encounters[g.combat.encounter].environment.type) {
		if (!brief) phaseBody += emojiSizeIntro;
		phaseBody += ':tornado: ' + slashcommands.localizedString(locale, 'condition-severity-' + g.encounters[g.combat.encounter].environment.level, []) + ' ' + slashcommands.localizedOption(locale, 'environment', 'type', g.encounters[g.combat.encounter].environment.type) + (brief ? ', ' : '\n');
	}

	// if there are secret ones, show those secretly
	if (phase == g.combat.phase) {
		let secretEvents = showCombatPhaseEvents(g, g.combat.round, phase, locale, brief, true);
		if (secretEvents != '') {
			f_botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, phaseHeading + secretEvents);
		}
	}

	// if there wasn't anything in the body, return an empty string
	if (phaseBody != '') return phaseHeading + phaseBody;
	
	if (!alwaysShow) return '';
	return phaseHeading + ': ' + slashcommands.localizedString(locale, 'none', []);
}

function showCombatPhaseOpeningVolley(g, locale, brief, emojiAction, emojiActionDone, emojiActionKnockback) {
	if (g.combat.phase != PHASE_OPENINGVOLLEY) return ''; // never show a body if it's in the past
	let r = '', sep = '\n';
	if (brief) sep = ', ';
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		// if they don't have a ranged weapon, skip them
		if (!hasRangedWeapon(g, p) || g.characters[p].knockback) continue;
		if (!brief) r += emojiSizeIntro;
		if (g.combat.actors.includes(p)) r += emojiAction;
		else r += emojiActionDone;
		r += ' ' + showCharacterName(g, p, brief, true);
		r += relevantCombatBonuses(g, p, PHASE_OPENINGVOLLEY, locale, brief) + sep;
	}
	if (brief && r.endsWith(', ')) r = r.slice(0,-2);
	if (g.encounters && g.combat.encounter && g.encounters[g.combat.encounter].adversaries && !g.combat.advsurprise) {
		for (adv in g.encounters[g.combat.encounter].adversaries) {
			if (!brief) r += emojiSizeIntro;
			if (encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[adv])) r += ':skull_crossbones:';
			else if (g.encounters[g.combat.encounter].adversaries[adv].knockback) r += emojiActionKnockback;
			else if (g.encounters[g.combat.encounter].adversaries[adv].actionsThisRound >= adversaryCombatActions(g, adv)) r += emojiActionDone;
			else if (g.encounters[g.combat.encounter].adversaries[adv].actionsThisRound > 0) r += ':arrow_right:';
			else r += emojiAction;
			r += ' ' + adv;
			if (!encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[adv])) r += relevantAdversaryBonuses(g, adv, PHASE_OPENINGVOLLEY, locale, brief);
			r += sep;
		}
	}
	if (brief && r.endsWith(', ')) r = r.slice(0,-2);
	return r;
}

function showCombatPhaseChangeStances(g, locale, brief, emojiAction, emojiActionDone) {
	if (g.combat.phase > PHASE_CHANGESTANCES) return ''; // never show a body if it's in the past
	let r = '', sep = '\n';
	if (brief) sep = ', ';
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		// if there was ever something that could preclude stance change, check for it here
		if (!brief) r += emojiSizeIntro;
		if (g.combat.actors.includes(p)) r += emojiAction;
		else r += emojiActionDone;
		r += ' ' + showCharacterName(g, p, brief, true) + ' (' + slashcommands.localizedString(locale, 'stance-' + f_charEffectiveStance(g, p),[]) + ')' + sep;
	}
	if (brief && r.endsWith(', ')) r = r.slice(0,-2);
	return r;
}

function showCombatPhaseStance(g, phase, locale, brief, emojiAction, emojiActionDone, emojiActionKnockback) {
	let showBonuses = true, r = '', sep = '\n';
	if (brief) sep = ', ';
	if (g.combat.phase < phase) showBonuses = false;
	for (let p in g.characters) {
		if (!g.characters[p].active || stanceNameToPhase[f_charEffectiveStance(g, p)] != phase) continue;
		if (!brief) r += emojiSizeIntro;
		if (phase < g.combat.phase) {
			r += emojiActionDone + ' ' + f_characterDataFetchHandleNulls(g, p, 'name', null) + sep;
		} else {
			if (g.characters[p].knockback) r += emojiActionKnockback;
			else if ((g.combat.phase == phase || g.config.includes('unphased-initiative')) && !(g.combat.actors.includes(p))) r += emojiActionDone;
			else r += emojiAction;
			r += ' ' + showCharacterName(g, p, brief, phase == g.combat.phase);
			r += relevantCombatBonuses(g, p, phase, locale, brief) + sep;
			// if engaged, add \n and then two spacings and a few more spaces and a list of engaged
			let engaged = encounters.engaged_adversaries(g, p);
			if (engaged.length > 0) r += emojiSizeIntro + emojiSizeIntro + emojiSizeIntro + slashcommands.localizedString(locale, 'combat-engagement', []) + ': ' + engaged.join(', ') + '\n';
		}
	}
	if (brief && r.endsWith(', ')) r = r.slice(0,-2);
	return r;
}

function adversaryCombatActions(g, adv) {
	let r = g.encounters[g.combat.encounter].adversaries[adv].might;
	if (encounters.encounters_adversary_has_ability(g, g.combat.encounter, adv, 'many arms')) r = r - g.encounters[g.combat.encounter].adversaries[adv].wounds + 1;
	return r;
}

function showCombatPhaseAdversaries(g, phase, locale, brief, emojiAction, emojiActionDone, emojiActionKnockback) {
	let r = '', sep = '\n';
	if (brief) sep = ', ';
	for (adv in g.encounters[g.combat.encounter].adversaries) {
		if (!brief) r += emojiSizeIntro;
		if (phase < g.combat.phase) {
			r += emojiActionDone + ' ' + adv + sep;
		} else {
			if (encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[adv])) r += ':skull_crossbones:';
			else if (g.encounters[g.combat.encounter].adversaries[adv].knockback) r += emojiActionKnockback;
			else if ((g.combat.phase == phase || g.config.includes('unphased-initiative')) && g.encounters[g.combat.encounter].adversaries[adv].actionsThisRound >= adversaryCombatActions(g, adv)) r += emojiActionDone;
			else if ((g.combat.phase == phase || g.config.includes('unphased-initiative')) && g.encounters[g.combat.encounter].adversaries[adv].actionsThisRound > 0) r += ':arrow_right:';
			else r += emojiAction;
			r += ' ' + adv;
			if (!encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[adv])) r += relevantAdversaryBonuses(g, adv, phase, locale, brief);
			r += sep;
		}
	}
	if (brief && r.endsWith(', ')) r = r.slice(0,-2);
	return r;
}

function showCombatPhaseEvents(g, round, phase, locale, brief, secret) {
	if (g.combat.encounter == '') return '';
	let r = '', sep = '\n';
	if (brief) sep = ', ';
	g.encounters[g.combat.encounter].events.forEach(event => {
		if (event.secret == null) event.secret = false;
		if (event.round == round && event.phase == phase && event.secret == secret) {
			if (!brief) r += emojiSizeIntro;
			r += ':timer: ' + event.text + sep;
		}
	});
	if (brief && r.endsWith(', ')) r = r.slice(0,-2);
	return r;
}

// if any bonuses, a colon and then the bonuses
function relevantCombatBonuses(g, actor, phase, locale, brief) {
	let r = [], bonus = 0;

	if (g.characters[actor].knockback) {
		return ' _(' + slashcommands.localizedString(locale, 'combat-knockedback',[]) + ')_';	
	}
	if (g.characters[actor].tempbonus) {
		r.push((g.characters[actor].tempbonus > 0 ? '+' : '') + g.characters[actor].tempbonus + 'd (' + slashcommands.localizedString(locale, 'initiative-tempbonus', []) + ')');
		bonus += g.characters[actor].tempbonus;
	}
	if (stanceNameToPhase[f_charEffectiveStance(g, actor)] - 1 <= g.rallycurrent) {
		r.push('+1d (' + slashcommands.localizedCommand(locale, 'rally') + ')');
		bonus++;
	}
	if (f_charEffectiveStance(g, actor) == 'forward' && phase != PHASE_OPENINGVOLLEY) {
		r.push('+1d (' + slashcommands.localizedOption(locale, 'stance', 'stance', 'forward') + ')');
		bonus++;
	}
	if (f_charEffectiveStance(g, actor) == 'defensive' && encounters.count_engagement(g,actor) && phase != PHASE_OPENINGVOLLEY) {
		r.push('-' + encounters.count_engagement(g,actor) + 'd (' + slashcommands.localizedOption(locale, 'stance', 'stance', 'defensive') + ')');
		bonus -= encounters.count_engagement(g,actor);
	}
	if (f_charEffectiveStance(g, actor) == 'skirmish' && phase != PHASE_OPENINGVOLLEY) {
		r.push('-1d (' + slashcommands.localizedOption(locale, 'stance', 'stance', 'skirmish') + ')');
		bonus--;
	}
	// add any combat mods
	g.combatMods.forEach(c => {
		if (f_combatModActive(c,actor)) {
			if (c.modifier > 0) r.push('+' + c.modifier + 'd (' + slashcommands.localizedString(locale, 'initiative-advantage', []) + ')');
			else r.push(c.modifier + 'd (' + slashcommands.localizedString(locale, 'initiative-complication', []) + ')');
			bonus += c.modifier;
		}
	});
	if (r.length > 0) {
		r = ': ' + r.join(', ') + ', ' + (brief ? '' : '**') + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'total', [])) + ' ';
		if (bonus > 0) r += '+' + bonus + 'd';
		else r += bonus + 'd';
		r += (brief ? '' : '**');
	} else r = '';
	if (g.characters[actor].tempparry > 0) {
		if (r == '') r += ': '; else r += ', ';
		r += '-' + String(g.characters[actor].tempparry) + 'd ' + slashcommands.localizedString(locale, 'token-tempparry', []);
	}
	if (g.characters[actor].fendParry > 0) {
		if (r == '') r += ': '; else r += ', ';
		r += '+' + String(g.characters[actor].fendParry) + ' ' + slashcommands.localizedString(locale, 'token-fendparry', []);
	}
	return r;
}

// if any bonuses, a colon and then the bonuses
function relevantAdversaryBonuses(g, adversary, phase, locale, brief) {
	let r = [], bonus = 0, mod = 0;

	if (g.encounters[g.combat.encounter].adversaries[adv].knockback > 0) {
		return ' _(' + slashcommands.localizedString(locale, 'combat-knockedback',[]) + ')_';	
	}
	if (g.encounters[g.combat.encounter].adversaries[adv].pushback > 0) {
		r.push('-1d (' + slashcommands.localizedString(locale, 'condition-pushback', []) + ')');
		bonus--;
	}
	if (g.combat.advsurprise) {
		r.push('-1d (' + slashcommands.localizedSubcommand(locale, 'start', 'ambush') + ')');
		bonus--;
	}
	if (g.encounters[g.combat.encounter].adversaries[adv].engagement.length == 1 && g.characters[g.encounters[g.combat.encounter].adversaries[adv].engagement[0]]) {
		let charID = g.encounters[g.combat.encounter].adversaries[adv].engagement[0];
		let charName = f_characterDataFetchHandleNulls(g, charID, 'name', null); 

		if (f_charEffectiveStance(g, charID) == 'forward') {
			r.push('+1d (' + charName + ' ' + slashcommands.localizedOption(locale, 'stance', 'stance', 'forward') + ')');
			bonus++;
		}
		// (when adversaries are in) if we knew if the attack was melee, skirmish would be bonus = -1
		if (f_charEffectiveStance(g, charID) == 'defensive') {
			r.push('-1d (' + charName + ' ' + slashcommands.localizedOption(locale, 'stance', 'stance', 'defensive') + ')');
			bonus--;
		}
		let armourStats = f_characterDataFetchHandleNulls(g, charID, 'sheet', 'armour');
		if (g.characters[charID].handedness != 2 && !g.characters[charID].shieldsmashed && armourStats[3] > 0) {
			r.push('-' + armourStats[3] + ' (' + charName + ' ' + slashcommands.localizedString(locale, 'condition-shield', []) + ')');
			mod -= armourStats[3];
		}
		if (g.characters[charID].tempparry > 0) {
			r.push('-' + g.characters[charID].tempparry + 'd (' + charName + ' ' + slashcommands.localizedString(locale, 'token-tempparry', []) + ')');
			bonus -= g.characters[charID].tempparry;
		}
		if (g.characters[charID].fendParry > 0) {
			r.push('-' + g.characters[charID].fendParry + ' (' + charName + ' ' + slashcommands.localizedString(locale, 'token-fendparry', []) + ')');
			mod -= g.characters[charID].fendParry;
		}
		if (f_hasTrait(g, charID, 'virtue', 'small folk') && !['small','tiny'].includes(stringutil.lowerCase(g.encounters[g.combat.encounter].adversaries[adversary].size))) {
			r.push('-2 (' + charName + ' Small Folk)');
			mod -= 2;
		}
	}
	g.combatMods.forEach(c => {
		if (f_combatModActive(c,0)) {
			if (c.modifier > 0) r.push('+' + c.modifier + 'd (' + slashcommands.localizedString(locale, 'initiative-advantage', []) + ')');
			else r.push(c.modifier + 'd (' + slashcommands.localizedString(locale, 'initiative-complication', []) + ')');
			bonus += c.modifier;
		}
	});

	if (r.length > 0) {
		r = ': ' + r.join(', ') + ', ' + (brief ? '' : '**') + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'total', [])) + ' ';
		if (bonus > 0) r += '+' + bonus + 'd';
		else if (bonus < 0) r += bonus + 'd';
		else if (mod == 0) r += slashcommands.localizedString(locale, 'none', []);
		r += (brief ? '' : '**');
	} else r = '';
	if (mod != 0) {
		if (r == '') r += ': '; else if (bonus != 0) r += ', ';
		r += (mod > 0 ? '+' : '') + String(mod);
	}
	return r;
}

// eligible to participate in an opening volley?
function hasRangedWeapon(g, p) {
	let rangedWeapon = false;
	for (w in f_characterDataFetchHandleNulls(g, p, 'weapons', null)) {
		if (f_characterDataFetchHandleNulls(g, p, 'weapons',w)[10] == 'TRUE') rangedWeapon = true;
	}
	return rangedWeapon;
}

function charactersInPhase(g, phase, unphased, clear) {
	let r = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		if (g.characters[p].stance == 'absent') continue;

		let canAct = false;
		// everyone can change stances
		if (phase == PHASE_CHANGESTANCES) canAct = true;
		// only people with ranged weapons can act in opening volley
		else if (phase == PHASE_OPENINGVOLLEY) canAct = (hasRangedWeapon(g, p) && !g.characters[p].knockback && !g.combat.ambush);
		// only people in the right stance can act in a stance phase
		else if (isStancePhase(phase) && (unphased || stanceNameToPhase[f_charEffectiveStance(g, p)] == phase) && !g.combat.ambush) canAct = true;

		if (canAct) {
			r.push(p);

			// if we're actually entering the phase, this is a good time to clear things to prepare for their next action
			if (clear && (stanceNameToPhase[f_charEffectiveStance(g, p)] == phase || (unphased && isStancePhase(phase))))
				g.characters[p].tengwar = 0;
				g.characters[p].protectionTN = 0;
				g.characters[p].attackFeatRoll = 0;
				g.characters[p].fendParry = 0;
				g.characters[p].dmgDone = 0;
				g.characters[p].handedness = 1;
				g.characters[p].curWeapon = '';
			}
	}
	return r;	
}

// Determines the next phase that will have activity in it
function nextCombatPhase(g) {
	let phase = null;
	// clear adversary surprise when leaving adversary phase
	if (g.combat.advsurprise && g.combat.phase == PHASE_ADVERSARY) g.combat.advsurprise = false;
	if (g.combat.phase == PHASE_ADVERSARY || g.combat.phase == PHASE_OPENINGVOLLEY) phase = PHASE_CHANGESTANCES;
	else if (g.combat.ambush && g.combat.phase == PHASE_CHANGESTANCES) phase = PHASE_ADVERSARY;
	else phase = g.combat.phase + 1;
	if (isStancePhase(phase) && g.config.includes('unphased-initiative')) phase = PHASE_ADVERSARY;
	if (phase == PHASE_ADVERSARY) return phase;
	//console.log('next combat phase is ' + phase + ' with these characters: ' + charactersInPhase(g, phase, false, false).join(','));
	while (charactersInPhase(g, phase, false, false).length == 0) {
		phase++;
		//console.log('now checking phase ' + phase + ' with these characters: ' + charactersInPhase(g, phase, false, false).join(','));
		if (phase == PHASE_ADVERSARY) return phase;
	}
	return phase;
}

// mark all adversaries as having not yet acted
function loadAdversaryActions(g) {
	if (g.combat.encounter) {
		for (adv in g.encounters[g.combat.encounter].adversaries) {
			g.encounters[g.combat.encounter].adversaries[adv].actionsThisRound = 0;
			g.encounters[g.combat.encounter].adversaries[adv].hateSpent = 0;
		}
	}
}

// Handles everything that happens when a combat enters a particular phase -- should always be a valid one based on nextCombatPhase
function enterCombatPhase(g, locale, channelID, phase, brief) {
	let r = '';
	let unphased = g.config.includes('unphased-initiative');

	// clear knockback on anyone in the phase we're leaving
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		if (stanceNameToPhase[f_charEffectiveStance(g, p)] == g.combat.phase || unphased) g.characters[p].knockback = false;
	}

	// if entering adversary actions, clear ambush flag
	if (phase == PHASE_ADVERSARY) g.combat.ambush = false;

	// if it's a new round, clear and advance things accordingly
	if (phase == PHASE_CHANGESTANCES) {
		g.combat.round++;
		g.rallycurrent = g.rallynext;
		g.rallynext = 0;
		if (unphased) phase = PHASE_FORWARD; // skip Change Stances when in unphased
		// handle bleeding, bitten, and drain
		for (let p in g.characters) {
			if (!g.characters[p].active) continue;
			g.characters[p].dmgReceivedThisRound = 0;
			if (g.characters[p].bleeding) r += f_tokenCommandExecute('damage', channelID, locale, g, p, 'endurance', '-', g.characters[p].bleeding, false) + '\n';
			if (g.characters[p].bitten) r += f_tokenCommandExecute('damage', channelID, locale, g, p, 'endurance', '-', 2, false) + '\n';
			if (g.characters[p].drained) {
				// piercing blow
				let tn = g.characters[p].drainedTN;
				g.characters[p].protectionTests.push({'injury': tn, 'responsible': g.characters[p].drainedAdv, 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': null, 'poison': 0, 'deadly': false});
				r += slashcommands.localizedString(locale, 'condition-mustprottest', [f_characterDataFetchHandleNulls(g, p, 'name', null), tn]) + '\n';
				// endurance loss from poison - moderate, or severe if Wounded
				let [dmg, r2] = f_enduranceLossDamage((f_characterDataFetchHandleNulls(g, p, 'wounded', null) == 'TRUE' ? 2 : 1), 'poison', g, p, locale, channelID);
				r += g.characters[p].drainedAdv + r2;
			}
		}
		if (g.combat.encounter != '') {
			// if there's an environment effect on this round or round 0
			if (g.encounters[g.combat.encounter].environment.round != undefined && (g.encounters[g.combat.encounter].environment.round == 0 || g.encounters[g.combat.encounter].environment.round == g.combat.round)) {
				// for every character that's active
				for (let p in g.characters) {
					if (!g.characters[p].active) continue;
					// do the endurance loss effect
					let [dmg, r2] = f_enduranceLossDamage(g.encounters[g.combat.encounter].environment.level, g.encounters[g.combat.encounter].environment.type, g, p, locale, channelID);
					r += slashcommands.localizedOption(locale, 'environment', 'type', g.encounters[g.combat.encounter].environment.type) + r2;
				}
				// if the round is not 0, delete it
				if (g.encounters[g.combat.encounter].environment.round != 0) {
					g.encounters[g.combat.encounter].environment = {};
				}
			}

			// delete any events in the round we just left (or earlier)
			let newEvents = [];
			g.encounters[g.combat.encounter].events.forEach(event => {
				if (event.round >= g.combat.round) newEvents.push(event);
			});
			g.encounters[g.combat.encounter].events = [... newEvents];
			// decrement adversary conditions that are counts of rounds
			for (let a in g.encounters[g.combat.encounter].adversaries) {
				if (g.encounters[g.combat.encounter].adversaries[a].knockback > 0) g.encounters[g.combat.encounter].adversaries[a].knockback--;
				if (g.encounters[g.combat.encounter].adversaries[a].weary > 0) g.encounters[g.combat.encounter].adversaries[a].weary--;
				if (g.encounters[g.combat.encounter].adversaries[a].pushback) g.encounters[g.combat.encounter].adversaries[a].pushback = false;
			}
			// implement Hate Sunlight
			if (g.combat.light == 'sunlight') {
				for (let a in g.encounters[g.combat.encounter].adversaries) {
					if ((encounters.encounters_adversary_has_ability(g, g.combat.encounter, a, 'hate sunlight') || encounters.encounters_adversary_has_ability(g, g.combat.encounter, a, 'hates sunlight')) && g.encounters[g.combat.encounter].adversaries[a].curhrscore > 0) {
						g.encounters[g.combat.encounter].adversaries[a].curhrscore--;
						f_botSendLMPrivateMessage(g, g.loremaster, g.lmchannel, slashcommands.localizedString(locale, 'initiative-losthate', [a, g.encounters[g.combat.encounter].adversaries[a].curhrscore]));
					}
				}
			}
		}

	}

	// move into the new phase
	g.combat.phase = phase;

	// activate or expire combat mods as necessary
	let newCombatMods = [];
	for (let i = 0; i < g.combatMods.length; i++) {
		if (g.combatMods[i].started == false && (unphased || g.combatMods[i].phase == phase)) {
			r += slashcommands.localizedString(locale, (g.combatMods[i].modifier > 0 ? 'combat-adv-start' : 'combat-comp-start'), [g.combatMods[i].modifier]) + '\n';
			g.combatMods[i].started = true;
			newCombatMods.push(g.combatMods[i]);
		} else if (g.combatMods[i].duration == 1 && (unphased || g.combatMods[i].phase == phase)) {
			r += slashcommands.localizedString(locale, (g.combatMods[i].modifier > 0 ? 'combat-adv-end' : 'combat-comp-end'), [g.combatMods[i].modifier]) + '\n';
		} else if (g.combatMods[i].duration > 1) {
			g.combatMods[i].duration--;
			newCombatMods.push(g.combatMods[i]);
		} else {
			newCombatMods.push(g.combatMods[i]);
		}
	}
	g.combatMods = [...newCombatMods];

	// if entering adversary phase, or starting the combat, clear all engagement counts and load adversary actions
	if (phase == PHASE_ADVERSARY || phase == PHASE_OPENINGVOLLEY || (unphased && isStancePhase(phase))) {
		for (let p2 in g.characters) {
			g.characters[p2].numEngaged = 0;
		}
		loadAdversaryActions(g);
	}
	if (phase != PHASE_ADVERSARY) {
		// load the list of actors
		g.combat.actors = [...charactersInPhase(g, phase, unphased, true)];
	}
	return r;
}

function isStancePhase(phase) {
	if (phase == PHASE_FORWARD || 
		phase == PHASE_OPEN ||
		phase == PHASE_DEFENSIVE || 
		phase == PHASE_SKIRMISH || 
		phase == PHASE_REARWARD) return true;
	return false;
}

// Call this when a player-hero is doing something that might consume their action for this phase
function takeInitiativeAction(locale, g, p, cmd) {
	if (!g.combat.active) return '';
	let filter = false, unphased = g.config.includes('unphased-initiative');
	if (cmd =='all') filter = true;
	if (g.combat.phase == PHASE_CHANGESTANCES && cmd == 'stance') filter = true;
	if ((g.combat.phase == PHASE_OPENINGVOLLEY || isStancePhase(g.combat.phase) || unphased) && 
		cmd == 'attack') filter = true;
	if ((isStancePhase(g.combat.phase) || unphased) && cmd == 'battle') filter = true;
	if ((g.combat.phase == PHASE_FORWARD || unphased) && cmd == 'intimidate') filter = true;
	if ((g.combat.phase == PHASE_OPEN || unphased) && cmd == 'rally') filter = true;
	if ((g.combat.phase == PHASE_DEFENSIVE || unphased) && cmd == 'protect') filter = true;
	if ((g.combat.phase == PHASE_REARWARD || unphased) && cmd == 'prepare') filter = true;
	if (cmd == 'gainground') filter = true;

	if (filter) {
		g.combat.actors = g.combat.actors.filter(x => x != p);
		// we ignore adversary actions here in opening volley since they often might not take any
		if (g.combat.actors.length == 0) return readyToAdvanceEmbed(g, locale);
		let allkb = true;
		g.combat.actors.forEach(p => {
			if (g.characters[p] && !g.characters[p].knockback) allkb = false;
		});
		if (allkb) return readyToAdvanceEmbed(g, locale);
	}
	return '';
}

// record an adversary taking an action and notify if that ends the phase
function takeAdversaryAction(g, locale, adversary) {
	if (!g.combat.active || !g.combat.encounter || (g.combat.phase != PHASE_ADVERSARY &&  g.combat.phase != PHASE_OPENINGVOLLEY)) return '';
	g.encounters[g.combat.encounter].adversaries[adversary].actionsThisRound++;
	if (g.combat.phase != PHASE_ADVERSARY) return ''; // don't use this to advance, it's up to player actions, in opening volley
	let allTaken = true;
	for (adv in g.encounters[g.combat.encounter].adversaries) {
		if (!encounters.adversaryOutOfCombat(g.encounters[g.combat.encounter].adversaries[adv])
			&& !g.encounters[g.combat.encounter].adversaries[adv].knockback
			&& g.encounters[g.combat.encounter].adversaries[adv].actionsThisRound < adversaryCombatActions(g, adv))
			allTaken = false;
	}
	if (allTaken) {
		return readyToAdvanceEmbed(g, locale);
	}
	return '';
}

// build an embed with buttons for advance and end combat
function readyToAdvanceEmbed(g, locale) {

	return embeds.buildDialog(
		locale, g, f_getGameName(g), [g.loremaster],
		slashcommands.localizedString(locale, 'initiative-phase' + String(g.combat.phase), []),
		'https://images.emojiterra.com/twitter/v13.1/512px/23ed.png',
		'https://bitbucket.org/HawthornThistleberry/narvi/src/master/HowTo.md',
		slashcommands.localizedString(locale, 'initiative-readytoadvance', []),
		'',
		embeds.BUTTON_BLURPLE,
		[
			{
				'command':'init-advance',
				'color':1,
				'label':'initiative-button-next',
				'extratext':'',
				'emoji':BUTTON_ADVANCE
			},
			{
				'command':'init-end',
				'color':1,
				'label':'initiative-button-end',
				'extratext':'',
				'emoji':BUTTON_END
			}
		]);
}

// a report that is just the displays of every phase in the round
function initiativeReport(locale, g, brief) {
	if (!g.combat.active) return slashcommands.localizedString(locale, 'error-nocombat', []);
	let r = '';
	let ending = PHASE_ADVERSARY;
	if (brief) ending = PHASE_REARWARD;
	for (let phase = PHASE_OPENINGVOLLEY; phase <= ending; phase++) {
		let r2 = showCombatPhase(g, phase, locale, brief, false);
		if (r2 !== '') {
			r += r2;
			if (brief) r += '; ';
		}
	}
	if (brief && r != '') r = r.slice(0,-2);
	return r;
}

module.exports = Object.assign({ PHASE_OPENINGVOLLEY, PHASE_CHANGESTANCES, PHASE_FORWARD, PHASE_OPEN, PHASE_DEFENSIVE, PHASE_SKIRMISH, PHASE_REARWARD, PHASE_ADVERSARY, stanceNameToPhase, initiativeInit, showCombatPhase, nextCombatPhase, enterCombatPhase, charactersInPhase, takeInitiativeAction, takeAdversaryAction, initiativeReport, adversaryCombatActions, relevantCombatBonuses });

