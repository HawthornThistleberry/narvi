// JOURNEYS --------------------------------------------------------------------

const filehandling = require('./filehandling.js');
const stringutil = require('./stringutil.js');
var table = require('./table.js');
var slashcommands = require('./slashcommands.js');
var lookuptables = require('./lookuptables.js');
var hexflower = require('./hexflower.js');
var regdice = require('./regdice.js');
var embeds = require('./embeds.js');

var journeys;
const journeysFilename = 'journeys.json';
const eventtests = ['terriblemisfortune','despair','illchoices','mishap','shortcut','chancemeeting','joyfulsight','deadlydark','longdark','watchfuleyes','branching','rightway','dreadwonder','fallingstones','eerienoises','bittercold','blizzard','paththroughsnow','gleamofhope','attacked','badweather','fairweather','favourable','light'];
const skillFromRole = {
	'guide':'travel',
	'scout':'explore',
	'hunter':'hunting',
	'delver':'craft',
	'lookout':'awareness'
};

/*
journeys: {
	"tags": [...],
	"journeys": {
		"shortname": {
			tags: ['core','strider',...]
			name: '...',
			notes: '...',
			creator: userID,
			createdate: today,
			published: false,
			origin
			destination
			eventtype
			image
			quick
			steps: [
				type (border, wild, or dark)
				terrain (road, hard, or '')
				length (numeric)
				peril (numeric)
				comment (freeform text)
				]
		}, ...
	}
*/

var f_charactersInJourneyRole;
var f_torRollDice;
var f_journey_log;
var f_hasTrait;
var f_characterDataFetch;
var f_characterDataFetchHandleNumbers;
var f_tokenCommandExecute;
var f_giveWound;
var f_userMention;
var f_advanceWounds;
var f_advancePoison;
var f_getGameName;
var f_setUserConfig;
var f_increaseEyeRating;
var f_enduranceLossDamage;

// load the journeys file on startup
function journeys_load(charactersInJourneyRole, torRollDice, journey_log, hasTrait, characterDataFetch, characterDataFetchHandleNumbers, tokenCommandExecute, giveWound, userMention, advanceWounds, advancePoison, getGameName, setUserConfig, increaseEyeRating, enduranceLossDamage) {

	f_charactersInJourneyRole = charactersInJourneyRole;
	f_torRollDice = torRollDice;
	f_journey_log = journey_log;
	f_hasTrait = hasTrait;
	f_characterDataFetch = characterDataFetch;
	f_characterDataFetchHandleNumbers = characterDataFetchHandleNumbers;
	f_tokenCommandExecute = tokenCommandExecute;
	f_giveWound = giveWound;
	f_userMention = userMention;
	f_advanceWounds = advanceWounds;
	f_advancePoison = advancePoison;
	f_getGameName = getGameName;
	f_setUserConfig = setUserConfig;
	f_increaseEyeRating = increaseEyeRating;
	f_enduranceLossDamage = enduranceLossDamage;
	
	journeys = filehandling.readJSONFile(journeysFilename);
	if (journeys == null || Object.keys(journeys).length === 0)	{
		journeys = {
			tags: ['tor', 'aime', 'lotrrp', 'core', 'strider', 'starter', 'homebrew', 'ruins', 'english', 'espanol', 'breeland', 'lonelands', 'trollshaws', 'eriador', 'rhovanion', 'rohan', 'gondor', 'coldfells', 'ettenmoors', 'shire', 'angmar', 'dunland', 'minhiriath', 'enedwaith', 'forochel', 'ettenmoors', 'northdowns', 'vales', 'mistymountains','mirkwood','dalelands','erebor','mordor', 'eregion'],
			journeys: {}
		};
		journeys_save();
	}

	/* data conversions */
	let found = false;
	for (let journeyName in journeys.journeys) {
		if (journeys.journeys[journeyName].eventtype == undefined) {
			if (journeys.journeys[journeyName].tags.includes('moria')) journeys.journeys[journeyName].eventtype = 'moriajourneyevents';
			else journeys.journeys[journeyName].eventtype = 'journeyevents';
			found = true;
		}
	}
	if (found) journeys_save();
}

// save the journeys file after each change
function journeys_save() {
	return filehandling.writeJSONFile(journeysFilename, journeys);
}

// HOST FUNCTIONS -------------------------------------------------------------

// add a tag
function journeys_tag_add(tagName) {
	if (journeys.tags.includes(stringutil.lowerCase(tagName))) return false;
	journeys.tags.push(stringutil.lowerCase(tagName));
	journeys_save();
	return true;
}

// remove a tag
function journeys_tag_remove(tagName) {
	if (!journeys.tags.includes(stringutil.lowerCase(tagName))) return false;
	journeys.tags = journeys.tags.filter(value => value != stringutil.lowerCase(tagName));
	journeys_save();
	return true;
}

// verify if a tag is available
function journeys_tag_verify(tagName) {
	return journeys.tags.includes(stringutil.lowerCase(tagName));
}

// return a tag list (e.g., for displaying)
function journeys_tag_list() {
	return journeys.tags;
}

// take over ownership of a journey
function journeys_claim(journeyName, userID) {
	journeys.journeys[journeyName].creator = userID;
}

// JOURNEY CREATION AND EDITING -----------------------------------------------

// \journey create <shortname>
function journeys_create(journeyName, userID) {
	journeyName = stringutil.lowerCase(journeyName);
	// if a journey already exists with that name, return false
	if (journeys.journeys[journeyName] != null) return false;

	// create a journey with that name and fill in some basic info:
	journeys.journeys[journeyName] = {
		tags: [],
		name: journeyName,
		notes: '',
		creator: userID,
		createdate: Date.now(),
		published: false,
		origin: '',
		destination: '',
		eventtype: 'journeyevents',
		image: '',
		quick: false,
		steps: []
	};
	journeys_save();
	return true;
}

function journeys_create_quick(userID, length, type, terrain, peril, eventtype) {
	// pick a journey name
	let i = 1;
	while (journeys.journeys['quickjourney' + String(i)] != null) {
		i++;
	}
	let journeyName = 'quickjourney' + String(i);

	// create the journey
	if (!journeys_create(journeyName, userID)) return false;

	// give it a name
	let name = slashcommands.localizedString(locale, 'report-joiner',[]);
	if (name == '') name = stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'journey')) + ' ' + name + ' ' + stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'journey','quick'));
	else name = stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'journey','quick')) + ' ' + stringutil.titleCaseWords(slashcommands.localizedCommand(locale, 'journey'));
	journeys.journeys[journeyName].name = name;

	// set it to a quick journey
	journeys.journeys[journeyName].quick = true;
	journeys.journeys[journeyName].eventtype = eventtype;

	// handle Moria quick journeys
	if (type == 'moria') {
		journeys.journeys[journeyName].tags = ['moria','underground'];
		type = 'wild';
	}

	// add a step
	journeys_add_step(journeyName, userID, null, length, type, terrain, peril, '', true);
	return journeyName;
}

// determine if a journey can be read or written by a particular user
function journeys_accessible(journeyName, userID, hostUser, usersLM, type) {
	// if the journey doesn't exist, you can't access it
	if (journeys.journeys[journeyName] == null) return false;
	// the host user can do everything to all journeys
	if (userID == hostUser) return true;
	// if the journey is owned by this user, they can do anything
	if (journeys.journeys[journeyName].creator == userID) return true;
	// if we're asking for write access, and no earlier test passed, they can't do it
	if (type == 'write') return false;
	// so we're asking for read now, and it's not the user's own journey
	// if userID's current game's LM is the creator then return true;
	if (usersLM == journeys.journeys[journeyName].creator) return true;
	// otherwise it only depends on whether the journey is published, 
	if (journeys.journeys[journeyName].published) return true;
	return false;
}

// basic journey editing: name, notes, tag, origin, destination, eventtype, image, published
function journeys_edit(journeyName, userID, hostUser, field, value) {
	if (!journeys_accessible(journeyName, userID, hostUser, null, 'write')) return 'journey-notexists';
	if (journeys.journeys[journeyName].quick) return 'journey-quick';
	// validation
	if (field == 'name' || field == 'notes' || field == 'origin' || field == 'destination' || field == 'eventtype') value = stringutil.mildSanitizeString(value);
	if (field == 'image' && !value.startsWith('http://') && !value.startsWith('https://' )) return 'error-badurl';
	if (field == 'name' && value.length > 120) return 'journey-nametoolong';
	if (field == 'name' && value.length > 60) return 'journey-nametoolong';
	if (field == 'eventtype' && !value.match(/^[a-zA-Z\u00C0-\u024F][0-9a-zA-Z\u00C0-\u024F\_]+$/)) return 'error-alphanumeric';
	if (field == 'tags') {
		let r = [];
		let problem = null;
		value.split(' ').forEach(tag => {
			if (!journeys_tag_verify(tag)) problem = 'error-unknowntag';
			r.push(tag);
		});
		if (problem) return problem;
		value = r;
	}
	if (field == 'published' && value) {
		// must have a name that's not the same as the shortname
		if (journeys.journeys[journeyName].name == '' || journeys.journeys[journeyName].name == journeyName) {
			return 'journey-noname';
		}
		// must have at least one tag
		if (journeys.journeys[journeyName].tags.length == 0) {
			return 'journey-notags';
		}
		// must have at least one step
		if (journeys.journeys[journeyName].steps.length == 0) {
			return 'journey-nosteps',[];
		}
		// must have origin and destination
		if (journeys.journeys[journeyName].origin == '' || journeys.journeys[journeyName].destination == '') {
			return 'journey-noends';
		}
	}
	journeys.journeys[journeyName][field] = value;
	journeys_save();
	return '';
}

// steps: show, add, and clear

function journeys_count_steps(journeyName) {
	if (!journeys.journeys[journeyName] || !journeys.journeys[journeyName].steps) return 0;
	return journeys.journeys[journeyName].steps.length;
}

// \journey <j> step <length> [border|wild|dark] [hard|road] [peril=<num>] [\<comment>]
function journeys_add_step(journeyName, userID, hostUser, length, type, terrain, peril, comment, creating) {
	if (!journeys_accessible(journeyName, userID, hostUser, null, 'write')) return 'journey-notexists';
	if (!creating && journeys.journeys[journeyName].quick) return 'journey-quick';
	if (type == null) type = 'wild';
	if (terrain == null) terrain = 'none';
	if (isNaN(length) || length <= 0) length = 1;
	let newStep = {
		'length': length,
		'type': type,
		'terrain':terrain,
		'peril': peril,
		'comment': comment.trim()
	};
	journeys.journeys[journeyName].steps.push(newStep);
	journeys_save();
	return journeys_count_steps(journeyName);
}

function journeys_remove_step(journeyName, userID, hostUser, stepnum) {
	if (!journeys_accessible(journeyName, userID, hostUser, null, 'write')) return 'journey-notexists';
	if (journeys.journeys[journeyName].quick) return 'journey-quick';
	if (journeys_count_steps(journeyName) < stepnum) return 'journey-nosuchstep';
	let newSteps = [], i = 0;
	journeys.journeys[journeyName].steps.forEach(step => {
		if (++i != stepnum) newSteps.push(step);
	});
	journeys.journeys[journeyName].steps = [...newSteps];
	journeys_save();
	return journeys_count_steps(journeyName);
}

function journeys_clear_steps(journeyName, userID, hostUser) {
	if (!journeys_accessible(journeyName, userID, hostUser, null, 'write')) return 'journey-notexists';
	if (journeys.journeys[journeyName].quick) return 'journey-quick';
	journeys.journeys[journeyName].steps = [];
	journeys_save();
	return '';
}

function journeys_show_steps(journeyName, userID, hostUser, locale) {
	let r = '', length = 0, i = 0;
	journeys.journeys[journeyName].steps.forEach(step => {
		r += ':motorway: `[' + String(++i) + ']` ' + journeys_show_step(step, locale) + '\n';
		length += step.length;
	});
	return r + slashcommands.localizedString(locale, 'total',[]) + ' **' + String(length) + '** ' + slashcommands.localizedString(locale, 'journey-hexes',[]);
}

function journeys_show_step(step, locale) {
	let r = String(step.length) + ' ' + slashcommands.localizedString(locale, 'journey-hexes',[]) + ' ';
	if (step.terrain == 'hard') r += slashcommands.localizedString(locale, 'journey-terrain-hard') + ' ';
	r += slashcommands.localizedString(locale, 'journey-type-' + step.type);
	if (step.terrain == 'road') r += ' ' + slashcommands.localizedString(locale, 'journey-terrain-road');
	if (step.peril > 0) r += ', ' + slashcommands.localizedOption(locale, 'jedit', 'add', 'peril') + ' ' + String(step.peril);
	if (step.comment != '') r += ': ' + step.comment;
	return r;
}

// \journey delete confirm
function journeys_delete(journeyName, userID, hostUser) {
	if (!journeys_accessible(journeyName, userID, hostUser, null, 'write')) return 'journey-notexists';
	// do not allow if it's published
	if (journeys.journeys[journeyName].published) return 'journey-unpublishfirst';
	journeys_delete_immediately(journeyName);
	return '';
}

// directly delete
function journeys_delete_immediately(journeyName) {
	delete journeys.journeys[journeyName];
	journeys_save();
}

function journeys_lookup(journeyName, field) {
	if (journeys.journeys[journeyName] == undefined) return null;
	return journeys.journeys[journeyName][field];
}

// /journey copy and journey reverse
function journeys_copy(journeyName, newJourneyName, userID, locale, reverse) {
	if (journeys.journeys[newJourneyName] != null) return 'journey-exists';
	// build the copy
	journeys.journeys[newJourneyName] = {
		tags: journeys.journeys[journeyName].tags,
		name: journeys.journeys[journeyName].name,
		notes: String(journeys.journeys[journeyName].notes + ' ' + slashcommands.localizedString(locale, 'journey-copynote',[journeyName]) ).trim(),
		creator: userID,
		createdate: Date.now(),
		published: false,
		origin: journeys.journeys[journeyName].origin,
		destination: journeys.journeys[journeyName].destination,
		eventtype: journeys.journeys[journeyName].eventtype,
		image: journeys.journeys[journeyName].image,
		steps: []
	};
	if (reverse) {
		for (let i=journeys.journeys[journeyName].steps.length - 1; i>=0; i--) {
			journeys.journeys[newJourneyName].steps.push(journeys.journeys[journeyName].steps[i]);
		}
		let swap = journeys.journeys[newJourneyName].origin;
		journeys.journeys[newJourneyName].origin = journeys.journeys[newJourneyName].destination;
		journeys.journeys[newJourneyName].destination = swap;
	} else {
		for (let i=0; i < journeys.journeys[journeyName].steps.length; i++) {
			journeys.journeys[newJourneyName].steps.push(journeys.journeys[journeyName].steps[i]);
		}
	}
	journeys_save();
	return '';
}

// /journey combine
function journeys_combine(journeyName, journeyName2, newJourneyName, userID, locale) {
	if (journeys.journeys[newJourneyName] != null) return 'journey-exists';
	// build the copy
	journeys.journeys[newJourneyName] = {
		tags: journeys.journeys[journeyName].tags,
		name: journeys.journeys[journeyName].name,
		notes: String(journeys.journeys[journeyName].notes + ' ' + journeys.journeys[journeyName2].notes + ' ' + slashcommands.localizedString(locale, 'journey-combinenote',[journeyName,journeyName2])).trim(),
		creator: userID,
		createdate: Date.now(),
		published: false,
		origin: journeys.journeys[journeyName].origin,
		destination: journeys.journeys[journeyName2].destination,
		eventtype: journeys.journeys[journeyName2].eventtype,
		image: journeys.journeys[journeyName].image,
		steps: []
	};
	for (let i=0; i < journeys.journeys[journeyName].steps.length; i++) {
		journeys.journeys[newJourneyName].steps.push(journeys.journeys[journeyName].steps[i]);
	}
	for (let i=0; i < journeys.journeys[journeyName2].steps.length; i++) {
		journeys.journeys[newJourneyName].steps.push(journeys.journeys[journeyName2].steps[i]);
	}
	journeys_save();
	return '';
}


// JOURNEY LISTS AND SHOW -----------------------------------------------------

// /journey list [mine] [tag=<tag>] [search-text] - lists all journeys that match (only shows mine or published, except that I can see them all)
function journeys_list(userID, hostUser, usersLM, locale, mine, published, searchtext, tags) {
	let matches = [];
	// go through the journeys checking both accessibility and the search terms
	for (let journeyName in journeys.journeys) {
		let listIt = true;
		if (!journeys_accessible(journeyName, userID, hostUser, usersLM, 'read')) listIt = false;
		if (mine === true && journeys.journeys[journeyName].creator != userID) listIt = false;
		if (mine === false && journeys.journeys[journeyName].creator == userID) listIt = false;
		if (published === true && !journeys.journeys[journeyName].published) listIt = false;
		if (published === false && journeys.journeys[journeyName].published) listIt = false;
		if (tags) tags.split(' ').forEach(tag => {
			if (!journeys.journeys[journeyName].tags.includes(tag)) listIt = false;
		});
		if (searchtext) searchtext.split(' ').forEach(term => {
			if (!journeys_search_term(journeyName,term)) listIt = false;
		});
		if (listIt) matches.push(journeyName);
	}
	return journeys_display_list(matches, locale);
}

function journeys_display_list(matches, locale) {
	if (matches.length == 0) return slashcommands.localizedString(locale, 'journey-listnomatch',[]);
	let reportHeadings = [
		{title:stringutil.titleCase(slashcommands.localizedCommand(locale, 'journey')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'jedit', 'origin')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'jedit', 'destination')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'jedit', 'eventtype')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'jedit', 'name')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'jedit','steps')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale,'journey-hexes',[])), emoji:false, minWidth:0, align:'left'},
		{title:'Pub', emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	matches.forEach(journeyName => {
		reportData.push([
			journeyName, 
			stringutil.trimRight(journeys.journeys[journeyName].origin,14),
			stringutil.trimRight(journeys.journeys[journeyName].destination,14),
			journeys.journeys[journeyName].eventtype,
			stringutil.trimRight(journeys.journeys[journeyName].name, 30),
			journeys.journeys[journeyName].steps.length,
			journeys_length(journeyName),
			journeys.journeys[journeyName].published ? ':ledger:' : ''
		]);
	});
	return table.buildTable(reportHeadings, reportData, 2) + '\n' + slashcommands.localizedString(locale, 'journey-matched',[matches.length, Object.keys(journeys.journeys).length]);
}

// check all the relevant fields of a journey for a search term
function journeys_search_term(journeyName, searchTerm) {
	searchTerm = stringutil.lowerCase(searchTerm);
	if (stringutil.lowerCase(journeyName).includes(searchTerm)) return true;
	if (stringutil.lowerCase(journeys.journeys[journeyName].name).includes(searchTerm)) return true;
	if (stringutil.lowerCase(journeys.journeys[journeyName].notes).includes(searchTerm)) return true;
	if (stringutil.lowerCase(journeys.journeys[journeyName].origin).includes(searchTerm)) return true;
	if (stringutil.lowerCase(journeys.journeys[journeyName].destination).includes(searchTerm)) return true;
	if (stringutil.lowerCase(journeys.journeys[journeyName].eventtype).includes(searchTerm)) return true;
	let result = false;
	journeys.journeys[journeyName].steps.forEach(step => {
		if (stringutil.lowerCase(step.comment).includes(searchTerm)) result = true;
	});
	return result;
}

// shows a journey
function journeys_show(journeyName, locale, userID, hostUser, userNameFromID) {

	let t = journeys.journeys[journeyName];
	if (t == undefined) return slashcommands.localizedString(locale, 'journey-notexists',[journeyName]);

	// build the introduction
	let r = '**' + t.name + '** (`' + journeyName + '`)' + (t.published ? ' :ledger:' : '') + '\n';
	if (t.tags.length > 0) r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale,'table','tags')) + '__: `' + t.tags.join('`, `') + '`\n';
	r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'created', [])) +'__: <t:' + String(Math.floor(t.createdate/1000)) + ':F> ' + slashcommands.localizedString(locale, 'by', []) +' ' + userNameFromID(t.creator) + '\n';
	r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'jedit', 'origin')) +  '__: ' + t.origin + '\n';
	r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'jedit', 'destination')) +  '__: ' + t.destination + '\n';
	r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'jedit', 'eventtype')) +  '__: ' + t.eventtype + '\n';
	r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'jedit', 'image')) +  '__: ' + t.image + '\n';

	// show the steps
	r += journeys_show_steps(journeyName, userID, hostUser, locale);

	return r +  '\n' + t.notes + '\n';
}

function journeys_image(journeyName) {
	return journeys.journeys[journeyName].image;
}

function journeys_fullname(journeyName) {
	return journeys.journeys[journeyName].name;
}

function journeys_has_tag(journeyName, tag) {
	if (journeys.journeys[journeyName].tags.includes(tag)) return true;
	return false;
}

// RUNNING JOURNEYS -----------------------------------------------------------

// journey status
function journeys_status(g, locale) {
	if (g.journey.active == '' || g.journey.active == null || journeys.journeys[g.journey.active] == undefined) return slashcommands.localizedString(locale,'journey-notexists',[]);

	let event = g.journey.event;
	// figure out what step we are on and how far into it
	let stepnum = journeys_on_step(g.journey.active, g.journey.progress, true);
	if (stepnum < 0 || stepnum >= journeys.journeys[g.journey.active].steps.length) return '';
	let step = journeys.journeys[g.journey.active].steps[stepnum];

	// build an introduction that shows current status
	// __Status__: Journey `name`, progress **4**/17 hexes (23%), on step 3: 2 hexes hard dark: Crossing the Loudwater
	let percentage = Math.floor((g.journey.progress * 100 / journeys_length(g.journey.active)) + 0.5);
	return slashcommands.localizedString(locale, 'journey-statusline', [g.journey.active, g.journey.progress, journeys_length(g.journey.active), percentage, String(stepnum+1)]) + journeys_show_step(step, locale);
}

// does endurance of the ranger apply to this player right now?
function enduranceOfTheRanger(g, p) {
	if (!f_hasTrait(g, p, 'virtue', 'endurance of the ranger')) return false;
	if (f_characterDataFetchHandleNumbers(g, p, 'sheet', 'armour') > 2) return false;
	if (f_characterDataFetchHandleNumbers(g, p, 'sheet','shield')) return false;
	let helm = f_characterDataFetchHandleNumbers(g, p, 'sheet','helm');
	if (helm[0] != 0 || helm[1] != 0) return false;
	return true;
}

// buttons for journey rolls
const BUTTON_PLAIN    = '\uD83D\uDEB6';  // U+1F6B6 person walking
const BUTTON_HOPE     = '\uD83C\uDFC3';  // U+1F3C3 person running
const BUTTON_INSPIRED = '\uD83E\uDD38';  // U+1F938 person cartwheeling
const BUTTON_MAGICAL  = '\uD83E\uDD39';  // U+1F939 person juggling 

// build the command data for all four buttons for each user who could click them
function saveCommandData(userID, skill) {
	let data = {
		"type":1,"options":[{"value":"skill","type":3,"name":"test"},{"value":skill,"type":3,"name":"skill"},{"value":"journey","type":3,"name":"affects"}],"name":"roll","id":"","guild_id":""
	};
	f_setUserConfig(userID, 'button-journey-plain', data);
	data = {
		"type":1,"options":[{"value":"skill","type":3,"name":"test"},{"value":skill,"type":3,"name":"skill"},{"value":"journey","type":3,"name":"affects"},{"value":"hope","type":3,"name":"hope"}],"name":"roll","id":"","guild_id":""
	};
	f_setUserConfig(userID, 'button-journey-hope', data);
	data = {
		"type":1,"options":[{"value":"skill","type":3,"name":"test"},{"value":skill,"type":3,"name":"skill"},{"value":"journey","type":3,"name":"affects"},{"value":"inspired","type":3,"name":"hope"}],"name":"roll","id":"","guild_id":""
	};
	f_setUserConfig(userID, 'button-journey-inspired', data);
	data = {
		"type":1,"options":[{"value":"skill","type":3,"name":"test"},{"value":skill,"type":3,"name":"skill"},{"value":"journey","type":3,"name":"affects"},{"value":"magical","type":3,"name":"hope"}],"name":"roll","id":"","guild_id":""
	};
	f_setUserConfig(userID, 'button-journey-magical', data);
}

function userIDfromCharacterID(g, charID) {
	let userID = null;
	for (player in g.players) {
		g.players[player].characters.forEach(c => {
			if (String(c) == String(charID)) {
				userID = player;
			}
		});
	}
	if (userID) return userID;
	g.lmCharacters.forEach(c => {
		if (String(c) == String(charID)) {
			userID = g.loremaster;
		}
	});
	if (userID) return userID;
	return null;
}

// builds an embed for a journey action like marching or an event test, with relevant buttons
function buildJourneyActionEmbed(locale, g, curStatus, peopleInRole, title, skill) {

	// get a list of the user IDs of the people in the role
	let userIDs = [];
	// if no one is in the role, put in everyone
	if (peopleInRole.length == 0) {
		for (let p in g.characters) {
			if (!g.characters[p].active || g.characters[p].dreaming) continue;
			userIDs.push(userIDfromCharacterID(g, p));
		}
	} else peopleInRole.forEach(ch => {
		let userID = journeys_userID_from_char_name(g, ch);
		userIDs.push(userID);
	});
	// save the commands to the LM since it could be clicked by any player
	saveCommandData(g.loremaster, skill);

	// build an array of buttons
	let buttons = [];
	buttons.push({
				'command':'button-journey-plain',
				'color':2,
				'label':'journey-button-plain',
				'extratext':'',
				'emoji':BUTTON_PLAIN
				});
	if (g.edition != 'strider') {
		buttons.push({
					'command':'button-journey-hope',
					'color':2,
					'label':'journey-button-hope',
					'extratext':'',
					'emoji':BUTTON_HOPE
					});
		}
	buttons.push({
				'command':'button-journey-inspired',
				'color':2,
				'label':'journey-button-inspired',
				'extratext':'',
				'emoji':BUTTON_INSPIRED
				});
	buttons.push({
				'command':'button-journey-magical',
				'color':2,
				'label':'journey-button-magical',
				'extratext':'',
				'emoji':BUTTON_MAGICAL
				});

	return embeds.buildDialog(
		locale, g, f_getGameName(g), userIDs,
		title,
		'https://creazilla-store.fra1.digitaloceanspaces.com/emojis/55907/national-park-emoji-clipart-md.png',
		'https://bitbucket.org/HawthornThistleberry/narvi/src/master/PlayerGuide.md',
		curStatus,
		'',
		embeds.BUTTON_GREYPLE,
		buttons
		);
}

// builds an embed for arrival
function buildJourneyArrivalEmbed(locale, g, curStatus, title) {

	// get a list of the user IDs of the people in the journey
	let userIDs = [];
	for (let p in g.characters) {
		if (!g.characters[p].active || g.characters[p].dreaming) continue;
		userIDs.push(userIDfromCharacterID(g, p));
	}
	// save the commands to the LM since it could be clicked by any player
	let data = {
		"type":1,"options":[{"value":"arrival","type":3,"name":"test"}],"name":"roll","id":"","guild_id":""
	};
	f_setUserConfig(g.loremaster, 'button-arrival-plain', data);
	data = {
		"type":1,"options":[{"value":"arrival","type":3,"name":"test"},{"value":"hope","type":3,"name":"hope"}],"name":"roll","id":"","guild_id":""
	};
	f_setUserConfig(g.loremaster, 'button-arrival-hope', data);
	data = {
		"type":1,"options":[{"value":"arrival","type":3,"name":"test"},{"value":"inspired","type":3,"name":"hope"}],"name":"roll","id":"","guild_id":""
	};
	f_setUserConfig(g.loremaster, 'button-arrival-inspired', data);
	data = {
		"type":1,"options":[{"value":"arrival","type":3,"name":"test"},{"value":"magical","type":3,"name":"hope"}],"name":"roll","id":"","guild_id":""
	};
	f_setUserConfig(g.loremaster, 'button-arrival-magical', data);

	// build an array of buttons
	let buttons = [];
	buttons.push({
				'command':'button-arrival-plain',
				'color':2,
				'label':'journey-button-plain',
				'extratext':'',
				'emoji':BUTTON_PLAIN
				});
	if (g.edition != 'strider') {
		buttons.push({
					'command':'button-arrival-hope',
					'color':2,
					'label':'journey-button-hope',
					'extratext':'',
					'emoji':BUTTON_HOPE
					});
		}
	buttons.push({
				'command':'button-arrival-inspired',
				'color':2,
				'label':'journey-button-inspired',
				'extratext':'',
				'emoji':BUTTON_INSPIRED
				});
	buttons.push({
				'command':'button-arrival-magical',
				'color':2,
				'label':'journey-button-magical',
				'extratext':'',
				'emoji':BUTTON_MAGICAL
				});

	return embeds.buildDialog(
		locale, g, f_getGameName(g), userIDs,
		title,
		'https://creazilla-store.fra1.digitaloceanspaces.com/emojis/55907/national-park-emoji-clipart-md.png',
		'https://bitbucket.org/HawthornThistleberry/narvi/src/master/PlayerGuide.md',
		curStatus,
		'',
		embeds.BUTTON_GREYPLE,
		buttons
		);
}

// figure out what should happen next in the journey, set things up accordingly, and return something to display
function journeys_next(g, locale, displayOnly) {

	if (g.journey.active == '' || g.journey.active == null || journeys.journeys[g.journey.active] == undefined) return slashcommands.localizedString(locale,'journey-notexists',[]);

	let r = '';
	let event = g.journey.event;
	let weather = null;
	// figure out what step we are on and how far into it
	let stepinc = journeys.journeys[g.journey.active].steps[journeys_on_step(g.journey.active, g.journey.progress, true)];
	if (stepinc == null) return buildJourneyArrivalEmbed(locale, g, slashcommands.localizedString(locale,'journey-arrival',[]), stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'roll', 'arrival')));
	let stepexc = journeys.journeys[g.journey.active].steps[journeys_on_step(g.journey.active, g.journey.progress, false)];
	let curStatus = '__' + slashcommands.localizedString(locale,'journey-status',[]) + '__: ' + journeys_status(g, locale) + '\n';

	if (event == 'marching') { // a marching test is now due but has not yet been done
		if (g.config.includes('journey-weather')) {
			if (!displayOnly) {
				let roll, rollLine, r, x;
				[roll, rollLine, r, x] = regdice.polyhedralDieRoll(['2d6'], '', '', locale);
				g.weather = hexflower.nextHex(g.weather, roll);
			}
			weather = hexflower.showHex('weather',g.weather,'');
		}

		if (stepexc.peril > 0 && g.journey.peril == -1) { // we're starting a peril section
			g.journey.peril = stepexc.peril;
			//console.log('TRACE: entering peril ' + g.journey.peril + 'in _next: progress now ' + g.journey.progress);
			if (!displayOnly) {
				r += slashcommands.localizedString(locale,'journey-log-enterperil',[stepexc.peril]) + '\n';
				f_journey_log(g, locale, "enterperil", [stepexc.peril]);
			}
			g.journey.event = 'target';
			event = 'target'; 
			// fall through to the event==target block below
		} else {
			g.journey.skill = 'travel';
			let peopleInRole = [...f_charactersInJourneyRole(g, 'guide')];
			if (!displayOnly) f_journey_log(g, locale, "marching", [peopleInRole.join(', ')]);
			curStatus += slashcommands.localizedString(locale,'journey-marchingtest',[peopleInRole.join(', ')]);
			let rtn = [];
			if (weather) rtn.push(weather);
			if (g.config.includes('journey-mention') && peopleInRole.length > 0) rtn.push(journeys_people_list(g, peopleInRole));
			rtn.push(buildJourneyActionEmbed(locale, g, curStatus, peopleInRole, slashcommands.localizedString(locale,'journey-title-marching',[]),'travel'));
			return rtn;
		}
	}

	if (event == 'target') {
		let peopleInRole, error, range, code, text, finalroll, rollstrings, newTableName;
		if (g.edition != 'strider') {
			// roll to select a target, and find who are the people who can do the roll on that
			let tableName = 'journeytarget';
			if (journeys_has_tag(g.journey.active, 'moria')) tableName = 'moriajourneytarget';
			[error, range, code, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll(tableName, locale, '', null, false, f_torRollDice);
			if (error != null) return slashcommands.localizedString(locale, error, [tableName]);
			r += '__journeytarget__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
			// note what skill must be rolled on
			g.journey.skill = skillFromRole[code];
			peopleInRole = [...f_charactersInJourneyRole(g, code)];
		}

		// roll the type of journey event -- favored or ill-favored depending on type, also allowing the override
		let type = g.journey.typeoverride;
		if (type == null) type = '';
		// Memory of Ancient Days: if the person targeted has this virtue, the roll is favored
		for (let p in g.characters) {
			if (f_hasTrait(g, p, 'virtue', 'memory of ancient days') && (g.characters[p].journeyRole.includes(code) || g.edition == 'strider')) {
				if (type == 'wild') type = 'border';
				else if (type == 'dark') type = 'wild';
			}
		}
		if (type == '') type = stepinc.type;
		let feat = null;
		if (type == 'border') feat = 'favoured';
		else if (type == 'dark') feat = 'illfavoured';
		if (g.journey.favourable) {
			feat = 'favoured';
			g.journey.favourable = false;
		}
		/*
		let tableName = 'journeyevents';
		if (journeys_has_tag(g.journey.active, 'moria')) tableName = 'moriajourneyevents';
		if (g.edition == 'strider') tableName = 'solojourney';
		*/
		let tableName = g.journey.eventtype;
		[error, range, g.journey.event, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll(tableName, locale, String(g.journey.featmod), feat, false, f_torRollDice);
		if (error != null) return slashcommands.localizedString(locale, error, [tableName]);
		r += '__' + tableName + '__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
		let eventName = text.split(":")[0];
		let fatigue = Number(text.slice(-10).split(' ')[0]);
		if (isNaN(fatigue)) { // should never happen
			//console.log('fatigue=' + fatigue + ' parse=' + text.slice(-10).split(' ')[0] + ' full=' + text); 
			fatigue = 0;
		}

		// strider mode uses different tables for the target
		text = '';
		if (g.edition == 'strider') {
			tableName = g.journey.event;
			// if table is broken, fall back on the default table
			if (lookuptables.tables_validation(tableName, locale) != '') {
				tableName = 'journeytarget';
				if (journeys_has_tag(g.journey.active, 'moria')) tableName = 'moriajourneytarget';
			}
			[error, range, g.journey.skill, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll(tableName, locale, String(g.journey.featmod), feat, false, f_torRollDice);
			if (error != null) return slashcommands.localizedString(locale, error, [tableName]);
			r += '__' + g.journey.event + '__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
			peopleInRole = [...f_charactersInJourneyRole(g, 'travel')]; // any skill will do in Strider mode since you'll match them all
			code = slashcommands.localizedString(locale,'player',[]);
		}

		// apply pendingFatigue (accounting for various virtues)
		let pendingPendingFatigue = {};
		for (let p in g.characters) {
			if (g.characters[p].active == false) continue;
			let thisFatigue = fatigue;
			if (thisFatigue && g.characters[p].journeyRole.includes('absent')) thisFatigue = 0;
			// Endurance of the Ranger eliminate if leather or less armor and no shield
			if (thisFatigue && enduranceOfTheRanger(g, p)) thisFatigue = 0;
			// Twice-Baked Honey-Cakes, Lembas, Cram reduces by 1
			if (thisFatigue && (f_hasTrait(g, p, 'virtue', 'twice-baked honey-cakes') || f_hasTrait(g, p, 'virtue', 'lembas') || f_hasTrait(g, p, 'virtue', 'cram'))) thisFatigue--;
			g.characters[p].pendingFatigue += thisFatigue;
			pendingPendingFatigue[p] = thisFatigue; // in case of a chance meeting
		}
		g.journey.pendingPendingFatigue = pendingPendingFatigue;

		// log the target, event, and who can roll (except if display only)
		if (!displayOnly) {
			if (text != '') eventName += ' (' + text + ')';
			f_journey_log(g, locale, "targetevent", [eventName, stringutil.titleCase(code), peopleInRole.join(', ')]);
		}

		curStatus += r + slashcommands.localizedString(locale, (g.journey.skill == 'noteworthy' ? "journey-noteworthy" : "journey-rollevent"),[stringutil.titleCase(code), peopleInRole.join(', '), stringutil.titleCase(g.journey.skill), eventName, g.journey.skill]);
		if (g.journey.skill == 'noteworthy') return (weather ? [weather, curStatus] : curStatus);
		let rtn = [];
		if (weather) rtn.push(weather);
		if (g.config.includes('journey-mention') && peopleInRole.length > 0) rtn.push(journeys_people_list(g, peopleInRole));
		rtn.push(buildJourneyActionEmbed(locale, g, curStatus, peopleInRole, slashcommands.localizedString(locale,'journey-title-event',[journeys_event_name(g.journey.event), stringutil.titleCase(code), stringutil.makeSmallCaps(stringutil.titleCase(g.journey.skill))]), g.journey.skill));
		return rtn;
	}

	if (eventtests.includes(g.journey.event)) {
		let code = '';
		for (let c in skillFromRole) {
			if (skillFromRole[c] == g.journey.skill) code = c;
		}
		let peopleInRole = [...f_charactersInJourneyRole(g, code)];
		curStatus += r + slashcommands.localizedString(locale, (g.journey.skill == 'noteworthy' ? "journey-noteworthy" : "journey-rollevent"),[stringutil.titleCase(code), peopleInRole.join(', '), stringutil.makeSmallCaps(stringutil.titleCase(g.journey.skill)), journeys_event_name(g.journey.event), g.journey.skill]);
		let rtn = [];
		if (weather) rtn.push(weather);
		if (g.config.includes('journey-mention') && peopleInRole.length > 0) rtn.push(journeys_people_list(g, peopleInRole));
		rtn.push(buildJourneyActionEmbed(locale, g, curStatus, peopleInRole, slashcommands.localizedString(locale,'journey-title-event',[journeys_event_name(g.journey.event), stringutil.titleCase(code), stringutil.makeSmallCaps(stringutil.titleCase(g.journey.skill))]), g.journey.skill));
		return rtn;
	}
}

// determine what modifiers apply to a particular roll
function journeys_modifier_dice(g, userID) {
	if (eventtests.includes(g.journey.event)) { // events (e.g., terrible misfortune) are influenced by the terrain
		// find the current step and see if it's hard or road
		let event = g.journey.event;
		// figure out what step we are on and how far into it
		let stepnum = journeys_on_step(g.journey.active, g.journey.progress, true);
		let step = journeys.journeys[g.journey.active].steps[stepnum];
		if (step.terrain == 'road') return 1;
		if (step.terrain == 'hard') return -1;
	}
	// marching tests are not modified, I think!
	return 0;
}

// someone has done a /roll or /skill that should affect the journey; resolve the result
function journeys_apply_roll(g, charID, charName, successes, locale, channelID) {

	if (g.journey.active == '' || g.journey.active == null || journeys.journeys[g.journey.active] == undefined) return slashcommands.localizedString(locale,'journey-notexists',[]);

	let r = '';
	let journeyName = g.journey.active;

	// save the current status for undo purposes
	journeys_save_undo(g);

	if (g.journey.event == 'marching') {
		// we can't be in peril -- no marching tests in peril, so that was prevented before we got here
		// figure out how far to advance
		let advance;
		if (successes) {
			advance = 2 + successes;
		} else {
			if (journeys_has_tag(g.journey.active, 'moria')) advance = 1;
			else advance = (g.journey.season == 'spring' || g.journey.season == 'summer') ? 2 : 1;
		}
		r += log_and_return(g, locale, 'advancing', [advance, charName]);
		// now do the advance, one hex at a time so we can be stopped by perilous areas or the journey's end
		let newStep, oldStep = journeys_on_step(journeyName, g.journey.progress, false);
		while (advance > 0) {
			g.journey.progress++;
			advance--;
			if (journey_over(journeyName, g.journey.progress)) return journeys_end(g, charName, locale, false, channelID);
			newStep = journeys_on_step(journeyName, g.journey.progress, false);
			if (newStep != oldStep) {
				// announce new step in the log and the return string
				r += log_and_return(g, locale, 'newstep', [journeys_show_step(journeys.journeys[journeyName].steps[newStep], locale)]);
				// if we just entered a perilous area
				if (journeys.journeys[journeyName].steps[newStep].peril > 0) {
					advance = 0; // this progress ends immediately on entering a perilous area
					g.journey.peril = journeys.journeys[journeyName].steps[newStep].peril;
					//console.log('TRACE: entering peril ' + g.journey.peril + ' in _apply_roll: progress now ' + g.journey.progress);
					r += log_and_return(g, locale, 'enterperil', [journeys.journeys[journeyName].steps[newStep].peril]);
				}
				oldStep = newStep; // so we can have more than one new step occurrence during this advance
			}
		}
		// set up for doing the target and event rolls and then hand off to journeys_next to do those
		g.journey.event = 'target';
		let jN = journeys_next(g, locale, false);
		if (!Array.isArray(jN)) return r + jN;
		jN.unshift(r);
		return jN;
	}
	if (eventtests.includes(g.journey.event)) {
		// resolve the consequences
		switch (g.journey.event) {
		case 'terriblemisfortune': // If the roll fails, the target is Wounded.
		case 'deadlydark': // Deadly Dark: If the roll fails, the target is Wounded, and the Loremaster raises the Eye Awareness of the Company by 1.
			if (successes <= 0) {
				r += f_giveWound(g, charID, channelID, locale, 'untreated', 1, null, false);
				f_journey_log(g, locale, 'wounded', [charName]);
				if (g.journey.event == 'deadlydark') {
					r += f_increaseEyeRating(g, 1);
				}
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'fallingstones': // Falling Stones and Shifting Snow: If the roll fails, the target suffers a grievous Endurance loss from falling, and everyone else in the company suffers a severe one.
			if (successes <= 0) {
				let [dmg, r2] = f_enduranceLossDamage(3, 'falling', g, charID, locale, channelID);
				r += r2;
				for (let p in g.characters) {
					if (g.characters[p].active == false) continue;
					if (p == charID) continue; // the person who rolled already got hit
					let [dmg, r2] = f_enduranceLossDamage(2, 'falling', g, p, locale, channelID);
					r += r2;
				}
				r += log_and_return(g, locale, 'fallingstones', [charName]);
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'bittercold': // Bitter Cold: If the roll fails, the target suffers a severe Endurance loss from cold.
			if (successes <= 0) {
				let [dmg, r2] = f_enduranceLossDamage(2, 'cold', g, charID, locale, channelID);
				r += r2 + log_and_return(g, locale, 'bittercold', [charName]);
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'blizzard': // Blinding Blizzard: If the roll fails, add 1 day to the length of the journey, and everyone in the Company suffers a moderate Endurance loss from cold.
			if (successes <= 0) {
				g.journey.duration++;
				for (let p in g.characters) {
					if (g.characters[p].active == false) continue;
					let [dmg, r2] = f_enduranceLossDamage(1, 'cold', g, p, locale, channelID);
					r += r2;
				}
				r += log_and_return(g, locale, 'blizzard', [charName]);
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'despair': // If the roll fails, everyone in the Company gains 1 Shadow point (Dread).
		case 'eerienoises': // If the roll fails, everyone in the Company gains 1 Shadow point (Dread).
		case 'longdark': // The Long Dark of Moria: If the roll fails, everyone in the company gains 2 Shadow points (Dread).
			if (successes <= 0) {
				r += log_and_return(g, locale, 'alldread', [g.edition == 'strider' ? '2' : '1']);
				for (let p in g.characters) {
					if (!g.characters[p].active) continue;
					g.characters[p].shadowTests.push({'type': 'dread', 'tn': null, 'responsible': 'journey', 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': null, 'points': (g.journey.event == 'longdark' ? 2 : 1)});
				}
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'attacked': // Attacked: If the roll fails, everyone in the Company must make a Protection roll (TN12) to avoid being Wounded.
			if (successes <= 0) {
				r += log_and_return(g, locale, 'allprotection', []);
				for (let p in g.characters) {
					if (!g.characters[p].active) continue;
					g.characters[p].protectionTests.push({'injury': 12, 'responsible': 'journey', 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': null, 'poison': 0, 'deadly': false});
				}
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'illchoices': // If the roll fails, the target gains 1 Shadow point (Dread).
		case 'watchfuleyes': // Watchful Eyes: If the roll fails, the target gains 1 Shadow point (Dread), and the Loremaster raises the Eye Awareness of the Company by 1.
			if (successes <= 0) {
				r += log_and_return(g, locale, 'dread', [charName]);
				g.characters[charID].shadowTests.push({'type': 'dread', 'tn': null, 'responsible': 'journey', 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': null, 'points': 1});
				if (g.journey.event == 'watchfuleyes') {
					r += f_increaseEyeRating(g, 1);
				}
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'mishap': // If the roll fails, add 1 day to the length of the journey, and the target gains 1 additional Fatigue.
		case 'branching': // Endlessly Branching Stairs and Passages: If the roll fails, add 1 day to the length of the journey, and thet arget gains 1 additional Fatigue. In addition, regardless of whether the roll is successful or not, roll on the Random Chamber Generator.
			if (successes <= 0) {
				g.journey.duration++;
				r += log_and_return(g, locale, 'addday', []);
				// apply one more fatigue to charID, but not with Endurance of the Ranger
				if (!enduranceOfTheRanger(g, charID)) {
					g.characters[charID].pendingFatigue++;
					r += log_and_return(g, locale, 'addedfatigue', [charName]);
				}
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			if (g.journey.event == 'branching') {
				let error, range, code, text, finalroll, rollstrings, newTableName;
				[error, range, code, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll('randomchambertype', locale, '', null, false, f_torRollDice);
				r += '__randomchambertype__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
				[error, range, code, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll('randomchambercondition', locale, '', null, false, f_torRollDice);
				r += '__randomchambercondition__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
				[error, range, code, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll('randomchamberappearance', locale, '', null, false, f_torRollDice);
				r += '__randomchamberappearance__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
				[error, range, code, text, finalroll, rollstrings, newTableName] = lookuptables.tables_roll('randomchamberchallenge', locale, '', null, false, f_torRollDice);
				r += '__randomchamberchallenge__: ' + rollstrings + ': `' + finalroll + '`' + (finalroll != range ? ' (' + range + ')' : '') + ' **' + text + '**\n';
			}	
			break;
		case 'badweather': // If the roll fails, everyone gains 1 additional Fatigue.
			if (successes <= 0) {
				// apply one more fatigue to everyone, but not with Endurance of the Ranger
				for (let p in g.characters) {
					if (g.characters[p].active == false) continue;
					if (!enduranceOfTheRanger(g, p)) {
						g.characters[p].pendingFatigue++;
						r += log_and_return(g, locale, 'addedfatigue', [f_characterDataFetch(g, p, 'name', null)]);
					}
				}
			} else r += log_and_return(g, locale, 'passed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'shortcut': // If the roll succeeds, reduce the length of the journey by 1 day.
		case 'rightway': // The Right Way: If the roll succeeds, reduce the length of the journey by 1 day.
			if (successes > 0) {
				g.journey.duration--;
				// in Moria solo, this also cancels the fatigue gain
				if (g.edition == 'strider') {
					// Back out all the pending pendingFatigue
					for (let p in g.journey.pendingPendingFatigue) {
						if (g.characters[p].active == false) continue;
						g.characters[p].pendingFatigue -= g.journey.pendingPendingFatigue[p];
					}
					g.journey.pendingPendingFatigue = {};
				}
				r += log_and_return(g, locale, 'subtractday', []);
			} else r += log_and_return(g, locale, 'failed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'chancemeeting': // If the roll succeeds, no Fatigue is gained, and the Loremaster improvises an encounter favouring the Company.
		case 'fairweather': // If the roll succeeds, no Fatigue is gained.
		case 'favourable': // If the roll succeeds, no Fatigue is gained, and the next roll is favoured.
			if (successes > 0) {
				// Back out all the pending pendingFatigue
				for (let p in g.journey.pendingPendingFatigue) {
					if (g.characters[p].active == false) continue;
					g.characters[p].pendingFatigue -= g.journey.pendingPendingFatigue[p];
				}
				g.journey.pendingPendingFatigue = {};
				if (g.journey.event == 'favourable') g.journey.favourable = true;
				r += log_and_return(g, locale, g.journey.event, []);
			} else r += log_and_return(g, locale, 'failed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'paththroughsnow': // If the roll succeeds, reduce the Fatigue gain of everyone in the Company except the target by 1 + Tengwar.
			if (successes > 0) {
				// Back out that much pending pendingFatigue
				for (let p in g.journey.pendingPendingFatigue) {
					if (g.characters[p].active == false) continue;
					if (p == charID) continue; // the person who rolled does not get this boost
					g.characters[p].pendingFatigue -= Math.min(successes, g.journey.pendingPendingFatigue[p]);
					g.journey.pendingPendingFatigue[p] -= Math.min(successes, g.journey.pendingPendingFatigue[p]);
				}
				r += log_and_return(g, locale, 'paththroughsnow', []);
			} else r += log_and_return(g, locale, 'failed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'joyfulsight': // If the roll succeeds, everyone in the Company regains 1 Hope.
		case 'gleamofhope': // If the roll succeeds, no Fatigue is gained and everyone in the Company regains 1 Hope.
			if (successes > 0) {
				for (let p in g.characters) {
					let gain = 1;
					if (f_hasTrait(g, p, 'virtue', 'the art of smoking')) gain++;
					r += f_tokenCommandExecute('gain', channelID, 'base', g, p, 'hope', '+', gain, false) + '\n';
				}
				if (g.journey.event == 'gleamofhope') {
					// Back out all the pending pendingFatigue
					for (let p in g.journey.pendingPendingFatigue) {
						if (g.characters[p].active == false) continue;
						g.characters[p].pendingFatigue -= g.journey.pendingPendingFatigue[p];
					}
					g.journey.pendingPendingFatigue = {};
					r += log_and_return(g, locale, 'gleamofhope', []);
				} else r += log_and_return(g, locale, 'gainhope', [charName]);
			} else r += log_and_return(g, locale, 'failed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'light': // If the roll succeeds, everyone in the Company removes 2 Shadow.
			if (successes > 0) {
				for (let p in g.characters) {
					r += f_tokenCommandExecute('lose', channelID, 'base', g, p, 'shadow', '-', 2, false) + '\n';
				}
				r += log_and_return(g, locale, 'light', []);
			} else r += log_and_return(g, locale, 'failed', [charName, journeys_event_name(g.journey.event)]);
			break;
		case 'dreadwonder': // Dread and Wonder of Moria: If the roll fails, the target gains 1 Shadow point (Dread). If the roll succeeds, everyone in the company gains 1 Hope instead.
			if (successes > 0) {
				for (let p in g.characters) {
					let gain = 1;
					if (f_hasTrait(g, p, 'virtue', 'the art of smoking')) gain++;
					r += f_tokenCommandExecute('gain', channelID, 'base', g, p, 'hope', '+', gain, false) + '\n';
				}
				r += log_and_return(g, locale, 'gainhope', [charName]);
			} else {
				r += log_and_return(g, locale, 'dread', [charName]);
				g.characters[charID].shadowTests.push({'type': 'dread', 'tn': null, 'responsible': 'journey', 'feat': null, 'bonus': 0, 'modifier': 0, 'condition': null, 'points': 1});
				r += log_and_return(g, locale, 'failed', [charName, journeys_event_name(g.journey.event)]);
			}
			break;
		}

		// if configured to do fatigue immediately instead of at journey's end, do it now
		if (g.config.includes('journey-tiring')) {
			r += journeys_apply_fatigue(g, locale, channelID);
		}

		// if in a perilous area, handle it counting down
		if (g.journey.peril > 0) {
			g.journey.peril--;
			if (g.journey.peril == 0) {
				// log exiting the peril
				r += log_and_return(g, locale, 'exitperil', []);
				// advance out of the perilous area step
				g.journey.progress += journeys.journeys[journeyName].steps[journeys_on_step(journeyName, g.journey.progress, false)].length;
				g.journey.peril = -1;
				//console.log('TRACE: exiting peril in _apply_roll: progress now ' + g.journey.progress);
			}
		}
		// if we are still in peril after the above
		if (g.journey.peril > 0) {
			// set up for another event
			g.journey.event = 'target';
		} else {
			// maybe the journey is over here!
			if (journey_over(journeyName, g.journey.progress)) {
				// we just hit the end of the journey
				return journeys_end(g, charName, locale, false, channelID);
			}
			// switch back to marching
			g.journey.event = 'marching';
		}
		let jN = journeys_next(g, locale, false);
		if (!Array.isArray(jN)) return r + jN;
		jN.unshift(r);
		return jN;
	}
	return journeys_next(g, locale, false);
}

// do the application of fatigue for the journey (so far, or at its end)
function journeys_apply_fatigue(g, locale, channelID) {
	if (g.journey.active == '' || g.journey.active == null || journeys.journeys[g.journey.active] == undefined) return slashcommands.localizedString(locale,'journey-notexists',[]);

	// apply (and clear) pendingFatigue, possibly increased by forced
	let r = '';
	let forcedFatigue = 0;
	if (g.journey.forced) forcedFatigue = Number(journeys_length(g.journey.active));
	for (let p in g.characters) {
		// endurance of the ranger should skip this even with a forced bonus
		if (!enduranceOfTheRanger(g, p)) {
			let fatigue = Number(g.characters[p].pendingFatigue) + forcedFatigue;
			if (isNaN(fatigue)) fatigue = 0;
			g.characters[p].pendingFatigue = 0; // so it's not double-dipped
			r += f_tokenCommandExecute('gain', channelID, 'base', g, p, 'fatigue', '+', fatigue, false) + '\n';
		} 
	}
	return r;
}

function journeys_end(g, charName, locale, abort, channelID) {
	let r = '';

	if (g.journey.active == '' || g.journey.active == null || journeys.journeys[g.journey.active] == undefined) {
		g.journey.active = '';
		return slashcommands.localizedString(locale,'journey-notexists',[]);
	}

	// log
	r += log_and_return(g, locale, (abort ? 'aborted' : 'ended'), [charName]);

	if (!abort) {
		r += journeys_apply_fatigue(g, locale, channelID);
		for (let p2 in g.characters) {
			g.characters[p2].unweary = 0;
		}
		g.songUsed = '';
		g.songBonus = 0;
			
		// calculate and display how long it took -- accounts for hard hexes, forced, duration (mods to the calculated duration), and horses
		let duration = journeys_length(g.journey.active);
		if (journeys_has_tag(g.journey.active, 'moria')) duration = Math.ceil(duration / 5); // 2mi hexes, so duration is 5 hexes a day
		// if forced, halve the above and round up
		if (g.journey.forced) duration = Math.ceil(duration / 2);
		// add in any shortcuts or ill choices
		duration += g.journey.duration;
		// add in 1 for every hard hex
		journeys.journeys[g.journey.active].steps.forEach(step => {
			if (step.terrain == 'hard') duration += step.length;
		});
		// do they all have horses? if so, halve it
		let allHorses = true;
		for (let p in g.characters) {
			let horse = Number(f_characterDataFetchHandleNumbers(g, p, 'steedvigour', null));
			if (journeys_has_tag(g.journey.active, 'underground') && !f_hasTrait(g, p, 'virtue', 'bree-pony')) horse = 0;
			if (horse == 0) allHorses = false;
		}
		if (g.journey.unmounted) allHorses = false;
		if (allHorses) duration = String(Math.ceil(duration / 2));
		// minimum 1
		if (duration < 1) duration = 1;
		// display regular and halved for horses
		r += log_and_return(g, locale, 'duration', [duration]);
		// advance wounds
		for (let p in g.characters) {
			let advanceText = f_advanceWounds(g, p, duration, locale, channelID);
			if (advanceText != '') r += f_characterDataFetch(g, p, 'name', null) + ': ' + advanceText + '.\n';
			advanceText = f_advancePoison(g, p, duration, locale, channelID);
			if (advanceText != '') r += f_characterDataFetch(g, p, 'name', null) + ': ' + advanceText + '.\n';
		}

		// tell everyone to /roll arrival to clear fatigue (or /company arrival)
		r = buildJourneyArrivalEmbed(locale, g, r + slashcommands.localizedString(locale,'journey-arrival',[]), stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'roll', 'arrival')));
	}

	if (journeys.journeys[g.journey.active].quick) {
		journeys_delete_immediately(g.journey.active);
		g.journey.journeys = g.journey.journeys.filter(x => (x != g.journey.active));
	}
	g.journey.active = '';
	return r;
}

// INTERNALS ------------------------------------------------------------------

function log_and_return(g, locale, logevent, params) {
	f_journey_log(g, locale, logevent, params);
	return slashcommands.localizedString(locale,'journey-log-' + logevent,params) + '\n';
}

function journeys_length(journeyName) {
	let length = 0;
	journeys.journeys[journeyName].steps.forEach(step => {
		length += step.length;
	});
	return length;
}

// figures out what step a journey is on
function journeys_on_step(journeyName, progress, inclusive) {
	let length = 0;
	for (let stepNum = 0; stepNum < journeys.journeys[journeyName].steps.length; stepNum++) {
		length += journeys.journeys[journeyName].steps[stepNum].length;
		//if (length == progress && journeys.journeys[journeyName].steps[stepNum].peril == 0) return stepNum;
		if (length > progress || (inclusive && length == progress)) return stepNum;
	}
	return journeys.journeys[journeyName].steps.length;
}

// determine if a journey is done
function journey_over(journeyName, progress) {
	if (progress >= journeys_length(journeyName)) return true;
	return false;
}

function journeys_event_name(event) {
	let eventName = event;
	if (eventName == 'terriblemisfortune') eventName = 'terrible misfortune';
	if (eventName == 'illchoices') eventName = 'ill choices';
	if (eventName == 'chancemeeting') eventName = 'chance meeting';
	if (eventName == 'joyfulsight') eventName = 'joyful sight';
	if (eventName == 'deadlydark') eventName = 'deadly dark';
	if (eventName == 'longdark') eventName = 'the long dark of moria';
	if (eventName == 'watchfuleyes') eventName = 'watchful eyes';
	if (eventName == 'branching') eventName = 'endlessly branching stairs and passages';
	if (eventName == 'rightway') eventName = 'the right way';
	if (eventName == 'dreadwonder') eventName = 'dread and wonder of moria';
	if (eventName == 'fallingstones') eventName = 'Falling Stones and Shifting Snow';
	if (eventName == 'eerienoises') eventName = 'Eerie Noises in the Darkness';
	if (eventName == 'bittercold') eventName = 'Bitter Cold';
	if (eventName == 'blizzard') eventName = 'Blinding Blizzard';
	if (eventName == 'paththroughsnow') eventName = 'A Path Through The Snow';
	if (eventName == 'gleamofhope') eventName = 'A Gleam of Good Hope';
	if (eventName == 'attacked') eventName = 'attacked';
	if (eventName == 'badweather') eventName = 'bad weather';
	if (eventName == 'fairweather') eventName = 'fair weather';
	if (eventName == 'favourable') eventName = 'Favourable Circumstances';
	if (eventName == 'light') eventName = 'A Light In The North';
	return stringutil.titleCaseWords(eventName);
}

function journeys_save_undo(g) {
	// save the current status for undo purposes
	let pendingFatigues = {};
	for (let p in g.characters) {
		pendingFatigues[p] = g.characters[p].pendingFatigue;
	}
	let hopes = {};
	for (let p in g.characters) {
		hopes[p] = f_characterDataFetchHandleNumbers(g, p, 'hope', null);
	}
	g.journey.undo = {
		'active': g.journey.active,
		'progress': g.journey.progress,
		'duration': g.journey.duration,
		'peril': g.journey.peril,
		'favourable': g.journey.favourable,
		'event': g.journey.event,
		'skill': g.journey.skill,
		'pendingFatigue': pendingFatigues,
		'pendingPendingFatigue': g.journey.pendingPendingFatigue,
		'hopes': hopes
	}
}

function journeys_userID_from_char_name(g, char) {
	let userID = 0;
	for (player in g.players) {
		g.players[player].characters.forEach(c => {
			if (f_characterDataFetch(g, c, 'name', null) == char) userID = player;
		});
	}
	if (userID == 0) {
		g.lmCharacters.forEach(c => {
			if (f_characterDataFetch(g, c, 'name', null) == char) userID = g.loremaster;
		});
	}
	//console.log('journeys_userID_from_char_name(g,' + char + ') = ' + userID); 
	return userID;
}

function journeys_people_list(g, people) {
	if (!g.config.includes('journey-mention')) return people.join(', ');
	let r = [];
	people.forEach(ch => {
		let userID = journeys_userID_from_char_name(g, ch);
		let m = f_userMention(userID);
		let s = ch;
		if (m != '') s += ' [' + m + ']';
		r.push(s);
	});
	if (r.length == 0) return 'none';
	return r.join(', ');
}

// EXPORTS --------------------------------------------------------------------

module.exports = Object.assign({ journeys_load, journeys_tag_add, journeys_tag_remove, journeys_tag_verify, journeys_tag_list, journeys_claim, journeys_create, journeys_create_quick, journeys_accessible, journeys_edit, journeys_count_steps, journeys_add_step, journeys_remove_step, journeys_show_steps, journeys_clear_steps, journeys_delete, journeys_delete_immediately, journeys_lookup, journeys_copy, journeys_combine, journeys_list, journeys_display_list, journeys_show, journeys_image, journeys_fullname, journeys_status, journeys_next, journeys_modifier_dice, journeys_apply_roll, journeys_end, journeys_has_tag });

