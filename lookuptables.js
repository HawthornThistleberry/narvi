// LOOKUP TABLES ---------------------------------------------------------------

const filehandling = require('./filehandling.js');
const stringutil = require('./stringutil.js');
var table = require('./table.js');
var emoji = require('./emoji.js');
var regdice = require('./regdice.js');
var slashcommands = require('./slashcommands.js');

var lookupTables;
const lookupTableFilename = 'lookuptables.json';
const MAX_ROLLS = 2;

/*
lookupTables: {
	tags: ['core','strider',...]
	groups: {
		'group': ['tablename', 'tablename', ...],
	},
	tables:
		'shortname': {
			tags: [...],
			name: '...',
			notes: '...',
			creator: userID,
			createdate: today,
			published: false,
			rolltype: 'feat',
			rows: [
				<range> <code> <text>
			]
		}, ...
	}
*/

// load the tables file on startup
function tables_load() {
	lookupTables = filehandling.readJSONFile(lookupTableFilename);
	if (lookupTables == null || Object.keys(lookupTables).length === 0)	{
		lookupTables = {
			tags: ['tor', 'aime', 'core', 'strider', 'starter', 'homebrew', 'ruins', 'lifepaths', 'encounter', 'chargen', 'english', 'espanol'],
			groups: {},
			tables: {}
		};
		tables_save();
	} else if (lookupTables.groups == undefined) {
		lookupTables.groups = {};
	}
}

// save the tables file after each change
function tables_save() {
	return filehandling.writeJSONFile(lookupTableFilename, lookupTables);
}

// HOST FUNCTIONS -------------------------------------------------------------

// add a tag
function tables_tag_add(tagName) {
	if (lookupTables.tags.includes(stringutil.lowerCase(tagName))) return false;
	lookupTables.tags.push(stringutil.lowerCase(tagName));
	tables_save();
	return true;
}

// remove a tag
function tables_tag_remove(tagName) {
	if (!lookupTables.tags.includes(stringutil.lowerCase(tagName))) return false;
	lookupTables.tags = lookupTables.tags.filter(value => value != stringutil.lowerCase(tagName));
	tables_save();
	return true;
}

// verify if a tag is available
function tables_tag_verify(tagName) {
	return lookupTables.tags.includes(stringutil.lowerCase(tagName));
}

// return a tag list (e.g., for displaying)
function tables_tag_list() {
	return lookupTables.tags;
}

// take over ownership of a table
function tables_claim(tableName, userID) {
	lookupTables.tables[tableName].creator = userID;
}

// TABLE CREATION AND EDITING -------------------------------------------------

// /table create <shortname>
function tables_create(tableName, userID) {
	tableName = stringutil.lowerCase(tableName);
	// if a table already exists with that name, return false
	if (lookupTables.tables[tableName] != null || lookupTables.groups[tableName] != null) return false;

	// create a table with that name and fill in some basic info:
	lookupTables.tables[tableName] = {
		tags: [],
		name: tableName,
		notes: '',
		creator: userID,
		createdate: Date.now(),
		published: false,
		rolltype: '',
		rows: []
	};
	tables_save();
	return true;
}

// /table creategroup <shortname> <tables>
function tablegroups_create(tableName, userID, hostUser, usersLM, tables) {
	tableName = stringutil.lowerCase(tableName);
	// if a table already exists with that name, return an error; not so for a group
	if (lookupTables.tables[tableName] != null) return 'table-exists';

	// if a group already exists with that name, return an error unless it's the same creator or the hostUser - those can overwrite
	if (lookupTables.groups[tableName] != null && lookupTables.groups[tableName].creator != userID && userID != hostUser) return 'table-exists';

	// change commas to spaces
	tables = tables.trim().replace(/,/g,' ');
	// collapse multiple spaces
	tables = tables.replace(/  +/g, ' ');
	let t = tables.split(' ');

	// verify all the tables are accessible and published
	let problem = null;
	t.forEach(table => {
		if (!tables_accessible(table, userID, hostUser, usersLM, 'read') || !lookupTables.tables[table].published) problem = table;
	});
	if (problem) return problem;

	// create a table with that name and fill in some basic info:
	lookupTables.groups[tableName] = {
		creator: userID,
		createdate: Date.now(),
		tables: [... t]
	};
	tables_save();
	return 'table-groupcreated';
}

// determine if a table can be read or written by a particular user
function tables_accessible(tableName, userID, hostUser, usersLM, type) {
	// if the table doesn't exist, you can't access it
	if (lookupTables.tables[tableName] == null) return false;
	// the host user can do everything to all tables
	if (userID == hostUser) return true;
	// if the table is owned by this user, they can do anything
	if (lookupTables.tables[tableName].creator == userID) return true;
	// if we're asking for write access, and no earlier test passed, they can't do it
	if (type == 'write') return false;
	// so we're asking for read now, and it's not the user's own table
	// if userID's current game's LM is the creator then return true;
	if (usersLM == lookupTables.tables[tableName].creator) return true;
	// otherwise it only depends on whether the table is published, 
	if (lookupTables.tables[tableName].published) return true;
	return false;
}

// basic table editing: name, notes, tag, rolltype, published
function tables_edit(tableName, userID, hostUser, locale, field, value) {
	if (!tables_accessible(tableName, userID, hostUser, null, 'write')) return 'table-notexists';
	// validation
	if (field == 'name' || field == 'notes') value = stringutil.mildSanitizeString(value);
	if (field == 'name' && value.length > 120) return 'table-nametoolong';
	if (field == 'tags') {
		let r = [];
		let problem = null;
		value.split(' ').forEach(tag => {
			if (!tables_tag_verify(tag)) problem = 'error-unknowntag';
			r.push(tag);
		});
		if (problem) return problem;
		value = r;
	}
	if (field == 'rolltype') {
		let r = tables_valid_rolltype(value);
		if (r != '') return r;
	}
	if (field == 'published' && value) {
		// must have a name that's not the same as the shortname
		if (lookupTables.tables[tableName].name == '' || lookupTables.tables[tableName].name == tableName) {
			return 'table-noname';
		}
		// must have a rolltype
		if (lookupTables.tables[tableName].rolltype == '') {
			return 'table-norolltype';
		}
		// must have at least one tag
		if (lookupTables.tables[tableName].tags.length == 0) {
			return 'table-notags';
		}
		// must pass validation
		let v = tables_validation(tableName, locale);
		if (v != '') return 'table-cannotpublish';
	}
	lookupTables.tables[tableName][field] = value;
	tables_save();
	return '';
}

// validate a rolltype
function tables_valid_rolltype(rolltype) {
	let rolls;
	if (!rolltype.includes('/')) rolls = [rolltype];
	else rolls = rolltype.split('/');
	if (rolls.length > MAX_ROLLS) return 'table-toomanyrolls';
	let valid = true;
	rolls.forEach(roll => {
		let thisvalid = false;
		// can be dN, NdN, d%, Nd%, feat, success
		if (roll == 'feat' || roll == 'success') thisvalid = true;
		let r = roll.split('d');
		if (r.length == 2 && (!isNaN(r[0]) || r[0]=='') && (!isNaN(r[1]) || r[1] == '%')) thisvalid = true;
		if (!thisvalid) valid = false;
	});
	if (!valid) return 'table-invalidrolltype';
	return '';
}

// /table add <range> <code> <text> - this would replace if the range is the same; needs validation for whether ranges overlap or conflict
function tables_add_row(tableName, userID, hostUser, locale, range, text) {
	if (!tables_accessible(tableName, userID, hostUser, null, 'write')) return ['table-notexists',''];

	// verify the range is in a valid range format
	let r = tables_valid_range(range);
	if (r != '') return [r,''];

	// verify the range has the right number of values to match the rolltype
	if (tables_count_indices(range) != tables_index_size(tableName)) return ['table-badrange',''];

	// sanitize the code and text
	text = stringutil.mildSanitizeString(text);
	let row = range + ' x ' + text;

	// look for an existing row with the same range and if found, just replace it
	let found = false;
	for (let i = 0; i < lookupTables.tables[tableName].rows.length; i++) {
		let r = lookupTables.tables[tableName].rows[i].split(' ');
		if (r[0] == range) {
			found = true;
			lookupTables.tables[tableName].rows[i] = row;
		}
	}
	// since we didn't find it, add it
	if (!found) {
		lookupTables.tables[tableName].rows.push(row);
		// sort them numerically left to right
		lookupTables.tables[tableName].rows.sort(table_row_order_compare);
	}
	tables_save();

	// now do a validation so we can warn the user if the table is inconsistent, incomplete, or ready
	return ['',tables_validation(tableName, locale)];
}

function tables_code_row(tableName, userID, hostUser, locale, range, code) {
	if (userID != hostUser) return ['table-notexists',''];

	// verify the range is in a valid range format
	let r = tables_valid_range(range);
	if (r != '') return [r,''];

	// verify the range has the right number of values to match the rolltype
	if (tables_count_indices(range) != tables_index_size(tableName)) return ['table-badrange',''];

	// sanitize the code
	code = stringutil.wordWithoutSpaces(stringutil.mildSanitizeString(code));

	// look for an existing row with the same range and if found, replace it
	let found = false;
	for (let i = 0; i < lookupTables.tables[tableName].rows.length; i++) {
		let r = lookupTables.tables[tableName].rows[i].split(' ');
		if (r[0] == range) {
			found = true;
			let text = lookupTables.tables[tableName].rows[i].split(' ').splice(2).join(' ');
			lookupTables.tables[tableName].rows[i] = range + ' ' + code + ' ' + text;
		}
	}

	// since we didn't find it, an error
	if (!found) return ['table-badrange',''];
	tables_save();

	// now do a validation so we can warn the user if the table is inconsistent, incomplete, or ready
	return ['',tables_validation(tableName, locale)];
}


// compare two ranges to figure out which one comes first
function table_row_order_compare(a, b) {
	// split first words by indices
	let aArray = a.split(' ')[0].split('/'), bArray = b.split(' ')[0].split('/');
	if (aArray.length != bArray.length) return 0; // should never happen if we've been checking things all along!

	for (let i=0; i<aArray.length; i++) {
		// figure out the numeric value of the first part of its range
		let aValue = tables_dieface_value(aArray[i].split('-')[0]);
		let bValue = tables_dieface_value(bArray[i].split('-')[0]);
		if (aValue < bValue) return -1;
		if (aValue > bValue) return 1;
		// if they're equal, keep looping and compare the next
	}
	// if we looped through all of them and everything matched, well, that should never happen, but since it did, leave them in the same order
	return 0;
}

// validate a range
function tables_valid_range(range) {
	if (range == null || typeof range != 'string') return 'table-badrange';
	let ranges;
	if (!range.includes('/')) ranges = [range];
	else ranges = range.split('/');
	if (ranges.length > MAX_ROLLS) return 'tablestoomanyrolls';
	let valid = true;
	ranges.forEach(thisRange => {
		let thisValues;
		if (!thisRange.includes('-')) thisValues = [thisRange];
		else thisValues = thisRange.split('-');
		if (thisValues.length > 2) valid = false;
		if (tables_dieface_value(thisValues[0]) == null) valid = false;
		if (thisValues.length == 2) {
			if (tables_dieface_value(thisValues[1]) == null) valid = false;
			if (tables_dieface_value(thisValues[0]) > tables_dieface_value(thisValues[1])) valid = false;
		}
	});
	if (!valid) return 'table-badrange';
	return '';
}

// die face values in ranges can be numbers, E, or G
function tables_dieface_value(s) {
	s = stringutil.lowerCase(s);
	if (s == 'e') return 0;
	if (s == 'g') return 11;
	if (!isNaN(s)) return Number(s);
	return null;
}

// count the number of indices in either a range or rolltype
function tables_count_indices(s) {
	if (s == null || s == '') return 0;
	if (!s.includes('/')) return 1;
	return s.split('/').length;
}

// determine from the rolltype how many indices a table has
function tables_index_size(tableName) {
	if (lookupTables.tables[tableName] == null) return 0;
	return tables_count_indices(lookupTables.tables[tableName].rolltype);
}

// validate that the rolltype and table ranges all match and cover all possible rolls
function tables_validation(tableName, locale) {

	// first check if every range has the right number of indices
	let indices = tables_index_size(tableName);
	if (!lookupTables.tables[tableName] || !lookupTables.tables[tableName].rows) return slashcommands.localizedString(locale, 'table-misformed',[]);
	let problem = '';
	lookupTables.tables[tableName].rows.forEach(row => {
		if (tables_count_indices(row.split(' ')[0]) != indices) {
			problem = slashcommands.localizedString(locale, 'table-val-wrongindices',[row.split(' ')[0], indices, lookupTables.tables[tableName].rolltype]);
		}
	});
	if (problem != '') return problem;

	// check for overlaps
	// for each row
	for (let i = 0; i < lookupTables.tables[tableName].rows.length; i++) {
		let rangei = lookupTables.tables[tableName].rows[i].split(' ')[0];
		// for each row after it
		for (let j = i+1; j < lookupTables.tables[tableName].rows.length; j++) {
			let rangej = lookupTables.tables[tableName].rows[j].split(' ')[0];
			// if they have any overlap
			if (tables_range_overlap(rangei, rangej))
				return slashcommands.localizedString(locale,'table-val-overlap',[rangei, rangej]);
		}
	}

	// build an array with all possible combinations of all possible rolls based on rolltype
	let possibles = [];
	let rolls = lookupTables.tables[tableName].rolltype.split('/');
	let low, high;
	let low2, high2;
	[low, high] = tables_dice_range_values(rolls[0]);
	if (indices == 1) {
		for (let i=low; i <= high; i++) possibles.push(String(i));
	} else {
		[low2, high2] = tables_dice_range_values(rolls[1]);
		for (let i=low; i <= high; i++) 
			for (let j=low2; j <= high2; j++)
				possibles.push(String(i) + '/' + String(j));
	}

	// now go through each row and remove entries from the possibles list if the row covers them
	// also watch for out of bounds possibilities
	lookupTables.tables[tableName].rows.forEach(row => {
		// find the corresponding range
		let range = row.split(' ')[0];
		let l1, h1, l2, h2;
		[l1, h1, l2, h2] = tables_parse_range(range);
		if (l1 < low || h1 > high) problem = slashcommands.localizedString(locale, 'table-val-outofbounds',[row.split(' ')[0], roll]);
		if (indices == 2 && (l2 < low2 || h2 > high2)) problem = slashcommands.localizedString(locale,'table-val-outofbounds',[row.split(' ')[0], roll]);
		if (indices == 1) {
			for (let i=l1; i <= h1; i++) possibles = possibles.filter(p => p != String(i));
		} else {
			for (let i=l1; i <= h1; i++) 
				for (let j=l2; j <= h2; j++)
					possibles = possibles.filter(p => p != String(i) + '/' + String(j));
		}
	});

	if (possibles.length != 0) {
		if (possibles.length > 10) {
			possibles.length = 10;
			possibles.push('...');
		}
		return slashcommands.localizedString(locale,'table-val-gaps',[possibles.join(', ')]);
	}

	// no problems found
	return '';
}

// find the high and low limits of any given roll
function tables_dice_range_values(roll) {
	let low, high;
	if (roll == 'feat') {
		low = 0;
		high = 11;
	} else if (roll == 'success') {
		low = 1;
		high = 6;
	} else {
		if (roll == undefined) roll = '';
		let r = roll.split('d');
		if (r[0] == '') r[0] = '1';
		if (r[1] == '%') r[1] = '100';
		low = Number(r[0]);
		high = Number(r[0]) * Number(r[1]);
	}
	return [low, high];
}

// compare two ranges to see if they overlap
function tables_range_overlap(rangea, rangeb) {
	let l1x, l1y, r1x, r1y, l2x, l2y, r2x, r2y;
	let ra1x, ra1y, ra2x, ra2y, rb1x, rb1y, rb2x, rb2y

	[ra1x, ra2x, ra1y, ra2y] = tables_parse_range(rangea);
	[rb1x, rb2x, rb1y, rb2y] = tables_parse_range(rangeb);

	if (is_point_in_rectangle(ra1x, ra1y, rb1x, rb1y, rb1y, rb2y)) return true;
	if (is_point_in_rectangle(ra2x, ra1y, rb1x, rb1y, rb1y, rb2y)) return true;
	if (is_point_in_rectangle(ra1x, ra2y, rb1x, rb1y, rb1y, rb2y)) return true;
	if (is_point_in_rectangle(ra2x, ra2y, rb1x, rb1y, rb1y, rb2y)) return true;
	return false;
}

// given a range with up to two indices, returns an array of four values, the lower and upper limit of the first range and the lower and upper limit of the second; if there's only one index, it'll just put the same values into both
function tables_parse_range(range) {
	let l1, h1, l2, h2;
	range = range.split('/');
	[l1, h1] = tables_parse_single_range(range[0]);
	if (range[1] == null) {
		l2 = l1;
		h2 = h1;
	} else [l2, h2] = tables_parse_single_range(range[1]);
	return [l1, h1, l2, h2];
}

// parses a single range (e.g., "5" or "5-7") and returns a two element array with its low and high
function tables_parse_single_range(range) {
	range = range.split('-');
	let low = tables_dieface_value(range[0]);
	let high = low;
	if (range[1] != null) high = tables_dieface_value(range[1]);
	return [Number(low), Number(high)];
}

// determine if a point in two dimensional space is within a rectangle
function is_point_in_rectangle(x, y, x1, y1, x2, y2) {
	if (x >= x1 && x <= x2 && y >= y1 && y <= y2) return true;
	return false;
}

// /table remove <range>
function tables_remove_row(tableName, userID, hostUser, locale, range) {
	if (!tables_accessible(tableName, userID, hostUser, null, 'write')) return ['table-notexists',''];

	// verify the range is in a valid range format
	let r = tables_valid_range(range);
	if (r != '') return [r,''];

	// find such a row
	let found = false;
	for (let i = 0; i < lookupTables.tables[tableName].rows.length; i++) {
		let r = lookupTables.tables[tableName].rows[i].split(' ');
		if (stringutil.caseInsensitiveMatch(r[0],range)) {
			found = true;
			lookupTables.tables[tableName].rows.splice(i,1);
		}
	}
	if (!found) return ['table-rownotexists',''];

	tables_save();

	// now do a validation so we can warn the user if the table is inconsistent, incomplete, or ready
	return ['',tables_validation(tableName, locale)];
}

// /table clear - remove all entries
function tables_clear_rows(tableName, userID, hostUser) {
	if (!tables_accessible(tableName, userID, hostUser, null, 'write')) return ['table-notexists',''];
	lookupTables.tables[tableName].rows = [];
	tables_save();
	return '';
}

// /table delete confirm
function tables_delete(tableName, userID, hostUser) {
	if (!tables_accessible(tableName, userID, hostUser, null, 'write')) return 'table-notexists';
	delete lookupTables.tables[tableName];
	tables_save();
	return '';
}

// /table deletegroup <group> confirm
function tablegroups_delete(tableName, userID, hostUser) {
	if (lookupTables.groups[tableName] == null || (lookupTables.groups[tableName].creator != userID && userID != hostUser)) return 'table-notexists';
	delete lookupTables.groups[tableName];
	tables_save();
	return '';
}

// TABLE LISTS AND SHOW -------------------------------------------------------

// /table list [mine] [tag=<tag>] [search-text] - lists all tables that match (only shows mine or published, except that I can see them all)
function tables_list(userID, hostUser, usersLM, locale, mine, published, tags, searchTerms) {
	let reportHeadings = [
		{title:stringutil.titleCase(slashcommands.localizedCommand(locale, 'table')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'table', 'rolltype')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'table','name')), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale,'rows',[])), emoji:false, minWidth:0, align:'left'},
		{title:'Pub', emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	let matches = 0;
	// go through the args to set search terms
	// go through the tables checking both accessibility and the search terms
	for (let tableName in lookupTables.tables) {
		let listIt = true;
		if (!tables_accessible(tableName, userID, hostUser, usersLM, 'read')) listIt = false;
		if (mine === true && lookupTables.tables[tableName].creator != userID) listIt = false;
		if (mine === false && lookupTables.tables[tableName].creator == userID) listIt = false;
		if (published === true && !lookupTables.tables[tableName].published) listIt = false;
		if (published === false && lookupTables.tables[tableName].published) listIt = false;
		if (tags) tags.split(' ').forEach(tag => {
			if (!lookupTables.tables[tableName].tags.includes(tag)) listIt = false;
		});
		if (searchTerms) searchTerms.split(' ').forEach(term => {
			if (!tables_search_term(tableName,term)) listIt = false;
		});
		if (listIt) {
			reportData.push([
				tableName, 
				lookupTables.tables[tableName].rolltype, 
				stringutil.trimPadRight(lookupTables.tables[tableName].name, 40),
				lookupTables.tables[tableName].rows.length, 
				lookupTables.tables[tableName].published ? ':ledger:' : ''
			]);
			matches++;
		}
	}
	for (let tableName in lookupTables.groups) {
		let listIt = true;
		if (mine === true && lookupTables.groups[tableName].creator != userID) listIt = false;
		if (mine === false && lookupTables.groups[tableName].creator == userID) listIt = false;
		if (searchTerms) searchTerms.split(' ').forEach(term => {
			if (!stringutil.lowerCase(tableName).includes(stringutil.lowerCase(term))) listIt = false;
		});
		if (listIt) {
			reportData.push([
				tableName, 
				'group',
				lookupTables.groups[tableName].tables.join(' '),
				lookupTables.groups[tableName].tables.length, 
				':ledger:'
			]);
			matches++;
		}
	}
	if (reportData.length == 0) return slashcommands.localizedString(locale, 'table-listnomatch',[]);
	return table.buildTable(reportHeadings, reportData, 2) + '\n' + slashcommands.localizedString(locale,'table-matched',[matches, Object.keys(lookupTables.tables).length]);
}

// check all the relevant fields of a table for a search term
function tables_search_term(tableName, searchTerm) {
	searchTerm = stringutil.lowerCase(searchTerm);
	if (stringutil.lowerCase(tableName).includes(searchTerm)) return true;
	if (stringutil.lowerCase(lookupTables.tables[tableName].name).includes(searchTerm)) return true;
	if (stringutil.lowerCase(lookupTables.tables[tableName].notes).includes(searchTerm)) return true;
	if (stringutil.lowerCase(lookupTables.tables[tableName].rolltype).includes(searchTerm)) return true;
	let result = false;
	lookupTables.tables[tableName].rows.forEach(row => {
		if (stringutil.lowerCase(row).includes(searchTerm)) result = true;
	});
	return result;
}

// calls table.js functions to build a display version of the table (range, code, text) -- we may have to do some thinking about how the range can be both displayable and algorithmically determinable, while supporting "01-11" types, "feat/success" types, etc.
function tables_show(tableName, locale, userID, hostUser, userNameFromID) {

	if (tableName == '') return slashcommands.localizedString(locale,'table-notselected',[]);

	// if a group, show that
	if (lookupTables.groups[tableName] != null) {
		let r = '**GROUP `' + tableName + '`** :ledger:\n';
		r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedString(locale,'created',[])) + '__: <t:' + String(Math.floor(lookupTables.groups[tableName].createdate/1000)) + ':F> by ' + userNameFromID(lookupTables.groups[tableName].creator) + '\n';
		r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedSuboption(locale,'table','creategroup','tables')) + '__: `' + lookupTables.groups[tableName].tables.join(' ') + '`\n';
		return r;
	}

	let t = lookupTables.tables[tableName];

	// build the introduction
	let r = '**' + t.name + '** (`' + tableName + '`)' + (t.published ? ' :ledger:' : '') + '\n';
	if (t.tags.length > 0) r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale,'table','tags')) + '__: `' + t.tags.join('`, `') + '`\n';
	r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedString(locale,'created',[])) + '__: <t:' + String(Math.floor(t.createdate/1000)) + ':F> by ' + userNameFromID(t.creator) + '\n';
	r += ':black_small_square: __' + stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale,'table','rolltype')) + '__: ' + t.rolltype + '\n';

	// figure out how wide the widest entry will be
	let max = 0;
	if (!t.rows) t.rows = [];

	t.rows.forEach(p => {
		let pa = p.split(' ');
		let l = pa.shift().length;
		pa.shift();
		l += pa.join(' ').length;
		if (l > max) max = l;
	});

	// build the table, in multiples or not
	if (t.rows && t.rows.length > 10 && max < 20 && userID != hostUser) {
		r += tables_show_multiple_wide(tableName, locale, Math.floor(60 / max));
	} else {
		let reportHeadings = [
			{title:stringutil.titleCaseWords(slashcommands.localizedString(locale,'range',[])), emoji:true, minWidth:0, align:'center'},
			{title:stringutil.titleCaseWords(slashcommands.localizedString(locale,'result',[])), emoji:false, minWidth:0, align:'left'}
			];
		if (userID == hostUser) {
			reportHeadings.splice(1,0,{title:'Code', emoji:false, minWidth:0, align:'left'});
		}
		let reportData = [];
		t.rows.forEach(p => {
			reportData.push(tables_show_build_single_entry(p, userID == hostUser, userID == hostUser));
		});
		r += table.buildTable(reportHeadings, reportData, 2);
	}

	return r +  '\n' + t.notes + '\n' + (tables_accessible(tableName, userID, hostUser, null, 'write') ? tables_validation(tableName, locale) : '');
}

// variant for long but narrow tables that shows them with several across
function tables_show_multiple_wide(tableName, locale, columns) {
	let t = lookupTables.tables[tableName];

	// calc number of rows by dividing the total length over the columns
	let numRows = Math.ceil(t.rows.length / columns);

	// build the headings and calculate the indices
	let reportHeadings = [];
	let indices = [];
	for (let i = 0; i < columns; i++) {
		reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedString(locale,'range',[])), emoji:true, minWidth:0, align:'center'});
		reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedString(locale,'result',[])), emoji:false, minWidth:0, align:'left'});
		indices[i] = Number(i * numRows);
	}

	// build the rows
	let reportData = [];
	let row;
	for (let i = 0; i < numRows ; i++) {
		row = [];
		for (let j = 0; j < columns; j++) {
			let rownum = Number(Number(indices[j])+Number(i));
			if (rownum < t.rows.length) {
				tables_show_build_single_entry(t.rows[rownum], false, false).forEach(x => row.push(x));
			}
		}
		reportData.push(row);
	}

	return table.buildTable(reportHeadings, reportData, 2);
}

// DRY the above
function tables_show_build_single_entry(p, complete, showcodes) {
	let parts = p.split(' ');
	let range = parts.shift();
	// replace G and E in the ranges with the appropriate emoji
	let newRange = [];
	range.split('/').forEach(r1 => {
		let newr1 = []
		r1.split('-').forEach(r => {
			if (stringutil.lowerCase(r) == 'g') r = emoji.customEmoji(emoji.GANDALF,false,false,false);
			if (stringutil.lowerCase(r) == 'e') r = emoji.customEmoji(emoji.EYE,false,false,false);
			newr1.push(r);
		});
		newRange.push(newr1.join('-'));
	});
	range = newRange.join('/');
	// */
	let code = parts.shift();
	let text = parts.join(' ');
	if (!complete && text.length > 60) text = text.substr(0,57) + '...';
	if (complete && showcodes) {
		return [range, code, text];
	} else {
		return [range, text];
	}
}


// TABLE ROLL AND LOOKUP ------------------------------------------------------

// /table result <value>: given a roll, returns the row's range, code, and text
function tables_rolltype(tableName, locale) {
	let v = tables_validation(tableName, locale);
	if (v != '') return null;
	return lookupTables.tables[tableName].rolltype;
}


// /table result <value>: given a roll, returns the row's range, code, and text
function tables_result(tableName, roll) {

	let v = tables_validation(tableName, locale);
	if (v != '') return [null,v,null,null];

	// verify the roll has the right number of indices
	if (tables_count_indices(roll) != tables_index_size(tableName)) return ['table-rollmismatch',null,null,null];

	// verify the roll is made of single values, no dashes
	let r1l, r1h, r2l, r2h;
	[r1l, r1h, r2l, r2h] = tables_parse_range(roll);
	if (r1l != r1h || r2l != r2h) return ['table-rollmismatch',null,null,null];

	// verify the roll is within ranges of the rolltype
	let rt1l, rt1h, rt2l, rt2h;
	let rolltypes = lookupTables.tables[tableName].rolltype.split('/');
	[rt1l, rt1h] = tables_dice_range_values(rolltypes[0]);
	rt2l = rt1l;
	rt2h = rt1h;
	if (rolltypes[1]) [rt2l, rt2h] = tables_dice_range_values(rolltypes[1]);
	if (r1l < rt1l || r1l > rt1h || r2l < rt2l || r2l > rt2h) return ['table-rollmismatch',null,null,null];

	// loop over the table to find a row that fits
	let range = null, code = null, text = null;
	lookupTables.tables[tableName].rows.forEach(row => {
		let ra1l, ra1h, ra2l, ra2h;
		[ra1l, ra1h, ra2l, ra2h] = tables_parse_range(row.split(' ')[0]);
		if (r1l >= ra1l && r1l <= ra1h && r2l >= ra2l && r2l <= ra2h) {
			let x = row.split(' ');
			range = x.shift();
			code = x.shift();
			text = x.join(' ');
		}
	});

	// if not found, return a validation error
	if (range == null) return ['table-misformed',null,null,null];
	return [null, range, code, text];
}

// /table roll but for a table group
function tablegroups_roll(tableName, locale, modifier, feat, allowModsOnEG, torRollDice) {
	if (lookupTables.groups[tableName] == undefined) return null;
	let r = [];
	// for each table in the group, do tables_roll() and append
	lookupTables.groups[tableName].tables.forEach(t => {
		let [error, range, code, text, finalroll, rollstrings] = tables_roll(t, locale, modifier, feat, allowModsOnEG, torRollDice);
		r.push([t, error, range, code, text, finalroll, rollstrings]);
	});
	return r;
}

// /table roll [modifier]: does the roll with the modifier, then calls tables_result and returns its return
function tables_roll(tableName, locale, modifier, feat, allowModsOnEG, torRollDice) {

	let v = tables_validation(tableName, locale);
	if (v != '') return [v, '', '', '', '', '', ''];

	// figure out any modifiers
	let rerolls = 1;
	if (feat == 'favoured') rerolls = 2;
	else if (feat == 'illfavoured') rerolls = -2;
	let mods = [];
	if (typeof modifier == 'string') mods = modifier.split('/');
	mods.push('0');
	mods.push('0');

	// get the table's rolltype
	let rolltype = lookupTables.tables[tableName].rolltype.split('/');

	// do the appropriate roll(s)
	let roll = [];
	let rollstrings = [];
	let junk1, junk2, junk3;
	rolltype.forEach(r => {
		let thisRoll, thisRollString;
		let low, high;
		[low, high] = tables_dice_range_values(r);
		// do the actual roll
		if (r == 'feat') {
			let theRoll = torRollDice(rerolls, 0, 0, 0, false, false, false, 0, false, locale, 'full', false);
			thisRoll = theRoll[0];
			if (theRoll[1] == 1) thisRoll = 11;
			thisRollString = theRoll[5];
		} else if (r == 'success') {
			thisRoll = regdice.simpleRoll(6);
			thisRollString = emoji.customEmoji(thisRoll, 1, 0, 0);
		} else {
			[thisRoll, thisRollString, junk1, junk2, junk3] = regdice.polyhedralDieRoll([r], '', '', locale);
		}
		// add or subtract mods.shift() but don't go above or below limits
		thisRoll = Number(thisRoll);
		let originalRoll = thisRoll;
		let m = Number(mods.shift());
		// apply mods, unless this is a feat roll without allowModsOnEG and it's an Eye or Gandalf -- prevents going away from those rolls
		if (r != 'feat' || allowModsOnEG || (originalRoll !=0 && originalRoll != 11)) {
			if (!isNaN(m)) thisRoll += Number(m);
			else if (m.startsWith('+') && !isNaN(m.substr(1))) thisRoll += Number(m.substr(1));
			if (thisRoll < low) thisRoll = low;
			if (thisRoll > high) thisRoll = high;
			// if the modification is going to make an Eye but allowModsOnEG is off, stop at 1 -- prevents going to those rolls
			if (!allowModsOnEG && originalRoll != 0 && thisRoll == 0) thisRoll = 1;
			// same thing for stopping at 10 and not Gandalf
			if (!allowModsOnEG && originalRoll != 11 && thisRoll == 11) thisRoll = 10;
		}

		// save the results
		roll.push(thisRoll);
		rollstrings.push(thisRollString);
	});

	// let tables_results do the rest of the work
	let r = tables_result(tableName, roll.join('/'));
	// add the final modified roll and rollstrings
	r.push(roll.join('/'));
	r.push(rollstrings);
	return r;
}

// EXPORTS --------------------------------------------------------------------

module.exports = Object.assign({ tables_load, tables_tag_add, tables_tag_remove, tables_tag_verify, tables_tag_list, tables_claim, tables_create, tables_accessible, tables_edit, tables_add_row, tables_code_row, tables_remove_row, tables_clear_rows, tables_delete, tables_list, tables_show, tables_result, tables_rolltype, tables_roll, tables_validation, tablegroups_create, tablegroups_delete, tablegroups_roll });

