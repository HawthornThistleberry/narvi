var stringutil = require('./stringutil.js');
var embeds = require('./embeds.js');
var slashcommands = require('./slashcommands.js');

// A simple linear die roll
function simpleRoll(n) {
	return Math.floor(Math.random() * n + 1);
}

// convert number of dice sides accounting for %
function diceSides(s) {
	if (s == '%') return 100;
	if (!isNaN(s)) return Number(s);
	return 6;
}

// handles normal polyhedra dice in formats: [n]d[%|size] with bonuses/penalties
function polyhedralDieRoll(args, comment, charName, locale) {
	let rollLine = '', total = 0, bonus = 0, sign, mult, numdice = 1, numsides = 12;
	if (charName == '') charName = 'You';
	let r = '**'+ charName + '** ';
	r += slashcommands.localizedString(locale, 'roll-rolled', []);
	r += ' ';

	// convert back to a string without spaces
	let s = args.join('').toLowerCase();
	r += s + ': ';
	// now put a space before every + or -
	s = '+' + s.replace(/\+/g,' +').replace(/\-/g,' -');

	// for each term...
	s.split(' ').forEach(term => {
		// pluck off the + or - and set sign to 1 or -1 accordingly
		sign = term.charAt(0);
		if (sign == '-') mult = -1; else mult = 1;
		term = term.substring(1);

		if (term.includes('d')) { // if it contains a d, this is a die roll
			// split it (accounting for d8, 2d8, and d%)
			let die = term.split('d');
			if (die.length == 1) { // d8
				numdice = 1;
				numsides = diceSides(die[0]);
			} else { // 3d8
				numdice = Math.max(1,Number(die[0]));
				numsides = diceSides(die[1]);
			}
			if (numdice > 50) numdice = 50;
			r += '[';
			// roll it
			for (let i = 0; i < numdice; i++) {
				// get a die roll
				let thisRoll = simpleRoll(numsides);
				rollLine += polyhedralDieFace(numsides, thisRoll);
				r += thisRoll + ', ';
				total += mult * thisRoll;
			}
			r = r.slice(0, -2) + '] ';
		} else { // add the numeric value to bonus
			bonus += mult * Number(term);
		}
	});

	r += '\n**__';
	r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', []));
	r += '__:** ' + (bonus != 0 ? (total + ' + ' + bonus + ' = ') : '') + '**' + String(total+bonus) + '**';
	if (comment != '') {
		r += '\n**__';
		r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []));
		r += '__**: ' + comment;
	}
	return [total+bonus, rollLine, r, null, null];
}

// 3d6 with drama die and stunts
function expanseDieRoll(args, comment, charName, charImage, target, locale) {

	let mods = 0, tn = 0;
	if (target != null) tn = target;
	let r = '**'+ charName + '** ';
	r += slashcommands.localizedString(locale, 'roll-rolled', []);
	r += ' ';
	r += stringutil.titleCase(slashcommands.localizedCommand(locale, 'expanse', []));
	r += ' ';
	let rollLine = '';

	// parse the string into an array made of number, non-numeric, number, non-numeric, etc.
	// this pattern is based on one I found at https://codereview.stackexchange.com/questions/2345/simplify-splitting-a-string-into-alpha-and-numeric-parts
	let parsed = args.join().split(/[^+\-0-9]+|(?<=[+\-])(?=[0-9])|(?<=[0-9])(?=[+\-])/);

	// if the first is numeric it's a plus
	if (!isNaN(parsed[0])) {
		mods = Number(parsed[0]);
		parsed = parsed.splice(1);
	}

	// if there are args beyond that, check them for plus/minus for mods
	for (let i = 0; i < parsed.length-1; i += 2) {
		if (parsed[i] == '-' && !isNaN(parsed[i+1])) mods -= Number(parsed[i+1]);
		if (parsed[i] == '+' && !isNaN(parsed[i+1])) mods += Number(parsed[i+1]);
	}
	
	// do the roll and its results (just text, no emoji)
	if (mods < 0) r += ' -' + (-mods);
	if (mods > 0) r += ' +' + mods;
	r += ': **';
	let rolls = [0, 0, 0];
	let total = 0;
	for (let i = 0; i < 3; i++) {
		rolls[i] = simpleRoll(6);
		if (i == 2) rollLine += polyhedralDieFace('b6', rolls[i]);
		else rollLine += polyhedralDieFace(6, rolls[i]);
		total += rolls[i];
	}
	total += mods;

	// output the roll
	r += rolls[0] + '**, **' + rolls[1] + '**, ';
	r += slashcommands.localizedString(locale, 'expanse-drama', []);
	r += ' **' + rolls[2] + '**\n**__';
	r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', []));
	r += '__: ' + total + '**';
	if (tn > 0) {
		r += '\n**';
		if (total >= tn) {
			r += stringutil.upperCase(slashcommands.localizedString(locale, 'success', []));
		} else {
			r += stringutil.titleCase(slashcommands.localizedString(locale, 'failure', []));
		}
		r += '** (TN=' + String(tn) + ')';
	}

	// identify stunts
	let stunt = 0;
	if (rolls[0] == rolls[1] || rolls[0] == rolls[2] || rolls[1] == rolls[2]) stunt = rolls[2];
	if (stunt != 0 && (tn == 0 || total >= tn)) {
		r += '\n**__';
		r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'expanse-stunt', []));
		r += '__**: ' + stunt;
	}
	if (rolls[2] == 6) {
		r += '\n';
		r += slashcommands.localizedString(locale, 'expanse-churn', []);
	}
	if (comment != '') {
		r += '\n**__';
		r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []));
		r += '__**: ' + comment;
	}

	// build the return
	let theEmbed = embeds.buildEmbed(
		charName,
		charImage,
		'https://greenroninstore.com/collections/the-expanse-rpg',
		r,
		null,
		null,
		null,
		null,
		embeds.BUTTON_GREEN,
		null
	);
	return [total, rollLine, {embed: theEmbed}, null, null];

}

// Serenity (Cortex) system rolls

// temporary saving of group rolls in progress
var groupRolls = {};

// do a single roll and return it as a number and a string
function serenityOneRoll(rolls, locale) {
	let total = 0, r = '', rollString = '', botch = true, rollLine = '';
	rolls.forEach(die => {
		r += 'd' + Number(die) + '+';
		let roll = simpleRoll(Number(die));
		rollLine += polyhedralDieFace(Number(die),roll);
		if (roll != 1) botch = false;
		rollString += '[' + roll + '] ';
		total += roll;
	});
	r = r.slice(0,-1);
	r += ': **' + rollString + '\n__'+ stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', [])) + ': ' + total + '__**';
	if (botch) r += ' :bangbang: **BOTCH**';
	return [total, r, rollLine, null, null];
}

// handle group rolls
function serenityDieRollGroup(args, comment, charName, charImage, locale, gameName, isLM, target, rolls) {
	let r = '';
	if (target) {
		// we are finishing a group roll
		if (!isLM) return [0, '', 'Only the gamemaster can specify a target and thus complete a group roll.', null, null];
		let group = groupRolls[gameName];
		if (group == undefined) return [0, '', 'There is no active group roll in the game `' + gameName + '` to complete.', null, null];

		// build an introduction line
		let members = [];
		group.forEach(member => {
			members.push(member.split(' ')[0]);
		});
		r = 'A group consisting of **';
		if (members.length <= 2) r += members.join(' and ');
		else {
			let last = members.pop();
			r += members.join(', ') + ', and ' + last;
			members.push(last);
		}
		r += '** rolled Serenity (target **' + target + '**) in `' + gameName + '`:\n';

		// do each of the rounds
		let grandTotal = 0; thisTotal = 0, round = 1, roundTotal = 0;
		let thisRoll, roundRolls;
		while (grandTotal < target) {
			roundRolls = '';
			group.forEach(member => {
				let m = member.split(' ');
				let rLine = '`  `:low_brightness: ' + m.shift() + ': ';
				// do their roll line and add to the total
				[thisTotal, thisRoll] = serenityOneRoll(m, locale);
				rLine += thisRoll + '\n';
				roundRolls += rLine;
				roundTotal += thisTotal;
				grandTotal += thisTotal;
			});
			// add the round to the output
			r += ':high_brightness: **Round ' + round + '** totals **' + roundTotal + '**, grand total so far **' + grandTotal + '**\n' + roundRolls;
			round++;
			roundTotal = 0;
		}

		// add a trailer line
		r += ':high_brightness: **Succeeded in __' + String(round-1) + ' rounds__**';
		if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', [])) + '__**: ' + comment;

		// clear the groupRolls entry
		delete groupRolls[gameName];

		// return the result
		return [0, '', r, null, null];
	}

	// we are adding to a group roll; initialize a group if necessary
	let group = groupRolls[gameName];
	let found = false;
	if (group == undefined) {
		group = [];
		r = 'Created a group roll in `' + gameName + '` and added **' + charName + '** to it.';
	} else {
		r = 'Added **' + charName + '** to a group roll in `' + gameName + '`.';
		// see if you're already in the group and if so only replace your entry
		for (let i = 0; i < group.length; i++) {
			member = group[i];
			if (member.split(' ')[0] == charName) {
				found = true;
				group[i] = charName + ' ' + rolls.join(' ');
				r = 'Replaced **' + charName + '**\'s roll in a group in `' + gameName + '`.';
			}
		}
	}
	// add to groupRolls an array element that's the name followed by the series of rolls
	if (!found) group.push(charName + ' ' + rolls.join(' '));
	// save the result
	groupRolls[gameName] = group;
	// return a message summarizing the group so far
	r += '\nThe group roll now has **' + String(group.length) + '** participant' + (group.length > 1 ? 's' : '') + ': ';
	group.forEach(member => {
		let m = member.split(' ');
		r += '**' + m[0] + '** (d';
		m.shift();
		r += m.join('+d') + '), ';
	});
	r = r.slice(0,-2) + '.';
	return [0, '', r, null, null];
}

// master function for Serenity die rolling
function cortexDieRoll(charName, charImage, locale, gameName, isLM, rolls, target, multiple, group, comment) {
	let total = 0;
	let rollLine = '';
	if (charName == '') charName = 'Unknown';
	//if (charName.includes(' ')) charName = charName.split(' ').join('_');
	let r = '**'+ charName + '** ' + slashcommands.localizedString(locale, 'roll-rolled', []) + ' ' + stringutil.titleCase(slashcommands.localizedCommand(locale, 'cortex', [])) + ' ';
	let rollString = '';

	if (group) { // handle group rolls
		return serenityDieRollGroup(args, comment, charName, charImage, locale, gameName, isLM, target, rolls);
	}
	if (multiple == 0 && target == 0) {
		// simple roll
		let thisroll;
		[total, thisroll, rollLine] = serenityOneRoll(rolls, locale);
		r += thisroll;
		if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', [])) + '__**: ' + comment;

		// build the return
		let theEmbed = embeds.buildEmbed(
			charName,
			charImage,
			'https://greenroninstore.com/collections/the-expanse-rpg',
			r,
			null,
			null,
			null,
			null,
			embeds.BUTTON_GREEN,
			null
		);
		return [total, rollLine, {embed: theEmbed}, null, null];
	}
	// multiple rolls until we either hit a total of target, or a number of rolls equal to multiple
	let finished = false, thistotal, thisroll, numrolls = 0;
	r += '(multiple rolls):\n';
	do {
		[thistotal, thisroll] = serenityOneRoll(rolls, locale);
		r += ' :low_brightness: ' + thisroll + '\n';
		numrolls++
		total += thistotal;
		if (target != 0 && total >= target) finished = true;
		if (multiple != 0 && numrolls >= multiple) finished = true;
	} while (!finished);
	r += '**Total**: ' + total + ' in ' + numrolls + ' rolls';
	if (target != 0) {
		if (total >= target) r += ': **SUCCESS**';
		else r += ': **FAILURE**';
	}
	if (comment != '') r += '\n**__' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', [])) + '__**: ' + comment;
	return [0, '', r, null, null];	
}

// dice for the Raven RPG
function ravenDieRoll(mist, corvus, comment, charName, locale) {
	// a result of -1 to 1 is complications; lower is tenebrous; higher is favorable

	let rollLine = '', total = 0;
	if (charName == '') charName = 'You';
	let r = '**'+ charName + '** ';
	r += slashcommands.localizedString(locale, 'roll-rolled', []);
	r += ' (' + mist + ',' + corvus + ') ';

	for (let i = 0; i < mist; i++) {
		let roll = simpleRoll(2) - 1;
		total += roll;
		r += ravenDieFace('mist',roll) + ' ';
		rollLine += ravenDieFace('mist',roll);
	}
	for (let i = 0; i < corvus; i++) {
		let roll = simpleRoll(2) - 2;
		total += roll;
		r += ravenDieFace('corvus',roll) + ' ';
		rollLine += ravenDieFace('corvus',roll);
	}

	r += '\n**__';
	r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'result', []));
	r += '__:** ' + (total > 0 ? '+' : '') + String(total) + ' **';
	if (total < -1) r += 'Tenebrous';
	else if (total > 1) r += 'Favorable';
	else r += 'Complications';
	r += '**';
	if (comment != '') {
		r += '\n**__';
		r += stringutil.titleCaseWords(slashcommands.localizedString(locale, 'comment', []));
		r += '__**: ' + comment;
	}
	return [total, rollLine, r, null, null];
}


// choose appropriate emoji for dice roll displays
function polyhedralDieFace(size, roll) {
	if (size > 20 && size <= 100) {
		if (roll == 100) return '<:d100_00:935657633921257582>' + polyhedralDieFace(10, 0);
		let tens = Math.floor(roll / 10);
		let ones = roll - 10 * tens;
		let r; // get the tens digit first; 00, 10, 20, .. 90
		if (tens == 0) r = '<:d100_00:935657633921257582>';
		if (tens == 1) r = polyhedralDieFace(10, 10);
		if (tens == 2) r = '<:d100_20:935657634554605659>';
		if (tens == 3) r = '<:d100_30:935657636764987392>';
		if (tens == 4) r = '<:d100_40:935657636773367808>';
		if (tens == 5) r = '<:d100_50:935657636962123837>';
		if (tens == 6) r = '<:d100_60:935657636991492146>';
		if (tens == 7) r = '<:d100_70:935657637046018158>';
		if (tens == 8) r = '<:d100_80:935657637150879765>';
		if (tens == 9) r = '<:d100_90:935657637285077012>';
		return r + polyhedralDieFace(10, ones);
	}
	if (size == 20) {
		if (roll == 1) return '<:d20_01:934233375294767144>';
		if (roll == 2) return '<:d20_02:934233375558991882>';
		if (roll == 3) return '<:d20_03:934233375538020473>';
		if (roll == 4) return '<:d20_04:934233375898751046>';
		if (roll == 5) return '<:d20_05:934233376209133650>';
		if (roll == 6) return '<:d20_06:934233376553050112>';
		if (roll == 7) return '<:d20_07:934233376720838727>';
		if (roll == 8) return '<:d20_08:934233376989261934>';
		if (roll == 9) return '<:d20_09:934233377555501086>';
		if (roll == 10) return '<:d20_10:934233380411830362>';
		if (roll == 11) return '<:d20_11:934233377966526494>';
		if (roll == 12) return '<:d20_12:934233380525047959>';
		if (roll == 13) return '<:d20_13:934233380944478208>';
		if (roll == 14) return '<:d20_14:934233378755063919>';
		if (roll == 15) return '<:d20_15:934233380315357235>';
		if (roll == 16) return '<:d20_16:934233379300323438>';
		if (roll == 17) return '<:d20_17:934233379547807804>';
		if (roll == 18) return '<:d20_18:934233380470530100>';
		if (roll == 19) return '<:d20_19:934233380168556554>';
		if (roll == 20) return '<:d20_20:934233380567011360>';
	}
	if (size == 12) {
		if (roll == 1) return '<:d12_01:935287816546557952>';
		if (roll == 2) return '<:d12_02:935287816752091167>';
		if (roll == 3) return '<:d12_03:935287819730034718>';
		if (roll == 4) return '<:d12_04:935287817406390362>';
		if (roll == 5) return '<:d12_05:935287817498665030>';
		if (roll == 6) return '<:d12_06:935287817863581716>';
		if (roll == 7) return '<:d12_07:935287818094247966>';
		if (roll == 8) return '<:d12_08:935287818320764979>';
		if (roll == 9) return '<:d12_09:935287818689867876>';
		if (roll == 10) return '<:d12_10:935287819830706267>';
		if (roll == 11) return '<:d12_11:935287819595829329>';
		if (roll == 12) return '<:d12_12:935287819834912838>';
	}
	if (size == 10) {
		if (roll == 0) return '<:d10_00:935657633526984744>';
		if (roll == 1) return '<:d10_01:935304393505525882>';
		if (roll == 2) return '<:d10_02:935304393375506482>';
		if (roll == 3) return '<:d10_03:935304396533792779>';
		if (roll == 4) return '<:d10_04:935304394205958204>';
		if (roll == 5) return '<:d10_05:935304396999360532>';
		if (roll == 6) return '<:d10_06:935304395019669525>';
		if (roll == 7) return '<:d10_07:935304395195842641>';
		if (roll == 8) return '<:d10_08:935304397146177536>';
		if (roll == 9) return '<:d10_09:935304397045526548>';
		if (roll == 10) return '<:d10_10:935304397724999701>';
	}
	if (size == 8) {
		if (roll == 1) return '<:d8_1:935667400475574302>';
		if (roll == 2) return '<:d8_2:935667400601395260>';
		if (roll == 3) return '<:d8_3:935667400949522462>';
		if (roll == 4) return '<:d8_4:935667400903372841>';
		if (roll == 5) return '<:d8_5:935667401113088021>';
		if (roll == 6) return '<:d8_6:935667401612218439>';
		if (roll == 7) return '<:d8_7:935667401750626415>';
		if (roll == 8) return '<:d8_8:935667401700294727>';
	}
	if (size == 6) {
		if (roll == 1) return '<:d6_1:1032067206105792562>';
		if (roll == 2) return '<:d6_2:1032067207309557811>';
		if (roll == 3) return '<:d6_3:1032067208496562306>';
		if (roll == 4) return '<:d6_4:1032067212208525375>';
		if (roll == 5) return '<:d6_5:1032067214674759700>';
		if (roll == 6) return '<:d6_6:1032067215744303114>';
	}
	if (size == 'b6') {
		if (roll == 1) return '<:drama_1:1032067496846569582>';
		if (roll == 2) return '<:drama_2:1032067497958047876>';
		if (roll == 3) return '<:drama_3:1032067499098906684>';
		if (roll == 4) return '<:drama_4:1032067500185243648>';
		if (roll == 5) return '<:drama_5:1032067501560963082>';
		if (roll == 6) return '<:drama_6:1032067503045742593>';
	}
	if (size == 4) {
		if (roll == 1) return '<:d4_1:1032066504369381408>';
		if (roll == 2) return '<:d4_2:1032066506198102056>';
		if (roll == 3) return '<:d4_3:1032066507515105371>';
		if (roll == 4) return '<:d4_4:1032066503631183942>';
	}
	// fallback in case someone goes rolling a d7 or something
	if (roll == 1) return ':one:';
	if (roll == 2) return ':two:';
	if (roll == 3) return ':three:';
	if (roll == 4) return ':four:';
	if (roll == 5) return ':five:';
	if (roll == 6) return ':six:';
	if (roll == 7) return ':seven:';
	if (roll == 8) return ':eight:';
	if (roll == 9) return ':nine:';
	if (roll == 10) return ':keycap_ten:';
	return '[' + roll + ']'; 
}

function ravenDieFace(type, roll) {
	if (roll == -1) return '<:raven_corvus:1292488286463590440>';
	if (roll == 0 && type == 'mist') return '<:raven_mist_blank:1292488293807558717>';
	if (roll == 0 && type == 'corvus') return '<:raven_corvus_blank:1292488288875184269>';
	if (roll == 1) return '<:raven_mist:1292488291987357779>';
}


module.exports = Object.assign({ simpleRoll, polyhedralDieRoll, expanseDieRoll, cortexDieRoll, ravenDieRoll });
