// REPORTS --------------------------------------------------------------------

var stringutil = require('./stringutil.js');
var slashcommands = require('./slashcommands.js');
var table = require('./table.js');
var emoji = require('./emoji.js');
var embeds = require('./embeds.js');
var initiative = require('./initiative.js');
var encounters = require('./encounters.js');

var f_characterDataFetchHandleNulls;
var f_characterDataFetchHandleNumbers;
var f_showStackedStatusLine;
var f_getUsefulItemFromExternalCache;
var f_getBlessingFromExternalCache;
var f_skillValue;
var f_skillFavored;
var f_charSkillTN;
var o_journeyRoleSkills;

// save stuff on bot startup
function reportsInit(characterDataFetchHandleNulls, characterDataFetchHandleNumbers, showStackedStatusLine, getUsefulItemFromExternalCache, getBlessingFromExternalCache, skillValue, skillFavored, charSkillTN, journeyRoleSkills) {
	f_characterDataFetchHandleNulls = characterDataFetchHandleNulls;
	f_characterDataFetchHandleNumbers = characterDataFetchHandleNumbers;
	f_showStackedStatusLine = showStackedStatusLine;
	f_getUsefulItemFromExternalCache = getUsefulItemFromExternalCache;
	f_getBlessingFromExternalCache = getBlessingFromExternalCache;
	f_skillValue = skillValue;
	f_skillFavored = skillFavored;
	f_charSkillTN = charSkillTN;
	o_journeyRoleSkills = journeyRoleSkills;
	return true;
}

const skillList = ['awe', 'athletics', 'awareness', 'hunting', 'song', 'craft', 'enhearten', 'travel', 'insight', 'healing', 'courtesy', 'battle', 'persuade', 'stealth', 'scan', 'explore', 'riddle', 'lore'];
const stanceList = ['forward', 'open', 'defensive', 'rearward'];

// dispatcher to functions for the specific reports
function generateReport(locale, g, rType, isLM) {
	switch (rType) {
	case 'skills': // Skills: all skill values in a dense format
		return skillsReport(locale, g);
	case 'roles': // Roles: all Journey-related skills to help choose roles
		return rolesReport(locale, g);
	case 'journey': // Journey: shows everyone in their journey roles and relevant skills
		return journeyReport(locale, g);

	case 'awe': // Awe
	case 'athletics': // Athletics
	case 'awareness': // Awareness
	case 'hunting': // Hunting
	case 'song': // Song
	case 'craft': // Craft
	case 'enhearten': // Enhearten
	case 'travel': // Travel
	case 'insight': // Insight
	case 'healing': // Healing
	case 'courtesy': // Courtesy
	case 'battle': // Battle
	case 'persuade': // Persuade
	case 'stealth': // Stealth
	case 'scan': // Scan
	case 'explore': // Explore
	case 'riddle': // Riddle
	case 'lore': // Lore
		return skillReport(locale, g, rType);

	case 'summary': // Summary: shows heart, strength, wits, wisdom, valour, etc.
		return summaryReport(locale, g);

	case 'strength': // Strength
	case 'heart': // Heart
	case 'wits': // Wits
	case 'valour': // Valour
	case 'wisdom': // Wisdom
		return attributeReport(locale, g, rType);

	case 'traits': // Traits: shows callings and distinctive features
		return traitsReport(locale, g);
	case 'weapons': // Weapons: a list of all weapons and their stats
		return weaponsReport(locale, g);
	case 'key': // Key: shows Google Sheets keys and links to character sheets
		return keyReport(locale, g);
	case 'treasure': // Treasure: shows treasure, standard of living, AP, and SP
		return treasureReport(locale, g);

	case 'hope': // Hope: shows all hope and the fellowship pool
	case 'shadow': // Shadow: shows shadow, shadow scars, and miserable status
		return tokenReport(locale, g, rType);
	case 'fatigue': // Fatigue: how tired is everyone?
		return fatigueReport(locale, g);
	case 'damage': // Damage: shows wounds and endurance loss
		return damageReport(locale, g);
	case 'stance': // Stance: who is in what stance, knocked back, and engaged?
		return stanceReport(locale, g);
	case 'adversaries': // Adversaries: status of all adversaries in the encounter
		return adversariesReport(locale, g, isLM);
	case 'combatlog': // Initiative: shows all the rounds
		if (g.combat.log == '') return slashcommands.localizedString(locale, 'error-nocombat', []);
		return g.combat.log;
	case 'initiative': // Initiative: shows all the rounds
		return initiative.initiativeReport(locale, g, false);
	}
}

// SUPPORT FUNCTIONS ----------------------------------------------------------

// assemble a localized title for a report
// reflecting both adjective-noun languages (English: Stance Report) and noun-of-adjective languages (Spanish: Informe de Postura)
function reportTitle(reportType, locale) {
	let reportjoiner = slashcommands.localizedString(locale, 'report-joiner', []);
	let report = stringutil.titleCase(slashcommands.localizedCommand(locale, 'report'));
	let r = slashcommands.localizedOption(locale, 'report', 0, reportType);
	if (r == reportType) r = slashcommands.localizedOption(locale, 'report', 1, reportType);
	reportType = stringutil.titleCase(r);
	if (reportjoiner == '')	{ // modifier first locale, e.g., English
		return reportType + ' ' + report;
	}
	// noun first locale, e.g., Portuguese
	return report + ' ' + reportjoiner + ' ' + reportType;
}

// assemble a full display of a skill including favored, useful item, and blessing
function skillPipsForReport(g, p, skill, abbreviated) {
	let r = emoji.sheetSkillPips(f_skillValue(f_characterDataFetchHandleNulls(g, p, 'sheet',skill)), abbreviated);
	let f = ':green_square:';
	if (f_skillFavored(f_characterDataFetchHandleNulls(g, p, 'sheet',skill))) f = ':white_check_mark:';
	let item = f_getUsefulItemFromExternalCache(g.characters[p].keyType, g.characters[p].repositoryKey, skill);
	if (item != '') r += ':tools:';
	item = f_getBlessingFromExternalCache(g.characters[p].keyType, g.characters[p].repositoryKey, skill);
	if (item != '') r += ':magic_wand:';
	return f + r;
}

// SKILLS REPORTS -------------------------------------------------------------

// Skills: all skill values in a dense format
function skillsReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'}
	];
	skillList.forEach(sk => {
		let localizedSkillName = slashcommands.localizedString(locale, 'skill-' + sk, []);
		reportHeadings.push({title:stringutil.titleCase(localizedSkillName.substring(0,2)), emoji:false, minWidth:2, align:'left'});
	});
	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		let rData = [f_characterDataFetchHandleNulls(g, p, 'name', null)];
		skillList.forEach(sk => {
			rData.push(f_characterDataFetchHandleNulls(g, p, 'sheet',stringutil.lowerCase(sk)));
		});

		reportData.push(rData);
	}
	return '**' + reportTitle('skills', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}

// Roles: all Journey-related skills to help choose roles
function rolesReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'skill-awareness', [])), emoji:true, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'skill-explore', [])), emoji:true, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'skill-hunting', [])), emoji:true, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'skill-craft', [])), emoji:true, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'skill-travel', [])), emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		reportData.push([
			stringutil.padRight(stringutil.trimRight(f_characterDataFetchHandleNulls(g, p, 'name', null),15), 15, ' '),
			skillPipsForReport(g, p, 'awareness',true),
			skillPipsForReport(g, p, 'explore',true),
			skillPipsForReport(g, p, 'hunting',true),
			skillPipsForReport(g, p, 'craft',true),
			skillPipsForReport(g, p, 'travel',true)
		]);
	}
	return '**' + reportTitle('roles', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}

// Journey: shows everyone in their journey roles and relevant skills
function journeyReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'skill-travel', [])), emoji:true, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'role', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'skill', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'amount', [])), emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;

		g.characters[p].journeyRole.forEach(role => {
			let jrole = stringutil.lowerCase(role);
			if (jrole == null) jrole = '';
			let jskill = o_journeyRoleSkills[jrole];
			if (jskill == null) jskill = '';

			reportData.push([
				stringutil.padRight(stringutil.trimRight(f_characterDataFetchHandleNulls(g, p, 'name', null),15), 15, ' '),
				skillPipsForReport(g, p, 'travel'),
				(jrole != '' ? stringutil.titleCase(slashcommands.localizedString(locale, 'role-' + jrole, [])) : ''),
				(jskill != '' ? stringutil.titleCase(slashcommands.localizedString(locale, 'skill-' + jskill, [])) : ''),
				((jrole != '' && jrole != 'absent') ? skillPipsForReport(g, p, jskill) : '')
			]);
		});

	}
	return '**' + reportTitle('journey', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}

// Skill: a specific skill with its TN
function skillReport(locale, g, skill) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'skill-' + skill, [])), emoji:true, minWidth:0, align:'left'},
		{title:stringutil.upperCase(slashcommands.localizedString(locale, 'report-tn', [])), emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		reportData.push([
			f_characterDataFetchHandleNulls(g, p, 'name', null),
			skillPipsForReport(g, p, skill, false),
			f_charSkillTN(g, p, skill)
		]);
	}
	return '**' + reportTitle(skill, locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);		
}

// ATTRIBUTES REPORTS ---------------------------------------------------------

// Summary: shows heart, strength, wits, wisdom, valour, etc.
function summaryReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-strength', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-heart', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-wits', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-valour', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-wisdom', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-parry', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-armour', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-damage', [])), emoji:false, minWidth:0, align:'left'}
		];

	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		let dmg = 0;
		for (w in f_characterDataFetchHandleNulls(g, p, 'weapons', null)) {
			if (dmg < f_characterDataFetchHandleNulls(g, p, 'weapons',w)[1]) dmg = f_characterDataFetchHandleNulls(g, p, 'weapons',w)[1];
		}
		let armourStats = f_characterDataFetchHandleNulls(g, p, 'sheet', 'armour');
		reportData.push([
			stringutil.padRight(stringutil.trimRight(f_characterDataFetchHandleNulls(g, p, 'name', null),15), 15, ' '),
			f_characterDataFetchHandleNumbers(g, p, 'sheet','strength'),
			f_characterDataFetchHandleNumbers(g, p, 'sheet','heart'),
			f_characterDataFetchHandleNumbers(g, p, 'sheet','wits'),
			f_characterDataFetchHandleNumbers(g, p, 'sheet','valour'),
			f_characterDataFetchHandleNumbers(g, p, 'sheet','wisdom'),
			String(f_characterDataFetchHandleNumbers(g, p, 'sheet','parry')) + (armourStats[3] > 0 ? ' +' + armourStats[3] : ''),
			String(armourStats[0] + armourStats[2]) + 'd' + (armourStats[1] > 0 ? ' +' + armourStats[1] : ''),
			(dmg == 0 ? '' : dmg)
		]);
	}
	return '**' + reportTitle('summary', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}

// A report for any specific attribute (e.g., Valour or Parry)
function attributeReport(locale, g, stat) {
	let r = '**' + reportTitle(stat, locale) + '**:\n';
	r += '`' + stringutil.padRight(stringutil.titleCase(slashcommands.localizedString(locale, 'report-character', [])),17,' ') + ' ';
	r += stringutil.titleCase(stringutil.padRight(slashcommands.localizedString(locale, 'stat-' + stat, []),11,' ')) + '`\n';
	r += '`----------------- ----------`\n';
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		r += f_showStackedStatusLine(g, p, stat, 'name', locale, false) + '\n';
	}
	r += '`----------------- ----------`\n';
	return r;
}

// Traits: shows callings and distinctive features
function traitsReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale,'stat-culture', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale,'stat-calling', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale,'stat-feature', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale,'stat-feature', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale,'stat-feature', [])), emoji:false, minWidth:0, align:'left'}
		];
	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		reportData.push([
			stringutil.padRight(stringutil.trimRight(f_characterDataFetchHandleNulls(g, p, 'name', null),15), 15, ' '),
			f_characterDataFetchHandleNulls(g, p, 'culture', null),
			f_characterDataFetchHandleNulls(g, p, 'calling', null),
			f_characterDataFetchHandleNulls(g, p, 'feature1', null),
			f_characterDataFetchHandleNulls(g, p, 'feature2', null),
			f_characterDataFetchHandleNulls(g, p, 'feature3', null)
		]);
	}
	return '**' + reportTitle('traits', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}

// Weapons: a list of all weapons and their stats
function weaponsReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'weapon', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'proficiency', [])), emoji:true, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'weapon-dmg', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'weapon-edge', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'weapon-inj', [])), emoji:false, minWidth:0, align:'left'}
		];

	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		for (weaponName in f_characterDataFetchHandleNulls(g, p, 'weapons', null)) {
			let weaponArray = f_characterDataFetchHandleNulls(g, p, 'weapons', weaponName);
			if (weaponArray == null) continue;
			//if (args[0] && !stringutil.lowerCase(weaponName).includes(stringutil.lowerCase(args[0]))) continue;
			reportData.push([
				stringutil.padRight(stringutil.trimRight(f_characterDataFetchHandleNulls(g, p, 'name', null),15), 15, ' '),
				weaponName,
				emoji.sheetSkillPips(f_skillValue(weaponArray[0])),
				String(weaponArray[1]),
				String(weaponArray[2]),
				String(weaponArray[3])
			]);
		}
	}
	return '**' + reportTitle('weapon', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}

// Key: shows Google Sheets keys and links to character sheets
function keyReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'player', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedSubcommand(locale, 'character', 'active')), emoji:true, minWidth:0, align:'left'},
		{title:stringutil.titleCase(slashcommands.localizedString(locale, 'character-key', [])), emoji:true, minWidth:0, align:'left'}
		];
	let reportData = [];
	for (let p in g.characters) {
		let thisPlayer = null;
		for (player in g.players) {
			if (g.players[player].characters.includes(Number(p))) thisPlayer = player;
		}
		if (thisPlayer == null && g.lmCharacters.includes(Number(p))) thisPlayer = slashcommands.localizedString(locale, 'loremaster', []);
		if (thisPlayer == null) thisPlayer = '?';
		else thisPlayer = String(thisPlayer);
		let keyLink = slashcommands.localizedString(locale, 'none', []);
		if (g.characters[p].repositoryKey != '') keyLink = '[' + g.characters[p].repositoryKey + '](<' + embeds.buildEmbedLink(g.characters[p].keyType, g.characters[p].repositoryKey) + '>)'
		reportData.push([
			f_characterDataFetchHandleNulls(g, p, 'name', null),
			thisPlayer,
			g.characters[p].active ? ':heavy_check_mark:' : ':x:',
			keyLink
		]);
	}
	return '**' + reportTitle('key', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}

// Treasure: shows treasure, standard of living, AP, and SP
function treasureReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:slashcommands.localizedString(locale, 'report-apspent', []), emoji:false, minWidth:0, align:'right'},
		{title:slashcommands.localizedString(locale, 'available', []), emoji:false, minWidth:0, align:'right'},
		{title:slashcommands.localizedString(locale, 'total', []), emoji:false, minWidth:0, align:'right'},
		{title:slashcommands.localizedString(locale, 'report-spspent', []), emoji:false, minWidth:0, align:'right'},
		{title:slashcommands.localizedString(locale, 'available', []), emoji:false, minWidth:0, align:'right'},
		{title:slashcommands.localizedString(locale, 'total', []), emoji:false, minWidth:0, align:'right'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-treasure', [])), emoji:false, minWidth:0, align:'right'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'stat-standardofliving', [])), emoji:false, minWidth:0, align:'left'}
		];
	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		reportData.push([
			stringutil.padRight(stringutil.trimRight(f_characterDataFetchHandleNulls(g, p, 'name', null),15), 15, ' '),
			f_characterDataFetchHandleNulls(g, p, 'adventurepointsspent', null),
			f_characterDataFetchHandleNulls(g, p, 'adventurepointsavailable', null),
			f_characterDataFetchHandleNulls(g, p, 'adventurepointstotal', null),
			f_characterDataFetchHandleNulls(g, p, 'skillpointsspent', null),
			f_characterDataFetchHandleNulls(g, p, 'skillpointsavailable', null),
			f_characterDataFetchHandleNulls(g, p, 'skillpointstotal', null),
			f_characterDataFetchHandleNulls(g, p, 'treasure', null),
			f_characterDataFetchHandleNulls(g, p, 'standardofliving', null)
		]);
	}
	return '**' + reportTitle('treasure', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}


// STATUS REPORTS -------------------------------------------------------------

// Hope: shows all hope and the fellowship pool
// Shadow: shows shadow, shadow scars, and miserable status
function tokenReport(locale, g, token) {

	let r = '**' + reportTitle(token, locale) + '**:\n';
	// add fellowship pool on hope
	if (token == 'hope') r += emoji.showStatusLineWithMax(stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-pool', [])), g.pool, g.maxpool, 'pool', false) + '\n';

	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		r += f_showStackedStatusLine(g, p, token, 'name', locale, false) + '\n';
	}
	//r += '`----------------- ----------`\n';
	return r;
}

// Fatigue: how tired is everyone?
function fatigueReport(locale, g) {
	let r = '**' + reportTitle('fatigue', locale) + '**:\n';
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		r += f_characterDataFetchHandleNulls(g, p, 'name', null) + '\n';
		r += '  ' + f_showStackedStatusLine(g, p, 'endurance', 'none', locale, true) + '\n';
		r += '  ' + f_showStackedStatusLine(g, p, 'fatigue', 'none', locale, true) + '\n';
	}
	return r;
}

// Damage: shows wounds and endurance loss
function damageReport(locale, g) {
	let r = '**' + reportTitle('damage', locale) + '**:\n';
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		// name
		r += '`' + stringutil.padRight(stringutil.trimRight(f_characterDataFetchHandleNulls(g, p, 'name', null),15), 15, ' ') + '` ';

		// wounds tokens
		let days = null;
		if (f_characterDataFetchHandleNulls(g, p, 'wounded', null) == 'FALSE') r += ':o:';
		else {
			r += emoji.tokenEmoji(f_characterDataFetchHandleNulls(g, p, 'woundtreated', ''));
			days = f_characterDataFetchHandleNulls(g, p, 'wounddaysleft', null);
		}

		// endurance number
		r += ' `' + stringutil.padLeft(f_characterDataFetchHandleNulls(g, p, 'endurance', null), 3,' ') + '` ';

		// endurance tokens
		if (f_characterDataFetchHandleNumbers(g, p, 'endurance', null) < 0) r += stringutil.repeatString(':large_blue_diamond:',-f_characterDataFetchHandleNumbers(g, p, 'endurance', null));
		else if (f_characterDataFetchHandleNumbers(g, p, 'endurance', null) > 20 || f_characterDataFetchHandleNumbers(g, p, 'maxendurance', null) > 20) r += emoji.halvedCircles('blue', f_characterDataFetchHandleNumbers(g, p, 'endurance', null), f_characterDataFetchHandleNumbers(g, p, 'maxendurance', null));
		else r += emoji.tokenEmojis('endurance', f_characterDataFetchHandleNumbers(g, p, 'endurance', null)) + stringutil.repeatString(':o:', f_characterDataFetchHandleNumbers(g, p, 'maxendurance', null)-f_characterDataFetchHandleNumbers(g, p, 'endurance', null));
		let net = f_characterDataFetchHandleNumbers(g, p, 'endurance', null) - f_characterDataFetchHandleNumbers(g, p, 'encumbrance', null) - f_characterDataFetchHandleNumbers(g, p, 'fatigue', null);
		if (g.characters[p].weary || net <= 0) r += ' **' + slashcommands.localizedString(locale, 'condition-weary', []) + '!**';
		let conditions = [];
		['knockback','daunted','shieldsmashed','seized','drained','dreaming','misfortune','dismayed','haunted','bleeding','poisoned','weary','miserable'].forEach(condition => {
			if (g.characters[p][condition]) {
				if (condition == 'poisoned') {
					conditions.push(slashcommands.localizedString(locale, 'condition-severity-' + g.characters[p][condition], []) + ' ' + slashcommands.localizedString(locale, 'condition-poison', []) + (g.characters[p].poisontreated ? ' (' + slashcommands.localizedString(locale, 'token-treated', []) + ')' : ''));
				}
				else conditions.push(slashcommands.localizedString(locale, 'condition-' + condition, []));
			}
		});
		if (days) {
			r += ' (' + days + ' ' + slashcommands.localizedString(locale, 'token-daysleft', []) + ')';
		}
		if (conditions.length != 0) {
			r += ' [' + conditions.join(', ') + ']';
		}
		r += '\n';
	}
	return r;
}

// Stance: who is in what stance, knocked back, and engaged?
function stanceReport(locale, g) {
	let reportHeadings = [
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'report-character', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'combat-stance', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'condition-knockback', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'combat-engagement', [])), emoji:false, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'total', [])), emoji:false, minWidth:0, align:'center'}
		];
	let reportData = [];
	for (let p in g.characters) {
		if (!g.characters[p].active) continue;
		reportData.push([
			stringutil.padRight(stringutil.trimRight(f_characterDataFetchHandleNulls(g, p, 'name', null),15), 15, ' '),
			slashcommands.localizedString(locale, 'stance-' + stringutil.lowerCase(g.characters[p].stance), []),
			g.characters[p].knockback == 1 ? slashcommands.localizedString(locale, 'on', []) : slashcommands.localizedString(locale, 'off', []),
			encounters.engaged_adversaries(g, p).join(', '),
			encounters.count_engagement(g, p)
		]);
	}
	let appendix = '';
	if (g.rallycurrent > 0 || g.rallynext > 0) {
		appendix = '\n**' + slashcommands.localizedString(locale, 'action-rally', []) + '**: ';
		if (g.rallycurrent > 0) {
			let sl = [];
			stanceList.slice(0,g.rallycurrent).forEach(s => {sl.push(slashcommands.localizedString(locale, 'stance-' + s))});
			appendix += sl.join(', ') + ' +1d ' + slashcommands.localizedString(locale, 'combat-thisround', []);
		}
		if (g.rallycurrent > 0 && g.rallynext > 0) appendix += '; ';
		if (g.rallynext > 0) {
			let sl = [];
			stanceList.slice(0,g.rallynext).forEach(s => {sl.push(slashcommands.localizedString(locale, 'stance-' + s))});
			appendix += sl.join(', ') + ' +1d ' + slashcommands.localizedString(locale, 'combat-nextround', []);
		}
	}
	return '**' + reportTitle('stance', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2) + appendix;
}

// Adversaries: status of all adversaries in the encounter
function adversariesReport(locale, g, isLM) {
	if (!g.combat.active || g.combat.encounter == '') return slashcommands.localizedString(locale, 'encounter-inactive', []);
	let adversaries = g.encounters[g.combat.encounter].adversaries;

	let hrHeading = stringutil.titleCaseWords(slashcommands.localizedString(locale, 'adversary-hate', [])).charAt(0) + '/' + stringutil.titleCaseWords(slashcommands.localizedString(locale, 'adversary-resolve', [])).charAt(0);

	let reportHeadings = [
		{title:' ', emoji:true, minWidth:0, align:'left'},
		{title:stringutil.titleCaseWords(slashcommands.localizedSubcommand(locale, 'encounter', 'adversary')), emoji:false, minWidth:0, align:'left'}
	];
	if (isLM) {
		reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedOption(locale, 'aedit','set','endurance')), emoji:false, minWidth:0, align:'left'});
		reportHeadings.push({title:hrHeading, emoji:false, minWidth:0, align:'left'});
		reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-spent', [])), emoji:false, minWidth:0, align:'left'});
		reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'token-wounds', [])).charAt(0), emoji:false, minWidth:0, align:'left'});
		reportHeadings.push({title:stringutil.upperCase(slashcommands.localizedString(locale, 'condition-kb', [])), emoji:false, minWidth:0, align:'center'});
		reportHeadings.push({title:stringutil.upperCase(slashcommands.localizedString(locale, 'condition-pb', [])), emoji:false, minWidth:0, align:'center'});
		reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedSubcommandOption(locale, 'foe', 'clear', 'type', 'weary')), emoji:false, minWidth:0, align:'center'});
		reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'test-testsheading', [])), emoji:false, minWidth:0, align:'left'});
	} else {
		reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedSubcommandOption(locale, 'encounter', 'set', 'property', 'name')), emoji:false, minWidth:0, align:'left'});
	}
	reportHeadings.push({title:stringutil.titleCaseWords(slashcommands.localizedString(locale, 'combat-engagement', [])), emoji:false, minWidth:0, align:'left'});

	let reportData = [];
	for (let a in adversaries) {
		let emoji = (encounters.adversaryOutOfCombat(adversaries[a]) ? ':skull_crossbones:' : ':troll:');
		if (g.config.includes('combat-advhealth') && emoji == ':troll:') {
			if (adversaries[a].wounds > 0) emoji = ':drop_of_blood:';
			else if (adversaries[a].curendurance < (adversaries[a].endurance * 3 / 10)) emoji = ':red_heart:';
			else if (adversaries[a].curendurance < (adversaries[a].endurance * 6 / 10)) emoji = ':yellow_heart:';
			else if (adversaries[a].curendurance < (adversaries[a].endurance * 9 / 10)) emoji = ':green_heart:';
		}
		let weary = '', protTests = '', engagement = '';
		if (adversaries[a].curhrscore <= 0) weary = slashcommands.localizedString(locale, 'condition-weary',[]);
		else if (adversaries[a].weary) weary = String(adversaries[a].weary);
		if (adversaries[a].protectionTests.length > 4) protTests = '(' + String(adversaries[a].protectionTests.length) + ')';
		else if (adversaries[a].protectionTests.length > 0) {
			let t = [];
			adversaries[a].protectionTests.forEach(test => {t.push(String(test.injury))});
			protTests = t.join(', ');
		}
		if (adversaries[a].engagement.length > 0) {
			let t = [];
			let maxlen = Math.floor(25 / adversaries[a].engagement.length);
			if (maxlen < 3) maxlen = 3;
			adversaries[a].engagement.forEach(e => {t.push(f_characterDataFetchHandleNulls(g, e, 'name', null).substring(0,maxlen))});
			engagement = t.join(',');
		}

		let reportDataEntry = [
			emoji,
			a
		];
		if (isLM) {
			reportDataEntry.push(adversaries[a].curendurance + '/' + adversaries[a].endurance);
			reportDataEntry.push(adversaries[a].curhrscore + '/' + adversaries[a].hrscore);
			reportDataEntry.push(adversaries[a].hateSpent + '/' + adversaries[a].might);
			reportDataEntry.push(String(adversaries[a].wounds));
			reportDataEntry.push(String(adversaries[a].knockback));
			reportDataEntry.push(adversaries[a].pushback ? stringutil.upperCase(slashcommands.localizedString(locale, 'condition-pb', [])) : '');
			reportDataEntry.push(weary);
			reportDataEntry.push(protTests);
		} else {
			reportDataEntry.push(adversaries[a].name);
		}
		reportDataEntry.push(engagement);
		reportData.push(reportDataEntry);
	}
	return '**' + reportTitle('adversaries', locale) + '**:\n' + table.buildTable(reportHeadings, reportData, 2);
}

module.exports = Object.assign({ reportsInit, generateReport, tokenReport });
