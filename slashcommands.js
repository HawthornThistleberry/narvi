// Handles all slash command building and localization; also handles output localization

const fs = require('fs');
var filehandling = require('./filehandling.js');

const slashCommandsFile = './slashcommands.json';


// INITIALIZATION -------------------------------------------------------------

// structures that will store the slash commands objects
var bot = null;
let commandFile = null;
let picklistFile = null;
let commonOptionsFile = null;
var locales = {};
let alreadyDidCommands = false;

// read in all the locale files
function readLocaleFiles() {
	fs.readdirSync('./locale/').forEach(file => {
		if (file.split('.')[1] == 'json') {
			let localeName = file.split('.')[0];
			try {
				locales[localeName] = filehandling.readJSONFile('./locale/' + file);
				checkForLengthLimits(localeName, locales[localeName].commands);
			} catch (err) {
				console.log('Error ' + String(err) + ' loading locale file locale/' + file);
			}
		}
	});
}

// find any problems in a local file, patch over them, and report them
function checkForLengthLimits(localeName, dictionary) {
	if (typeof dictionary != 'object') return;
	for (item in dictionary) {
		// check command desc
		//console.log('checking ' + item + ' in locale ' + localeName + ': ' + dictionary[item][1]);
		if (dictionary[item][1].length > 100) {
			console.log('LOCALE PROBLEM: ' + localeName + '.json: description for ' + item + ' too long, truncated [' + dictionary[item][1] + ']');
			dictionary[item][1] = dictionary[item][1].substr(0,97) + '...';
		}
		// for each option, recursively, check for problems
		checkForLengthLimits(localeName, dictionary[item][2]);
	}
}

// change name from Narvi to Rúmil as appropriate
function botRename(s) {
	if (bot.user.username == 'Narvi') return s;
	if (s.includes('Narvi')) {
		return s.split('Narvi').join(bot.user.username);
	}
	return s;
}

// as above but for an options array's descriptions
function botRenameOptions(opts) {
	let r = [];
	if (opts == null || opts.length == 0) return opts;
	opts.forEach(opt => {
		let newopt = opt;
		if (newopt.description) newopt.description = botRename(newopt.description);
		r.push(newopt);
	});
	return r;
}

// initialize function to read locale files and prepare slash commands
function slashInitializeLocales() {
	try {
		// load all the command and locale files
		commandFile = filehandling.readJSONFile(slashCommandsFile);
		picklistFile = filehandling.readJSONFile('./picklists.json');
		commonOptionsFile = filehandling.readJSONFile('./commonoptions.json');
		readLocaleFiles();
		console.log('Finished loading locale files');
	}
	catch (err) {
		console.log('Caught error in slashInitializeLocales: ' + err + '\n' + err.stack);
	}
}

// initialize function to prepare slash commands
function slashInitialize(theBot, notifyHostUser, commandsGuild, slashLastLoaded, saveSlashLastLoadedTimestamp) {
	bot = theBot;

	/*
	// remove non-guild commands from Rúmil:
	if (commandsGuild != '' && commandsGuild != undefined) {
		bot.getCommands()
		.then(response => {
			response.forEach(cmd => {
				console.log('deleting ' + cmd.fullLabel);
				bot.deleteCommand(cmd.id);
				//cmd.deleteCommand = true;
			});
		});
	}
	*/

	let slashLastModified = filehandling.fileLastModified(slashCommandsFile);

	if (slashLastModified == slashLastLoaded) {
		console.log('Skipping slash command load since the file has not been modified');
		alreadyDidCommands = true;
	}

	if (alreadyDidCommands) return;

	// if the command and locale files haven't loaded, try again in five seconds
	if (commandFile == null || picklistFile == null || commonOptionsFile == null || locales['base'] == null) {
		notifyHostUser('Delaying preparing slash commands for five seconds due to incomplete locale loads',false);
		setTimeout(function(){ slashInitialize(theBot, notifyHostUser, commandsGuild, slashLastLoaded, saveSlashLastLoadedTimestamp); }, 5000);
		return;
	}

	try {
		
		let commands = [];

		for (cmd in commandFile) {

			// process any common elements that'll become part of the options for this command
			let opts = commandFile[cmd].options;
			if (Array.isArray(opts)) {
				for (let i = 0; i < opts.length; i++) {

					// if the options include a choices entry that has a string instead of an array, it's a picklist; replace it with the corresponding picklist entry
					if (opts[i].choices && !Array.isArray(opts[i].choices)) {
						opts[i].choices = picklistFile[opts[i].choices];
					}

					// if any of the options has only a 'commonoptions' element, replace it with the corresponding commonOptionsFile entry
					if (opts[i].commonoptions) {
						// remove opts[i]
						let copts = opts[i].commonoptions.split('|');
						opts.splice(i,1);
						copts.forEach(copt => {
							// append the entries in commonOptionsFile
							if (commonOptionsFile[copt] == null) {
								notifyHostUser('Failed to find common options for ' + copt + ' when preparing command `/' + cmd + '`!\nCommon options file has these keys: [' + Object.keys(commonOptionsFile).join(', ') + ']',false);
							} else {
								opts = opts.concat(commonOptionsFile[copt]);
							}
						});
						// move back so we can reprocess the item now in slot i which just moved down
						i--;
					}
				}
			}

			// create the base English command structure
			let cmdStructure = {
				'name': cmd,
				'name_localizations': {},
				'description': botRename(commandFile[cmd].description),
				'description_localizations': {},
				'options': botRenameOptions(opts)
			};

			// for every localization that has this command, add its localizations, including options
			// excluding the base locale, which is only for strings
			for (locale in locales) 
				if (locale != 'base') cmdStructure = localizeCmdStructure(cmdStructure, locale, locales[locale]);

			// add the resulting structure to the bulk list
			commands.push(cmdStructure);
		}

		// do the bulk add
		// console.log(JSON.stringify(commands[47]));
		if (commandsGuild != '' && commandsGuild != undefined) {
			console.log('loading guild application commands to ' + commandsGuild);
			bot.bulkEditGuildCommands(commandsGuild, commands);
		} else {
			console.log('loading bulk application commands');
			bot.bulkEditCommands(commands);
		}
		alreadyDidCommands = true;
		saveSlashLastLoadedTimestamp(slashLastModified);
		return;

	}
	catch (err) {
		console.log('Caught error in slashInitialize: ' + err + '\n' + err.stack);
		return;
	}
}

// tells whether a given command is available as a slash command (no longer used)
function isSlashCommand(cmd) {
	if (commandFile && commandFile[cmd]) return true;
	return false;
}

// insert localizations for a single locale into a command structure
function localizeCmdStructure(cmdStructure, localeName, localeData) {

	// verify if this command is even in here; if not return the unmodified file
	if (localeData.commands[cmdStructure.name] == null) return cmdStructure;

	// add the command's own name and description, which will always be present
	cmdStructure.name_localizations[localeName] = localeData.commands[cmdStructure.name][0];
	cmdStructure.description_localizations[localeName] = botRename(localeData.commands[cmdStructure.name][1]);

	// add localizations to the options and choices; this will call itself recursively as necessary
	if (cmdStructure.options != null) {
		cmdStructure.options = localizeCmdStructureOptions(cmdStructure.options, localeName, localeData.commands[cmdStructure.name][2]);
	}
	if (cmdStructure.choices != null) {
		//console.log('localizing choices: ' + JSON.stringify(cmdStructure.choices)); 
		cmdStructure.choices = localizeCmdStructureOptions(cmdStructure.choices, localeName, localeData.commands[cmdStructure.name][2]);
		//console.log('after localization: ' + JSON.stringify(cmdStructure.choices)); 
	}

	// return the modified structure
	return cmdStructure;
}

// localize the options at a single level, calling yourself recursively if necessary
function localizeCmdStructureOptions(optionsArray, localeName, localeData) {

	let thisLocaleData = {};

	// if the localeData is actually a picklist identified by a string, replace it with the actual picklist from this locale
	if (typeof localeData === 'string') {
		//console.log('replacing picklist for ' + localeData + ' in optionsArray ' + JSON.stringify(optionsArray));
		thisLocaleData = {};
		for (let key in locales[localeName].picklists[localeData]) {
			thisLocaleData[key] = [key, locales[localeName].picklists[localeData][key],{}];
		}
		//localeData = locales[localeName].picklists[localeData];
		//console.log('  replacement is ' + JSON.stringify(thisLocaleData));
	} else {
		thisLocaleData = Object.assign({}, localeData);
		//console.log('  without replacement localeData is ' + JSON.stringify(thisLocaleData));
	}

	// include any commonoptions if they are present in this locale
	//let thisLocaleData = Object.assign({}, localeData); // makes a temporary copy so we can add to it
	if (thisLocaleData.commonoptions) {
		//console.log('replacing commonoptions for ' + thisLocaleData.commonoptions);
		let copts = thisLocaleData.commonoptions[0].split('|');
		copts.forEach(copt => {
			let coptions = locales[localeName].commonoptions[copt];
			for (let k in coptions) {
				thisLocaleData[k] = coptions[k];
			}
		});
		delete thisLocaleData.commonoptions;
		//console.log('  replacement is ' + JSON.stringify(thisLocaleData));
	} //else console.log('  without replacement is ' + JSON.stringify(thisLocaleData));

	for (let i = 0; i < optionsArray.length; i++) {
		let optionName = optionsArray[i].name;
		let value = false;
		if (optionsArray[i].value != undefined) {
			optionName = optionsArray[i].value;
			value = true;
			//console.log('value name for ' + i + ' = ' + JSON.stringify(optionsArray[i]));
		}

		// verify if the localization data is present before attempting to load it
		if (thisLocaleData[optionName] != null && thisLocaleData[optionName].length >= 2) {
			if (optionsArray[i].name_localizations == null) optionsArray[i].name_localizations = {};
			if (value) { // this is a choices block; name is the second field, we never use the first
				//console.log('value name localizing for ' + i + ' for optionName ' + optionName + ' is ' + thisLocaleData[optionName][1]);
				optionsArray[i].name_localizations[localeName] = thisLocaleData[optionName][1];
			} else { // options block; name is the first, description is the second
				optionsArray[i].name_localizations[localeName] = thisLocaleData[optionName][0];
				if (optionsArray[i].description_localizations == null) optionsArray[i].description_localizations = {};
				optionsArray[i].description_localizations[localeName] = botRename(thisLocaleData[optionName][1]);
			}
			if (optionsArray[i].options != null) {
				// recursive call
				optionsArray[i].options = localizeCmdStructureOptions(optionsArray[i].options, localeName, thisLocaleData[optionName][2]);
			}
			if (optionsArray[i].choices != null) {
				// recursive call
				//console.log('recursively localizing ' + localeName + ' choices: ' + JSON.stringify(optionsArray[i])); 
				optionsArray[i].choices = localizeCmdStructureOptions(optionsArray[i].choices, localeName, thisLocaleData[optionName][2]);
				//console.log('after localization: ' + JSON.stringify(optionsArray[i])); 
			}
		}
	}
	return optionsArray;
}

// localize the choices at a single level
function localizeCmdStructureChoices(choicesArray, localeName, localeData) {
	for (let i = 0; i < choicesArray.length; i++) {
		let choiceName = choicesArray[i].name;
		// verify if the localization data is present before attempting to load it
		if (localeData[choiceName] != null && localeData[choiceName].length >= 2) {
			if (choicesArray[i].name_localizations == null) choicesArray[i].name_localizations = {};
			choicesArray[i].name_localizations[localeName] = localeData[choiceName][1];
		}
	}
	return choicesArray;
}

// return back the options for a particular command
function dumpOptions(cmd, locale) {
	let source, result = {};

	// first try a locale-based result
	if (locales[locale] != null && locales[locale].commands[cmd] != null) {
		// use locale data to build the result
		source = locales[locale].commands[cmd][2];
		for (let element in source) {
			result[element] = [];
			for (let subelement in source[element][2]) {
				result.element.push(source[element][2][subelement][1]);
			}
		}
		return result;
	}

	// since that failed, try building something from the base commands file
	if (commandFile[cmd] == null) return null;
	source = commandFile[cmd].options;
	if (source == null) return null;
	source.forEach(element => {
		result[element.name] = [];
		element.choices.forEach(subelement => {
			result[element.name].push(subelement.name);
		});
	});
	return result;
}

// LOCALIZATION ---------------------------------------------------------------

// localize a string for return
function localizedString(locale, id, params) {
	// if an unknown locale, switch back to base
	if (!locales[locale]) locale = 'base';
	if (!locales[locale]) return '(_still loading locales_: `' + id + '`)'; // in case someone tried something so early that the load hasn't happened yet
	// find the localized string
	let r = locales[locale].strings[id];
	// if not found, fall back to base
	if (r == null) r = locales['base'].strings[id];
	// if still not found, log and abort with a desperate fallback
	if (r == null) {
		console.log('Localization error: locale=' + locale + ' id=' + id + ' not found!');
		return (id + ' ' + params.join(' ')).trim();
	}
	// substitute parameters, if present
	// replace '<@' with '\u274C'; this is a hack to avoid substituting any @ that is immediately preceeded by a <
	let regex;
	regex = new RegExp('<@', 'g');
	r = r.replace(regex, '\u274C');
	if (r.includes('@') && params.length > 0) {
		let i = 1;
		params.forEach(param => {
			regex = new RegExp('@' + String(i), 'g');
			r = r.replace(regex, String(param));
			i++;
		});
	}
	// replace '\u274C' with '<@'
	regex = new RegExp('\u274C', 'g');
	r = r.replace(regex, '<@');
	return r;
}

// find the localized version of a base command
function localizedCommand(locale, cmd) {
	if (!locales[locale] || !locales[locale].commands || !locales[locale].commands[cmd]) return cmd;
	return locales[locale].commands[cmd][0];
}

// find the localized version of a subcommand
function localizedSubcommand(locale, cmd, subcmd) {
	if (!locales[locale] || !locales[locale].commands || !locales[locale].commands[cmd] || !locales[locale].commands[cmd][2] || !locales[locale].commands[cmd][2][subcmd]) return subcmd;
	return locales[locale].commands[cmd][2][subcmd][0];
}

// find the localized version of an option
function localizedOption(locale, cmd, index, choice) {
	//console.log('localizedOption('+ locale + ', '+ cmd+ ', '+ index + ', '+ choice + ') indexing into ' + JSON.stringify(locales[locale].commands[cmd]));
	if (!locales[locale] || !locales[locale].commands || !locales[locale].commands[cmd] || !locales[locale].commands[cmd][2][index] || !locales[locale].commands[cmd][2][index][2] || !locales[locale].commands[cmd][2][index][2][choice]) return choice;
	return locales[locale].commands[cmd][2][index][2][choice][0];
}

// find the localized version of an option
function localizedOptionDescription(locale, choices) {
	try {
		if (locales[locale] == undefined || locales[locale].commands == undefined) {
			// this language doesn't have a locale commands translation file; it's probably the default language, so find this in the slashcommands.json file
			let result = null;
			commandFile[choices[0]].options.forEach(option => {
				if (option.name == choices[1]) {
					option.options.forEach(o2 => {
						if (o2.name == choices[2]) {
							o2.choices.forEach(o3 => {
								if (o3.value == choices[3]) {
									result = o3.name;
								}
							});
						}
					});
				}
			});
			if (result) return result;
			return choices.join(' ');
		}
		// the language does have a commands file so fetch from there
		return locales[locale].commands[choices[0]][2][choices[1]][2][choices[2]][2][choices[3]][1];
	} catch (err) {
		return choices.join(' ');
	}
}

function localizedSuboption(locale, cmd, index, choice) {
	//console.log('localizedSuboption('+ locale + ', '+ cmd+ ', '+ index+ ', '+ choice + ') indexing into ' + JSON.stringify(locales[locale].commands[cmd][2][index][2].option[2][choice][0]));
	try {
		return locales[locale].commands[cmd][2][index][2].option[2][choice][0];
	} catch (err) {
		return choice;
	}
}

// find the localized version of an option within a subcommand
function localizedSubcommandOption(locale, cmd, subcmd, index, choice) {
	if (!locales[locale] || !locales[locale].commands || !locales[locale].commands[cmd] || !locales[locale].commands[cmd][2][subcmd] || !locales[locale].commands[cmd][2][subcmd][2][index] || !locales[locale].commands[cmd][2][subcmd][2][index][2] || !locales[locale].commands[cmd][2][2][subcmd][index][2][choice]) return choice;
	return locales[locale].commands[cmd][2][subcmd][2][index][2][choice][0];
}

// find the localized version of a picklist item
function localizedPicklist(locale, picklist, option) {
	if (!locales[locale] || !locales[locale].picklists || !locales[locale].picklists[picklist]) return option;
	return locales[locale].picklists[picklist][choice];
}

// a list of known locales in code form
function localesKnown() {
	let r = Object.keys(locales);
	r = r.filter(l => l != 'base');
	r.unshift('en-US');
	return r;
}

// a list of known locales in friendly form
const localeNames = {
	'id':		'Bahasa Indonesia',
	'da':		'Dansk',
	'de':		'Deutsch',
	'en-GB':	'English, UK',
	'en-US':	'English, US',
	'es-ES':	'Español',
	'fr':		'Français',
	'hr':		'Hrvatski',
	'it':		'Italiano',
	'lt':		'Lietuviškai',
	'hu':		'Magyar',
	'nl':		'Nederlands',
	'no':		'Norsk',
	'pl':		'Polski',
	'pt-BR':	'Português do Brasil',
	'ro':		'Română',
	'fi':		'Suomi',
	'sv-SE':	'Svenska',
	'vi':		'Tiếng Việt',
	'tr':		'Türkçe',
	'cs':		'Čeština',
	'el':		'Ελληνικά',
	'bg':		'български',
	'ru':		'Pусский',
	'uk':		'Українська',
	'hi':		'हिन्दी',
	'th':		'ไทย',
	'zh-CN':	'中文',
	'ja':		'日本語',
	'zh-TW':	'繁體中文',
	'ko':		'한국어'
};

function localesKnownNames() {
	let l = localesKnown();
	let r = [];
	l.forEach(locale => {
		r.push(localeNames[locale]);
	});
	return r;
}

// EXPORTS --------------------------------------------------------------------

module.exports = Object.assign({ slashInitialize, slashInitializeLocales, isSlashCommand, localizedString, localizedCommand, localizedSubcommand, localizedOption, localizedOptionDescription, localizedSuboption, localizedSubcommandOption, localizedPicklist, dumpOptions, localesKnown, localesKnownNames });

/* The following Eris updates are required to make this work, as of 0.17.1 (after a few months of abandonment):

in node_modules\eris\lib\structures\CommandInteraction.js around line 105 after the setting of guildID:

        if(info.locale !== undefined) { // FJP hack to get interaction locale
            this.locale = info.locale ;
        }

*/

