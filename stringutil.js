// STRING UTILITIES -----------------------------------------------------------
// some of these are functions that Javascript and NodeJS should have, but somehow don't
// some are wrappers to ensure safety in case of null/undefined values etc. (instead of crashing)
// and some are genuinely string utilities!

function repeatString(s, num) {
	let r = '', i;
	for (i = 0; i < num ; i++) {
		r += s;
	}
	return r;
}

function padRight(s, len, pad) {
	return s + repeatString(pad, len - String(s).length);
}

function padRightIgnoreBold(s, len, pad) {
	return s + repeatString(pad, len - String(s.replace(/\*\*/g, '')).length);
}

function padLeft(s, len, pad) {
	return repeatString(pad, len - String(s).length) + s;
}

function padCenter(s, len, pad) {
	let fullPad = len - String(s).length;
	let leftPad = Math.floor(fullPad / 2);
	return repeatString(pad, leftPad) + s + repeatString(pad, fullPad-leftPad);
}

function trimRight(s, len) {
	if (s.length <= len) return s;
	return s.slice(0,len-3) + '...';
}

function trimPadRight(s, len) {
	if (s.length <= len) return padRight(s, len, ' ');
	return s.slice(0,len-3) + '...';
}

function titleCase(s) {
	if (s == null || s == undefined || s == '') return '';
	return String(s).substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
}

function lowerCase(s) {
	if (s == null || s == undefined || s == '') return '';
	return String(s).toLowerCase();
}

function upperCase(s) {
	if (s == null || s == undefined || s == '') return '';
	return String(s).toUpperCase();
}

function titleCaseWords(s) {
	if (s == null || s == undefined || s == '') return '';
	return s.replace(/[^\s]+/g, function(word) {
	  return word.replace(/^./, function(first) {
		return first.toUpperCase();
	  });
	});
	/*return s.replace(/\b\w/g, function(match) {
		return match.toUpperCase();
	});*/
}

// why isn't this a native function in Javascript, it comes up so much!
function caseInsensitiveMatch(s1, s2) {
	if (s1 == null && s2 == null) return true;
	if (s1 == null || s2 == null) return false;
	return s1.toLowerCase() == s2.toLowerCase();
}

// search a list for a word, case insensitively, finding only exact matches
function listIncludesCaseInsensitive(list, s) {
	let found = false;
	list.forEach(x => {
		if (caseInsensitiveMatch(x,s)) found = true;
	});
	return found;
}

// search a list for a word, case insensitively, finding exact matches first, then substring matches when exact matches aren't there
function findMatchInList(list, s) {
	// first check for exact matches
	if (listIncludesCaseInsensitive(list,s)) return s;
	// next check for substring matches
	let r = '';
	list.forEach(x => {
		if (r == '' && x.toLowerCase().startsWith(s.toLowerCase())) r = x;
	});
	return r;
}

// strip strings down to things that are safe to store in JSON files -- this one is aggressive and good for things like game names
// this originally came from https://stackoverflow.com/questions/23187013/is-there-a-better-way-to-sanitize-input-with-javascript
function sanitizeString(str){
	if (!str || typeof str != 'string') return '';
    str = str.replace(/[^a-z0-9\u00E1\u00C1\u00E9\u00C9\u00ED\u00CD\u00F3\u00D3\u00FA\u00DA\u00FD\u00DD\u00E2\u00C2\u00EA\u00CA\u00EE\u00CE\u00F4\u00D4\u00FB\u00DB\u00E4\u00C4\u00EB\u00CB\u00EF\u00CF\u00F6\u00D6\u00FC\u00DC\u00E0\u00C0\u00E8\u00C8\u00EC\u00CC\u00F2\u00D2\u00F9\u00D9\u00E3\u00C3\u00F1\u00D1\u00F5\u00D5\u00E7\u00C7 \.,_\-]/gim,'');
    return str.trim();
}

// a milder version of the above that also adds various punctuation; good for things like table text
function mildSanitizeString(str){
	if (!str || typeof str != 'string') return '';
    str = str.replace(/[^a-z0-9\u00E1\u00C1\u00E9\u00C9\u00ED\u00CD\u00F3\u00D3\u00FA\u00DA\u00FD\u00DD\u00E2\u00C2\u00EA\u00CA\u00EE\u00CE\u00F4\u00D4\u00FB\u00DB\u00E4\u00C4\u00EB\u00CB\u00EF\u00CF\u00F6\u00D6\u00FC\u00DC\u00E0\u00C0\u00E8\u00C8\u00EC\u00CC\u00F2\u00D2\u00F9\u00D9\u00E3\u00C3\u00F1\u00D1\u00F5\u00D5\u00E7\u00C7 \.,_\-\!@\#\$\%\^\&\*\(\)\/\[\]\'=+;:\?]/gim,'');
    return str.trim();
}

function multiwordMatch(s1, s2) {
	s1 = lowerCase(wordWithoutSpaces(sanitizeString(s1)));
	s2 = lowerCase(wordWithoutSpaces(sanitizeString(s2)));
	return (s1 == s2);
}

function multiwordMatchSubstring(s1, s2) {
	s1 = lowerCase(wordWithoutSpaces(sanitizeString(s1)));
	s2 = lowerCase(wordWithoutSpaces(sanitizeString(s2)));
	return (s1 == s2) || (s1.startsWith(s2));
}

function wordWithoutSpaces(s) {
	return s.replace(/\s/g, '');
}

function stringWithoutNewlines(s) {
	return s.replace(/\n\n/g, '\n').replace(/\n/g, '; ');
}

/* convert to Small Caps based on https://stackoverflow.com/questions/59969250/converting-a-string-to-small-caps-pseudoalphabet-in-java
but that's Java, and Javascript does not have a codePointOf, so I need my own lookup table (surprised no one else had done this)
*/
function makeSmallCaps(s) {
	let smallCaps = ['\u1D00', '\u0299', '\u1D04', '\u1D05', '\u1D07', '\ua730', '\u0262', '\u029c', '\u026a', '\u1D0a', '\u1D0b', '\u029f', '\u1D0d', '\u0274', '\u1D0f', '\u1D18', '\ua7af', '\u0280', '\ua731', '\u1D1b', '\u1D1c', '\u1D20', '\u1D21', '\u0078', '\u028f', '\u1D22',];
	let r = '';
	if (!s || s == '') return '';
	for (i=0; i < s.length; i++) {
		let c = s.charAt(i);
		if (c >= 'a' && c <= 'z') c = smallCaps[c.charCodeAt(0) - 'a'.charCodeAt(0)];
		r += c;
	}
	return r;
}


module.exports = Object.assign({ repeatString, padRight, padRightIgnoreBold, padLeft, padCenter, trimRight, trimPadRight, lowerCase, upperCase, titleCase, titleCaseWords, caseInsensitiveMatch, listIncludesCaseInsensitive, findMatchInList, sanitizeString, mildSanitizeString, multiwordMatch, multiwordMatchSubstring, wordWithoutSpaces, stringWithoutNewlines, makeSmallCaps });
