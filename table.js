// TABLE CREATOR ---------------------------------------------------------------

// parameter is an array of column definitions, then an array of arrays of row values
// for each column, there is a title, a minwidth, an alignment, and a flag about whether it's made of emojis

var stringutil = require('./stringutil.js');

// take a string of emoji and figure out how many emoji wide it is
// only works if the string is nothing but Discord-format-emoji (e.g., :one:), stacked right on top of one another
function emojiWidth(s) {

	if (s == null || s == undefined || s == '') return 0;
	s = String(s);
	let len = 0;
	let inside = 0; // 0 = nothing, 1 = colon delineated emoji, 2 = bracket delineated emoji
	for (let i = 0; i < s.length; i++) {
		let c = s.substr(i,1);
		if (c == '<' && inside == 0) {
			inside = 2;
		} else if (c == '>' && inside == 2) {
			len += 3;
			inside = 0;
		} else if (c == ':' && inside == 0) {
			inside = 1;
		} else if (c == ':' && inside == 1) {
			len += 3;
			inside = 0;
		} else if (inside == 0) {
			len += 1;
		}
	}
	return len;
	//return Math.floor(s.split('::').length * 3); // each emoji takes up about the same space as three monospace characters
}

function columnAlign(s, width, alignment) {
	if (alignment == 'right') return stringutil.padLeft(s, width, ' ');
	if (alignment == 'center') return stringutil.padCenter(s, width, ' ');
	return stringutil.padRight(s, width, ' ');
}

// column format: array of {title:'title', emoji:false, minWidth:6, align:'left'}
// contents format: array of array of values
// spacing: number of spaces between columns
function buildTable(columns, contents, spacing) {
	let colMinWidths = [];
	let emojiCol = [];
	let colAlign = [];

	// calculate the column widths starting with just the headings
	columns.forEach(col => {
		colMinWidths.push(Math.max(col.minWidth, col.title.length));
		emojiCol.push(col.emoji);
		colAlign.push(col.align);
	});

	// loop through contents and widen as necessary
	let colnum;
	contents.forEach(row => {
		colnum = 0;
		row.forEach(item => {
			let colWidth = String(item).length;
			if (emojiCol[colnum]) colWidth = emojiWidth(item); else colWidth = String(item).length;
			if (colWidth > colMinWidths[colnum]) colMinWidths[colnum] = colWidth;
			colnum++;
		});
	});

	// build heading row -- unless they're all blank
	let dashRow = '`';
	let r = '';
	let blankHeader = true;
	columns.forEach(col => {if (col.title != '') blankHeader = false;});
	if (!blankHeader) {
		r = '`';
		colnum = 0;
		columns.forEach(col => {
			r += columnAlign(col.title, colMinWidths[colnum], col.align) + stringutil.repeatString(' ',spacing);
			colnum++;
		});
		r = r.slice(0,-spacing) + '`\n';
		// build dashes row
		colMinWidths.forEach(w => {
			dashRow += stringutil.repeatString('-',w) + stringutil.repeatString(' ',spacing);
		});
		dashRow = dashRow.slice(0,-spacing) + '`';
		r += dashRow + '\n';
	}


	// build contents rows
	contents.forEach(row => {
		if (!emojiCol[0]) r += '`';
		colnum = 0;
		row.forEach(item => {
			if (emojiCol[colnum] && item != '') r += (colnum == 0 ? '' : '`') + item + '`' + stringutil.repeatString(' ',spacing + colMinWidths[colnum] - emojiWidth(item));
			else r += columnAlign(item, colMinWidths[colnum], colAlign[colnum]) + stringutil.repeatString(' ',spacing);
			colnum++;
		});
		r = r.trim() + ' `\n';
	});

	// add another dashes row
	if (!blankHeader) r += dashRow;

	return r;
}

module.exports = Object.assign({ buildTable });
